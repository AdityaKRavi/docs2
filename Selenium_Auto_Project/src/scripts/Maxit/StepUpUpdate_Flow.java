package scripts.Maxit;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Test;

import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.StepupFns;
import functionalLibrary.Maxit.StepupUpdate_DataProvider;

public class StepUpUpdate_Flow extends BaseClass {
	StepupFns objStepUpScriptFns ;

	Reporting reporter = new Reporting();	

	Map<String, Boolean> stepUpdateStatusMapper = new HashMap<String, Boolean>();

	//***********************************Stepup_Update Update Data Provider*************************************************************
	@Test(description = "StepupUpdate",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_StepupUpdateTestData",dataProviderClass = StepupUpdate_DataProvider.class)
	public void StepupUpdate(String To_Execute, String strAcct, String strSecType, String strSecValue, String stepUpLevel, String strStepUp_DoD, String altValuationDate, 
			String rowNumberToEdit, String fmvPerShare, String stepPercentage, String bondSecNo, String useCaseNumFromSheet , int exclRowCnt, String strSheetName) throws Exception {
		boolean stepUpUpdateStatus = false;

		/*creating object and setting it globally for the class &
		passing driver object to prevent null pointer*/
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		setobject_UpdateScriptFns(driver);


		if(To_Execute.equalsIgnoreCase("Y")){
			
			//Getting the status of stepup update process
			stepUpUpdateStatus = objStepUpScriptFns.StepUpUpdate_Flow(strAcct, strSecType, strSecValue, stepUpLevel, strStepUp_DoD, altValuationDate, rowNumberToEdit, fmvPerShare, stepPercentage, bondSecNo, exclRowCnt, strSheetName);

			System.out.println("Inside StepUpUpdate :After update " +strSheetName + "stepUpUpdateStatus :  "+stepUpUpdateStatus);

			//If the stepup is applied successfully, Adding the status "true" in the map.
			if(stepUpUpdateStatus){

				//Generating Key 
				String statusKey = "["+To_Execute+";useCaseNumFromSheet:"+useCaseNumFromSheet+";Account:"+strAcct+"]";
				System.out.println("statusKey StepUpUpdate "+statusKey);

				//Updating the status = true in map. Using this map, user can get the status of the stepup update for the corresponding account-security and perform data validation for stepup successfully applied tests.
				stepUpdateStatusMapper.put(statusKey, stepUpUpdateStatus);
				System.out.println("Mapper value StepUpUpdate "+stepUpdateStatusMapper.get(statusKey) + " stepUpdateStatusMapper size :" +stepUpdateStatusMapper.size());
			}
			else{
				//Generating Key
				String statusKey = "["+To_Execute+";useCaseNumFromSheet:"+useCaseNumFromSheet+";Account:"+strAcct+"]";
				System.out.println("statusKey StepUpUpdate "+statusKey);

				//Updating the status = "false" in map.
				stepUpdateStatusMapper.put(statusKey, stepUpUpdateStatus);

				System.out.println("StepUp has been already applied or there is an issue with updating stepup , so please check it. !!");
				reporter.reportStep(exclRowCnt, "Failed", "StepUp has been already applied or there is an issue with updating stepup , so stepup update has been skipped. Please check it. !!", "", "StepUp_update_Failed", driver, "", strSheetName, null);
			}

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: StepupUpdate>

	//***********************************End of Step Up Data Provider*************************************************************		

	//***********************************Step Up Data validation DataProvider*************************************************************
	@Test(description = "StepUp_DataValidation",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_StepupDataValidationTestData",dataProviderClass = StepupUpdate_DataProvider.class)
	public void StepUp_DataValidation(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCountFromSheet , String strColNames, String strStepUp_DoD, String altValuationDate, String[] strExpRows, String useCaseNumFromSheet, int exclRowCnt , String strSheetName) throws Exception {
		Boolean stepupUpdateStatus;
		if(To_Execute.equalsIgnoreCase("Y")){

			//Generating key to fetch the status from Map for the match
			String statusKey = "["+To_Execute+";useCaseNumFromSheet:"+useCaseNumFromSheet+";Account:"+strAcct+"]";
			System.out.println("statusKey StepUp_DataValidation"+statusKey);

			//Retrieving status for generated key
			stepupUpdateStatus = stepUpdateStatusMapper.get(statusKey);

			//if the status is true then perform data validation for the search data 
			if(stepupUpdateStatus)
			{
				objStepUpScriptFns.StepUpResult_DataValidation(strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCountFromSheet , strColNames, strStepUp_DoD, altValuationDate, strExpRows, exclRowCnt , strSheetName);		
			}
			else if(!stepupUpdateStatus)
			{
				
				reporter.reportStep(exclRowCnt, "Failed", "StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so Data validation has been skipped. Please check it. !!", "", "StepUp_validation_Failed", driver, "", strSheetName, null);
				System.err.println("StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so  Data validation has been skipped. Please check it. !!");
				System.out.println("\n#########################[StepUp_DataValidation : End  Row iteration:["+exclRowCnt+"] with test data:"+"[STEUP UP DATA Validation;Account:"+strAcct+";SecVal:"+strSecValue+";DoD:"+strStepUp_DoD+"]"+"#########################");
			}
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: StepUp_DataValidation>
	//***********************************End of Step Up Data Provider*************************************************************		

	//***********************************Ledger Data Provider*************************************************************
	@Test(description = "Ledger",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_UpdateScript_DataVal_TestData",dataProviderClass = StepupUpdate_DataProvider.class)
	public void Ledger(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows, String useCaseNumFromSheet, int rowcounter , String strSheetName) throws Exception {
		Boolean stepupUpdateStatus;
		if(To_Execute.equalsIgnoreCase("Y")){

			//Generating key to fetch the status from Map for the match
			String statusKey = "["+To_Execute+";useCaseNumFromSheet:"+useCaseNumFromSheet+";Account:"+strAcct+"]";
			System.out.println("statusKey StepUp_DataValidation"+statusKey);

			//Retrieving status for generated key
			stepupUpdateStatus = stepUpdateStatusMapper.get(statusKey);


			//if the status is true then perform data validation for the search data 
			if(stepupUpdateStatus)
			{
				objStepUpScriptFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);		
			}
			else if(!stepupUpdateStatus)
			{
				reporter.reportStep(rowcounter, "Failed", "StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so data validation for Ledger view has been skipped. Please check it. !!", "", "StepUp_validation_Failed", driver, "", strSheetName, null);
				System.err.println("StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so data validation for Ledger view has been skipped. Please check it. !!");
				System.out.println("\n#########################[End  Row iteration:["+strViewPage+"] with test data:"+"[Account:"+strAcct+";SecVal:"+strSecValue+"]#########################");
			}

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of SearchResult_DataVal_Ledger()


	//***********************************Realized Data Provider*************************************************************	
	@Test(description = "Realized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_UpdateScript_DataVal_TestData",dataProviderClass = StepupUpdate_DataProvider.class)
	public void Realized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows, String useCaseNumFromSheet, int rowcounter , String strSheetName) throws Exception {
		Boolean stepupUpdateStatus;
		if(To_Execute.equalsIgnoreCase("Y")){

			//Generating key to fetch the status from Map for the match
			String statusKey = "["+To_Execute+";useCaseNumFromSheet:"+useCaseNumFromSheet+";Account:"+strAcct+"]";
			System.out.println("statusKey StepUp_DataValidation"+statusKey);

			//Retrieving status for generated key
			stepupUpdateStatus = stepUpdateStatusMapper.get(statusKey);


			//if the status is true then perform data validation for the search data 
			if(stepupUpdateStatus)
			{
				objStepUpScriptFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);		
			}
			else if(!stepupUpdateStatus)
			{
				reporter.reportStep(rowcounter, "Failed", "StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so data validation for Realized view has been skipped. Please check it. !!", "", "StepUp_validation_Failed", driver, "", strSheetName, null);
				System.err.println("StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so data validation for Realized view has been skipped. Please check it. !!");
				System.out.println("\n#########################[End  Row iteration:["+strViewPage+"] with test data:"+"[Account:"+strAcct+";SecVal:"+strSecValue+"]#########################");
			}
		}//End of IF condition to check To_Execute=Y/N
		else{
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");
		}//End of ELSE condition to check To_Execute=Y/N

	}//End of Realized()	


	//***********************************Unrealized Data Provider*************************************************************		
	@Test(description = "Unrealized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_UpdateScript_DataVal_TestData",dataProviderClass = StepupUpdate_DataProvider.class)
	public void Unrealized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows, String useCaseNumFromSheet, int rowcounter , String strSheetName) throws Exception {
		Boolean stepupUpdateStatus;
		if(To_Execute.equalsIgnoreCase("Y")){

			//Generating key to fetch the status from Map for the match
			String statusKey = "["+To_Execute+";useCaseNumFromSheet:"+useCaseNumFromSheet+";Account:"+strAcct+"]";
			System.out.println("statusKey Unrealized"+statusKey);

			//Retrieving status for generated key
			stepupUpdateStatus = stepUpdateStatusMapper.get(statusKey);


			//if the status is true then perform data validation for the search data 
			if(stepupUpdateStatus)
			{
				objStepUpScriptFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);		
			}
			else if(!stepupUpdateStatus)
			{
				reporter.reportStep(rowcounter, "Failed", "StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so data validation for Unrealized view has been skipped. Please check it. !!", "", "StepUp_validation_Failed", driver, "", strSheetName, null);
				System.err.println("StepUp has been already applied or there is an issue with updating stepup for the search data, "
						+ "so data validation for Unrealized view has been skipped. Please check it. !!");
				System.out.println("\n#########################[End  Row iteration:["+strViewPage+"] with test data:"+"[Account:"+strAcct+";SecVal:"+strSecValue+"]#########################");
			}
		}//End of IF condition to check To_Execute=Y/N
		else{
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");
		}//End of ELSE condition to check To_Execute=Y/N

	}//End of Unrealized()	

	//creating object and passing the driver 
	public void setobject_UpdateScriptFns(WebDriver driver) {
		objStepUpScriptFns=new StepupFns(driver);
	}

}//End of <Class: StepUpUpdate_Flow>
