package scripts.Maxit;

import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;

public class Dummy_InputData {
//	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
//	CommonUtils CommonUtils = PageFactory.initElements(driver, CommonUtils.class);

	@Test(enabled = true)
	public void updateMaxit_InputFile() throws InvalidFormatException{
		try {
			File[] arrFilesList;
			//String strExcelFilePath = "C:\\SVN_Automation_New\\Trunk\\Maxit_TestData\\Production_Site";
			String strExcelFilePath = "H:/Kavitha/Maxit/SmokeData/Test_Fn"; 
			String strFileName;
			//arrFilesList = this.getFilesList_fromDirectory(strExcelFilePath,"xlsx");
			arrFilesList = this.getFilesList(strExcelFilePath, ".xlsx");
			for (File file : arrFilesList) {
				System.out.println("file: " + file.getName().toString());
				strFileName = file.getName().toString();
				this.modifyExistingWorkbook(strExcelFilePath, strFileName);
			}
			
			
			
			System.out.println("end of file list");
			

             
        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }//End of Method
//		
//		
//	public List<String> getFilesList_fromDirectory(String strDirectory,String strExtensions) throws IOException{
//		List<String> arrFilePaths=null;
//		File dir = new File(strDirectory);
//		String[] extensions = new String[] { strExtensions };
//		System.out.println("Getting all files in " + dir.getPath()
//				+ " including those in subdirectories");
//		
//		
//		List<File> files = (List<File>) FileUtils.listFiles(dir, extensions, false); // of the arguement here is marked "true" it will return from subdirectories under given directory.
//		//List<File> files = (List<File>) FileUtils.listFiles(dir, extensions, true);
//		//File [] files = dir.listFiles();
//		if(files != null) {
//			System.out.println("count of xlsx files in given location is:"+files.size());
//			for (File file : files) {
//				arrFilePaths.add(file.getCanonicalPath());
//				//arrFilePaths.add(file.getName());
//				System.out.println("file: " + file.getCanonicalPath());
//			}
//		}
//		return arrFilePaths;
//		
//	}
//	
//
//	
	public File[] getFilesList(String strDirectory,String strExtension) throws IOException{
		//String strDirectory="H:/Kavitha/Maxit/SmokeData/Dummy";
		
		File dir = new File(strDirectory);
		System.out.println("Getting all files in " + dir.getPath());
		File[] files = dir.listFiles(new FilenameFilter(){
	    
		public boolean accept(File dir, String name) {
	        //return name.toLowerCase().endsWith(".xlsx");
			return name.toLowerCase().endsWith(strExtension);
	    }
		});

//		if(files != null) {
//			System.out.println("count of xlsx files in given location is:"+files.length);
//			for (File file : files) {
//				System.out.println("file: " + file.getName());
//			}
//		}
		return files;
	}

//@Test(enabled=false)
	private void modifyExistingWorkbook(String strExcelPath,String strExcelName) throws InvalidFormatException, IOException {
		String strExcelFileName = (strExcelPath+"/"+strExcelName).toString();
		System.out.println("strExcelFileName:"+strExcelFileName);
		// Obtain a workbook from the excel file
	    Workbook workbook = WorkbookFactory.create(new File(strExcelFileName));

	    // Get Sheet at index 0
	    Sheet sheet = workbook.getSheetAt(0);

	    // Get Row at index 0
	    Row row = sheet.getRow(0);
	    
	  //New Column = CAID
	    Cell cell = row.getCell(10);
	    if (cell == null)
	        cell = row.createCell(10);
	    cell.setCellType(CellType.STRING);
	    cell.setCellValue("CAID");

	    //New Column = AddNewCA
	    Cell cell_AddNewCA = row.getCell(11);
	    if (cell_AddNewCA == null)
	    	cell_AddNewCA = row.createCell(11);
	    cell_AddNewCA.setCellType(CellType.STRING);
	    cell_AddNewCA.setCellValue("AddNewCA");
	    
	    //CA Manager data: BlankSearch=N ; SecurityIDValue= Target Cusip[84763R101] ; AddNewCA=Y
	    Row row_CAManager = sheet.getRow(20);
	    Cell cell_BlankSearch = row_CAManager.getCell(3);
	    cell_BlankSearch.setCellType(CellType.STRING);
	    cell_BlankSearch.setCellValue("N");
	    
	    Cell cell_SecID = row_CAManager.getCell(6);
	    cell_SecID.setCellType(CellType.STRING);
	    cell_SecID.setCellValue("84763R101");
	    
	    Cell cell_NewCA = row_CAManager.getCell(11);
	    if (cell_NewCA == null)
	    	cell_NewCA = row_CAManager.createCell(11);
	    cell_NewCA.setCellType(CellType.STRING);
	    cell_NewCA.setCellValue("Y");
	    
	    // Write the output to the file
	    FileOutputStream fileOut = new FileOutputStream("H:/Kavitha/Maxit/SmokeData/Test_Fn/New_data/"+strExcelName);
	    workbook.write(fileOut);
	    fileOut.close();

	    // Closing the workbook
	    workbook.close();
	}

}
