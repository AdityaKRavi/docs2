package scripts.Maxit;

import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.SearchPageFns;
import functionalLibrary.Maxit.DataVal_DataProviders;

import org.testng.annotations.Test;

//import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.SkipException;


public class DataValidation_Flow extends BaseClass {
	WebDriver driver;
	boolean strRetStatus;
	SearchPageFns objSearchPageFns;
	Reporting reporter = new Reporting();	
	//Logger log = Logger.getLogger(DataValidation_Flow.class);	
	
	//***********************************Ledger Data Provider*************************************************************
	@Test(description = "Ledger",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_DataValTestData",dataProviderClass = DataVal_DataProviders.class)
	 public void Ledger(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows,int rowcounter , String strSheetName) throws Exception {
		
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSearchPageFns = new SearchPageFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSearchPageFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of SearchResult_DataVal_Ledger()

	
	//***********************************Realized Data Provider*************************************************************	
	@Test(description = "Realized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_DataValTestData",dataProviderClass = DataVal_DataProviders.class)
	 public void Realized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows,int rowcounter , String strSheetName) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();		
		objSearchPageFns = new SearchPageFns();
		
		if(To_Execute.equalsIgnoreCase("Y")){
			objSearchPageFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);			
		}//End of IF condition to check To_Execute=Y/N
		else{
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");
		}//End of ELSE condition to check To_Execute=Y/N
		        
	}//End of Realized()	
	
	
	//***********************************Unrealized Data Provider*************************************************************		
	@Test(description = "Unrealized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_DataValTestData",dataProviderClass = DataVal_DataProviders.class)
	 public void Unrealized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows,int rowcounter , String strSheetName) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();		
		objSearchPageFns = new SearchPageFns();
		
		if(To_Execute.equalsIgnoreCase("Y")){
			objSearchPageFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);			
		}//End of IF condition to check To_Execute=Y/N
		else{
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");
		}//End of ELSE condition to check To_Execute=Y/N
		        
	}//End of Unrealized()	
	

	//***********************************Open/Closed Data Provider*************************************************************		
	@Test(description = "Open/Closed",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_DataValTestData",dataProviderClass = DataVal_DataProviders.class)
	 public void OpenClosed(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows,int rowcounter , String strSheetName) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();		
		objSearchPageFns = new SearchPageFns();
		
		if(To_Execute.equalsIgnoreCase("Y")){
			objSearchPageFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);			
		}//End of IF condition to check To_Execute=Y/N
		else{
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");
		}//End of ELSE condition to check To_Execute=Y/N
		        
	}//End of OpenClosed()	
	
	//***********************************RawTrades Data Provider*************************************************************		
	@Test(description = "Raw Trades",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_DataValTestData",dataProviderClass = DataVal_DataProviders.class)
	 public void RawTrades(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows,int rowcounter , String strSheetName) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();		
		objSearchPageFns = new SearchPageFns();
		
		if(To_Execute.equalsIgnoreCase("Y")){
			objSearchPageFns.SearchResult_DataValidation(rowcounter,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);			
		}//End of IF condition to check To_Execute=Y/N
		else{
			reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+rowcounter+"] from DataValidation.");
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of RawTrades()	
	
	
}//End of <Class:DataValidation_Flow>
