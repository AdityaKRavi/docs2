package scripts.Maxit;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Period;
import org.testng.annotations.Test;

import functionalLibrary.Global.DateUtils;

public class ResearchTest {
  @Test
  public void f() throws ParseException {
	  Map<String, Boolean> Update_StatusMapper = new HashMap<String, Boolean>();
	  Update_StatusMapper.put("key1", true);
	  System.out.println("Value of mapper with key1:"+Update_StatusMapper.get("key1"));
	  Update_StatusMapper.put("key1", false);
	  Update_StatusMapper.put("key2", false);
	  System.out.println("Value of mapper with key1:"+Update_StatusMapper.get("key1"));
	  System.out.println("Value of mapper with key2:"+Update_StatusMapper.get("key2"));
	  
	  String strTest = "\u2260";
	  System.out.println("Decoded string"+StringEscapeUtils.unescapeJava(strTest));
	  
	  byte[] bytes = strTest.getBytes(StandardCharsets.UTF_8);
	  String text = new String(bytes, StandardCharsets.UTF_8);
	  System.out.println("Text:" +text);
	  
	  
	  String strRowNum = "Records 1 - 24 of 24";

		int strStartRowNumberTag = strRowNum.indexOf("-");
		int strEndRowNumberTag = strRowNum.indexOf("of");
		System.out.println("Index of - is"+strStartRowNumberTag);
		System.out.println("Index of OF is"+strEndRowNumberTag);
		int strStartRowNum = Integer.parseInt((strRowNum.substring(7, (strStartRowNumberTag))).trim());
		
		System.out.println("strStartRowNum is"+strStartRowNum);
		int strEndRowNum = Integer.parseInt((strRowNum.substring((strStartRowNumberTag+1), (strEndRowNumberTag))).trim());
		System.out.println("strEndRowNum is"+strEndRowNum);
//		strStartRowNum = CInt(Trim(Mid(strRowNum,8,(strStartRowNumberTag-8))))
//		strEndRowNum = CInt(Trim(Mid(strRowNum,(strStartRowNumberTag+1),(strEndRowNumberTag-strStartRowNumberTag-1))))
//	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  String strExpTitle = "(.*);(Mutual Fund|Stocks, Options, Bonds...|ETF RIC)";
	  String strActTitle = ";;First In - First Out;Mutual Fund";
	  
	  
	  
	  
	  System.out.println("Macthes with regexp:"+strActTitle.matches(strExpTitle));
	  
	  System.out.println("Macthes with regexp:"+strActTitle.matches("Tax Strategy - Account #(.*)"));
	  
	  System.out.print("FI multiple regex Macthes with regexp:");
	  System.out.println("Fixed Income Elections - Account #: 1131408 Security #: 161716".matches("Fixed Income Elections - Account #: (.*) Security #: (.*)"));
	  
	  System.out.print("FI multiple regex Macthes with negatove scenario:");
	  System.out.println("Fixed Income Elections - Account #: 1131408 Security #: 161716".matches("Fixed Income Elections - Account #: (.*) Security #:"));
	  //Fixed Income Elections - Account #: 1131408 Security #: 161716
	  
	  System.out.println("Fixed Income Elections - Account #: 1131408 Security #: 161716".matches("Fixed Income Elections - Account #: 1131408 Security #: 161716"));
	  
	  String[] strActAr = {"Transfers;01/06/2014;0.0000;$0.00;200.0000;$5,484.00;$5,484.00;-G/L not applicable to Transfer Events ;Covered;Long; ","Transfers;01/06/2014;0.0000;$0.00;200.0000;$5,484.00;$5,484.00;-G/L not applicable to Transfer Events ;Covered;Long;"};
	  System.out.println("arr[0] is:"+strActAr[0]);
	  String stractVal0 = strActAr[0].toString();
	  System.out.println("arr[0].trim is:"+strActAr[0].trim());
	  System.out.println("arr[0].toString() is:"+strActAr[0].toString());
	  System.out.println("arr[0].toString().trim() is:"+strActAr[0].toString().trim());
	  System.out.println("stractVal0 is:"+stractVal0);
	  System.out.println("stractVal0.trim() is:"+stractVal0.trim());
	  
	  
	  
	  
	  String strTempp = "-120.00";
	  System.out.println("SubString of string:" + strTempp.substring(0, 1));
	  
	  
	  
	  DateUtils dtUtils = new DateUtils();
	  Date dtRetdate = dtUtils.convertString_to_Date("7/12/2017");
	  System.out.println(dtRetdate.getDate());
	  
	  System.out.println("Utils Date :"+dtRetdate);
	  
	  Date dtRetdate1 = dtUtils.convertString_to_Date("12/07/2017");
	  System.out.println("Utils Date :"+dtRetdate1);
	  
	  
	  
	  
	  String strOpenDate = "07/12/2017";
	  DateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy"); 
	  Date startDate = dateformat.parse(strOpenDate);
	  
	  
	  System.out.println("strOpenDate:"+startDate);
	  String strOpenDate_New = dateformat.format(startDate);
	  System.out.println("Date in format MM/dd/yyyy: " + strOpenDate_New);
	  
	  
	  Date Current_date = new Date();
	  String strCurrentDate= new SimpleDateFormat("MM/dd/yyyy").format(Current_date);
	  System.out.println("Current Date in format MM/dd/yyyy: " + strCurrentDate);
	  
	  LocalDate ldt_startDate= new LocalDate(startDate);
	  LocalDate ldt_Current_date= new LocalDate(Current_date);
	  
	  System.out.println(ldt_startDate.getDayOfMonth());
	  System.out.println(ldt_startDate.getMonthOfYear());
	  System.out.println(ldt_startDate.getYear());
	  
	  //07/12/2017 
	  Months m = Months.monthsBetween(ldt_startDate, ldt_Current_date);
	  int monthDif = Math.abs(m.getMonths());//this return 1
	  System.out.println("Date Diff in months:" +monthDif);
	  
	  Days d = Days.daysBetween(ldt_startDate, ldt_Current_date);
	  int daysDiff = Math.abs(d.getDays());
	  
	  System.out.println("Date Diff in Days:" +daysDiff);
	  //System.out.println("Date differnce is: " + startDate-Current_date );

	  
	  Period per = new Period(ldt_startDate,ldt_Current_date);
	  System.out.print(per.getYears() + " years, ");
	  System.out.print(per.getMonths() + " months, ");
	  System.out.print(per.getDays() + " months, ");
	  
	  
	  String strMarketValue = "-8,001.44";
	  String strExpCost = "-1,01.44";
	  String strMvalSign = "+";
	  String strCostSign = "+";
	  
	  String strMValSign = strMarketValue.substring(0, 1);
	  String strExpCostSign = strExpCost.substring(0, 1);
	  System.out.println("strMValSign:"+strMValSign);
	  System.out.println("strExpCostSign:"+strExpCostSign);
	  
		if(strMarketValue.substring(0, 1).equals("-")) strMvalSign = "-"; // Check Market Value Sign
		if(strExpCost.substring(0, 1).equals("-")) strCostSign = "-";// Check Cost Sign		
		  System.out.println("strMvalSign:"+strMvalSign);
		  System.out.println("strCostSign:"+strCostSign);
		
	  
	  
	  double testTry = 123.408;
		DecimalFormat df = new DecimalFormat("####0.00");
		String strTempTry = df.format(testTry);
		System.out.println("Value after format:"+strTempTry);
	  
	  
	  
		//String strExpCost = "-$1000.00";
		String strMrtVal = "-$2000.00";
		String strNegSign = "-";
		//strExpCost = strExpCost.replace("-", ""); 
		strExpCost = strExpCost.replace("$", "");
		
		strMrtVal = strMrtVal.replace("-", ""); 
		strMrtVal = strMrtVal.replace("$", "");
		
		double dblExpCost = Double.parseDouble(strExpCost)	;
		double dblMrtVal = Double.parseDouble(strMrtVal)	;
		System.out.println("Value of dblExpCost is : "+dblExpCost);
		System.out.println("Value of dblMrtVal is : "+dblMrtVal);
  
		double dblExpectedGainLoss = Math.abs(dblMrtVal - dblExpCost);
		//DecimalFormat df = new DecimalFormat("####0.00");
		String strExpectedGainLoss = df.format(dblExpectedGainLoss);		
		
		
		String dblExpGL = strNegSign+"$"+strExpectedGainLoss;
		System.out.println("<UGL_CalGL_NonDual>: dblExpGL = "+dblExpGL);
  
  
		String strActualCost ="DualHigh Basis:$8,000.00Low Basis:$7,000.00";
		strActualCost = strActualCost.replace("$", "");
		strActualCost = strActualCost.replace(",", "");
		String[] arrTemp = strActualCost.split(":");

		//strDonorBasis = 8000.00Low Basis
		System.out.println("arrTemp[1] is"+arrTemp[1]);
		System.out.println("arrTemp[1] length is"+arrTemp[1].length());
		System.out.println("arrTemp[1].indexOf(Low)  is"+arrTemp[1].indexOf("Low"));
		
		int endIndex = (arrTemp[1].indexOf("Low")-1);
		System.out.println("endIndex is"+endIndex);
		
		String strDonorBasis = arrTemp[1].substring(0, endIndex);
		strDonorBasis = strDonorBasis.replace(",", "");
		System.out.println("String strDonorBasis is"+strDonorBasis);
		
		double dblDonorBasis = Double.parseDouble(strDonorBasis);
		System.out.println("strDonorBasis is"+dblDonorBasis);
		double dblFMV = Double.parseDouble(arrTemp[2]);
		System.out.println("dblFMV is"+dblFMV);
		
		
  }
  
  
  
}
