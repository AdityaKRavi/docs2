package scripts.Maxit;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.Login_Maxit;
import functionalLibrary.Maxit.ConfigInputFile;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;


public class BaseClass {
	SoftAssert softAssert = new SoftAssert();
	WebDriver driver;
	boolean strRetStatus;
	Reporting reporter = new Reporting();
	ConfigInputFile objConfigInputFile = new ConfigInputFile();
	DataTable dt = new DataTable();
	CommonUtils utils = new CommonUtils();

	//******************************************** @BeforeTest*****************************************************************
	// Browser Initialization
	//Global Objects Hash map for thread-ITestContext
	//Manage Driver hashmap for thread-browser driver
	//Login to application
	//Create Results file based on parameters
	//******************************************** @BeforeTest*****************************************************************
	@Parameters({"browser", "clientName", "environment" , "scriptName","productName","resultsBasePath"})
	@BeforeTest(description = "BaseClass: Login_Method")
	public void BaseMethod(String strBrowser, String strClient, String strEnvi , String strscriptName, String strProdName , String strResPath , ITestContext context ){
		try{
			
			// AUTO-420 : Write code logic to set testDataPath & browserDriverPath based on machine name(ex: qa-auto7 has C:\SVN_Automation instead of C:\SVN_Automation_New)
			String autoMachineName = utils.getComputerName();
			System.out.println("autoMachineName:"+autoMachineName);
			if(autoMachineName.equalsIgnoreCase("qa-auto7")){
				System.setProperty("testDataPath", "C:/SVN_Automation/Trunk/Maxit_TestData");
				System.setProperty("browserDriverPath", "C:/SVN_Automation/Trunk/Selenium_Automation/Selenium_Auto_Project/BrowserDrivers");
			}
			else{
				System.setProperty("testDataPath", "C:/SVN_Automation_New/Trunk/Maxit_TestData");
				System.setProperty("browserDriverPath", "C:/SVN_Automation_New/Trunk/Selenium_Automation/Selenium_Auto_Project/BrowserDrivers");
			}
			System.setProperty("resBasePath", strResPath);
			
			CommonUtils.startTestCaseLog("BaseClass->BaseMethod for Login",context);
			Login_Maxit objLogin = new Login_Maxit(strBrowser); //Create an object for Class "Login"}	
			GlobalObjectsFactory.getGlobalObjects().setiTestContext(context);
			strRetStatus = objLogin.fnLogin(strEnvi,strBrowser, strClient,context);						
			Assert.assertTrue(strRetStatus,"Login Failed, Please Check..!!");
			driver = ManageDriver_Maxit.getManageDriver().getDriver();
			String strCheck = dt.getInputFIlePath_Maxit(strscriptName, strEnvi, strClient);
			Assert.assertNotNull(strCheck,"Input File config failed, please check..!!");
	        String resultFilePath = reporter.reportCreateResultFile(strscriptName, strEnvi, strClient, strProdName);
	        context.setAttribute("resultPath", resultFilePath);
	        System.out.println("Result path in @beforetest -  " + resultFilePath);
			strRetStatus = true;	
		}
		catch(Exception e){
			System.out.println("Exception in BaseClass->BaseMethod in @BeforeTest method..!!");
			e.printStackTrace();
		}
	}//End of "BaseMethod" method
	
	//********************************************End of @BeforeTest*****************************************************************************
	

	
	
	//******************************************** @Test to check Pre-requisites*****************************************************************	
	// Verifies if the Login is successful or not
	//******************************************** Pre-requisites********************************************************************************
	@Test(description = "PreReq_CheckPoint Method- to Verify Login/Input data config Success status",enabled = true)
	public void PreReq_CheckPoint(){
		try{
			if(strRetStatus){
			softAssert.assertTrue(strRetStatus, "@TEST: PreReq_CheckPoint, dependent on @BeforeTest annotation.");				
			}
			else{
				throw new SkipException("Fail exception in PreReq_CheckPoint");
			}
		}//End of Try block
		catch(Exception e){
			System.out.println("@TEST:PreReq_CheckPoint[Failed], Login/Input file config is not done.");
		}//End of Catch block
	}//End of PreReq_CheckPoint
	
	//********************************************End of Pre-requisites check*********************************************************************
	
	
	
	//******************************************** @AfterTest*************************************************************************************
	//To log off out of application and quit browser instance.
	//******************************************** @AfterTest*************************************************************************************
	@AfterTest
		public void afterTest() {
		  try {
			  CommonUtils.endTestCaseLog("BaseClass->AfterTest Method");  
			  //ManageDriver_Maxit.getManageDriver().quit();
		  } catch (Exception e) {
			  System.out.println("Exception in BaseClass->afterTest in @AfterTest method..!!");
			  e.printStackTrace();
			}//End of Catch block
	  }//End of afterTest Annotation method.
	//******************************************** End of @AfterTest*******************************************************************************
}
