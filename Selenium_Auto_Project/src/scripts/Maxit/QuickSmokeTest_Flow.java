package scripts.Maxit;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageObjects.Maxit.DMI_Objs;
import PageObjects.Maxit.SearchPage_POM;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.ConfigInputFile;
import functionalLibrary.Maxit.Login_Maxit;

public class QuickSmokeTest_Flow {
	WebDriver driver;
	Reporting reporter = new Reporting();
	ConfigInputFile objConfigInputFile = new ConfigInputFile();
	DataTable dt = new DataTable();
	ManageDriver_Maxit manageDriver = ManageDriver_Maxit.getManageDriver();
	CommonUtils utils = new CommonUtils();
	
	String resultFilePath;
	
	@Parameters({"productName","environment","scriptName","resBasePath"})
	@BeforeTest(description = "BaseClass: Create Report")
	public void BaseMethod(String strProdName , String strEnvironment , String strscriptName, String strResPath,ITestContext context ){
		try{
			System.out.println("xml parameter:" +strscriptName);
			
			String autoMachineName = utils.getComputerName();
			System.out.println("autoMachineName:"+autoMachineName);
			if(autoMachineName.equalsIgnoreCase("qa-auto7")){
				System.setProperty("testDataPath", "C:/SVN_Automation/Trunk/Maxit_TestData");
				System.setProperty("browserDriverPath", "C:/SVN_Automation/Trunk/Selenium_Automation/Selenium_Auto_Project/BrowserDrivers");
			}
			else{
				System.setProperty("testDataPath", "C:/SVN_Automation_New/Trunk/Maxit_TestData");
				System.setProperty("browserDriverPath", "C:/SVN_Automation_New/Trunk/Selenium_Automation/Selenium_Auto_Project/BrowserDrivers");
			}
			System.setProperty("resBasePath", strResPath);
			
	        resultFilePath = reporter.reportCreateResultFile_Maxit_QuickSMoke(strscriptName, strProdName ,strEnvironment );

	        System.out.println("Result path in @beforetest -  " + resultFilePath);
	
		}
		catch(Exception e){
			System.out.println("Exception in BaseClass->BaseMethod in @BeforeTest method..!!");
			e.printStackTrace();
		}
	}//End of "BaseMethod" method
	
	@Test(description = "QuickSmokeTest",dataProvider = "Read_QuickSmoke_Data")
	public void QuickSmokeTest(String ToExecute, String strClient,String strSite,String strUrl,String strUserName,String strPassword,String strLoginType,String strClientType,String strBrowser,String strProxy,int rowcounter,ITestContext context){
		//String strSheetName = "Login";
		boolean strRetStatus;
		String strEnv = context.getCurrentXmlTest().getParameter("environment");
		String strSheetName = strEnv;
		//context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		try{
			//Configure Input data file
			//Create a file for reporting
			//read which client to run To_Execute, browser and credentials
			//login
			//verify DMI
			//close browser
			
			GlobalObjectsFactory.getGlobalObjects().setiTestContext(context);
			context.setAttribute("resultPath", resultFilePath);
			
			//String resultFilePath = reporter.reportCreateResultFile("QuickSmokeTest_Flow", "Maxit");
			//context.setAttribute("resultPath", resultFilePath);
			if(ToExecute.equalsIgnoreCase("Y")){
							
				Login_Maxit objLogin = new Login_Maxit(strBrowser); //Create an object for Class "Login"}	
				System.out.println("iTestContext get productName "+context.getCurrentXmlTest().getParameter("productName"));	
				System.out.println("To_Execute is Y for client:"+strClient+"in row#"+rowcounter);
				strRetStatus = objLogin.fnLogin_QuickSmoke(strUrl,strEnv,strClient,strBrowser,strLoginType,strUserName,strPassword,strProxy,rowcounter,context);
				driver = ManageDriver_Maxit.getManageDriver().getDriver();
				if(strRetStatus) {
					System.out.println("Login successful for row:"+rowcounter);
					//Verify Search Page Account txt box existence:
					this.SearchPage_Verification(rowcounter,strEnv);
					
					// Verify DMI
					this.DMI_QuickSmoke_Flow(rowcounter,strEnv);
					Thread.sleep(1000);
				}
				else{
					System.out.println("Login NOT succesfull for row:"+rowcounter);
					reporter.reportStep(rowcounter, "Failed", "Login to client is not Successful, please check!", "", "LoginFail", driver, "", strSheetName, null);
				}
					manageDriver.quit();
			}//End of IF condition to check To_Execute=Y/N
			else{	
				reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this client:"+strClient, "", "Skip Row", driver, "", strSheetName, null);
			}//End of ELSE condition to check To_Execute=Y/N
			

		}//End of Try block

		catch(Exception e){
			System.out.println("Exception in BaseClass->BaseMethod in @BeforeTest method..!!");
			e.printStackTrace();
		}
	
	}//End of QuickSmokeTest Method
	
	@Test(description = "QuickSmokeTest_WithoutDMI",dataProvider = "Read_QuickSmoke_Data")
	public void QuickSmokeTest_WithoutDMI(String ToExecute, String strClient,String strSite,String strUrl,String strUserName,String strPassword,String strLoginType,String strClientType,String strBrowser,String strProxy,int rowcounter,ITestContext context){
		//String strSheetName = "Login";
		boolean strRetStatus;
		String strEnv = context.getCurrentXmlTest().getParameter("environment");
		String strSheetName = strEnv;
		//context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		try{
			//Configure Input data file
			//Create a file for reporting
			//read which client to run To_Execute, browser and credentials
			//login
			//close browser
			
			GlobalObjectsFactory.getGlobalObjects().setiTestContext(context);
			context.setAttribute("resultPath", resultFilePath);
			if(ToExecute.equalsIgnoreCase("Y")){
							
				Login_Maxit objLogin = new Login_Maxit(strBrowser); //Create an object for Class "Login"}	
				System.out.println("iTestContext get productName "+context.getCurrentXmlTest().getParameter("productName"));	
				System.out.println("To_Execute is Y for client:"+strClient+"in row#"+rowcounter);
				strRetStatus = objLogin.fnLogin_QuickSmoke(strUrl,strEnv,strClient,strBrowser,strLoginType,strUserName,strPassword,strProxy,rowcounter,context);
				driver = ManageDriver_Maxit.getManageDriver().getDriver();
				if(strRetStatus) {
					System.out.println("Login successful for row:"+rowcounter);
					//Verify Search Page Account txt box existence:
					this.SearchPage_Verification(rowcounter,strEnv);
					
//					// Verify DMI
//					this.DMI_QuickSmoke_Flow(rowcounter,strEnv);
//					Thread.sleep(1000);
				}
				else{
					System.out.println("Login NOT succesfull for row:"+rowcounter);
					reporter.reportStep(rowcounter, "Failed", "Login to client is not Successful, please check!", "", "LoginFail", driver, "", strSheetName, null);
				}
					manageDriver.quit();
			}//End of IF condition to check To_Execute=Y/N
			else{	
				reporter.reportStep(rowcounter, "Not Executed", "To_Execute = N , so Skipping this client:"+strClient, "", "Skip Row", driver, "", strSheetName, null);
			}//End of ELSE condition to check To_Execute=Y/N
			

		}//End of Try block

		catch(Exception e){
			System.out.println("Exception in BaseClass->BaseMethod in @BeforeTest method..!!");
			e.printStackTrace();
		}
	
	}//End of QuickSmokeTest_WithoutDMI Method

//###################################################################################################################################################################  
//Function name		: Read_QuickSmoke_Data()
//Class name		: configInputFile
//Description 		: Data Provider to read Quick Smoke test data
//Parameters 		: None
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	@DataProvider
	public Object[][] Read_QuickSmoke_Data(ITestContext context) throws Exception{
		Object[][] dpArray = null;
		String strEnv_Sheet = context.getCurrentXmlTest().getParameter("environment");
		String strSheetName = strEnv_Sheet;	
		//dt.setExcelFile("C:\\SVN_Automation_New\\Trunk\\Maxit_TestData\\Ops\\Quick_SmokeTest.xlsx");
		dt.setExcelFile(System.getProperty("testDataPath") + "/Maxit_Login/Sites.xlsx");
        if(dt.setSheet(strSheetName)){	
        	
        	int intDTRows = dt.getRowCount();
        	dpArray = new Object[intDTRows][11];
        	
        	for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

        		  String To_Execute = dt.getCellData(intDTRowIndex, "ToExecute").toString() ;
        		  if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
        			  return(dpArray);
        		  }
        		  
        		  String strToExecute = dt.getCellData(intDTRowIndex, "ToExecute").toString() ;
        		  String strClient = dt.getCellData(intDTRowIndex, "Client").toString() ;
        		  String strSite = dt.getCellData(intDTRowIndex, "Site").toString() ;
        		  String strUrl = dt.getCellData(intDTRowIndex, "URL").toString() ;
				  String strUserName = dt.getCellData(intDTRowIndex, "UserName").toString() ;
				  String strPassword = dt.getCellData(intDTRowIndex, "Password").toString() ;
				  String strLoginType = dt.getCellData(intDTRowIndex, "LoginType").toString() ;
				  String strClientType = dt.getCellData(intDTRowIndex, "ClientTYpe").toString() ;
				  String strBrowserType = dt.getCellData(intDTRowIndex, "BrowserType").toString() ;
				  String strProxy = dt.getCellData(intDTRowIndex, "SetProxy").toString() ;

				  //Assigning into array records
				  
				  dpArray[intDTRowIndex-1][0]=strToExecute;
				  dpArray[intDTRowIndex-1][1]=strClient;
				  dpArray[intDTRowIndex-1][2]=strSite;
				  dpArray[intDTRowIndex-1][3]=strUrl;
				  dpArray[intDTRowIndex-1][4]=strUserName;
				  dpArray[intDTRowIndex-1][5]=strPassword;
				  dpArray[intDTRowIndex-1][6]=strLoginType;
				  dpArray[intDTRowIndex-1][7]=strClientType;
				  dpArray[intDTRowIndex-1][8]=strBrowserType;
				  dpArray[intDTRowIndex-1][9]=strProxy;
				  dpArray[intDTRowIndex-1][10]=intDTRowIndex;
			  }//End of for loop to iterate thru different rows	
	  }//End of IF condition to check  DataTable.setSheet

		return(dpArray);
	}//End of <Method: Read_QuickSmoke_Data>
		
		
	//###################################################################################################################################################################  
	//Function name		: DMI_QuickSmoke_Flow(int exclRowCnt) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying DMI Page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public  void DMI_QuickSmoke_Flow(int exclRowCnt,String strSheetName) throws Exception{
			DMI_Objs DMI_Objs = PageFactory.initElements(driver, DMI_Objs.class);
			//String strSheetName = "Login";
			ArrayList<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
			boolean strRet;
			try{		
				System.out.println("\n#########################[DMI]Quick Smoke Test: Executing Row iteration:["+exclRowCnt+"] #########################");
				strRet = DMI_Objs.VerifyandClick_DMILink(exclRowCnt, strSheetName);
				if(strRet){
					System.out.println("DMI page Navigation Passed");
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] DMI page Navigation passed.", "", "DMI_Navigate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					String strWindowTitle = driver.getTitle();
					if(strWindowTitle.toLowerCase().contains("dmi") || strWindowTitle.toLowerCase().contains("data management interface")|| strWindowTitle.toLowerCase().contains("maxit: broker dashboard")){
						Thread.sleep(3000);
						driver.close();
					}
					driver.switchTo().window(browserTabs.get(0));

				}//End of IF condition
				else{
					System.out.println("DMI page Navigation failed, please check..!!");
					reporter.reportStep(exclRowCnt, "Failed", "DMI page Navigation failed, please check..!!", "", "DMI_Navigate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of Else condition
	
				System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
				return;
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Smoke Test Flow <Method: DMI_QuickSmoke_Flow>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in DMI_QuickSmoke_Flow ", "", "DMI_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of Catch block
		}//End of >Method: DMI_QuickSmoke_Flow>	

	//###################################################################################################################################################################  
	//Function name		: SearchPage_Verification(int exclRowCnt,String strSheetName) throws Exception{
	//Class name		: QuickSmokeTest_Flow
	//Description 		: Function to verify if Search page is not blank, by checking "Account" filed exists.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public void SearchPage_Verification(int exclRowCnt,String strSheetName) throws Exception{
			SearchPage_POM Search_Objs = PageFactory.initElements(driver, SearchPage_POM.class);
			try{	
				if(!(CommonUtils.isElementPresent(Search_Objs.txtbx_Account))){
					System.out.println("Search Page is blank - doesn't display 'Account' field, please check");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search Page is blank - doesn't display 'Account' field, please check..!!", "", "SearchPage",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}
				else{
					System.out.println("Search Page is displayed succesfully.");
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Search Page is displayed succesfully.", "", "SearchPage",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}

			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Quick Smoke Test Flow <Method: SearchPage_Verification>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in SearchPage_Verification ", "", "SearchPage",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of Catch block
		}//End of >Method: SearchPage_Verification>			
		
		


}//End of <Class: QuickSmokeTest_Flow>
