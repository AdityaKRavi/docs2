package scripts.Maxit;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.Test;

import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.DataVal_DataProviders;
import functionalLibrary.Maxit.LRM_DataProvider;
import functionalLibrary.Maxit.LRM_Fns;
import functionalLibrary.Maxit.SearchPageFns;
import functionalLibrary.Maxit.StepupUpdate_DataProvider;


public class LRM_TxnLevel_Flow extends BaseClass {
	
	WebDriver driver ;
	boolean strRetStatus;
	LRM_Fns objLRMFns;
	Reporting reporter = new Reporting();	
	//SearchPageFns objSearchPageFns  = PageFactory.initElements(driver, SearchPageFns.class);
	
	Map<String, Boolean> statusMapper = new HashMap<String, Boolean>();
	boolean blnRetStatus;
	
	//***********************************TxnLRM_Update Data Provider*************************************************************
	@Test(description = "Txn_LRMUpdate",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_TxnLRM_Update",dataProviderClass = LRM_DataProvider.class)
	 public void Txn_LRMUpdate(String To_Execute,String strAcct,String strSecType, String strSecValue,String strLRMLevel , String strExisting_LRM, String strNew_LRM,String strTxn_AcctType,String strTxn_ClosingDate,String strTestCase_Name,String useCaseNum,int exclRowCnt,String strSheetName,String strClient ) throws Exception {
		
		/*creating object and setting it globally for the class & passing driver object to prevent null pointer*/
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		setobject_UpdateScriptFns(driver);
		String mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+useCaseNum+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
		
		if(To_Execute.equalsIgnoreCase("Y")){
			System.out.println("\n#########################[Txn_LRMUpdate]Executing Row iteration:["+exclRowCnt+"] with test data:"+mapperKey+"#########################");
			blnRetStatus = objLRMFns.TxnLRM_Update_Flow(useCaseNum, strAcct, strSecType, strSecValue, strLRMLevel, strExisting_LRM, strNew_LRM, strTxn_AcctType, exclRowCnt, strSheetName, strClient);
			statusMapper.put(mapperKey, blnRetStatus);
			System.out.println("status Mapper size: "+statusMapper.size());
			System.out.println("###########################################################[End of LRM_Update_Flow Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of IF condition to check To_Execute=Y/N
		
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "Txn_LrmUpdate", null);
			System.out.println("SkipException: To_Execute=N, so skipping the row["+exclRowCnt+"] from 'LRM_update'.");
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from 'LRM_update'.");			
		}//End of ELSE condition to check To_Execute=Y/N	
		
	}//End of <TxnLRM_Update>
	//***********************************End of TxnLRM_Update Data Provider*************************************************************
	
	//TxnLRM_VerifyUpdate
	//***********************************TxnLRM_Update Data Provider*************************************************************
	@Test(description = "TxnLRM_VerifyUpdate",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_TxnLRM_Update",dataProviderClass = LRM_DataProvider.class)
	 public void TxnLRM_VerifyUpdate(String To_Execute,String strAcct,String strSecType, String strSecValue,String strLRMLevel , String strExisting_LRM, String strNew_LRM,String strTxn_AcctType,String strTxn_ClosingDate,String strTestCase_Name,String useCaseNum,int exclRowCnt,String strSheetName,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		setobject_UpdateScriptFns(driver);
		String mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+useCaseNum+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
		
		if(To_Execute.equalsIgnoreCase("Y")){
			System.out.println("\n#########################[TxnLRM_VerifyUpdate]Executing Row iteration:["+exclRowCnt+"] with test data:"+mapperKey+"#########################");
			if(statusMapper.get(mapperKey).equals(true)){
				blnRetStatus = objLRMFns.TxnLRM_VerifyUpdate_Flow(useCaseNum, strAcct, strSecType, strSecValue, strLRMLevel, strExisting_LRM, strNew_LRM, strTxn_AcctType, exclRowCnt, strSheetName, strClient);
				//statusMapper.put(mapperKey, blnRetStatus);
				System.out.println("status Mapper size within TxnLRM_VerifyUpdate: "+statusMapper.size());
			}//End of IF condition to check statusMapper
			else{
				System.out.println("SkipException: LRM Update=failed, so skipping the row["+exclRowCnt+"] from 'LRM_VerifyUpdate DataValidation'.");
				throw new SkipException("LRM Update=failed, so skipping the row["+exclRowCnt+"] from 'LRM_VerifyUpdate DataValidation'.");	
			}
			System.out.println("###########################################################[End of TxnLRM_VerifyUpdate Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of IF condition to check To_Execute=Y/N
		
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "Txn_LrmUpdate", null);
			System.out.println("SkipException: To_Execute=N, so skipping the row["+exclRowCnt+"] from 'TxnLRM_VerifyUpdate'.");
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from 'TxnLRM_VerifyUpdate'.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	
	}//End of <TxnLRM_VerifyUpdate>
	//***********************************End of TxnLRM_Update Data Provider*************************************************************
	
	//TxnLRM_RawTrades
	//***********************************RawTrades Data Provider*************************************************************
	@Test(description = "RawTrades",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_LRM_DataValidation",dataProviderClass = LRM_DataProvider.class)
	public void RawTrades(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows, String useCaseNum, int exclRowCnt , String strSheetName, String strClient) throws Exception {
		Boolean lrmUpdateStatus;
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		SearchPageFns objSearchPageFns  = PageFactory.initElements(driver, SearchPageFns.class);
		
		if(To_Execute.equalsIgnoreCase("Y")){
			//Generating key to fetch the status from Map for the match
			String mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+useCaseNum+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
			System.out.println("LRM_RawTrades Mapper key"+mapperKey);
			System.out.println("\n#########################[RawTrades]Executing Row iteration:["+exclRowCnt+"] with test data:"+mapperKey+"#########################");
			lrmUpdateStatus = statusMapper.get(mapperKey);

			if(lrmUpdateStatus==null){
				reporter.reportStep(exclRowCnt, "Not Executed", "This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation. ", "", "Skip_DataVal", driver, "", strSheetName, null);
				System.err.println("This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation.");		
			}//End of IF condition where lrmUpdateStatus==null in cases where "To_Execute=N" in LRM_Update sheets
			
			//if the status is true then perform data validation for the search data 
			if(lrmUpdateStatus)
				objSearchPageFns.SearchResult_DataValidation(exclRowCnt,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);		
			else if(!lrmUpdateStatus)
				reporter.reportStep(exclRowCnt, "Failed", "Txn Level LRM Update already applied or there is an issue with LRM update, so data validation for Raw Trades view has been skipped. Please check it. !!", "", "Skip_DataVal", driver, "", strSheetName, null);

			System.out.println("###########################################################[End of Ledger Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of IF condition to check To_Execute=Y/N
		
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of RawTrades()
	
	//***********************************End of RawTrades Data Provider*************************************************************
	
	
	//TxnLRM_Ledger
	//***********************************Ledger Data Provider*************************************************************
	@Test(description = "Ledger",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_LRM_DataValidation",dataProviderClass = LRM_DataProvider.class)
	public void Ledger(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows, String useCaseNum, int exclRowCnt , String strSheetName, String strClient) throws Exception {
		Boolean lrmUpdateStatus;
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		SearchPageFns objSearchPageFns  = PageFactory.initElements(driver, SearchPageFns.class);
		
		if(To_Execute.equalsIgnoreCase("Y")){
			//Generating key to fetch the status from Map for the match
			String mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+useCaseNum+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
			System.out.println("LRM_Ledger Mapper key"+mapperKey);
			System.out.println("\n#########################[Ledger]Executing Row iteration:["+exclRowCnt+"] with test data:"+mapperKey+"#########################");
			lrmUpdateStatus = statusMapper.get(mapperKey);

			if(lrmUpdateStatus==null){
				reporter.reportStep(exclRowCnt, "Not Executed", "This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation. ", "", "Skip_DataVal", driver, "", strSheetName, null);
				System.err.println("This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation.");		
			}//End of IF condition where lrmUpdateStatus==null in cases where "To_Execute=N" in LRM_Update sheets
			
			//if the status is true then perform data validation for the search data 
			if(lrmUpdateStatus)
				objSearchPageFns.SearchResult_DataValidation(exclRowCnt,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);		
			else if(!lrmUpdateStatus)
				reporter.reportStep(exclRowCnt, "Failed", "Txn Level LRM Update already applied or there is an issue with LRM update, so data validation for Ledger view has been skipped. Please check it. !!", "", "Skip_DataVal", driver, "", strSheetName, null);

			System.out.println("###########################################################[End of Ledger Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of IF condition to check To_Execute=Y/N
		
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of Ledger()
	
	//***********************************End of Ledger Data Provider*************************************************************
	
	//TxnLRM_UGL
	//***********************************Ledger Data Provider*************************************************************
	@Test(description = "Unrealized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_LRM_DataValidation",dataProviderClass = LRM_DataProvider.class)
	public void Unrealized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows, String useCaseNum, int exclRowCnt , String strSheetName, String strClient) throws Exception {
		Boolean lrmUpdateStatus;
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		SearchPageFns objSearchPageFns  = PageFactory.initElements(driver, SearchPageFns.class);
		
		if(To_Execute.equalsIgnoreCase("Y")){
			//Generating key to fetch the status from Map for the match
			String mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+useCaseNum+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
			System.out.println("LRM_Unrealized Mapper key"+mapperKey);
			System.out.println("\n#########################[Unrealized]Executing Row iteration:["+exclRowCnt+"] with test data:"+mapperKey+"#########################");
			lrmUpdateStatus = statusMapper.get(mapperKey);

			if(lrmUpdateStatus==null){
				reporter.reportStep(exclRowCnt, "Not Executed", "This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation. ", "", "Skip_DataVal", driver, "", strSheetName, null);
				System.err.println("This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation.");		
			}//End of IF condition where lrmUpdateStatus==null in cases where "To_Execute=N" in LRM_Update sheets
			
			//if the status is true then perform data validation for the search data 
			if(lrmUpdateStatus)
				objSearchPageFns.SearchResult_DataValidation(exclRowCnt,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);		
			else if(!lrmUpdateStatus)
				reporter.reportStep(exclRowCnt, "Failed", "Txn Level LRM Update already applied or there is an issue with LRM update, so data validation for Unrealized view has been skipped. Please check it. !!", "", "Skip_DataVal", driver, "", strSheetName, null);

			System.out.println("###########################################################[End of Ledger Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of IF condition to check To_Execute=Y/N
		
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of Unrealized()
	
	//***********************************End of Unrealized Data Provider*************************************************************
	
	
	//TxnLRM_RGL
	//***********************************Ledger Data Provider*************************************************************
	@Test(description = "Realized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_LRM_DataValidation",dataProviderClass = LRM_DataProvider.class)
	public void Realized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows, String useCaseNum, int exclRowCnt , String strSheetName, String strClient) throws Exception {
		Boolean lrmUpdateStatus;
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		SearchPageFns objSearchPageFns  = PageFactory.initElements(driver, SearchPageFns.class);
		
		if(To_Execute.equalsIgnoreCase("Y")){
			//Generating key to fetch the status from Map for the match
			String mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+useCaseNum+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
			System.out.println("LRM_Realized Mapper key"+mapperKey);
			System.out.println("\n#########################[Realized]Executing Row iteration:["+exclRowCnt+"] with test data:"+mapperKey+"#########################");
			lrmUpdateStatus = statusMapper.get(mapperKey);

			if(lrmUpdateStatus==null){
				reporter.reportStep(exclRowCnt, "Not Executed", "This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation. ", "", "Skip_DataVal", driver, "", strSheetName, null);
				System.err.println("This test case doesn't have LRM_Update[To_Execute=Y],so skipping data validation.");		
			}//End of IF condition where lrmUpdateStatus==null in cases where "To_Execute=N" in LRM_Update sheets
			
			//if the status is true then perform data validation for the search data 
			if(lrmUpdateStatus)
				objSearchPageFns.SearchResult_DataValidation(exclRowCnt,strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount, strColNames, strExpRows,strSheetName);		
			else if(!lrmUpdateStatus)
				reporter.reportStep(exclRowCnt, "Failed", "Txn Level LRM Update already applied or there is an issue with LRM update, so data validation for Realized view has been skipped. Please check it. !!", "", "Skip_DataVal", driver, "", strSheetName, null);

			System.out.println("###########################################################[End of Ledger Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of IF condition to check To_Execute=Y/N
		
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", strSheetName, null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of Realized()
	
	//***********************************End of Realized Data Provider*************************************************************

	
	//###################################################################################################################################################################  
	//Function name		: setobject_UpdateScriptFns(WebDriver driver){
	//Class name		: LRM_TxnLevel_Flow
	//Description 		: Function to create an object for LRM_Fns and pass driver object to the Constructor to instantiate all page objects
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	//creating object and passing the driver 
	public void setobject_UpdateScriptFns(WebDriver driver) {
		objLRMFns=new LRM_Fns(driver);
	}
	
}//End of <ClassL: LRM_TxnLevel_Flow>
