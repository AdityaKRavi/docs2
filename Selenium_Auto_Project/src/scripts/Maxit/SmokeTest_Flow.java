package scripts.Maxit;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Test;

import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.SmokeTestFns;
import functionalLibrary.Maxit.Smoke_DataProvider;

public class SmokeTest_Flow extends BaseClass {
	WebDriver driver;
	boolean strRetStatus;
	SmokeTestFns objSmokeTestFns;
	Reporting reporter = new Reporting();	
	//Logger log = Logger.getLogger(DataValidation_Flow.class);	
	
	//***********************************Search_Account Data Provider*************************************************************
	@Test(description = "Search_Account",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Search_Account(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_Account_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch,strClient);		
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method:Search_Account>
	//***********************************End of Search_Account Data Provider*************************************************************
	
	//***********************************Search_OpenClosed Data Provider*************************************************************
	@Test(description = "Search_OpenClosed",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Search_OpenClosed(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_OpenClosed_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch,strClient);		
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Search_OpenClosed>
	//***********************************End of Search_OpenClosed Data Provider*************************************************************	
	
	//***********************************Search_AuditTrail Data Provider*************************************************************
	@Test(description = "Search_AuditTrail",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Search_AuditTrail(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_AuditTrail_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);		
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Search_AuditTrail>
	//***********************************End of Search_AuditTrail Data Provider*************************************************************	
	
	
	//***********************************Search_TxnError Data Provider*************************************************************
	@Test(description = "Search_TrxError",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Search_TrxError(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_TrxError_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);		
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Search_TrxError>
	//***********************************End of Search_TxnError Data Provider*************************************************************		

	//***********************************Search_Realized Data Provider*************************************************************
			@Test(description = "Search_Realized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
			 public void Search_Realized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
				System.out.println(strViewPage +";" + To_Execute+";" +strAcct+";" +strSecType+";" +strSecValue);
				driver = ManageDriver_Maxit.getManageDriver().getDriver();
				objSmokeTestFns = new SmokeTestFns();
					
				if(To_Execute.equalsIgnoreCase("Y")){
					objSmokeTestFns.Search_Realized_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);		
				}//End of IF condition to check To_Execute=Y/N
				else{	
					reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
					throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
				}//End of ELSE condition to check To_Execute=Y/N		        
			}//End of <Method: Search_Realized>
			//***********************************End of Search_Realized Data Provider*************************************************************
	
	//***********************************Search_Unrealized Data Provider*************************************************************
			@Test(description = "Search_Unrealized",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
			 public void Search_Unrealized(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
				System.out.println(strViewPage +";" + To_Execute+";" +strAcct+";" +strSecType+";" +strSecValue);
				driver = ManageDriver_Maxit.getManageDriver().getDriver();
				objSmokeTestFns = new SmokeTestFns();
					
				if(To_Execute.equalsIgnoreCase("Y")){
					objSmokeTestFns.Search_Unrealized_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);		
				}//End of IF condition to check To_Execute=Y/N
				else{	
					reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
					throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
				}//End of ELSE condition to check To_Execute=Y/N		        
			}//End of <Method: Search_Unrealized>
			//***********************************End of Search_Unrealized Data Provider*************************************************************
	
		
		//***********************************Search_Ledger Data Provider*************************************************************
		@Test(description = "Search_Ledger",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
		 public void Search_Ledger(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
			System.out.println(strViewPage +";" + To_Execute+";" +strAcct+";" +strSecType+";" +strSecValue);
			driver = ManageDriver_Maxit.getManageDriver().getDriver();
			objSmokeTestFns = new SmokeTestFns();
				
			if(To_Execute.equalsIgnoreCase("Y")){
				objSmokeTestFns.Search_Ledger_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);		
			}//End of IF condition to check To_Execute=Y/N
			else{	
				reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
				throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
			}//End of ELSE condition to check To_Execute=Y/N		        
		}//End of <Method: Search_Ledger>
		//***********************************End of Search_Ledger Data Provider*************************************************************
			
	//***********************************Dashboard Data Provider*************************************************************
	@Test(description = "Dashboard",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Dashboard(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Dashboard_Flow(exclRowCnt);		
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Dashboard>
	//***********************************End of Dashboard Data Provider*************************************************************

	//***********************************DMI Data Provider*************************************************************
	@Test(description = "DMI",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void DMI(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.DMI_Flow(exclRowCnt,strAcct);		
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: DMI>
	//***********************************End of DMI Data Provider*************************************************************
	
	//***********************************Historic_Correction Data Provider*************************************************************
	@Test(description = "Historic_Correction",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Historic_Correction(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_HistCorrec_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch,strPageExists,strFrom_Date,strTo_Date );	
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Historic_Correction>
	//***********************************End of Historic_Correction Data Provider*************************************************************		
	//***********************************Step Up Data Provider*************************************************************
	@Test(description = "StepUp",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void StepUp(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.StepUp_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strStepUp_DoD, strPageExists);			
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: StepUp>
	//***********************************End of Historic_Correction Data Provider*************************************************************		

	//***********************************Search_RawTrades Data Provider*************************************************************
	@Test(description = "Search_RawTrades",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Search_RawTrades(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_RawTrades_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch,strClient);	
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Search_RawTrades>
	//***********************************End of Search_RawTrades Data Provider*************************************************************		

	//***********************************NoCostBasis Data Provider*************************************************************
	@Test(description = "NoCostBasis",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void NoCostBasis(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.NoCostBasis_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch,strClient);
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: NoCostBasis>
	//***********************************End of NoCostBasis Data Provider*************************************************************		

	//***********************************Search_SecurityXref Data Provider*************************************************************
	@Test(description = "Search_SecurityXref",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Search_SecurityXref(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_SecXref_Flow(exclRowCnt, strViewPage, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);		
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Search_SecurityXref>
	//***********************************End of Search_SecurityXref Data Provider*************************************************************	
	
	//***********************************Transaction Error Data Provider*************************************************************
	@Test(description = "Transaction_Error",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Transaction_Error(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Transaction_Error_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue,strFrom_Date, strTo_Date,strBlankSearch);
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Transaction_Error>
	//***********************************End of Transaction Error Data Provider*************************************************************		

	//***********************************CBManager_Imports Data Provider*************************************************************
	@Test(description = "CBManager_Imports",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void CBManager_Imports(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_Imports_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: CBManager_Imports>
	//***********************************End of CBManager_Imports Data Provider*************************************************************	
	
	//***********************************CBManager_Exports Data Provider*************************************************************
	@Test(description = "CBManager_Exports",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void CBManager_Exports(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.Search_Exports_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: CBManager_Exports>
	//***********************************End of CBMCBManager_Exports Data Provider*************************************************************	

	//***********************************PosRecon Data Provider*************************************************************
	@Test(description = "PosRecon",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void PosRecon(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.PositionRecon_Flow(strViewPage, exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: PosRecon>
	//***********************************End of PosRecon Data Provider*************************************************************	

	//***********************************AvgCostGift Data Provider*************************************************************
	@Test(description = "Avg_Cost_Gift",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	 public void Avg_Cost_Gift(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();
			
		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.AvgGiftCost_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date,strClient);
			
		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: AvgCostGift>
	//***********************************End of AvgCostGift Data Provider*************************************************************
	
	//***********************************Rollback_Rerun Data Provider*************************************************************
	@Test(description = "Rollback_Rerun",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	public void Rollback_Rerun(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();

		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.RollbackRerun_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: Rollback_Rerun>
	//***********************************End of Rollback_Rerun Data Provider*************************************************************	

//	//***********************************RBI_Upload Data Provider*************************************************************
//	@Test(description = "RBI_Upload",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
//	public void RBI_Upload(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
//		driver = ManageDriver_Maxit.getManageDriver().getDriver();
//		objSmokeTestFns = new SmokeTestFns();
//
//		if(To_Execute.equalsIgnoreCase("Y")){
//			objSmokeTestFns.BasisUpload_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);
//
//		}//End of IF condition to check To_Execute=Y/N
//		else{	
//			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
//			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
//		}//End of ELSE condition to check To_Execute=Y/N		        
//	}//End of <Method: RBI_Upload>
//	//***********************************End of RBI_Upload Data Provider*************************************************************

	//***********************************CB_Upload Data Provider*************************************************************
	@Test(description = "CB_Upload",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	public void CB_Upload(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();

		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.CB_Upload_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: RBI_Upload>
	//***********************************End of CB_Upload Data Provider*************************************************************

	//***********************************RBI_Upload Data Provider*************************************************************
	@Test(description = "Basis_Upload",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	public void Basis_Upload(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();

		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.BasisUpload_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: RBI_Upload>
	//***********************************End of RBI_Upload Data Provider*************************************************************
	
	//***********************************CA_Manager Data Provider*************************************************************
	@Test(description = "CA_Manager",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	public void CA_Manager(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();

		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.CAManager_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date,strCAID,strAddNewCA);

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: CA_Manager>
	//***********************************End of CA_Manager Data Provider*************************************************************	
	
	//***********************************GlobalDM_Download Data Provider*************************************************************
	@Test(description = "GlobalDM_Download",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	public void GlobalDM_Download(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();

		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.GlobalDM_Download_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: GlobalDM_Download>
	//***********************************End of GlobalDM_Download Data Provider*************************************************************		
	
	//***********************************GlobalDM_FileManager Data Provider*************************************************************
	@Test(description = "GlobalDM_FileManager",dependsOnMethods = "PreReq_CheckPoint",dataProvider = "Read_SmokeTestData",dataProviderClass = Smoke_DataProvider.class)
	public void GlobalDM_FileManager(String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFrom_Date , String strTo_Date, String strStepUp_DoD,String strBlankSearch,int exclRowCnt,String strPageExists,String strCAID,String strAddNewCA,String strClient ) throws Exception {
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		objSmokeTestFns = new SmokeTestFns();

		if(To_Execute.equalsIgnoreCase("Y")){
			objSmokeTestFns.GlobalDM_FileManager_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strPageExists, strFrom_Date, strTo_Date);

		}//End of IF condition to check To_Execute=Y/N
		else{	
			reporter.reportStep(exclRowCnt, "Not Executed", "To_Execute = N , so Skipping this row", "", "Skip Row", driver, "", "SmokeData", null);
			throw new SkipException("To_Execute=N, so skipping the row["+exclRowCnt+"] from DataValidation.");			
		}//End of ELSE condition to check To_Execute=Y/N		        
	}//End of <Method: GlobalDM_FileManager>
	//***********************************End of GlobalDM_FileManager Data Provider*************************************************************		
	
	@Test(description = "CloseBrowser", priority = 1)
	public void CloseBrowser() {
	  try {
		  System.out.println("Closing Driver object, Browser will be closed.");
		  ManageDriver_Maxit.getManageDriver().quit();
	  } catch (Exception e) {
		  System.out.println("Exception in @Test = CloseBrowser in SmokeTest_Flow class !!");
		  e.printStackTrace();
		}//End of Catch block
  }//End of afterTest Annotation method.
	
	
}//End of <Class: SmokeTest_Flow>
