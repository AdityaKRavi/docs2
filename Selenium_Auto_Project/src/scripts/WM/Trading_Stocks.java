
package scripts.WM;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.RGL_Page;
import PageObjects.WM.ResultTable_Section;
import PageObjects.WM.STF_NavigationPanel;
import PageObjects.WM.TradingPage;
import PageObjects.WM.UGL_Page;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.WM.WM_CommonFunctions;
import functionalLibrary.Global.*;
import functionalLibrary.WM.BaseTest;

public class Trading_Stocks extends BaseTest {
	  WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_TradingPages(ITestContext context, String to_Execute, String rowNo, String AccountNo, String description, String strKeyword, String fieldName, String dropDown_Value, String fields_Value) throws Exception {
		//###############################    Pre-requisite initialization calls    ############################################
		//######################################################################################################################
		driver = ManageDriver_WM.getManageDriver().getDriver();
		Login_WM Login_WM = new Login_WM();
		Reporting Reporting = new Reporting();
		int rowNum = Integer.parseInt(rowNo);
		System.out.println("######  Row No: = " + rowNum + "  ######");
		//Thread.sleep(1000);
		context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		String strClient = context.getCurrentXmlTest().getParameter("clientName");
		String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		//###########################################    LOGIN    ##############################################################
		//######################################################################################################################
		if(to_Execute.equals("Y")){
			//Main Login Call
			boolean loginPassFlg = Login_WM.HybridLogin_Call(strClient, strEnvi, strBrowser, rowNum, AccountNo, "", context);
			if(loginPassFlg == false){
				System.out.println("Login has failed for script - FilterCheck_RGL at row no: - " + rowNum);
				Reporting.reportStep(rowNum, "Failed", "Login failed.", "", "LoginFailed", driver, "", "", null);
				throw new SkipException("Skipping test row due to failed Login");
			}
		}
		else{
			  System.out.println("Row No: - " + rowNum + "Testing skipped for To_Execute = N");
			  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
				
			  //Skip the test iteration if To_Execute = N
			  throw new SkipException("Testing skipped for to execute = N");
		}//********  End of If-else for To_execute check  ********
		//#################################################   END of Login    ##################################################
		//######################################################################################################################
		System.out.println("Login step has Passed.");
		TradingPage tradingPage = new TradingPage();
		ResultTable_Section ResultTable_Section = new ResultTable_Section();
		WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
		// Navigate to Trading Stocks page
		if(!((driver.getTitle().equals("Trading - Stocks"))|| (driver.getTitle().equals("Trading - Stock")))){
			WM_CommonFunctions.navigate_toPage("Trading - Stocks", strClient, strEnvi);
			System.out.println("Page navigated to Tradings - Stocks for row - " + rowNum);
			Thread.sleep(3000);
		}
		Thread.sleep(3000);
		//Function Call
		//Check UI tests
		if(strKeyword.equalsIgnoreCase("UI Test")){
			tradingPage.tradingPage_UITests(rowNum, fieldName, dropDown_Value, fields_Value, strClient);
		}else{
			tradingPage.tradingPage_FunctionalTests(rowNum, fieldName, dropDown_Value, fields_Value, strClient);
		}
		
		//Check Functional Tests
		//##########################################   End Of Navigation test Case    ##########################################
		//######################################################################################################################
	  }//End Of @test Annotation and Method - test_Navigation_Panel
	
}//END Of Class
