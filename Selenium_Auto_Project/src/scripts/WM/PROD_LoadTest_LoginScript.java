
package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.HomePage;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.BaseTest;

public class PROD_LoadTest_LoginScript extends BaseTest {
		WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_Prod_Login(ITestContext context, String rowNo, String To_Execute, String strClientName) throws Exception {
			  Thread.sleep(50);
		  
			  driver = ManageDriver_WM.getManageDriver().getDriver();
			  Login_WM Login_WM = new Login_WM();
			  Reporting Reporting = new Reporting();
			//######################################################    Step 1 - Login  ###########################################
			//#####################################################################################################################
			  int rowNum = Integer.parseInt(rowNo);
		try{
			  System.out.println("######Checkpoint >> Prod Login Check >>  Row No: = " + rowNum + "; Client Name = " + strClientName + "  ######");
			  context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
			  String strClient = strClientName;
			  String strEnvi = context.getCurrentXmlTest().getParameter("environment");
			  String strBrowser = context.getCurrentXmlTest().getParameter("browser");
			  //###############  Check if To_Execute = Y  ###############
			  Boolean loginFlag;
			  if(To_Execute.equals("Y")){
				  //Thread.sleep(3000);
				  for(int i = 1; i <=10; i++){ // Adding loop for multiple login
					  loginFlag = Login_WM.fnWM_MainLogin(strEnvi, strBrowser, strClient, "");
				  
					  
					  //##############  Call Login if To_Execute = Y ##############
					  if(loginFlag == true){	
						  System.out.println("Login PASSED check for >> " + strClient.toUpperCase());
						  Reporting.reportStep(rowNum, "Passed", "", "", "", driver, "", "", null);
					  	
					  }//End of Nested If
					  else{
						context.setAttribute("resultStatus_AllPassed", "false");
						System.out.println("Login has failed for - " + strClient.toUpperCase());
						Reporting.reportStep(rowNum, "Failed", "Login step has failed. Please check the screensot.", "", "LoginFailed", driver, "", "", null);
						//throw new SkipException("Skipping test row due to failed Login");
					  }//********  End of If-else for login Call  ********
				  }//End of FOR
			  }//End of IF
			  else{
					  //System.out.println("To_Execute = N, thus Test row not executed.");
					  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
					  //Skip the test iteration if To_Execute = N
					  //throw new SkipException("Testing skipped for To_Execute = N.");
			  }//********  End of If-else for To_execute check  ********
			  
			//######################################################    END of Login    ###########################################
			//#####################################################################################################################
		  }catch(Exception e){
			  Reporting.reportStep(rowNum, "Failed", "Exception thrown. Login check failed.", "", "LoginScript", driver, "", "", null);
			  System.out.println("Client = " + strClientName + " >> Exception found in login script for row - " + rowNum + ". Please check!!! ");
		  }
	}//---------------------------------    END of @test Annotation    -------------------------------------

		


}//END Of Class
