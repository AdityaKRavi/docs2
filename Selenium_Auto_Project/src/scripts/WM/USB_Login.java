
package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.HomePage;
import PageObjects.WM.SummaryPage;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.BaseTest;

public class USB_Login extends BaseTest {
		WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_USB_Login(ITestContext context, String rowNo, String To_Execute, String strTestCase) throws Exception {
		  Thread.sleep(50);
		  driver = ManageDriver_WM.getManageDriver().getDriver();
		  Login_WM Login_WM = new Login_WM();
		  Reporting Reporting = new Reporting();
		  
		//######################################################    Step 1 - Login  ###########################################
		//#####################################################################################################################
		  int rowNum = Integer.parseInt(rowNo);
		  //System.out.println("######  Row No: = " + rowNum + "; Account No = " + strAccountNo + "Test Code = "+ restrictionCode + "  ######");
		  context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		  if(context.equals(null)){
			  System.out.println("*********%%%%%%%   USB Login - Context is NULL *********%%%%%%%    " + rowNum);
		  }//End of IF
		  String strClient = context.getCurrentXmlTest().getParameter("clientName");
		  String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		  String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		  System.out.println("*********%%%%%%%   USB Login Context client  *********%%%%%%%    " + strClient);
		  //###############  Check if To_Execute = Y  ###############
		  Boolean loginFlag;
		  if(To_Execute.equals("Y")){
			  //Thread.sleep(3000);
			  System.out.println("REACHED USB LOGIN CALL - " + rowNum);
				  loginFlag = Login_WM.fnWM_MainLogin(strEnvi, strBrowser, strClient, "");
				  //##############  Call Login if To_Execute = Y ##############
				if(loginFlag == true){	
					  System.out.println("1. Login was done successfully.");
					  //Check Summary Title is present of home page
					  //HomePage HomePage = new HomePage(driver);
					  SummaryPage SummaryPage = new SummaryPage();
					  
					  if(CommonUtils.isElementPresent(SummaryPage.page_TitleHead)){
						  String pageTitle = SummaryPage.page_TitleHead.getText();
						  if(pageTitle.equals("Summary")){
							  System.out.println("Passed. page title - " + pageTitle);
							  Reporting.reportStep(rowNum, "Passed", "Login Paased. Summary page displayed.", "", "", driver, "", "", null);
							  context.setAttribute("resultStatus_AllPassed", "true");
						  }else{
							  System.out.println("Failed. page title - " + pageTitle);
							  Reporting.reportStep(rowNum, "Failed", "Summary page not present. Login failed", "", "", driver, "", "", null);
							  context.setAttribute("resultStatus_AllPassed", "false");
						  }
					  }else{
						  context.setAttribute("resultStatus_AllPassed", "false");
						  System.out.println("Summary page dod not load");
						  Reporting.reportStep(rowNum, "Failed", "Summary page did not load.", "", "LoginFailed", driver, "", "", null);
							
					  }
				}//End of Nested If
				  else{
					context.setAttribute("resultStatus_AllPassed", "false");
					System.out.println("Login has failed.");
					Reporting.reportStep(rowNum, "Failed", "Login step has failed.", "", "LoginFailed", driver, "", "", null);
					throw new SkipException("Skipping test row due to failed Login");
				  }//********  End of If-else for login Call  ********
		  }//End of IF
		  else{
				  System.out.println("To_Execute = N, thus Test row not executed.");
				  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
				  //Skip the test iteration if To_Execute = N
				  throw new SkipException("Testing skipped for To_Execute = N.");
		  }//********  End of If-else for To_execute check  ********
		  
		//######################################################    END of Login    ###########################################
		//#####################################################################################################################
		  
		
	  }//---------------------------------    END of @test Annotation    -------------------------------------

}//END Of Class
