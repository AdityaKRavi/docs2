
package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.HomePage;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.BaseTest;

public class USB_MTRC extends BaseTest {
		WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_USB_Codes(ITestContext context, String rowNo, String To_Execute, String strAccountNo, String restrictionCode, String tradeMenuLink, String externalTransfer) throws Exception {
		  Thread.sleep(50);
		  driver = ManageDriver_WM.getManageDriver().getDriver();
		  Login_WM Login_WM = new Login_WM();
		  Reporting Reporting = new Reporting();
		//######################################################    Step 1 - Login  ###########################################
		//#####################################################################################################################
		  int rowNum = Integer.parseInt(rowNo);
		  System.out.println("######  Row No: = " + rowNum + "; Account No = " + strAccountNo + "Test Code = "+ restrictionCode + "  ######");
		  context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		  if(context.equals(null)){
			  System.out.println("*********%%%%%%%   USB MTRC - Context is NULL *********%%%%%%%    " + rowNum);
		  }//End of IF
		  String strClient = context.getCurrentXmlTest().getParameter("clientName");
		  String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		  String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		  System.out.println("*********%%%%%%%   USB MTRC Context client  *********%%%%%%%    " + strClient);
		  //###############  Check if To_Execute = Y  ###############
		  Boolean loginFlag;
		  if(To_Execute.equals("Y")){
			  //Thread.sleep(3000);
			  System.out.println("REACHED USB MTRC LOGIN CALL - " + rowNum);
				  loginFlag = Login_WM.fnWM_MainLogin(strEnvi, strBrowser, strClient, strAccountNo);
				  //##############  Call Login if To_Execute = Y ##############
				  if(loginFlag == true){	
					  System.out.println("1. Login was done successfully.");
					}//End of Nested If
				  else{
					context.setAttribute("resultStatus_AllPassed", "false");
					System.out.println("Login has failed.");
					Reporting.reportStep(rowNum, "Failed", "Login step has failed.", "", "LoginFailed", driver, "", "", null);
					throw new SkipException("Skipping test row due to failed Login");
				  }//********  End of If-else for login Call  ********
		  }//End of IF
		  else{
				  System.out.println("To_Execute = N, thus Test row not executed.");
				  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
				  //Skip the test iteration if To_Execute = N
				  throw new SkipException("Testing skipped for To_Execute = N.");
		  }//********  End of If-else for To_execute check  ********
		  
		//######################################################    END of Login    ###########################################
		//#####################################################################################################################
		  
		///####################    Step 2 - Check Trade Menu Tab    ####################
		//##############################################################################
		driver = ManageDriver_WM.getManageDriver().getDriver();
		//driver.manage().window().setPosition(new Point(300,2));
		//Initialize Home Page
		HomePage HomePage = PageFactory.initElements(driver, HomePage.class);
		//##############    Check if Trade Menu tab is present    ##############
		if(HomePage.clickOn("lnk_TradingTab") == false){
			Reporting.reportStep(rowNum, "Failed", "Trade menu link was not found", "", "TradeMenuTab_failed", driver, "", "", null);
			//Exit iteration if Trade Menu is not present
			throw new SkipException("Skip Test - Trade Menu link was not found.");
		}//End of If
		
		Thread.sleep(3000);
		String TradeMenuActualResult, strExpectedResult, strDetailsStmnt;
		//Get Expected result from data provider variable for all asset types - Stocks, Options and MF
		strExpectedResult = tradeMenuLink.trim().toUpperCase();
		//Create Details stmnt for reporting purpose
		if(strExpectedResult.equals("YES")){
			strDetailsStmnt = " link should be present.";
		}else{
			strDetailsStmnt = " link should not be present.";
		}//End of Details stmnt if-else
		
		//###############    Step 2.1 - Check for STOCKS Link  #############
		//##################################################################
		TradeMenuActualResult = "";
		//**************    Get actual result based on link presence    ************
        if(CommonUtils.isElementPresent(HomePage.lnk_TradeMenuStocksLink) == true){
        	TradeMenuActualResult = "YES";
        }else{
        	TradeMenuActualResult = "NO";
	    }//End of Actual result if-else loop
	        
        //**************    Report result of Expected and Actual value comparison    ************
        if (strExpectedResult.equals(TradeMenuActualResult)){
        	Reporting.reportStep(rowNum, "Passed", "Stocks link has passed.", "", "", driver,"", "", null);
        	System.out.println("2. Stocks link has Passed.");
        }
        else{
        	Reporting.reportStep(rowNum, "Failed", "Stocks" + strDetailsStmnt + " Stocks link has failed for code - " + restrictionCode, "", "StockTradeLinkFailed_Code" +restrictionCode, driver, "", "", HomePage.lnk_TradingTab);
        	
        	System.out.println("2. Stocks link has Failed.");	
        }//End of Report stmnt if-else
        
      //##########    Step 2.2 - Check for Options Link    ########
	  //##################################################################
        TradeMenuActualResult = "";
        //**************    Get Actual result based on link presence    ************
        if(CommonUtils.isElementPresent(HomePage.lnk_TradeMenuOptionsLink) == true){
        	TradeMenuActualResult = "YES";
        }else{
        	TradeMenuActualResult = "NO";
        }//End of Actual result if-else loop
        
        //********    Report result of Expected and Actual value comparison    *******
        if (strExpectedResult.equals(TradeMenuActualResult)){
        	Reporting.reportStep(rowNum, "Passed", "Options link has passed.", "", "", driver, "", "", null);
        	System.out.println("3. Options link has Passed.");
        }
        else{
        	Reporting.reportStep(rowNum, "Failed", "Options" + strDetailsStmnt + " Options link has failed for code - " + restrictionCode, "", "OptionsLinkFailed_Code" +restrictionCode, driver, "", "", HomePage.lnk_TradingTab);
        	
        	System.out.println("3. Options link has Failed.");
	    }//End of Report stmnt if-else
        
      //##########    Step 2.3 - Check for Mutual Funds Link    ########
	  //##################################################################
        TradeMenuActualResult = "";
        //**************    Get Actual result based on link presence    ************
        if(CommonUtils.isElementPresent(HomePage.lnk_TradeMenuMFLink) == true){
        	TradeMenuActualResult = "YES";
        }else{
        	TradeMenuActualResult = "NO";
        }//End of Actual value if-else
        
        //********    Report result of Expected and Actual value comparison    ********
        if (strExpectedResult.equals(TradeMenuActualResult)){
        	Reporting.reportStep(rowNum, "Passed", "MF link has passed.", "", "", driver, "", "", null);
        	System.out.println("4. MF link has Passed.");
        }
        else{
        	Reporting.reportStep(rowNum, "Failed", "MF" + strDetailsStmnt + " MF link has failed for code - " + restrictionCode, "", "MFLinkFailed_Code" +restrictionCode, driver, "", "", HomePage.lnk_TradingTab);
        	
        	System.out.println("4. MF link has Failed.");
        }//End of Report stmnt if-else
        //---------------------------------    END of Step 2    -------------------------------------    

		//********************    Step 3 - Check Transfer Menu Tab    ********************
		//*************************************************************************

        //##############    Check if Transfer Menu tab is present    ##############
		if(HomePage.clickOn("lnk_ExTransfersTab") == false){
			Reporting.reportStep(rowNum, "Failed", "External transfer menu drop down link was not found", "", "TxTranferTab_failed", driver, "", "", null);
			
			//Exit iteration if Transfer Menu is not present
			throw new SkipException("Skip Test - External transfer menu link was not found.");
		}//End of if to check Transfer Menu tab
		
		Thread.sleep(3000);
		String TransferMenuActualResult, strExpecteExTransferLinkResult, strExTransferDetailsStmnt;
		//*********    Get Expected result from data table for External Transfer    *********
		strExpecteExTransferLinkResult = externalTransfer.trim().toUpperCase();
		if(strExpecteExTransferLinkResult.equals("YES")){
			strExTransferDetailsStmnt = "External Transfer link should be present.";
		}else{
			strExTransferDetailsStmnt = "External Transfer link should not be present.";
		}//End of expected value if-else
		
		//##########    Step 3.1 - Test for External Transfer Link  ########
		//##################################################################
		TransferMenuActualResult = "";
		//**************    Get Actual result based on link presence    ************
        if(CommonUtils.isElementPresent(HomePage.lnk_ExTransfersMenuLink) == true){
        	TransferMenuActualResult = "YES";
        }else{
        	TransferMenuActualResult = "NO";
        }//End of Actual value if-else
        
        //**********    Report result of Expected and Actual value comparison    *********
        if (strExpecteExTransferLinkResult.equals(TransferMenuActualResult)){
        	Reporting.reportStep(rowNum, "Passed", "Extrnal transfer link has passed.", "", "", driver, "", "", null);
        	System.out.println("5. Ex. transfer link has Passed.");
        }
        else{
        	Reporting.reportStep(rowNum, "Failed", strExTransferDetailsStmnt + " Ex Tranfer link check has failed for code - " + restrictionCode, "", "ExTransferLinkFailed_Code" +restrictionCode, driver, "", "", HomePage.lnk_ExTransfersTab);
        	
        	System.out.println("5. Ex. transfer link has Failed.");
        }//End of Report stmnt if-else
      //---------------------------------    END of Step 3    -------------------------------------	
	  }//---------------------------------    END of @test Annotation    -------------------------------------

}//END Of Class
