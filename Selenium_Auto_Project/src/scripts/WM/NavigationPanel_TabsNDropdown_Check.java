
package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;
import PageObjects.WM.BOW_NavigationPanel;
import PageObjects.WM.NavigationPanel;
import PageObjects.WM.STF_NavigationPanel;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.WM.WM_CommonFunctions;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.BaseTest;

public class NavigationPanel_TabsNDropdown_Check extends BaseTest {
	  WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_Navigation_Panel(ITestContext context, String rowNo, String testScenario, String ExpTrading) throws Exception {
		//###############################    Pre-requisite initialization calls    ############################################
		//######################################################################################################################
		driver = ManageDriver_WM.getManageDriver().getDriver();
		Login_WM Login_WM = new Login_WM();
		Reporting Reporting = new Reporting();
		System.out.println (rowNo);
		int rowNum = Integer.parseInt(rowNo);
		System.out.println("######  Row No: = " + rowNum + "  ######");
		//Thread.sleep(1000);
		context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		String strClient = context.getCurrentXmlTest().getParameter("clientName");
		String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		System.out.println("*********%%%%%%%   Navigation panel Context client  *********%%%%%%%    " + strClient);
		//###########################################    LOGIN    ##############################################################
		//######################################################################################################################
		
		if(rowNo.equals("1")){
			System.out.println("REACHED HYBRID LOGIN CALL - " + rowNum);
			//Main Login Call
			boolean loginPassFlg = Login_WM.HybridLogin_Call(strClient, strEnvi, strBrowser, rowNum, "", "", context);
			if(loginPassFlg == false){
				Reporting.reportStep(rowNum, "Failed", strClient + "_NavigationPanel_TabsNDropdown_Check >> Login step has Failed.", "", "LoginFailed", driver, "", "", null);
			}else{
				System.out.println("Checkpoint >> Login has PASSED.");
			}
		}
		else{
			  System.out.println("Testing skipped for rowNum <> 1");
			  //Skip the test iteration if To_Execute = N
			  throw new SkipException("Testing skipped for rowNum <> 1");
		}//********  End of If-else for To_execute check  ********
		//#################################################   END of Login    ##################################################
		//######################################################################################################################
		
		//Call function for panel object verification
		ExpTrading = ExpTrading.trim();
		Thread.sleep(300);
		//wait for page load
		WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		NavigationPanel NavigationPanel = new NavigationPanel(driver);
		switch (strClient){
			case ("DAV"):
				NavigationPanel.DAV_Check_AllNavigationPanelObjects(rowNum, ExpTrading);
				break;
			case ("LPL"):
				NavigationPanel.LPL_Check_AllNavigationPanelObjects(rowNum, ExpTrading);
				break;
			case ("1DB"):
				NavigationPanel.FDB_Check_AllNavigationPanelObjects(rowNum, ExpTrading);
				break;
			case ("BOW"):
				NavigationPanel.BOW_Check_AllNavigationPanelObjects(rowNum, ExpTrading);
				break;
			case ("WDB"):
				break;
		}
		
		/*BOW_NavigationPanel BOW_NavigationPanel = new BOW_NavigationPanel(driver);
		BOW_NavigationPanel.BOW_Check_AllNavigationPanelObjects(rowNum, ExpTrading);*/
		//##########################################   End Of Navigation test Case    ##########################################
		//######################################################################################################################
	  }//End Of @test Annotation and Method - test_Navigation_Panel
	
}//END Of Class
