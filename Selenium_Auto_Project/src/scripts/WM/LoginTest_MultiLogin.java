package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageObjects.WM.HomePg_TopPanel;
import PageObjects.WM.HomePage;
import PageObjects.WM.LoginPage_WM;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.BaseTest;

public class LoginTest_MultiLogin extends BaseTest {
		
	WebDriver driver;
	

	  //@Parameters({"browser", "clientName", "environment"})
	  @Test(dataProvider="TestData")
	  public void test_Login(ITestContext context, String rowNo, String To_Execute, String strURL, String LoginId, String sUserName, String Pwd, String LoginType, String strAccountNo) throws Exception {
		  Login_WM Login = new Login_WM();
		  Reporting Reporting = new Reporting();
		  //Initialize the browser
		  System.out.println("***********************************  Class - Login Test ***************************");
		  System.out.println("*************************************************************************************");
		  
			//Login with given Account no:
		  String strClient = context.getCurrentXmlTest().getParameter("clientName");
		  String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		  String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		  LoginPage_WM LoginPage = PageFactory.initElements(driver, LoginPage_WM.class);
		  int rowNum = Integer.parseInt(rowNo);
		  //check summary page is present
		  HomePg_TopPanel SummaryPage = PageFactory.initElements(driver, HomePg_TopPanel.class);
		  
		  //Calling login function for each row
		  if(To_Execute.equals("Y")){
			  Boolean loginFlag = Login.fnWM_MainLogin(strEnvi, strBrowser, strClient, strAccountNo);
			  if(loginFlag == true){
					
				  System.out.println("Login was done successfully.");
				  Reporting.reportStep(rowNum, "Passed", "Login step has passed.", "", "LoginFailed", driver, "", "", null);
					
				}else{
				System.out.println("login failed");
				Reporting.reportStep(rowNum, "Failed", "Login step has failed.", "", "LoginFailed", driver, "", "", null);
				
			  }
		  }else{
			  System.out.println("To_Execute = N, thus Test row not executed.");
			  throw new SkipException("Testing skip.");
		  }

		  System.out.println("after calling login function...");
		  //Start the Test case from here:
			HomePage HomePage = PageFactory.initElements(driver, HomePage.class);
			
		
	  }
	  


}
