package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageObjects.WM.HomePg_TopPanel;
import PageObjects.WM.HomePage;
import PageObjects.WM.LoginPage_WM;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.BaseTest;

public class LoginTest_SingleAcntLogin extends BaseTest {
		
	WebDriver driver;

	CommonUtils CommonUtils = new CommonUtils();
	  @Parameters({"browser", "clientName", "environment"})
	  @Test(priority = 0)
	  public void test_Login(String strBrowser, String strClient, String strEnvi, ITestContext context) throws Exception {
		  Login_WM Login = new Login_WM();
			new Reporting();
		  //Initialize the browser
		  System.out.println("***********************************  Class - Login Test ***************************");
		  System.out.println("*************************************************************************************");
		  
		  PageFactory.initElements(driver, LoginPage_WM.class);
		  PageFactory.initElements(driver, HomePg_TopPanel.class);
		  
		  //Calling login function for each row
		  Boolean loginFlag = Login.fnWM_MainLogin(strEnvi, strBrowser, strClient, "");
		  String imgPath = CommonUtils.captureScreenshot(driver, "LoginFailed", "failed detail", "", null);
		  System.out.println("Path of login screenshot - " + imgPath);
		  if(loginFlag.equals(false)){
			  System.out.println("Login has failed. Test script could not be executed. Please check");
			  imgPath = CommonUtils.captureScreenshot(driver, "LoginFailed", "failed detail", "", null);
			  System.out.println("Path of failed login - " + imgPath);
		  }
		  Assert.assertTrue(loginFlag, "Login was done successfully");
	  }

	  @Test(dataProvider="TestData", dependsOnMethods={"test_Login"}, priority = 1)
	  public void test_USBTransferCodes(String rowNo, String To_Execute, String strAccountNo, String restrictionCode, String tradeMenuLink, String externalTransfer) throws Exception {

			//Loop thru each row and execute test where To_Execute =Y
		  	System.out.println("*************************************************************************************");
		  	System.out.println("");
		  	System.out.println("");
			System.out.println("******************************  Executing ROW No -  "+ rowNo + "    ****** From Test method ************************");
			String strTo_Execute = To_Execute.toString();	
			//Skip iteration if To_Execute = N or blank
			if (strTo_Execute.equals("N") || strTo_Execute.equals("")){
				System.out.println("To_Execute = N, Thus test row not executed.");
				throw new SkipException("Testing skip.");
				}
	  		PageFactory.initElements(driver, HomePage.class);
			
	  		// Start of Test Case from here

	  }
	  @Test(priority = 2)
	  public void test_Priority(){
		  System.out.println("Priority test");
	  }
	  
}
