package scripts.WM;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.Branding_Panel;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.WM.SingleLogin_BaseTest;
import functionalLibrary.WM.WM_CommonFunctions;

public class BrandingPanel_Check extends SingleLogin_BaseTest{
	WebDriver driver;
	//#############    Login is handled by @beforetest    #############
	@Test(dataProvider="TestData", dependsOnMethods = "beforeTest_ResultCheck")
	public void BrandingPanel(String rowNum, String to_Execute, String strTestKeyword) throws Exception{
		//@test - Check branding based on client name
		if(to_Execute.equals("Y")){
			//##############################   Pre-Requisite    ###############################
			int rowNo = Integer.parseInt(rowNum);
			System.out.println("Script - Branding Check. Running row - " + rowNum);
			Branding_Panel Branding_Panel = new Branding_Panel();
			ITestContext context =  GlobalObjectsFactory.getGlobalObjects().getiTestContext();
			String strClient = context.getCurrentXmlTest().getParameter("clientName");
			//wait for page load
			WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
			//WM_CommonFunctions.wait_ForPageLoad(rowNo);
			//#################   Test function call based on client name   ###################
			Branding_Panel.check_AllBrandingPanelElements(strClient, rowNo);

			//#################################################################################
			
		}else{

			//#################   SKIP Test row   #################//#################
			  System.out.println("To_Execute = N, thus Test row not executed.");
			  throw new SkipException("Testing skipped.");
		  }	//END of IF - ELSE
		
	}//END of Test Method
	
}//END of CLASS
