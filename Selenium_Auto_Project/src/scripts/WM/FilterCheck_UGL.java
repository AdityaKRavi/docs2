
package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.ResultTable_Section;
import PageObjects.WM.STF_NavigationPanel;
import PageObjects.WM.UGL_Page;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.WM.WM_CommonFunctions;
import functionalLibrary.Global.*;
import functionalLibrary.WM.BaseTest;

public class FilterCheck_UGL extends BaseTest {
	  WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_UGL_Filters(ITestContext context, String to_Execute, String rowNo, String AccountNo, String strKeyword, String strFilterName, String strAllAssetTypeValue, String strAllTermTypeValue, String strSymbolSearchValue) throws Exception {
		//###############################    Pre-requisite initialization calls    ############################################
		//######################################################################################################################
		driver = ManageDriver_WM.getManageDriver().getDriver();
		Login_WM Login_WM = new Login_WM();
		Reporting Reporting = new Reporting();
		int rowNum = Integer.parseInt(rowNo);
		System.out.println("######  Row No: = " + rowNum + "  ######");
		//Thread.sleep(1000);
		context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		String strClient = context.getCurrentXmlTest().getParameter("clientName");
		String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		
		//###########################################    LOGIN    ##############################################################
		//######################################################################################################################
		if(to_Execute.equals("Y")){
			System.out.println("REACHED HYBRID LOGIN CALL - " + rowNum);
			//Main Login Call
			//Using Hybrid login to support multiple accounts usage
			boolean loginPassFlg = Login_WM.HybridLogin_Call(strClient, strEnvi, strBrowser, rowNum, AccountNo, "", context);
			if(loginPassFlg == false){
				//Failed Login will report in result file and skip iteration
				Reporting.reportStep(rowNum, "Failed", "Login failed.", "", "LoginFailed", driver, "", "", null);
				System.out.println("Login has failed for script - FilterCheck_UGL at row no: - " + rowNum);
				throw new SkipException("Skipping test row due to failed Login");
			}
		}
		else{
			  System.out.println("Testing skipped for To_Execute = N");
			  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
			  //Skip the test iteration if To_Execute = N
			  throw new SkipException("Testing skipped for to execute = N");
		}//********  End of If-else for To_execute check  ********
		//#################################################   END of Login    ##################################################
		//######################################################################################################################
		System.out.println("Login step has Passed.");
		UGL_Page UGL_Page = new UGL_Page();
		ResultTable_Section ResultTable_Section = new ResultTable_Section();
		WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
		// Navigate to UGL page
		//Navigate to home page and then back to UGL page so that the UGL page comes back to original state with no left over filter applied
		WM_CommonFunctions.navigate_toPage("Summary", strClient, strEnvi);
		WM_CommonFunctions.navigate_toPage("UGL", strClient, strEnvi);
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		System.out.println("Page navigated to UGL for row - " + rowNum);
		Thread.sleep(1000);
		//Function Call
		UGL_Page.checkFilters_ValueAndFunctioning(rowNum, strKeyword, strFilterName, strAllAssetTypeValue, strAllTermTypeValue, strSymbolSearchValue, strClient);
		//##########################################   End Of Navigation test Case    ##########################################
		//######################################################################################################################
	  }//End Of @test Annotation and Method - test_Navigation_Panel
	
}//END Of Class
