package scripts.WM;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import functionalLibrary.Global.DateUtils;

public class TestCode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DateUtils DateUtils = new DateUtils();
		 LocalTime time = new LocalTime();    
		 DateTimeFormatter fmt = DateTimeFormat.forPattern("h:mm:ss:SS a");
		 String str = fmt.print(time);
		 System.out.println("Time Now = " + str);
		
		DateTime jodaTime = new DateTime();
		//Friday, November 17, 2017 1:57 PM ET
		String strDate = jodaTime.dayOfWeek().getAsText() + ", " + jodaTime.monthOfYear().getAsText() +  " " + jodaTime.getDayOfMonth() + ", " + jodaTime.getYear() + " " +  str;
		
		
		  
		    
		    System.out.println("Time = " + strDate);
		/*System.out.println("year = " + jodaTime.getYear());
		System.out.println("month = " + jodaTime.getMonthOfYear());
		System.out.println("day = " + jodaTime.getDayOfMonth());
		System.out.println("hour = " + jodaTime.getHourOfDay());
		System.out.println("minute = " + jodaTime.getMinuteOfHour());
		System.out.println("second = " + jodaTime.getSecondOfMinute());
		System.out.println("millis = " + jodaTime.getMillisOfSecond());*/
		    
		    DateTime dt = new DateTime();  // current time
		    String monthStr = dt.monthOfYear().getAsText();
		    String monthSt2r = dt.dayOfWeek().getAsText();
		    
		    System.out.println("month name - " + monthSt2r);
		    System.out.println("Day number - " + DateUtils.getCurrentDateTime_Parameters("WeekDayNumber"));
		    
		    String test11 = DateUtils.minusMin_ToDate(1);
		    String test12 = DateUtils.minusMin_ToDate(2);
		    
		    String testAmPm = DateUtils.getCurrentDateTime_Parameters("AmPm");
		    System.out.println(" AmPm is = " + testAmPm);
		    
		    System.out.println(" date + 1 is = " + test11);
		    System.out.println(" date + 2 is = " + test12);
		    
		    String DateTime = DateUtils.getCurrentDateTime_Parameters("WeekDayName") + ", " + DateUtils.getCurrentDateTime_Parameters("MonthName") +" " + DateUtils.getCurrentDateTime_Parameters("Day") + ", " + DateUtils.getCurrentDateTime_Parameters("Year") + " " + DateUtils.getCurrentDateTime_Parameters("TimeInMinAmPm") + " ET";
		    System.out.println("Actual date is = " + DateTime);
		    
		    
		    String time1 = DateUtils.minusMin_ToDate(1);
		    String time2 = DateUtils.minusMin_ToDate(2);
		    String actualDate1 =  DateUtils.getCurrentDateTime_Parameters("WeekDayName") + ", " + DateUtils.getCurrentDateTime_Parameters("MonthName") +" " + DateUtils.getCurrentDateTime_Parameters("Day") + ", " + DateUtils.getCurrentDateTime_Parameters("Year") + " " + time1 + DateUtils.getCurrentDateTime_Parameters("AmPm") + " ET";;
			String actualDate2 =  DateUtils.getCurrentDateTime_Parameters("WeekDayName") + ", " + DateUtils.getCurrentDateTime_Parameters("MonthName") +" " + DateUtils.getCurrentDateTime_Parameters("Day") + ", " + DateUtils.getCurrentDateTime_Parameters("Year") + " " +time2 + DateUtils.getCurrentDateTime_Parameters("AmPm") + " ET";;
			 System.out.println(" date 1 is = " + actualDate1);
			 System.out.println(" date2  is = " + actualDate2);
		    

	}

}
