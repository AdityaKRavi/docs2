
package scripts.WM;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.Holdings_Page;
import PageObjects.WM.RGL_Page;
import PageObjects.WM.ResultTable_Section;
import PageObjects.WM.STF_NavigationPanel;
import PageObjects.WM.UGL_Page;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.WM.WM_CommonFunctions;
import functionalLibrary.Global.*;
import functionalLibrary.WM.BaseTest;

public class ActionMenu extends BaseTest {
	  WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_HoldingsPage_Filter(ITestContext context, String to_Execute, String rowNo, String pageName, String AccountNo, String strKeyword, String strFilterName, String strAllAssetTypeValue, String strHeldAway) throws Exception {
		//###############################    Pre-requisite initialization calls    ############################################
		//######################################################################################################################
		driver = ManageDriver_WM.getManageDriver().getDriver();
		Login_WM Login_WM = new Login_WM();
		Reporting Reporting = new Reporting();
		int rowNum = Integer.parseInt(rowNo);
		System.out.println("######  Row No: = " + rowNum + "  ######");
		//Thread.sleep(1000);
		context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		String strClient = context.getCurrentXmlTest().getParameter("clientName");
		String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		//###########################################    LOGIN    ##############################################################
		//######################################################################################################################
		if(to_Execute.equals("Y")){
			//Main Login Call
			boolean loginPassFlg = Login_WM.HybridLogin_Call(strClient, strEnvi, strBrowser, rowNum, AccountNo, "", context);
			if(loginPassFlg == false){
				System.out.println(strClient + " >> Login has failed for script - Actions Menu at row no: - " + rowNum);
				Reporting.reportStep(rowNum, "Failed", "Login failed.", "", "LoginFailed", driver, "", "", null);
				throw new SkipException("Skipping test row due to failed Login");
			}
		}
		else{
			  System.out.println("Row No: - " + rowNum + "Testing skipped for To_Execute = N");
			  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
			 
			  //Skip the test iteration if To_Execute = N
			  throw new SkipException("Testing skipped for to execute = N");
		}//********  End of If-else for To_execute check  ********
		//#################################################   END of Login    ##################################################
		//######################################################################################################################
		System.out.println("Login step has Passed.");
		Holdings_Page Holdings_Page = new Holdings_Page();
		ResultTable_Section ResultTable_Section = new ResultTable_Section();
		WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
		//Select the account number from dropdown for LPL
		if(strClient.equals("LPL")) {
			WM_CommonFunctions.select_AccountNum(rowNum, strClient, AccountNo);
		}
		// Navigate to UGL page
		//Navigate to home page and then back to UGL page so that the UGL page comes back to original state with no left over filter applied
		WM_CommonFunctions.navigate_toPage("Summary", strClient, strEnvi);
		//System.out.println("Keyword = " + strKeyword);
		
		if(strKeyword.contains("AssetView")){
			System.out.println("Asset view");
			WM_CommonFunctions.navigate_toPage("Holdings Asset", strClient, strEnvi);
		}else{
			WM_CommonFunctions.navigate_toPage("Holdings", strClient, strEnvi);
		}
		
		System.out.println("Page navigated to Holdings for row - " + rowNum);
		
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		if(!strClient.equalsIgnoreCase("USB")){
			WM_CommonFunctions.wait_ForPageLoad(rowNum);
		}else{
			Thread.sleep(500);
		}
		
		//Function Call
		Holdings_Page.checkFilters_ValueAndFunctioning(rowNum, strKeyword, strFilterName, strAllAssetTypeValue, strHeldAway, strClient);
		//##########################################   End Of Navigation test Case    ##########################################
		//######################################################################################################################
	  }//End Of @test Annotation and Method - test_Navigation_Panel
	
}//END Of Class
