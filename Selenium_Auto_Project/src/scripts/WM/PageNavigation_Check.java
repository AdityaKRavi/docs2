
package scripts.WM;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;

import PageObjects.WM.BOW_NavigationPanel;
import PageObjects.WM.NavigationPanel;
import PageObjects.WM.NavigationPanel.rwb_NavigationPanel;
import PageObjects.WM.STF_NavigationPanel;
import PageObjects.WM.USB_NavigationPanel;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.WM.WM_CommonFunctions;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.BaseTest;

public class PageNavigation_Check extends BaseTest {
	  WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_PageNavigation(ITestContext context, String to_Execute, String rowNo, String AccountNo, String strPageName, String strExpectedPageTitle) throws Exception {
		//###############################    Pre-requisite initialization calls    ############################################
		//######################################################################################################################
		driver = ManageDriver_WM.getManageDriver().getDriver();
		Login_WM Login_WM = new Login_WM();
		Reporting Reporting = new Reporting();
		int rowNum = Integer.parseInt(rowNo);
		System.out.println("######  Row No: = " + rowNum + "  ######");
		//Thread.sleep(1000);
		context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		String strClient = context.getCurrentXmlTest().getParameter("clientName");
		String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		System.out.println("*********%%%%%%%   Page Navigation for "  + strClient + "*********%%%%%%%    ");
		//###########################################    LOGIN    ##############################################################
		//######################################################################################################################
		if(to_Execute.equals("Y")){
			//System.out.println("REACHED HYBRID LOGIN CALL - " + rowNum);
			//Main Login Call
			boolean loginPassFlg = Login_WM.HybridLogin_Call(strClient, strEnvi, strBrowser, rowNum, AccountNo, "", context);
			if(loginPassFlg == false){
				Reporting.reportStep(rowNum, "Failed", "Login failed.", "", "LoginFailed", driver, "", "", null);
			}
		}
		else{
			  System.out.println("Testing skipped for To_Execute = N");
			  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
				
			  //Skip the test iteration if To_Execute = N
			  throw new SkipException("Testing skipped for to execute = N");
		}//********  End of If-else for To_execute check  ********
		//#################################################   END of Login    ##################################################
		//######################################################################################################################
		Thread.sleep(500);
		WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
		NavigationPanel NavigationPanel = new NavigationPanel(driver);
		switch (strClient){
			case "STF":
				STF_NavigationPanel STF_NavigationPanel = new STF_NavigationPanel(driver);
				//Page load wait
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				STF_NavigationPanel.check_PageNavigation_viaNavPanel(rowNum, strPageName, strExpectedPageTitle);
				break;
			case "USB":
				USB_NavigationPanel USB_NavigationPanel = new USB_NavigationPanel(driver);
				USB_NavigationPanel.check_PageNavigation_viaNavPanel(rowNum, strPageName, strExpectedPageTitle);
				break;
			case "BOW":
				//BOW_NavigationPanel BOW_NavigationPanel = new BOW_NavigationPanel(driver);
				NavigationPanel.checkBOW_PageNavigation_viaNavPanel(rowNum, strPageName, strExpectedPageTitle);
				break;
			case "LPL":
				NavigationPanel.checkLPL_PageNavigation_viaNavPanel(rowNum, strPageName, strExpectedPageTitle);
				break;
			case "DAV":
				NavigationPanel.checkDAV_PageNavigation_viaNavPanel(rowNum, strPageName, strExpectedPageTitle);
				break;
			case "1DB":
				NavigationPanel.checkFDB_PageNavigation_viaNavPanel(rowNum, strPageName, strExpectedPageTitle);
				break;
			case "RWB":
				NavigationPanel.rwb_NavigationPanel rwb_inner = NavigationPanel.new rwb_NavigationPanel();
				rwb_inner.checkRWB_PageNavigation_viaNavPanel(rowNum, strPageName, strExpectedPageTitle);
				break;
		}
		//Call function for panel object verification
		
		//##########################################   End Of Navigation test Case    ##########################################
		//######################################################################################################################
	  }//End Of @test Annotation and Method - test_Navigation_Panel
	
}//END Of Class
