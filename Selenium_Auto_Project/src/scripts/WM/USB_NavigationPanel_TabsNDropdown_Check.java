
package scripts.WM;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.Test;
import PageObjects.WM.LoginPage_WM;
import PageObjects.WM.USB_NavigationPanel;
import functionalLibrary.WM.Login_WM;
import functionalLibrary.WM.USB_PageFunctions;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.BaseTest;
import functionalLibrary.WM.WM_CommonFunctions;

public class USB_NavigationPanel_TabsNDropdown_Check extends BaseTest {
	  WebDriver driver;
	  @Test(dataProvider="TestData")
	  public void test_Navigation_Panel(ITestContext context, String rowNo, String To_Execute, String strAccountNo, String testScenario, String strTestKeyword, String ExpTrading, String ExpTransfer) throws Exception {
		//###############################    Pre-requisite initialization calls    ############################################
		//######################################################################################################################
		driver = ManageDriver_WM.getManageDriver().getDriver();
		Login_WM Login_WM = new Login_WM();
		Reporting Reporting = new Reporting();
		int rowNum = Integer.parseInt(rowNo);
		System.out.println("######  Row No: = " + rowNum + "  ######");
		//Thread.sleep(1000);
		context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		String strClient = context.getCurrentXmlTest().getParameter("clientName");
		String strEnvi = context.getCurrentXmlTest().getParameter("environment");
		String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		System.out.println("*********%%%%%%%   USB Navigation panel Context client  *********%%%%%%%    " + strClient);
		//###########################################    LOGIN    ##############################################################
		//######################################################################################################################
		if(To_Execute.equals("Y")){
			System.out.println("REACHED HYBRID LOGIN CALL - " + rowNum);
			//Main Login Call
			boolean loginPassFlg = Login_WM.HybridLogin_Call(strClient, strEnvi, strBrowser, rowNum, strAccountNo, To_Execute, context);
			if(loginPassFlg == false){
				Reporting.reportStep(rowNum, "Failed", "USB_NavigationPanel_TabsNDropdown_Check >> Login step has failed in @test.", "", "LoginFailed", driver, "", "", null);
			}
		}
		else{
			  System.out.println("To_Execute = N, thus Test row not executed.");
			  //Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
			  Reporting.reportStep(rowNum, "Not Executed", "", "", "", driver, "", "", null);
			  //Skip the test iteration if To_Execute = N
			  throw new SkipException("Testing skipped for To_Execute = N.");
		}//********  End of If-else for To_execute check  ********
		//#################################################   END of Login    ##################################################
		//######################################################################################################################
		WM_CommonFunctions pageFunction = new WM_CommonFunctions();
		//Initialize Test Page  (POM)
		USB_NavigationPanel navPanel = new USB_NavigationPanel(driver);
		USB_PageFunctions usb_Func = new USB_PageFunctions();
		String allActualOptions = null;
		String allExpOptions = "";
		//Note - Test case for each row are executed based on hybrid keyword driven approach.
				switch (strTestKeyword){
				case "Navigation_Panel":
					navPanel.checkAllNavTabs_arePresent(rowNum);
					break;
				case "Account Details":
					allActualOptions = navPanel.getNavigation_DropDownItems("Account Details", rowNum);
					System.out.println("Actual - " + allActualOptions);
					if(!allActualOptions.equals("")){
						allExpOptions = usb_Func.getNavigation_ExpectedDropDownValues("Account Details");
						//Call a report function that does value comparison and adds result in report file
						System.out.println("Expected - " + allExpOptions);
						pageFunction.add_ReportStep(rowNum, allActualOptions, allExpOptions, "AccountDdCheck", "");
					}//End of IF stmnt
					break;
				case "Trading":
					allActualOptions = navPanel.getNavigation_DropDownItems("Trading", rowNum);
					if(!allActualOptions.equals("")){
						System.out.println("Actual - " + allActualOptions);
						//Trading can have multiple expected values thus fetching the non default expected value from datatable
						if(!ExpTrading.trim().equals("")){
							allExpOptions = ExpTrading;
						}else{
							//else use a default one from function call
							allExpOptions = usb_Func.getNavigation_ExpectedDropDownValues("Trading");}//End of nested IF-else stmnt
						System.out.println("in trading");
						pageFunction.add_ReportStep(rowNum, allActualOptions, allExpOptions, "TradingDdCheck", "");
					}//End of IF stmnt
					break;
				case "Transfers":
					allActualOptions = navPanel.getNavigation_DropDownItems("Transfers", rowNum);
					if(!allActualOptions.equals("")){						
						System.out.println("Actual - " + allActualOptions);
						//Trading can have multiple expected values thus fetching the non default expected value from datatable
						if(!ExpTransfer.trim().equals("")){
							allExpOptions = ExpTransfer;
						}else{
							//else use a default one from function call
							allExpOptions = usb_Func.getNavigation_ExpectedDropDownValues("Transfers");}//End of nested IF-else stmnt
						System.out.println("in transfer");
						pageFunction.add_ReportStep(rowNum, allActualOptions, allExpOptions, "TransferstDdCheck", "");
					}//End of IF stmnt
					break;
				case "My Documents":
					allActualOptions = navPanel.getNavigation_DropDownItems("My Documents", rowNum);
					if(!allActualOptions.equals("")){						
						System.out.println("Actual - " + allActualOptions);
						allExpOptions = usb_Func.getNavigation_ExpectedDropDownValues("My Documents");
						System.out.println("in My doc");
						pageFunction.add_ReportStep(rowNum, allActualOptions, allExpOptions, "MyDocDdCheck", "");
					}//End of IF stmnt
					break;
				case "Markets & Research":
					allActualOptions = navPanel.getNavigation_DropDownItems("Markets & Research", rowNum);
					if(!allActualOptions.equals("")){						
						System.out.println("Actual - " + allActualOptions);
						allExpOptions = usb_Func.getNavigation_ExpectedDropDownValues("Markets & Research");
						System.out.println("in mkt n r");
						pageFunction.add_ReportStep(rowNum, allActualOptions, allExpOptions, "MktAndResearchDdCheck", "");
					}//End of IF stmnt
					break;
				case "Education Center":
					allActualOptions = navPanel.getNavigation_DropDownItems("Education Center", rowNum);
					if(!allActualOptions.equals("")){						
						System.out.println("Actual - " + allActualOptions);
						allExpOptions = usb_Func.getNavigation_ExpectedDropDownValues("Education Center");
						System.out.println("in Edu center");
						pageFunction.add_ReportStep(rowNum, allActualOptions, allExpOptions, "EduCenterDdCheck", "");
					}//End of IF stmnt
					break;
				case "Failed":
					pageFunction.add_ReportStep(rowNum, "Invalid test data -Navigation Tab keyword not found.", "", "FailedTab", "");
					break;
			  }//End of Switch case
		//##########################################   End Of Navigation test Case    ##########################################
		//######################################################################################################################
	  }//End Of @test Annotation and Method - test_Navigation_Panel
	
}//END Of Class
