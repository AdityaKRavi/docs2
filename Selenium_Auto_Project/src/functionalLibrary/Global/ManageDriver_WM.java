package functionalLibrary.Global;


import java.util.HashMap;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.MalformedURLException;
import java.net.URL;


public class ManageDriver_WM {
	//private static ManageDriver_WM manageDriver = null;
	static ManageDriver_WM manageDriver;
	//private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
	private WebDriver driver;
	public WebDriverWait wait ;
	private HashMap<Long, WebDriver> threadDriverMap = null;
 
	/*private ManageDriver_WM(){

	    }*/
    //getManageDriver is a factory method and returns new object in first call and returns existing object for subsequent calls.
    public static ManageDriver_WM getManageDriver() {
    	try {
			Thread.sleep((long)(Math.random() * 1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if (manageDriver == null){
            manageDriver = new ManageDriver_WM();
        }
        return manageDriver;
    }
    
	//public static WebDriver initializeDriver(String strBrowser){
	public WebDriver initializeDriver(String strBrowser, String csv_Others) throws MalformedURLException, InterruptedException{
	//public WebDriver initializeDriver(String strBrowser){
		long id = Thread.currentThread().getId();
		//Client_Envi_ScriptName__OS_Browser_Version
		String strClient = "", strScript = "", strEnv = "", strOS = "", strVersion = "", strHeadless ="";
		String strSauceRun_Name = "";
		String[] arrOthers = csv_Others.split(";");
		if(!csv_Others.equals("") || arrOthers.length < 5){
			strClient = arrOthers[0];
			strEnv = arrOthers[1];
			strScript = arrOthers[2];
			strOS = arrOthers[3];
			strVersion = arrOthers[4];
			strHeadless = arrOthers[5];
			if(strHeadless.equals("true")){
				strSauceRun_Name = strClient + "_" + strEnv + "_" + strScript + "__" + strOS + "_" + strBrowser.replace("_s", "") + " - " + strVersion + "_Headless";	
			}else{
				strSauceRun_Name = strClient + "_" + strEnv + "_" + strScript + "__" + strOS + "_" + strBrowser.replace("_s", "") + " - " + strVersion;	
			}//end of nested If
		}else{
			System.out.println("Method : initializeDriver >> Other Variables are blank thus browser used = Local Chrome.");
			strBrowser = "chrome";
		}
		driver = null;
		if (threadDriverMap != null)
			driver = threadDriverMap.get(id);
		else
			threadDriverMap = new HashMap<Long, WebDriver>();
		
		// *********    Part of SE Grid code    ********** 
		//Capabilities caps;
		//ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
       // String platformVal = ((String) context.getAttribute("platform")).toUpperCase();
		//******************************************************
			//String strURL = "http://dhanasekarkumar:8283df4c-eb06-4455-9faf-ad25cc3df0a5@sauce.dev.wm.scivantage.com:4444/wd/hub";
			//String strURL = "http://nhorvath:f2844476-764e-47d1-9db2-a86565bcd6e9@sauce.dev.wm.scivantage.com:4444/wd/hub";
			String strURL = "http://nvatsa:d154d3da-b4de-4a9e-998a-b448e2c6eff8@sauce.dev.wm.scivantage.com:4444/wd/hub";
			
		Capabilities caps = null;
			ChromeOptions chromeOptions = new ChromeOptions();
		
 		if (driver == null) {
 			switch (strBrowser.toLowerCase()) {
			
			case "chrome" :
				System.out.println("Test no saucelab");
				System.setProperty("webdriver.chrome.driver", "C://SVN_Automation_New//Trunk//Selenium_Automation//Selenium_Auto_Project//BrowserDrivers//chromedriver.exe");
				chromeOptions.addArguments("--start-maximized");
				driver = new ChromeDriver(chromeOptions);
				threadDriverMap.put(id, driver);
				//break;
				return driver;
				
			case "chrome_s" : 
				//System.out.println("Its Chrome S");
				 chromeOptions.setCapability("platform", strOS );
				 chromeOptions.setCapability("version", strVersion );
				 //chromeOptions.setHeadless(true);
				 chromeOptions.setCapability("parentTunnel", "nhorvath");
				 chromeOptions.setCapability("tunnelIdentifier", "sauce-4444");
				 chromeOptions.setCapability("name", strSauceRun_Name);
				 caps = (Capabilities)chromeOptions;
				 driver = new RemoteWebDriver(new URL(strURL), caps);
				 break;
                 
			case "firefox_s" : 
				FirefoxOptions firefoxOptions = new FirefoxOptions();
	             firefoxOptions.setHeadless(false);
	             firefoxOptions.setCapability("platform", strOS);
	             firefoxOptions.setCapability("version", strVersion);
	             firefoxOptions.setCapability("parentTunnel", "nhorvath");
	             firefoxOptions.setCapability("tunnelIdentifier", "sauce-4444");
	             firefoxOptions.setCapability("name", strSauceRun_Name);
	             caps = (Capabilities)firefoxOptions;
	             driver = new RemoteWebDriver(new URL(strURL), caps);
	             break;
	             
			case "ie_s":
				DesiredCapabilities capIe = DesiredCapabilities.internetExplorer();
				capIe.setCapability("platform", strOS);
				capIe.setCapability("version", strVersion);
				capIe.setCapability("parentTunnel", "nhorvath");
				capIe.setCapability("tunnelIdentifier", "sauce-4444");
				capIe.setCapability("name", strSauceRun_Name);
				 driver = new RemoteWebDriver(new URL(strURL), capIe);
				 break;
				
			case "edge_s":
				DesiredCapabilities capEd = DesiredCapabilities.edge();
				capEd.setCapability("platform", strOS);
				capEd.setCapability("version", strVersion);
				capEd.setCapability("parentTunnel", "nhorvath");
				capEd.setCapability("tunnelIdentifier", "sauce-4444");
				capEd.setCapability("name", strSauceRun_Name);
				 driver = new RemoteWebDriver(new URL(strURL), capEd);
				 break;
				 
			case "safari_s":
				SafariOptions safariOptions = new SafariOptions();
				//DesiredCapabilities capps = DesiredCapabilities.safari();
				safariOptions.setCapability("platform", strOS);
				safariOptions.setCapability("version", strVersion);
				safariOptions.setCapability("parentTunnel", "nhorvath");
				safariOptions.setCapability("tunnelIdentifier", "sauce-4444");
				safariOptions.setCapability("name", strSauceRun_Name);
				 driver = new RemoteWebDriver(new URL(strURL), safariOptions);
				 break;
				 
			default: System.out.println("Incorrect browser type, default browser is local Chrome");	
				System.setProperty("webdriver.chrome.driver", "C://SVN_Automation_New//Trunk//Selenium_Automation//Selenium_Auto_Project//BrowserDrivers//chromedriver.exe");
				chromeOptions.addArguments("--start-maximized");
				driver = new ChromeDriver(chromeOptions);
				break;
			}//End of Switch Stmnt
			
		}//End of If
 		threadDriverMap.put(id, driver);
		return driver;
	}//End of Function
	
	
	//###################################################################################################################################################################  
	//Function name		: getDriver()
	//Class name		: ManageDriver
	//Description 		: Generic function to return the driver object based on Thread id hash map
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################				
		public WebDriver getDriver(){
			long id = Thread.currentThread().getId();
			driver = threadDriverMap.get(id);
			return driver;
		}
		
	//###################################################################################################################################################################  
	//Function name		: quit()
	//Class name		: ManageDriver
	//Description 		: Generic function to quit the driver based on thread id hash map.
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################				
		public void quit(){
			long id = Thread.currentThread().getId();
			System.out.println("HashMap Values in thread:"+threadDriverMap);
			driver = threadDriverMap.get(id);
			driver.quit();
			threadDriverMap.remove(id);
		}

}