package functionalLibrary.Global;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class DataTable {
	
	private Workbook excelWBook;
	private Sheet excelWSheet;	 
	private Cell cellObj;
	private Row rowObj;
	private HashMap<String,Integer> colList;
	public DataFormatter dataformat = new DataFormatter();
	
	
	
	CommonUtils commonUtils = new CommonUtils();
	//static String Maxit_excelRelativePath = "C:\\SVN_Automation_New\\Trunk\\Maxit_TestData\\";
	public String Maxit_excelRelativePath ;
	public String Maxit_excelAbsPath;
	//************************************************************************************************************
	//************************************************************************************************************
	//###################################################################################################################################################################  
	//Function name		: getInputFIlePath(String scriptName, String strEnvi, String strClient)
	//Class name		: configInputFile
	//Description 		: Generic function to configure input file dynamically during runtime based on scriptName
	//Parameters 		: ScriptName: Name of the script
	//						: Environment( QA/PROD)
	//						: Client name
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
		public String getInputFIlePath_Maxit(String scriptName, String strEnvi, String strClient){
			try{
			if(scriptName.trim().isEmpty()||strEnvi.trim().isEmpty()||strClient.trim().isEmpty()){
				System.out.println("<Class:configInputFile><Method:getInputFIlePath>	One/all of Input arguements are empty.Arugements(ScriptName:"+scriptName+";Environment:"+strEnvi+";ClientName:"+strClient+". Please Check.!");
				return null;
			}//End of IF to check if the parameters are empty
			
				switch(strEnvi.trim().toUpperCase()){
				case "QA":
					Maxit_excelAbsPath = System.getProperty("testDataPath") + "/QA_Site/"+strClient.trim()+"/"+strClient.trim().toLowerCase()+"_"+scriptName.trim()+".xls";				
					if(commonUtils.fileExists(Maxit_excelAbsPath)){
						System.out.println("Setting Input data Config File, Path - "+Maxit_excelAbsPath);
						return Maxit_excelAbsPath;
					}//Checking for .xls file
					
					//Maxit_excelAbsPath = Maxit_excelRelativePath+"QA_Site\\"+strClient.trim()+"\\"+strClient.trim().toLowerCase()+"_"+scriptName.trim()+".xlsx";
					Maxit_excelAbsPath = System.getProperty("testDataPath")+"/QA_Site/"+strClient.trim()+"/"+strClient.trim().toLowerCase()+"_"+scriptName.trim()+".xlsx";
					if(commonUtils.fileExists(Maxit_excelAbsPath)){
						System.out.println("Setting Input data Config File, Path - "+Maxit_excelAbsPath);
						return Maxit_excelAbsPath;
					}//Checking for .xlsx file				
					return null;			
				case "PROD":
					
					//Maxit_excelAbsPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\Production_Site\\"+strClient.trim().toUpperCase()+"_SmokeTest_Data.xls";
					//Maxit_excelAbsPath = "C:\\SVN_Automation_New\\Trunk\\Maxit_TestData\\Production_Site\\"+strClient.trim().toUpperCase()+"_SmokeTest_Data.xls";
					Maxit_excelAbsPath = System.getProperty("testDataPath")+"/Production_Site/"+strClient.trim().toUpperCase()+"_SmokeTest_Data.xls";
					
					//excelAbsPath = excelRelativePath+"Production_Site\\"+strClient.trim()+"\\"+"SmokeTest_Data.xls";
					if(commonUtils.fileExists(Maxit_excelAbsPath) ){
						System.out.println("Setting Input data Config File, Path - "+Maxit_excelAbsPath);
						return Maxit_excelAbsPath;
					}//Checking for .xls file
					
					//Maxit_excelAbsPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\Production_Site\\"+strClient.trim().toUpperCase()+"_SmokeTest_Data.xlsx";
					Maxit_excelAbsPath = System.getProperty("testDataPath")+"/Production_Site/"+strClient.trim().toUpperCase()+"_SmokeTest_Data.xlsx";
					if(commonUtils.fileExists(Maxit_excelAbsPath) ){
						System.out.println("Setting Input data Config File, Path - "+Maxit_excelAbsPath);
						return Maxit_excelAbsPath;
					}//Checking for .xlsx file				
					return null;
				default:
					System.out.println("<Class:configInputFile><Method:getInputFIlePath>	Error: strEnv argument passed is not QA/PROD, Please check..!");			
					return null;
				}//End of Switch case
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class:configInputFile><Method:getInputFIlePath> Exception Message" + e.getMessage() );
				return null;
			}

			
		}//End of getInputFIlePath method
	
	//######################################################################################################################
	//######################################################################################################################
    //Function name     : getExcelDatasheetPath()
    //Class name        : Reporting
    //Description       : This function returns the path of excel test data sheet.
    //Parameters        : Script Name, Environment and Client name
    //Assumption        : None
    //Developer         : Neelesh Vatsa
	//Product			: WM Specific
	//######################################################################################################################
	//######################################################################################################################
	public String getExcelDatasheetPath_WM(String scriptName, String strEnviron, String strClient) throws Exception{
		String strPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\WM_TestData\\" + strEnviron + "\\" + strClient.toUpperCase() +"\\" + scriptName;
		commonUtils.createFolder_NotExists(strPath);
		return strPath +"\\"+ scriptName + ".xlsx";
	}
	
	//######################################################################################################################
    //Function name     : getResultFilePath()
    //Class name        : Reporting
    //Description       : This function returns the path of excel result folder.
    //Parameters        : Script Name, Environment and Client name
    //Assumption        : None
    //Developer         : Neelesh Vatsa
	//Product			: WM Specific
	//######################################################################################################################
	//######################################################################################################################
	public String getResultFilePath_WM(String scriptName, String strEnviron, String strClient) throws Exception{
		String strPath = "C:\\SE_Reports\\WM\\" + strEnviron + "\\" + strClient +"\\" + scriptName;
		commonUtils.createFolder_NotExists(strPath);
		return strPath;
	}
		
//#####################################################################################################################################################
//Function name		: setExcelFile(String Path,String SheetName)
//Class name		: Datatable
//Description 		: This method is to set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method
//Parameters 		: Path - Excel workbook path
//					: SheetName - Sheet name	
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.setExcelFile("H:\\Kavitha\\Workspace\\Selenium_WM_Auto\\TestData\\DataSheets\\WM_Registration_Pos_Case.xlsx", "Global");
//#####################################################################################################################################################
public void setExcelFile(String Path,String SheetName) throws Exception {
		try {
			setExcelFile(Path);
			setSheet(SheetName);
		} catch (Exception e){
			excelWBook = null;
			excelWSheet = null;
			throw (e);
		}

} //End of "setExcelFile" method[Method overloading]

//#####################################################################################################################################################
//Function name		: setExcelFile(String Path)
//Class name		: Datatable
//Description 		: This method is to set the File path and to open the Excel file, Pass Excel Path as Arguments to this method
//Parameters 		: Path - Excel wokbook path
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.setExcelFile("H:\\Kavitha\\Workspace\\Selenium_WM_Auto\\TestData\\DataSheets\\WM_Registration_Pos_Case.xlsx");
//	NOTE: Difference between above & this method is arguments, method definition is different.
//Modified			: Neelesh Vatsa - Updated code with Workbook factory concept to support both file formats, replacing code to fetch file extension
//					  and then calling if-else to to create workbook for that specific format. 
//#####################################################################################################################################################
public void setExcelFile(String Path) throws Exception {
		try {
				// Open the Excel file
					/*FileInputStream ExcelFile = new FileInputStream(Path);
				    //Find the file extension by splitting file name in substring  and getting only extension name
				    String fileExtensionName = Path.substring(Path.indexOf("."));
			
				    //Check condition if the file is xlsx file
				    if(fileExtensionName.equals(".xlsx")){
				    	excelWBook = new XSSFWorkbook(ExcelFile);
				    }
			
				    //Check condition if the file is xls file
				    else if(fileExtensionName.equals(".xls")){
				    	excelWBook = new HSSFWorkbook(ExcelFile);
				    }*/
		    
		    InputStream fileInputStream = null;
		    fileInputStream = new FileInputStream(Path);
		    excelWBook = WorkbookFactory.create(fileInputStream);
			

		}//End of Try block
		catch (Exception e){
			excelWBook = null;
			throw (e);
		}

} //End of "setExcelFile" method[Method overloading]

//################################################################################################################################################################### 
//Function name		: setSheet(String SheetName)
//Class name		: Datatable
//Description 		: This method to set Sheet from the workbook
//Parameters 		: SheetName - Sheet name
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.setSheet("Global");
//################################################################################################################################################################### 
public boolean setSheet(String SheetName) {
	excelWSheet = null;
	try {
		int noOfSheets = excelWBook.getNumberOfSheets();
		for(int i=0;i<noOfSheets ; i++){
			if(excelWBook.getSheetName(i).equalsIgnoreCase(SheetName)){
				excelWSheet = excelWBook.getSheet(SheetName);
				setColumnList(excelWSheet);
				return true;		
			}			
		}
			System.out.println("<Class:DataTable><Method: setSheet>: Given Sheet Name:"+SheetName+" does not exists, please check");
			excelWSheet = null;
			return false;
	}//End of try block
	catch (Exception e){
		System.out.println("<Class:DataTable><Method: setSheet>: Given Sheet Name:"+SheetName+" does not exists, please check");
		excelWSheet = null;
		e.printStackTrace();
		return false;
	}//End of Catch block

} //End of "setSheet" method

//###################################################################################################################################################################  
//Function name		: setCellData(String Result,  int RowNum, int ColNum)
//Class name		: Datatable
//Description 		: This method is to write in the Excel cell, Row num and Col num are the parameters
//Parameters 		: Result - Value to be written to Excelsheet
//					  RowNum - Row Number
//					  ColNum - column Number
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.setCellData("Pass",2,10);
//################################################################################################################################################################### 
@SuppressWarnings({"deprecation", "static-access" })
public void setCellData(String strCellValue,  int RowNum, int ColNum,String strOutputPath,String strFileName) throws Exception	{

		try{
			rowObj  = excelWSheet.getRow(RowNum);
			cellObj = rowObj.getCell(ColNum, rowObj.RETURN_BLANK_AS_NULL);
			if (cellObj == null) {	
				cellObj = rowObj.createCell(ColNum);	
				cellObj.setCellValue(strCellValue);	
				} 
			else {
					cellObj.setCellValue(strCellValue);
				}
				// Constant variables Test Data path and Test Data file name
				FileOutputStream fileOut = new FileOutputStream(strOutputPath + strFileName);				
				excelWBook.write(fileOut);
				//fileOut.flush();
				fileOut.close();
			}catch(Exception e){
				throw (e);
			}

}// End of setCellData method[Method overloading]
//###################################################################################################################################################################  
//Function name		: setCellData(String Result,  int RowNum, String ColName)
//Class name		: Datatable
//Description 		: This method is to write in the Excel cell, Row num and Col Name are the parameters
//Parameters 		: Result - Value to be written to Excelsheet
//					  RowNum - Row Number
//					  ColName - column Name				  
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.setCellData("Pass",2,10);
//################################################################################################################################################################### 
@SuppressWarnings({ "static-access", "deprecation" })
public void setCellData(String strResult,  int RowNum, String strColName) throws Exception	{

		try{
			int colIndex = getColumnIndex(strColName);
			if(colIndex==-1){
				System.out.println("<METHOD: setCellData><CLASS: DataTable> - There is no column in DataSheet with columnName : ["+ strColName +"]");
			}//End of IF condition to check colIndex
			rowObj  = excelWSheet.getRow(RowNum);
			cellObj = rowObj.getCell(colIndex, rowObj.RETURN_BLANK_AS_NULL);
				
			if (cellObj == null) {	
				cellObj = rowObj.createCell(colIndex);	
				cellObj.setCellValue(strResult);	
				} 
			else {
					cellObj.setCellValue(strResult);
				}
				// Constant variables Test Data path and Test Data file name
				FileOutputStream fileOut = new FileOutputStream("C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_WM_Auto\\TestData\\DataSheets\\Output_Results\\" + "TestResults.xlsx");
				//FileOutputStream fileOut = new FileOutputStream(util.getGlobalProperty("dtRegistration_PosCase_Output") + "TestResults.xlsx");
				excelWBook.write(fileOut);
				//fileOut.flush();
				fileOut.close();
			}catch(Exception e){

				throw (e);
			}

}// End of setCellData method[Method overloading]

//################################################################################################################################################################### 
//Function name		: getRowCount() 
//Class name		: Datatable
//Description 		: This function returns the total number of used rows in datasheet 
//Parameters 		: N/A
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.getRowCount();
//################################################################################################################################################################### 
public int getRowCount(){
	try {
	return excelWSheet.getLastRowNum();
	}
	
	catch(Exception e){
		System.out.println("There is an exception in <Class: Datatable><Method: getRowCount>, please check..!!");		
		e.printStackTrace();
		return 0;
		} // End of Catch block
	
	} //End of "getRowCount" method


//###################################################################################################################################################################  
//Function name		: getCellData(int RowNum, int ColNum)
//Class name		: Datatable
//Description 		: This method is to read the test data from the Excel cell, in this we are passing parameters as Row num and Col num (starts with 0)
//Parameters 		: RowNum - Row Number
//					  ColNum - column Number
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.getCellData(2,10);
//################################################################################################################################################################### 
public String getCellData(int RowNum, int ColNum) throws Exception{
		try{
			cellObj = excelWSheet.getRow(RowNum).getCell(ColNum);
			//String CellData = cellObj.getStringCellValue();
			String CellData = dataformat.formatCellValue(cellObj);
			return CellData;
			}
		catch (Exception e){
			return"";
			}
}//End of "getCellData" method[Method overloading]

//###################################################################################################################################################################  
//Function name		: getCellData(int intCellRow,String strColName) 
//Class name		: Datatable
//Description 		: This function returns the cell value by column_name from datasheet for given row/col_Name 
//Parameters 		: intCellRow : is the row number
//					  strColName : Column Name
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.getCellData(1,"To_Execute") 
//################################################################################################################################################################### 
public String getCellData(int intCellRow,String strColName) throws Exception{
	//System.out.println("Inside getCellData method");
	int colIndex = getColumnIndex(strColName);
	if(colIndex==-1){
		System.out.println("<METHOD: getCellData><CLASS: DataTable> - There is no column in DataSheet with columnName : ["+ strColName +"]");
		return "Column not found";
	}//End of IF condition to check colIndex
	
	cellObj = excelWSheet.getRow(intCellRow).getCell(colIndex);
	try{
		//String ColumnValue = cellObj.getStringCellValue().toString();
		String ColumnValue = dataformat.formatCellValue(cellObj);
		return ColumnValue;		
	}catch (NullPointerException e) {
		return "";
	}
		
}//End of getCellData method[Method overloading]

//###################################################################################################################################################################  
//Function name		: getCellData(int intCellRow,String strColName,String strSheetName) 
//Class name		: Datatable
//Description 		: This function returns the cell value by column_name from datasheet for given row/col_Name 
//Parameters 		: intCellRow : is the row number
//					  strColName : Column Name
//					  strSheetName : Sheet Name
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: Datatable.getCellData(1,"To_Execute","Global") 
//################################################################################################################################################################### 
public String getCellData(int intCellRow,String strColName,String strSheetName) throws Exception{
	excelWSheet = excelWBook.getSheet(strSheetName);
	//System.out.println("Inside value method");
	int colIndex = getColumnIndex(strColName);
	if(colIndex==-1){
		System.out.println("<METHOD: getCellData><CLASS: DataTable> - There is no column in DataSheet with columnName : ["+ strColName +"]");
		return "Column not found";
	}//End of IF condition to check colIndex
	
	cellObj = excelWSheet.getRow(intCellRow).getCell(colIndex);
	try{
		//String ColumnValue = cellObj.getStringCellValue();
		String ColumnValue = dataformat.formatCellValue(cellObj);
		return ColumnValue;		
	}catch (NullPointerException e) {
		return "";
	}
	
}//End of "getCellData" method[Method overloading]

//###################################################################################################################################################################  
//Function name		: getColumnIndex(String strColName)
//Class name		: Datatable
//Description 		: This function returns the column index of the given column_name
//Parameters 		: strColName : Column Name
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: getColumnIndex("To_Execute")
//################################################################################################################################################################### 
public Integer getColumnIndex(String strColName) throws Exception{
	try{
		int ret = colList.get(strColName);		
		return ret;
	}
	catch(Exception e){
		return -1;
	}
	
}//End of "getColumnIndex" method

//###################################################################################################################################################################  
//Function name		: setColumnList(XSSFSheet sheetObj)
//Class name		: Datatable
//Description 		: This function initializes ColList map (ColName;ColIndex) pair
//Parameters 		: N/A
//Assumption		: None
//Developer			: Kavitha Golla
//Example			: setColumnList(sheetObj)
//################################################################################################################################################################### 
private void setColumnList(Sheet sheetObj) throws Exception{

	if(sheetObj!=null){
		colList=null;
		colList=new HashMap<String,Integer>();
		Row r = sheetObj.getRow(0);			
		for (int cn=0; cn < r.getLastCellNum(); cn++) {
			colList.put(r.getCell(cn).getStringCellValue(), cn);		
		}//End of for loop
	
	}//End of If condition to check"sheetObj==null"
}//End of "setColumnList" method

//#####################################################################################################################################################
//Function name		: getExcelFile(String Path)
//Class name		: Datatable
//Description 		: This method is to get the File path and return the excel workbook
//Parameters 		: Path - Excel wokbook path
//Assumption		: None
//Developer			: Kavitha Golla and Neelesh Vatsa
//Example			: Datatable.setExcelFile("H:\\Kavitha\\Workspace\\Selenium_WM_Auto\\TestData\\DataSheets\\WM_Registration_Pos_Case.xlsx");
//Modified			: Updated this function using Workbook factory to support both xls and xlsx file format dynamically. This replaces the need to 
//					  fetch file extension and applying if-else loop to return specific file type format.
//Modified			: Neelesh - Updated this function to check for file path existence else return null
//#####################################################################################################################################################
public Workbook getExcelFile(String Path) throws  InvalidFormatException, IOException {

			if(Path.isEmpty()||Path==""||Path==null){	
				System.out.println("<DataTable.getExcelFile>: Excel Path paramter is null,please check..!!");
				return null;
			}
			//check if file exists
			File f = new File(Path);
			if (f.exists()){
				InputStream outputStream = null;
				outputStream = new FileInputStream(Path);
				Workbook resultWorkBook = WorkbookFactory.create(outputStream);
				return resultWorkBook;
			}
			//else return null
			return null;
		}//End of "getExcelFile" method


//##########################################################################################################################################################################
//Function name     : setExcelFileFormat(String Path)
//Class name        : Datatable
//Description       : This method will format the result file to make results fit in cell and easy to read.
//Parameters        : Path - Excel workbook path
//Assumption        : None
//Developer         : Neelesh Vatsa
// Notes - WIP. It works as intended so far. Will add border to cells if required n 
//format only the result related cells not the test data cells. Also run a loop for all sheets
//Modified			: Kavitha - replaced method "getExcelWorkBook" with "getExcelFile", as both methods does same thing, we merged the code into single method.
//##########################################################################################################################################################################
public void setExcelFileFormat(String Path) throws InvalidFormatException, IOException {
	if(Path.isEmpty()||Path==""||Path==null){ 
    System.out.println("<DataTable.setExcelFileFormat>: Excel Path paramter is null,please check..!!");
}
	Workbook excelWorkBook = getExcelFile(Path);
	Sheet resultSheet = excelWorkBook.getSheetAt(0);
	//get sheet count
	 int sheetCount = excelWorkBook.getNumberOfSheets();
	//get row n col count
	 int noOfColumns = resultSheet.getRow(0).getPhysicalNumberOfCells();
	 int rowCount =  resultSheet.getPhysicalNumberOfRows();
	 System.out.println("row = " + rowCount);
	 System.out.println("col = " + noOfColumns);
	//loop to format each cell in each sheet or last 4 columns
	//Sheet resultSheet = strCurrentRow.getSheet();
    CellStyle cs = excelWorkBook.createCellStyle();
    cs.setWrapText(true);
    for(int i = 0; i<=rowCount-1; i++){
    	System.out.println("at Row - " + i);
    	for(int j = 0; j<=noOfColumns-1; j++){
    		System.out.println("at col - " + j);
    		Cell cellContent = resultSheet.getRow(i).getCell(j);
    		if(cellContent!= null){
    			 resultSheet.getRow(i).getCell(j).setCellStyle(cs);
    			    //adjust column width to fit the content
    			    resultSheet.autoSizeColumn(j);
    		}  
    	}
    	 //auto adjust the height of cell to accommodate multi line details
    	
	    //Row row = resultSheet.getRow(i);
	    //row.setHeight((short)-1);
    }
    FileOutputStream outFile1 =new FileOutputStream(new File(Path));
    excelWorkBook.write(outFile1); 
    outFile1.flush();
    outFile1.close();
    excelWorkBook.close();
  }
//End of "setExcelFileFormat" method


//############################ ***** End of Methods for class "Datatable" ******###############################################
}//End of Class	
//############################ ********************** END **********************###############################################



