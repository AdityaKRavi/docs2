package functionalLibrary.Global;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReporting{
	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public static ExtentTest test, suiteTest;
	public String testCaseName, testNodes, testDescription;

	CommonUtils commonUtils = new CommonUtils();
	ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
	//Before suite in basetest
	public void startReport() {
		String productName = context.getCurrentXmlTest().getParameter("productName");
		if(productName.equalsIgnoreCase("Maxit")) 
			html = new ExtentHtmlReporter("./Reports/Maxit_Reports/extentreport.html");
		else
			html = new ExtentHtmlReporter("./Reports/WM_Reports/extentreport.html");
		
		html.setAppendExisting(true);		
		extent = new ExtentReports();		
		extent.attachReporter(html);	
	}

	//Before class in Basetest
	public ExtentTest startTestModule(String testCaseName, String testDescription) {
		suiteTest = extent.createTest(testCaseName, testDescription);
		return suiteTest;
	}

	//Before method in Basetest
	public ExtentTest startTestCase(String testNodes) {
		test = 	suiteTest.createNode(testNodes);
		return test;
	}

		public void endResult() {
			extent.flush();
		}	
	}

