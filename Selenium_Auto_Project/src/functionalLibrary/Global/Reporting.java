package functionalLibrary.Global;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reporting {

	DataTable dt = new DataTable();
	CommonUtils commonUtils = new CommonUtils();


	//############################################################################################################################################################################################################################################################  
	//Function name     : reportStep()
	//Class name        : Reporting
	//Description       : This function write result in Excel report file along withthe links for screenshots.
	//Parameters        :Row number of test data
	//                  :Passed or Failed value
	//                  :Details about the step
	//                  :screenshot path,if null then this function will call capture screenshot method
	//                  :name of the step to be used as img file name
	//                  :WebDriver
	//                  :Scroll Strategy - None, Vertical, Horizontal or Both
	//                  :Name of the excel sheet to be used for writting results
	//                  :Element on the screen to be highlighted in screenshot
	//Assumption        :None
	//Developer         : Neelesh Vatsa
	//Modified          : Kavitha - added support for both xls & xlsx file formats by using "Workbook interface". 
	//					: Replaced all objects definition to generic "Workbook/Sheet/Row/Cell" objects instead of support to "XSSFWorkbook"(.xlsx) format only.
	//					: Removed "Static" declaration of methods, to leverage DataTable.class methods, defined an object for DataTable class to access all its methods under "Reporting.class"
	//					: Removed 	code for creatting "Workbook" object like "resultWorkBook" within "reportStep" method, coded a generic method and added to DataTable.class "getExcelFile" method.
	//Modified			: Neelesh - Created new function to get excel workbook of any format to enable support for xlsx as well as xls as opposed to only xlsx format based on initial earlier requirement.
	//Modified			: Neelesh - New function call automatically removes code for workbook creation from this function along with dynamic support for different excel file format.
	//############################################################################################################################################################################################################################################################   

	/* public void reportStep(int rowNum, String stepPassFailVal, String stepDetails, String scrnShotPath, String stepName, WebDriver driver,  String scrollStrategy, String sheetName, WebElement objElement){
    	try{

		    	ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		        //Setting global variable value for All Pass Status
		        if(stepPassFailVal.equals("Failed")){
		        	context.setAttribute("resultStatus_AllPassed", "False");
		        }
		        //1. Screenshot Step
		        //Logic - If "scrnShotPath" parameter is empty then take a screenshot calling the default screenshot method. But if 
		        // >> it has some value then use that path. Scenario where we want to add more details to screenshot using shutterbug tool
		        // >> should be called before the report method n path should be passed.
		        String imgPath = "";
		        if(scrnShotPath == "" && stepPassFailVal.equals("Failed")){
		            //take screenshot with basic details
		            imgPath = commonUtils.captureScreenshot(driver, stepName, stepDetails, scrollStrategy, objElement);
		        }else if(!(scrnShotPath == "") && stepPassFailVal.equals("Failed")){
		            imgPath = scrnShotPath;
		        }

		        //2. Writing to result Excel file step
		        // Get workbook
		        String filePath = (String) context.getAttribute("resultPath");
		        //System.out.println("function: reportStep >> Result file path in report step = " + filePath);             
		        Workbook resultWorkBook = dt.getExcelFile(filePath);
		        if(resultWorkBook.equals(null)){
		            System.out.println("function: reportStep >> Result file path in report step is null");
		            throw new SkipException("Skip Test - File path is null.");    
		        }
		        //Get sheet
		        Sheet resultSheet;
		        if(sheetName.equals("")){
		            resultSheet= resultWorkBook.getSheetAt(0);
		        }else{
		            int sheetIndex = resultWorkBook.getSheetIndex(sheetName);
		            resultSheet= resultWorkBook.getSheetAt(sheetIndex);
		        }
		        //get col num for 4 reporting columns that are at the end of result table
		        Row rowCurrent = resultSheet.getRow(rowNum);
		        int lastColNum = resultSheet.getRow(0).getPhysicalNumberOfCells();
		        //last col no: is for screenshot,then Failed details, Passed details and last Passed/Failed
		        int colPassFail = lastColNum - 4;
		        int colPassedDetails = lastColNum - 3;
		        int colFailedDetails = lastColNum - 2;
		        int colScreenshot = lastColNum - 1;
		        //check if details cell is null which means no failed step has been added so far.
		        // >> Add space to use it as string while concatenating the first failed result.
		        Cell cellFailedDetails =resultSheet.getRow(rowNum).getCell(colFailedDetails);
		        Cell cellPassedDetails =resultSheet.getRow(rowNum).getCell(colPassedDetails);
		        //Cell cellScrnShot =resultSheet.getRow(rowNum).getCell(colScreenshot);

		        if(cellFailedDetails == null){
		               // add space in empty cell 
		             resultSheet.getRow(rowNum).createCell(colFailedDetails).setCellValue(" ");

		             resultSheet.getRow(rowNum).createCell(colScreenshot).setCellValue(" ");
		        }
		        if(cellPassedDetails == null){
		             resultSheet.getRow(rowNum).createCell(colPassedDetails).setCellValue(" ");
		        }

		         // 1) Set Passed and failed value
		         //Logic - if its failed thn set fail in result file else if its passed then set to passed only if the existing screenshot 
		         //cell value is null which means some step has failed in past.
		        String screenshotCellData = resultSheet.getRow(rowNum).getCell(colScreenshot).getStringCellValue();
		         if(stepPassFailVal.equalsIgnoreCase("Failed")){
		             resultSheet.getRow(rowNum).createCell(colPassFail).setCellValue(stepPassFailVal);
		         }else if(stepPassFailVal.equalsIgnoreCase("Passed")){ 
		             if(screenshotCellData.trim().equals("")){
		                 resultSheet.getRow(rowNum).createCell(colPassFail).setCellValue(stepPassFailVal);   
		             } 
		         }else if(stepPassFailVal.equalsIgnoreCase("Not Executed")|| (stepPassFailVal.equalsIgnoreCase("N/A"))){
		        	 resultSheet.getRow(rowNum).createCell(colPassFail).setCellValue(stepPassFailVal);
		         }
		         //add passed details if step has passed 

		         if(stepPassFailVal.equals("Passed")){
		                String cellPasseddata = resultSheet.getRow(rowNum).getCell(colPassedDetails).getStringCellValue();
		                String finalPassData = (cellPasseddata + "\n" + stepDetails).trim();
		                resultSheet.getRow(rowNum).createCell(colPassedDetails).setCellValue(finalPassData);
		                setResultFileFormat(rowCurrent, rowNum, colPassedDetails);
		         }else if(stepPassFailVal.equals("Failed")){

		             // 2) Set Details cell value
		             // ----- Combine content for failure detail and screenshot
		            String cellFaileddata = resultSheet.getRow(rowNum).getCell(colFailedDetails).getStringCellValue();
		            String finalData = (cellFaileddata + "\n" + stepDetails).trim();
		            resultSheet.getRow(rowNum).createCell(colFailedDetails).setCellValue(finalData);

		            // 3) Set value for screenshot
		            String finalImgData = (screenshotCellData + "\n" + imgPath).trim();
		            resultSheet.getRow(rowNum).createCell(colScreenshot).setCellValue(finalImgData);

		            // 4) Add hyperlinked text for img path
		            String screenshotCellDataNew = (resultSheet.getRow(rowNum).getCell(colScreenshot).getStringCellValue()).trim();
		            String[] cellImgArray = screenshotCellDataNew.split("[\\r\\n]+");
		            int arrSize = cellImgArray.length;
		            if(arrSize>=1){
		                Cell cell;
		                cell = resultSheet.getRow(rowNum).createCell((short) (colScreenshot+arrSize));
		                cell.setCellValue("Check screenshot" + (arrSize));
		                setResultFileFormat(rowCurrent, rowNum, colScreenshot+arrSize);
		                String imgPathFromArray = cellImgArray[arrSize-1];
		                //Function call to set hyperlinks for screenshots
		                setHyperlinkScreenShot(cell, imgPathFromArray); 
		            }
		             //Formatting Results related cell
		             setResultFileFormat(rowCurrent, rowNum, colFailedDetails);
		             setResultFileFormat(rowCurrent, rowNum, colScreenshot);
		         }
		         //save file
		        FileOutputStream outFile1 =new FileOutputStream(new File(filePath));
		        resultWorkBook.write(outFile1); 
		        outFile1.flush();
		        outFile1.close();
		        resultWorkBook.close();
	       }catch(Exception e){
	    	   System.out.println("Exception found in Reporting class ->>  reportStep method");
	    	   e.printStackTrace();}//End of try Catch
        }
	 */   
	//###################################################################################################################################################################  
	//Function name     : reportCreateResultFile()
	//Class name        : Reporting
	//Description       : This function crates an excel result file by copying test data file to given reports folder location.
	//Parameters        : All the parameters are used to find and create excel file path.
	//                  : Name of the script to be used to fetch the file and place it in result folder with appended timestamp.
	//                  : Environment - qa, integration
	//                  : Client name
	//                  : Product name
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//Modified          : Kavitha: Replaced all objects definition to generic "Workbook/Sheet/Row/Cell" objects instead of support to "XSSFWorkbook"(.xlsx) format only.
	//    				: Added logic to read InputFilePath based on product name[Maxit & WM]
	//M2odified			: Neelesh - created new function that handles both .xlsx and .xls file format dynamically as opposed to the initial requirement known, of xlsx file only.
	//###################################################################################################################################################################   

	public String reportCreateResultFile(String strScriptname, String strEnvi, String strClient, String productName) throws Exception {
		String xlTestDataPath;
		String resultFilePath;

		//Kavitha: AUTO-421 : Added logic to restrict Maxit scripts to read Reports path from testNG.xml base path.
		if(productName.equalsIgnoreCase("Maxit")){
			if(System.getProperty("resBasePath").toUpperCase().startsWith("S:")){
				resultFilePath = (System.getProperty("resBasePath") + strClient +  "/" + strScriptname);
			}
			else
				resultFilePath = (System.getProperty("resBasePath") + productName + "/" + strEnvi +"/" + strClient +  "/" + strScriptname);
		}
		else{
			resultFilePath = ("C:/SE_Reports/" + productName + "/" + strEnvi +"/" + strClient +  "/" + strScriptname);
		}


		//Create the report folder for script if it does not exist
		commonUtils.createFolder_NotExists(resultFilePath);
		//check if u can save this file in new location
		if(productName.equalsIgnoreCase("Maxit")){
			xlTestDataPath = dt.getInputFIlePath_Maxit(strScriptname, strEnvi, strClient);
		}
		else{
			xlTestDataPath = dt.getExcelDatasheetPath_WM(strScriptname, strEnvi, strClient);
		}
		//Get workbook from above path
		Workbook wb = dt.getExcelFile(xlTestDataPath);
		if(wb==null){
			//return error if the path is empty here
			commonUtils.create_TextResultFile(resultFilePath, "Test Data sheet does not exists for this script - " + strScriptname);
			System.out.println("function: reportStep >> Result file path in report step is null");
			throw new SkipException("Skip Test - File path is null.");    
		}
		//create file name and extension based on product name
		String fileExtensionName = xlTestDataPath.substring(xlTestDataPath.indexOf("."));
		Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
		Date date = new Date();
		String fileName = strClient + "_" + strScriptname + "Results_" + (formatter.format(date)) +fileExtensionName;

		resultFilePath = (resultFilePath + "/" + fileName);

		FileOutputStream outFile =new FileOutputStream(new File(resultFilePath));
		wb.write(outFile);  
		outFile.flush();
		outFile.close();
		wb.close();


		//Open newly created result excel file and format it.
		Workbook outWB = dt.getExcelFile(xlTestDataPath);
		// get the sheet which you want to modify or create
		int sheetCount = outWB.getNumberOfSheets();
		//Add result col in all sheets
		for(int i = 0; i<=sheetCount-1; i++){
			Sheet sh1= outWB.getSheetAt(i);
			//Create a new font and alter it.
			Font font = outWB.createFont();
			font.setFontHeightInPoints((short) 10);
			font.setBold(true);
			font.setColor(IndexedColors.BLACK.getIndex());
			//Set font into style
			CellStyle style = outWB.createCellStyle();
			style.setFont(font);
			int noOfColumns = sh1.getRow(0).getPhysicalNumberOfCells();
			Cell cellResult = sh1.getRow(0).createCell(noOfColumns);
			Cell cellPassedDetail = sh1.getRow(0).createCell(noOfColumns + 1);
			Cell cellFailedDetail = sh1.getRow(0).createCell(noOfColumns + 2);
			Cell cellScreenshot = sh1.getRow(0).createCell(noOfColumns + 3);
			sh1.autoSizeColumn(noOfColumns + 2);
			cellResult.setCellValue("Results");
			cellResult.setCellStyle(style);

			cellPassedDetail.setCellValue("Passed Details");
			cellPassedDetail.setCellStyle(style);

			cellFailedDetail.setCellValue("Failure Details");
			cellFailedDetail.setCellStyle(style);

			cellScreenshot.setCellValue("ScreenshotPath");
			cellScreenshot.setCellStyle(style);
		}

		FileOutputStream fout=new FileOutputStream(resultFilePath);
		outWB.write(fout);
		fout.close();
		outWB.close();

		System.out.println("Method - reportCreateResultFile >> Intial Result File copy has been created in local directory");
		System.out.println("Result File Name -" + fileName);

		return resultFilePath;
	}
	//###################################################################################################################################################################  
	//Function name     : setHyperlinkScreenShot()
	//Class name        : Reporting
	//Description       : This function create File type hyperlink in Excel cell, mainly for screenshots in result file.
	//Parameters        : Excel Cell and Address of file to be hyperlinked
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//Modified          : Kavitha : Replaced all objects definition to generic "Workbook/Sheet/Row/Cell" objects instead of support to "XSSFWorkbook"(.xlsx) format only.
	//Modified			: Neelesh - created new function that handles both .xlsx and .xls file format dynamically as opposed to the initial requirement known, of xlsx file only.
	//###################################################################################################################################################################   
	/*  public void setHyperlinkScreenShot(Cell cell, String FileAddress){
        Workbook resultWorkBook=cell.getRow().getSheet().getWorkbook();
        CreationHelper createHelper = resultWorkBook.getCreationHelper();
        CellStyle hlink_style = resultWorkBook.createCellStyle();
        Font hlink_font = resultWorkBook.createFont();
        hlink_font.setUnderline(Font.U_SINGLE);
        hlink_font.setColor(IndexedColors.BLUE.getIndex());
        hlink_style.setFont(hlink_font);
        Hyperlink link = createHelper.createHyperlink(HyperlinkType.FILE);
        link.setAddress(FileAddress);
        cell.setHyperlink(link);
        cell.setCellStyle(hlink_style);

    }
    //###################################################################################################################################################################  
    //Function name     : setResultFileFormat()
    //Class name        : Reporting
    //Description       : This function sets the file format of excel file so that the text fits in visible area.
    //Parameters        : Current row, row num and col num
    //Assumption        : None
    //Developer         : Neelesh Vatsa
    //Modified          : Kavitha: Replaced all objects definition to generic "Workbook/Sheet/Row/Cell" objects instead of support to "XSSFWorkbook"(.xlsx) format only.
    //Modified			: Neelesh - created new function that handles both .xlsx and .xls file format dynamically as opposed to the initial requirement known, of xlsx file only.
    //###################################################################################################################################################################   
    public void setResultFileFormat(Row strCurrentRow, int rowNo, int colNum){
        //Formatting cell
        Workbook resultWorkBook=strCurrentRow.getSheet().getWorkbook();
        Sheet resultSheet = strCurrentRow.getSheet();
        CellStyle cs = resultWorkBook.createCellStyle();
        cs.setWrapText(true);
        resultSheet.getRow(rowNo).getCell(colNum).setCellStyle(cs);
        //adjust column width to fit the content
        resultSheet.autoSizeColumn(colNum);
        //auto adjust the height of cell to accommodate multi line details
        strCurrentRow.setHeight((short)-1);

    }       

    //###################################################################################################################################################################  
    //Function name     : reportCreateResultFile_Maxit_QuickSMoke(String strScriptname, String productName)
    //Class name        : Reporting
    //Description       : This function [Is used for creating a report for Quick Smoke Test for Ops team on maxit]creates an excel result file by copying test data file to given reports folder location.
    //Parameters        : All the parameters are used to find and create excel file path.
    //                  : Name of the script to be used to fetch the file and place it in result folder with appended timestamp.
    //                  : Product name
    //Assumption        : None
    //Developer         : Kavitha Golla
    //###################################################################################################################################################################   

	 */ public String reportCreateResultFile_Maxit_QuickSMoke(String strScriptname, String productName, String strEnvironment) throws Exception {
		 String xlTestDataPath;
		 String resultFilePath ;//= ("C:/SE_Reports/" + productName + "/OpsTeam/QuickSmokeTest");
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");        
		 Date Ops_folderDate = new Date();
		 String Ops_ResFOlder =  sdf.format(Ops_folderDate);

		 if(productName.equalsIgnoreCase("Maxit") && strScriptname.equalsIgnoreCase("QuickSmoke")){
			 if(!(System.getProperty("resBasePath").toUpperCase().contains("OpsTeam_Reports"))){
				 resultFilePath = (System.getProperty("resBasePath") + "/"+productName + "/"+strScriptname+"/"+Ops_ResFOlder);
			 }
			 else
				 resultFilePath = (System.getProperty("resBasePath")+"/"+Ops_ResFOlder);
		 }
		 else{
			 resultFilePath = ("C:/SE_Reports/" + productName + "/OpsTeam/" + strScriptname);
		 }

		 //Create the report folder for script if it does not exist
		 commonUtils.createFolder_NotExists(resultFilePath);
		 //check if u can save this file in new location
		 //xlTestDataPath = "C:\\SVN_Automation_New\\Trunk\\Maxit_TestData\\Ops\\Quick_SmokeTest.xlsx";
		 xlTestDataPath = System.getProperty("testDataPath") + "/Maxit_Login/Sites.xlsx";
		 System.out.println("xlTestDataPath is:"+xlTestDataPath);
		 //Get workbook from above path
		 Workbook wb = dt.getExcelFile(xlTestDataPath);
		 if(wb==null){
			 //return error if the path is empty here
			 commonUtils.create_TextResultFile(resultFilePath, "Test Data sheet does not exists for this script - " + strScriptname);
			 System.out.println("function: reportStep >> Result file path in report step is null");
			 throw new SkipException("Skip Test - File path is null.");    
		 }
		 //create file name and extension based on product name
		 String fileExtensionName = xlTestDataPath.substring(xlTestDataPath.indexOf("."));
		 Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
		 Date date = new Date();
		 String fileName = strEnvironment+"_"+strScriptname + "_Res_" + (formatter.format(date)) +fileExtensionName;

		 resultFilePath = (resultFilePath + "/" + fileName);

		 FileOutputStream outFile =new FileOutputStream(new File(resultFilePath));
		 wb.write(outFile);  
		 outFile.flush();
		 outFile.close();
		 wb.close();

		 //Open newly created result excel file and format it.
		 Workbook outWB = dt.getExcelFile(xlTestDataPath);


		 //Logic to delete other sheets from Maxit report:
		 switch (strEnvironment.toUpperCase()){
		 case "PROD":
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("UAT")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("PREUAT")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("QA")));
			 break;
		 case "UAT":
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("PROD")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("PREUAT")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("QA")));
			 break;
		 case "PREUAT":
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("PROD")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("UAT")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("QA")));
			 break;

		 case "QA":
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("PROD")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("UAT")));
			 outWB.removeSheetAt(outWB.getSheetIndex(outWB.getSheet("PREUAT")));
			 break;
		 }

		 // get the sheet which you want to modify or create
		 int sheetCount = outWB.getNumberOfSheets();
		 //Add result col in all sheets
		 for(int i = 0; i<=sheetCount-1; i++){
			 Sheet sh1= outWB.getSheetAt(i);
			 //Create a new font and alter it.
			 Font font = outWB.createFont();
			 font.setFontHeightInPoints((short) 10);
			 font.setBold(true);
			 font.setColor(IndexedColors.BLACK.getIndex());
			 //Set font into style
			 CellStyle style = outWB.createCellStyle();
			 style.setFont(font);
			 int noOfColumns = sh1.getRow(0).getPhysicalNumberOfCells();
			 Cell cellResult = sh1.getRow(0).createCell(noOfColumns);
			 Cell cellPassedDetail = sh1.getRow(0).createCell(noOfColumns + 1);
			 Cell cellFailedDetail = sh1.getRow(0).createCell(noOfColumns + 2);
			 Cell cellScreenshot = sh1.getRow(0).createCell(noOfColumns + 3);
			 sh1.autoSizeColumn(noOfColumns + 2);
			 cellResult.setCellValue("Results");
			 cellResult.setCellStyle(style);

			 cellPassedDetail.setCellValue("Passed Details");
			 cellPassedDetail.setCellStyle(style);

			 cellFailedDetail.setCellValue("Failure Details");
			 cellFailedDetail.setCellStyle(style);

			 cellScreenshot.setCellValue("ScreenshotPath");
			 cellScreenshot.setCellStyle(style);
		 }

		 FileOutputStream fout=new FileOutputStream(resultFilePath);
		 outWB.write(fout);
		 fout.close();
		 outWB.close();

		 System.out.println("Method - reportCreateResultFile >> Intial Result File copy has been created in local directory");
		 System.out.println("Result File Name -" + fileName);

		 return resultFilePath;
	 }//End of <Method: reportCreateResultFile> a method overload for creating report for Ops team Quick Smoke Test>



	 //############################################################################################################################################################################################################################################################  
	 //Function name     : reportStep()
	 //Class name        : Reporting
	 //Description       : This function write result in Excel report file along withthe links for screenshots.
	 //Parameters        :Row number of test data
	 //                  :Passed or Failed value
	 //                  :Details about the step
	 //                  :screenshot path,if null then this function will call capture screenshot method
	 //                  :name of the step to be used as img file name
	 //                  :WebDriver
	 //                  :Scroll Strategy - None, Vertical, Horizontal or Both
	 //                  :Name of the excel sheet to be used for writting results
	 //                  :Element on the screen to be highlighted in screenshot
	 //Assumption        :None
	 //Developer         : Neelesh Vatsa
	 //Modified          : Kavitha - added support for both xls & xlsx file formats by using "Workbook interface". 
	 //					: Replaced all objects definition to generic "Workbook/Sheet/Row/Cell" objects instead of support to "XSSFWorkbook"(.xlsx) format only.
	 //					: Removed "Static" declaration of methods, to leverage DataTable.class methods, defined an object for DataTable class to access all its methods under "Reporting.class"
	 //					: Removed 	code for creatting "Workbook" object like "resultWorkBook" within "reportStep" method, coded a generic method and added to DataTable.class "getExcelFile" method.
	 //Modified			: Neelesh - Created new function to get excel workbook of any format to enable support for xlsx as well as xls as opposed to only xlsx format based on initial earlier requirement.
	 //Modified			: Neelesh - New function call automatically removes code for workbook creation from this function along with dynamic support for different excel file format.
	 //############################################################################################################################################################################################################################################################   

	 public static ExtentHtmlReporter html;
	 public static ExtentReports extent;
	 public static ExtentTest test, suiteTest;

	 public void reportStep(int rowNum, String stepPassFailVal, String stepDetails, String scrnShotPath, String stepName, WebDriver driver,  String scrollStrategy, String sheetName, WebElement objElement){
		 try{

			 ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
			 String strProductName = context.getCurrentXmlTest().getParameter("productName");
			 String strClientName = context.getCurrentXmlTest().getParameter("clientName");
			 String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			 String strEnviName = context.getCurrentXmlTest().getParameter("environment");
			 //Setting global variable value for All Pass Status
			 if(stepPassFailVal.equals("Failed")){
				 context.setAttribute("resultStatus_AllPassed", "False");
			 }
			 //1. Screenshot Step
			 //Logic - If "scrnShotPath" parameter is empty then take a screenshot calling the default screenshot method. But if 
			 // >> it has some value then use that path. Scenario where we want to add more details to screenshot using shutterbug tool
			 // >> should be called before the report method n path should be passed.
			 String imgPath = "";
			 if(scrnShotPath == "" && stepPassFailVal.equals("Failed")){
				 //take screenshot with basic details
				 imgPath = commonUtils.captureScreenshot(driver, stepName, stepDetails, scrollStrategy, objElement);
			 }else if(!(scrnShotPath == "") && stepPassFailVal.equals("Failed")){
				 imgPath = scrnShotPath;
			 }

			 html = new ExtentHtmlReporter("./Reports/result.html");
			 html.setAppendExisting(true);
			 extent = new ExtentReports();	
			 extent.attachReporter(html);	
			 suiteTest = extent.createTest(context.getCurrentXmlTest().getParameter("scriptName"));
			 test = suiteTest.createNode(stepDetails); 
			 if(stepPassFailVal.equalsIgnoreCase("Failed")) {
				 MediaEntityModelProvider img=null;
				 try {
					 System.out.println("C://SE_Reports//WM//Integration//BOW//NavigationPanel_TabsNDropdown_Check//"+imgPath);
					 img = MediaEntityBuilder.createScreenCaptureFromPath
							 //("./"+imgPath).build();
							 ("C://SE_Reports//"+strProductName+"//"+strEnviName+"//"+strClientName+"//"+strScriptName+"//"+imgPath).build();

				 } catch (IOException e) {		
				 }

				 test.fail(stepDetails, img);
			 }
			 else if(stepPassFailVal.equalsIgnoreCase("Passed")) {
				 test.pass(stepDetails);			
			 }else if (stepPassFailVal.equalsIgnoreCase("WARNING")) {
				 test.warning(stepDetails);
			 }else if (stepPassFailVal.equalsIgnoreCase("INFO")) {
				 test.info(stepDetails);
			 }						

			 extent.flush();

		 }catch(Exception e){
			 System.out.println("Exception found in Reporting class ->>  reportStep method");
			 e.printStackTrace();}//End of try Catch
	 }

	/*public void startReport() {
		 System.out.println("-----------------Before Suite------------------");
		 ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		 String productName = context.getCurrentXmlTest().getParameter("productName");
			if(productName.equalsIgnoreCase("Maxit")) 
				html = new ExtentHtmlReporter("./Reports/Maxit_Reports/extentreport.html");
			else
				html = new ExtentHtmlReporter("./Reports/WM_Reports/extentreport.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();		
		extent.attachReporter(html);	
	}*/

	
	/*public ExtentTest startTestModule(String testCaseName) {
		 System.out.println("------------------Before Class-------------------");
		 ITestContext context;
    	context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
    	testCaseName = context.getCurrentXmlTest().getParameter("scriptName");
		suiteTest = extent.createTest(testCaseName);
		return suiteTest;
	}*/

	/*@BeforeMethod
    public ExtentTest startTestCase(String stepDetails) {
		test = 	suiteTest.createNode(stepDetails);
		return test;
	}*/

	/*@AfterSuite
    public void endTestCase() {
		System.out.println("--------------------------After Suite-------------------");
		extent.flush();
	}	   */   
}