package functionalLibrary.Global;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
//import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;

import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.assertthat.selenium_shutterbug.utils.web.ScrollStrategy;



public class CommonUtils {
	//static Logger log = Logger.getLogger(CommonUtils.class);
	public static Exception excep;
	ITestContext contxt = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
	
//###################################################################################################################################################################  
//Function name		: fnExceptionHandling(Exception excep, WebElement strElement) ------- This is on Hold for advanced phase
//Class name		: CommonUtils
//Description 		: Generic function to handle exceptions
//Parameters 		: WebElement
//					: Exception object	
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	
	public void fnExceptionHandling(Exception excep, WebElement strElement){
		if(excep.toString().toUpperCase().contains("NOSUCHELEMENTEXCEPTION")){
			fnNoSuchElementException(excep,strElement);			
		}//End of IF condition to see if the Exception=NoSuchElementException
		
	}//End of <Method: fnExceptionHandling>
	
	
	private void fnNoSuchElementException(Exception excep,WebElement strElement){
		System.out.println("Object not found in browser,Please check its properties in PageObjects Package. Web Object Name:"+strElement);
		
	}//End of <Method: fnNoSuchElementException>
	
//###################################################################################################################################################################  
//Function name		: isElementPresent(WebElement varElement)
//Class name		: CommonUtils
//Description 		: Generic function to check if the element exists
//Parameters 		: WebElement
//Assumption		: None
//Developer			: Kavitha Golla & Neelesh Vatsa
//###################################################################################################################################################################	
	public static boolean isElementPresent(WebElement varElement){
		try{
			if(varElement.isDisplayed()){
				return true;
				}
			return false;
		}//End of try block

		catch(Exception e){		
			return false;
		}//End of catch block	
	}//End of Method:isElementPresent
	
//###################################################################################################################################################################  
//Function name		: isAlertPresent(WebDriver driver)
//Class name		: CommonUtils
//Description 		: Generic function to check if an alert exists
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	public static boolean isAlertPresent(WebDriver driver){
	    try {
	        Alert alert = driver.switchTo().alert();
	        String alertMessage = alert.getText();
	        System.out.println("Alert is present, Alert Message: "+alertMessage);
	        return true;
	    } // End of try block
	    catch (NoAlertPresentException e) {
	    	System.out.println("Alert is not present");
	        return false;
	    } // End of NoAlertPresentException catch block
	    
	    catch (Exception e) {
	    	System.out.println("Exception in <Method: isAlertPresent> <Class:CommonUtils>: Alert is not present");
	    	e.printStackTrace();
	        return false;
	    } // End of NoAlertPresentException catch block	    
	}//End of Method: isAlertPresent
	
//###################################################################################################################################################################  
//Function name		: acceptAlert(Alert alert)
//Class name		: CommonUtils
//Description 		: Generic function to accept if alert exists.
//Parameters 		: Alert alert
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	public static boolean acceptAlert(WebDriver driver){
	    try {
	    	Alert alert = driver.switchTo().alert();
	        String alertMessage = alert.getText();
	        System.out.println("Alert is present, Alert Message: "+alertMessage);
	        alert.accept();
	        return true;
	    } // End of try block
	    catch (NoAlertPresentException e) {
	    	System.out.println("NoAlertPresentException in <Method: acceptAlert> <Class:CommonUtils>: Alert is not present");
	        return true;
	    } // End of NoAlertPresentException catch block
	    
	    catch (Exception e) {
	    	System.out.println("Exception in <Method: acceptAlert> <Class:CommonUtils>: Alert is not present");
	    	e.printStackTrace();
	        return false;
	    } // End of NoAlertPresentException catch block	    
	}//End of Method: acceptAlert
	//###################################################################################################################################################################  
	//Function name		: captureScreenshot(String strStepName, String stepDetail, String strScrollStgy, WebElement objElement)
	//Class name		: CommonUtils
	//Description 		: Generic function to Capture Screenshot of the page
	//Parameters        :   Context - to get parameter value
	//  					StepName - to be used in img name
	//  					stepDetail - to be used as img title
	//   					Scroll Strategy - None, Vertical, Horizontal or both
	//    					Web Element to be highlighted if any
	//Assumption		: None
	//Developer			: Neelesh Vatsa 
	//Modified 			: Kavitha Golla(Removed ITestContext parameter, added code for global context declaration , added code for BOTH_DIRECTIONS case in switch case]
	//Modified			: Neelesh Vatsa ( Made image path being returned as relative so it can be opened from any location.
	//###################################################################################################################################################################	
		public  String captureScreenshot(WebDriver driver , String strStepName, String stepDetail, String strScrollStgy, WebElement objElement) throws Exception {
			//WebDriver driver = ManageDriver.getManageDriver().getDriver();
			ITestContext contxt = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
	        String strProductName = contxt.getCurrentXmlTest().getParameter("productName");
	        String strClientName = contxt.getCurrentXmlTest().getParameter("clientName");
	        String strScriptName = contxt.getCurrentXmlTest().getParameter("scriptName");
	        String strEnviName = contxt.getCurrentXmlTest().getParameter("environment");
	        
	        Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
	        Date date = new Date();
	        String imgName = strStepName + "_Error_" + (formatter.format(date));
	        
	        //Kavitha: AUTO-421 : Added logic to restrict Maxit scripts to read screenshots path from testNG.xml base path.
	        String imgPath;
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");        
	        Date Ops_folderDate = new Date();
	        String Ops_ResFOlder =  sdf.format(Ops_folderDate);
	        
	        if(strProductName.equalsIgnoreCase("Maxit") && strScriptName.equalsIgnoreCase("QuickSmoke")){
	        	if(!(System.getProperty("resBasePath").toUpperCase().contains("OpsTeam_Reports"))){
	        		imgPath = (System.getProperty("resBasePath") + "/"+strProductName + "/"+strScriptName+"/"+Ops_ResFOlder+"/Screenshots");
	        	}
	        	else
	        		imgPath = (System.getProperty("resBasePath")+"/"+Ops_ResFOlder+"/Screenshots");
	        }
//	        		imgPath = (System.getProperty("resBasePath") + "/Screenshots");
//	        }
	        else if((strProductName.equalsIgnoreCase("Maxit")) && !(strScriptName.equalsIgnoreCase("QuickSmoke"))){
//	        	if(System.getProperty("resBasePath").toUpperCase().startsWith("S:")){
//	        		imgPath = (System.getProperty("resBasePath") + strClientName +  "/" + strScriptName + "/Screenshots");
//	        		}
//	        	else
	        		imgPath = (System.getProperty("resBasePath") + strProductName + "/" + strEnviName +"/" + strClientName +  "/" + strScriptName + "/Screenshots");
			}
	        else{
	        	imgPath = "C:/SE_Reports/" + strProductName + "/" + strEnviName + "/" + strClientName + "/" + strScriptName + "/Screenshots";
	        	
	        }
	        
	        //imgPath = "C:/SE_Reports/" + strProductName + "/" + strEnviName + "/" + strClientName + "/" + strScriptName + "/Screenshots";
	        createFolder_NotExists(imgPath);
	      //Scroll strategy based call
	       //Check if the web element to be highlighted is not present, if not then report wont have result value.
	        if(!isElementPresent(objElement)){
	        	objElement = null;
			}
	        strScrollStgy = strScrollStgy.toUpperCase();
	        if(objElement==null){
	        	//Highlight the webElement if parameter is provided
	            switch (strScrollStgy){
	            case "HORIZONTALLY":
	                Shutterbug.shootPage(driver, ScrollStrategy.HORIZONTALLY).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            case "VERTICALLY":
	                Shutterbug.shootPage(driver, ScrollStrategy.VERTICALLY).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            case "BOTH_DIRECTIONS":
	                Shutterbug.shootPage(driver, ScrollStrategy.BOTH_DIRECTIONS).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            default:
	                Shutterbug.shootPage(driver).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            }
	            
	        }else{
	            switch (strScrollStgy){
	            case "HORIZONTALLY":
	                Shutterbug.shootPage(driver, ScrollStrategy.HORIZONTALLY).highlight(objElement).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            case "VERTICALLY":
	                Shutterbug.shootPage(driver, ScrollStrategy.VERTICALLY).highlight(objElement).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            case "BOTH_DIRECTIONS":
	                Shutterbug.shootPage(driver, ScrollStrategy.BOTH_DIRECTIONS).highlight(objElement).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            default:
	                Shutterbug.shootPage(driver).highlight(objElement).withTitle(stepDetail + new Date()).withName(imgName).save(imgPath);
	                break;
	            }
	            
	        }
	        //scroll back to top of page
	        scroll_toTopOfPage(driver);
      	  	//return path
	        imgPath = "Screenshots";
      	  	String fullImgPath = imgPath + "/" + imgName + ".png";
	        return fullImgPath;

	        
		}//End of Method: captureScreenshot	[Method overloading]
//###################################################################################################################################################################  
//Function name		: scroll_ToTopOfPage(WebDriver driver)
//Class name		: CommonUtils
//Description 		: Generic function to scroll back to the top of webpage.
//Parameters 		: webdriver
//Assumption		: None
//Developer			: Neelesh Vatsa
//###################################################################################################################################################################	
	public void scroll_toTopOfPage(WebDriver driver){
		//scrolling to the top of the page
  	  	JavascriptExecutor js = ((JavascriptExecutor) driver);
  	  	js.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		        
	}//End of Method: scroll_ToTopOfPage	
//###################################################################################################################################################################  
//Function name		: scroll_ToBottomOfPage(WebDriver driver)
//Class name		: CommonUtils
//Description 		: Generic function to scroll down to the bottom of webpage.
//Parameters 		: webdriver
//Assumption		: None
//Developer			: Neelesh Vatsa
//###################################################################################################################################################################	
	public void scroll_toBottomOfPage(WebDriver driver){
		//scrolling to the top of the page
  	  	JavascriptExecutor js = ((JavascriptExecutor) driver);
  	  	js.executeScript("window.scrollTo(0, document.body.scrollHeight)");		        
	}//End of Method: scroll_ToBottomOfPage				
//###################################################################################################################################################################  

//###################################################################################################################################################################  
//Function name		: scroll_webelementIntoView(WebDriver driver, WebElement element)
//Class name		: CommonUtils
//Description 		: Generic function to scroll down to the bottom of webpage.
//Parameters 		: webdriver
//Assumption		: None
//Developer			: Neelesh Vatsa
//###################################################################################################################################################################	
	public void scroll_webelementIntoView(WebDriver driver, WebElement element){
		//scrolling to the top of the page
	  	JavascriptExecutor js = ((JavascriptExecutor) driver);
	  	js.executeScript("arguments[0].scrollIntoView(true);",element);		        
	}//End of Method: scroll_ToBottomOfPage				
//###################################################################################################################################################################  

//###################################################################################################################################################################  
//Function name		: loggerScreenshot_Path(String strPath)
//Class name		: CommonUtils
//Description 		: Generic function to place screenshot path in logger html file.
//Parameters 		: Screenshot path
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	public static String loggerScreenshot_Path(String strPath) throws Exception {
		if(strPath.isEmpty()){
			//log.error("Screenshot path is empty");
			return "";
		}
		return strPath;
		        
	}//End of Method: loggerScreenshot_Path		
	
	
//###################################################################################################################################################################  
//Function name		: fileExists(String strPath)
//Class name		: CommonUtils
//Description 		: Generic function to verify if the file or directory exists
//Parameters 		: Path of the file
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	public boolean fileExists(String strPath) throws Exception {
		if(strPath.trim()== null || strPath.trim().isEmpty() || strPath.trim().equals("")){
			System.out.println("<class:CommonUtils><Method:fileExists>	Path passed to this methos is NULL, please check!");
			//log.info("<class:CommonUtils><Method:fileExists>	Path passed to this methos is NULL, please check!");			
			return false;
		}
		File f = new File(strPath);
		if (f.exists()) {
		    return true;
		} else {
			System.out.println("File "+strPath+" does NOT exists in the directory.Please check!");
			return false;
		}
	}// End of Method: fileExists

//###################################################################################################################################################################  
//Function name		: createFolder_NotExists(String strPath)
//Class name		: CommonUtils
//Description 		: Generic function to verify if the file or directory exists
//Parameters 		: Path of the file
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	public boolean createFolder_NotExists(String strPath) throws Exception {
		if(strPath.trim()== null || strPath.trim().isEmpty() || strPath.trim().equals("")){
			System.out.println("<class:CommonUtils><Method: createFolder_NotExists>	Path passed to this method is NULL, please check!");			
			return false;
		}
		File f = new File(strPath);
		if (f.exists()) {		    
		    return true;
		} else {			
			System.out.println("File "+strPath+" does NOT exists in the directory.Please check!");
			if(f.mkdir()) { 
				System.out.println("Folder "+strPath+" is created."); 
				return true;
				}
			else if(f.mkdirs()){
				System.out.println("Folder "+strPath+" is created."); 
				return true;
			}
			else { 
				System.out.println("Folder "+strPath+" is NOT created, Please check.!!"); 
				return false;
				}
			
		}

	}// End of Method: createFolder_NotExists	
	

	//###################################################################################################################################################################  
	//Function name		: startTestCaseLog(String strTestCaseName)
	//Class name		: CommonUtils
	//Description 		: Generic function to create a start of Test case execution format for log
	//Parameters 		: Test Case Name
	//Assumption		: None
	//Developer			: Kavitha Golla& Neelesh Vatsa
	// Modified			: Neelesh - Added space before each variable in print stmnt, for more clarity.
	//###################################################################################################################################################################	
		 public static void startTestCaseLog(String strTestCaseName){
			 
			 ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
			 if (context == null){
				 System.out.println("ITestContext Object is not initialized yet!!!");
			 }
			 long id = Thread.currentThread().getId();
			 System.out.println("****************************************************************************************");		 
			 System.out.println("****************************************************************************************");		 
			 System.out.println("$$$$$$$$$$$$$$$$$$$$$                 "+strTestCaseName+ "                 $$$$$$$$$$$$$$$$$$$$$$$$$");	
			 System.out.println("$$$$$$$$$$$$$$$$$$$$$                Browser Thread ID:"+id+"              $$$$$$$$$$$$$$$$$$$$$$$$$");				
			 System.out.println("****************************************************************************************");		 
			 System.out.println("****************************************************************************************");	
			 String strClient = context.getCurrentXmlTest().getParameter("clientName");
			 String strEnv = context.getCurrentXmlTest().getParameter("environment");
			 String strProduct = context.getCurrentXmlTest().getParameter("productName");
			 String strBrowser = context.getCurrentXmlTest().getParameter("browser");
			 String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			 		 		 
			 //log.info("Test Execution started: Product="+strProduct+";ClientName="+strClient+";Environment="+strEnv+";Browser="+strBrowser+";Script Name="+strScriptName);
			 System.out.println("Test Execution started: Product="+strProduct+"; ClientName="+strClient+"; Environment="+strEnv+"; Browser="+strBrowser+"; Script Name="+strScriptName);
			}//End of startTestCaseLog method.
 
//###################################################################################################################################################################  
//Function name		: startTestCaseLog(String strTestCaseName,ITestContext context)
//Class name		: CommonUtils
//Description 		: Generic function to create a start of Test case execution format for log
//Parameters 		: Test Case Name
//			: ITestContext context [ this overload method is used for Maxit only, because thread is not initiated before this method call for ITestContext to be set.		 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	 public static void startTestCaseLog(String strTestCaseName,ITestContext context){
		 
		 if (context == null){
			 System.out.println("ITestContext Object is not initialized yet!!!");
		 }
		 long id = Thread.currentThread().getId();
		 System.out.println("****************************************************************************************");		 
		 System.out.println("****************************************************************************************");		 
		 System.out.println("$$$$$$$$$$$$$$$$$$$$$                 "+strTestCaseName+ "                 $$$$$$$$$$$$$$$$$$$$$$$$$");	
		 System.out.println("$$$$$$$$$$$$$$$$$$$$$                Browser Thread ID:"+id+"              $$$$$$$$$$$$$$$$$$$$$$$$$");				
		 System.out.println("****************************************************************************************");		 
		 System.out.println("****************************************************************************************");	
		 String strClient = context.getCurrentXmlTest().getParameter("clientName");
		 String strEnv = context.getCurrentXmlTest().getParameter("environment");
		 String strProduct = context.getCurrentXmlTest().getParameter("productName");
		 String strBrowser = context.getCurrentXmlTest().getParameter("browser");
		 String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
		 		 		 
		 //log.info("Test Execution started: Product="+strProduct+";ClientName="+strClient+";Environment="+strEnv+";Browser="+strBrowser+";Script Name="+strScriptName);
		 System.out.println("Test Execution started: Product="+strProduct+"; ClientName="+strClient+"; Environment="+strEnv+"; Browser="+strBrowser+"; Script Name="+strScriptName);
		}//End of startTestCaseLog method.
		 

//###################################################################################################################################################################  
//Function name		: endTestCaseLog(String strTestCaseName,ITestContext context)
//Class name		: CommonUtils
//Description 		: Generic function to create a start of Test case execution format for log
//Parameters 		: Test Case Name
//					: ITestContext context [ this overload method is used for Maxit only, because thread is not initiated before this method call for ITestContext to be set. 	 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	public static void endTestCaseLog(String strTestCaseName,ITestContext context){	
			long id = Thread.currentThread().getId();
			//ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
			System.out.println("****************************************************************************************");		 
			System.out.println("****************************************************************************************");		 
			System.out.println("$$$$$$$$$$$$$$$$$$$$$                 "+strTestCaseName+ "       $$$$$$$$$$$$$$$$$$$$$$$$$");	
			System.out.println("$$$$$$$$$$$$$$$$$$$$$      Browser Thread ID:"+id+" is closed.   $$$$$$$$$$$$$$$$$$$$$$$$$");				
			System.out.println("****************************************************************************************");		 
			System.out.println("**************************	"+"-E---N---D-"+"	***************************************");	
		 	 
			 String strClient = context.getCurrentXmlTest().getParameter("clientName");
			 String strEnv = context.getCurrentXmlTest().getParameter("environment");
			 String strProduct = context.getCurrentXmlTest().getParameter("productName");
			 String strBrowser = context.getCurrentXmlTest().getParameter("browser");
			 String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			 		 		 
			 System.out.println("Test Execution Complete: Product="+strProduct+" ;ClientName="+strClient+" ;Environment="+strEnv+" ;Browser="+strBrowser+" ;Script Name="+strScriptName);
			 			
		}//End of endTestCaseLog method.[Method Overloading]	 
//###################################################################################################################################################################  
//Function name		: endTestCaseLog(String strTestCaseName)
//Class name		: CommonUtils
//Description 		: Generic function to create a start of Test case execution format for log
//Parameters 		: Test Case Name
//Assumption		: None
//Developer			: Kavitha Golla and Neelesh Vatsa
// Modified			: Neelesh - Added space before each variable in print stmnt, for more clarity.
//###################################################################################################################################################################	
	public static void endTestCaseLog(String strTestCaseName){	
			long id = Thread.currentThread().getId();
			ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
			System.out.println("****************************************************************************************");		 
			System.out.println("****************************************************************************************");		 
			System.out.println("$$$$$$$$$$$$$$$$$$$$$                 "+strTestCaseName+ "       $$$$$$$$$$$$$$$$$$$$$$$$$");	
			System.out.println("$$$$$$$$$$$$$$$$$$$$$      Browser Thread ID:"+id+" is closed.   $$$$$$$$$$$$$$$$$$$$$$$$$");				
			System.out.println("****************************************************************************************");		 
			System.out.println("**************************	"+"-E---N---D-"+"	***************************************");	
		 	 
			 String strClient = context.getCurrentXmlTest().getParameter("clientName");
			 String strEnv = context.getCurrentXmlTest().getParameter("environment");
			 String strProduct = context.getCurrentXmlTest().getParameter("productName");
			 String strBrowser = context.getCurrentXmlTest().getParameter("browser");
			 String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			 		 		 
			 System.out.println("Test Execution Complete: Product="+strProduct+" ;ClientName="+strClient+" ;Environment="+strEnv+" ;Browser="+strBrowser+" ;Script Name="+strScriptName);
			 			
		}//End of endTestCaseLog method.[Method Overloading]
 
//###################################################################################################################################################################  
//Function name        	: contextGetParameterName(String strParameterName)
//Class name        	: CommonUtils
//Description        	: Use this function to get different parameters passed in testng.xml file for a given test case
//Parameters         	: parameter name
//Assumption        	: None
//Developer            : Neelesh Vatsa
//Modifed 				: Kavitha [Changed the return type to strParamName instead of strParameterName and added ITestContext to refer context object]
//###################################################################################################################################################################   
    
	public String contextGetParameterName(String strParameterName){
	    String strParamName = "";
	    ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
	     switch (strParameterName) {
	         case "scriptName":
	             strParamName = context.getCurrentXmlTest().getParameter("scriptName");
	             break;
	         case "clientName":
	             strParamName = context.getCurrentXmlTest().getParameter("clientName");
	             break;
	         case "productName":
	             strParamName = context.getCurrentXmlTest().getParameter("productName");
	             break;
	         case "browser":
	             strParamName = context.getCurrentXmlTest().getParameter("browser");
	             break;
	         case "environment":
	             strParamName = context.getCurrentXmlTest().getParameter("environment");
	             break;
	         default:
	             throw new IllegalArgumentException("Invalid parameter name: " + strParameterName);
	     }
	     return strParamName;
	} 
	//************************************************************************************************************
	//************************************************************************************************************
	//###################################################################################################################################################################  
	//Function name        	: create_TextResultFile
	//Class name        	: CommonUtils
	//Description        	: This funxtion creates a test file to report the failure caused before test case is run. Ex at >> login, dataprovider.
	//Parameters         	: Path where file should be created , Message string to be written
	//Assumption        	: None
	//Developer            : Neelesh Vatsa
	//###################################################################################################################################################################   
	public void create_TextResultFile(String strFilePath, String strResultMessage) throws Exception{
		//Create File In give location  
		Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
        Date date = new Date();
        String fileName = "FailedReport_From" + (formatter.format(date));
        boolean fileCreated = createFolder_NotExists(strFilePath);
        if(fileCreated == true){
			String TestFile = strFilePath + "\\" + fileName + ".txt";
			File FC = new File(TestFile);//Created object of java File class.
			FC.createNewFile();//Create file.
		
			//Writing result In to file.
			FileWriter FW = new FileWriter(TestFile);
			BufferedWriter BW = new BufferedWriter(FW);
			BW.write("Test Script could not be executed due to - " + strResultMessage); //Writing In To File.
			BW.newLine();
			BW.close();
        }
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - highlighElement
	//Description - This function highlights an element under test in UI.
	//Parameters - Webelement and driver
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################

	public void highlighElement(WebElement element, WebDriver driver){
	    JavascriptExecutor js=(JavascriptExecutor)driver; 
	    js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);

	    try 
	    {
	    	Thread.sleep(1000);
	    }catch (InterruptedException e) {
	    	System.out.println(e.getMessage());
	    } 

	    js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", element); 
	}
	
	//###################################################################################################################################################################  
	//Function name        	: switch_BrowserTabs(WebDriver driver,String strTabName)
	//Class name        	: CommonUtils
	//Description        	: This function switches the focus to different browser tab
	//Parameters         	: WebDriver -> driver object
	//						: Tab number to be switched to
	//Assumption        	: None
	//Developer            	: Kavitha Golla
	//###################################################################################################################################################################   
	public boolean switch_BrowserTabs(WebDriver driver,String strTabName){
		ArrayList<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		try{
		    for(int i=0;i<browserTabs.size();i++){
		    	driver.switchTo().window(browserTabs.get(i));
		    	System.out.println("Window handle title name for window:"+i+" is :"+driver.getTitle());
		    	if(driver.getTitle().contains(strTabName)){
		    		driver.switchTo().window(browserTabs.get(i));
		    		return true;
		    	}			    	
		    }//End of FOR loop
	    	driver.switchTo().window(browserTabs.get(0));
	    	System.out.println("Browser Tab with name ="+strTabName+" is not found, so switching the focus to default tab(0)");	    	
	    	return false;
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in <CommonUtils.switch_BrowserTabs>, please check");
			System.out.println(e.getMessage());
	    	driver.switchTo().window(browserTabs.get(0));
	    	System.out.println("Browser Tab with name ="+strTabName+" is not found, so switching the focus to default tab(0)");
	    	return false;
		}//End of catch block
	}//End of <Method: switch_BrowserTabs>	
	
	
	//###################################################################################################################################################################  
	//Function name        	: getComputerName()
	//Class name        	: CommonUtils
	//Description        	: This function is used to return the name of Computer's name
	//Parameters         	: 
	//Assumption        	: None
	//Developer            	: Kavitha Golla
	//###################################################################################################################################################################   
	public String getComputerName(){
		Map<String, String> env = System.getenv();
		try{
		    if (env.containsKey("COMPUTERNAME")) // This will be for Windows machine, tested in Windows7 & 10
		        return env.get("COMPUTERNAME");
		    else if (env.containsKey("HOSTNAME")) // this will be used for Mac/Linux machine, yet to test
		        return env.get("HOSTNAME");
		    else
		        return "Unknown Computer";
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in <CommonUtils.getComputerName>, please check");
			return "Unknown Computer";
		}//End of catch block
	}//End of <Method: getComputerName>	
	
	//###################################################################################################################################################################  
	//Function name        	: getFilesList(String strDirectory,String strExtension)
	//Class name        	: CommonUtils
	//Description        	: This function is used to return list of files with given extension from a directory
	//Parameters         	: strDirectory - Folder path ; strExtension - file extension(example .xlsx)
	//Assumption        	: None
	//Developer            	: Kavitha Golla
	//Example            	: getFilesList("H:/Kavitha/Maxit/SmokeData/Dummy",".xlsx");
	//###################################################################################################################################################################	
	public File[] getFilesList(String strDirectory,String strExtension) throws IOException{
		
		File dir = new File(strDirectory);
		System.out.println("Getting all files with extension "+strExtension+" from folder path: " + dir.getPath());
		File[] files = dir.listFiles(new FilenameFilter(){
	    
		public boolean accept(File dir, String name) {
			return name.toLowerCase().endsWith(strExtension);
	    }
		});
		return files;
	}
	
	//###################################################################################################################################################################  
	//Function name        	: compare_2StringArrays()
	//Class name        	: CommonUtils
	//Description        	: This function returns a string after comparing 2 arrays to be same. Retuned string contains mismatched items.
	//Parameters         	: 2 arrays under test
	//Assumption        	: None
	//Developer            	: Neelesh Vatsa
	//###################################################################################################################################################################   
	public boolean compare_2StringArrays(String[] arr1, String[] arr2, boolean isDuplicatePresent){
		//check if the size is even same
		if(arr1.length != arr2.length){
			System.out.println(" CommonUtils >> method: compare_2StringArrays -- 2 arrays are not of same size");
			return false;
		}//end of If
		//##################################
		//##################################
		
		//if duplicates cant be present and we just want to amke sure they have same elements
		if(isDuplicatePresent){
			ArrayList<String> arrList1 = new ArrayList<>(Arrays.asList(arr1));
			ArrayList<String> arrList2 = new ArrayList<>(Arrays.asList(arr2));
			if (arrList1.containsAll(arrList2)) {
				return true;
			}else{
				return false;
			}
		}//end of If
		//if duplicates can be present and the arrays should still have exact same values
		else{

			Arrays.sort(arr1);
			Arrays.sort(arr2);
			if (Arrays.equals(arr1, arr2)) {
				return true;
			}else{
				return false;
			}
		}// end of else
		//##################################
		//##################################
	}//End of function
	
}//End of <Class:CommonUtils>
