package functionalLibrary.Global;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;



public class DateUtils {

//###################################################################################################################################################################
    //Function name     : dateDiff(Date dtFromDate, Date dtEnddate, String strDiffIn)
    //Class name        : DateUtils
    //Description       : This function returns the difference between 2 dates in years or months or days etc
    //Parameters        : dtFromDate : in Date format[start date]
	//					: dtEnddate : in Date format[End date]
	//					: strDiffIn : String to get the difference in y or m or d etc
	//Assumption        : None
    //Developer         : Kavitha Golla
//###################################################################################################################################################################	
	public int dateDiff(Date dtFromDate, Date dtEnddate, String strDiffIn){
		try{
			  LocalDate ldt_FromDate= new LocalDate(dtFromDate);
			  LocalDate ldt_dtEnddate= new LocalDate(dtEnddate);
			  //Period per = new Period(ldt_FromDate,ldt_dtEnddate);
			  
			  switch (strDiffIn.toLowerCase() ){
				  case "y":
				  case "years":
					  Years y = Years.yearsBetween(ldt_FromDate, ldt_dtEnddate);
					  int yearDif = Math.abs(y.getYears());//this return 1
					  return yearDif;
				  case "m":
				  case "months":
					  Months m = Months.monthsBetween(ldt_FromDate, ldt_dtEnddate);
					  int monthDif = Math.abs(m.getMonths());//this return 1
					  return monthDif;
				  case "d":
				  case "days":
					  Days d = Days.daysBetween(ldt_FromDate, ldt_dtEnddate);;
					  int daysDiff = Math.abs(d.getDays());
					  return daysDiff;
				  case "w":
				  case "weeks":	
					  Weeks w = Weeks.weeksBetween(ldt_FromDate, ldt_dtEnddate);;
					  int weeksDiff = Math.abs(w.getWeeks());
					  return weeksDiff;
					  
				  case "h":
				  case "hours":
					  Hours h = Hours.hoursBetween(ldt_FromDate, ldt_dtEnddate);;
					  int hrDiff = Math.abs(h.getHours());
					  return hrDiff;
				  
				  case "mins":
				  case "minutes":
					  Minutes min = Minutes.minutesBetween(ldt_FromDate, ldt_dtEnddate);;
					  int minDiff = Math.abs(min.getMinutes());
					  return minDiff;
				  
				  case "secs":
				  case "seconds":
					  Seconds sec = Seconds.secondsBetween(ldt_FromDate, ldt_dtEnddate);;
					  int secDiff = Math.abs(sec.getSeconds());
					  return secDiff;				  
				  
				  default :
					  Months mon = Months.monthsBetween(ldt_FromDate, ldt_dtEnddate);
					  int monDif = Math.abs(mon.getMonths());//this return 1
					  return monDif;		  }//End of Switch case
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Class: DateUtils ><Method: dateDiff>: Please check");
			e.printStackTrace();
			return 0;
		}//End of catch block
		
	}//End of <Method: dateDiff >
//###################################################################################################################################################################
    //Function name     : convertString_to_Date(String strDate)
    //Class name        : DateUtils
    //Description       : This function converts as string in "mm/dd/yyyy" format to Date java.util object in mm/dd/yyyy format
    //Parameters        : String with date value in "mm/dd/yyy" format
    //Assumption        : None
    //Developer         : Kavitha Golla
//###################################################################################################################################################################
	public Date convertString_to_Date(String strDate){
	try{
	  DateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy"); 
	  Date utilDate = dateformat.parse(strDate);
	return utilDate;
	}
	catch(ParseException e){
		System.out.println("Parse Exception in <Class: DateUtils ><Method: convertString_to_Date>: Input parameter is not valida date format, Please check");
		e.printStackTrace();
		return null;
	}
	catch(Exception e){
		System.out.println("Exception in <Class: DateUtils ><Method: convertString_to_Date>Please check");
		e.printStackTrace();
		return null;
	}
}// End of <Method: convertString_to_Date>
	
//###################################################################################################################################################################
    //Function name     : getDatePart(Date strDate,String strDatePart)
    //Class name        : DateUtils
    //Description       : This function returns part of the date(ex: month, day or year)
    //Parameters        : Date in "mm/dd/yyy" format
	//					: String strDatePart : string specifying which part of date to be returned
    //Assumption        : None
    //Developer         : Kavitha Golla
	//Ex: getDatePart("07/12/2017","day")
//###################################################################################################################################################################
	public int getDatePart(Date strDate,String strDatePart){
	try{
		  LocalDate ldt_Date= new LocalDate(strDate);
		  switch(strDatePart.toUpperCase()){
		  	case "DAY":
		  	case "D":
		  		return ldt_Date.getDayOfMonth();
		  		
		  	case "MONTH":
		  	case "M":
		  	case "MON":
		  		return ldt_Date.getMonthOfYear();
			
		  	case "YEAR":
		  	case "Y":
		  		return ldt_Date.getYear();
		  		
		  	default:
		  		return ldt_Date.getDayOfMonth();
		  }//End of switch case
	}//End of try block

	catch(Exception e){
		System.out.println("Exception in <Class: DateUtils ><Method: getDatePart>Please check");
		e.printStackTrace();
		return 0;
	}
}// End of <Method: convertString_to_Date>
	
	//###################################################################################################################################################################
    //Function name     : getCurrentDateTime_Parameters
    //Class name        : DateUtils
    //Description       : This function returns various parameters associated with current date, like current month, day, day of the week etc
    //Parameters        : Name of the parameter required. | String strFieldName
    //Assumption        : None
    //Created By        : Neelesh Vatsa
	//Ex: getCurrentDateTime_Parameters("MonthName") = November
//###################################################################################################################################################################

	public String getCurrentDateTime_Parameters(String strFieldName){
    	try{
			String strDate = "";
	    	LocalTime time = new LocalTime();    
			DateTime jodaTime = new DateTime();
			switch(strFieldName){
			case "Year":
				strDate = String.valueOf(jodaTime.getYear());
				return strDate;
			case "MonthName":
				strDate = jodaTime.monthOfYear().getAsText();
				return strDate;
			case "MonthNumber":
				strDate = String.valueOf(jodaTime.getMonthOfYear());
				return strDate;
			case "Day":
				strDate = String.valueOf(jodaTime.getDayOfMonth());
				return strDate;
			case "WeekDayName":
				strDate = jodaTime.dayOfWeek().getAsText();
				return strDate;
			case "WeekDayNumber":
				strDate = String.valueOf(jodaTime.getDayOfWeek());
				return strDate;
			case "Hour":
				strDate = String.valueOf(jodaTime.getHourOfDay());
				return strDate;
			case "Minute":
				strDate = String.valueOf(jodaTime.getMinuteOfHour());
				return strDate;
			case "Seconds":
				strDate = String.valueOf(jodaTime.getSecondOfMinute());
				return strDate;
			case "MilliSeconds":
				strDate = String.valueOf(jodaTime.getMillisOfSecond());
				return strDate;
			case "TimeInMin":
				DateTimeFormatter fmtMin = DateTimeFormat.forPattern("h:mm");
				strDate = fmtMin.print(time);
				return strDate;
			case "AmPm":
				DateTimeFormatter fmtAmPm = DateTimeFormat.forPattern(" a");
				strDate = fmtAmPm.print(time);
				return strDate;
			case "TimeInMinAmPm":
				DateTimeFormatter fmtMinAmPm = DateTimeFormat.forPattern("h:mm a");
				strDate = fmtMinAmPm.print(time);
				return strDate;
			case "TimeInSecAmPm":
				DateTimeFormatter fmtSec = DateTimeFormat.forPattern("h:mm:ss a");
				strDate = fmtSec.print(time);
				return strDate;
			case "TimeInMilliSecAmPm":
				DateTimeFormatter fmtMilliSec = DateTimeFormat.forPattern("h:mm:ss:SS a");
				strDate = fmtMilliSec.print(time);
				return strDate;
			default:
				return "NoFound";
			}
    	}catch(Exception e){
    		System.out.println("EXCEPTION found in Class: DateUtils, Method: currentDateTime_Parameters.");
    		e.printStackTrace();
    		return "NotFound";
    	}
//End of try-catch
    }//END of Functionn
	//###################################################################################################################################################################
    //Function name     : minusMin_ToDate
    //Class name        : DateUtils
    //Description       : This function returns time after subtracting some value like 1 or 2 min. Mainly required tomatch the time of our machine with actual ESt time.
    //Parameters        : Value to be subtracted | int minusValue
    //Assumption        : None
    //Created By        : Neelesh Vatsa
	//Ex: String minusMin_ToDate(1)
//###################################################################################################################################################################

	public String minusMin_ToDate(int minusValue) {
       DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
       String strTime =  getCurrentDateTime_Parameters("TimeInMin");
       LocalTime time = formatter.parseLocalTime(strTime);
       time = time.minusMinutes(minusValue);
       //System.out.println(formatter.print(time));
       //Remove the trailing 0 from hour
       String newTime = formatter.print(time);
       if(String.valueOf(newTime.charAt(0)).equals("0")){
    	   newTime = newTime.substring(1);
       }
       return newTime;
    }
//###################################################################################################################################################################	
	//###################################################################################################################################################################
    //Function name     : getMonth_Number_FromName
    //Class name        : DateUtils
    //Description       : This function returns month number from moth name ex - mar or March
    //Parameters        : Name of the month
    //Assumption        : None
    //Created By        : Neelesh Vatsa
//###################################################################################################################################################################

	public int getMonth_Number_FromName(String monthName) {
     
		try{   
	        Date date = new SimpleDateFormat("MMM").parse(monthName);//put your month name here
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        int monthNumber=cal.get(Calendar.MONTH);
	        //System.out.println(monthNumber);
	        return (monthNumber+1);
		
		 }
		catch(Exception e)
		{
		 e.printStackTrace();
		 return -1;
		}
	}	
	//###################################################################################################################################################################	
		//###################################################################################################################################################################
	    //Function name     : isWithinRange
	    //Class name        : DateUtils
	    //Description       : This function verifies if a given date is within the inclusive range of to and from test dates
	    //Parameters        : date to be tested, from date and to date
	    //Assumption        : None
	    //Created By        : Neelesh Vatsa
	//###################################################################################################################################################################
	public boolean isWithinRange(Date testDate, Date fromDate, Date toDate)  {
	      
			return testDate.compareTo(fromDate) >= 0 && testDate.compareTo(toDate) <= 0;
			
		}
	     

}//End of <Class: DateUtils>
