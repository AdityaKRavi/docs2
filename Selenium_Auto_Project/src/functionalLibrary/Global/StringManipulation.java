package functionalLibrary.Global;


import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class StringManipulation {

	//************************************************************************************************************
	//************************************************************************************************************
	//#######################################################################################
	//Function name		: find_SubStringMatch
	//Description 		: This Function returns True if a string is found under another Test String. (uft Instr)
	//Parameters 		: sub string to be found and parent string under test
	//Assumption		: None
	//Created By 		: Neelesh Vatsa
	//########################################################################################	
	public boolean find_SubStringMatch(String strParentString, String strSubString){
		boolean flg_SubstringFound = false;
		int indexVal = strParentString.indexOf(strSubString);
		if(indexVal > 0){
			flg_SubstringFound = true;
		}
		return flg_SubstringFound;
		
		}//End of Function
	//************************************************************************************************************
	//************************************************************************************************************
	
	//###################################################################################################################################################################  
	//Function name        	: string_RemoveFromStringEnd
	//Class name        	: CommonUtils
	//Description        	: This function creates a test file to report the failure caused before test case is run. Ex at >> login, dataprovider.
	//Parameters         	: Path where file should be created , Message string to be written
	//Assumption        	: None
	//Developer            : Neelesh Vatsa
	//###################################################################################################################################################################   
	public String string_RemoveFromStringEnd(String strTestString, String strRemoveThisSubString){
		return StringUtils.removeEnd(strTestString, strRemoveThisSubString);
	}
	//************************************************************************************************************
	//************************************************************************************************************
	
	//#######################################################################################
	//Function name		: is_StringEmpty
	//Description 		: This function returns true if a string is blank "", null or has only whitespace
	//Parameters 		: string under test
	//Assumption		: None
	//Created By 		: Neelesh Vatsa
	//########################################################################################	
	public boolean is_StringNotEmpty(String strtestString){
		if(!StringUtils.isBlank(strtestString)){
			return true;
		}else{
			return false;
		}
		
		}//End of Function
	
	//#################################################################################################################################
	//Function name		: compareString_Match(String strActual,String strExpected)
	//Description 		: This function compare 2 strings including regExp within it and returns TRUE if string matches
	//Parameters 		: Actual & expected string to be compared.
	//Assumption		: None
	//Created By 		: Kavitha golla
	//##################################################################################################################################	
	public boolean compareString_Match(String strActual,String strExpected){
		if(strActual.matches(strExpected)) return true;
		else return false;		
		}//End of <Method: compareString_Match>
	//#################################################################################################################################
	//Function name		: compareString_Equality
	//Description 		: This function returns true if 2 string values are equal. It also handles null value and wild card symbol as is.
	//Note				: Above "Match" function doesnt work if the string contains wild card symbol by treating is as regex.
	//Parameters 		: Actual & expected string to be compared.
	//Assumption		: None
	//Created By 		: Neelesh Vatsa
	//##################################################################################################################################	
	public boolean compareString_Equality(String strActual,String strExpected){
		if(Objects.equals(strActual, strExpected) ) return true;
		else return false;		
		}//End of <Method: compareString_Match>
	
	//#######################################################################################
	//Function name		: replace_MultipleOccurences
	//Description 		: This function  replaces multiple occurrences of a single charter
	//Parameters 		: string under test, char that shud be replaced, number of occurrences allowed, string to be replaced by
	//Assumption		: None
	//Example usage		: i/p = ";;;;;Balances;Holdings;Order;;;"  || o/p = "Balances;Holdings;Order" || Call = replace_MultipleOccurences(strInput, ';', 1, "")
	//Created By 		: Neelesh Vatsa
	//########################################################################################	
	public String replace_MultipleOccurences(String strTestString, char strReplace, int aboveThis, String replaceWith){
		return strTestString.replaceAll("(" + strReplace + ")\\1{" + aboveThis + ",}", replaceWith);
	}//End of Function
	
	//************************************************************************************************************
	//************************************************************************************************************


}//End of <Class: StringManipulation>
