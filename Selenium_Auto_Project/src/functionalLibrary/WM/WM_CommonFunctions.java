package functionalLibrary.WM;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import PageObjects.WM.ResultTable_Section;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import org.openqa.selenium.support.ui.FluentWait;
import functionalLibrary.Global.DateUtils;

public class WM_CommonFunctions {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	//DataTable DataTable = new DataTable();
	//LoginPage_WM LoginPage_WM = new LoginPage_WM(driver);
	//HomePg_TopPanel HomePg_TopPanel = new HomePg_TopPanel(driver);
	Reporting Reporting = new Reporting();
	//WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	StringManipulation StringManipulation = new StringManipulation();
	DateUtils DateUtils = new DateUtils();
	
	@FindBy(how = How.XPATH, using = ".//div[@class='alert alert-info']/p[contains(text(), 'There are no' )]")
	public WebElement alert_NoResult_Message;
	// ************ Page Load icon   ************

	public WM_CommonFunctions(){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
	//************************************************************************************************************
	//************************************************************************************************************
	
	
	public boolean noResultFound() throws InterruptedException {
		try {
			Thread.sleep(50);
			if(CommonUtils.isElementPresent(alert_NoResult_Message)){
				System.out.println("No Results found.");
				return true;	
			}//end of if
			return false;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - wait_ForPageLoad
	//Description - This function waits till the spinner object - page load goes away for approx. 50sec.
	//Argument - none
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean wait_dynamicFluentWait(int rowNum, WebElement testObj, String objName) throws Exception{
		Instant start = Instant.now();
		boolean flgResult = true;
		try{
			//System.out.println("Dynamic wait starts for - " + objName);
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
									wait.withTimeout(30,TimeUnit.SECONDS)
									.pollingEvery(4, TimeUnit.SECONDS)				 
									.ignoring(NoSuchElementException.class)
									.until(ExpectedConditions.visibilityOf(testObj));
									//.until(ExpectedConditions.elementToBeClickable(testObj));
			}
		catch(org.openqa.selenium.TimeoutException e){
			System.out.println("Dynamic wait for - " + objName + " has timedout. Object load is extremly slow. Please check!!");
			return false;
		}//End of try-catch
		
		if(!CommonUtils.isElementPresent(testObj)){
			System.out.println("Failed - " + objName  + " is still missing from UI.");
			Reporting.reportStep(rowNum, "Failed", objName + " obect is missing from UI.", "", "wait_dynamicFluentWait_Failed", driver, "", "", null);
			flgResult = false;
		}//End of IF 
		Instant end = Instant.now();
		Duration timeElapsed = Duration.between(start, end);
		//System.out.println("Dynamic wait ended for - " + objName + ". Total Time Taken = "+ timeElapsed.toMillis() +" milliseconds");
		return flgResult;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - wait_ForPageLoad
	//Description - This function waits till the spinner object - page load goes away for approx. 50sec.
	//Argument - none
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean wait_ForPageLoad(int rowNum) throws Exception{
		try{
			TimeUnit.SECONDS.sleep(10);
			List<WebElement> list_LoadingObject = driver.findElements(By.cssSelector("div:not(.ng-hide).well.spinner"));
			 int intLoadingCount = 0;
			 intLoadingCount = list_LoadingObject.size();
			 //Check no of loading objects in page, if its less than equalto 1 then page is ready
			 //1 object may eb present due to News section loading object that Nick will fix later.
			 if(intLoadingCount <= 1){
					//System.out.println("Page is ready. Loading icon not found.");
					return true;
				}
			 for(int i = 0; i<=60; i++){
				 list_LoadingObject = driver.findElements(By.cssSelector("div:not(.ng-hide).well.spinner"));
				 intLoadingCount = list_LoadingObject.size();
				 if(intLoadingCount > 1){
					 System.out.println ("Page loading..");
					 //if the list size is greater than 1, then add a wait of 1sec
					 if(i == 0){
						 System.out.println("Page Loading in process...");
					 }
					 Thread.sleep(1000);
				 }else{
					return true;
				 }
			 }
			 if(intLoadingCount > 1){
				System.out.println("Application is too slow. Page is still loading after 50 seconds. Please check!!! ");
	  			//Reporting.reportStep(rowNum, "Failed","Page loading is extremly slow, over 50sec. ", "", "PageLoadCheck_Failed", driver, "", "", null);
	  			return false; 
			 }
		}catch(Exception e){
			System.out.println("Exception found in method >>wait_ForPageLoad, class = WM_CommonFunctions, while trying to wait for Loading... element. Please check!!! ");
  			e.printStackTrace();
  			Reporting.reportStep(rowNum, "Failed","Exception thrown while waiting for Loading... spinner element ", "", "PageLoadCheck_Failed", driver, "", "", null);
  			return false;
		}
		return false;
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - wait_ForObject_ToDisappear
	//Description - This function waits aan object disappears, mostly a loading/searching icon.
	//Argument - none
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean wait_ForObject_ToDisappear(int rowNum, WebElement objTest, String objName) throws Exception{
		try{
			Thread.sleep(500);
			if(!CommonUtils.isElementPresent(objTest)){
				System.out.println(objName + " has loaded");
				return true;
			}
			for(int i = 1; i <=15; i++){
				if(CommonUtils.isElementPresent(objTest)){
					if(i ==1){
						System.out.println("Wait cycle starts >> " + objName + " is Loading...");
					}
					Thread.sleep(100);
				}else{
					System.out.println(objName + " >> has loaded");
					return true;
				}
			}
			 
			System.out.println("Loading of " + objName + " is too slow. Please check!!! ");
  			Reporting.reportStep(rowNum, "Failed", objName + " - was loading extremly slow, over 50sec. ", "", "PageLoadCheck_Failed", driver, "", "", null);
  			return false; 
			 
		}catch(Exception e){
			System.out.println("Exception found in method >>wait_ForObject_ToDisappear, class = WM_CommonFunctions, while trying to wait for Loading... element. Please check!!! ");
  			e.printStackTrace();
  			Reporting.reportStep(rowNum, "Failed","Exception thrown while waiting for an element to disappear.", "", "LoadCheck_Failed", driver, "", "", null);
  			return false;
		}
	}
		
	//#######################################################################################
	//Function name		: add_ReportStep
	//Description 		:This function adds report step in excel file for both pass n fail result, replacing if stmnt from main class
	//Parameters 		: row no:, Actual value from ui, Expected value, Step Name and test data sheet name.
	//Assumption		: None
	//Created By		: Neelesh Vatsa
	//########################################################################################	
	public boolean add_ReportStep(int rowNum, String actualVaues, String ExpValues, String stepName, String sheetName) throws Exception{
		if(actualVaues.equals(ExpValues)){
			System.out.println(stepName + " step has Passed");
			Reporting.reportStep(rowNum, "Passed", "", "", "", driver, "", "", null);
			return true;
		}else{
			System.out.println(stepName + " step has Failed");
			Reporting.reportStep(rowNum, "Failed", "Actual values = " + actualVaues + " But Expected = " + ExpValues, "", "stepName", driver, "", "", null);
			return false;
		}
		
	
	}//End of Function
	//************************************************************************************************************
	//************************************************************************************************************	
	//######################################################################################################################
    //Function name     : getCellData()
    //Class name        : Reporting
    //Description       : This function gets cell data from excel sheet.
    //Parameters        : Script Name, Environment and Client name
    //Assumption        : None
    //Developer         : Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String getCellData(Sheet xlSheet, int RowNum, int ColNum) throws Exception {
		try{
			Cell cellResult = xlSheet.getRow(RowNum).getCell(ColNum);
			int dataType = cellResult.getCellType();
			if  (dataType == 3) {
				return "";
			}else{
				String CellData = cellResult.getStringCellValue();
				return CellData;
			}
		}catch (Exception e){
			System.out.println(e.getMessage());
			throw (e);
			}
		}//End of Function

//************************************************************************************************************
	//##################################################################################
	//Function name	: clickItem()
	//Description 	: this function clicks on the object after checking its existence
	//Parameters 	: object to be clicked
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################
	public boolean clickItem( WebElement objToClick, String itemName, int rowNum) throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 40);
		if(CommonUtils.isElementPresent(objToClick)){
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(objToClick)));
			objToClick.click();
			Thread.sleep(1000);
			return true;
		}else{
			System.out.println("Element not found, cannot perform click action.");
			Reporting.reportStep(rowNum, "Failed", itemName + " - object is missng for click action.", "", "", driver, "", "", null);
			throw new SkipException("Object not found!!");
		}
	}
	
	//##################################################################################
		//Function name	: hoverOver_Item()
		//Description 	: this function clicks on the object after checking its existence
		//Parameters 	: object to be clicked
		//Assumption	: None
		//Created By	: Neelesh Vatsa
		//##################################################################################
		public boolean hoverOver_Item( WebElement objToHover, String itemName, int rowNum) throws Exception{
			WebDriverWait wait = new WebDriverWait(driver, 60);
			if(CommonUtils.isElementPresent(objToHover)){
				wait.ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(objToHover)));
				
				Actions action = new Actions(driver);
				action.moveToElement(objToHover).perform();
				Thread.sleep(10);
				return true;
			}else{
				System.out.println(itemName + " - Element not found, cannot perform hover over action.");
				Reporting.reportStep(rowNum, "Failed", itemName + " - object is missng for hover over action.", "", "", driver, "", "", null);
				return false;
			}
		}
	//##################################################################################
	//Function name	: enterText()
	//Description 	: This function enters the text in textbox webelement after checking its existence.
	//Parameters 	: None
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################
	public void enterText( WebElement textboxObj, String strText, int rowNum) throws Exception{
		if(CommonUtils.isElementPresent(textboxObj)){
			textboxObj.clear();
			textboxObj.sendKeys(strText);
		}
		System.out.println("textbox object not found, cannot enter text.");
		Reporting.reportStep(rowNum, "Failed", textboxObj + " - textbox object is missng for text input action.", "", "", driver, "", "", null);
		throw new SkipException("Object not found!!");
	}
	//##################################################################################
	//Function name	: Set_ParentObjForImg()
	//Description 	: this function sets the parent object that will be highlighted
					// >> in screenshot. if that object itself is missing then it passes null to report.
	//Parameters 	: None
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################
	public WebElement Set_ParentObjForImg(WebElement objName) throws Exception{
		if(!CommonUtils.isElementPresent(objName)){
			objName = null;
		}
		return objName;
	}
	
	//##################################################################################
	//Function name	: object_getText()
	//Description 	: This function returns the page title like - Summary if page loaded.
	//Parameters 	: None
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################
	public String object_getText(WebElement objName, int rowNum) throws Exception{
		String innerText = "";
		
		if(CommonUtils.isElementPresent(objName)){
			innerText = objName.getText();
			return innerText;
		}else{
			//Reporting.reportStep(rowNum, "Failed", objName.toString() + " - text object is missng for get text action.", "", "", driver, "", "", null);
			//throw new SkipException("Object not found!!");
			return "";
		}
		
	}
	
	
	//##################################################################################
	//Function name	: dropwdown_ClickOnOption()
	//Description 	: This function click on a specific dropdown option.
	//Parameters 	: dropdown list element, option to click
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################	
	public boolean dropwdown_ClickOnOption(int rowNum, List ddList, String optionToClick) throws InterruptedException{
		try{
			//create a list of web element option to loop thru for click action
			Thread.sleep(1000);
			if(ddList.size()==0)
			{
			System.out.println("Dropdown List is empty - " + optionToClick);
			Reporting.reportStep(rowNum, "Failed", optionToClick + " - Dropdown list is empty. Please check the element.", "", "", driver, "", "", null);
			return false;
			}
			//WebDriverWait wait = new WebDriverWait(driver, 40);
			
			List<WebElement> allOptions = ddList;
			String valueTest = "";
			
				for(WebElement option:allOptions){
					// click on that option if the option from list matches the String that needs to be clicked
					valueTest = option.getText().trim();	
					if(valueTest.equals(optionToClick)){
						//wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(option)));
						option.click();	
						return true;
					}
				}//End of For Loop
			System.out.println("WM_CommonFunctions >> method -> dropwdown_ClickOnOption. Note- Dropdown option not found - " + optionToClick);
			// if option is not found in the dropdown, return false
			//Reporting.reportStep(rowNum, "Failed", optionToClick + " - not found in the dropdown list, please check.", "", "", driver, "", "", null);	
			
			return false;
		}catch(Exception e){
			System.out.println("Exception found in method >>dropwdown_ClickOnOption, class = WM_CommonFunctions. Please check!!! ");
  			//e.printStackTrace();
  			Reporting.reportStep(rowNum, "Failed", optionToClick + " -Could not click this dd option.", "", "", driver, "", "", null);
			return false;
		}
	}
	//##################################################################################
		//Function name	: dropwdown_ClickOnOption()  | Function Overloading to perform both click on navigation tab and dropdown together.
		//Description 	: This function click on a specific dropdown option.
		//Parameters 	: dropdown list element, option to click
		//Assumption	: None
		//Created By	: Neelesh Vatsa
		//##################################################################################	
		public boolean dropwdown_ClickOnOption(int rowNum, List ddList, String optionToClick, WebElement navigationTabObject, String tabName, String hoverOrClick, String strClient) throws InterruptedException{
			try{
				int i = 1;
				while(i<=3){
				//Loop 3 times to click on a dropdown option if not found	
					if(hoverOrClick.equals("hover")){
						hoverOver_Item(navigationTabObject, tabName, rowNum);
					}else{
						clickItem(navigationTabObject, tabName + " Dropdown", rowNum);
					}//end of If
					Thread.sleep(1000);
					if(ddList.size()==0)
					{
						System.out.println("Dropdown List is empty - " + optionToClick);
						Reporting.reportStep(rowNum, "Failed", optionToClick + " - Dropdown list is empty. Please check the element.", "", "", driver, "", "", null);
						return false;
					}
					List<WebElement> allOptions = ddList;
					String valueTest = "";
					// click on the required option in dropdown
					for(WebElement option:allOptions){
						// click on that option if the option from list matches the String that needs to be clicked
						valueTest = option.getText().trim();	
						if(valueTest.equals(optionToClick)){
							option.click();	
							//return control if click was successful
							return true;
						}
					}//End of For Loop
				// increment counter by 1 if attempt failed	
					System.out.println("Dropwdown could not be clicked in attepmt - " + i + ". Trying again..");
					i++;
				}//end of while
				System.out.println("WM_CommonFunctions >> method -> dropwdown_ClickOnOption. Note- Dropdown option not found - " + optionToClick);
				// if option is not found in the dropdown, return false
				Reporting.reportStep(rowNum, "Failed", optionToClick + " - not found in the dropdown list, please check.", "", "", driver, "", "", null);	
				return false;
			}catch(Exception e){
				System.out.println("Exception found in method >>dropwdown_ClickOnOption, class = WM_CommonFunctions. Please check!!! ");
	  			//e.printStackTrace();
	  			Reporting.reportStep(rowNum, "Failed", optionToClick + " -Could not click this dd option.", "", "", driver, "", "", null);
				return false;
			}
		}
	//######################################################################################################################
	//##################dropwdown_IsItemPresent_InList###################################################################################################
	//Name - dropdown_GetAllActualOptions
	//Description - check if an item to be clicked in dropdown is presnrt in the dd list or not.
	//Parameters - dropdown list object, element to click for dropdown to be displayed if required.
	//Returns - This function returns a String variable containing all Actual dropdown values from ui.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public boolean dropwdown_IsItemPresent_InList(int rowNum, List ddList, String optionToClick) throws InterruptedException{
		try{
			StringBuffer allItems = new StringBuffer("");
			List<WebElement> allOptions = ddList;
			for(WebElement optionItem:allOptions){
				// click on that option if the option from list matches the String that needs to be clicked
				allItems = allItems.append(";");
				allItems = allItems.append(optionItem.getText().trim());
					
			}//End of For Loop
			System.out.println("All List items are - " + StringManipulation.replace_MultipleOccurences(allItems.toString(), ';', 1, ""));
			//System.out.println("All List items are - " + allItems.toString().replaceAll("(;)\\1{1,}", ""));
			if(allItems.toString().contains(optionToClick)){
				return true;
			}else{
				System.out.println("Dropdown item not found - " + optionToClick);
				return false;
			}
		}catch(Exception e){
			System.out.println("Exception found in method >>dropwdown_IsItemPresent_InList, class = WM_CommonFunctions. Please check!!! ");
  			//e.printStackTrace();
  			Reporting.reportStep(rowNum, "Failed", optionToClick + " -Could not find this dd option.", "", "", driver, "", "", null);
			return false;
		}//end of try-catch
	}//End of Function
	//######################################################################################################################
	//######################################################################################################################
	//Name - dropdown_GetAllActualOptions
	//Description - This function returns a string of all actual dropdown values for given dropdown field .
	//Parameters - dropdown list object, element to click for dropdown to be displayed if required.
	//Returns - This function returns a String variable containing all Actual dropdown values from ui.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String dropdown_GetAllActualOptions(int rowNum, List ddList, WebElement toClick) throws InterruptedException{
		try{
			//click on dropdown list if required
			
			if(!(toClick == null)){
				clickItem( toClick, "Dropdown", rowNum);
				//toClick.click();
				Thread.sleep(500);
			}
			// loop thru the list of web elements to create a string seprated by semi colon
			String resultOptionsList="";
			List<WebElement> allOptions = ddList;
			String valueTest = "";
			//>> for all options in the list
			for(WebElement option:allOptions){
				valueTest = option.getText().trim();
				//>> if the value is not blank or null
				if(StringManipulation.is_StringNotEmpty(valueTest)){
					//add it to the result String
					resultOptionsList = resultOptionsList  +  valueTest + ";";	
				}//End of Nested If
			}//End of For Loop
			if(!resultOptionsList.equals("")){
				//if result string is not empty then remove extra semi colon from the end(trailing) and return the string
				resultOptionsList = StringManipulation.string_RemoveFromStringEnd(resultOptionsList, ";");
				System.out.println("Actual values = " + resultOptionsList);
				return resultOptionsList;
			}else{
				//else return a blank string
				Reporting.reportStep(rowNum, "Failed", "Dropdown list is empty. Dropdown element may be missing. Please check." , "", "GL_ddCheck", driver, "", "", null);
				return "";
			}
		}catch(Exception e){
			System.out.println("Exception found in method >>dropdown_GetAllActualOptions, class = WM_CommonFunctions. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception thrown. Dropdown object not found." , "", "GL_ddCheck", driver, "", "", null);
  			//e.printStackTrace();
  			return "";
		}
		
	}
	
	//######################################################################################################################
		//######################################################################################################################
		//Name - selectDD_GetAllActualOptions
		//Description - This function returns a string of all actual dropdown values for given Select/checkbox style dropdown field .
		//Parameters - dropdown list object, element to click for dropdown to be displayed if required.
		//Returns - This function returns a String variable containing all Actual dropdown values from ui.
		//Created By - Neelesh Vatsa
		//######################################################################################################################
		//######################################################################################################################
		public String selectDD_GetAllActualOptions(int rowNum, WebElement toClick) throws InterruptedException{
			try{
				//click on dropdown list if required
				//toClick.click();
				clickItem( toClick, "Dropdown", rowNum);
				Select selectDD = new Select(toClick);
				String resultOptionsList="";
				List<WebElement> allOptions = selectDD.getOptions();
				
				
				String valueTest = "";
				//>> for all options in the list
				for(WebElement option:allOptions){
					valueTest = option.getText().trim();
					//>> if the value is not blank or null
					if(StringManipulation.is_StringNotEmpty(valueTest)){
						//add it to the result String
						resultOptionsList = resultOptionsList  +  valueTest + ";";	
					}//End of Nested If
				}//End of For Loop
				if(!resultOptionsList.equals("")){
					//if result string is not empty then remove extra semi colon from the end(trailing) and return the string
					resultOptionsList = StringManipulation.string_RemoveFromStringEnd(resultOptionsList, ";");
					System.out.println("Actual values = " + resultOptionsList);
					return resultOptionsList;
				}else{
					//else return a blank string
					Reporting.reportStep(rowNum, "Failed", "Dropdown list is empty. Dropdown element may be missing. Please check." , "", "dropDown_optionCheck", driver, "", "", null);
					return "";
				}
			}catch(Exception e){
				System.out.println("Exception found in method >>selectDD_GetAllActualOptions, class = WM_CommonFunctions. Please check!!! ");
				Reporting.reportStep(rowNum, "Failed", "Exception thrown. Dropdown object not found." , "", "dropDown_Check", driver, "", "", null);
	  			//e.printStackTrace();
	  			return "";
			}
			
		}
	//######################################################################################################################
	//Name - selectDD_clickOnOption
	//Description - This function returns a string of all actual dropdown values for given dropdown field .
	//Parameters - dropdown list object, element to click for dropdown to be displayed if required.
	//Returns - This function returns a String variable containing all Actual dropdown values from ui.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public boolean selectDD_clickOnOption(int rowNum, WebElement ddObject, String fieldName, String selectValue) throws InterruptedException{
		try{
			Select ddList = new Select(ddObject);
			List<WebElement> allOptions = ddList.getOptions();
			clickItem(ddObject, fieldName, rowNum);
			dropwdown_ClickOnOption(rowNum, allOptions, selectValue);
			return true;
		}catch(Exception e){
			System.out.println("Exception found in method >>selectDD_clickOnOption, class = WM_CommonFunctions. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception thrown. Dropdown object not found." , "", "dropDown_Check", driver, "", "", null);
  			//e.printStackTrace();
  			return false;
		}
		
	}
	
	//######################################################################################################################
	//Name - Compare_DDList_ActualExpectedValues
	//Description - This function returns a string of all actual dropdown values for given dropdown field .
	//Parameters - dropdown list object, element to click for dropdown to be displayed if required.
	//Returns - This function returns a String variable containing all Actual dropdown values from ui.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public void Compare_DDList_ActualExpectedValues(int rowNum, String strDropdownName, List dd_List, WebElement tab_Name, String str_ExpValues, WebElement screnShot, String hoverOrClick) throws InterruptedException{
		try{
			// Click on dropdown or Hover Over
			if(hoverOrClick.equals("hover")){
				hoverOver_Item(  tab_Name,  strDropdownName,  rowNum);
			}else if(hoverOrClick.equals("click")){
				tab_Name.click();
			}
			String actualValue = dropdown_GetAllActualOptions(rowNum, dd_List, null).trim();
			if(StringManipulation.compareString_Equality(actualValue, str_ExpValues)){
				Reporting.reportStep(rowNum, "Passed", strDropdownName + " dropdown list has expected values", "", "", driver, "", "", null);
			}else{
				Reporting.reportStep(rowNum, "Failed", strDropdownName + " dropdown value check failed. Actual // Expected = " + actualValue + "//" + str_ExpValues, "", "Failed_Home_NavTab", driver, "", "", screnShot);
				
			}
		}catch(Exception e){
			System.out.println("Exception found in method >>Compare_DDList_ActualExpectedValues, class = WM_CommonFunctions. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception thrown. Dropdown list could not be verified" , "", "dropDown_Check", driver, "", "", null);
  			//e.printStackTrace();
		}
	}
	//######################################################################################################################
	//Name - textBox_ClearValue
	//Description - This function deletes the value from text box filed.
	//Parameters - 
	//Returns - 
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public void delete_textboxValue(int rowNum, WebElement ddObject) throws InterruptedException{
		try{
			ddObject.sendKeys(Keys.CONTROL + "a");
			ddObject.sendKeys(Keys.DELETE);
		}catch(Exception e){
			System.out.println("Exception found in method >>textBox_ClearValue, class = WM_CommonFunctions. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception thrown. Textbox object not found for clear action" , "", "dropDown_Check", driver, "", "", null);
  			//e.printStackTrace();
		}
		
	}
	
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - list_MatchAllColValues_WithThis
	//Description - This function verifies if all row value of given column matches a given string value. .
	//Parameters - row num, list with all row values for given column, string to match with
	//Returns - true if all rows match the expected value.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public boolean list_MatchAllColValues_WithThis(int rowNum, List ddList, String strToMatchWith, String colName) throws InterruptedException{
		try{
			
			boolean resultOptionsList= false;
			int i = 0;
			String failedRows = "";
			List<WebElement> allOptions = ddList;
			String valueTest = "";
			String[] strSelectOptions;
			//create an array of filter options or String that a cell needs to be compared with.
			//Example with a column can have 2 asset types - Stocks and Bonds
			if (strToMatchWith.contains(";")) {
				strSelectOptions = strToMatchWith.split(";");
			}else{
				strSelectOptions = new String[1];
				strSelectOptions[0] = strToMatchWith;
			}
			// for each row/cell value, verify that it matches with either of the expected Filter value, in case of multiple filter like Socks, options
			//Outer For loop to check each Cell value
			//System.out.println("##################   Size of the list = " + allOptions.size());
			for(WebElement option:allOptions){
				valueTest = option.getText().trim();
				resultOptionsList= false;
				//System.out.println("Table value for row no : " + i + " = " + valueTest);
				//Inner For loop to check each applied filter option
				for(String strFilterOption:strSelectOptions){
					//In case of Symbol, format the string as Symbol search does not return exact search text. Symbol = C will return C, CC etc
					if(colName.contains("Symbol")){
						if(valueTest.startsWith(strFilterOption)){
							valueTest = strFilterOption;
						}
					}//end of if for Symbol
					if(colName.contains("Description")){
						if((strFilterOption.contains("Held Away"))&& (valueTest.contains("Not Held at USBI"))){
							valueTest = strFilterOption;
						}else if((strFilterOption.contains("Held at USBI"))&& (!valueTest.contains("Not Held at USBI"))){
							valueTest = strFilterOption;
						}
					}//end of If for Description
					
					
					//pass the value comparison if the 2 values match, or actual value from UI is blank, Totals or Multiple text
					if(valueTest.equalsIgnoreCase(strFilterOption.trim()) || valueTest.equalsIgnoreCase("")||valueTest.equals("Money")|| valueTest.equalsIgnoreCase("Totals")|| valueTest.contains("Totals")|| valueTest.equalsIgnoreCase("Multiple")){
						resultOptionsList = true;
						break;
					}
				}//End of Nested For
				
				if(!resultOptionsList){
					//add failed row number to this variable for reporting purpose
					failedRows = failedRows + ";" + Integer.toString(i);
					System.out.println("Col value does not match with applied filter - Result table Row value = "+ valueTest + " || BUT Filter applied = " + strToMatchWith);
				
				}//End of IF
				//increment counter for row number 
				i++;
			}//End of Outer For
			
			//report failed row numbers into the report if there are any
			if(!failedRows.equals("")){
				Reporting.reportStep(rowNum, "Failed", "These rows do no match with applied filter value of -  "+ failedRows , "", "page_ddCheck", driver, "", "", null);
				return false;
			
			}//End of IF
			return resultOptionsList;
			
		}//End of TRY
		catch(Exception e){
			System.out.println("Exception found in method >>list_GetAllActualOptions, class = WM_CommonFunctions. Please check!!! ");
			//e.printStackTrace();
			return false;
		}//End of Catch
	}
	
	
	//##################################################################################
	//Function name	: dropwdown_ClickOnOption()
	//Description 	: This function click on a specific dropdown option.
	//Parameters 	: dropdown list element, option to click
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################	
	public boolean dropwdown_ClickOn_CheckboxOptions(WebElement dropdown_Tab, List ddList, String optionsToClick, int rowNum) throws InterruptedException{
		try{
			
			Thread.sleep(1000);
			if(ddList.size()==0)
			{
			System.out.println("Dropdown List is empty - " + optionsToClick);
			Reporting.reportStep(rowNum, "Failed", optionsToClick + " - Dropdown list is empty. Please check the element.", "", "", driver, "", "", null);
			return false;
			}
			List<WebElement> allOptions = ddList;
			String valueTest = "";
			//check all options are present
			String[] strSelectOptions;
			if (optionsToClick.contains(";")) {
				strSelectOptions = optionsToClick.split(";");
			}else{
				strSelectOptions = new String[1];
				strSelectOptions[0] = optionsToClick;
			}
			boolean flg_optionFound;
			
			for(String str_ToClick:strSelectOptions){  
				//System.out.println("method : dropwdown_ClickOn_CheckboxOptions ..looking for dd option = " + w);
				flg_optionFound = false;
				for(WebElement option:allOptions){
					valueTest = option.getText().trim();	
					if(valueTest.equals(str_ToClick.trim())){
						option.click();
						flg_optionFound = true;
						break;
					}
				}//End of nested For Loop
				if(!flg_optionFound){
					Reporting.reportStep(rowNum, "Failed", "Dropdown option not found = "+ optionsToClick , "", "GL_ddCheck", driver, "", "", null);
					System.out.println("dropwdown_ClickOn_CheckboxOptions >> Dropdown option not found - " + optionsToClick);
					return false;
				}
			} 
			
			dropdown_Tab.click();
			//System.out.println("dropwdown_ClickOn_CheckboxOptions >> All Dropdown options are selected - " + optionsToClick);
			return true;
		}catch(Exception e){
			System.out.println("Exception found in method >>dropwdown_ClickOnOption, class = WM_CommonFunctions. Please check!!! ");
  			//e.printStackTrace();
  			return false;
		}
	}
	//##################################################################################
	//Function name	: return_LongTerm_DateDiff()
	//Description 	: This function returns true if no: of year between 2 dates is 1 or more.
	//Parameters 	: to date, from date
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################	
	public String return_LongTermShortTerm_DateDiff(Date fromDate, Date endDate) throws InterruptedException{
		String actual_Term = "";
		int num_of_Years = DateUtils.dateDiff(fromDate, endDate, "y");
		if(num_of_Years>=1){
			actual_Term = "Long Term";
		}else{
			actual_Term = "Short Term";
		}
		return actual_Term;
		
	}
	
	//###################################################################################################################################################################
	//Function name     : convertString_to_Date(String strDate)
	//Class name        : WM_CommonFunctions
	//Description       : This function converts as string in "mmm dd yyyy" format date format mmm dd yyyy
	//Parameters        : String with date value in "mmm dd yyyy" format
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//###################################################################################################################################################################
		public Date convert_StringTo_Date(String strDate){
		try{
			 SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
	         Date formattedDate = formatter.parse(strDate);
		return formattedDate;
		}
		catch(ParseException e){
			System.out.println("Parse Exception in <Class: WM_CommonFunctions ><Method: convertString_to_Date>: Input parameter is not valida date format, Please check");
			e.printStackTrace();
			return null;
		}
		catch(Exception e){
			System.out.println("Exception in <Class: WM_CommonFunctions ><Method: convertString_to_Date>Please check");
			e.printStackTrace();
			return null;
		}
	}// End of <Method: convertString_to_Date>
		//###################################################################################################################################################################
		//Function name     : navigate_toPage
		//Class name        : WM_CommonFunctions
		//Description       : This function navgates to a specific page
		//Parameters        : page name and client name
		//Assumption        : None
		//Developer         : Neelesh Vatsa
		//###################################################################################################################################################################
		public void navigate_toPage(String strPageName, String strClientName, String strEnv){
			//Fetch current url from browser to get the first part with current client and envi detail
			String strCurrentUrl = driver.getCurrentUrl();
			String strSplitText ="";
			if(strEnv.equalsIgnoreCase("PROD_B") || strEnv.equalsIgnoreCase("PROD_A")){
				strSplitText = ".net";
			}else{
				strSplitText = ".com";
			}
			String arr[] = strCurrentUrl.split(strSplitText);
			
			String client_UrlPart = "";
			String url_pagepart = "";
			//String with correct client and envi part of url.
			client_UrlPart=arr[0] + strSplitText;
			if(strClientName.equals("LPL") ||strClientName.equals("1DB")) {
				switch(strPageName){
				case "Summary":
					url_pagepart = ("/Modules/Dashboard/dashboard.php?navLevel%5B%5D=dashboard");
					break;
				case "Home":
					url_pagepart = ("/Modules/Dashboard/dashboard.php?navLevel%5B%5D=dashboard");
					break;
					
				case "UGL":
					url_pagepart = ("/Modules/Accounts/CostBasis/Unrealized/unrealizedExpandable.php?navLevel[]=accounts&navLevel[]=costBasisUnrealized");
					break;
				case "RGL":
					url_pagepart = ("/Modules/Accounts/CostBasis/Realized/realizedCy.php");
					break;
				case "Holdings":
					url_pagepart = ("/Modules/Accounts/Positions/positions.php/?navLevel[]=accounts&navLevel[]=holdings");
					break;
				case "Holdings Asset":
					url_pagepart = ("");
					break;
				case "Order Status":
					url_pagepart = ("Modules/Trading/OrderStatus/combinedOrderStatus.php?navLevel[]=trading&navLevel[]=orderStatus");
					break;
				case "Trading - Stocks":
					url_pagepart = ("/Modules/Trading/Trade/Standard/Stock/enter.php?navLevel[]=trading&navLevel[]=stock");
					break;
				}
			}else {
				switch(strPageName){
				case "Summary":
					if(strClientName.equals("RWB")){
						url_pagepart = ("/Web/user/dashboard/ucd/1");
					}else{
						url_pagepart = ("/Modules/Dashboard/dashboard.php?navLevel%5B%5D=dashboard");
					}
						break;
				case "Home":
					url_pagepart = ("/Modules/Dashboard/dashboard.php?navLevel%5B%5D=dashboard");
					break;
					
				case "UGL":
					url_pagepart = ("/Web/account/unrealizedGainLoss/unrealizedGainLoss/?navLevel[]=accounts&navLevel[]=costBasisUnrealized");
					break;
				case "RGL":
					url_pagepart = ("/Web/account/realizedGainLoss/realizedGainLoss/?navLevel%5B%5D=accounts&navLevel%5B%5D=costBasisRealized");
					break;
				case "Holdings":
					url_pagepart = ("/Web/account/holdings/holdings/?navLevel[]=accounts&navLevel[]=holdings");
					break;
				case "Holdings Asset":
					url_pagepart = ("/Web/account/holdings/assetView/");
					break;
				case "Order Status":
					url_pagepart = ("/Web/account/orders/status/");
					break;
				case "Trading - Stocks":
					url_pagepart = ("/Modules/Trading/Trade/Standard/Stock/enter.php");
					break;
				}
			}
			String full_PageUrl	= client_UrlPart + url_pagepart;
			//driver.navigate().to(full_PageUrl);
			driver.get(full_PageUrl);
		}

	//###################################################################################################################################################################
	//Function name     : convert_ElementList_To_StringArray
	//Class name        : WM_CommonFunctions
	//Description       : This functionconverts a webelement list to a string array
	//Parameters        : WebElement list
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//###################################################################################################################################################################
		public String[] convert_ElementList_To_StringArray(List webEleLists){
			//check for empty list
			if(webEleLists.isEmpty()){
				return null;
				
			}
			List<WebElement> allOptions = webEleLists;
			String [] arrayEle = new String[allOptions.size()];
			int i =0;
			for(WebElement item : allOptions){
				arrayEle[i] = item.getText();
				i++;
			}
			return arrayEle;
			
		}
		
	//###################################################################################################################################################################
	//Function name     : get_DefaultColNames
	//Class name        : WM_CommonFunctions
	//Description       : This function returns a String of default column nmaes in their expected order.
	//Parameters        : client name
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//###################################################################################################################################################################
	public String get_DefaultColNames(String strClient){
		String colNames = "";
		switch(strClient) {
		case "USB":
			colNames = "Symbol / CUSIP;Description;Asset Class;Account Type;Qty;Price;$ Chg;% Chg;Day Chg;Market Value;% of Holdings;Estimated Annual Income;Current Yield";
			break;
		case "STF":
			colNames = "Asset Type;Description;Symbol / CUSIP;Quantity;Price;Daily Price Change;Market Value;Daily Value Change;Daily % Change;Unrealized G/L;Estimated Yield";
			break;
		case "BOW":
			colNames = "Symbol / CUSIP;Description;Quantity;Price;Price Change;Value;Value Change;Gain/Loss";
			break;
		case "DAV":
			colNames = "Symbol / CUSIP;Description;Quantity;% of Assets;Adjusted Cost/Original Cost;Avg Cost;Price;Day Chg;% Day Chg;UnrealizedGain/Loss;% UnrealizedGain/Loss;Value";
			break;
		case "LPL":
			colNames = "";
			break;
		case "1DB":
			colNames = "Type;Quantity;Symbol/CUSIP;Description;Price;Daily Price Change;% Change;Daily Value Change;Value";
			break;
		case "RWB":
			colNames = "Symbol;Security Name;Asset Classification;Account Type;Quantity;Price;Daily $ Price Change;Daily % Price Change;Daily Value Change;Value;% of Assets;Est. Annual Income;Est. Yield";
			break;
		}
		
		return colNames;
		
	}
	//###################################################################################################################################################################
	//Function name     : is_DefaultSorted
	//Class name        : WM_CommonFunctions
	//Description       : This function returns a returns true if the col name passed is sorted by default.
	//Parameters        : client name
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//###################################################################################################################################################################
		public boolean is_DefaultSorted(String strClient, String colName){
			boolean defaultSort_flg = false;
			
			switch(strClient) {
			case "USB":
			
				if( colName.equals("Symbol / CUSIP")) {
					defaultSort_flg = true;
				}
				
			case "RWB":
				break;
			case "STF":
				defaultSort_flg = false;
				
				
			}
			return defaultSort_flg;
			
	}
		
	//###################################################################################################################################################################
	//Function name     : select_AccountNum
	//Class name        : WM_CommonFunctions
	//Description       : This function returns a returns true if the col name passed is sorted by default.
	//Parameters        : client name
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//###################################################################################################################################################################
		public boolean select_AccountNum(int rowNum, String strClient, String acntNum){
			try {
				ResultTable_Section rs = new ResultTable_Section();
				List<WebElement> options = null;
				WebElement acntBtn = null;
				switch(strClient) {
				case "USB":
				case "BOW":
				case "DAV":
					acntBtn = rs.ddTab_AllAccountsFilter1;
					options =rs.acnt1;
					break;
				
				case "STF":
				case "WDB":
				case "LPL":
					acntBtn = rs.ddTab_AllAccountsFilter3;
					options = rs.acnt3;
					break;
				case "RWB":
					acntBtn = rs.ddTab_AllAccountsFilter2;
					options =rs.acnt2;
					break;
				}
				String acntName = "";
				//acntBtn.click();
				for(WebElement option : options) {
					acntName = option.getText();
					if(acntName.contains(acntNum)) {
						option.click();
						return true;
					}
				}//end of for
				//report that the given account no: was not found
				Reporting.reportStep(rowNum, "Failed", "Account num not found in dropdown list. Acnt no: = "+ acntNum , "", "acntNum_dd", driver, "", "", null);
				System.out.println("Dropdown option not found for Account no: - " + acntNum);
				return false;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}	
		
//	//###################################################################################################################################################################
	//Function name     : get_AllListOptions
	//Class name        : WM_CommonFunctions
	//Description       : This function returns a string with all options from a list.
	//Parameters        : client name
	//Assumption        : None
	//Developer         : Neelesh Vatsa
	//###################################################################################################################################################################
	
	public String get_AllListOptions(int rowNum, String strClient, String itemName, List items) {
		//StringBuffer sb = new StringBuffer();
		String resultOptionsList = "";
		String valueTest = "";
		List<WebElement> Options = items;

		for(WebElement option : Options) {
			valueTest = option.getText().trim();
			if(StringManipulation.is_StringNotEmpty(valueTest)){
				//add it to the result String
				//sb = sb .append(";" + option.getText());
				resultOptionsList = resultOptionsList  +  valueTest + ";";	
			}//End of Nested If
			//if(!CommonUtils.isElementPresent(option)) {
				//sb = sb .append(";" + option.getText());
			//}
		}//end of For
		if(!resultOptionsList.equals("")){
			//if result string is not empty then remove extra semi colon from the end(trailing) and return the string
			resultOptionsList = StringManipulation.string_RemoveFromStringEnd(resultOptionsList, ";");
			System.out.println("Actual values = " + resultOptionsList);
			return resultOptionsList;
		}
		return resultOptionsList;
		//return sb.toString();
	}//End of function
	
	
			
}//End of Class
