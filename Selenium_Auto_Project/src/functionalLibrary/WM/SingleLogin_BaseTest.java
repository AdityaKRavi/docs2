package functionalLibrary.WM;


import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.time.Duration;
import java.time.Instant;

import org.openqa.selenium.*;

import functionalLibrary.Global.*;


public class SingleLogin_BaseTest {

	WebDriver driver;
	Instant start = Instant.now();
	SoftAssert softAssert = new SoftAssert();
	//################################################    DataProvider    ################################################
	//####################################################################################################################
	@DataProvider
	public Object[][] TestData(ITestContext context) throws Exception{
		boolean flgLoginPassed = Boolean.parseBoolean((String) context.getAttribute("resultStatus_AllPassed"));
		
		
		System.out.println();
		System.out.println("***********  Dataprovider  Call  **********");
		System.out.println("*******************************************");
		
		try{
			if(!flgLoginPassed){
				System.out.println("Data Provider call -- >> Login status is Failed thus skipping Dataprovider.");
				throw new SkipException("BeforeTest check has failed");
			}
			String strClient = context.getCurrentXmlTest().getParameter("clientName");
			String strEnvi = context.getCurrentXmlTest().getParameter("environment");
			String strScript = context.getCurrentXmlTest().getParameter("scriptName");
			//Get excel data sheet path for given envi, client and script combination
			DataTable excelPath = new DataTable();
			CommonUtils commonUtils = new CommonUtils();
			WM_DataProviders WM_DataProviders = new WM_DataProviders();
			String xlTestDataPath = excelPath.getExcelDatasheetPath_WM(strScript, strEnvi, strClient);
			System.out.println("Test Data sheet path = " + xlTestDataPath);
			boolean fileExistCheck = commonUtils.fileExists(xlTestDataPath);
			if(fileExistCheck == false){
				System.out.println("*******************  NOTE  ******************************");
				System.out.println("Test excel file does not exists under the script test data folder for script - " + strScript);
				String str_textFileLocation = excelPath.getResultFilePath_WM(strScript, strEnvi, strClient);
				commonUtils.create_TextResultFile(str_textFileLocation, "Data provider has Failed. Test excel sheet is missing from folder location.");
				Object[][] testObjArray = new Object[0][0];
				context.setAttribute("resultStatus_AllPassed", "false");
				return (testObjArray);
			}//END of IF
			else{
				System.out.println("Dataprovider - Test data file exists");
				Object[][] testObjArray =  WM_DataProviders.getTableArray(xlTestDataPath,"Global");
				System.out.println("***********  End Of Dataprovider Call For - " + strScript + "  ***********");
				System.out.println("*************************************************");
				if (testObjArray.length<=0){
					context.setAttribute("resultStatus_AllPassed", "false");
					System.out.println("Test Not Executed as data provider has 0 rows");
				}//END of sub-IF
				return (testObjArray);}//END of Else
		}//END of TRY
		catch(Exception e){
			context.setAttribute("resultStatus_AllPassed", "false");
			System.out.println("DataProvider has Failed. Exception found.");
			return null;
		}//END of Catch	
	 }//END of Function - DataProvider
	//################################################    @Before Test    ################################################
	//####################################################################################################################
	@Parameters({"browser", "clientName", "environment", "productName", "scriptName", "strOS", "strVersion", "strHeadless"})
	@BeforeTest
	public void beforeTest(String browser, String clientName, String environment, String prodName, String scriptName, String strOS, String strVersion, String strHeadless, ITestContext context) throws Exception {	
		try{
			
			CommonUtils.startTestCaseLog(scriptName, context);
			System.out.println("---------- Before Test method for - " + scriptName + "  --------");
			String strOthers = clientName + ";" + environment + ";" + scriptName + ";" + strOS + ";" + strVersion + ";" + strHeadless;
			ManageDriver_WM manageDriver = ManageDriver_WM.getManageDriver();
	     	driver = manageDriver.initializeDriver(browser, strOthers);
	     	GlobalObjectsFactory.getGlobalObjects().setiTestContext(context);
	     	//Save the result file in new location
			Reporting Reporting = new Reporting ();
	     	String resultFilePath = Reporting.reportCreateResultFile(scriptName, environment, clientName, prodName);
	     	context.setAttribute("resultPath", resultFilePath);
	     	System.out.println("Result file path set from Before test method to -  " + resultFilePath);
	     	
	     	//initialize login_wm >> which initializes login page
	     	Login_WM Login_WM = new Login_WM();
	     	//call the login function n get true false value
	     	boolean loginPassFlg = Login_WM.fnWM_MainLogin(environment, browser, clientName, "");
	     	if(loginPassFlg==true){
	     		System.out.println("Login has passed for - " + scriptName);
	     		context.setAttribute("resultStatus_AllPassed", "true");
	     	}else{
	     		System.out.println("Login has failed for - " + scriptName);
	     		Reporting.reportStep(1, "Failed", "Login step has Failed.", "", "LoginFailed", driver, "", "", null);
	     		context.setAttribute("resultStatus_AllPassed", "false");
	     	}//END of If-Else
	    	
		}//END of TRY
		catch(Exception e){
			context.setAttribute("resultStatus_AllPassed", "false");
			System.out.println("Exception found in BaseTest Class-> @BeforeTest method..!!");
			//e.printStackTrace();
		}//END of Catch
		
	}//END of Function - @BeforeTest
	//################################################    Pre-Req Test    ################################################
	//####################################################################################################################
	@Test(description = "Check @Beforetest status",priority = 0)
	public void beforeTest_ResultCheck()throws Exception{
		System.out.println("ccheck -> beforeTest_ResultCheck");
		ITestContext context =  GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		

		try{
			boolean flgLoginPassed = Boolean.parseBoolean((String) context.getAttribute("resultStatus_AllPassed"));
			if(flgLoginPassed){
				System.out.println("Checkpoint >> Login has Passed ");
				softAssert.assertTrue(flgLoginPassed, "Login dependency has passed.");				
			}
			else{
				System.out.println("Checkpoint >> Login has  Failed -> beforeTest_ResultCheck");
				throw new SkipException("BeforeTest check has failed");
			}//END of If-Else
		}//End of Try
		catch(Exception e){
			System.out.println("Method - beforeTest_ResultCheck has Failed, resultStatus_AllPassed variable is not yet set or context is failing");
			
		}//End of Catch
	}//END of Function
	//################################################    DataProvider    ################################################
	//####################################################################################################################
	
	  @AfterMethod
	  public void afterMethod() {
		System.out.println("***********************    END of Iteration     ************************");
		System.out.println("************************************************************************");
	  }//END of Function @AfterMethod
	//################################################    @ After test    ################################################
	//####################################################################################################################
	  @Parameters({"browser", "clientName", "environment", "productName", "scriptName"})
	  @AfterTest
	  public void afterTest(String browser, String clientName, String environment, String prodName, String scriptName, ITestContext contx) throws Exception  {
	    try{
	    	Instant end = Instant.now();
			Duration timeElapsed = Duration.between(start, end);
			//Close the driver 
			System.out.println("---------- After Test method for - " + scriptName + "  --------");
			driver = ManageDriver_WM.getManageDriver().getDriver();
			ManageDriver_WM.getManageDriver().quit();
			System.out.println("$$$$$$$$$$$$$$$$$$    TEST RESULT     $$$$$$$$$$$$$$$$$");
			System.out.println("");
			
			long diffInMins = ((timeElapsed.getSeconds())%3600)/60;
			long seconds_output = ((timeElapsed.getSeconds())% 3600)%60;
			System.out.println("Time Taken = "+ diffInMins +" Mins and " + seconds_output + " seconds, for script execution.");
			System.out.println("");
			String resultStatus = ((String) contx.getAttribute("resultStatus_AllPassed")).toUpperCase();
			if (resultStatus.equals("FALSE")){
				System.out.println(scriptName + " RUN RESULT -> Test Case HAS FAILED STEPS. Please check report file");
				
			}else{
				System.out.println(scriptName + " RUN RESULT -> ALL PASSED. No failed step found.");
			}//END of If-Else
			System.out.println("");
			System.out.println("$$$$$$$$$$$$$$$$$$    TEST RESULT     $$$$$$$$$$$$$$$$$");
			CommonUtils.endTestCaseLog(scriptName, contx);
	    }//END of TRY
	    catch (Exception e) {
			  System.out.println("Exception in Basetest-> in @AfterTest method..!!");
			  //e.printStackTrace();
	    }//END of Catch
	  }
	  

}
//################################################    End of Class    ################################################
//####################################################################################################################
