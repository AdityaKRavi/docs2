package functionalLibrary.WM;


import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import java.time.Duration;
import java.time.Instant;

import org.openqa.selenium.*;
import functionalLibrary.Global.*;


public class BaseTest {

	WebDriver driver;
	Instant start = Instant.now();
	Reporting report = new Reporting();
	//################################################    @Before Suite    ################################################
    //####################################################################################################################
	/*@BeforeSuite
	public void beforeSuite() {
		report.startReport();
	}//END of function @beforeSuite
*/	
	//################################################    @Before Class   ################################################
    //####################################################################################################################
	/*@BeforeClass
	public void beforeClass() {
		ITestContext context;
    	context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
    	String testCaseName = context.getCurrentXmlTest().getParameter("scriptName");
		report.startTestModule(testCaseName);
	}//END of function @beforeClass
*/	
	//Get all the data using data provider(Data Validation not applicable)
	//################################################    DataProvider    ################################################
	//####################################################################################################################
	@DataProvider
	public Object[][] TestData(ITestContext context) throws Exception{
		boolean flgLoginPassed = Boolean.parseBoolean((String) context.getAttribute("resultStatus_AllPassed"));
		
		try{
			if(!flgLoginPassed){
				System.out.println("Data Provider call -- >> Login status is Failed thus skipping Dataprovider.");
				throw new SkipException("BeforeTest check has failed");
			}
			System.out.println();
			System.out.println("***********  Dataprovider  Call  **********");
			System.out.println("*******************************************");
			String strClient = context.getCurrentXmlTest().getParameter("clientName");
			String strEnvi = context.getCurrentXmlTest().getParameter("environment");
			String strScript = context.getCurrentXmlTest().getParameter("scriptName");
			//Get excel data sheet path for given envi, client and script combination
			DataTable excelPath = new DataTable();
			CommonUtils commonUtils = new CommonUtils();
			WM_DataProviders WM_DataProviders = new WM_DataProviders();
			String xlTestDataPath = excelPath.getExcelDatasheetPath_WM(strScript, strEnvi, strClient);
			System.out.println("Test Data sheet path = " + xlTestDataPath);
			boolean fileExistCheck = commonUtils.fileExists(xlTestDataPath);
			if(fileExistCheck == false){
				System.out.println("*******************  NOTE  ******************************");
				System.out.println("Test excel file does not exists under the script test data folder for script - " + strScript);
				String str_textFileLocation = excelPath.getResultFilePath_WM(strScript, strEnvi, strClient);
				commonUtils.create_TextResultFile(str_textFileLocation, "Data provider has Failed. Test excel sheet is missing from folder location.");
				//throw new SkipException("Testing skipped for missing test data file");
				Object[][] testObjArray = new Object[0][0];
				context.setAttribute("resultStatus_AllPassed", "false");
				return (testObjArray);
			}//END of IF
			else{
				Object[][] testObjArray =  WM_DataProviders.getTableArray(xlTestDataPath,"Global");
				System.out.println("***********  End Of Dataprovider Call For - " + strScript + "  ***********");
				System.out.println("*************************************************");
				if (testObjArray.length<=0){
					context.setAttribute("resultStatus_AllPassed", "false");
					System.out.println("Test Not Executed as data provider has 0 rows");
				}//END of sub-IF
				return (testObjArray);}//END of Else
		}//END of TRY
		catch(Exception e){
			context.setAttribute("resultStatus_AllPassed", "false");
			System.out.println("DataProvider has Failed. Exception found.");
			return null;
		}//END of Catch
	 }//END of Function - DataProvider
	//################################################    @Before Test    ################################################
	//####################################################################################################################

	@Parameters({"browser", "clientName", "environment", "productName", "scriptName", "strOS", "strVersion", "strHeadless"})
	@BeforeTest
	public void beforeTest(String browser, String clientName, String environment, String prodName, String scriptName, String strOS, String strVersion, String strHeadless, ITestContext context) throws Exception {	
		try{
			
			CommonUtils.startTestCaseLog(scriptName, context);
			System.out.println("---------- Before Test method --------");
			//GlobalObjectsFactory.getGlobalObjects().setiTestContext(context);
			//context.setAttribute("platform", platform);
			String strOthers = clientName + ";" + environment + ";" + scriptName + ";" + strOS + ";" + strVersion + ";" + strHeadless;
			ManageDriver_WM manageDriver = ManageDriver_WM.getManageDriver();
	     	driver = manageDriver.initializeDriver(browser, strOthers);
	     	GlobalObjectsFactory.getGlobalObjects().setiTestContext(context);
			System.out.println("");
			System.out.println("");
	    	//Save the result file in new location
			Reporting Reporting = new Reporting ();
	     	String resultFilePath = Reporting.reportCreateResultFile(scriptName, environment, clientName, prodName);
	     	context.setAttribute("resultPath", resultFilePath);
	     	context.setAttribute("resultStatus_AllPassed", "true");
	     	context.setAttribute("currentAccountNo", "");
	     	System.out.println("Result file path set from Before test method -  " + resultFilePath);
		}//END of TRY
		catch(Exception e){
			System.out.println("Exception in BaseTest Class-> @BeforeTest method..!!");
			context.setAttribute("resultStatus_AllPassed", "false");
			e.printStackTrace();
		}//END of Catch
		
	}//END of Function @BeforeTest
	//################################################    After Method    ################################################
	//####################################################################################################################
	  @AfterMethod
	  public void afterMethod() {
		System.out.println("***********************    END of Iteration     ************************");
		System.out.println("************************************************************************");
	  }//END of Function @AfterMethod
	  
	//################################################    @ After Test    ################################################
	//####################################################################################################################  
	  @Parameters({"browser", "clientName", "environment", "productName", "scriptName"})
	  @AfterTest
	  public void afterTest(String browser, String clientName, String environment, String prodName, String scriptName, ITestContext contx) throws Exception  {
	    try{
	    	Instant end = Instant.now();
			Duration timeElapsed = Duration.between(start, end);
			//Close the driver 
			driver = ManageDriver_WM.getManageDriver().getDriver();
			//ManageDriver_WM.getManageDriver().quit();
			System.out.println("$$$$$$$$$$$$$$$$$$    TEST RESULT     $$$$$$$$$$$$$$$$$");
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			System.out.println("");
			long diffInMins = ((timeElapsed.getSeconds())%3600)/60;
			long seconds_output = ((timeElapsed.getSeconds())% 3600)%60;
			System.out.println("Time Taken = "+ diffInMins +" Mins and " + seconds_output + " seconds, for script execution.");
			System.out.println("");
			String resultStatus = ((String) contx.getAttribute("resultStatus_AllPassed")).toUpperCase();
			System.out.println("resultStatus_AllPassed in after test = " + resultStatus);
			if (resultStatus.toLowerCase().equals("false")){
				System.out.println(scriptName + " RUN RESULT -> Test Case HAS FAILED STEPS. Please check the Script's Report folder.");
				
			}else{
				System.out.println(scriptName + " RUN RESULT -> ALL PASSED. No failed step found.");
			}//END of If-Else
			
			System.out.println("");
			System.out.println("$$$$$$$$$$$$$$$$$$    TEST RESULT     $$$$$$$$$$$$$$$$$");
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			CommonUtils.endTestCaseLog(scriptName, contx);
	    }//END of TRY
	    catch (Exception e) {
			  System.out.println("Exception in Basetest->afterTest in @AfterTest method..!!");
			 // e.printStackTrace();
	    }//END of Catch
	  }//END of Function
}//END OF CLASS
//################################################    End of Base Test    ################################################
//########################################################################################################################
