package functionalLibrary.WM;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.SkipException;
import PageObjects.WM.LoginPage_WM;
import PageObjects.WM.USB_NavigationPanel;
import PageObjects.WM.HomePg_TopPanel;
import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;

public class USB_PageFunctions {
	//WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	//DataTable DataTable = new DataTable();
	//LoginPage_WM LoginPage_WM = new LoginPage_WM(driver);
	//HomePg_TopPanel HomePg_TopPanel = new HomePg_TopPanel(driver);
	//Reporting Reporting = new Reporting();
	//WM_CommonFunctions pageFunction = new WM_CommonFunctions();

	//#######################################################################################
	//Name	- getNavigation_ExpectedDropDownValues
	//Description - This function returns Expected Navigation tab's drop down values.
	//Argument - Navigation tab name like Trading
	//Returns - This function returns a String variable with the names of all expected drop down 
				//options separated by semi colon.
	//Date - Oct 2017
	//Created By - Neelesh Vatsa
	//########################################################################################	
	public String getNavigation_ExpectedDropDownValues(String strTabName){
		String expDropDownValues = "";
		switch (strTabName){
		case "Account Details":
			expDropDownValues = "Account Details;Summary;Balances;Asset Allocation;Holdings;Account Activity;Unrealized Gain/Loss;Realized Gain/Loss;Portfolio At A Glance;Household Overview;Download";
			break;
		case "Trading":
			expDropDownValues = "Trading;Stocks;Mutual Funds;Options;Order Status";
			break;
		case "Transfers":
			expDropDownValues = "Transfers;External Transfers;Asset Transfers";
			break;
		case "My Documents":
			expDropDownValues = "My Documents;Statements;Letters & Notices;Tax Documents;Performance Reports;Trade Documents;Paperless Preferences";
			break;
		case "Markets & Research":
			expDropDownValues = "Markets & Research;Economic & Market Commentary;Market Overview;Market Events Calendar;Screeners;Stock Report;Mutual Fund / ETF Report;Option Chains;Interactive Chart;Stock Alerts";
			break;
		case "Education Center":
			expDropDownValues = "Education Center;Help Center;Calculators;Fixed Income Investing;General Investing;International Investing;Life Insurance/ Annuities;Mutual Funds/ETFs;Personal Finance;Retirement;Saving for College;Tax Planning";
			break;
		default:
			expDropDownValues = "Incorrect tab name";
			break;
		}
		return expDropDownValues;

	}	//End of Function
	//************************************************************************************************************
	//************************************************************************************************************
	
}//End of Class
