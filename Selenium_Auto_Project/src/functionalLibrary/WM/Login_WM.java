package functionalLibrary.WM;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.SkipException;

import PageObjects.WM.HomePg_TopPanel;
import PageObjects.WM.LoginPage_WM;
import PageObjects.WM.ResultTable_Section;
import PageObjects.WM.SummaryPage;
import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.ManageDriver_WM;

public class Login_WM {
	//WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	//DataTable DataTable = new DataTable();
	//LoginPage_WM LoginPage_WM = new LoginPage_WM(driver);
	
	
	WebDriver driver;
	ManageDriver_WM manageDriver;
	DataTable DataTable;
	LoginPage_WM LoginPage_WM;
	
	
	public Login_WM() throws Exception{		
		manageDriver = ManageDriver_WM.getManageDriver();
		driver = manageDriver.getDriver();
		DataTable = new DataTable();
		LoginPage_WM = new LoginPage_WM();

	}	
	
	//##################################################################################################
	//Function name		: fnWM_MainLogin(String strEnvironment,String strBrowser, String strClient)
	//Class name		: Login
	//Description 		: Use this function to perform SSO or Non SSO logins.
	//Parameters 		: Environment, CLient and Browser
	//Assumption		: None
	//Developer			: Neelesh Vatsa
	//##################################################################################################
		
		public boolean fnWM_MainLogin(String strEnvironment,String strBrowser, String strClient, String strAccountNo)
		{
			
			try{
				
				//Check for browser type and create an object:
				if(strEnvironment.isEmpty() || strBrowser.isEmpty() ||strClient.isEmpty() )
				{
					System.out.println("class = Login_WM   |  Method = fnWM_MainLogin   | - strEnvironment/strBrowser/strClient is empty");
					return false;
				}
				
				DataTable.setExcelFile("C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\WM_TestData\\WM_Login\\WMLoginSheet.xlsx",strEnvironment);
				int intLoginRowCnt = DataTable.getRowCount();
				//System.out.println("Main Login Function Call...");
				for(int i=1;i<=intLoginRowCnt;i++){			
					if(DataTable.getCellData(i, "Client").toString().equals(strClient.toString()))
					{	//Get login credentials
						//System.out.println("Loging in for client - " + strClient);
						String strURL = DataTable.getCellData(i, "URL");
						String strLoginType = DataTable.getCellData(i, "LoginType");
						String strLoginId = DataTable.getCellData(i, "LoginId");
						String strUserName = DataTable.getCellData(i, "UserName");
						String strPwd = DataTable.getCellData(i, "Password");
						String strAcntNo;
						//Use the account no: from test data sheet if present else use default login sheet account no:
						if(!(strAccountNo.equals(""))){
							strAcntNo = strAccountNo;
						}else{
							strAcntNo = ((DataTable.getCellData(i, "Accounts")).toString());
						}
						
						// Open the URL based on login type
						//System.out.println("Login Class - URL for client = "+strClient+" is "+strURL);
						if(strLoginType.equals("SSOTrust")||strLoginType.equals("SSO")){
							if(!(strBrowser.equals("IE"))){
								strURL=strURL.replace("http://", "");
								strURL=strURL.replace("https://", "");	
								if(strPwd.contains("@")){
									strPwd = strPwd.replace("@", "%40").toString();																				
								}//End of IF for password containing "@" character
								strURL = "https://"+strUserName+":"+strPwd+"@"+strURL;
							}
 					}
						// Open the URL
						System.out.println("URL being used for Login is - " + strURL + ", with acnt no if applicable - " + strAccountNo);						
						try{
							driver = ManageDriver_WM.getManageDriver().getDriver();
							//System.out.println("strURL:"+strURL);
							driver.get(strURL);
						}catch(Exception e){
							//workaround stmnt for parallel testing for now
							System.out.println("Main Login. Driver is NULL for - " + strAccountNo);
							//re-initialize driver
							/*ManageDriver_WM manageDriver = ManageDriver_WM.getManageDriver();
							driver = manageDriver.initializeDriver(strBrowser, "");
							driver.get(strURL);
							e.printStackTrace();*/
						}
						Thread.sleep(1000);
						if(strBrowser.equals("IE")){
							WebElement element = driver.findElement(By.partialLinkText("Continue to this website "));
							element.click();
							fnIE_SSOPage(strUserName, strPwd);
						}
						
						
						//driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
						boolean flgLoginCheck = false;
						switch (strLoginType) {
							case "SSO":
								System.out.println("Checking SSO login for " + strClient);
								flgLoginCheck = fnLogin_SSO(strLoginId, strAcntNo, strClient);
								break;
								//return fnLogin_SSO(strLoginId, strAcntNo, strClient);
							case "NonSSO":
								System.out.println("Checking Non SSO login for - " + strClient);
								flgLoginCheck = fnLogin_NonSSO(strLoginId, strPwd, strClient);
								break;
								//return fnLogin_NonSSO(strLoginId, strPwd, strClient);
							default:
								System.out.println("This login Type could not be found in the login sheet - "+strLoginType);
								break;
						}
						//Check home page has loaded successfully
						if(flgLoginCheck){
							System.out.print("Waiting for Home Page to  Load");
							Thread.sleep(8000);
							SummaryPage SummaryPage = new SummaryPage();
							return SummaryPage.checkPageTitle_IsPresent(strClient);
						}else{
							return false;
						}
					}						
				}// End of FOR loop
				System.out.println("This client could not be found in the login sheet - "+strClient);
				return false;
				}//End of Try block
				catch (Exception e){
					//e.printStackTrace();
					return false;
				}//End of catch block
		}
		
		//##################################################################################################
		//Function name		: fnLogin_NonSSO(String strEnvironment,String strBrowser, String strClient)
		//Class name		: Login
		//Description 		: Use this function to perform Non SSO login.
		//Parameters 		: Environment, CLient and Browser
		//Assumption		: None
		//Developer			: Neelesh Vatsa
		//##################################################################################################
		public boolean fnLogin_NonSSO(String strloginId,String strPwd, String strClient) throws InterruptedException
		{
			if(LoginPage_WM.submitNonSso_Login(strloginId, strPwd, strClient)){
				//check for landing page element
				return true;
			}
			System.out.println("<Class: Login>Non SSO login could not be performed due to missing fields.");
			return false;
		}
		
			
		//##################################################################################################
		//Function name		: fnLogin_SSO(String strEnvironment,String strBrowser, String strClient)
		//Class name		: Login
		//Description 		: Use this function to perform SSO login.
		//Parameters 		: Environment, CLient and Browser
		//Assumption		: None
		//Developer			: Neelesh Vatsa
		//##################################################################################################
		public boolean fnLogin_SSO(String strloginId,String strAcntNo, String strClientName) throws InterruptedException
		{	
			//call function to check elements
			if(LoginPage_WM.loginPage_SSO_CheckElements_ArePresent()){
				//call sso login page function
				LoginPage_WM.submitSso_Login(strloginId, strAcntNo, strClientName);
				//check landing page element
				Thread.sleep(1000);
				//HomePg_TopPanel HomePg_TopPanel = new HomePg_TopPanel();
				//return HomePg_TopPanel.checkPrint_IsPresent();
				return true;
			}else{
				System.out.println("SSO login could not be performed due to missing fields.");
				return false;
			}
			
		}
		//##################################################################################################
		//Function name		: fnIE_SSOPage(String strEnvironment,String strBrowser, String strClient)
		//Class name		: Login
		//Description 		: Use this function to perform SSO or Non SSO logins.
		//Parameters 		: Environment, CLient and Browser
		//Assumption		: None
		//Developer			: Neelesh Vatsa
		//##################################################################################################
		public  void fnIE_SSOPage(String userName, String Pwd) throws InterruptedException
		{	
			//wait added for troubleshooting
			Thread.sleep(5000);
			driver = ManageDriver_WM.getManageDriver().getDriver();
			Alert alert = driver.switchTo().alert();
			//alert.authenticateUsing(new UserAndPassword(userName, Pwd));
		}
		
		//************************************************************************************************************
		//************************************************************************************************************
		//#######################################################################################
		//Function name		: HybridLogin_Call
		//Description 		: This function perform hybrid style login i.e. for each tets data row, 
							//..>>a login is performed only if the last iteration account no: was different from the current account no:
		//Parameters 		: row no, account no, to_execute and login page object.
		//Assumption		: To be used when all test data rows are being run
		//Created By 		: Neelesh Vatsa
		//########################################################################################	
		public boolean HybridLogin_Call(String strClient, String strEnvi, String strBrowser, int rowNum, String strAccountNo, String To_Execute, ITestContext context) throws Exception{
			//Pre-requisite variables
			//ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext(); 
			
			String doLogin ="";
			  if (rowNum == 1){
				//Perform Login if row num = 1
				  
				  context.setAttribute("currentAccountNo", strAccountNo);
				  doLogin = "Yes";
				  
				  //Change Account no: from dropdown(spl for non sso)
				 // ResultTable_Section ResultTable_Section = new ResultTable_Section();
				  //ResultTable_Section.applyFilter(rowNum,  "Account",  filterVal,  strClient);
				  
			  }else{
				  //After first row, if the account no: in current row is same as last row then skip login
				  String globalAcntNo = (String) context.getAttribute("currentAccountNo");
				  if(globalAcntNo.equals(strAccountNo)){
					  doLogin = "No";
				  }else{
					  context.setAttribute("currentAccountNo", strAccountNo);
					  doLogin ="Yes";
				  }
			  }
			  //###############  Check if login func needs to be called  ###############
			  boolean loginFlag;
			  String LoginPassed = (String) context.getAttribute("resultStatus_AllPassed");
			  if(doLogin.equals("Yes") || LoginPassed.equals("false")){
				  		//System.out.println("REACHED HYBRID LOGIN - Login Condition Pass" + rowNum);
					  loginFlag = fnWM_MainLogin(strEnvi, strBrowser, strClient, strAccountNo);
					  /*if(loginFlag == true){	
						  System.out.println("Hybrid Login was  successfully done.");
						}*/
					  if(!loginFlag == true){
						//System.out.println("Hybrid Login has failed.");
						context.setAttribute("resultStatus_AllPassed", "false");
						context.setAttribute("LoginPassed", "false");
						return false;
						//throw new SkipException("Skipping test row due to failed Login");
					  }//********  End of If-else for login Call  ********
			  }
			  else{
				  //System.out.println("Same account no: as previous iteration, thus Login was skipped");
				  loginFlag = true;
			  }
			return loginFlag;
		}//End of HybridLogin_Call function 
	//************************************************************************************************************

		

}
