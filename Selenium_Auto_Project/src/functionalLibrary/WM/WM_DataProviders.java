package functionalLibrary.WM;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.DataTable;


public class WM_DataProviders {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();;
	WM_CommonFunctions commonFunc = new WM_CommonFunctions();
	DataTable excel_Path = new DataTable();
//###################################################################################################################################################################  
//Function name		: 
//Class name		: 
//Description 		: 
//Parameters 		: context: ITestContest to read testNG.xml parameters
//Assumption		: None
//Developer			: 
//###################################################################################################################################################################		
	@DataProvider(name = "Read_TestSheetData")
	public Object[][] ReadTestSheetData(Method m)throws Exception{
		ITestContext context1 = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
		System.out.println();
		System.out.println("***********  Dataprovider - Read_TestSheetData  **********");
		System.out.println("*******************************************");
		String strClient = context1.getCurrentXmlTest().getParameter("clientName");
		String strEnvi = context1.getCurrentXmlTest().getParameter("environment");
		String strScript = context1.getCurrentXmlTest().getParameter("scriptName");
		System.out.println("Dataprovider client = " + strClient);
		System.out.println("Dataprovider Envi = " + strEnvi);
		//Get excel data sheet path for given envi, client and script combination
		String xlTestDataPath = excel_Path.getExcelDatasheetPath_WM(strScript, strEnvi, strClient);
		System.out.println("Test Data sheet path = " + xlTestDataPath);
		//new code to get sheet name
		String sheetName = (m.getName()).trim();
		System.out.println("Sheet name to be used = " + sheetName);
		Object[][] testObjArray1 = getTableArray(xlTestDataPath, sheetName);
		System.out.println("***********  End Of Dataprovider Call ***********");
		System.out.println("*************************************************");
		System.out.println();
		System.out.println("************************************************************************");
		System.out.println("*******************      Test Execution started      *******************");
		return (testObjArray1);
	  
	}//End of DataProvider 
	//************************************************************************************************************
//************************************************************************************************************
//###################################################################################################################################################################  
//Function name        	: getTableArray
//Class name        	: WM_DataProvider
//Description        	: This funxtion returns an object array required by dataprovider call.
//Parameters         	: Path of data file , sheet name
//Assumption        	: None
//Developer            : Neelesh Vatsa
//###################################################################################################################################################################  
	
	public Object[][] getTableArray(String FilePath, String SheetName) throws Exception {   

		   String[][] tabArray = null;

		   try {
			   InputStream inputStream = null;
		       inputStream = new FileInputStream(FilePath);
		       Workbook ExcelWBook = WorkbookFactory.create(inputStream);
		       Sheet ExcelWSheet = ExcelWBook.getSheet(SheetName);
			   int startRow = 1;
			   int startCol = 0;
			   int ci,cj;
			   int totalRows = ExcelWSheet.getLastRowNum();
			   System.out.println("total rows - " + totalRows);
			   int totalCols = ExcelWSheet.getRow(0).getPhysicalNumberOfCells();
			   System.out.println("total cols - " + totalCols);

			   tabArray=new String[totalRows][totalCols];
			   ci=0;
			   for (int i=startRow;i<=totalRows;i++, ci++) {           	   
				  cj=0;
				   for (int j=startCol;j<=totalCols - 1;j++, cj++){
					   //System.out.println(i);
					   //System.out.println(j);
					   tabArray[ci][cj]= commonFunc.getCellData(ExcelWSheet, i,j);
					   //System.out.println(tabArray[ci][cj]);  
						}
					}
				}
			catch (FileNotFoundException e){
				System.out.println("Could not read the Excel sheet");
				e.printStackTrace();
				}
			catch (IOException e){
				System.out.println("Could not read the Excel sheet");
				e.printStackTrace();
				}
			return(tabArray);

			}
//###################################################################################################################################################################		
}//End of DataVal_DataProviders
//###################################################################################################################################################################	
