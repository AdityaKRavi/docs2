package functionalLibrary.Maxit;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.ManageDriver_Maxit;

public class StepupUpdate_DataProvider {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	ConfigInputFile objConfigInputFile = new ConfigInputFile();
	DataTable dt = new DataTable();

	//###################################################################################################################################################################  
	//Function name		: dp_StepupUpdateTestData(ITestContext context,Method mPageName)
	//Class name		: UpdateScript_DataProvider
	//Description 		: Generic method to read data for stepup update.
	//Parameters 		: context: ITestContest to read testNG.xml parameters
	//					: mSheetName : Send calling Method (@Test method name) name as parameter
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################		
	@DataProvider(name="Read_StepupUpdateTestData")
	public Object[][] dp_StepupUpdateTestData(ITestContext context,Method mSheetName)throws Exception{
		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
		String strEnv = context.getCurrentXmlTest().getParameter("environment");
		String strClient = context.getCurrentXmlTest().getParameter("clientName");

		String excelPath = dt.getInputFIlePath_Maxit(strScriptName,strEnv,strClient);
		String strSheetName = mSheetName.getName().trim();
		System.out.println("Excel Path is :"+excelPath + " & Sheet name is " +strSheetName);
		return(this.getStepupUpdate_ScriptData(excelPath, strSheetName));

	}//End of DataProvider "dp_StepupUpdateTestData"	

	//###################################################################################################################################################################  
	//Function name		: dp_StepupDataValidationTestData(ITestContext context,Method mPageName)
	//Class name		: UpdateScript_DataProvider
	//Description 		: Generic method to read data for stepup update data validation in stepup page.
	//Parameters 		: context: ITestContest to read testNG.xml parameters
	//					: mSheetName : Send calling Method (@Test method name) name as parameter
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################		
	@DataProvider(name="Read_StepupDataValidationTestData")
	public Object[][] dp_StepupDataValidationTestData(ITestContext context,Method mSheetName)throws Exception{
		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
		String strEnv = context.getCurrentXmlTest().getParameter("environment");
		String strClient = context.getCurrentXmlTest().getParameter("clientName");

		String excelPath = dt.getInputFIlePath_Maxit(strScriptName,strEnv,strClient);
		String strSheetName = mSheetName.getName().trim();
		System.out.println("Excel Path is :"+excelPath + " & Sheet name is " +strSheetName);
		return(this.getstepUp_DataValidationData(excelPath, strSheetName));

	}//End of DataProvider "dp_StepupDataValidationTestData"

	//###################################################################################################################################################################  
	//Function name		: dp_UpdateScriptDataVal(ITestContext context,Method mPageName)
	//Class name		: UpdateScript_DataProvider
	//Description 		: Generic method to read data for stepup update data validation in admin views.
	//Parameters 		: context: ITestContest to read testNG.xml parameters
	//					: mSheetName : Send calling Method (@Test method name) name as parameter
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	@DataProvider(name="Read_UpdateScript_DataVal_TestData")
	public Object[][] dp_UpdateScriptDataVal(ITestContext context,Method mSheetName)throws Exception{
		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
		String strEnv = context.getCurrentXmlTest().getParameter("environment");
		String strClient = context.getCurrentXmlTest().getParameter("clientName");

		String excelPath = dt.getInputFIlePath_Maxit(strScriptName,strEnv,strClient);
		String strSheetName = mSheetName.getName().trim();
		System.out.println("Excel Path is :"+excelPath + " & Sheet name is " +strSheetName);
		return(this.getData_UpdateScriptDataValidation(excelPath, strSheetName));

	}//End of DataProvider "dp_UpdateScriptDataVal"	

	//###################################################################################################################################################################  
	//Function name		: getStepupUpdate_ScriptData(String strExcelPath, String strSheetName)
	//Class name		: configInputFile
	//Description 		: Generic function to read data from input file for stepup update process . 
	//Parameters 		: strExcelPath: Input excel file path
	//					: strSheetName: Name of the sheet
	//					: strViewPage : Search View page name
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public Object[][] getStepupUpdate_ScriptData(String strExcelPath, String strSheetName) throws Exception{
		Object[][] dpArray = null;
		dt.setExcelFile(strExcelPath);
		if(dt.setSheet(strSheetName)){			  		 
			int intDTRows = dt.getRowCount();
			dpArray = new Object[intDTRows][14];

			for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

				String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
				if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
					return(dpArray);
				}

				String strAcct = dt.getCellData(intDTRowIndex, "Account_No").toString() ;
				String strSecType = dt.getCellData(intDTRowIndex, "Sec_Type").toString() ;
				String strSecValue = dt.getCellData(intDTRowIndex, "Sec_Value").toString() ;
				String stepUpLevel = dt.getCellData(intDTRowIndex, "StepUp_Level").toString() ;
				String dod = dt.getCellData(intDTRowIndex, "DOD").toString() ;
				String altValuationDate = dt.getCellData(intDTRowIndex, "AltValuationDate").toString() ;
				String rowNumberToEdit = dt.getCellData(intDTRowIndex, "RowNo_ToEdit").toString() ;

				String fmvPerShare = dt.getCellData(intDTRowIndex, "FMV_Per_Share").toString() ;
				String stepPercentage = dt.getCellData(intDTRowIndex, "Step_Percent").toString() ;
				String bondSecNo = dt.getCellData(intDTRowIndex, "BondSecNo").toString() ;
				String useCaseNum = dt.getCellData(intDTRowIndex, "Use_Case").toString() ;

				dpArray[intDTRowIndex-1][0]=To_Execute;
				dpArray[intDTRowIndex-1][1]=strAcct;
				dpArray[intDTRowIndex-1][2]=strSecType;
				dpArray[intDTRowIndex-1][3]=strSecValue;
				dpArray[intDTRowIndex-1][4]=stepUpLevel;
				dpArray[intDTRowIndex-1][5]=dod;
				dpArray[intDTRowIndex-1][6]=altValuationDate;
				dpArray[intDTRowIndex-1][7]=rowNumberToEdit;
				dpArray[intDTRowIndex-1][8]=fmvPerShare;
				dpArray[intDTRowIndex-1][9]=stepPercentage;
				dpArray[intDTRowIndex-1][10]=bondSecNo;
				dpArray[intDTRowIndex-1][11]=useCaseNum;

				dpArray[intDTRowIndex-1][12]=intDTRowIndex;
				dpArray[intDTRowIndex-1][13]=strSheetName;
				System.out.println("intDTRowIndex : " +intDTRowIndex + "To_Execute : " +To_Execute+"strAcct : " +strAcct+"strSecType : " +strSecType+
						"strSecValue : " +strSecValue+"stepUpLevel : " +stepUpLevel+"dod : " +dod+
						"altValuationDate : " +altValuationDate+"rowNumberToEdit : " +rowNumberToEdit+
						"fmvPerShare : " +fmvPerShare+"stepPercentage : " +stepPercentage+"bondSecNo : " +bondSecNo
						+"useCaseNum : " +useCaseNum
						+"intDTRowIndex : " +intDTRowIndex+"strSheetName : " +strSheetName);
			}//End of for loop to iterate thru different rows	
		}//End of IF condition to check  DataTable.setSheet

		return(dpArray);
	}//End of getStepupUpdate_ScriptData Method	
	//###################################################################################################################################################################

	//###################################################################################################################################################################  
	//Function name		: getstepUp_DataValidationData(String strExcelPath,String strSheetName)
	//Class name		: configInputFile
	//Description 		: Generic function to read data from input file for stepup update  data validation in stepup page.
	//Parameters 		: strExcelPath: Input excel file path
	//					: strSheetName: Name of the sheet
	//					: strViewPage : Search View page name
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public Object[][] getstepUp_DataValidationData(String strExcelPath,String strSheetName) throws Exception{
		Object[][] dpArray = null;
		String strViewPage = "STEPUP";
		dt.setExcelFile(strExcelPath);
		if(dt.setSheet(strSheetName)){			  		 
			int intDTRows = dt.getRowCount();
			dpArray = new Object[intDTRows][13];

			for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

				String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
				if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
					return(dpArray);
				}

				String strAcct = dt.getCellData(intDTRowIndex, "Account_No").toString() ;
				String strSecType = dt.getCellData(intDTRowIndex, "Sec_Type").toString() ;
				String strSecValue = dt.getCellData(intDTRowIndex, "Sec_Value").toString() ;
				String strRowCount = dt.getCellData(intDTRowIndex, "Row_Count").toString() ;
				String strColNames = dt.getCellData(intDTRowIndex, "Column_Names").toString() ;

				String strdod = dt.getCellData(intDTRowIndex, "DoD").toString() ;
				String altValDate = dt.getCellData(intDTRowIndex, "AltValuationDate").toString() ;
				String useCaseNum = dt.getCellData(intDTRowIndex, "Use_Case").toString() ;

				dpArray[intDTRowIndex-1][0]=strViewPage;
				dpArray[intDTRowIndex-1][1]=To_Execute;
				dpArray[intDTRowIndex-1][2]=strAcct;
				dpArray[intDTRowIndex-1][3]=strSecType;
				dpArray[intDTRowIndex-1][4]=strSecValue;
				dpArray[intDTRowIndex-1][5]=strRowCount;
				dpArray[intDTRowIndex-1][6]=strColNames;
				dpArray[intDTRowIndex-1][7]=strdod;
				dpArray[intDTRowIndex-1][8]=altValDate;

				//Reading Expected rows
				int rowCnt = Integer.parseInt(strRowCount);
				String[] expRows = new String[rowCnt];
				if(rowCnt>0){
					for(int readExpRow = 1;readExpRow<=rowCnt;readExpRow++){
						expRows[readExpRow-1]= (dt.getCellData(intDTRowIndex, "Row"+readExpRow).toString());						  
					}//End of for loop to read Expected rows from Input sheet
					dpArray[intDTRowIndex-1][9]=expRows;
				}//End of IF condition to check ExpectedRow count in data sheet>0

				else {
					dpArray[intDTRowIndex-1][9] = null;
				}//End of ELSE condition to check ExpectedRow count in data sheet>0				  				  
				dpArray[intDTRowIndex-1][10]=useCaseNum;
				dpArray[intDTRowIndex-1][11]=intDTRowIndex;
				dpArray[intDTRowIndex-1][12]=strSheetName;
			}//End of for loop to iterate thru different rows	
		}//End of IF condition to check  DataTable.setSheet

		return(dpArray);
	}//End of getstepUp_DataValidationData Method


	//###################################################################################################################################################################  
	//Function name		: getData_UpdateScriptDataValidation(String strExcelPath,String strSheetName)
	//Class name		: configInputFile
	//Description 		: Generic function to read data from input file for stepup update data validation in admin views.
	//Parameters 		: strExcelPath: Input excel file path
	//					: strSheetName: Name of the sheet
	//					: strViewPage : Search View page name
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public Object[][] getData_UpdateScriptDataValidation(String strExcelPath,String strSheetName) throws Exception{
		Object[][] dpArray = null;
		String strViewPage;
		dt.setExcelFile(strExcelPath);
		if(dt.setSheet(strSheetName)){			  		 
			int intDTRows = dt.getRowCount();
			dpArray = new Object[intDTRows][11];

			for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

				String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
				if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
					return(dpArray);
				}

				String strAcct = dt.getCellData(intDTRowIndex, "Account_No").toString() ;
				String strSecType = dt.getCellData(intDTRowIndex, "Sec_Type").toString() ;
				String strSecValue = dt.getCellData(intDTRowIndex, "Sec_Value").toString() ;
				String strRowCount = dt.getCellData(intDTRowIndex, "Row_Count").toString() ;
				String strColNames = dt.getCellData(intDTRowIndex, "Column_Names").toString() ;
				String useCaseNum = dt.getCellData(intDTRowIndex, "Use_Case").toString() ;

				switch (strSheetName){
				case "Ledger":
					strViewPage = "Ledger";
					break;
				case "Realized":
					strViewPage = "Realized";
					break;
				case "Unrealized":
					strViewPage = "Unrealized";
					break;
				case "OpenClosed":
					strViewPage = "Open/Closed";
					break;
				case "RawTrades":
					strViewPage = "Raw Trades";
					break;
				default:
					strViewPage = "Ledger";	
					break;
				}//End of Switch case


				dpArray[intDTRowIndex-1][0]=strViewPage;
				dpArray[intDTRowIndex-1][1]=To_Execute;
				dpArray[intDTRowIndex-1][2]=strAcct;
				dpArray[intDTRowIndex-1][3]=strSecType;
				dpArray[intDTRowIndex-1][4]=strSecValue;
				dpArray[intDTRowIndex-1][5]=strRowCount;
				dpArray[intDTRowIndex-1][6]=strColNames;

				//Reading Expected rows
				int rowCnt = Integer.parseInt(strRowCount);
				String[] expRows = new String[rowCnt];
				if(rowCnt>0){
					for(int readExpRow = 1;readExpRow<=rowCnt;readExpRow++){
						expRows[readExpRow-1]= (dt.getCellData(intDTRowIndex, "Row"+readExpRow).toString());						  
					}//End of for loop to read Expected rows from Input sheet
					dpArray[intDTRowIndex-1][7]=expRows;
				}//End of IF condition to check ExpectedRow count in data sheet>0

				else {
					dpArray[intDTRowIndex-1][7] = null;
				}//End of ELSE condition to check ExpectedRow count in data sheet>0	
				dpArray[intDTRowIndex-1][8]=useCaseNum;
				dpArray[intDTRowIndex-1][9]=intDTRowIndex;
				dpArray[intDTRowIndex-1][10]=strSheetName;
			}//End of for loop to iterate thru different rows	
		}//End of IF condition to check  DataTable.setSheet

		return(dpArray);
	}//End of getData_UpdateScriptDataValidation Method


	//###################################################################################################################################################################
}//End of <class: StepupUpdate_DataProvider>
