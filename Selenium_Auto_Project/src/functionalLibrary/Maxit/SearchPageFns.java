package functionalLibrary.Maxit;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;


import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;


import PageObjects.Maxit.*;
import functionalLibrary.Global.DateUtils;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

 
@SuppressWarnings({})
public class SearchPageFns {
	ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();	
	Reporting reporter = new Reporting();
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	
	 SearchPage_POM SearchPage = PageFactory.initElements(driver, SearchPage_POM.class);
	 SearchPage_Results SearchPageResults = PageFactory.initElements(driver, SearchPage_Results.class);
	 
	public  boolean Search_NavigatePage(String strViewPage){
		try{
			if(!SearchPage.ClickSearchLink()){
				//CommonUtils.captureScreenshot("Search Page_Click Search Link[Failed]", "Search Page_Click Search link", "HORIZONTALLY", null);
				return false;
			}
			if(!SearchPage.VerifySearchPageTitle()){
				return false;
			}
			
			if(!SearchPage.SelectViewPage(strViewPage)){
				System.out.println("Please Input correct View page parameter.");
				return false;
			}//End of IF Condition to Select View Page
			return true;
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <SearchPageFns.Search_NavigatePage>: Unable to Navigate to Search Page:"+strViewPage+". Please Check..!!");			
			return false;
		}
				
	}//End of Search_NavigatePage method	
//*********************************************************************************************************************	
	public  boolean Search_Input(String strViewPage , String strAccount,String strSecIDType,String strSecValue,int exclRowCnt,String strSheetName ){		
		try{			
			if(!Search_NavigatePage(strViewPage)){
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strViewPage+" Search Page Navigation failed", "", "SearchPage_Navigation",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;				
			}			
			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strViewPage+" Search Page Navigation Passed", "", "SearchPage_Navigation",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return SearchPage.SearchPage_Input(strAccount, strSecIDType, strSecValue,exclRowCnt,strSheetName);						
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <SearchPageFns.Search_Input>: Search Page is not displayed in UI..!!!");
			return false;
		}
		
	}//End of Search_Input method	
//*********************************************************************************************************************	
	public  boolean Search_Input_Blank(String strViewPage , int exclRowCnt,String strSheetName ){		
		try{			
			if(!Search_NavigatePage(strViewPage)){
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strViewPage+" Search Page Navigation failed", "", "SearchPage_Navigation",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;				
			}			
			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strViewPage+" Search Page Navigation Passed", "", "SearchPage_Navigation",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return SearchPage.SearchPage_InputBlank(exclRowCnt,strSheetName);						
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <SearchPageFns.Search_Input_Blank>: Search Page is not displayed in UI..!!!");
			return false;
		}
		
	}//End of Search_Input method	
//*********************************************************************************************************************		
	public  boolean SearchRes_RowCnt_Checkpoint(int expectedRowCount,String strViewPage,int exclRowCnt,String strSheetName){
		try{
		int sysRowCount = SearchPageResults.searchRes_NumOfRows_app(strViewPage);
		if(expectedRowCount != sysRowCount){
			//System.out.println("<SearchPageFns.SearchRes_RowCnt_Checkpoint>: Expected Row Count = "+expectedRowCount+" and Actual Row Count = "+sysRowCount);
			System.out.println("<SearchPageFns.SearchRes_RowCnt_Checkpoint> : Checkpoint : Row Count [Failed].Expected Row Count = "+expectedRowCount+" and Actual Row Count = "+sysRowCount);
			reporter.reportStep(exclRowCnt, "Failed", "SearchResults_RowCount_Checkpoint failed", "", "Res_row_count", driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}
		System.out.println("Checkpoint : Row Count [Passed].Expected Row Count = "+expectedRowCount+" and Actual Row Count = "+sysRowCount);
		reporter.reportStep(exclRowCnt, "Passed", "SearchResults_RowCount_Checkpoint_Passed", "", "Res_row_count",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		return true;
		}
		catch(Exception e){
			System.out.println("Exception in <SearchPageFns.SearchRes_RowCnt_Checkpoint>..!!");
			e.printStackTrace();
			return false;
		}
	}//End of SearchRes_RowCnt_Checkpoint method
	
//*********************************************************************************************************************		
	public  String[] SearchRes_GetActualRowArray(String strColNames,int rowCnt){
		String[] actColList = strColNames.split(";");
		String [] aActualArr = new String[rowCnt];
		
		for(int i=1;i<=rowCnt;i++){
			String sActualValues ="";
			for(int iLoop=0;iLoop< actColList.length;iLoop++){
				String strColName = actColList[iLoop].trim();
				int colIndex = SearchPageResults.searchRes_GetColIndexByColName(strColName);
				String strColValue = SearchPageResults.searchRes_getColValue(i-1, colIndex) ;//fn to get Col Value from UI
				
				if(iLoop==actColList.length){
					sActualValues = (sActualValues.toString() + strColValue.toString()).trim();
				}
				else{
					sActualValues = (sActualValues.toString() + strColValue.toString() + ";").trim();
				}				
			}// End of FOR Loop to loop through ColumnsList
			//Format each row value, add it to the result array-list 
			if(sActualValues.endsWith(";")){
				sActualValues = StringUtils.left(sActualValues, sActualValues.length()-1);	
			}
				aActualArr[i - 1] = sActualValues;			
		}// End of FOR Loop to loop through RowCount in Search Results table
		
		
		return aActualArr;
	}//End of SearchRes_GetActualRowArray method	
	
//*********************************************************************************************************************
	@SuppressWarnings({"all"})
	public  void SearchResult_CompareArrays(int exclRowCnt,String[] expArray, String[] actArray, String strColNames,String strViewPage, String strSearchValues,String strSheetName){
		boolean blnFound=false;
		String strExpArrayVal="";
		String strActArrayVal = "";
		try{
		for(String expArrayVal : expArray){
			strExpArrayVal = expArrayVal.toString().trim();
			strExpArrayVal = strExpArrayVal.replaceAll("\u00A0", "");
			for(int actArryLoop=0;actArryLoop<actArray.length;actArryLoop++){
				strActArrayVal = actArray[actArryLoop].toString().trim();
				strActArrayVal = strActArrayVal.replaceAll("\u00A0", "");
				System.out.println("Actual array value actArray["+actArryLoop+"].trim() is "+ strActArrayVal);
				System.out.println("Expected array value expArray is "+ strExpArrayVal);
				
				if(strExpArrayVal.equals(strActArrayVal)){
					blnFound = true;
					break;
				}//End of IF condition to compare Each Expected value with Actual Array value
			}//For loop for each expectedArray value
	        
			Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
	        Date date = new Date();
			if(blnFound){
				System.out.println(strViewPage+": Compare row values for " + strSearchValues +"[Passed]. Expected Row Value = ["+ strExpArrayVal +"] as Actual Row Value.");
				reporter.reportStep(exclRowCnt, "Passed", "Row Compare Passed", "", "Row Compare Passed",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}
			else{
				System.out.println(strViewPage+": Compare row values for " + strSearchValues +"[Failed]. Expected Row Value = ["+ strExpArrayVal +"] NOT as Actual Row Value.Column Names compared are ["+strColNames+"]. Please check screenshot.");	
				reporter.reportStep(exclRowCnt, "Failed", "Row_Compare_Failed", "", "Row_Compare",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}
		}//For loop for each expectedArray value
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in <SearchResult_CompareArrays> method..!!");
			e.printStackTrace();
		}
	}//End of SearchResult_CompareArrays method
	
//*********************************************************************************************************************		
	public  void SearchResult_DataValidation(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows,String strSheetName ){
		String[] aActualArr;	
		String strSearchValues = "["+strSheetName+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
		int expectedRowCount = Integer.parseInt(strRowCount);
		System.out.println("\n#########################["+strViewPage+"]Data Valiadtion: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				
			if(this.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName)){
				if(this.SearchRes_RowCnt_Checkpoint(expectedRowCount,strViewPage,exclRowCnt,strSheetName)){
					if(expectedRowCount!=0){
						aActualArr = this.SearchRes_GetActualRowArray(strColNames, expectedRowCount);					
						this.SearchResult_CompareArrays(exclRowCnt,strExpRows, aActualArr, strColNames, strViewPage, strSearchValues,strSheetName);
					}//Check for RowCount not zero to call comparison method.
				}//End of IF condition to check RowCount checkpoint
			}//End of IF condition to check Search_Input Checkpoint

			
			//Phase 2:
			//COde for UGL G/l & Term validation:
			for(int i=1;i<=expectedRowCount;i++){
				if(strViewPage.toLowerCase().contains("unrealized")){
					UGL_CheckGainLoss(exclRowCnt,strSheetName,i);	
					UGL_Checkpoint_CheckTerm(exclRowCnt,strSheetName,i);
				}//End of IF condition to check if the page is UGL
				
			}//End of FOR loop for UGL verification
									
		System.out.println("###########################################################[End of Data Valiadtion: Row iteration:["+exclRowCnt+"]###########################################################");

		//return false;
	}//End of SearchResult_DataValidation method

//*****************************************************************************************************************************************************************
	public void UGL_CheckGainLoss(int intExclRowNum,String strSheetName,int expectedRowCount){
		String strActualTerm, strTermFalg,strActualCost,strActualMarketValue, strActualLT_GL, strActualST_GL;
		String strExpectedGainLoss;
		int colIndex;
		
		//Read Actual Term from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("Term");
		strActualTerm = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
		
		if(strActualTerm.toLowerCase().contains("short")){
			strTermFalg = "ST G/L";
		}//IF condition to check if Term=Short
		else{
			strTermFalg = "LT G/L";
		}//IF condition to check if Term <> Short

		//Read Actual Cost from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("Cost");
		strActualCost = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
		
		//Read Market Value from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("Market Value");
		strActualMarketValue = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
		
		//Read LT G/L value from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("LT G/L");
		strActualLT_GL = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
		
		//Read ST G/L value from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("ST G/L");
		strActualST_GL = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
		
		strExpectedGainLoss = UGL_Calculate_GL(strActualCost,strActualMarketValue);
		double dblExpGL = fn_FormalNumber(strExpectedGainLoss);
		
		if(strTermFalg.equalsIgnoreCase("ST G/L")){
			UGL_GainLoss_RoundOff(intExclRowNum,dblExpGL, strActualST_GL,"Short Term G/L", strSheetName,expectedRowCount);
		}
		else{
			UGL_GainLoss_RoundOff(intExclRowNum,dblExpGL, strActualLT_GL,"Long Term G/L", strSheetName,expectedRowCount);
		}
		
		
	}//End of UGL_CheckGainLoss method
	
//*****************************************************************************************************************************************************************	
private String UGL_Calculate_GL(String strActualCost,String strActualMarketValue){
	String strExpectedGainLoss = "0.00";
	
	//Checkpoint to verify DUAL cost for Gifting Rules
	if(strActualCost.contains("Dual")){		
		strExpectedGainLoss = UGL_CalGL_Dual(strActualCost,strActualMarketValue);
	}//IF Condition to check if ActualCost contain "Dual"
	else{
		strExpectedGainLoss = UGL_CalGL_NonDual(strActualCost,strActualMarketValue);
	}//End of else condition to check if ActualCost contain "Dual"
	return strExpectedGainLoss;
}//End of <Method: UGL_Calculate_GL>
	
//*****************************************************************************************************************************************************************
private String UGL_CalGL_Dual(String strActualCost,String strActualMarketValue){
	try{
		double dblDonorBasis, dblFMV,dblExpectedGainLoss = 0.00;
		String stExpectedGainLoss = "0.00";
		
		//DualHigh Basis:$8,000.00Low Basis:$7,000.00
		strActualCost = strActualCost.replace("$", "");
		strActualCost = strActualCost.replace(",", "");
		String[] arrTemp = strActualCost.split(":");
		
		strActualMarketValue = strActualMarketValue.replace("$", "");
		strActualMarketValue = strActualMarketValue.replace(",", "");
	
		//strDonorBasis = 8000.00Low Basis
		int endIndex = (arrTemp[1].indexOf("Low")-1);
		String strDonorBasis = arrTemp[1].substring(0, endIndex);
		
		dblDonorBasis = Double.parseDouble(strDonorBasis);
		dblFMV = Double.parseDouble(arrTemp[2]);
		
		double dblActualMarketValue = Double.parseDouble(strActualMarketValue);
		
		if((dblDonorBasis > dblFMV) && (dblFMV > dblActualMarketValue)) dblExpectedGainLoss = dblActualMarketValue - dblFMV;	
		else if ((dblDonorBasis > dblActualMarketValue) && (dblActualMarketValue > dblFMV)) dblExpectedGainLoss = 0.00;				
		else if ((dblActualMarketValue > dblDonorBasis) && (dblDonorBasis > dblFMV)) dblExpectedGainLoss = dblActualMarketValue - dblDonorBasis;	
		else dblExpectedGainLoss = 0.00;
		
		stExpectedGainLoss = Double.toString(dblExpectedGainLoss);	
		return stExpectedGainLoss ; 
	}//End of Try block
	
	catch(NumberFormatException e){
		System.out.println("NumberFormatException in <Method: UGL_CalGL_Dual>, please check..!!");
		return "";
	}
	catch(Exception e){
		System.out.println("Exception in <Method: UGL_CalGL_Dual>, please check..!!");
		e.printStackTrace();
		return "";
	}
	
}//End of <Methid: UGL_CalGL_Dual>

//*****************************************************************************************************************************************************************
private String UGL_CalGL_NonDual(String strExpCost,String strMarketValue) throws NumberFormatException{
	try{
		double dblExpectedGainLoss =0.00 , dblMarketValue, dblExpCost;	
		String strNegSign = "",strMvalSign = "+" , strCostSign = "+";
		
		if(strMarketValue.contains("N")) strMarketValue = "0";
		if(strMarketValue.substring(0, 1).equals("-")) strMvalSign = "-"; // Check Market Value Sign
		if(strExpCost.substring(0, 1).equals("-")) strCostSign = "-";// Check Cost Sign
		
		//Remove '-' sign and '$' sign from values for  GainLoss calculation
		strMarketValue = strMarketValue.replace("-", ""); 
		strMarketValue = strMarketValue.replace("$", "");
		strMarketValue = strMarketValue.replace(",", "");
		dblMarketValue = Double.parseDouble(strMarketValue)	;	
	
			
		strExpCost = strExpCost.replace("-", ""); 
		strExpCost = strExpCost.replace("$", "");
		strExpCost = strExpCost.replace(",", "");
		dblExpCost = Double.parseDouble(strExpCost)	;
		
		   //Determine the "-, +" sign of  final GainLoss value
			if((dblMarketValue > dblExpCost) && (strMvalSign.equals("-"))  && (strCostSign.equals("-"))) { strNegSign = "-"; }		
			else if((dblMarketValue < dblExpCost) && (strMvalSign.equals("-"))  && (strCostSign.equals("-"))) strNegSign = "";
			else if((dblMarketValue > dblExpCost) && (strMvalSign.equals("-"))  && (strCostSign.equals("+"))) strNegSign = "-";
			else if((dblMarketValue < dblExpCost) && (strMvalSign.equals("+"))  && (strCostSign.equals("+"))) strNegSign = "-";
			else if((dblMarketValue > dblExpCost) && (strMvalSign.equals("+"))  && (strCostSign.equals("-"))) strNegSign = "";
			else if((dblMarketValue < dblExpCost) && (strMvalSign.equals("+"))  && (strCostSign.equals("-"))) strNegSign = "";
			else if((dblMarketValue > dblExpCost) && (strMvalSign.equals("+"))  && (strCostSign.equals("+"))) strNegSign = "";
			else if((dblMarketValue < dblExpCost) && (strMvalSign.equals("+"))  && (strCostSign.equals("+"))) strNegSign = "-";
			else strNegSign = "";
	
			dblExpectedGainLoss = Math.abs(dblMarketValue - dblExpCost);
			DecimalFormat df = new DecimalFormat("####0.00");
			String strExpectedGainLoss = df.format(dblExpectedGainLoss);		
			String strExpGL = strNegSign+"$"+strExpectedGainLoss;
			//System.out.println("<UGL_CalGL_NonDual>: dblExpGL = "+strExpGL);
			return strExpGL; 	
	}//End of Try block
	catch(NumberFormatException e){
		System.out.println("NumberFormatException in <Method: UGL_CalGL_NonDual>, please check..!!");
		return "";
	}
	catch(Exception e){
		System.out.println("Exception in <Method: UGL_CalGL_NonDual>, please check..!!");
		e.printStackTrace();
		return "";
	}
		
}//End of <Method: UGL_CalGL_NonDual>

//*****************************************************************************************************************************************************************
public double fn_FormalNumber(String dblExpGL){
	String strResult;
	double dblRes = 0.00;
	if(dblExpGL.isEmpty() || dblExpGL.equalsIgnoreCase("0.00")||dblExpGL ==null){
		return dblRes;
	}
	strResult = dblExpGL.replace("$", ""); 
	strResult = strResult.replace(",", ""); 

	return Double.parseDouble(strResult);
}

//*****************************************************************************************************************************************************************
public void UGL_GainLoss_RoundOff(int intExclRowNum,double strExpected, String strActual,String strGL_term, String strSheetName,int expectedRowCount){
	try{	
		String strGL_Temp ;
		if(strGL_term.toLowerCase()=="short term g/l") strGL_Temp = "ShortTerm";
		else  strGL_Temp = "LongTerm";
		
		String strExpectedGL = Double.toString(strExpected).replace("$", "");
		strExpectedGL = strExpectedGL.replace(",", "");	
		String strActualGL = strActual.replace("$", "");
		strActualGL = strActualGL.replace(",", "");
	
			if(strExpectedGL.equalsIgnoreCase(strActualGL)){
				System.out.println("UGL page[Row#"+expectedRowCount+"]: GL checkpoint_Pass. [Actual G/L:"+strActualGL+"] and Expected G/L:["+strExpectedGL+"]");
				reporter.reportStep(intExclRowNum, "Passed", "Unrealized:G/L["+strGL_term+"] checkpoint pass ,Actual ["+strActual+"] and Expected ["+strExpected+"].", "", "UGL "+strGL_Temp+"_Pass", driver, "HORIZONTALLY", strSheetName, null);
			}
			else{
				double dblExpectedGL = Double.parseDouble(strExpectedGL);
				double dblActualGL = Double.parseDouble(strActualGL);	
				double dblTemp = dblExpectedGL - dblActualGL;
				if(Math.abs(dblTemp)==0.0 || Math.abs(dblTemp)==0.00 ){
					System.out.println("UGL page[Row#"+expectedRowCount+"]: GL checkpoint_Pass. [Actual G/L:"+dblActualGL+"] and Expected G/L:["+dblExpectedGL+"]");
					reporter.reportStep(intExclRowNum, "Passed", "Unrealized:G/L["+strGL_term+"] checkpoint pass ,Actual ["+strActual+"] and Expected ["+strExpected+"].", "", "UGL "+strGL_Temp+"_Pass", driver, "HORIZONTALLY", strSheetName, null);
				}
				else if(Math.abs(dblTemp)==0.01){
					System.out.println("UGL page[Row#"+expectedRowCount+"]: GL checkpoint_Pass. [Actual G/L:"+dblActualGL+"] and Expected G/L:["+dblExpectedGL+"]");
					reporter.reportStep(intExclRowNum, "Passed", "Unrealized:G/L["+strGL_term+"] checkpoint pass with threashold of 0.01 due to Market Value round off,Actual ["+strActual+"] and Expected ["+strExpected+"].", "", "UGL "+strGL_Temp+"_Pass", driver, "HORIZONTALLY", strSheetName, null);
				}
				else{
					System.out.println("UGL page[Row#"+expectedRowCount+"]: GL checkpoint_Failed. [Actual G/L:"+dblActualGL+"] and Expected G/L:["+dblExpectedGL+"]");
					reporter.reportStep(intExclRowNum, "Failed", "Unrealized:G/L["+strGL_term+"] checkpoint fail with threashold of 0.01 due to Market Value round off,Actual ["+strActual+"] and Expected ["+strExpected+"].", "", "UGL "+strGL_Temp+"_Fail", driver, "HORIZONTALLY", strSheetName, null);
				}
			}//End of else condition to compare G/L

	}//End of try block
	catch(Exception e){
		System.out.println("Exception in <Method: UGL_GainLoss_RoundOff>, please check details below.");
		e.printStackTrace();
	}
}//End of <Method: UGL_GainLoss_RoundOff>

//*****************************************************************************************************************************************************************
private void UGL_Checkpoint_CheckTerm(int intExclRowNum,String strSheetName,int expectedRowCount){
	try{
		String strActualTerm, strActOpenDate,strActualQTy , strExpectedTerm;
		int colIndex;
		
		//Read Actual Term from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("Term");
		strActualTerm = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
		
		//Read Open Date from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("Open Date");
		strActOpenDate = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
	
		//Read Actual Qty from UI
		colIndex = SearchPageResults.searchRes_GetColIndexByColName("Qty");
		strActualQTy = SearchPageResults.searchRes_getColValue(expectedRowCount-1, colIndex) ;//fn to get Col Value from UI
		
	    //Calculate expected Term by  function
	    strExpectedTerm = UGL_CalculateTerm(strActOpenDate, strActualQTy) ;
	    if(strActualTerm.equalsIgnoreCase(strExpectedTerm)){
	    	System.out.println("UGL page[Row#"+expectedRowCount+"]: Term checkpoint_Passed. [Actual Term:"+strActualTerm+"] and Expected G/L:["+strExpectedTerm+"]");
			reporter.reportStep(intExclRowNum, "Passed", "Unrealized:Term checkpoint Pass. Actual ["+strActualTerm+"] and Expected ["+strExpectedTerm+"].", "", "UGL Term_Pass", driver, "HORIZONTALLY", strSheetName, null);
	    }
	    else{
	    	System.out.println("UGL page[Row#"+expectedRowCount+"]: Term checkpoint_Failed. [Actual Term:"+strActualTerm+"] and Expected G/L:["+strExpectedTerm+"]");
			reporter.reportStep(intExclRowNum, "Passed", "Unrealized:Term checkpoint Fail. Actual ["+strActualTerm+"] and Expected ["+strExpectedTerm+"].", "", "UGL Term_Fail", driver, "HORIZONTALLY", strSheetName, null);
	    }
	}//End of try block
	catch(Exception e){
		System.out.println("Exception in <Method: UGL_Checkpoint_CheckTerm>, please check details below.");
		e.printStackTrace();
	}

    
	
}//End of <Method: UGL_Checkpoint_CheckTerm >

//*****************************************************************************************************************************************************************
private String UGL_CalculateTerm(String strActOpenDate,String strActualQTy ){
	DateUtils dtUtils = new DateUtils();
	Date Current_date = new Date();
	Date dtActualOpenDate = dtUtils.convertString_to_Date(strActOpenDate);
	
	if(strActualQTy.substring(0, 1).equals("-"))	return "Short";		
	else if(dtUtils.dateDiff(dtActualOpenDate, Current_date, "m")> 12) return "Long";	
	else if (dtUtils.dateDiff(dtActualOpenDate, Current_date, "m")< 12) return "Short";
	//Handling Leap year:
	else if(dtUtils.dateDiff(dtActualOpenDate, Current_date, "m")==12){
		if(dtUtils.getDatePart(dtActualOpenDate, "d") <= dtUtils.getDatePart(Current_date, "d")) return "Short";	
		else return "Long";
	}//End of Else IF for handling Leap year
	
	return null;
}

//*****************************************************************************************************************************************************************
}//End of <Class: SearchPageFns>
