package functionalLibrary.Maxit;

//import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;

import functionalLibrary.Global.*;
import PageObjects.Maxit.*;


public class Login_Maxit {
	//static Logger log = Logger.getLogger(Login_Maxit.class);
	WebDriver driver;
	ManageDriver_Maxit manageDriver;
	DataTable dt;
	LoginPage_Maxit objLoginPage;
	CommonUtils CommonUtils = new CommonUtils();
	Reporting reporter = new Reporting();
	
	public Login_Maxit() throws Exception{		
		manageDriver = ManageDriver_Maxit.getManageDriver();
		driver = manageDriver.getDriver();
		dt = new DataTable();
		objLoginPage = new LoginPage_Maxit();
	}//End of Login method

	public Login_Maxit(String strBrowser) throws Exception{		
		manageDriver = ManageDriver_Maxit.getManageDriver();
		dt = new DataTable();
		objLoginPage = new LoginPage_Maxit();
		//manageDriver.initializeDriver(strBrowser);
		//driver = manageDriver.getDriver();
	}	
//###################################################################################################################################################################  
//Function name		: fnLogin(String strEnvironment,String strBrowser, String strClient)
//Class name		: Login
//Description 		: Use this function to Invoke the browser and login to application
//Parameters 		: N/A
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################	
	public boolean fnLogin(String strEnvironment,String strBrowser, String strClient,ITestContext context)
	{		
		try{
			//Check for browser type and create an object:
			if(strEnvironment.isEmpty() || strBrowser.isEmpty() ||strClient.isEmpty() )
			{
				System.out.println("<METHOD: fnLogin><CLASS: Login> - strEnvironment/strBrowser/strClient is empty");
				//log.info("<METHOD: fnLogin><CLASS: Login> - strEnvironment/strBrowser/strClient is empty");
				return false;
			}
			
			//dt.setExcelFile("C:\\SVN_Automation_New\\Trunk\\Maxit_TestData\\Maxit_Login\\Sites_v1.xls",strEnvironment);
			dt.setExcelFile(System.getProperty("testDataPath") + "/Maxit_Login/Sites.xlsx",strEnvironment);
			int intLoginRowCnt = dt.getRowCount();
			for(int i=1;i<=intLoginRowCnt;i++){			
				if(dt.getCellData(i, "Client").toString().equals(strClient.toString()))
				{					
					if(dt.getCellData(i, "SetProxy").toString().toUpperCase().equals("YES")){
						manageDriver.initializeDriver(strBrowser,"setProxy_ON");						
					}
					else{
						manageDriver.initializeDriver(strBrowser,"setProxy_OFF");					
					}
					driver = manageDriver.getDriver();
					//manageDriver.initializeDriver(strBrowser);
					String strURL=dt.getCellData(i, "URL");
					System.out.println("URL for client: "+strClient+" is "+strURL);
					//log.info("URL for client: "+strClient+" is "+strURL);
					if(dt.getCellData(i, "LoginType").equals("SSO")){						
						strURL=strURL.replace("http://", "");
						strURL=strURL.replace("https://", "");					
						String password = dt.getCellData(i, "Password");
						if(password.contains("@")){
							password = password.replace("@", "%40").toString();																				
						}//End of IF for password containing "@" character
						strURL = "https://"+dt.getCellData(i, "UserName")+":"+password+"@"+strURL;
						//System.out.println(strURL);
						System.out.println("URL for client: "+strClient+" is "+strURL);
					}//End of IF condition to check if Login Type=SSOTrust
						
					driver.get(strURL);						
					Thread.sleep(1000);
					driver.manage().window().maximize();
					switch (dt.getCellData(i, "LoginType").toUpperCase()) {
						case "NONSSO":
							return fnLogin_NonSSO_v1(i,context);
						case "SSO":
							return fnLogin_SSO(i,context);
						case "SITEMINDER":
							return fnLogin_SiteMinder(i,context); 
						default:
							return false;
						
							
					}//End of Switch condition for Login type	
				}//End of IF to check client name						
			}//End of for loop for total number of rows
	
			return false;
		}//End of Try block
		catch (Exception e){
			e.printStackTrace();
			return false;
		}//End of catch block

	} // End of "fnLogin" method
		
//###################################################################################################################################################################  
//Function name		: fnLogin_NonSSO_v1(int intDTLogin_Row)
//Class name		: Login
//Description 		: Generic function to login to Maxit NonSSO Clients
//Parameters 		: N/A
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
	private boolean fnLogin_NonSSO_v1(int intDTLogin_Row,ITestContext context){
		//ITestContext context;
		String strUserName="",strPassword="", strClient ;
		//Read values from LoginSheet
		try {					
			strUserName = dt.getCellData(intDTLogin_Row, "UserName");
			strPassword = dt.getCellData(intDTLogin_Row, "Password");
			strClient = dt.getCellData(intDTLogin_Row, "Client");
			if((strUserName.equals("Column not found")||strUserName.equals("")) || (strPassword.equals("Column not found")||strPassword.equals("")) || (strClient.equals("Column not found")||strClient.equals("")) ){
				System.out.println("Exiting fnLogin_NonSSO as the credentials are not read from excel due to 'Column not found/other exception',please check..!!Client:"+strClient+";Login ID:"+strUserName+";Password:"+strPassword);
				return false;
			}//End of IF condition to check if Credentials are retrieved.
			System.out.println("Logging into Maxit client(NonSSO):"+strClient+";LoginID:"+strUserName+";Password:"+strPassword);
			
			//NonSSO CLient: Verify & Enter User name
			if(objLoginPage.VerifyUserNameExists(driver)){
				//CommonUtils.captureScreenshot( driver, "Verify UserName Exists", "Verifying User Name field exists in Login page", "", objLoginPage.txt_UserName);
				if(!objLoginPage.EnterUserName(driver,strUserName)){
					//String strScreenshotPath = CommonUtils.captureScreenshot( driver, "Verify UserName Exists", "Verifying User Name field exists in Login page", "", objLoginPage.txt_UserName);
					//System.out.println("Screenshot path is:"+strScreenshotPath);
					return false;
				}//End of if to check return value of "EnterUserName" method
			}//End of if to check return value of "VerifyUserNameExists" method

			//NonSSO CLient: Verify & Enter Password
			if(objLoginPage.VerifyPasswordExists(driver)){
				if(!objLoginPage.EnterPassword(driver,strPassword)){
					return false;
				}//End of if to check return value of "EnterPassword" method
			}//End of if to check return value of "VerifyPasswordExists" method

			//NonSSO CLient: Verify & Click on Login button
			if(objLoginPage.VerifyLoginExists(driver)){
				if(!objLoginPage.ClickLogin(driver)){
					return false;
				}//End of if to check return value of "ClickLogin" method
			}//End of if to check return value of "VerifyLoginExists" method
		
			//NonSSO CLient: Verify if Login is success by verifying SciVantage Logo is displayed.
			//if(objLoginPage.VerifyScivanLogo(driver)){
			if(objLoginPage.VerifyScivanLogo(driver)||objLoginPage.VerifyPageTitle(driver)){
				System.out.println("<METHOD: fnLogin_NonSSO_v1><CLASS: Login> - Login is to client "+strClient+" is successful");
				return true;
			}//End of IF condition to Verify if Login is success by verifying SciVantage Logo is displayed.
			else{
				System.out.println("<METHOD: fnLogin_NonSSO_v1><CLASS: Login> - Login is to client "+strClient+" is NOT successful..!!");
				//String strScreenshotPath = CommonUtils.captureScreenshot( driver, "Login Unsuccessful", "Login Unsuccessful", "", null);
				return false;
			}	
			
		}//End of try Block
		
		catch (Exception e){
			System.out.println("<METHOD: fnLogin_NonSSO><CLASS: Login> - Exception, please check!!!!");	
			e.printStackTrace();			
			return false;
			
		}//End of Catch Block
	}//End of fnLogin_NonSSO_v1 method		

//###################################################################################################################################################################  
//Function name		: fnLogin_SSO(int intDTLogin_Row)
//Class name		: Login
//Description 		: Generic function to login to Maxit SSO Clients
//Parameters 		: N/A
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
	private boolean fnLogin_SSO(int intDTLogin_Row,ITestContext context){
		String strUserName="",strPassword="", strClient ;
		//Read values from LoginSheet
		try {					
			strUserName = dt.getCellData(intDTLogin_Row, "UserName");
			strPassword = dt.getCellData(intDTLogin_Row, "Password");
			strClient = dt.getCellData(intDTLogin_Row, "Client");
			if((strUserName.equals("Column not found")||strUserName.equals("")) || (strPassword.equals("Column not found")||strPassword.equals("")) || (strClient.equals("Column not found")||strClient.equals("")) ){
				System.out.println("Exiting fnLogin_SSO as the credentials are not read from excel due to 'Column not found/other exception',please check..!! ");
				System.out.println("Client:"+strClient+";Login ID:"+strUserName+";Password:"+strPassword);
				return false;
			}//End of IF condition to check if Credentials are retrieved.
			System.out.println("Logging into Maxit client(SSO):"+strClient+";LoginID:"+strUserName+";Password:"+strPassword);
			
			if(!CommonUtils.acceptAlert(driver)){
				return false;
			}
			
			//SSO CLient: Verify & Enter User name in Single Sign-On Test Page
			if(objLoginPage.VerifyUserNameExists(driver)){
				if(!objLoginPage.EnterUserName(driver,"AutoTest")){
					return false;
				}//End of if to check return value of "EnterUserName" method
			}//End of if to check return value of "VerifyUserNameExists" method

			if(!objLoginPage.VerifySSO_CheckALL_Click(driver)){
					return false;
			}//End of if to check return value of "VerifyUserNameExists" method			
			
			if(objLoginPage.VerifyLoginExists(driver)){
				if(!objLoginPage.ClickLogin(driver)){
					return false;
				}//End of if to check return value of "ClickLogin" method
			}//End of if to check return value of "VerifyLoginExists" method
		
			//NonSSO CLient: Verify if Login is success by verifying SciVantage Logo is displayed.
			if(objLoginPage.VerifyScivanLogo(driver)||objLoginPage.VerifyPageTitle(driver)){
				System.out.println("<METHOD: fnLogin_SSO><CLASS: Login> - Login is to client "+strClient+" is successful");
				return true;
			}//End of IF condition to Verify if Login is success by verifying SciVantage Logo is displayed.
			else{
				System.out.println("<METHOD: fnLogin_SSO><CLASS: Login> - Login is to client "+strClient+" is NOT successful..!!");	
				return false;
			}

		}//End of try Block		
		
		catch (Exception e){
			System.out.println("<METHOD: fnLogin_SSO><CLASS: Login> - Exception, please check!!!!");	
			e.printStackTrace();			
			return false;
			
		}//End of Catch Block
	}//End of fnLogin_SSO method	

//###################################################################################################################################################################  
//Function name		: fnLogin_SiteMinder(int intDTLogin_Row)
//Class name		: Login
//Description 		: Generic function to login to Maxit SiteMinder(Endeavour clients with Foxy Proxy) Clients
//Parameters 		: N/A
//Assumption		: These clients can be logged into Firefox only(List of Prod clients : EDJ,APX,CTR,FMS,ING,LEU,MSC,PPR,PPL,SFR,SFB,SCH,SFM,STC,SWN,WLF)
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
	private boolean fnLogin_SiteMinder(int intDTLogin_Row, ITestContext context){
		String strUserName="",strPassword="", strClient ;
		//Read values from LoginSheet
		try {					
			strUserName = dt.getCellData(intDTLogin_Row, "UserName");
			strPassword = dt.getCellData(intDTLogin_Row, "Password");
			strClient = dt.getCellData(intDTLogin_Row, "Client");
			if((strUserName.equals("Column not found")||strUserName.equals("")) || (strPassword.equals("Column not found")||strPassword.equals("")) || (strClient.equals("Column not found")||strClient.equals("")) ){
				System.out.println("Exiting fnLogin_SiteMinder as the credentials are not read from excel due to 'Column not found/other exception',please check..!! ");
				System.out.println("Client:"+strClient+";Login ID:"+strUserName+";Password:"+strPassword);
				return false;
			}//End of IF condition to check if Credentials are retrieved.
			System.out.println("Logging into Maxit client(SiteMinder):"+strClient+";LoginID:"+strUserName+";Password:"+strPassword);
			
//			if(!CommonUtils.acceptAlert(driver)){
//				return false;
//			}//Checkpoint to see if Alert exists and Accept it

			if(!objLoginPage.EnterUserID(driver,strUserName)){
				return false;
			}//End of if to check return value of "EnterUserID" method
			
			if(!objLoginPage.EnterUserPwrd(driver,strPassword)){
				return false;
			}//End of if to check return value of "EnterUserPwrd" method
		
			if(!objLoginPage.ClickEndeavourLogin(driver)){
				return false;				
			}//End of if to check return value of "ClickEndeavourLogin" method
		
			//SiteMinder CLient: Verify if Login is success by verifying SciVantage Logo is displayed.
			//if(objLoginPage.VerifyScivanLogo(driver)){
			if(objLoginPage.VerifyScivanLogo(driver)||objLoginPage.VerifyPageTitle(driver)){
				System.out.println("<METHOD: fnLogin_SiteMinder><CLASS: Login> - Login is to client "+strClient+" is successful");				
				return true;
			}//End of IF condition to Verify if Login is success by verifying SciVantage Logo is displayed.
			else{
				System.out.println("<METHOD: fnLogin_SiteMinder><CLASS: Login> - Login is to client "+strClient+" is NOT successful..!!");	
				return false;
			}

		}//End of try Block		
		
		catch (Exception e){
			System.out.println("<METHOD: fnLogin_SiteMinder><CLASS: Login> - Exception, please check!!!!");	
			e.printStackTrace();			
			return false;
			
		}//End of Catch Block
	}//End of fnLogin_SiteMinder method	
	
	//###################################################################################################################################################################  
	//Function name		: fnLogin_QuickSmoke(String strURL,String strEnv,String strClient,String strBrowser,String strLoginType,String strUserName,String strPassword,String strProxy,int rowcounter,ITestContext context)
	//Class name		: Login
	//Description 		: Use this function to Invoke the browser and login to application
	//Parameters 		: N/A
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################	
		public boolean fnLogin_QuickSmoke(String strURL,String strEnv,String strClient,String strBrowser,String strLoginType,String strUserName,String strPassword,String strProxy,int rowcounter,ITestContext context)
		{	
			String password = strPassword;
			try{
				//Check for browser type and create an object:
				if(strBrowser.isEmpty() ||strLoginType.isEmpty()||strURL.isEmpty()||strUserName.isEmpty() ||strPassword.isEmpty()||strProxy.isEmpty())
				{
					System.out.println("<METHOD: fnLogin><CLASS: Login> - strBrowser/strLoginType/strUrl/setProxy/credentials is empty");
					return false;
				}
				System.out.println("URL for client: "+strClient+" is "+strURL);
				if(strLoginType.equalsIgnoreCase("SSO")){						
					strURL=strURL.replace("http://", "");
					strURL=strURL.replace("https://", "");					
					if(password.contains("@")){
						password = password.replace("@", "%40").toString();																				
					}//End of IF for password containing "@" character
					strURL = "https://"+strUserName+":"+password+"@"+strURL;
					System.out.println("SSO URL for client with Auth : "+strClient+" is "+strURL);
				}//End of IF condition to check if Login Type=SSOTrust
				
				if(strProxy.toString().toUpperCase().equals("YES")) manageDriver.initializeDriver(strBrowser,"setProxy_ON");						
				else manageDriver.initializeDriver(strBrowser,"setProxy_OFF");		
				driver = manageDriver.getDriver();
				driver.get(strURL);
				Thread.sleep(1000);
				driver.manage().window().maximize();
				
				
				switch (strLoginType.toUpperCase()) {
					case "NONSSO":
						return QuickSmokeLogin_NonSSO(strClient,strUserName,password,rowcounter,strEnv);
					case "SSO":
						return QuickSmokeLogin_SSO(strClient,strUserName,password,rowcounter,strEnv);
					case "SITEMINDER":
						return QuickSmokeLogin_SiteMinder(strClient,strUserName,password,rowcounter,strEnv); 
					default:
						return false;
					
						
				}//End of Switch condition for Login type	
						

		
			}//End of Try block

			
			catch (Exception e){
				e.printStackTrace();
				return false;
			}//End of catch block

		} // End of "fnLogin_QuickSmoke" method
	
	//###################################################################################################################################################################  
	//Function name		: QuickSmokeLogin_SSO(int intDTLogin_Row)
	//Class name		: Login
	//Description 		: Generic function to login to Maxit SSO Clients
	//Parameters 		: N/A
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
		private boolean QuickSmokeLogin_SSO(String strClient,String strUserName,String strPassword,int excelRow,String strSheetName){
			try {					
				System.out.println("Logging into Maxit client(SSO):"+strClient+";LoginID:"+strUserName+";Password:"+strPassword);
				
				if(!(CommonUtils.acceptAlert(driver))){
					return false;
				}
				
				//SSO CLient: Verify & Enter User name in Single Sign-On Test Page
					if(objLoginPage.VerifyUserNameExists(driver)){
					if(!objLoginPage.EnterUserName(driver,"AutoTest")){
						return false;
					}//End of if to check return value of "EnterUserName" method
				}//End of if to check return value of "VerifyUserNameExists" method
				
					else{
						String strErrors = objLoginPage.SiteNotReachable(driver);
						if(!(strErrors.isEmpty()||strErrors==null || strErrors=="")){
							System.out.println("Login errors displayed, Error message is:'"+strErrors+"'. Login to client "+strClient+" is NOT successful..!!");	
							reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login Error Message:"+strErrors+" is displayed. Please check.! If client is RJF prod, please add '170.12.112.236 cbaadm.rjf.prod.scivantage.com' to hosts file.", "", "LoginError",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return false;
						}	
						else{
							reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] User Name field is NOT found, 'NoSuchElementException' Exception in <Login_Maxit><QuickSmokeLogin_SSO> method, please check.", "", "LoginError",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return false;
						}

					}
//				//Checkpoint to catch "This site can't be reached error [ For RJF when is not added to hosts]
//				catch(org.openqa.selenium.NoSuchElementException e) {
//					String strErrors = objLoginPage.SiteNotReachable(driver);
//					if(!(strErrors.isEmpty()||strErrors==null || strErrors=="")){
//						System.out.println("Login errors displayed, Error message is:'"+strErrors+"'. Login to client "+strClient+" is NOT successful..!!");	
//						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login Error Message:"+strErrors+" is displayed. Please check.! If client is RJF prod, please add '170.12.112.236 cbaadm.rjf.prod.scivantage.com' to hosts file.", "", "LoginError",  driver, "BOTH_DIRECTIONS", strSheetName, null);
//						return false;
//					}	
//					else{
//						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] User Name field is NOT found, 'NoSuchElementException' Exception in <Login_Maxit><QuickSmokeLogin_SSO> method, please check.", "", "LoginError",  driver, "BOTH_DIRECTIONS", strSheetName, null);
//						return false;
//					}
//				}//End of Catch block to catch NoSuchElementException exception.
				
				
				if(!objLoginPage.VerifySSO_CheckALL_Click(driver)){
						return false;
				}//End of if to check return value of "VerifyUserNameExists" method			
				
				if(objLoginPage.VerifyLoginExists(driver)){
					if(!objLoginPage.ClickLogin(driver)){
						return false;
					}//End of if to check return value of "ClickLogin" method
				}//End of if to check return value of "VerifyLoginExists" method
			
				String strErrors = objLoginPage.VerifyLoginErrors(driver);
				if(!(strErrors.isEmpty()||strErrors==null || strErrors=="")){
					System.out.println("Login errors displayed, Error message is:'"+strErrors+"'. Login to client "+strClient+" is NOT successful..!!");	
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login Error Message:"+strErrors+" is displayed. Please check.!", "", "LoginError",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
				
				//NonSSO CLient: Verify if Login is success by verifying SciVantage Logo is displayed.
				if(objLoginPage.VerifyScivanLogo(driver)||objLoginPage.VerifyPageTitle(driver)){
					System.out.println("<METHOD: QuickSmokeLogin_SSO><CLASS: Login_Maxit> - Login is to client "+strClient+" is successful");
					reporter.reportStep(excelRow, "Passed", "Checkpoint :[Passed] Login Successful.", "", "LoginSucces",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return true;
				}//End of IF condition to Verify if Login is success by verifying SciVantage Logo is displayed.
				
				else{
					System.out.println("<METHOD: QuickSmokeLogin_SSO><CLASS: Login_Maxit> - Login is to client "+strClient+" is NOT successful..!!");
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT successful. Please check.!", "", "LoginFail",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}

			}//End of try Block		
			
			catch (Exception e){
				System.out.println("<METHOD: QuickSmokeLogin_SSO><CLASS: Login_Maxit> - Exception, please check!!!!");	
				e.printStackTrace();			
				return false;
				
			}//End of Catch Block
		}//End of QuickSmokeLogin_SSO method		
		
	//###################################################################################################################################################################  
	//Function name		: QuickSmokeLogin_NonSSO_(int intDTLogin_Row)
	//Class name		: Login
	//Description 		: Generic function to login to Maxit NonSSO Clients
	//Parameters 		: N/A
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
		private boolean QuickSmokeLogin_NonSSO(String strClient,String strUserName,String strPassword,int excelRow,String strSheetName){
			try {					
				System.out.println("Logging into Maxit client(NonSSO):"+strClient+";LoginID:"+strUserName+";Password:"+strPassword);
				
				//NonSSO CLient: Verify & Enter User name
				if(objLoginPage.VerifyUserNameExists(driver)){
					//CommonUtils.captureScreenshot( driver, "Verify UserName Exists", "Verifying User Name field exists in Login page", "", objLoginPage.txt_UserName);
					if(!objLoginPage.EnterUserName(driver,strUserName)){
						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of if to check return value of "EnterUserName" method
				}//End of if to check return value of "VerifyUserNameExists" method

				//NonSSO CLient: Verify & Enter Password
				if(objLoginPage.VerifyPasswordExists(driver)){
					if(!objLoginPage.EnterPassword(driver,strPassword)){
						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of if to check return value of "EnterPassword" method
				}//End of if to check return value of "VerifyPasswordExists" method

				//NonSSO CLient: Verify & Click on Login button
				if(objLoginPage.VerifyLoginExists(driver)){
					if(!objLoginPage.ClickLogin(driver)){
						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of if to check return value of "ClickLogin" method
				}//End of if to check return value of "VerifyLoginExists" method
			
				String strErrors = objLoginPage.VerifyLoginErrors(driver);
				if(!(strErrors.isEmpty()||strErrors==null || strErrors=="")){
					System.out.println("Login errors displayed, Error message is:'"+strErrors+"'. Login to client "+strClient+" is NOT successful..!!");	
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login Error Message:"+strErrors+" is displayed. Please check.!", "", "LoginError",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
				
				//NonSSO CLient: Verify if Login is success by verifying SciVantage Logo is displayed.
				if(objLoginPage.VerifyScivanLogo(driver)||objLoginPage.VerifyPageTitle(driver)){
					System.out.println("<METHOD: fnLogin_NonSSO_v1><CLASS: Login> - Login to client "+strClient+" is successful");
					reporter.reportStep(excelRow, "Passed", "Checkpoint :[Passed] Login Successful.", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
					return true;
				}//End of IF condition to Verify if Login is success by verifying SciVantage Logo is displayed.
				else{
					System.out.println("<METHOD: fnLogin_NonSSO_v1><CLASS: Login> - Login to client "+strClient+" is NOT successful..!!");
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}	
				
			}//End of try Block
			
			catch (Exception e){
				System.out.println("<METHOD: fnLogin_NonSSO><CLASS: Login> - Exception, please check!!!!");	
				e.printStackTrace();
				reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Exception in <Method: QuickSmokeLogin_NonSSO>. Please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
				
			}//End of Catch Block
		}//End of QuickSmokeLogin_NonSSO method			
		
	//###################################################################################################################################################################  
	//Function name		: QuickSmokeLogin_SiteMinder(String strClient,String strUserName,String strPassword,int excelRow,String strSheetName)
	//Class name		: Login
	//Description 		: Generic function to login to Maxit SiteMinder(Endeavour clients with Foxy Proxy) Clients
	//Parameters 		: N/A
	//Assumption		: These clients can be logged into Firefox only(List of Prod clients : EDJ,APX,CTR,FMS,ING,LEU,MSC,PPR,PPL,SFR,SFB,SCH,SFM,STC,SWN,WLF)
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
		private boolean QuickSmokeLogin_SiteMinder(String strClient,String strUserName,String strPassword,int excelRow,String strSheetName){
			try {					
				System.out.println("Logging into Maxit client(SiteMinder):"+strClient+";LoginID:"+strUserName+";Password:"+strPassword);
				
				if(!objLoginPage.EnterUserID(driver,strUserName)){
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Succesful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if to check return value of "EnterUserID" method
				
				if(!objLoginPage.EnterUserPwrd(driver,strPassword)){
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if to check return value of "EnterUserPwrd" method
			
				if(!objLoginPage.ClickEndeavourLogin(driver)){
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
					return false;				
				}//End of if to check return value of "ClickEndeavourLogin" method
			
				String strErrors = objLoginPage.VerifyLoginErrors(driver);
				if(!(strErrors.isEmpty()||strErrors==null || strErrors=="")){
					System.out.println("Login errors displayed, Error message is:'"+strErrors+"'. Login to client "+strClient+" is NOT successful..!!");	
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login Error Message:"+strErrors+" is displayed. Please check.!", "", "LoginError",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
				
				//SiteMinder CLient: Verify if Login is success by verifying SciVantage Logo is displayed.
				if(objLoginPage.VerifyScivanLogo(driver)||objLoginPage.VerifyPageTitle(driver)){
					System.out.println("<METHOD: QuickSmokeLogin_SiteMinder><CLASS: Login> - Login is to client "+strClient+" is successful");
					reporter.reportStep(excelRow, "Passed", "Checkpoint :[Passed] Login Successful.", "", "LoginPass",  driver, "HORIZONTALLY", strSheetName, null);
					return true;
				}//End of IF condition to Verify if Login is success by verifying SciVantage Logo is displayed.
				else{
					String strErrorPassword = objLoginPage.VerifyPasswrdError(driver);
					//Verify if error message:
					if(!((strErrorPassword==null) || (strErrorPassword==""))){
						System.out.println("<METHOD: QuickSmokeLogin_SiteMinder><CLASS: Login> - Login is to client "+strClient+" is NOT successful..!!");
						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful. Error message is displayed: '"+strErrorPassword, "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
					
					//Chekcpoint: verifies for UserName/Passowrd incorrect combination error
					else if(!((objLoginPage.Incorrect_Credentials(driver)==null) || (objLoginPage.Incorrect_Credentials(driver)==""))){
						strErrorPassword=objLoginPage.Incorrect_Credentials(driver);
						System.out.println("<METHOD: QuickSmokeLogin_SiteMinder><CLASS: Login> - Login is to client "+strClient+" is NOT successful..!!");
						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful. Incorect credentials Error message is displayed: '"+strErrorPassword, "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
					
					//Checkpoint: verify if Password expired error
					
					else if(!((objLoginPage.Password_Expiry(driver)==null) || (objLoginPage.Password_Expiry(driver)==""))){
						strErrorPassword=objLoginPage.Password_Expiry(driver);
						System.out.println("<METHOD: QuickSmokeLogin_SiteMinder><CLASS: Login> - Login is to client "+strClient+" is NOT successful..!!");
						reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful. Password Expiry Error message is displayed: '"+strErrorPassword, "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
					
					System.out.println("<METHOD: QuickSmokeLogin_SiteMinder><CLASS: Login> - Login is to client "+strClient+" is NOT successful..!!");
					reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Successful, please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}

			}//End of try Block		
			
			catch (Exception e){
				System.out.println("<METHOD: QuickSmokeLogin_SiteMinder><CLASS: Login_Maxit> - Exception, please check!!!!");	
				e.printStackTrace();	
				reporter.reportStep(excelRow, "Failed", "Checkpoint :[Failed] Login is NOT Succesful. Exception in <Method: QuickSmokeLogin_SiteMinder>.Please check..!", "", "LoginError",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
				
			}//End of Catch Block
		}//End of QuickSmokeLogin_SiteMinder method			
		
		
		
		
		
		
	
}//End of <Class: Login_Maxit>
