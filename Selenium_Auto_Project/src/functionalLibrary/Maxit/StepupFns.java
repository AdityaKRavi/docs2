package functionalLibrary.Maxit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;


import PageObjects.Maxit.*;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.Reporting;


@SuppressWarnings({})
public class StepupFns{
	ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();	
	Reporting reporter = new Reporting();
	WebDriver driver ;
	CBManager_Objs CBMngr_Objs;
	SearchPageFns SearchPagefns;
	SearchPage_POM SearchPage;
	SearchPage_Results SearchPageResults;
	StepUp_Objs stepup_Objs;

	boolean stepupApplied;

	public StepupFns(WebDriver driver2) {
		this.driver = driver2;
		this.CBMngr_Objs = PageFactory.initElements(this.driver, CBManager_Objs.class);
		this.SearchPagefns = PageFactory.initElements(this.driver, SearchPageFns.class);
		this.SearchPage = PageFactory.initElements(this.driver, SearchPage_POM.class);
		this.SearchPageResults = PageFactory.initElements(this.driver, SearchPage_Results.class);
		this.stepup_Objs = PageFactory.initElements(this.driver, StepUp_Objs.class);
	}

	//*********************************************************************************************************************
	public boolean StepUpUpdate_Flow(String strAcct, String strSecType, String strSecValue, String stepUpLevel, String strStepUp_DoD,
			String altValuationDate, String rowNumberToEdit, String fmvPerShare, String stepPercentage, String bondSecNo, int exclRowCnt, String strSheetName) {
		try
		{
			String strSearchValues, strTabName = "STEPUP";
			boolean strRet;

			// Navigate to CB Manager stepup page
			boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager("STEPUP",exclRowCnt,strSheetName,"Y");
			if(strPageExistsCheck){								
				strSearchValues = "["+strTabName+";Account:"+strAcct+";SecVal:"+strSecValue+";DoD:"+strStepUp_DoD+"]";
				System.out.println("\n#########################["+strTabName+"]Update script : Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				//Perform search in Stepup page
				strRet = stepup_Objs.StepUp_SearchForUpdateScript(strTabName, strAcct, strSecType, strSecValue, strStepUp_DoD, altValuationDate, exclRowCnt, strSheetName);

				if(strRet) 
				{
					//Perform stepup update and capture the status
					this.stepupApplied = stepup_Objs.StepUp_Update(strTabName, strAcct, strSecType, strSecValue, strStepUp_DoD, altValuationDate, rowNumberToEdit, fmvPerShare, stepPercentage, exclRowCnt, bondSecNo, strSheetName, strSearchValues, stepUpLevel);
				}

			}//End of IF to check if Page Exists for this client
			System.out.println("###########################################################[End of Update script for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in StepUp Update Flow <Method: StepUpUpdate_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in StepUpUpdate_Flow ", "", "StepUpUpdate_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in StepUp Update Flow <Method: StepUpUpdate_Flow>,please check..!!");
			return false;
		}//End of Catch block
		return this.stepupApplied;
	}

	//###################################################################################################################################################################  
	//Function name		: StepUpResult_DataValidation
	//Class name		: UpdateScriptFns
	//Description 		: Function to perform stepup data validation process.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public void StepUpResult_DataValidation(String strViewPage, String To_Execute, String strAcct, String strSecType, String strSecValue, String strRowCount ,	String strColNames,
			String strStepUp_DoD, String altValuationDate, String[] strExpRows,	int exclRowCnt , String strSheetName) throws Exception {

		String strSearchValues, strTabName = "STEPUP";
		boolean strRet;
		//Navigate to CB manager step up page
		boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager("STEPUP",exclRowCnt,strSheetName,"Y");

		if(strPageExistsCheck){								
			strSearchValues = "["+strTabName+";Account:"+strAcct+";SecVal:"+strSecValue+";DoD:"+strStepUp_DoD+"]";
			System.out.println("\n#########################["+strTabName+"]Data Validation : Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");

			//perform search in step up page for the corresponding test data
			strRet = stepup_Objs.StepUp_SearchForUpdateScript(strTabName, strAcct, strSecType, strSecValue, strStepUp_DoD, altValuationDate, exclRowCnt, strSheetName);

			if(strRet)
			{
				//perform step up data validation
				stepup_Objs.stepUp_DataValidation(strViewPage, To_Execute, strAcct, strSecType, strSecValue, strRowCount ,	strColNames,
						strStepUp_DoD, altValuationDate, strExpRows, exclRowCnt , strSheetName, strSearchValues);
			}
		}
	}//End of StepUpResult_DataValidation method

	//*********************************************************************************************************************		
	public  void SearchResult_DataValidation(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strRowCount , String strColNames, String[] strExpRows,String strSheetName ){
		String[] aActualArr;	
		String strSearchValues = "["+strSheetName+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
		int expectedRowCount = Integer.parseInt(strRowCount);
		System.out.println("\n#########################["+strViewPage+"]Data Valiadtion: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");

		if(SearchPagefns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName)){
			if(SearchPagefns.SearchRes_RowCnt_Checkpoint(expectedRowCount,strViewPage,exclRowCnt,strSheetName)){
				if(expectedRowCount!=0){
					aActualArr = SearchPagefns.SearchRes_GetActualRowArray(strColNames, expectedRowCount);					
					SearchPagefns.SearchResult_CompareArrays(exclRowCnt,strExpRows, aActualArr, strColNames, strViewPage, strSearchValues,strSheetName);
				}//Check for RowCount not zero to call comparison method.
			}//End of IF condition to check RowCount checkpoint
		}//End of IF condition to check Search_Input Checkpoint


		//Phase 2:
		//COde for UGL G/l & Term validation:
		for(int i=1;i<=expectedRowCount;i++){
			if(strViewPage.toLowerCase().contains("unrealized")){
				SearchPagefns.UGL_CheckGainLoss(exclRowCnt,strSheetName,i);	
				stepup_Objs.UGL_Checkpoint_CheckTerm(exclRowCnt,strSheetName,i);

			}//End of IF condition to check if the page is UGL

		}//End of FOR loop for UGL verification

		System.out.println("###########################################################[End of Data Valiadtion: Row iteration:["+exclRowCnt+"]###########################################################");

		//return false;
	}//End of SearchResult_DataValidation method

}//End of <Class: StepupFns>
