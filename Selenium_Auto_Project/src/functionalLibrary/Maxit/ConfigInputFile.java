package functionalLibrary.Maxit;


//import org.apache.log4j.Logger;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.DataTable;


public class ConfigInputFile {
	CommonUtils CUtils = new CommonUtils();
	DataTable dt = new DataTable();


//###################################################################################################################################################################  
//Function name		: getData_DataValidation(String strExcelPath,String strSheetName, String strViewPage)
//Class name		: configInputFile
//Description 		: Generic function to read data provider from Input data files for data validation
//Parameters 		: strExcelPath: Input excel file path
//					: strSheetName: Name of the sheet
//					: strViewPage : Search View page name
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public Object[][] getData_DataValidation(String strExcelPath,String strSheetName, String strViewPage) throws Exception{
		Object[][] dpArray = null;
		dt.setExcelFile(strExcelPath);
        if(dt.setSheet(strSheetName)){			  		 
        	int intDTRows = dt.getRowCount();
        	dpArray = new Object[intDTRows][10];
        	
        	for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

        		  String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
        		  if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
        			  return(dpArray);
        		  }
        		  
				  String strAcct = dt.getCellData(intDTRowIndex, "Account_No").toString() ;
				  String strSecType = dt.getCellData(intDTRowIndex, "Sec_Type").toString() ;
				  String strSecValue = dt.getCellData(intDTRowIndex, "Sec_Value").toString() ;
				  String strRowCount = dt.getCellData(intDTRowIndex, "Row_Count").toString() ;
				  String strColNames = dt.getCellData(intDTRowIndex, "Column_Names").toString() ;
				  dpArray[intDTRowIndex-1][0]=strViewPage;
				  dpArray[intDTRowIndex-1][1]=To_Execute;
				  dpArray[intDTRowIndex-1][2]=strAcct;
				  dpArray[intDTRowIndex-1][3]=strSecType;
				  dpArray[intDTRowIndex-1][4]=strSecValue;
				  dpArray[intDTRowIndex-1][5]=strRowCount;
				  dpArray[intDTRowIndex-1][6]=strColNames;
				  
				  //Reading Expected rows
				  int rowCnt = Integer.parseInt(strRowCount);
				  String[] expRows = new String[rowCnt];
				  if(rowCnt>0){
					  for(int readExpRow = 1;readExpRow<=rowCnt;readExpRow++){
						  expRows[readExpRow-1]= (dt.getCellData(intDTRowIndex, "Row"+readExpRow).toString());						  
					  }//End of for loop to read Expected rows from Input sheet
					  dpArray[intDTRowIndex-1][7]=expRows;
				  }//End of IF condition to check ExpectedRow count in data sheet>0
				  
				  else {
					  dpArray[intDTRowIndex-1][7] = null;
				  }//End of ELSE condition to check ExpectedRow count in data sheet>0				  				  
				  dpArray[intDTRowIndex-1][8]=intDTRowIndex;
				  dpArray[intDTRowIndex-1][9]=strSheetName;
			  }//End of for loop to iterate thru different rows	
	  }//End of IF condition to check  DataTable.setSheet

		return(dpArray);
	}//End of getData_DataValidation Method
	
//###################################################################################################################################################################  
//Function name		: getData_DataValidation(String strExcelPath,String strSheetName)
//Class name		: configInputFile
//Description 		: Generic function to read data provider from Input data files for data validation
//Parameters 		: strExcelPath: Input excel file path
//						: strSheetName: Name of the sheet
//						: strViewPage : Search View page name
//Assumption		: None
//Developer			: Kavitha Golla
//This is [Method Overloading withour strViewPage parameter] used in @DataProvider(name="Read_DataValTestData")
//###################################################################################################################################################################			
	public Object[][] getData_DataValidation(String strExcelPath,String strSheetName) throws Exception{
		Object[][] dpArray = null;
		String strViewPage;
		dt.setExcelFile(strExcelPath);
        if(dt.setSheet(strSheetName)){			  		 
        	int intDTRows = dt.getRowCount();
        	dpArray = new Object[intDTRows][10];
        	
        	for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

        		  String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
        		  if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
        			  return(dpArray);
        		  }
        		  
				  String strAcct = dt.getCellData(intDTRowIndex, "Account_No").toString() ;
				  String strSecType = dt.getCellData(intDTRowIndex, "Sec_Type").toString() ;
				  String strSecValue = dt.getCellData(intDTRowIndex, "Sec_Value").toString() ;
				  String strRowCount = dt.getCellData(intDTRowIndex, "Row_Count").toString() ;
				  String strColNames = dt.getCellData(intDTRowIndex, "Column_Names").toString() ;
				  
				  switch (strSheetName){
				  case "Ledger":
					  strViewPage = "Ledger";
					  break;
				  case "Realized":
					  strViewPage = "Realized";
					  break;
				  case "Unrealized":
					  strViewPage = "Unrealized";
					  break;
				  case "OpenClosed":
					  strViewPage = "Open/Closed";
					  break;
				  case "RawTrades":
					  strViewPage = "Raw Trades";
					  break;
				  default:
					  strViewPage = "Ledger";	
					  break;
				  }//End of Switch case
				  
				  
				  dpArray[intDTRowIndex-1][0]=strViewPage;
				  dpArray[intDTRowIndex-1][1]=To_Execute;
				  dpArray[intDTRowIndex-1][2]=strAcct;
				  dpArray[intDTRowIndex-1][3]=strSecType;
				  dpArray[intDTRowIndex-1][4]=strSecValue;
				  dpArray[intDTRowIndex-1][5]=strRowCount;
				  dpArray[intDTRowIndex-1][6]=strColNames;
				  
				  //Reading Expected rows
				  int rowCnt = Integer.parseInt(strRowCount);
				  String[] expRows = new String[rowCnt];
				  if(rowCnt>0){
					  for(int readExpRow = 1;readExpRow<=rowCnt;readExpRow++){
						  expRows[readExpRow-1]= (dt.getCellData(intDTRowIndex, "Row"+readExpRow).toString());						  
					  }//End of for loop to read Expected rows from Input sheet
					  dpArray[intDTRowIndex-1][7]=expRows;
				  }//End of IF condition to check ExpectedRow count in data sheet>0
				  
				  else {
					  dpArray[intDTRowIndex-1][7] = null;
				  }//End of ELSE condition to check ExpectedRow count in data sheet>0				  				  
				  dpArray[intDTRowIndex-1][8]=intDTRowIndex;
				  dpArray[intDTRowIndex-1][9]=strSheetName;
			  }//End of for loop to iterate thru different rows	
	  }//End of IF condition to check  DataTable.setSheet

		return(dpArray);
	}//End of getData_DataValidation Method
	
//###################################################################################################################################################################  
//Function name		: getSmokeData(String strExcelPath,String strSheetName, String strViewPage)
//Class name		: configInputFile
//Description 		: Generic function to configure input file dynamically during runtime based on scriptName
//Parameters 		: strExcelPath: Input excel file path
//						: strSheetName: Name of the sheet
//						: strViewPage : Search View page name
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public Object[][] getSmokeData(String strExcelPath, String strSmokePage,String strViewPage,String strClient) throws Exception{
		Object[][] dpArray = null;
		String strSheetName = "SmokeData";	
		//if(strViewPage.equalsIgnoreCase("OpenClosed")) strViewPage = "Open/Closed";
		dt.setExcelFile(strExcelPath);
        if(dt.setSheet(strSheetName)){			  		 
        	int intDTRows = dt.getRowCount();
        	dpArray = new Object[1][14];
        	
        	for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  
        		String strSearchPage = dt.getCellData(intDTRowIndex, "Page").toString() ;

        		//fn to get actual view page name based on input page name(Ex: Search_Account = Account from view menu)
    		  String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
    		  if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
    			  return(dpArray);
    		  }
      		if(strSmokePage.equalsIgnoreCase(strSearchPage)||To_Execute=="Y" ){
			  String strAcct = dt.getCellData(intDTRowIndex, "Account").toString().trim() ;
			  String strSecType = dt.getCellData(intDTRowIndex, "SecurityID").toString().trim() ;
			  String strSecValue = dt.getCellData(intDTRowIndex, "SecurityIDValue").toString().trim() ;
			  String strFrom_Date = dt.getCellData(intDTRowIndex, "From_Date").toString().trim() ;
			  String strTo_Date = dt.getCellData(intDTRowIndex, "To_Date").toString().trim() ;
			  String strStepUp_DoD = dt.getCellData(intDTRowIndex, "StepUp_DoD").toString().trim() ;
			  String strBlankSearch = dt.getCellData(intDTRowIndex, "BlankSearch").toString().trim() ;
			  String strPageExists = dt.getCellData(intDTRowIndex, "PageExists").toString().trim() ;
			  
			  String strCAID = dt.getCellData(intDTRowIndex, "CAID").toString().trim() ;
			  String strAddNewCA = dt.getCellData(intDTRowIndex, "AddNewCA").toString().trim() ;
			  
			  dpArray[0][0]=strViewPage;
			  dpArray[0][1]=To_Execute;
			  dpArray[0][2]=strAcct;
			  dpArray[0][3]=strSecType;
			  dpArray[0][4]=strSecValue;
			  dpArray[0][5]=strFrom_Date;
			  dpArray[0][6]=strTo_Date;
			  dpArray[0][7]=strStepUp_DoD;
			  dpArray[0][8]=strBlankSearch;
			  				 				  				  
			  dpArray[0][9]=intDTRowIndex;
			  dpArray[0][10]=strPageExists;
			  dpArray[0][11]=strCAID;
			  dpArray[0][12]=strAddNewCA;
			  dpArray[0][13]=strClient;
			  return(dpArray);
      		}//End of IF condition
		  }//End of for loop to iterate thru different rows	
	  }//End of IF condition to check  DataTable.setSheet

		return(dpArray);
	}//End of getSmokeData Method	
//###################################################################################################################################################################	
}//							End of <configInputFile> class
//###################################################################################################################################################################
