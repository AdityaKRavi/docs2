package functionalLibrary.Maxit;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class Maxit_CommonFns {

	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	Reporting reporter = new Reporting();
	WebDriverWait wait;
	
	
	//Page Objects
    @FindBy(how=How.XPATH,using="//*[contains(@class, \"x-combo-list\") and contains(@style, \"visibility: visible;\")]//*[contains(@class, \"x-combo-list-item\")]")
	public List<WebElement> ele_PopUpCombo;
  //###################################################################################################################################################################  
  //Function name			: get_TableHeader(String strPageName,List<WebElement> eleTableName,int tbl_index,String strTagName,int exclRowCnt,String strSheetName)
  //Class name				: MaxitCommonFns
  //Description 			: Function to return table header text
  //Parameters 				: Web table as a list object, the index of the table if multiple are displayed, excel row count and excel sheet for reporting.
  //Assumption				: None
  //Developer				: Kavitha Golla
  //###################################################################################################################################################################		
  	public String get_TableHeader(String strPageName,List<WebElement> eleTableName,int tbl_index,String strTagName,int exclRowCnt,String strSheetName){
			List<WebElement> tbl_Header= eleTableName;
  		String strHeaderRow="";
  		try{  	    		
  			if(eleTableName.isEmpty()){
  				System.out.println("<Class: MaxitCommonFns><Method: get_TableHeader>: eleTableName parameter passed is empty, please check..!!");
  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<MaxitCommonFns.get_TableHeader>!", "", "eleTableName",  driver, "HORIZONTALLY", strSheetName, null);
  				return "";
  			}
  			if((tbl_Header.size()>0) && (tbl_Header.size()>=tbl_index)){
  				List<WebElement> getCols = tbl_Header.get(tbl_index).findElements(By.tagName(strTagName));
        			for (WebElement eachCol : getCols) {  
        				strHeaderRow = strHeaderRow+eachCol.getAttribute("textContent").trim()+";";		
        			}//End of FOR loop to read header column names
        			if(strHeaderRow.endsWith(";")){
        				strHeaderRow = StringUtils.left(strHeaderRow, strHeaderRow.length()-1);	
        			}
        			
        			return strHeaderRow;
  			}//End of IF condition to check if table exists
  			else{
  				System.out.println("<Class: MaxitCommonFns><Method: get_TableHeader>: Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index);
  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index+",please check method<MaxitCommonFns.get_TableHeader>!", "", "Tble_Header",  driver, "HORIZONTALLY", strSheetName, null);
  				return "";
  			}//End of IF condition to check if table exists
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <Class: MaxitCommonFns><Method: get_TableHeader>: Table Header size in the browser is not as expected..!!!");
  			e.printStackTrace();
  			return null;
  		}
  	}//End of get_TableHeader Method
  	
	  //###################################################################################################################################################################  
	  //Function name		: getAct_DropDowns(String strDropDownName,WebElement eleDropDown,int exclRowCnt,String strSheetName)
	  //Class name			: Maxit_CommonFns.java
	  //Description 		: Function to get drop down values from UI
	  //Parameters 			: eleDropDown : drop down element
  	  //  					: strDropDown: drop down name
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	public ArrayList<String> getAct_DropDowns(String strDropDownName,WebElement eleDropDown,int exclRowCnt,String strSheetName){ 
	  try{
			ArrayList<String> arrAct_Values = new ArrayList<String>();
			WebElement eleActDropdown=eleDropDown;
			
			if(!(CommonUtils.isElementPresent(eleDropDown))){
  				System.out.println("<Class: MaxitCommonFns><Method: getAct_DropDowns>: "+strDropDownName+" Dropdown is not displayed in UI, please check.!");
  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strDropDownName+" Dropdown is not displayed in UI, please check.!!", "", strDropDownName,  driver, "HORIZONTALLY", strSheetName, null);
  				return null;
			}
						
			List<WebElement> Act_List = eleActDropdown.findElements(By.tagName("option"));
			for (int i = 0; i < Act_List.size(); i++) {
				arrAct_Values.add(Act_List.get(i).getText());
			}
	
	        return arrAct_Values;
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: getAct_DropDowns>,"+e.getMessage()+",please check..!!!");
		  return null;
	  }//End of catch block
	  
	}//End of <Method: getAct_DropDowns>
  	
	  //###################################################################################################################################################################  
	  //Function name		: getAct_DropDowns(String strDropDownName,WebElement eleDropDown,int exclRowCnt,String strSheetName)
	  //Class name			: Maxit_CommonFns.java
	  //Description 		: Function to get drop down values from UI
	  //Parameters 			: eleDropDown : drop down element
	  //  					: strDropDown: drop down name
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	public ArrayList<String> getAct_ComboList(String strDropDownName,List<WebElement> eleComboBox,int exclRowCnt,String strSheetName){ 
	  try{
			ArrayList<String> arrAct_Values = new ArrayList<String>();
			
	    	if(!(eleComboBox.size()>0)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strDropDownName+" object is NOT displayed", "", strDropDownName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strDropDownName+" object is NOT displayed,please check..!!");
	    		return null;
			}//End of if condition to check if Combo list is displayed

	    	//Logic to read the ComboList:
	    	List<WebElement> comboItems = eleComboBox;
	        for (int i = 0; i < comboItems.size(); i++) {
	        	arrAct_Values.add(comboItems.get(i).getText());
	        };

	        return arrAct_Values;
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: getAct_ComboList>,"+e.getMessage()+",please check..!!!");
		  return null;
	  }//End of catch block
	  
	}//End of <Method: getAct_ComboList>
	
	//###################################################################################################################################################################  
	  //Function name		: getSelectedValue_ComboList(String strDropDownName,WebElement eleDropDown,int exclRowCnt,String strSheetName)
	  //Class name			: Maxit_CommonFns.java
	  //Description 		: Function to get 'Selected Value' from combo List drop down
	  //Parameters 			: eleDropDown : drop down element
	  //  					: strDropDown: drop down name
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	public String getSelectedValue_ComboList(String strDropDownName,List<WebElement> eleComboBox,int exclRowCnt,String strSheetName){ 
		String strSelectedValue="";
		try{
	    	if(!(eleComboBox.size()>0)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strDropDownName+" object is NOT displayed", "", strDropDownName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strDropDownName+" object is NOT displayed,please check..!!");
	    		return null;
			}//End of if condition to check if Combo list is displayed

	    	//Logic to read the ComboList:
	    	List<WebElement> comboItems = eleComboBox;
	        for (int i = 0; i < comboItems.size(); i++) {
	        	if(comboItems.get(i).getAttribute("class").contains("x-combo-list-item x-combo-selected")){       	
	        		strSelectedValue= comboItems.get(i).getText();	  
	        		return strSelectedValue;
	        	}
	        }//End of FOR loop

	        return null;
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: getSelectedValue_ComboList>,"+e.getMessage()+",please check..!!!");
		  return null;
	  }//End of catch block
	  
	}//End of <Method: getSelectedValue_ComboList>	
	//###################################################################################################################################################################  
	  //Function name		: setValue_ComboList(String strDropDownName,WebElement eleDropDown,int exclRowCnt,String strSheetName)
	  //Class name			: Maxit_CommonFns.java
	  //Description 		: Function to get 'Selected Value' from combo List drop down
	  //Parameters 			: eleDropDown : drop down element
	  //  					: strDropDown: drop down name
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	public boolean setValue_ComboList(String strDropDownName,List<WebElement> eleComboBox,String strValuetoSelect,int exclRowCnt,String strSheetName){ 
		String strSelectedValue="";
		try{
	    	if(!(eleComboBox.size()>0)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strDropDownName+" object is NOT displayed", "", strDropDownName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strDropDownName+" object is NOT displayed,please check..!!");
	    		return false;
			}//End of if condition to check if Combo list is displayed

	    	//Logic to read the ComboList:
	    	List<WebElement> comboItems = eleComboBox;
	        for (int i = 0; i < comboItems.size(); i++) {
	        		strSelectedValue= comboItems.get(i).getText();	 
	        		if(strSelectedValue.equalsIgnoreCase(strValuetoSelect)){
	        			comboItems.get(i).click();
	        			return true;
	        		}
	        }//End of FOR loop

	        return false;
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: setValue_ComboList>,"+e.getMessage()+",please check..!!!");
		  return false;
	  }//End of catch block
	  
	}//End of <Method: getSelectedValue_ComboList>		
	  //###################################################################################################################################################################  
	  //Function name		: Compare_ArrayLists(String strStepName,ArrayList<String> arrExp_Values,ArrayList<String> arrAct_Values,int exclRowCnt,String strSheetName)
	  //Class name			: Maxit_CommonFns
	  //Description 		: Function to compare 2 array lists Actual & expected dropdown values
	  //Parameters 			: 
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	  public void Compare_ArrayLists(String strStepName,ArrayList<String> arrExp_Values,ArrayList<String> arrAct_Values,int exclRowCnt,String strSheetName){		
			
	        //Compare 2 arrayLists Actual & Expected  values
	        if(!(arrAct_Values.containsAll(arrExp_Values) && arrExp_Values.containsAll(arrAct_Values) )){
	        	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Comparing "+strStepName+" failed.Expected values = "+arrExp_Values.toString()+"; Actual values = "+arrAct_Values.toString(),"", "compare_ArrayLists",   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Failed] Comparing "+strStepName+" failed.Expected values = "+arrExp_Values.toString()+"; Actual values = "+arrAct_Values.toString());
			}//End of IF condition to compare expected & actual  values
	        else {
	        	reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Comparision of "+strStepName+" is success.Actual value = "+arrAct_Values.toString(),"", "compare_ArrayLists",   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Passed] Comparision of "+strStepName+" is success.Actual value = "+arrAct_Values.toString());
	        }
	  
	  }//End of <Method: Compare_ArrayLists>  
	  //###################################################################################################################################################################  
	  //Function name		: elementExists(WebElement objElement,int exclRowCnt,String strSheetName)
	  //Class name			: Maxit_CommonFns
	  //Description 		: Function to verify if element exists
	  //Parameters 			: 
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	  public boolean elementExists(WebElement objElement,String strStepName,int exclRowCnt,String strSheetName){				
			
			if(CommonUtils.isElementPresent(objElement)){
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strStepName+" element exists.","", "ElementExists",   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Passed]  "+strStepName+" element exists.");
	        	return true;
			}
			else{
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed]  "+strStepName+" element is not displayed,please check.","", "ElementExists",   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Failed]  "+strStepName+" element is not displayed,please check.!!.");
	        	return false;
			}
			
	  
	  }//End of <Method: elementExists> 	  
  	
//  //###################################################################################################################################################################  
//  //Function name		: compareDropDown(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName)
//  //Class name			: MaxitCommonFns.java
//  //Description 		: Function to compare expected & actual drop downs
//  //Parameters 			: strPopUpName : pop up name
//  //  					: strDropDown: drop down name
//  //    				: Excel row number for report and sheet name.
//  //  					: strSheetName: Excel sheet name for reporting  
//  //Assumption			: None
//  //Developer			: Kavitha Golla
//  //###################################################################################################################################################################		
//    public void compareDropDown(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName){ 
//	  	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
//	  	SearchRes_FIElection_Objs FI_Objs = PageFactory.initElements(driver, SearchRes_FIElection_Objs.class);
//	  	SearchRes_MTM_Objs MTM_Objs = PageFactory.initElements(driver, SearchRes_MTM_Objs.class);
//	  	TaxLotEdit_Objs TLE_Objs = PageFactory.initElements(driver, TaxLotEdit_Objs.class);
//	  	  try{
//	  			ArrayList<String> arrAct_Values = new ArrayList<String>();
//	  			ArrayList<String> arrExp_Values = new ArrayList<String>();
//	  			
//	  			if(strPopUpName.equalsIgnoreCase("FIElection")) arrExp_Values = FI_Objs.getExp_FIPopUp_DropDowns(strDropDown);
//	  			else if(strPopUpName.equalsIgnoreCase("MTM")) arrExp_Values = MTM_Objs.getExp_MTMPopUp_DropDowns(strDropDown);
//	  			else if(strPopUpName.equalsIgnoreCase("ACCLEVEL_CBM")) arrExp_Values = CBM_Objs.getExp_AccLevel_CBM_DropDowns(strDropDown);
//	  			else if(strPopUpName.equalsIgnoreCase("SecLevel_CBM")) arrExp_Values = CBM_Objs.getExp_SecLevel_CBM_DropDowns(strDropDown);
//	  			else if(strPopUpName.equalsIgnoreCase("SecLevel_CBM_AvgCost")) arrExp_Values = CBM_Objs.getExp_SecLevel_AvgCost_CBM_DropDowns(strDropDown);
//	  			else if(strPopUpName.equalsIgnoreCase("SecLevel_DRP")) arrExp_Values = CBM_Objs.getExp_SecLevel_DRP_DropDowns(strDropDown);
//	  			else if(strPopUpName.equalsIgnoreCase("DETAILS_CBM")) arrExp_Values = CBM_Objs.getExp_Details_SellMethod(strDropDown);
//	  			else if(strPopUpName.equalsIgnoreCase("TLE_TRNXTYPE")) arrExp_Values = TLE_Objs.getExp_TLE_TrnxType(strDropDown);
//	  			
//	  			else if(strPopUpName.equalsIgnoreCase("ADDNEWCA")) {
//	  				CAManager_Objs CA_Objs = PageFactory.initElements(driver, CAManager_Objs.class);
//	  				arrExp_Values = CA_Objs.getExp_CAManager_DropDowns(strDropDown);
//	  			}
//	  			
//	  			if(strPopUpName.equalsIgnoreCase("SecLevel_CBM")) strPopUpName = "CBMethod_CBM";
//	  			else if(strPopUpName.equalsIgnoreCase("SecLevel_CBM_AvgCost")) strPopUpName = "CBMethod_CBM_AvgCost";
//	  			else if(strPopUpName.equalsIgnoreCase("SecLevel_DRP")) strPopUpName = "CBMethod_DRP";
//	  			else if(strPopUpName.equalsIgnoreCase("ACCLEVEL_CBM")) strPopUpName = "CBMethod";
//	  			else if(strPopUpName.equalsIgnoreCase("DETAILS_CBM")) strPopUpName = "RawTrades_Details";
//	  			else if(strPopUpName.equalsIgnoreCase("TLE_TRNXTYPE")) strPopUpName = "RawTrades_TLE";
//	  			
//	  	    	if(!(this.ele_PopUpCombo.size()>0)){
//	  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed", "", strPopUpName+"_"+strDropDown,  driver, "BOTH_DIRECTIONS", strSheetName, null);
//	  	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed,please check..!!");
//	  	    		return;
//	  			}//End of if condition to check if dropdown list is displayed
//	
//	  	    	//Logic to read the ComboList:
//	  	    	List<WebElement> comboItems = ele_PopUpCombo;
//	  	        for (int i = 0; i < comboItems.size(); i++) {
//	  	        	arrAct_Values.add(comboItems.get(i).getText());
//	  	        };
//	  	        
//	  	        //Compare Actual & Expected Dropdown values
//	  	        //if(!arrAct_Values.toString().equals(arrExp_Values.toString())){
//	  	        if(!(arrAct_Values.containsAll(arrExp_Values) && arrExp_Values.containsAll(arrAct_Values) )){
//	  	        	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+".Expected Dropdown values = "+arrExp_Values.toString()+"; Actual Dropdown values = "+arrAct_Values.toString(),"", strPopUpName+"_"+strDropDown,   driver, "BOTH_DIRECTIONS", strSheetName, null);
//	  	        	System.out.println("Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+".Expected Dropdown values = "+arrExp_Values.toString()+"; Actual Dropdown values = "+arrAct_Values.toString());
//	  			}//End of IF condition to check Expected Action Menu List & Actual Action Menu List
//	  	        else {
//	  	        	reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->"+strDropDown+".Dropdown values = "+arrExp_Values.toString(),"", strPopUpName+"_"+strDropDown,   driver, "BOTH_DIRECTIONS", strSheetName, null);
//	  	        	System.out.println("Checkpoint :[Passed] "+strPopUpName+"->"+strDropDown+".Dropdown values = "+arrExp_Values.toString());
//	  	        }
//	  		  
//	  	  }//End of try block
//	  	  catch(Exception e){
//	  		  System.out.println("Exception in <Method: compareDropDown>,"+e.getMessage()+",please check..!!!");
//	  	  }//End of catch block
//  	  
//    }//End of <Method: compareDropDown>  	
    
    //###################################################################################################################################################################  
    //Function name			: webTable_NumOfRows(String strPageName,List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
    //Class name			: Maxit_CommonFns.java
    //Description 			: Function to read number of records retrieved from search result
    //Parameters 			: 
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
    	public int webTable_NumOfRows(String strPageName,List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
    		try{  	
    			List<WebElement> tbl_WebTable= eleTableName;
      			if(eleTableName.isEmpty()){
      				System.out.println("<Class: MaxitCommonFns><Method: webTable_NumOfRows>: eleTableName parameter passed is empty, please check..!!");
      				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<MaxitCommonFns.webTable_NumOfRows>!", "", "eleTableName",  driver, "HORIZONTALLY", strSheetName, null);
      				return 0;
      			}
      			if((tbl_WebTable.size()>0) && (tbl_WebTable.size()>=tbl_index)){
        			wait = new WebDriverWait(driver,30);
        			wait.until(ExpectedConditions.visibilityOf(tbl_WebTable.get(tbl_index)));
        			if(!CommonUtils.isElementPresent(tbl_WebTable.get(tbl_index))){
        				System.out.println("Web Table is not displayed,please check..!!");
        				return 0;	
        			}//End of IF Condition to check if tbl_WebTable element exists
      				
      				List<WebElement> app_rows = tbl_WebTable.get(tbl_index).findElements(By.tagName("tr"));
      				return app_rows.size();  	

      			}//End of IF condition to check if table exists
      			else{
      				System.out.println("<Class: MaxitCommonFns><Method: webTable_NumOfRows>: Table size in the browser is "+tbl_WebTable.size()+" and given index is "+tbl_index);
      				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Table size in the browser is "+tbl_WebTable.size()+" and given index is "+tbl_index+",please check method<MaxitCommonFns.webTable_NumOfRows>!", "", "Tble_Rows",  driver, "HORIZONTALLY", strSheetName, null);
      				return 0;
      			}//End of IF condition to check if table exists

    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <MaxitCommonFns.webTable_NumOfRows>: Web Table is not displayed in UI..!!!");
    			e.printStackTrace();
    			return 0;
    		}
    	}//End of webTable_NumOfRows Method    

    //###################################################################################################################################################################  
    //Function name			: webTable_getColValue(String strPageName,List<WebElement> eleTableName,int tbl_index,int intRowNum,int intColNum,int exclRowCnt,String strSheetName)
    //Class name			: SearchPage_Results
    //Description 			: Function to read number of records retrieved from search result
    //Parameters 			: Search Page name
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
    	public String webTable_getColValue(String strPageName,List<WebElement> eleTableName,int tbl_index,int intRowNum,int intColNum,int exclRowCnt,String strSheetName){
    		try{ 
    			String strColValue="";
    			List<WebElement> tbl_WebTable= eleTableName;
      			if(eleTableName.isEmpty()){
      				System.out.println("<Class: MaxitCommonFns><Method: webTable_NumOfRows>: eleTableName parameter passed is empty, please check..!!");
      				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<MaxitCommonFns.webTable_NumOfRows>!", "", "eleTableName",  driver, "HORIZONTALLY", strSheetName, null);
      				return null;
      			}
      			if((tbl_WebTable.size()>0) && (tbl_WebTable.size()>=tbl_index)){
        			wait = new WebDriverWait(driver,30);
        			wait.until(ExpectedConditions.visibilityOf(tbl_WebTable.get(tbl_index)));
        			if(!CommonUtils.isElementPresent(tbl_WebTable.get(tbl_index))){
        				System.out.println("Web Table is not displayed,please check..!!");
        				return null;	
        			}//End of IF Condition to check if tbl_WebTable element exists
      				
      				List<WebElement> app_rows = tbl_WebTable.get(tbl_index).findElements(By.tagName("tr"));
      				List<WebElement> app_cols = app_rows.get(intRowNum).findElements(By.tagName("td"));
      				strColValue = app_cols.get(intColNum).getAttribute("textContent");
      				return strColValue;
      				

      			}//End of IF condition to check if table exists
      			else{
      				System.out.println("<Class: MaxitCommonFns><Method: webTable_NumOfRows>: Table size in the browser is "+tbl_WebTable.size()+" and given index is "+tbl_index);
      				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Table size in the browser is "+tbl_WebTable.size()+" and given index is "+tbl_index+",please check method<MaxitCommonFns.webTable_NumOfRows>!", "", "Tble_Rows",  driver, "HORIZONTALLY", strSheetName, null);
      				return null;
      			}//End of IF condition to check if table exists

    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <MaxitCommonFns.webTable_NumOfRows>: Web Table is not displayed in UI..!!!");
    			e.printStackTrace();
    			return null;
    		}
    	}//End of webTable_NumOfRows Method       	

	//######################################################################################################################
	//######################################################################################################################
    //Function name			: dynamicWait(String strPageName,List<WebElement> eleTableName,int tbl_index,int intRowNum,int intColNum,int exclRowCnt,String strSheetName)
    //Class name			: Maxit_CommonFns
    //Description 			: Function to dynamically wait for certain condition for given time in seconds
    //Parameters 			: 
    //Assumption			: None
    //Developer				: Kavitha Golla
	//######################################################################################################################
	//######################################################################################################################
	public boolean dynamicWait(WebElement eleObject, String strExpecCondition,int timeSeconds,int timeSeconds_Polling,String objName,int exclRowCnt,String strSheetName ) throws Exception{
		LocalDateTime strStartWaitTime,strEndWaitTime;  
		strStartWaitTime = LocalDateTime.now();
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			switch(strExpecCondition.toUpperCase()){
				case "OBJECT_VISIBLE":
					try{
						wait.withTimeout(timeSeconds,TimeUnit.SECONDS)
						.pollingEvery(timeSeconds_Polling, TimeUnit.SECONDS)				 
						.ignoring(NoSuchElementException.class)
						.until(ExpectedConditions.visibilityOf(eleObject));
					}//End of try block
					catch(org.openqa.selenium.TimeoutException e){
						strEndWaitTime = LocalDateTime.now();
						System.out.println("Dynamic wait for - Visibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Dynamic wait for - Visibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!", "", "dynamicWait",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of try-catch
						
					strEndWaitTime = LocalDateTime.now();
					if(CommonUtils.isElementPresent(eleObject)) return true;
					else{
						System.out.println(objName+" is NOT displayed in UI.Dynamic wait for - Visibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+objName+" is NOT displayed in UI.Dynamic wait for - Visibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!", "", "dynamicWait",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
					
				case "OBJECT_INVISIBLE":
					try{
						wait.withTimeout(timeSeconds,TimeUnit.SECONDS)
						.pollingEvery(timeSeconds_Polling, TimeUnit.SECONDS)				 
						//.ignoring(NoSuchElementException.class)
						.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(eleObject)));
					}		
					catch(org.openqa.selenium.TimeoutException e){
						strEndWaitTime = LocalDateTime.now();
						System.out.println("Dynamic wait for - Invisibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Dynamic wait for - InVisibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!", "", "dynamicWait",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of try-catch

					if(!(CommonUtils.isElementPresent(eleObject))) return true;
					else{
						System.out.println(objName+" is still displayed in UI.Dynamic wait for - InVisibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+objName+" is still displayed in UI.Dynamic wait for - InVisibility of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!", "", "dynamicWait",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
					
				case "OBJECT_CLICKABLE":
					try{
						wait.withTimeout(timeSeconds,TimeUnit.SECONDS)
						.pollingEvery(timeSeconds_Polling, TimeUnit.SECONDS)				 
						.ignoring(StaleElementReferenceException.class)
						.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(eleObject)));
					}		
					catch(org.openqa.selenium.TimeoutException e){
						strEndWaitTime = LocalDateTime.now();
						System.out.println("Dynamic wait for - 'ElementToBeClickable' of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Dynamic wait for - 'ElementToBeClickable' of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!", "", "dynamicWait",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of try-catch

					if(!(CommonUtils.isElementPresent(eleObject))) return true;
					else{
						System.out.println(objName+" is still displayed in UI.Dynamic wait for - 'ElementToBeClickable' of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+objName+" is still displayed in UI.Dynamic wait for - 'ElementToBeClickable' of " + objName + " has timedout[Max threshold of "+timeSeconds+" seconds].Please check!!", "", "dynamicWait",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
					
					
				default:
					return false;
			
			}//End of Switch case

	}    	
    
	//######################################################################################################################
    //Function name			: wait_ForObject_ToDisappear(WebElement objTest, String objName,int timeSeconds,int timePollingSeconds,int exclRowCnt,String strSheetName)
    //Class name			: Maxit_CommonFns
    //Description 			: Function to wait for an object to disappear from UI
    //Parameters 			: 
    //Assumption			: None
    //Developer				: Kavitha Golla
	//######################################################################################################################	
	public  boolean wait_ForObject_ToDisappear(WebElement objTest, String objName,int timeSeconds,int timePollingSeconds,int exclRowCnt,String strSheetName) throws Exception{
		try{
			if(!CommonUtils.isElementPresent(objTest)){
				System.out.println(objName + " has disappeared");
				return true;
			}
			for(int i = 1; i <=timeSeconds; i++){
				if(CommonUtils.isElementPresent(objTest)){
					if(i ==1){
						System.out.println("Wait cycle starts >> " + objName + " is still displayed...");
					}
					Thread.sleep(timePollingSeconds*1000);
				}else{
					System.out.println(objName + " >> has disappeared");
					return true;
				}
			}
			 
			System.out.println("Loading of " + objName + " is too slow. Please check!!! ");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+objName+" is still displayed in UI.Waited for object to disapper for ["+timeSeconds+"seconds].Please check!!", "", "wait_ForObject_ToDisappear",  driver, "HORIZONTALLY", strSheetName, null);
  			return false; 
			 
		}catch(Exception e){
			System.out.println("Exception found in method >>wait_ForObject_ToDisappear, class = Maxit_CommonFns, while trying to wait for "+objName+" to disappear. Please check!!! ");
  			e.printStackTrace();
  			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+objName+" is still displayed in UI.Waited for object to disapper for ["+timeSeconds+"seconds].Please check!!", "", "wait_ForObject_ToDisappear",  driver, "HORIZONTALLY", strSheetName, null);
  			return false;
		}
	}//End of <Method: wait_ForObject_ToDisappear>
    	
}//End of <Class: MaxitCommonFns>
