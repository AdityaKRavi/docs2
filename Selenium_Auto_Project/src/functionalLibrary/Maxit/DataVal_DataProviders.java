package functionalLibrary.Maxit;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.ManageDriver_Maxit;

public class DataVal_DataProviders {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	ConfigInputFile objConfigInputFile = new ConfigInputFile();
	DataTable dt = new DataTable();
	//GlobalObjects gloObj = new GlobalObjects();
	
////###################################################################################################################################################################  
////Function name		: Read_Ledger(ITestContext context)
////Class name		: DataVal_DataProviders
////Description 		: Generic method to read data from Ledger sheet for data Validation Data Provider
////Parameters 		: context: ITestContest to read testNG.xml parameters
////Assumption		: None
////Developer			: Kavitha Golla
////###################################################################################################################################################################		
//	@DataProvider(name="Ledger")
//	public Object[][] Read_Ledger(ITestContext context)throws Exception{
//	//public Object[][] Read_Ledger()throws Exception{
//		
//		//ITestContext context = gloObj.getiTestContext();
//		//ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();
//		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
//		String strEnv = context.getCurrentXmlTest().getParameter("environment");
//		String strClient = context.getCurrentXmlTest().getParameter("clientName");
//		
//	    String excelPath = objConfigInputFile.getInputFIlePath(strScriptName,strEnv,strClient);
////		String excelPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\edj_StandardBasicProcessing_DataValidation.xls";	  
//		System.out.println("Excel Path is :"+excelPath);
//		return(objConfigInputFile.getData_DataValidation(excelPath, "Ledger","Ledger"));
//	  
//	}//End of DataProvider "Read_Ledger"
//
////###################################################################################################################################################################  
////Function name		: Read_RGL(ITestContext context)
////Class name		: DataVal_DataProviders
////Description 		: Generic method to read data from RGL sheet for data Validation Data Provider
////Parameters 		: context: ITestContest to read testNG.xml parameters
////Assumption		: None
////Developer			: Kavitha Golla
////###################################################################################################################################################################			
//	@DataProvider(name="Realized")
//	public Object[][] Read_RGL(ITestContext context)throws Exception{
//		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
//		String strEnv = context.getCurrentXmlTest().getParameter("environment");
//		String strClient = context.getCurrentXmlTest().getParameter("clientName");
//		
//	    String excelPath = objConfigInputFile.getInputFIlePath(strScriptName,strEnv,strClient);
////		String excelPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\edj_StandardBasicProcessing_DataValidation.xls";	  
//		System.out.println("Excel Path is :"+excelPath);
//	  return(objConfigInputFile.getData_DataValidation(excelPath, "Realized","Realized"));	  
//	}//End of DataProvider "Read_RGL"
//
////###################################################################################################################################################################  
////Function name		: Read_UGL(ITestContext context)
////Class name		: DataVal_DataProviders
////Description 		: Generic method to read data from UGL sheet for data Validation Data Provider
////Parameters 		: context: ITestContest to read testNG.xml parameters
////Assumption		: None
////Developer			: Kavitha Golla
////###################################################################################################################################################################		
//	@DataProvider(name="Unrealized")
//	public Object[][] Read_UGL(ITestContext context)throws Exception{
//		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
//		String strEnv = context.getCurrentXmlTest().getParameter("environment");
//		String strClient = context.getCurrentXmlTest().getParameter("clientName");
//		
//	    String excelPath = objConfigInputFile.getInputFIlePath(strScriptName,strEnv,strClient);
////		String excelPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\edj_StandardBasicProcessing_DataValidation.xls";	  
//		System.out.println("Excel Path is :"+excelPath);
//	  return(objConfigInputFile.getData_DataValidation(excelPath, "Unrealized","Unrealized"));	  
//	}//End of DataProvider "Read_UGL"	
//
////###################################################################################################################################################################  
////Function name		: Read_OpenClosed(ITestContext context)
////Class name		: DataVal_DataProviders
////Description 		: Generic method to read data from Open/Closed sheet for data Validation Data Provider
////Parameters 		: context: ITestContest to read testNG.xml parameters
////Assumption		: None
////Developer			: Kavitha Golla
////###################################################################################################################################################################	
//	@DataProvider(name="OpenClosed")
//	public Object[][] Read_OpenClosed(ITestContext context)throws Exception{
//		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
//		String strEnv = context.getCurrentXmlTest().getParameter("environment");
//		String strClient = context.getCurrentXmlTest().getParameter("clientName");
//		
//	    String excelPath = objConfigInputFile.getInputFIlePath(strScriptName,strEnv,strClient);
////		String excelPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\edj_StandardBasicProcessing_DataValidation.xls";	  
//		System.out.println("Excel Path is :"+excelPath);
//	  return(objConfigInputFile.getData_DataValidation(excelPath, "OpenClosed","Open/Closed"));
//	  
//	}//End of DataProvider "Read_OpenClosed"	
//	
////###################################################################################################################################################################  
////Function name		: Read_RawTrades(ITestContext context)
////Class name		: DataVal_DataProviders
////Description 		: Generic method to read data from Raw_Trades sheet for data Validation Data Provider
////Parameters 		: context: ITestContest to read testNG.xml parameters
////Assumption		: None
////Developer			: Kavitha Golla
////###################################################################################################################################################################	
//	@DataProvider(name="RawTrades")
//	public Object[][] Read_RawTrades(ITestContext context)throws Exception{
//		String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
//		String strEnv = context.getCurrentXmlTest().getParameter("environment");
//		String strClient = context.getCurrentXmlTest().getParameter("clientName");
//		
//	    String excelPath = objConfigInputFile.getInputFIlePath(strScriptName,strEnv,strClient);
////		String excelPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\edj_StandardBasicProcessing_DataValidation.xls";	  
//		System.out.println("Excel Path is :"+excelPath);
//	  return(objConfigInputFile.getData_DataValidation(excelPath, "RawTrades","Raw Trades"));
//	  
//	}//End of DataProvider "Read_RawTrades"	
	
	
	//###################################################################################################################################################################  
	//Function name		: Read_DataValTestData(ITestContext context,Method mSheetName)
	//Class name		: DataVal_DataProviders
	//Description 		: Generic method to read data from Raw_Trades sheet for data Validation Data Provider
	//Parameters 		: context: ITestContest to read testNG.xml parameters
	//					: mSheetName : Send calling Method (@Test method name) name as parameter
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################	
		@DataProvider(name="Read_DataValTestData")
		public Object[][] dp_DataVal(ITestContext context,Method mSheetName)throws Exception{
			String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			String strEnv = context.getCurrentXmlTest().getParameter("environment");
			String strClient = context.getCurrentXmlTest().getParameter("clientName");
			
		    String excelPath = dt.getInputFIlePath_Maxit(strScriptName,strEnv,strClient);
//			String excelPath = "C:\\SVN_Automation_New\\Trunk\\Selenium_Automation\\Selenium_Auto_Project\\TestData\\Maxit_TestData\\edj_StandardBasicProcessing_DataValidation.xls";	  
			System.out.println("Excel Path is :"+excelPath);
			String strSheetName = mSheetName.getName().trim();
		  return(objConfigInputFile.getData_DataValidation(excelPath, strSheetName));
		  
		}//End of DataProvider "Read_RawTrades"		
	
//###################################################################################################################################################################		
}//End of DataVal_DataProviders
//###################################################################################################################################################################	
