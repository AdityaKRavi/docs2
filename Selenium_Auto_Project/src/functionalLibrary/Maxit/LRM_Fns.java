package functionalLibrary.Maxit;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import PageObjects.Maxit.CBManager_Objs;
import PageObjects.Maxit.SearchPage_POM;
import PageObjects.Maxit.SearchPage_Results;
import PageObjects.Maxit.SearchRes_ActionMenu;
import PageObjects.Maxit.SearchRes_CBMethod_Objs;
import PageObjects.Maxit.StepUp_Objs;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class LRM_Fns {
	WebDriver driver;
	
	Maxit_CommonFns maxitFns;
	SearchPageFns objSearchPageFns;// = new SearchPageFns();
	SearchRes_PopUpFns objRes_ActionFns;// = new SearchRes_PopUpFns();
	
	SearchPage_Results searchRes_Objs;// = PageFactory.initElements(driver, SearchPage_Results.class);
	SearchRes_CBMethod_Objs CBM_Objs ;//= PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	SearchRes_ActionMenu actionMenu_Objs;// = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
	Reporting reporter = new Reporting();

	
	Map<String, Boolean> Update_StatusMapper = new HashMap<String, Boolean>();
	String mapperKey;
	
	public LRM_Fns(WebDriver driver2) {
		this.driver = driver2;
		this.maxitFns = PageFactory.initElements(this.driver, Maxit_CommonFns.class);
		this.objSearchPageFns = PageFactory.initElements(this.driver, SearchPageFns.class);
		this.objRes_ActionFns = PageFactory.initElements(this.driver, SearchRes_PopUpFns.class);
		this.searchRes_Objs = PageFactory.initElements(this.driver, SearchPage_Results.class);
		this.CBM_Objs = PageFactory.initElements(this.driver, SearchRes_CBMethod_Objs.class);
		this.actionMenu_Objs = PageFactory.initElements(this.driver, SearchRes_ActionMenu.class);
	}
	//###################################################################################################################################################################  
	//Function name		: TxnLRM_Update_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: LRM_Fns
	//Description 		: Function to list the methods for verifying Search_Account page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  boolean TxnLRM_Update_Flow(String UseCase,String strAcct,String strSecType,String strSecValue,String LRM_UpdLevel,String Existing_LRM,String New_LRM,String strTxnAccType,int exclRowCnt,String strSheetName,String strClient) throws Exception{
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		String strViewPage="Raw Trades";
		int intEventColNo, intResRows;
		int intFirst_ClosingEvent_RowNo;
		mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+UseCase+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
		try{		
			String strSearchValues = "[LRM_TxnLevel: RawTrades;"+strAcct+";"+strSecType+";"+strSecValue+"]";
			if(objSearchPageFns.Search_Input(strViewPage, strAcct , strSecType,strSecValue,exclRowCnt,strSheetName)){
				intResRows = this.LRM_SearchRes_Rows(strViewPage, strSearchValues, exclRowCnt, strSheetName);
				if(intResRows==0){
					return false;
				}//End of IF Condition to check if intResRows==0
				
				//Get 'Event' column index from UI:
				intEventColNo = searchRes_Objs.searchRes_GetColIndexByColName("Event");
				if(intEventColNo==-1){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_Results table doesn't have 'Event' column ,please check..!!", "", "SearchRes_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
				
				//Identify first closing event based on Account type:
				intFirst_ClosingEvent_RowNo = searchRes_Objs.SearchResult_Get_ClosingEvent_RowNo(intResRows, strTxnAccType, exclRowCnt, strSheetName);
    			if(intFirst_ClosingEvent_RowNo==0){
    				if(strTxnAccType.equalsIgnoreCase("Long")){
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No closing event(Sell/Sell To Close) transaction found in 'Event' column. Please check.", "", "Res_GetClosingEvent_RowNum",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}
    				else{
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No closing event(Purchase/Buy To Close/Cover Short) transaction found in 'Event' column. Please check.", "", "Res_GetClosingEvent_RowNum",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}
    			}//End of IF condition to check if intFirst_ClosingEvent_RowNo==0

    			//Category: "UpdateOnly"
    			if(!(this.TxnLevel_LRM_Update_Fns(intFirst_ClosingEvent_RowNo, Existing_LRM, New_LRM, exclRowCnt, strSheetName))){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] LRM Update failed for Row#"+exclRowCnt+". Please check.", "", "LRMUpdateDail",  driver, "HORIZONTALLY", strSheetName, null);
					if(CBM_Objs.btn_CBMXClose.size()>0){
						CBM_Objs.btn_CBMXClose.get(0).click();
					}
					return false;
    			}
				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}
			}//End of IF condition to check Search_Input Checkpoint
			return true;
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in <Method: TxnLRM_Update_Flow>,please check..!!"+e.getMessage());
			reporter.reportStep(exclRowCnt, "Failed", "Exception in TxnLRM_Update_Flow ", "", "TxnLRM_Update_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}//End of Catch block
	}//End of >Method: TxnLRM_Update_Flow>
	
	//###################################################################################################################################################################  
	//Function name		: TxnLRM_Update_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to get total number of search results rows and 0 if 'No results returened'
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public int LRM_SearchRes_Rows(String strViewPage,String strSearchValues,int exclRowCnt, String strSheetName){	
		//Verify search results row number, if No Results returned fail the case.		
		int appRowsReturned = searchRes_Objs.searchRes_NumOfRows_app(strViewPage);
		if(!(appRowsReturned>0)){
			if(searchRes_Objs.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
				System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return 0;
			}
			System.out.println("Search Results for "+strSearchValues+" is not >0");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return 0;
		}//End of IF condition to check No of rows returned
		return appRowsReturned;
	}//End of <Method: LRM_SearchRes_Rows>

	//###################################################################################################################################################################  
	//Function name		: TxnLRM_Update_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to get total number of search results rows and 0 if 'No results returened'
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public int LRM_FirstClosingEvent(String strViewPage,String strTxnAccType,int intResRows,int exclRowCnt, String strSheetName){	
		int intFirst_ClosingEvent_RowNo;
		intFirst_ClosingEvent_RowNo = searchRes_Objs.SearchResult_Get_ClosingEvent_RowNo(intResRows, strTxnAccType, exclRowCnt, strSheetName);
		return intFirst_ClosingEvent_RowNo;

	}//End of <Method: LRM_FirstClosingEvent>
	
	
	//###################################################################################################################################################################  
	//Function name		: TxnLevel_Details_LRMUpdate(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to get total number of search results rows and 0 if 'No results returened'
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public boolean TxnLevel_Details_LRMUpdate(String strPopUpName,String New_LRM,int exclRowCnt, String strSheetName) throws Exception{	
		String strErrorMessage;
		CBM_Objs.ele_Det_SellMethod.get(0).click();
		if(!(maxitFns.setValue_ComboList("CurrentSellMethod", actionMenu_Objs.ele_PopUpCombo, New_LRM, exclRowCnt, strSheetName))){
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Txn Level_LRM Update failed,please check.!!", "", "LRMUpdate",  driver, "HORIZONTALLY", strSheetName, null);
			return false;
		}//End of IF condition to check if Selecting New_LRM is passed
			
    	if(!(CBM_Objs.btn_CBMSave.size()>0)){
    		if(!(CBM_Objs.btn_CBMSave.get(0).isEnabled())){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed", "", "Details_Save",  driver, "HORIZONTALLY", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed,please check..!!");
	    		return false;
    		}
    		
		}//End of if condition to check if SAVE button is displayed in Details tab
    	else CBM_Objs.btn_CBMSave.get(0).click();
    	//Thread.sleep(10000);
    	//Dynamic wait for Loading object to disappear: Max Threshold of 60Sec
    	if(!(maxitFns.wait_ForObject_ToDisappear(CBM_Objs.ele_LRMSaveLoading, "Loading", 60, 2, exclRowCnt, strSheetName))){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] UI is very slow,LRM Update SAVE is still Loading since 60seconds. Please check!!", "", "LRM_Save",  driver, "HORIZONTALLY", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] UI is very slow,LRM Update SAVE is still Loading since 60seconds. Please check!!");
			
    		if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
    	}
    	
		//Verify if "Unable to Process Error"
		if(CBM_Objs.ele_OrangeErrors.size()>0){
			strErrorMessage = CBM_Objs.ele_OrangeErrors.get(0).getAttribute("textContent");
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] LRM Update failed, Error Message:"+strErrorMessage, "", "Details_Save",  driver, "HORIZONTALLY", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] LRM Update failed, Error Message:"+strErrorMessage+".Please check..!!");
			
    		if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}//End of IF condition to check for errors.
		
		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Txn Level_LRM Update passed", "", "LRMUpdate",  driver, "HORIZONTALLY", strSheetName, null);
		return true;
	}//End of <Method: TxnLevel_Details_LRMUpdate>
	
	//###################################################################################################################################################################  
	//Function name		: TxnLevel_LRMVerifyUpdate_Saved(String New_LRM,int exclRowCnt, String strSheetName) throws Exception{
	//Class name		: LRM_Fns
	//Description 		: Use this function to verify if the updated LRM is saved or not in RawTrades-->Details pop up
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public boolean TxnLevel_LRMVerifyUpdate_Saved(int intFirst_ClosingEvent_RowNo,String New_LRM,int exclRowCnt, String strSheetName){	
		String strLrmValueFoundAfterUpdate;
		
		//Click Action->Details
		if(!objRes_ActionFns.searchRes_ClickAction(intFirst_ClosingEvent_RowNo, "Details", "rightaction", "Raw Trades")){						
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Click Action->Details failed.", "", "SearchRes_ClickDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		}//End of IF condition to click Action menu.
		
		if(!actionMenu_Objs.verifyPopUpExists("RAWTRADES_DETAILS")){
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_RawTrades->Details pop up not displayed", "", "SearchRes_ClickDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Checkpoint :[Failed] Search_RawTrades->Details pop up not displayed");
        	if(CBM_Objs.btn_CBMXClose.size()>0){
        		CBM_Objs.btn_CBMXClose.get(0).click();
        	}
			return false;
		}//End of IF condition to check if the Details pop up Exists
		
		CBM_Objs.ele_Det_SellMethod.get(0).click();
		strLrmValueFoundAfterUpdate = maxitFns.getSelectedValue_ComboList("CurrentSellMethod", actionMenu_Objs.ele_PopUpCombo, exclRowCnt, strSheetName);
		CBM_Objs.btn_CBMCancel.get(0).click();
		if(strLrmValueFoundAfterUpdate.equalsIgnoreCase(New_LRM)){
			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Verifying LRM update after saving is Passed. LRM updated to "+strLrmValueFoundAfterUpdate, "", "LRMUpdate",  driver, "HORIZONTALLY", strSheetName, null);
    		if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return true;
		}
		else{
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Verifying LRM update after saving Failed. Expected LRM="+New_LRM+" but actual LRM after Update:"+strLrmValueFoundAfterUpdate, "", "LRMUpdate_fail",  driver, "HORIZONTALLY", strSheetName, null);
    		if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}
			
	}//End of <Method: TxnLevel_LRMVerifyUpdate_Saved>	
	
	
	//###################################################################################################################################################################  
	//Function name		: TxnLevel_LRMCheck(String New_LRM,int exclRowCnt, String strSheetName) throws Exception{
	//Class name		: LRM_Fns
	//Description 		: Use this function to verify LRM values " Existing Vs New LRM" before updating
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public boolean TxnLevel_LRMCheck(String Existing_LRM,String New_LRM,int exclRowCnt, String strSheetName){	
		//Verify Existing LRM Vs New LRM
		String strUI_ExistingLRM;
		CBM_Objs.ele_Det_SellMethod.get(0).click();
		strUI_ExistingLRM = maxitFns.getSelectedValue_ComboList("CurrentSellMethod", actionMenu_Objs.ele_PopUpCombo, exclRowCnt, strSheetName);
		if(strUI_ExistingLRM.isEmpty()||strUI_ExistingLRM==null){
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Existing LRM in UI is blank,please check.", "", "ExistingLRM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			//Update_StatusMapper.put(mapperKey, false);
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}
		CBM_Objs.ele_Det_SellMethod.get(0).click();
		if(strUI_ExistingLRM.equalsIgnoreCase(Existing_LRM)){
			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Existing LRM Check - Actual LRM matches with the Existing LRM value from test data.", "", "ExistingLRM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		}
		else{
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Existing LRM Check - Current Sell Method[Exisitng LRM] is differnt from expected 'Exiting LRM'.Actual LRM:"+strUI_ExistingLRM+" and expected LRM:"+Existing_LRM, "", "ExistingLRM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			//Update_StatusMapper.put(mapperKey, false);
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}//End of ELSE condition to check strUI_ExistingLRM==Existing_LRM
		
		if(strUI_ExistingLRM.equalsIgnoreCase(New_LRM)){
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] New LRM to apply is same as existing LRM["+New_LRM+"]. Invalid test data.", "", "ExistingLRM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			//Update_StatusMapper.put(mapperKey, false);
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
			
		}//End of IF condition to check strUI_ExistingLRM==New_LRM
			
		return true;
	}//End of <Method: TxnLevel_LRMCheck>			
	
	//###################################################################################################################################################################  
	//Function name		: TxnLevel_LRMNavigateDetails(int intRowNo,int exclRowCnt, String strSheetName) throws Exception{
	//Class name		: LRM_Fns
	//Description 		: Use this function to navigate to Raw Trades->Details from Action menu
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public boolean TxnLevel_LRMNavigateDetails(int intRowNo,int exclRowCnt, String strSheetName){	
		//Click Action->Details
		if(!objRes_ActionFns.searchRes_ClickAction(intRowNo, "Details", "rightaction", "Raw Trades")){						
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Click Action->Details failed.", "", "SearchRes_ClickDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}//End of IF condition to click Action menu.
		
		if(!actionMenu_Objs.verifyPopUpExists("RAWTRADES_DETAILS")){
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_RawTrades->Details pop up not displayed", "", "SearchRes_ClickDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Checkpoint :[Failed] Search_RawTrades->Details pop up not displayed");
        	if(CBM_Objs.btn_CBMXClose.size()>0){
        		CBM_Objs.btn_CBMXClose.get(0).click();
        	}
			return false;
		}//End of IF condition to check if the Details pop up Exists
					
		CBM_Objs.verifyDetails_Checkpoints(exclRowCnt, strSheetName);
		return true;
	}//End of <Method: TxnLevel_LRMNavigateDetails>			
	
	
	//###################################################################################################################################################################  
	//Function name		: TxnLevel_LRMNavigateDetails(int intRowNo,int exclRowCnt, String strSheetName) throws Exception{
	//Class name		: LRM_Fns
	//Description 		: This function has the entire flow from Navigation to updating the LRM value in Raw Trades->Details
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public boolean TxnLevel_LRM_Update_Fns(int intRowNo,String Existing_LRM,String New_LRM,int exclRowCnt, String strSheetName) throws Exception{	
		//Category: "UpdateOnly"
		if(!(this.TxnLevel_LRMNavigateDetails(intRowNo, exclRowCnt, strSheetName))){
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}

		//Verify Existing LRM Vs New LRM
		if(!(this.TxnLevel_LRMCheck(Existing_LRM, New_LRM, exclRowCnt, strSheetName))){
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}//End of IF condition to check Existing LRM Vs New LRM
		
		
		//Method: TxnLevel_Details_LRMUpdate
		if(!(this.TxnLevel_Details_LRMUpdate("Details", New_LRM, exclRowCnt, strSheetName))){
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Txn Level_LRM Update failed,please check.!!", "", "LRMUpdate",  driver, "HORIZONTALLY", strSheetName, null);
			//Update_StatusMapper.put(mapperKey, false);
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}//End of IF condition to Update LRM in front end
		
    	if(CBM_Objs.btn_CBMXClose.size()>0){
    		CBM_Objs.btn_CBMXClose.get(0).click();
    	}
		return true;
	}//End of <Method: TxnLevel_LRM_UpdateFlow>			
	
	//###################################################################################################################################################################  
	//Function name		: TxnLevel_RawTrades_OtherSells_Unaffected(int intRowNo,int exclRowCnt, String strSheetName){
	//Class name		: LRM_Fns
	//Description 		: Use this function to verify other sells(closing event types) are not affected by the updated New_LRM value in Raw Trades-->Details
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public boolean TxnLevel_RawTrades_OtherSells_Unaffected(int intTotalResRows,int intFirst_ClosingEvent_RowNo,int intEventColNo,String strTxnAccType,String Existing_LRM,int exclRowCnt, String strSheetName){		
		boolean blnCloseEvent=false;
		if(intFirst_ClosingEvent_RowNo==intTotalResRows){
			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Check Other Closing Txn LRM: There are no other closing events.", "", "OtherSells",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Checkpoint :[Passed] Check Other Closing Txn LRM: There are no other closing events. intFirst_ClosingEvent_RowNo is"+intFirst_ClosingEvent_RowNo+" and intTotalResRows is"+(intTotalResRows));
			return true;
		}
		
		for(int iLoop=intFirst_ClosingEvent_RowNo+1;iLoop<=intTotalResRows;iLoop++){
			//Check if account type is long or short and then compare close event:
			if(strTxnAccType.equalsIgnoreCase("Long")){
				if((searchRes_Objs.searchRes_getCellData(iLoop-1, "Event").equalsIgnoreCase("Sell")) || (searchRes_Objs.searchRes_getCellData(iLoop-1, "Event").equalsIgnoreCase("Sell To Close"))){
					blnCloseEvent = true;
				}
			}//End of IF condition to check AccountType=Long
			else{
				if((searchRes_Objs.searchRes_getCellData(iLoop-1, "Event").equalsIgnoreCase("Purchase")) || (searchRes_Objs.searchRes_getCellData(iLoop-1, "Event").equalsIgnoreCase("Cover Short")) ||(searchRes_Objs.searchRes_getCellData(iLoop-1, "Event").equalsIgnoreCase("Buy To Close")) ){
					blnCloseEvent = true;
				}
			}//End of Else condition to check AccountType
		
		if(blnCloseEvent){
			if(!(this.TxnLevel_LRMNavigateDetails(iLoop, exclRowCnt, strSheetName))){
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}
				return false;
			}
			
			String strOtherTxnExistingLrm;
			CBM_Objs.ele_Det_SellMethod.get(0).click();
			strOtherTxnExistingLrm = maxitFns.getSelectedValue_ComboList("CurrentSellMethod", actionMenu_Objs.ele_PopUpCombo, exclRowCnt, strSheetName);
			if(strOtherTxnExistingLrm.isEmpty()||strOtherTxnExistingLrm==null){
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Check Other Closing Txn LRM : Existing LRM in UI is blank for UI Results row#"+iLoop+",please check.", "", "ExistingLRM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}
				return false;
			}
			if(!(strOtherTxnExistingLrm.equalsIgnoreCase(Existing_LRM))){
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Check Other Closing Txn LRM - Actual LRM in UI row#" + (iLoop) + " is " + strOtherTxnExistingLrm + ". Mismatch with existing LRM value of -"+Existing_LRM, "", "ExistingLRM",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}
			else{
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Check Other Closing Txn LRM - Closing Txn in UI row#"+iLoop+" is same as Existing LRM:"+Existing_LRM, "", "ExistingLRM",  driver, "HORIZONTALLY", strSheetName, null);
				return true;
			}
		}//End of IF condition to check if we got a closing event
		
		}//End of FOR loop
		if(CBM_Objs.btn_CBMXClose.size()>0){
			CBM_Objs.btn_CBMXClose.get(0).click();
		}
		return true;
	}//End of <Method: TxnLevel_RawTrades_OtherSells_Unaffected>
	
	//###################################################################################################################################################################  
	//Function name		: TxnLRM_VerifyUpdate_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: LRM_Fns
	//Description 		: Function to list the methods for verifying LRM update in Txn level
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  boolean TxnLRM_VerifyUpdate_Flow(String UseCase,String strAcct,String strSecType,String strSecValue,String LRM_UpdLevel,String Existing_LRM,String New_LRM,String strTxnAccType,int exclRowCnt,String strSheetName,String strClient) throws Exception{
		driver = ManageDriver_Maxit.getManageDriver().getDriver();
		String strViewPage="Raw Trades";
		int intEventColNo, intResRows;
		int intFirst_ClosingEvent_RowNo;
		mapperKey = "[To_Execute:Y;useCaseNumFromSheet:"+UseCase+";Account:"+strAcct+";SecValue:"+strSecValue+"]";
		try{		

			String strSearchValues = "[LRM_TxnLevel: RawTrades;"+strAcct+";"+strSecType+";"+strSecValue+"]";
			//System.out.println("\n#########################Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
			reporter.reportStep(exclRowCnt, "Passed", "\n Starting LRM update verification. ", "", "LRMUpdate_verify",  driver, "BOTH_DIRECTIONS", strSheetName, null);

			if(objSearchPageFns.Search_Input(strViewPage, strAcct , strSecType,strSecValue,exclRowCnt,strSheetName)){
				intResRows = this.LRM_SearchRes_Rows(strViewPage, strSearchValues, exclRowCnt, strSheetName);
				if(intResRows==0){
					return false;
				}//End of IF Condition to check if intResRows==0
				
				//Get 'Event' column index from UI:
				intEventColNo = searchRes_Objs.searchRes_GetColIndexByColName("Event");
				if(intEventColNo==-1){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_Results table doesn't have 'Event' column ,please check..!!", "", "SearchRes_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
				
				//Identify first closing event based on Account type:
				intFirst_ClosingEvent_RowNo = searchRes_Objs.SearchResult_Get_ClosingEvent_RowNo(intResRows, strTxnAccType, exclRowCnt, strSheetName);
    			if(intFirst_ClosingEvent_RowNo==0){
    				if(strTxnAccType.equalsIgnoreCase("Long")){
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No closing event(Sell/Sell To Close) transaction found in 'Event' column. Please check.", "", "Res_GetClosingEvent_RowNum",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}
    				else{
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No closing event(Purchase/Buy To Close/Cover Short) transaction found in 'Event' column. Please check.", "", "Res_GetClosingEvent_RowNum",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}
    				
    			}//End of IF condition to check if intFirst_ClosingEvent_RowNo==0
				
				
    			
    			//Category: "Verify_Updated LRM"
    			if(!(this.TxnLevel_LRM_VerifyUpdate_Fns(intResRows, intFirst_ClosingEvent_RowNo, intEventColNo, strTxnAccType, Existing_LRM, New_LRM, exclRowCnt, strSheetName))){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] LRM Update failed for Row#"+exclRowCnt+". Please check.", "", "LRMUpdateDail",  driver, "HORIZONTALLY", strSheetName, null);
					if(CBM_Objs.btn_CBMXClose.size()>0){
						CBM_Objs.btn_CBMXClose.get(0).click();
					}
					return false;
    			}

				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}

			}//End of IF condition to check Search_Input Checkpoint

			//System.out.println("###########################################################[End of TxnLRM_VerifyUpdate for Row iteration:["+exclRowCnt+"]###########################################################");
			return true;
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in TxnLRM_VerifyUpdate_Flow Test Flow <Method: TxnLRM_VerifyUpdate_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in TxnLRM_VerifyUpdate_Flow ", "", "TxnLRM_VerifyUpdate_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}//End of Catch block
		//return false;
	}//End of >Method: TxnLRM_VerifyUpdate_Flow>

	//###################################################################################################################################################################  
	//Function name		: TxnLevel_LRM_VerifyUpdate_Fns(int intTotalResRows,int intFirst_ClosingEvent_RowNo,int intEventColNo,String strTxnAccType,String Existing_LRM,String New_LRM,int exclRowCnt, String strSheetName){
	//Class name		: LRM_Fns
	//Description 		: Use this function to verify the updated LRM and ensure other closing events are not affected by the update.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public boolean TxnLevel_LRM_VerifyUpdate_Fns(int intTotalResRows,int intFirst_ClosingEvent_RowNo,int intEventColNo,String strTxnAccType,String Existing_LRM,String New_LRM,int exclRowCnt, String strSheetName){	
		//Method: Verify LRM update saved
		if(!(this.TxnLevel_LRMVerifyUpdate_Saved(intFirst_ClosingEvent_RowNo, New_LRM, exclRowCnt, strSheetName))){
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}//End of IF condition Verify LRM update saved

		//Method: Verify other Closing Events are not effected
		if(!(this.TxnLevel_RawTrades_OtherSells_Unaffected(intTotalResRows, intFirst_ClosingEvent_RowNo, intEventColNo, strTxnAccType, Existing_LRM, exclRowCnt, strSheetName))){
			if(CBM_Objs.btn_CBMXClose.size()>0){
				CBM_Objs.btn_CBMXClose.get(0).click();
			}
			return false;
		}//End of IF condition Verify other Closing Events are not effected
		
		
    	if(CBM_Objs.btn_CBMXClose.size()>0){
    		CBM_Objs.btn_CBMXClose.get(0).click();
    	}
		return true;
	}//End of <Method: TxnLevel_LRM_VerifyUpdate_Fns>		


}//End of <class: LRM_Fns>
