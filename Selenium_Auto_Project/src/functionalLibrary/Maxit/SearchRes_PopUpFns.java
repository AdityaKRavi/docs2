package functionalLibrary.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import PageObjects.Maxit.AvgGiftCost_Objs;
import PageObjects.Maxit.NoCostBasis_Objs;
import PageObjects.Maxit.SearchPage_Results;
import PageObjects.Maxit.SearchRes_ActionMenu;
import PageObjects.Maxit.SearchRes_CBMethod_Objs;
import PageObjects.Maxit.SearchRes_FIElection_Objs;
import PageObjects.Maxit.SearchRes_MTM_Objs;
import PageObjects.Maxit.TaxLotEdit_Objs;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class SearchRes_PopUpFns {

	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	Reporting reporter = new Reporting();
	SearchRes_ActionMenu SearchRes_Action = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
	SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
	
	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	SearchRes_FIElection_Objs FI_Objs = PageFactory.initElements(driver, SearchRes_FIElection_Objs.class);
	SearchRes_MTM_Objs MTM_Objs = PageFactory.initElements(driver, SearchRes_MTM_Objs.class);
	TaxLotEdit_Objs TLE_Objs = PageFactory.initElements(driver, TaxLotEdit_Objs.class);
	NoCostBasis_Objs NCB_Objs = PageFactory.initElements(driver, NoCostBasis_Objs.class);
	AvgGiftCost_Objs AvgGift_Objs = PageFactory.initElements(driver, AvgGiftCost_Objs.class);

//###################################################################################################################################################################  
//Function name		: searchRes_ClickAction(int appRowNum,String strDesiredPageLink, String strWhichActionButton)
//Class name		: SearchRes_PopUps
//Description 		: Function to click on the action menu item
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public boolean searchRes_ClickAction(int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName){
			try{
				if(!SearchRes_Action.searchRes_VerifyActionButtonExists(strWhichActionButton,appRowNum)){
					System.out.println("<Method:searchRes_ClickAction >Search_Results Table Action button is NOT displayed,please check..!!");
					return false;
				}//End of IF condition to check if ActionButton Exists
				
				//Modified [ 03/20/2018] : Removing Verifying Action menu list as per AUTO-417, instead adding checkpoint to verify if desired page exists or not in the Action Menu
//				if(!SearchRes_Action.verifyActionMenuList(strSearchPageName)){
//					System.out.println("<Method:searchRes_ClickAction >Search_Results Table Action Menu list is NOT displayed,please check..!!");
//					return false;
//				}//End of IF condition to check if Action menu List Exists
				
//				//verifyDesiredActionItem
//				if(!SearchRes_Action.verifyDesiredActionItem(strSearchPageName)){
//					System.err.println("<Method: searchRes_ClickAction >Search_Results Table Action Menu list is NOT displayed,please check..!!");
//					return false;
//				}//End of IF condition to check if Action menu List Exists
				
				//Click on desired page link from Action menu list:
				for (WebElement eachActionList : SearchRes_Action.ele_ActionMenuList) { 
					if(eachActionList.getAttribute("textContent").equalsIgnoreCase(strDesiredPageLink)){
						eachActionList.click();
						Thread.sleep(3000);						
						return true;
					}
				}//End of for loop to check Desired Page Link from Action Menu list
				System.out.println("<Method: searchRes_ClickAction >Action menu List doesn't display desired page link "+strDesiredPageLink+" ,please check..!!");
				return false;
		
			}//End of Try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_ActionMenu ><Method: searchRes_ClickAction > Please check..!!");
				return false;
			}//End of catch block
		}//End of <Method: searchRes_ClickAction>
	 
//###################################################################################################################################################################  
//Function name		: AcctLevel_CBMethod(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName)
//Class name		: SearchRes_PopUps
//Description 		: Function to verify Account Level CB Method pop up
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public void AcctLevel_CBMethod(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName,String strClient){
			try{
				if(!this.searchRes_ClickAction(1, "CB Method", "leftaction", "Account")){						
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint: Click Action->CB Method failed.", "", "SearchRes_ClickCBMethodAction",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Failed");
				}//End of IF condition to click Action menu.
			
				if(!SearchRes_Action.verifyPopUpExists("ACCTLEVEL_CBMETHOD")){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_Account->CB Method pop up not displayed", "", "SearchRes_ClickCBMethodAction",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] Search_Account->CB Method pop up not displayed");
				}//End of IF condition to check if the CB Method pop up Exists
			
				//Check for error messages in Pop up:
				if(!SearchRes_Action.verifyPopUpError("CBMethod", exclRowCnt, strSheetName)){
					return;
				}
				
				//function call to verify checkpoints:				
				CBM_Objs.verifyAccLevel_CBM_DropDowns(exclRowCnt, strSheetName,strClient);				
				SearchRes_Action.PopUp_EditHist("CBMethod", "ACCTLEVEL_CBMETHOD", exclRowCnt, strSheetName);
				SearchRes_Action.verifyPopUp_SaveCancel(exclRowCnt,strSheetName,"CBMethod");
				
			
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_PopUps><Method: AcctLevel_CBMethod >, Please check..!!");
			}//End of Catch block
		}//End of <Method: AcctLevel_CBMethodCheckpoints>
	
//###################################################################################################################################################################  
//Function name		: AcctLevel_FIElection(String strPopUpName,String strAcctSec_Level,int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName)
//Class name		: SearchRes_PopUps
//Description 		: Function to verify Account Level FI Election pop up
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public void AcctLevel_FIElection(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName){
			try{
				if(!this.searchRes_ClickAction(1, "FI-Elections", "rightaction", "Account")){						
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint: Click Action->FI-Elections failed.", "", "SearchRes_ClickFI-Elections",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of IF condition to click Action menu.
			
				if(!SearchRes_Action.verifyPopUpExists("ACCTLEVEL_FIELECTION")){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_Account->FIElection pop up not displayed", "", "SearchRes_ClickFIElection",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] Search_Account->FIElection pop up not displayed");
				}//End of IF condition to check if the CB Method pop up Exists
			
				//Check for error messages in Pop up:
				if(!SearchRes_Action.verifyPopUpError("FIElection", exclRowCnt, strSheetName)){
					return;
				}
				
				//function call to verify checkpoints:	
				FI_Objs.verifyAcct_FI_DropDowns(exclRowCnt, strSheetName);
				SearchRes_Action.PopUp_EditHist("FIElection", "ACCTLEVEL_FIELECTION", exclRowCnt, strSheetName);				
				SearchRes_Action.verifyPopUp_SaveCancel(exclRowCnt,strSheetName,"FIElection");
				
			
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_PopUps><Method: AcctLevel_FIElection >, Please check..!!");
			}//End of Catch block
		}//End of <Method: AcctLevel_FIElection>		
		
//###################################################################################################################################################################  
//Function name		: AcctLevel_MTM(String strPopUpName,String strAcctSec_Level,int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName)
//Class name		: SearchRes_PopUps
//Description 		: Function to verify Account Level FI Election pop up
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public void AcctLevel_MTM(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName){
			try{
				if(!this.searchRes_ClickAction(1, "Mark-To-Market", "leftaction", "Account")){						
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Click Action->Mark-To-Market.", "", "SearchRes_ClickMTM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of IF condition to click Action menu.
			
				if(!SearchRes_Action.verifyPopUpExists("ACCTLEVEL_MTM")){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_Account->MTM pop up not displayed", "", "SearchRes_ClickMTM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] Search_Account->MTM pop up not displayed");
				}//End of IF condition to check if the CB Method pop up Exists
				else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Search_Account->MTM pop up displayed", "", "SearchRes_ClickMTM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			
				//Check for error messages in Pop up:
				if(!SearchRes_Action.verifyPopUpError("MTM", exclRowCnt, strSheetName)){
					return;
				}
				
				//function call to verify checkpoints:	
				MTM_Objs.verifyAcct_MTM_DropDowns(exclRowCnt, strSheetName);
				//MTM_Objs.MTM_EditHist("MTM", "ACCTLEVEL_MTM", exclRowCnt, strSheetName);	
				SearchRes_Action.PopUp_EditHist("MTM", "ACCTLEVEL_MTM", exclRowCnt, strSheetName);
				SearchRes_Action.verifyPopUp_SaveCancel(exclRowCnt,strSheetName,"MTM");
				
			
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_PopUps><Method: AcctLevel_MTM >, Please check..!!");
			}//End of Catch block
		}//End of <Method: AcctLevel_MTM>			
		
//###################################################################################################################################################################  
//Function name		: SecLevel_CBMethod(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName)
//Class name		: SearchRes_PopUps
//Description 		: Function to verify Account Level CB Method pop up
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public void SecLevel_CBMethod(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName,String strClient){
			//SearchPage_Results ResTbl_objs = PageFactory.initElements(driver, SearchPage_Results.class);
			try{
				int colIndex = objSearchRes.searchRes_GetColIndexByColName("CBM");
				String strColValue = objSearchRes.searchRes_getColValue(1, colIndex) ;//fn to get Col Value from UI
				
				
				if(!this.searchRes_ClickAction(1, "CB Method", "leftaction", "Open/Closed")){						
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint: Click Action->CB Method failed.", "", "SearchRes_ClickCBMethodAction",  driver, "BOTH_DIRECTIONS", strSheetName, null);					
					return;
				}//End of IF condition to click Action menu.
			
				if(!SearchRes_Action.verifyPopUpExists("SECLEVEL_CBMETHOD")){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_Account->CB Method pop up not displayed", "", "SearchRes_ClickCBMethodAction",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] Search_Account->CB Method pop up not displayed");
				}//End of IF condition to check if the CB Method pop up Exists
			
				//Check for error messages in Pop up:
				if(!SearchRes_Action.verifyPopUpError("CBMethod", exclRowCnt, strSheetName)){
					return;
				}
				
				if(strColValue.equalsIgnoreCase("AVGCOST")){
					if(CBM_Objs.verifySecLevel_CBM_Tab(exclRowCnt, strSheetName)){
						CBM_Objs.verifySecLevel_CBM_AvgCost_DropDowns(exclRowCnt, strSheetName);
						SearchRes_Action.PopUp_EditHist("CBMethod", "SECLEVEL_CBMETHOD_CBM_AVG", exclRowCnt, strSheetName);
						CBM_Objs.verifyCBM_SaveCancel(exclRowCnt,strSheetName,"CBMethod");			
					}//End of IF condition to verify if CBM Tab is displayed.
				}//End of IF condition to check if the record is AVGCOST
				
				else{
					//function call to verify CBM tab checkpoints:	
					if(CBM_Objs.verifySecLevel_CBM_Tab(exclRowCnt, strSheetName)){
						CBM_Objs.verifySecLevel_CBM_DropDowns(exclRowCnt, strSheetName,strClient);
						SearchRes_Action.PopUp_EditHist("CBMethod", "SECLEVEL_CBMETHOD_CBM", exclRowCnt, strSheetName);
						CBM_Objs.verifyCBM_SaveCancel(exclRowCnt,strSheetName,"CBMethod");			
					}//End of IF condition to verify if CBM Tab is displayed.
				}//End of else condition to check if the record is NOT AVGCOST
				
				//function call to verify DRP tab checkpoints:	
				if(CBM_Objs.verifySecLevel_DRP_Tab(exclRowCnt, strSheetName)){
					CBM_Objs.verifySecLevel_DRP_DropDowns(exclRowCnt, strSheetName);
					SearchRes_Action.PopUp_EditHist("CBMethod", "SECLEVEL_CBMETHOD_DRP", exclRowCnt, strSheetName);
					CBM_Objs.verifyDRP_SaveCancel(exclRowCnt,strSheetName,"CBMethod");
				}//End of IF condition to verify if DRP Tab is displayed.

				
			
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_PopUps><Method: SecLevel_CBMethod >, Please check..!!");
			}//End of Catch block
		}//End of <Method: SecLevel_CBMethod>		
		
				
//###################################################################################################################################################################  
//Function name		: SecLevel_FIElection(String strPopUpName,String strAcctSec_Level,int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName)
//Class name		: SearchRes_PopUps
//Description 		: Function to verify Account Level FI Election pop up
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public void SecLevel_FIElection(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName){
			try{
				if(!this.searchRes_ClickAction(1, "FI-Elections", "rightaction", "Open/Closed")){						
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint: Click Action->FI-Elections failed.", "", "SearchRes_ClickFI-Elections",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of IF condition to click Action menu.
			
				if(!SearchRes_Action.verifyPopUpExists("SECLEVEL_FIELECTION")){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_OpenClosed->FIElection pop up not displayed", "", "SearchRes_ClickFIElection",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] Search_OpenClosed->FIElection pop up not displayed");
				}//End of IF condition to check if the CB Method pop up Exists
			
				//Check for error messages in Pop up:
				if(!SearchRes_Action.verifyPopUpError("FIElection", exclRowCnt, strSheetName)){
					return;
				}
				
				//function call to verify checkpoints:	
				FI_Objs.verifySec_FI_DropDowns(exclRowCnt, strSheetName);
				SearchRes_Action.PopUp_EditHist("FIElection", "SECLEVEL_FIELECTION", exclRowCnt, strSheetName);				
				SearchRes_Action.verifyPopUp_SaveCancel(exclRowCnt,strSheetName,"FIElection");
				
			
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_PopUps><Method: SecLevel_FIElection >, Please check..!!");
			}//End of Catch block
		}//End of <Method: SecLevel_FIElection>	
		
//###################################################################################################################################################################  
//Function name		: RawTrades_Details(int exclRowCnt,String strSheetName)
//Class name		: SearchRes_PopUps
//Description 		: Function to verify Details pop up from Raw Trades page
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public void RawTrades_Details(int exclRowCnt,String strSheetName){
			try{
				//SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
				//objSearchRes = new SearchPage_Results();
				
				//Add logic to navigate thru all rows until desired event type is found:
				String strRowNum = objSearchRes.ele_RecordsNum.getText();
				int strStartRowNumberTag = strRowNum.indexOf("-");
				int strEndRowNumberTag = strRowNum.indexOf("of");
				int strEndRowNum = Integer.parseInt((strRowNum.substring((strStartRowNumberTag+1), (strEndRowNumberTag))).trim());
				
				for(int intAppRowNav = 0;intAppRowNav < strEndRowNum;intAppRowNav++){
					String strAppEvent = objSearchRes.searchRes_getCellData(intAppRowNav,"Event");
					if(strAppEvent.equalsIgnoreCase("Deliver")||strAppEvent.equalsIgnoreCase("Sell")||strAppEvent.equalsIgnoreCase("Cover Short")){
						this.Navigate_Details(exclRowCnt, strSheetName, intAppRowNav, "Details", "rightaction", "Raw Trades", strAppEvent);
						return;
					}//End of IF condition to check event type
					if((intAppRowNav==strEndRowNum-1) && (strAppEvent.equalsIgnoreCase("Deliver")||strAppEvent.equalsIgnoreCase("Sell")||strAppEvent.equalsIgnoreCase("Cover Short"))){
						System.out.println("Checkpoint :[Failed] Raw Trades->verify [Details] pop-up : No row whose Event = {Sell;Deliver;Cover Short}");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Raw Trades->verify [Details] pop-up : No row whose Event = {Sell;Deliver;Cover Short}", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
				}//End of for loop to check for traverse thru each row till desired Event type is found			
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_PopUps><Method: RawTrades_Details >, Please check..!!");
			}//End of Catch block
		}//End of <Method: RawTrades_Details>		
//###################################################################################################################################################################  
//Function name		: Navigate_Details(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName,String strEvent)
//Class name		: SearchRes_PopUps
//Description 		: Function to verify Details pop up from Raw Trades page
//Parameters 		: Desired action menu item & which side of the action button to be clicked
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
		public void Navigate_Details(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName,String strEvent){
			try{
				if(strEvent.equalsIgnoreCase("Deliver")){
					if(!this.searchRes_ClickAction(appRowNum+1, "Details", "rightaction", "Raw Trades_TLE")){						
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Click Action->Details failed.", "", "SearchRes_ClickDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					}//End of IF condition to click Action menu.
				}
				else if(strEvent.equalsIgnoreCase("Sell")||strEvent.equalsIgnoreCase("Cover Short")){
					if(!this.searchRes_ClickAction(appRowNum+1, "Details", "rightaction", "Raw Trades")){						
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Click Action->Details failed.", "", "SearchRes_ClickDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					}//End of IF condition to click Action menu.
				}

				if(!SearchRes_Action.verifyPopUpExists("RAWTRADES_DETAILS")){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_RawTrades->Details pop up not displayed", "", "SearchRes_ClickDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] Search_RawTrades->Details pop up not displayed");
		        	if(CBM_Objs.btn_CBMXClose.size()>0){
		        		CBM_Objs.btn_CBMXClose.get(0).click();
		        	}
					return;
				}//End of IF condition to check if the Details pop up Exists
							
				CBM_Objs.verifyDetails_Checkpoints(exclRowCnt, strSheetName);
				
				//VSP Advaced Pop up verification:
				if(Navigate_VSPAdvanced(exclRowCnt, strSheetName)){
					//VSP Pop up verification:
					Navigate_VSP(exclRowCnt, strSheetName);
				};
				

	        	if(CBM_Objs.btn_CBMXClose.size()>0){
	        		CBM_Objs.btn_CBMXClose.get(0).click();
	        	}

			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Class: SearchRes_PopUps><Method: Navigate_Details >, Please check..!!");
			}//End of Catch block
		}//End of <Method: Navigate_Details>		
		
		
	//###################################################################################################################################################################  
	//Function name		: RawTrades_TLE_PopUp(int exclRowCnt,String strSheetName)
	//Class name		: SearchRes_PopUps
	//Description 		: Function to verify Details pop up from Raw Trades page
	//Parameters 		: Desired action menu item & which side of the action button to be clicked
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
			public void RawTrades_TLE_PopUp(int exclRowCnt,String strSheetName,String strClient){
				//SearchPage_Results objSearchRes = new SearchPage_Results();
				try{
					//Add logic to navigate thru all rows until desired event type is found:
					String strRowNum = objSearchRes.ele_RecordsNum.getText();
					int strStartRowNumberTag = strRowNum.indexOf("-");
					int strEndRowNumberTag = strRowNum.indexOf("of");
					int strEndRowNum = Integer.parseInt((strRowNum.substring((strStartRowNumberTag+1), (strEndRowNumberTag))).trim());
					
					for(int intAppRowNav = 0;intAppRowNav < strEndRowNum;intAppRowNav++){
						String strAppEvent = objSearchRes.searchRes_getCellData(intAppRowNav,"Event");
						if(strAppEvent.equalsIgnoreCase("Deliver")||strAppEvent.equalsIgnoreCase("Receive")||strAppEvent.equalsIgnoreCase("Transfers In")){
							this.Navigate_TLE(exclRowCnt, strSheetName, intAppRowNav+1, "Tax Lot Edit", "leftaction", "Raw Trades", strAppEvent,strClient);
							return;
						}//End of IF condition to check event type
						if((intAppRowNav==strEndRowNum-1) && !(strAppEvent.equalsIgnoreCase("Deliver")||strAppEvent.equalsIgnoreCase("Receive")||strAppEvent.equalsIgnoreCase("Transfers In"))){
							System.out.println("Checkpoint :[Failed] Raw Trades->verify [TLE] pop-up : No row whose Event = {Deliver;Receive;Transfers In}");
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Raw Trades->verify [TLE] pop-up : No row whose Event = {Deliver;Receive;Transfers In}", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return;
						}
					}//End of for loop to check for traverse thru each row till desired Event type is found			
				}//End of try block
				catch(Exception e){
					System.out.println("Exception in <Class: SearchRes_PopUps><Method: RawTrades_TLE_PopUp >, Please check..!!");
				}//End of Catch block
			}//End of <Method: RawTrades_TLE_PopUp>		
			
	//###################################################################################################################################################################  
	//Function name		: Navigate_TLE(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName,String strEvent)
	//Class name		: SearchRes_PopUps
	//Description 		: Function to verify Details pop up from Raw Trades page
	//Parameters 		: Desired action menu item & which side of the action button to be clicked
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
			public void Navigate_TLE(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName,String strEvent,String strClient){
				try{
					if(strSearchPageName.equalsIgnoreCase("Raw Trades")){
						if(!this.searchRes_ClickAction(appRowNum, strDesiredPageLink, strWhichActionButton, "Raw Trades_TLE")){						
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Click Action->TaxLot Edit failed.", "", "SearchRes_ClickTLE",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						}//End of IF condition to click Action menu.
					}
					else if(strSearchPageName.equalsIgnoreCase("No Cost Basis")) {
						if(strClient.equalsIgnoreCase("USA")||strClient.equalsIgnoreCase("UMF")) strDesiredPageLink = "Cost Basis Entry";
						if(!NCB_Objs.NCBRes_ClickAction(strDesiredPageLink)){						
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB Results Click Action->TaxLot Edit failed.", "", "NCBRes_ClickTLE",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						}//End of IF condition to click Action menu.
					}
					else if(strSearchPageName.equalsIgnoreCase("Avg Cost Gift")) {
						if(!AvgGift_Objs.AvgGiftRes_ClickAction(strDesiredPageLink)){						
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Avg Cost Gift Results Click Action->TaxLot Edit failed.", "", "AvgGiftRes_ClickTLE",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						}//End of IF condition to click Action menu.
					}
					Thread.sleep(5000);
					if(!SearchRes_Action.verifyPopUpExists("RAWTRADES_TLE")){
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE pop up not displayed", "", "TLE_popUp",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						System.out.println("Checkpoint :[Failed] TaxLot Edit pop up not displayed");
			        	if(CBM_Objs.btn_CBMXClose.size()>0){
			        		CBM_Objs.btn_CBMXClose.get(0).click();
			        	}
						return;
					}//End of IF condition to check if the Details pop up Exists
								
					if(strSearchPageName.equalsIgnoreCase("Raw Trades")){
						TLE_Objs.RawTrades_TLECheckpoints(exclRowCnt, strSheetName);
					}
					else{
						TLE_Objs.Generic_TLECheckpoints(exclRowCnt, strSheetName,strSearchPageName);
					}
						
					if(CBM_Objs.btn_CBMXClose.size()>0){
		        		CBM_Objs.btn_CBMXClose.get(0).click();
		        	}

				}//End of try block
				catch(Exception e){
					System.out.println("Exception in <Class: SearchRes_PopUps><Method: Navigate_TLE >, Please check..!!");
				}//End of Catch block
			}//End of <Method: Navigate_TLE>
		
	//###################################################################################################################################################################  
	//Function name		: Navigate_VSP(int exclRowCnt,String strSheetName)
	//Class name		: SearchRes_PopUps
	//Description 		: Function to verify Details->VSP pop up from Raw Trades page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
			public void Navigate_VSP(int exclRowCnt,String strSheetName){
				try{	
					//Navigate to VSP pop up
					ArrayList<String> arrAct_Values = new ArrayList<String>();
					if(CBM_Objs.ele_Det_SellMethod.size()>0) 	CBM_Objs.ele_Det_SellMethod.get(0).click();
					arrAct_Values = SearchRes_Action.getAct_DropDown_Values("DETAILS_CBM","SELLMETHOD",exclRowCnt,strSheetName);
					if(arrAct_Values.contains("VsPurchase")){
				    	//Logic to read the ComboList:
				    	List<WebElement> comboItems = SearchRes_Action.ele_PopUpCombo;
				        for (int i = 0; i < comboItems.size(); i++) {
				        	if(comboItems.get(i).getText().equalsIgnoreCase("VsPurchase")){
				        		comboItems.get(i).click();
				        		//Thread.sleep(10000);
				        		break;
				        	}
				        }//End of For Loop to read dropdown values

				        
				      //Dynamic wait for 1.5 minute for VSP pop up to display.
				        LocalDateTime strStartWaitTime,strEndWaitTime;
						strStartWaitTime = LocalDateTime.now();
						try{
							System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
							FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
													wait.withTimeout(90,TimeUnit.SECONDS)
													.pollingEvery(2, TimeUnit.SECONDS)				 
													.ignoring(NoSuchElementException.class)
													.until(ExpectedConditions.visibilityOf(SearchRes_Action.ele_PopUpWinTitle));
						}//End of try block for FluentWait
						
						catch(org.openqa.selenium.TimeoutException e){
							strEndWaitTime = LocalDateTime.now();
							System.out.println("Dynamic Wait time Ends with a Max threshold wait of 90seconds @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
							
							//Check if page is loading:			
							if(CommonUtils.isElementPresent(CBM_Objs.ele_Loading)){
								System.out.println("Checkpoint : VSP pop up - Stil Loading..[Failed]");
								reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] VSP pop up :Failed,still Loading[Waited for Max threshold of 90Seconds]", "", "VSP_Loading",  driver, "HORIZONTALLY", strSheetName, null);
								return ;
							}//End of IF condition to check for loading object.

							//Checkpoint: Check for Errors:
							if(CommonUtils.isElementPresent(CBM_Objs.ele_VSPError)){
								String strErMsg = CBM_Objs.ele_VSPError.getAttribute("textContent");
								System.out.println("Checkpoint : Error in VSP Pop Up, Error Message is - "+strErMsg);
								reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] VSP Pop Up_ErrorMessage:"+strErMsg, "", "VSP_Error",  driver, "HORIZONTALLY", strSheetName, null);
								return ;
							}//End of if condition to to see if there are errors.				
						}//End of Catch block for FluentWait
						catch(Exception e){
							strEndWaitTime = LocalDateTime.now();
							System.out.println("Dynamic Wait time Ends with a Max threshold wait of 90Seconds @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
							return ;
						}//End of catch block with generic exception for Fluent Wait 
				        
			        	
			        	//verify VSP pop up exists:
						if(!SearchRes_Action.verifyPopUpExists("RAWTRADES_VSP")){
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_RawTrades->VsPurchase pop up not displayed,so skipping 'VsPurchase' pop up verification.", "", "SearchRes_ClickVSP",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							System.out.println("Checkpoint :[Failed] Search_RawTrades->VsPurchase pop up not displayed");
				        	if(CBM_Objs.btn_CBMXClose.size()>0){
				        		CBM_Objs.btn_CBMXClose.get(0).click();
				        	}
							return;
						}//End of IF condition to check if the VSP pop up Exists
						
						//VSP Checkpoints
						CBM_Objs.verifyVSP_Checkpoints(exclRowCnt, strSheetName);
					}//End of IF condition to check if Actual dropdown contains "VsPurchase".
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_RawTrades->Details->'VsPurchase' is not displayed in the SellMethod dropdown, so skipping 'VsPurchase' pop up verification", "", "SearchRes_ClickVSP",  driver, "HORIZONTALLY", strSheetName, null);
					}//End of else condition to check if dropdown have 'VsPurchase'
					
				}//End of try block
				catch(Exception e){
					System.out.println("Exception in <Class: SearchRes_PopUps><Method: Navigate_VSP >, Please check..!!");
				}//End of Catch block
			}//End of Method: Navigate_VSP>
		
	//###################################################################################################################################################################  
	//Function name		: Navigate_VSPAdvanced(int exclRowCnt,String strSheetName,int appRowNum,String strDesiredPageLink, String strWhichActionButton,String strSearchPageName,String strEvent)
	//Class name		: SearchRes_PopUps
	//Description 		: Function to verify Details->VSP Advanced Search pop up from Raw Trades page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
			public boolean Navigate_VSPAdvanced(int exclRowCnt,String strSheetName){
				try{	
					//Navigate to VSP pop up
					ArrayList<String> arrAct_Values = new ArrayList<String>();
					if(CBM_Objs.ele_Det_SellMethod.size()>0) 	CBM_Objs.ele_Det_SellMethod.get(0).click();
					
					arrAct_Values = SearchRes_Action.getAct_DropDown_Values("DETAILS_CBM","SELLMETHOD",exclRowCnt,strSheetName);
					//CBM_Objs.ele_Det_SellMethod.get(0).click();
					if(arrAct_Values.contains("VsPurchase - Advanced Search")){
				    	//Logic to read the ComboList:
				    	List<WebElement> comboItems = SearchRes_Action.ele_PopUpCombo;
				        for (int i = 0; i < comboItems.size(); i++) {
				        	if(comboItems.get(i).getText().equalsIgnoreCase("VsPurchase - Advanced Search")){
				        		comboItems.get(i).click();
				        		break;
				        	}
				        }//End of For Loop to read dropdown values

			        	//Delay wait for 1 minute:
			        	
			        	//verify VSP pop up exists:
						if(!SearchRes_Action.verifyPopUpExists("RAWTRADES_VSP")){
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_RawTrades->VSP_AdvacedSearch pop up not displayed,so skipping 'VsPurchase/VSP_AdvacedSearch' pop up verification.", "", "SearchRes_ClickVSP",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							System.out.println("Checkpoint :[Failed] Search_RawTrades->VSP_AdvacedSearch pop up not displayed");
				        	if(CBM_Objs.btn_CBMXClose.size()>0){
				        		CBM_Objs.btn_CBMXClose.get(0).click();
				        	}
							return false;
						}//End of IF condition to check if the VSP_AdvacedSearch pop up Exists
						
						//VSP_AdvacedSearch Checkpoints
						if(!(CBM_Objs.verifyVSPAdvanced_Checkpoints(exclRowCnt, strSheetName))) return false;
						return true;
					}//End of IF condition to check if Actual dropdown contains "VsPurchase".
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search_RawTrades->Details->'VSP_AdvacedSearch' is not displayed in the SellMethod dropdown, so skipping 'VSP_AdvacedSearch' pop up verification", "", "SearchRes_ClickVSP",  driver, "HORIZONTALLY", strSheetName, null);
			        	if(CBM_Objs.btn_CBMXClose.size()>0){
		        		CBM_Objs.btn_CBMXClose.get(0).click();
		        		return false;
		        	}
						//return false;
					}//End of else condition to check if dropdown have 'VsPurchase'
					return true;
				}//End of try block
				catch(Exception e){
					System.out.println("Exception in <Class: SearchRes_PopUps><Method: Navigate_VSPAdvanced >, Please check..!!");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->Details->Current Sell Method dropdown is not displayed, so skipping 'VSP_AdvacedSearch/VSP' pop up verification", "", "SearchRes_ClickVSP",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of Catch block
			}//End of Method: Navigate_VSPAdvanced>			
//###################################################################################################################################################################		
}//End of SearchRes_PopUps
//###################################################################################################################################################################
