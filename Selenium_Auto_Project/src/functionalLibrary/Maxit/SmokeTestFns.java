package functionalLibrary.Maxit;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;

import PageObjects.Maxit.AvgGiftCost_Objs;
import PageObjects.Maxit.CAManager_Objs;
import PageObjects.Maxit.CBManager_Objs;
import PageObjects.Maxit.DMI_Objs;
import PageObjects.Maxit.Dashboard_Objs;
import PageObjects.Maxit.Exports_Objs;
import PageObjects.Maxit.GlobalDM_Objs;
import PageObjects.Maxit.HistCorrections_Objs;
import PageObjects.Maxit.Imports_Objs;
import PageObjects.Maxit.NoCostBasis_Objs;
import PageObjects.Maxit.PositionRecon_Objs;
import PageObjects.Maxit.RBIUpload_Objs;
import PageObjects.Maxit.RollBackRerun_Objs;
import PageObjects.Maxit.SearchPage_Results;
import PageObjects.Maxit.SearchRes_CBMethod_Objs;
import PageObjects.Maxit.SearchRes_FIElection_Objs;
import PageObjects.Maxit.SearchRes_MTM_Objs;
import PageObjects.Maxit.StepUp_Objs;
import PageObjects.Maxit.TxnError_Objs;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.GlobalObjectsFactory;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class SmokeTestFns {

	ITestContext context = GlobalObjectsFactory.getGlobalObjects().getiTestContext();	
	Reporting reporter = new Reporting();
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	CommonUtils util = new CommonUtils();

	//SearchPage_Results objSearchRes;
	SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
	SearchPageFns objSearchPageFns = PageFactory.initElements(driver, SearchPageFns.class);
	SearchRes_PopUpFns objRes_Action = PageFactory.initElements(driver, SearchRes_PopUpFns.class);

	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	SearchRes_FIElection_Objs FI_Objs = PageFactory.initElements(driver, SearchRes_FIElection_Objs.class);
	SearchRes_MTM_Objs MTM_Objs = PageFactory.initElements(driver, SearchRes_MTM_Objs.class);

	Dashboard_Objs Dash_Objs = PageFactory.initElements(driver, Dashboard_Objs.class);
	DMI_Objs DMI_Objs = PageFactory.initElements(driver, DMI_Objs.class);

	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	HistCorrections_Objs HistCorrec_Objs = PageFactory.initElements(driver, HistCorrections_Objs.class);
	StepUp_Objs stepup_Objs = PageFactory.initElements(driver, StepUp_Objs.class);
	Imports_Objs imports_Objs = PageFactory.initElements(driver, Imports_Objs.class);
	Exports_Objs exports_Objs = PageFactory.initElements(driver, Exports_Objs.class);
	AvgGiftCost_Objs AvgGift_Objs = PageFactory.initElements(driver, AvgGiftCost_Objs.class);
	RollBackRerun_Objs RollbackRerun_Objs = PageFactory.initElements(driver, RollBackRerun_Objs.class);
	RBIUpload_Objs RBIupload_Objs = PageFactory.initElements(driver, RBIUpload_Objs.class);

	//###################################################################################################################################################################  
	//Function name		: Search_Account_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_Account page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_Account_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strClient ) throws Exception{
		String strSheetName = "SmokeData";
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			String strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
			System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");

			if(objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName)){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned

				//Checkpoints for Account level CB Method pop up
				objRes_Action.AcctLevel_CBMethod(exclRowCnt, strSheetName, 1, "CB Method", "leftaction", "Account",strClient);
				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}

				//Checkpoints for Account level FI Election pop up
				objRes_Action.AcctLevel_FIElection(exclRowCnt, strSheetName, 1, "FI-Elections", "rightaction", "Account");
				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}

				//Checkpoints for Account level 'Mark-To-Market' pop up
				objRes_Action.AcctLevel_MTM(exclRowCnt, strSheetName, 1, "FI-Elections", "rightaction", "Account");
				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}

			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_Account_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Method:Search_Account_Flow ", "", "Search_Account_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_Account_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_Account_Flow>

	//###################################################################################################################################################################  
	//Function name		: Search_OpenClosed_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_OpenClosed_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_OpenClosed_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strClient ) throws Exception{
		String strSheetName = "SmokeData";
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			String strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
			System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");

			if(objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName)){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned

				//Checkpoints for Security level CB Method pop up
				objRes_Action.SecLevel_CBMethod(exclRowCnt, strSheetName, 1, "CB Method", "leftaction", "Open/Closed",strClient);
				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}

				//Checkpoints for Sec level FI Election pop up
				objRes_Action.SecLevel_FIElection(exclRowCnt, strSheetName, 1, "FI-Elections", "rightaction", "Open/Closed");
				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}

			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_OpenClosed_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_OpenClosed_Flow ", "", "Search_OpenClosed_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_OpenClosed_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_OpenClosed_Flow>	

	//###################################################################################################################################################################  
	//Function name		: Search_AuditTrail_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_AuditTrail_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_AuditTrail_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			if(strBlankSearch.equalsIgnoreCase("Y")) {
				strSearchValues = "["+strViewPage+";Blank Search]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input_Blank(strViewPage , exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			else{
				strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y

			if(strRet){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				else{
					System.out.println("Search Results for "+strSearchValues+" is >0");
					reporter.reportStep(exclRowCnt, "Passed", "Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;	
				}


			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_AuditTrail_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_AuditTrail_Flow ", "", "Search_AuditTrail_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_AuditTrail_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_AuditTrail_Flow>	

	//###################################################################################################################################################################  
	//Function name		: Search_TrxError_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_AuditTrail_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_TrxError_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			if(strBlankSearch.equalsIgnoreCase("Y")) {
				strSearchValues = "["+strViewPage+";Blank Search]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input_Blank(strViewPage , exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			else{
				strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y

			if(strRet){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				else{
					System.out.println("Search Results for "+strSearchValues+" is >0");
					reporter.reportStep(exclRowCnt, "Passed", "Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;	
				}


			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_TrxError_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_TrxError_Flow ", "", "Search_TxnError_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_TrxError_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_TrxError_Flow>	

	//###################################################################################################################################################################  
	//Function name		: Search_Realized_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_RealizedGL_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public  void Search_Realized_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
			System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
			strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);

			if(strRet){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				else{
					System.out.println("Search Results for "+strSearchValues+" is >0");
					reporter.reportStep(exclRowCnt, "Passed", "Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;	
				}


			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.err.println("Exception in Smoke Test Flow <Method: Search_Realized_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_Realized_Flow ", "", "Search_Realized_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_Realized_Flow>	

	//###################################################################################################################################################################  
	//Function name		: Search_Unrealized_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_UnrealizedGL_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public  void Search_Unrealized_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
			System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
			strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);

			if(strRet){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				else{
					System.out.println("Search Results for "+strSearchValues+" is >0");
					reporter.reportStep(exclRowCnt, "Passed", "Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;	
				}


			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.err.println("Exception in Smoke Test Flow <Method: Search_Unrealized_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_Unrealized_Flow ", "", "Search_Unrealized_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_Unrealized_Flow>	

	//###################################################################################################################################################################  
	//Function name		: Search_Ledger_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_Ledger_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_Ledger_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			if(strBlankSearch.equalsIgnoreCase("Y")) {
				strSearchValues = "["+strViewPage+";Blank Search]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input_Blank(strViewPage , exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			else{
				strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y

			if(strRet){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				else{
					System.out.println("Search Results for "+strSearchValues+" is >0");
					reporter.reportStep(exclRowCnt, "Passed", "Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
					return;	
				}


			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_Ledger_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_Ledger_Flow ", "", "Search_Ledger_Flow",  driver, "HORIZONTALLY", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_Ledger_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_Ledger_Flow>	


	//###################################################################################################################################################################  
	//Function name		: Dashboard_Flow(int exclRowCnt,String strViewPage,String To_Execute) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Dashboard Page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Dashboard_Flow(int exclRowCnt) throws Exception{
		String strSheetName = "SmokeData";
		boolean strRet;
		try{		
			strRet = Dash_Objs.Navigate_DashboardPage(exclRowCnt, strSheetName);
			if(strRet){
				//Verify Dashboard checkpoints	
				Dash_Objs.Dashboard_Checkpoints(exclRowCnt, strSheetName);
			}//End of IF condition
			else{
				System.out.println("Dashboard page Navigation failed, please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Dashboard page Navigation failed, please check..!!", "", "Dashboard_Navigate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}//End of Else condition

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
			return;
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Dashboard_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Dashboard_Flow ", "", "Dashboard_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
	}//End of >Method: Dashboard_Flow>	

	//###################################################################################################################################################################  
	//Function name		: DMI_Flow(int exclRowCnt,String strViewPage,String To_Execute) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying DMI Page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void DMI_Flow(int exclRowCnt,String strAccount) throws Exception{
		String strSheetName = "SmokeData";
		ArrayList<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
		boolean strRet;
		String strSearchValues = "[Account:"+strAccount+"]";
		try{		
			System.out.println("\n#########################[DMI]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
			strRet = DMI_Objs.VerifyandClick_DMILink(exclRowCnt, strSheetName);					
			if(strRet){
				if(DMI_Objs.VerifyandClick_Transaction(exclRowCnt,strSheetName)){
					//DMI_Objs.Dashboard_Checkpoints(exclRowCnt, strSheetName);
					DMI_Objs.DMITxn_search(exclRowCnt,strSheetName,strAccount);
				}

				String strWindowTitle = driver.getTitle();
				if(strWindowTitle.toLowerCase().contains("dmi") || strWindowTitle.toLowerCase().contains("data management interface")|| strWindowTitle.toLowerCase().contains("maxit: broker dashboard")){
					driver.close();
				}
				driver.switchTo().window(browserTabs.get(0));

			}//End of IF condition
			else{
				System.out.println("DMI page Navigation failed, please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "DMI page Navigation failed, please check..!!", "", "DMI_Navigate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}//End of Else condition

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
			return;
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: DMI_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in DMI_Flow ", "", "DMI_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
	}//End of >Method: DMI_Flow>		

	//###################################################################################################################################################################  
	//Function name		: Search_HistCorrec_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_AuditTrail_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_HistCorrec_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		String strTabName = "HIST_CORRECTION";
		boolean strRet;
		try{
			boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
			if(strPageExistsCheck){								
				if(strBlankSearch.equalsIgnoreCase("Y")){
					strSearchValues = "["+strTabName+";Blank Search]";
					System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
					strRet = HistCorrec_Objs.HistCorrec_BlankSearch(strTabName,exclRowCnt,strSheetName);
				}//End of IF to check blank search
				else{
					strSearchValues = "["+strTabName+";Account:"+strAcct+"]";
					System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
					//AUTO-422: Added a checkpoint- If historical correction page has no account provided in data sheet, please modify script to only validate page exist and pass 
					if(strAcct.toString().isEmpty() || strAcct.toString()=="" || strAcct==null){
						System.out.println("As Account number is NOT provided in data sheet, we are just verifying for 'Historic Correction' tab existence.");
						System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
						return;
					}
					strRet = HistCorrec_Objs.HistCorrec_Search(strTabName,strAcct,strSecType,strSecValue,strFromDate,strToDate,exclRowCnt,strSheetName);
				}//End of ELSE to check blank search
				if(strRet){
					HistCorrec_Objs.HistCorrec_CheckPoints(strTabName,strAcct,exclRowCnt,strSheetName);
				}
			}//End of IF to check if Page Exists for this client
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_HistCorrec_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_HistCorrec_Flow ", "", "Search_HistCorrec_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_HistCorrec_Flow>,please check..!!");
			return;
		}//End of Catch block
	}//End of >Method: Search_HistCorrec_Flow>			

	//###################################################################################################################################################################  
	//Function name		: SetpUp_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_AuditTrail_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void StepUp_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strDoD,String strPageExists ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		String strTabName = "STEPUP";
		boolean strRet;
		try{
			boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
			if(strPageExistsCheck){								
				strSearchValues = "["+strTabName+";Account:"+strAcct+";SecVal:"+strSecValue+";DoD:"+strDoD+"]";
				System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = stepup_Objs.StepUp_Search(strTabName, strAcct, strSecType, strSecValue, strDoD, exclRowCnt, strSheetName);
				if(strRet){
					stepup_Objs.StepUp_CheckPoints(strTabName, strAcct, strSecValue, strDoD, exclRowCnt, strSheetName);						
				}
			}//End of IF to check if Page Exists for this client
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: StepUp_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in SetpUp_Flow ", "", "StepUp_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: StepUp_Flow>,please check..!!");
			return;
		}//End of Catch block
	}//End of >Method: SetpUp_Flow>				

	//###################################################################################################################################################################  
	//Function name		: Search_RawTrades_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_Account page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_RawTrades_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strClient ) throws Exception{
		String strSheetName = "SmokeData";
		try{		

			String strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
			System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");

			if(objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName)){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned

				//RawTrades_Details Verification checkpoints:
				objRes_Action.RawTrades_Details(exclRowCnt, strSheetName);

				//RawTrades_TaxLotEdit verification checkpoints:
				objRes_Action.RawTrades_TLE_PopUp(exclRowCnt, strSheetName,strClient);

				//To ensure the pop up is closed.
				if(CBM_Objs.btn_CBMXClose.size()>0){
					CBM_Objs.btn_CBMXClose.get(0).click();
				}

			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_RawTrades_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_RawTrades_Flow ", "", "Search_RawTrades_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_RawTrades_Flow>		


	//###################################################################################################################################################################  
	//Function name		: NoCostBasis_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying No Cost Basis page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void NoCostBasis_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strClient ) throws Exception{
		NoCostBasis_Objs NCB_Objs = PageFactory.initElements(driver, NoCostBasis_Objs.class);
		String strSheetName = "SmokeData";
		String strSearchValues;
		try{		
			//Navigate to NCB page
			if(!(NCB_Objs.Navigate_NCB(exclRowCnt, strSheetName))){
				System.out.println("Navigation to NCB Failed, please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Navigation to 'No Cost Basis' page failed, please check..!!", "", "NCBPage",  driver, "HORIZONTALLY", strSheetName, null);
				return;
			}
			//Search flows based on blank search/data search in NCB page
			if(strBlankSearch.equalsIgnoreCase("Y")) {
				strSearchValues = "[NCB;Blank Search]";
				System.out.println("\n#########################[NCB]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				NCB_Objs.NCB_BlankSearch_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch,strClient);
			}//End of IF condition to check BlankSearch=Y
			else{
				strSearchValues = "[NCB;"+strAcct+";"+strSecType+";"+strSecValue+"]";
				System.out.println("\n#########################[NCB]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				NCB_Objs.NCB_DataSearch_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch,strClient);
			}//End of IF condition to check BlankSearch=Y

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: NoCostBasis_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in NoCostBasis_Flow ", "", "NoCostBasis_Flow",  driver, "HORIZONTALLY", strSheetName, null);
			return;
		}//End of Catch block
	}//End of >Method: NoCostBasis_Flow>			

	//###################################################################################################################################################################  
	//Function name		: Search_SecXref_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_SecXref_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void Search_SecXref_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			objSearchPageFns = new SearchPageFns(); 
			objRes_Action = new SearchRes_PopUpFns();

			if(strBlankSearch.equalsIgnoreCase("Y")) {
				strSearchValues = "["+strViewPage+";Blank Search]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input_Blank(strViewPage , exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			else{
				strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y

			if(strRet){
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
				if(!(appRowsReturned>0)){
					if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "No Resuls found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				else{
					System.out.println("Search Results for "+strSearchValues+" is >0");
					reporter.reportStep(exclRowCnt, "Passed", "Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;	
				}


			}//End of IF condition to check Search_Input Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_SecXref_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_SecXref_Flow ", "", "Search_SecXref_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_SecXref_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_SecXref_Flow>	


	//###################################################################################################################################################################  
	//Function name		: PositionRecon_Flow(String strViewPage, int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch)
	//Class name		: SmokeTestFns
	//Description 		: Function to perform search and verifying records in Position Recon view/page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar 
	//###################################################################################################################################################################			

	public  void PositionRecon_Flow(String strViewPage, int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch) throws Exception
	{
		System.out.println(" InsidePositionRecon_Flow : ");
		PositionRecon_Objs PosRecon_Objs = PageFactory.initElements(driver, PositionRecon_Objs.class);
		String strSheetName = "SmokeData";
		try{
			//Navigate to Position Recon Page
			if(!(PosRecon_Objs.Navigate_PosRecon(exclRowCnt, strSheetName))){
				System.out.println("Navigation to Position Recon page Failed, please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Navigation to 'Position Recon' page failed, please check..!!", "", "PositionReconPage",  driver, "HORIZONTALLY", strSheetName, null);
				return;
			}
			//Verifying the position Recon Summary widget in Position Recon page
			if(!(PosRecon_Objs.VerifyPositionRecon_SummaryWidget(exclRowCnt, strSheetName))){
				System.out.println("Position Recon Summary check Failed, please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "'Position Recon summary' failed, please check..!!", "", "PositionReconSummary",  driver, "HORIZONTALLY", strSheetName, null);
				return;
			}

			//Search flows based on position Recon Summary data in Position Recon page
			System.out.println("\n#########################[Position Recon page]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strBlankSearch+"#########################");
			PosRecon_Objs.PositionReconPage_SearchFlow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strSheetName);
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");

			//Search flows based on position Recon Summary data in Search_PosRecon view
			System.out.println("\n#########################[Pos Recon Search view]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strBlankSearch+"#########################");
			PosRecon_Objs.PosRecon_SearchFlow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch, strSheetName, PositionRecon_Objs.SEARCH_POSITION_RECON_VIEW);
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");

		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: PositionRecon_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in PositionRecon_Flow ", "", "PositionRecon_Flow",  driver, "HORIZONTALLY", strSheetName, null);
			return;
		}//End of Catch block
	}//End of >Method: PositionRecon_Flow>	

	//###################################################################################################################################################################  
	//Function name		: Search_Imports_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying Search_Imports_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public  void Search_Imports_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
		System.out.println("Inside Search_Imports_Flow : ");
		String strSheetName = "SmokeData";
		String strSearchValues;
		String strTabName = "IMPORTS";
		boolean strRet;
		try{
			boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
			if(strPageExistsCheck){								
				if(strBlankSearch.equalsIgnoreCase("Y")){
					strSearchValues = "["+strTabName+";Blank Search]";
					System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
					strRet = imports_Objs.Imports_BlankSearch(strTabName,exclRowCnt,strSheetName);
				}//End of IF to check blank search
				else{
					strSearchValues = "["+strTabName+";Account: "+strAcct+";SecValue: "+strSecValue+"+FromDate: "+strFromDate+"+ToDate: "+strToDate+"]";
					System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
					strRet = imports_Objs.Imports_Data_Search(strTabName,strAcct,strSecType,strSecValue,strFromDate,strToDate,exclRowCnt,strSheetName);
				}//End of ELSE to check blank search
				if(strRet){
					imports_Objs.Imports_CheckPoints(strTabName,strAcct,exclRowCnt,strSheetName);
				}
			}//End of IF to check if Page Exists for this client
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_Imports_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_Imports_Flow ", "", "Search_Imports_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_Imports_Flow>,please check..!!");
			return;
		}//End of Catch block
	}//End of >Method: Search_Imports_Flow>	

	//###################################################################################################################################################################  
		//Function name		: Search_Exports_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		//Class name		: SmokeTestFns
		//Description 		: Function to list the methods for verifying Search_Exports_Flow page
		//Parameters 		: 
		//Assumption		: None
		//Developer			: Dhanasekar Kumar
		//###################################################################################################################################################################			
		public  void Search_Exports_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
			System.out.println("Inside Search_Exports_Flow : " + strBlankSearch);
			String strSheetName = "SmokeData";
			String strSearchValues;
			String strTabName = "EXPORTS";
			boolean strRet;
			try{
				boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
				if(strPageExistsCheck){								
					if(strBlankSearch.equalsIgnoreCase("Y")){
						strSearchValues = "["+strTabName+";Blank Search]";
						System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
						strRet = exports_Objs.Exports_BlankSearch(strTabName,exclRowCnt,strSheetName);
					}//End of IF to check blank search
					else{
						strSearchValues = "["+strTabName+";Account: "+strAcct+";SecValue: "+strSecValue+"+FromDate: "+strFromDate+"+ToDate: "+strToDate+"]";
						System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
						strRet = exports_Objs.Exports_Data_Search(strTabName,strAcct,strSecType,strSecValue,strFromDate,strToDate,exclRowCnt,strSheetName);
					}//End of ELSE to check blank search
					if(strRet){
						exports_Objs.Exports_CheckPoints(exclRowCnt,strSheetName);
					}
				}//End of IF to check if Page Exists for this client
				System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Smoke Test Flow <Method: Search_Exports_Flow>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_Exports_Flow ", "", "Search_Exports_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				System.out.println("Exception in Smoke Test Flow <Method: Search_Exports_Flow>,please check..!!");
				return;
			}//End of Catch block
		}//End of >Method: Search_Exports_Flow>
	
		//###################################################################################################################################################################  
		//Function name		: Transaction_Error_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		//Class name		: SmokeTestFns
		//Description 		: Function to list the methods for verifying 'Transaction Error' page
		//Parameters 		: 
		//Assumption		: None
		//Developer			: Kavitha Golla
		//###################################################################################################################################################################			
			public  void Transaction_Error_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFromDate,String strToDate,String strBlankSearch ) throws Exception{
				TxnError_Objs TXN_Objs = PageFactory.initElements(driver, TxnError_Objs.class);
				String strSheetName = "SmokeData";
				String strSearchValues;
				try{		
					//Navigate to NCB page
					if(!(TXN_Objs.Navigate_TxnError(exclRowCnt, strSheetName))){
						System.out.println("Navigation to 'Transaction Error' Failed, please check..!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Navigation to 'Transaction Error' page failed, please check..!!", "", "TXNPage",  driver, "HORIZONTALLY", strSheetName, null);
						return;
					}
					//Search flows based on blank search/data search in NCB page
					if(strBlankSearch.equalsIgnoreCase("Y")) {
						strSearchValues = "[Transaction Error;Blank Search]";
						System.out.println("\n#########################[Transaction Error]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
						TXN_Objs.TxnError_BlankSearch_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strFromDate, strToDate, strBlankSearch);
					}//End of IF condition to check BlankSearch=Y
					else{
						strSearchValues = "[Transaction Error;Acct:"+strAcct+";"+strSecType+";"+strSecValue+";FromDate:"+strFromDate+";ToDate:"+strToDate+"]";
						System.out.println("\n#########################[Transaction Error]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
						TXN_Objs.TxnError_DataSearch_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strFromDate, strToDate, strBlankSearch);
					}//End of IF condition to check BlankSearch=Y

					System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
				}//End of try block
				catch(Exception e){
					System.out.println("Exception in Smoke Test Flow <Method: Transaction_Error_Flow>,please check..!!");
					reporter.reportStep(exclRowCnt, "Failed", "Exception in Transaction_Error_Flow ", "", "Transaction_Error_Flow",  driver, "HORIZONTALLY", strSheetName, null);
					return;
				}//End of Catch block
			}//End of >Method: Transaction_Error_Flow>		
		
		//###################################################################################################################################################################  
		//Function name		: AvgGiftCost_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		//Class name		: SmokeTestFns
		//Description 		: Function to list the methods for verifying 'Avg Gift Cost' page
		//Parameters 		: 
		//Assumption		: None
		//Developer			: Kavitha Golla
		//###################################################################################################################################################################			
		public  void AvgGiftCost_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate,String strClient ) throws Exception{
			String strSheetName = "SmokeData";
			String strSearchValues;
			String strTabName = "AVG_COST_GIFT";
			boolean strRet;
			try{
				boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
				if(strPageExistsCheck){								
					if(strBlankSearch.equalsIgnoreCase("Y")){
						strSearchValues = "["+strTabName+";Blank Search]";
						System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
						strRet = AvgGift_Objs.AvgGiftCost_BlankSearch(strTabName,exclRowCnt,strSheetName);
					}//End of IF to check blank search
					else{
						strSearchValues = "["+strTabName+";Account:"+strAcct+"]";
						System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
						strRet = AvgGift_Objs.AvgGiftCost_Search(strTabName, strAcct, strSecType, strSecValue, strFromDate, strToDate, exclRowCnt, strSheetName);
					}//End of ELSE to check blank search
					if(strRet){
						AvgGift_Objs.AvgGiftCost_CheckPoints(strTabName,strAcct,exclRowCnt,strSheetName,strClient);
					}
				}//End of IF to check if Page Exists for this client
				System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Smoke Test Flow <Method: AvgGiftCost_Flow>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in AvgGiftCost_Flow ", "", "AvgGiftCost_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				System.out.println("Exception in Smoke Test Flow <Method: AvgGiftCost_Flow>,please check..!!");
				return;
			}//End of Catch block
		}//End of >Method: AvgGiftCost_Flow>		
			
		//###################################################################################################################################################################  
		//Function name		: RollbackRerun_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		//Class name		: SmokeTestFns
		//Description 		: Function to list the methods for verifying Search_Exports_Flow page
		//Parameters 		: 
		//Assumption		: None
		//Developer			: Dhanasekar Kumar
		//###################################################################################################################################################################			
		public  void RollbackRerun_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
			String strSheetName = "SmokeData";
			String strTabName = "RRERUN";
			try{
				boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
				if(strPageExistsCheck){	
					//Check if there are any error messages:
					if(CommonUtils.isElementPresent(CBMngr_Objs.ele_CBM_Errors)){
						String strErMsg = CBMngr_Objs.ele_CBM_Errors.getAttribute("textContent");
						System.out.println("Checkpoint : Error in CB Manager->"+strTabName+", Error Message is - "+strErMsg);
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+"_ErrorMessage:"+strErMsg, "", "CBManager_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}//End of if condition to to see if there are errors.

					System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"]#########################");
					
					RollbackRerun_Objs.RollbackRerun_CheckPoints(strTabName, exclRowCnt,strSheetName);
				}//End of IF to check if Page Exists for this client
				System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Smoke Test Flow <Method: RollbackRerun_Flow>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in RollbackRerun_Flow ", "", "RollbackRerun_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of Catch block
		}//End of >Method: RollbackRerun_Flow>

		//###################################################################################################################################################################  
		//Function name		: BasisUpload_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		//Class name		: SmokeTestFns
		//Description 		: Function to list the methods for verifying Search_Exports_Flow page
		//Parameters 		: 
		//Assumption		: None
		//Developer			: Dhanasekar Kumar
		//###################################################################################################################################################################			
		public  void BasisUpload_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
			String strSheetName = "SmokeData";
			String strTabName = "BASIS_UPLOAD";
			try{
				boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
				if(strPageExistsCheck){	
					//Check if there are any error messages:
					if(CommonUtils.isElementPresent(CBMngr_Objs.ele_CBM_Errors)){
						String strErMsg = CBMngr_Objs.ele_CBM_Errors.getAttribute("textContent");
						System.out.println("Checkpoint : Error in CB Manager->"+strTabName+", Error Message is - "+strErMsg);
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+"_ErrorMessage:"+strErMsg, "", "CBManager_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}//End of if condition to to see if there are errors.
					System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"]#########################");
					
					RBIupload_Objs.RBIUpload_CheckPoints(strTabName, exclRowCnt,strSheetName);
				}//End of IF to check if Page Exists for this client
				System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Smoke Test Flow <Method: BasisUpload_Flow>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in BasisUpload_Flow ", "", "BasisUpload_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of Catch block
		}//End of >Method: BasisUpload_Flow>	
		
	//###################################################################################################################################################################  
	//Function name		: CB_Upload_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying CB_Upload_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void CB_Upload_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
		String strSheetName = "SmokeData";
		String strTabName = "CB_UPLOAD";
		try{
			boolean strPageExistsCheck = CBMngr_Objs.Navigate_CBManager(strTabName,exclRowCnt,strSheetName,strPageExists);
			if(strPageExistsCheck){	
				//Check if there are any error messages:
				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_CBM_Errors)){
					String strErMsg = CBMngr_Objs.ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in CB Manager->"+strTabName+", Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+"_ErrorMessage:"+strErMsg, "", "CBManager_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of if condition to to see if there are errors.
				System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"]#########################");
				
				RBIupload_Objs.RBIUpload_CheckPoints(strTabName, exclRowCnt,strSheetName);
			}//End of IF to check if Page Exists for this client
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: CB_Upload_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in CB_Upload_Flow ", "", "CB_Upload_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
	}//End of >Method: CB_Upload_Flow>		
	
	//###################################################################################################################################################################  
	//Function name		: CAManager_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying CAManager_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void CAManager_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate,String strCAID,String strAddNewCA ) throws Exception{
		CAManager_Objs CA_Objs = PageFactory.initElements(driver, CAManager_Objs.class);
		String strSheetName = "SmokeData";
		String strTabName = "CA_MANAGER";
		try{
			System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"]#########################");
			if(CA_Objs.Navigate_CAManager(exclRowCnt, strSheetName)){
				CA_Objs.VerifyPage_CAManager(exclRowCnt, strSheetName);
				CA_Objs.CAManager_SearchCheckpoints(exclRowCnt, strSheetName, "CAManager", strCAID, strSecValue,strBlankSearch, strAddNewCA);				
			}	
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: CAManager_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in CAManager_Flow ", "", "CAManager_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
	}//End of <Method: CAManager_Flow>	
	
	//###################################################################################################################################################################  
	//Function name		: GlobalDM_Download_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying GlobalDM_Download_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void GlobalDM_Download_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
		GlobalDM_Objs GDM_Objs = PageFactory.initElements(driver, GlobalDM_Objs.class);
		String strSheetName = "SmokeData";
		String strTabName = "DOWNLOAD";
		try{
			System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"]#########################");
			
				boolean strPageExistsCheck = GDM_Objs.Navigate_GlobalDM(strTabName,exclRowCnt,strSheetName,strPageExists);
				if(strPageExistsCheck){	
					GDM_Objs.GlobalDM_GenericCheckpoints(strTabName, exclRowCnt,strSheetName);
				}//End of IF to check if Page Exists for this client

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: GlobalDM_Download_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in GlobalDM_Download_Flow ", "", "GlobalDM_Download_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
	}//End of <Method: GlobalDM_Download_Flow>		
	
	//###################################################################################################################################################################  
	//Function name		: GlobalDM_FileManager_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
	//Class name		: SmokeTestFns
	//Description 		: Function to list the methods for verifying GlobalDM_FileManager_Flow page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
	public  void GlobalDM_FileManager_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strPageExists,String strFromDate,String strToDate ) throws Exception{
		GlobalDM_Objs GDM_Objs = PageFactory.initElements(driver, GlobalDM_Objs.class);
		String strSheetName = "SmokeData";
		String strTabName = "FILE_MANAGER";
		try{
			System.out.println("\n#########################["+strTabName+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"]#########################");
			
				boolean strPageExistsCheck = GDM_Objs.Navigate_GlobalDM(strTabName,exclRowCnt,strSheetName,strPageExists);
				if(strPageExistsCheck){	
					GDM_Objs.GlobalDM_GenericCheckpoints(strTabName, exclRowCnt,strSheetName);
				}//End of IF to check if Page Exists for this client

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: GlobalDM_FileManager_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in GlobalDM_FileManager_Flow ", "", "GlobalDM_FileManager_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
	}//End of <Method: GlobalDM_FileManager_Flow>			
	
}//End of <Class: SmokeTestFns>
