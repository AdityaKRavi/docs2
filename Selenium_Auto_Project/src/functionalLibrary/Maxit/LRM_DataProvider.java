package functionalLibrary.Maxit;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.ManageDriver_Maxit;

public class LRM_DataProvider {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	ConfigInputFile objConfigInputFile = new ConfigInputFile();
	DataTable dt = new DataTable();
		

	//###################################################################################################################################################################  
	//Function name		: Read_TxnLRM_Update(ITestContext context,Method mPageName)
	//Class name		: LRM_DataProvider
	//Description 		: 
	//Parameters 		: context: ITestContest to read testNG.xml parameters
	//					: mSheetName : Send calling Method (@Test method name) name as parameter
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
		@DataProvider(name="Read_TxnLRM_Update")
		public Object[][] dp_TxnLRM_Update(ITestContext context,Method mPageName)throws Exception{
			String strSheetName;
			System.out.println("Data Provider: Inside Method:dp_SmokeData for reading the data from Input file.");			
			String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			String strEnv = context.getCurrentXmlTest().getParameter("environment");
			String strClient = context.getCurrentXmlTest().getParameter("clientName");
			if(mPageName.getName().trim().equalsIgnoreCase("TxnLRM_VerifyUpdate")) strSheetName = "Txn_LRMUpdate";
			else strSheetName = mPageName.getName().trim();
			
			
		    String excelPath = dt.getInputFIlePath_Maxit(strScriptName,strEnv,strClient);
			return(this.get_TxnLRM_UpdateData(excelPath, strSheetName,strClient));
		  
		}//End of DataProvider "dp_TxnLRM_Update"	
		
	//###################################################################################################################################################################  
	//Function name		: Read_TxnLRM_Ledger(ITestContext context,Method mPageName)
	//Class name		: LRM_DataProvider
	//Description 		: 
	//Parameters 		: context: ITestContest to read testNG.xml parameters
	//					: mSheetName : Send calling Method (@Test method name) name as parameter
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
		@DataProvider(name="Read_LRM_DataValidation")
		public Object[][] dp_LRM_DataValidation(ITestContext context,Method mPageName)throws Exception{
			String strSheetName;
			System.out.println("Data Provider: Inside Method:dp_SmokeData for reading the data from Input file.");			
			String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			String strEnv = context.getCurrentXmlTest().getParameter("environment");
			String strClient = context.getCurrentXmlTest().getParameter("clientName");
			if(mPageName.getName().trim().equalsIgnoreCase("TxnLRM_VerifyUpdate")) strSheetName = "Txn_LRMUpdate";
			else strSheetName = mPageName.getName().trim();
			
			
		    String excelPath = dt.getInputFIlePath_Maxit(strScriptName,strEnv,strClient);
			return(this.get_LRM_DataValidation(excelPath, strSheetName,strClient));
		  
		}//End of DataProvider "dp_LRM_DataValidation"		
		

	//###################################################################################################################################################################  
	//Function name		: get_LRM_DataValidation(String strExcelPath,String strSheetName,String strClient)
	//Class name		: LRM_DataProvider
	//Description 		: Generic function to read data provider from Input data files for LRM data validation
	//Parameters 		: strExcelPath: Input excel file path
	//					: strSheetName: Name of the sheet
	//					: strViewPage : Search View page name
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public Object[][] get_LRM_DataValidation(String strExcelPath,String strSheetName,String strClient) throws Exception{
			Object[][] dpArray = null;
			String strViewPage;
			dt.setExcelFile(strExcelPath);
	        if(dt.setSheet(strSheetName)){			  		 
	        	int intDTRows = dt.getRowCount();
	        	dpArray = new Object[intDTRows][12];
	        	
	        	for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

	        		  String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
	        		  if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
	        			  return(dpArray);
	        		  }
	        		  
					  String strAcct = dt.getCellData(intDTRowIndex, "Account_No").toString() ;
					  String strSecType = dt.getCellData(intDTRowIndex, "Sec_Type").toString() ;
					  String strSecValue = dt.getCellData(intDTRowIndex, "Sec_Value").toString() ;
					  String strRowCount = dt.getCellData(intDTRowIndex, "Row_Count").toString() ;
					  String strColNames = dt.getCellData(intDTRowIndex, "Column_Names").toString() ;
					  String strUseCaseNum = dt.getCellData(intDTRowIndex, "UseCase").toString() ;
					  
					  switch (strSheetName){
					  case "Ledger":
						  strViewPage = "Ledger";
						  break;
					  case "Realized":
						  strViewPage = "Realized";
						  break;
					  case "Unrealized":
						  strViewPage = "Unrealized";
						  break;
					  case "OpenClosed":
						  strViewPage = "Open/Closed";
						  break;
					  case "RawTrades":
						  strViewPage = "Raw Trades";
						  break;
					  default:
						  strViewPage = "Ledger";	
						  break;
					  }//End of Switch case
					  
					  
					  dpArray[intDTRowIndex-1][0]=strViewPage;
					  dpArray[intDTRowIndex-1][1]=To_Execute;
					  dpArray[intDTRowIndex-1][2]=strAcct;
					  dpArray[intDTRowIndex-1][3]=strSecType;
					  dpArray[intDTRowIndex-1][4]=strSecValue;
					  dpArray[intDTRowIndex-1][5]=strRowCount;
					  dpArray[intDTRowIndex-1][6]=strColNames;
					  
					  //Reading Expected rows
					  int rowCnt = Integer.parseInt(strRowCount);
					  String[] expRows = new String[rowCnt];
					  if(rowCnt>0){
						  for(int readExpRow = 1;readExpRow<=rowCnt;readExpRow++){
							  expRows[readExpRow-1]= (dt.getCellData(intDTRowIndex, "Row"+readExpRow).toString());						  
						  }//End of for loop to read Expected rows from Input sheet
						  dpArray[intDTRowIndex-1][7]=expRows;
					  }//End of IF condition to check ExpectedRow count in data sheet>0
					  
					  else {
						  dpArray[intDTRowIndex-1][7] = null;
					  }//End of ELSE condition to check ExpectedRow count in data sheet>0				  				  
					  
					  dpArray[intDTRowIndex-1][8] = strUseCaseNum;
					  dpArray[intDTRowIndex-1][9]=intDTRowIndex;
					  dpArray[intDTRowIndex-1][10]=strSheetName;
					  dpArray[intDTRowIndex-1][11]=strClient;
				  }//End of for loop to iterate thru different rows	
		  }//End of IF condition to check  DataTable.setSheet

			return(dpArray);
		}//End of get_LRM_DataValidation Method		
		
		//###################################################################################################################################################################  
		//Function name		: get_TxnLRM_UpdateData(String strExcelPath, String strSheetName,strClient)
		//Class name		: LRM_DataProvider
		//Description 		: Generic function to read data from input file for Txn LRM update process . 
		//Parameters 		: strExcelPath: Input excel file path
		//					: strSheetName: Name of the sheet
		//					: strClient : Client name
		//Assumption		: None
		//Developer			: Kavitha Golla
		//###################################################################################################################################################################			
		public Object[][] get_TxnLRM_UpdateData(String strExcelPath, String strSheetName,String strClient) throws Exception{
			Object[][] dpArray = null;
			dt.setExcelFile(strExcelPath);
			if(dt.setSheet(strSheetName)){			  		 
				int intDTRows = dt.getRowCount();
				dpArray = new Object[intDTRows][14];

				for(int intDTRowIndex = 1 ; intDTRowIndex<=intDTRows;intDTRowIndex++){	  

					String To_Execute = dt.getCellData(intDTRowIndex, "To_Execute").toString() ;
					if(To_Execute=="" || To_Execute.isEmpty()||To_Execute==null){
						return(dpArray);
					}

					String strAcct = dt.getCellData(intDTRowIndex, "Account_No").toString() ;
					String strSecType = dt.getCellData(intDTRowIndex, "Sec_Type").toString() ;
					String strSecValue = dt.getCellData(intDTRowIndex, "Sec_Value").toString() ;
					String strLRMLevel = dt.getCellData(intDTRowIndex, "LRM_UpdLevel").toString() ;
					String strExisting_LRM = dt.getCellData(intDTRowIndex, "Existing_LRM").toString() ;
					String strNew_LRM = dt.getCellData(intDTRowIndex, "New_LRM").toString() ;
					String strTxn_AcctType = dt.getCellData(intDTRowIndex, "Txn_Acct_Type").toString() ;

					String strTxn_ClosingDate = dt.getCellData(intDTRowIndex, "Txn_ClosingTDate").toString() ;
					String strTestCase_Name = dt.getCellData(intDTRowIndex, "Comments").toString() ;
					
					
					String useCaseNum = dt.getCellData(intDTRowIndex, "UseCase").toString() ;

					dpArray[intDTRowIndex-1][0]=To_Execute;
					dpArray[intDTRowIndex-1][1]=strAcct;
					dpArray[intDTRowIndex-1][2]=strSecType;
					dpArray[intDTRowIndex-1][3]=strSecValue;
					dpArray[intDTRowIndex-1][4]=strLRMLevel;
					dpArray[intDTRowIndex-1][5]=strExisting_LRM;
					dpArray[intDTRowIndex-1][6]=strNew_LRM;
					dpArray[intDTRowIndex-1][7]=strTxn_AcctType;
					dpArray[intDTRowIndex-1][8]=strTxn_ClosingDate;
					dpArray[intDTRowIndex-1][9]=strTestCase_Name;
					dpArray[intDTRowIndex-1][10]=useCaseNum;
					
					dpArray[intDTRowIndex-1][11]=intDTRowIndex;
					dpArray[intDTRowIndex-1][12]=strSheetName; 
					dpArray[intDTRowIndex-1][13]=strClient;
					System.out.println("intDTRowIndex : " +intDTRowIndex + "To_Execute : " +To_Execute+"strAcct : " +strAcct+"strSecType : " +strSecType+
							"strSecValue : " +strSecValue+"LRM_UpdLevel : " +strLRMLevel+"Existing_LRM : " +strExisting_LRM+
							"New_LRM : " +strNew_LRM+"Txn_AccountType : " +strTxn_AcctType+
							"Comments : " +strTestCase_Name
							+"useCaseNum : " +useCaseNum
							+"intDTRowIndex : " +intDTRowIndex+"strSheetName : " +strSheetName);
				}//End of for loop to iterate thru different rows	
			}//End of IF condition to check  DataTable.setSheet

			return(dpArray);
		}//End of get_TxnLRM_UpdateData Method	
		//###################################################################################################################################################################
	

		
//###################################################################################################################################################################
}//End of <class: LRM_DataProvider>
