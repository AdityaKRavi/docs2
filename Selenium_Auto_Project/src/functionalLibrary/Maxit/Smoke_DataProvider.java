package functionalLibrary.Maxit;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import functionalLibrary.Global.DataTable;
import functionalLibrary.Global.ManageDriver_Maxit;

public class Smoke_DataProvider {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	ConfigInputFile objConfigInputFile = new ConfigInputFile();
	DataTable dt = new DataTable();
		

	//###################################################################################################################################################################  
	//Function name		: dp_SmokeData(ITestContext context,Method mPageName)
	//Class name		: Smoke_DataProvider
	//Description 		: Generic method to read data from SmokeData sheet for specific page in smoke test Data Provider
	//Parameters 		: context: ITestContest to read testNG.xml parameters
	//					: mSheetName : Send calling Method (@Test method name) name as parameter
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################		
		@DataProvider(name="Read_SmokeTestData")
		public Object[][] dp_SmokeData(ITestContext context,Method mPageName)throws Exception{
			System.out.println("Data Provider: Inside Method:dp_SmokeData for reading the data from Input file.");			
			String strScriptName = context.getCurrentXmlTest().getParameter("scriptName");
			String strEnv = context.getCurrentXmlTest().getParameter("environment");
			String strClient = context.getCurrentXmlTest().getParameter("clientName");
			String strPageName = mPageName.getName().trim();
			
			String strUIPageName = getUIPageName(strPageName);
			
		    String excelPath = dt.getInputFIlePath_Maxit(strScriptName,strEnv,strClient);
			System.out.println("Excel Path is :"+excelPath);
			return(objConfigInputFile.getSmokeData(excelPath, strPageName,strUIPageName,strClient));
		  
		}//End of DataProvider "dp_SmokeData"	
		
//###################################################################################################################################################################  
//Function name		: getUIPageName(String strDataPageName)
//Class name		: Smoke_DataProvider
//Description 		: Method to get the actual page name in UI from the page name in input excel smoke data file
//Parameters 		: strDataPageName: Page name from excel file
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
	public String getUIPageName(String strDataPageName) {
		switch(strDataPageName.toUpperCase()){
		case "SEARCH_ACCOUNT":
			return "Account";
		case "SEARCH_OPENCLOSED":
			return "Open/Closed";
		case "SEARCH_SECURITYXREF":
			return "Security Xref";
		case "SEARCH_POSRECON":
			return "Pos Recon";
		case "SEARCH_TRXERROR":
			return "Trx Error";
		case "SEARCH_RAWTRADES":
			return "Raw Trades";
		case "SEARCH_LEDGER":
			return "Ledger";
		case "SEARCH_UNREALIZED":
			return "Unrealized";
		case "SEARCH_REALIZED":
			return "Realized";
		case "SEARCH_NOCOSTBASIS":
			return "No Cost Basis";
		case "SEARCH_AUDITTRAIL":
			return "Audit Trail";

		case "POSRECON":
			return "Position Recon";
		case "NOCOSTBASIS":
			return "No Cost Basis";
		case "TRANSACTION_ERROR":
			return "Transaction Error";
		//CB Manager pages:
		case "CBMANAGER_IMPORTS":
			return "Imports";
		case "CBMANAGER_EXPORTS":
			return "Exports";
		case "STEPUP":
			return "Step-Up";
		case "HISTORIC_CORRECTION":
			return "Historical Corrections";
		case "ROLLBACK_RERUN":
			return "Rollback Rerun";
		case "RBI_UPLOAD":
			return "RBI Upload";
		case "AVG_COST_GIFT":
			return "Average Cost Gift";
		case "CB_UPLOAD":
			return "Upload";
		//Other pages
		case "DMI":
			return "Data Management";
		case "CA_AGING":
			return "";
		case "CA_MANAGER":
			return "CA Manager ";
		case "DASHBOARD":
			return "Dashboard";
		default:
			return "Account";
		}//End of switch case
	  
	}//End of "getUIPageName" method
		

		
//###################################################################################################################################################################
}//End of <class: Smoke_DataProvider>
