package PageObjects.Maxit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.Maxit_CommonFns;

public class GlobalDM_Objs {

	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	Reporting reporter = new Reporting();
	Maxit_CommonFns maxitFns = PageFactory.initElements(driver, Maxit_CommonFns.class);
	
	//Objects:
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/UploadDownload/MaxitDownload/enter.php']")
    private WebElement lnk_GlobalDM;
	
	@FindBy(how=How.XPATH,using="//a[contains(@href, 'UploadDownload/MaxitDownload/enter.php')]")
    public WebElement lnk_Download;
	@FindBy(how=How.XPATH,using="//*[@id=\"maxitDownloadSearchForm\"]")
    private WebElement ele_DownloadSearchForm;
	@FindBy(how=How.XPATH,using="//*[@id=\"clearBtn\"]")
    private WebElement btn_Reset;
	@FindBy(how=How.XPATH,using="//*[@id=\"submitBtn\"]")
    private WebElement btn_Search;
	
	//File Manager
	@FindBy(how=How.XPATH,using="//a[contains(@href, 'UploadDownload/FileManager/enter.php')]")
    public WebElement lnk_FileManager;
	@FindBy(how=How.XPATH,using="//*[@id=\"fileManagerSearchForm\"]")
    private WebElement ele_FileManagerSearchForm;
	
	
	//*********************************************************************************************************************  
	//Performing Actions on objects in CA MAnager:
	//*********************************************************************************************************************   

    //###################################################################################################################################################################  
    //Function name		: Navigate_GlobalDM(String strTabName,int exclRowCnt,String strSheetName)
    //Class name		: GlobalDM_Objs
    //Description 		: Function to Navigate to give tab under GLobal DM page
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
    	public boolean Navigate_GlobalDM(String strTabName,int exclRowCnt,String strSheetName,String strPageExists){
    		WebElement ele_TabName;
    		try{  			
    			if(CommonUtils.isElementPresent(lnk_GlobalDM)){
    				lnk_GlobalDM.click();
    				ele_TabName = getGlobalDM_TabElement(strTabName);
    				if(ele_TabName==null){
    	    			System.out.println("<GlobalDM_Objs.Navigate_GlobalDM> : TabName parameter is incorrect, please check");
    	    			reporter.reportStep(exclRowCnt, "Failed", "Global DM_Cannot navigate to given tab, <GlobalDM_Objs.Navigate_GlobalDM> : TabName parameter["+strTabName+"] is incorrect, please check..!!", "", "GlobalDM_IncorrectTabName",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    			return false;
    				}
    				
    				//Verify if give tab exists for this client from input test data
    				if(strPageExists.equalsIgnoreCase("Y")){
    	    			if(CommonUtils.isElementPresent(ele_TabName)){
    	    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] GlobalDM_"+strTabName+" tab is displayed.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    				ele_TabName.click();
    	    				//Verify for error messages & Header
    	    				if(!this.GlobalDM_Errors(strTabName,exclRowCnt,strSheetName)) return false;
    	    				if(!this.GlobalDM_Header(strTabName,exclRowCnt,strSheetName)) return false;    	    				   	    
    	    			}
    	    			else{
    	    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM__"+strTabName+" tab is expected, but NOT displayed in UI.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    				return false;
    	    			}//End of else condition to check if strTabName tab is displayed or not.
    	    				
    				}//End of IF condition to check if PageExists for given client
    				
    				else{
    					if(!CommonUtils.isElementPresent(ele_TabName)){
    						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] GlobalDM__"+strTabName+" tab is NOT displayed as expected.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    						return false;
    					}
    					else{
    	    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM__"+strTabName+" tab is NOT expected for this client, but displayed in UI.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    				return false;
    					}
    				}//End of Else condition to check if PageExists for given client
    				
    				
    				return true;	
    			}//End of IF Condition to check if Global DM element exists
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Global DM' link in left side Navigation pannel is NOT displayed in UI,Please check.!", "", "GlobalDM",  driver, "BOTH_DIRECTIONS", strSheetName, null);				
    			return false;	
    		}//End of Try block
    		catch(Exception e){
    			System.out.println(e.getMessage());
    			System.out.println("Exception in <GlobalDM_Objs.Navigate_GlobalDM>: Global DM Link is not displayed in UI..!!!");
    			reporter.reportStep(exclRowCnt, "Failed", "Global DM_Exception in<GlobalDM_Objs.Navigate_GlobalDM> Please Check..!!", "", "GlobalDM_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    		}
    	}//End of Navigate_GlobalDM Method		
    	
    	
	//###################################################################################################################################################################  
	//Function name		: GlobalDM_Errors(String strTabName,int exclRowCnt,String strSheetName)
	//Class name		: GlobalDM_Objs
	//Description 		: Function to verify if there are any error messages in 'Global DM' page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################
		public boolean GlobalDM_Errors(String strTabName,int exclRowCnt,String strSheetName){
			try{
				//Check if there are any error messages:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in 'Global DM' page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'GlobalDM_"+strTabName+"' page_ErrorMessage:"+strErMsg, "", strTabName+"_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.
				return true;
			}//End of Try block
			catch(Exception e){
				System.out.println("Exception in <GlobalDM_Objs.GlobalDM_Errors>: 'Global DM->"+strTabName+"' verifying for error messages.");
				return false;
			}//End of catch block
		}//End of <Method: GlobalDM_Errors>	    	
    //###################################################################################################################################################################  
    //Function name		: GlobalDM_Header(String strTabName,int exclRowCnt,String strSheetName)
    //Class name		: GlobalDM_Objs
    //Description 		: Function to verify Header in the corresponding tab of GLobal DM
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean GlobalDM_Header(String strTabName,int exclRowCnt,String strSheetName){
    		String strExp_Header = this.GlobalDM_ExpectedPageTitle(strTabName);
    		try{
				//Check if the header is correct
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_PageHeader)){
					String strAct_Header = CBManager_Objs.ele_PageHeader.getAttribute("textContent");
					if(strAct_Header.contains(strExp_Header)){
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] GlobalDM_"+strTabName+" page header/title is as expected, Expected Header:"+strExp_Header, "", "GlobalDM_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return true;
					}
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM_"+strTabName+" page header/title is not as expected, Actual header:"+strAct_Header+" and Expected Header:"+strExp_Header, "", "GlobalDM_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return false;
					}
				}//End of if condition to to see if there are errors.
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM_"+strTabName+" page header/title object is not displayed, please check..!", "", "GlobalDM_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <GlobalDM_Objs.GlobalDM_Header>: GlobalDM->"+strTabName+" verifying page header title.");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM_"+strTabName+":Exception in <GlobalDM_Objs.GlobalDM_Header>, while verifying page title/Header. Please Check..!!", "", "GlobalDM_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    		}//End of catch block
    	}//End of <Method: GlobalDM_Header>
	
    //###################################################################################################################################################################  
    //Function name		: GlobalDM_GenericCheckpoints(String strTabName,int exclRowCnt,String strSheetName)
    //Class name		: GlobalDM_Objs
    //Description 		: Function to verify Generic Checkpoints in GLobal DM tabs
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public void GlobalDM_GenericCheckpoints(String strTabName,int exclRowCnt,String strSheetName){
    		WebElement ele_SearchFormName;
    		try{
    			if(strTabName.equalsIgnoreCase("Download")) ele_SearchFormName = ele_DownloadSearchForm;
    			else if(strTabName.equalsIgnoreCase("File_Manager")) ele_SearchFormName = ele_FileManagerSearchForm;
    			else ele_SearchFormName = null;
				//Check if the header is correct
				if(CommonUtils.isElementPresent(ele_SearchFormName)){
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] GlobalDM_"+strTabName+" Search form is displayed.", "", strTabName+"_SearchForm",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of if condition to to see if there are errors.
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM_"+strTabName+" Search form is NOT displayed,please check.!!", "", strTabName+"_SearchForm",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}
			
				//Check if the Reset button is displayed
				if(CommonUtils.isElementPresent(btn_Reset)){
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] GlobalDM_"+strTabName+" Reset button is displayed.", "", strTabName+"_Reset",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of if condition to to see if there are errors.
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM_"+strTabName+" Reset button is NOT displayed,please check!!", "", strTabName+"_Reset",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}
				
				//Check if the Search button is displayed
				if(CommonUtils.isElementPresent(btn_Search)){
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] GlobalDM_"+strTabName+" Search button is displayed.", "", strTabName+"_Search",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of if condition to to see if there are errors.
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM_"+strTabName+" Search button is NOT displayed,please check!!", "", strTabName+"_Search",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}
				
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <GlobalDM_Objs.GlobalDM_Header>: GlobalDM->"+strTabName+" verifying page header title.");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] GlobalDM_"+strTabName+":Exception in <GlobalDM_Objs.GlobalDM_Header>, while verifying page title/Header. Please Check..!!", "", "GlobalDM_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return ;
    		}//End of catch block
    	}//End of <Method: GlobalDM_Header>    	
    //###################################################################################################################################################################  
    //Function name		: getGlobalDM_TabElement(String strTabName)
    //Class name		: GlobalDM_Objs
    //Description 		: Function to verify Header in the corresponding tab of Global DM
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public WebElement getGlobalDM_TabElement(String strTabName){
    		
    			switch(strTabName.toUpperCase()){
    			case "DOWNLOAD":
    				return this.lnk_Download;
    			case "FILE_MANAGER":
    				return this.lnk_FileManager;
    			default:
    				return null;
    					
    			}

    	}//End of <Method: getGlobalDM_TabElement>    
    //###################################################################################################################################################################  
    //Function name		: GlobalDM_ExpectedPageTitle(String strTabName)
    //Class name		: GlobalDM_Objs
    //Description 		: Function to get Expected page title for give tab under Global DM page
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public String GlobalDM_ExpectedPageTitle(String strTabName){
    			switch(strTabName.toUpperCase()){
    			case "DOWNLOAD":
    				return "Download";
    			case "FILE_MANAGER":
    				return "File Manager";	
    			default:
    				return null;
    					
    			}

    	}//End of <Method: GlobalDM_ExpectedPageTitle>
    	
}//End of <Class: GlobalDM_Objs>
