
package PageObjects.Maxit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class RollBackRerun_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	Reporting reporter = new Reporting();

	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);

	//Rollback Rerun
	@FindBy(how=How.XPATH,using="//a[text()='Rollback Rerun']")
	public WebElement lnk_RollbackRerun;

	//Rollback Rerun upload file link
	@FindBy(how=How.LINK_TEXT,using="Upload Rollback Rerun File")
	public WebElement lnk_UploadRRFile;
	
	//Input box search no
	@FindBy(how=How.ID,using="searchSecNo")
	public WebElement txtbx_SecurityNo;
	
	//Refresh Button
	@FindBy(how=How.XPATH,using="//input[@value='Refresh']")
    public WebElement btn_Refresh;
	
	


	//*********************************************************************************************************************  
	//Performing Actions on objects in Search Page:
	//*********************************************************************************************************************      
	//###################################################################################################################################################################  
	//Function name		: RollbackRerun_CheckPoints(String strTabName, int exclRowCnt,String strSheetName)
	//Class name		: RollBackRerun_Objs
	//Description 		: Function to verify the Rollback Rerun check points in RR page 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public void RollbackRerun_CheckPoints(String strTabName, int exclRowCnt,String strSheetName){
		try{
			//Check if the Upload Rollback Rerun File  link available  
			if((CommonUtils.isElementPresent(lnk_UploadRRFile))){
				System.out.println("Link - 'Upload Rollback Rerun File' is available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Link - 'Upload Rollback Rerun File' is available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("Link - 'Upload Rollback Rerun File' is not available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Link - 'Upload Rollback Rerun File' is not available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			
			//Check if the Account box  is present 
			if((CommonUtils.isElementPresent(CBMngr_Objs.txtbx_Acct))){
				System.out.println("'Account Input Box' is available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Account Input Box' is available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("'Account Input Box' is not available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Account Input Box' is not available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}

			//Check if the Security box  is present 
			if((CommonUtils.isElementPresent(txtbx_SecurityNo))){
				System.out.println("'SecurityNo Input Box' is available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'SecurityNo Input Box' is available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("'SecurityNo Input Box' is not available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'SecurityNo Input Box' is not available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			
			//Check if the Submit button is present 
			if((CommonUtils.isElementPresent(CBMngr_Objs.btn_Submit))){
				System.out.println("'Submit' Button is available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Submit' Button is available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("'Submit' Button is not available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Submit' Button is not available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			
			//Check if the Refresh button is present 
			if((CommonUtils.isElementPresent(btn_Refresh))){
				System.out.println("'Refresh' Button is available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Refresh' Button is available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("'Refresh' Button is not available in RollbackRerun page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Refresh' Button is not available in RollbackRerun page.", "", "RollbackRerun_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <RollBackRerun_Objs.RollbackRerun_CheckPoints>: CBManger->Rollback Rerun page verifying header title.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <RollBackRerun_Objs.RollbackRerun_CheckPoints>,please check..!!", "", "RollbackRerun_Exceptions",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of catch block
	}//End of <Method: RollbackRerun_CheckPoints>     	

}//End of <Class: RollBackRerun_Objs>
