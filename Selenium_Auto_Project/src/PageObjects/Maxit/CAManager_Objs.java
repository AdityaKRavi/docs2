package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.Maxit.Maxit_CommonFns;



public class CAManager_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	Reporting reporter = new Reporting();
	Maxit_CommonFns maxitFns = PageFactory.initElements(driver, Maxit_CommonFns.class);
	
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/CAService/CAMaster/enter.php']")
    private WebElement lnk_CAManager;	
	@FindBy(how=How.XPATH,using="//*[@id=\"targetCusip\"]")
    private WebElement txt_TargetCusip;
	@FindBy(how=How.XPATH,using="//*[@id=\"caId\"]")
    private WebElement txt_CAID;
	
	//Dropdown lists:
	@FindBy(how=How.XPATH,using="//*[@id=\"eventType\"]")
    private WebElement lst_EventType;
	@FindBy(how=How.XPATH,using="//*[@id=\"status\"]")
    private WebElement lst_Status;
    @FindBy(how=How.XPATH,using="//*[(@id=\"targetType\")]")
    private WebElement lst_TargetType;
    @FindBy(how=How.XPATH,using="//*[(@id=\"taxability\")]")
    private WebElement lst_Taxability;
    @FindBy(how=How.XPATH,using="//*[(@id=\"volMandCode\")]")
    private WebElement lst_VolMandatory;
    @FindBy(how=How.XPATH,using="//*[(@id=\"errorCode\")]")
    private WebElement lst_ReasonCode;

	//Buttons
    @FindBy(how=How.XPATH,using="//*[@id=\"clearBtn\"]")
    public WebElement btn_Clear;
    @FindBy(how=How.XPATH,using="//*[@class=\"mediumButton\" and contains(@value,\"Submit\")]")
    public WebElement btn_Submit;
	
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Reassess Population\")]")
	private WebElement btn_ReassessPopulation;
    @FindBy(how=How.XPATH,using="//*[@id=\"addNewCaButton\"]")
    private WebElement btn_AddNewCA;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Save\")]")
	private WebElement btn_Save;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Cancel\")]")
	private WebElement btn_Cancel;
    
    
    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
    public List<WebElement> ele_ResTble_Header;
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-body\"]")
    public WebElement ele_ResTble;
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid-empty\"]")
    public List<WebElement> ele_NoResults;
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-panel-header-text\") and contains(text(),\"Details\")]")
	private WebElement ele_CA_Details;
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-panel-header-text\") and contains(text(),\"Enable Event Details\")]")
    private WebElement ele_CA_EnableEventDetails;
    @FindBy(how=How.XPATH,using="//*[@id=\"termsHeadForm\"]")
    private WebElement ele_termsHead_form;    
    @FindBy(how=How.XPATH,using="//*[@id=\"enableEventForm\"]")
    private WebElement ele_enableEvent_form;
    @FindBy(how=How.XPATH,using="//*[(@id=\"addNewOptionButton\") and (@class=\"x-btn x-btn-noicon x-item-disabled\")]")
    private WebElement btn_AddNewOption_Disabled;
    @FindBy(how=How.XPATH,using="//*[(@id=\"addNewOptionButton\") and (@class=\"x-btn x-btn-noicon\")]")
    private WebElement btn_AddNewOption_Enabled;
    
    @FindBy(how=How.XPATH,using="//*[@name=\"cashShareFlag\"]")
    private List<WebElement> ele_PaymentMethod;
    @FindBy(how=How.XPATH,using="//*[@name=\"partRedm\"]")
    private WebElement ele_Redemption;
    @FindBy(how=How.XPATH,using="//*[@name=\"seqPartRedm\"]")
    private List<WebElement> ele_SeqRedemption;

    
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-panel-header-text\") and contains(text(),\"Option\")]")
	private List<WebElement> ele_CA_Option;
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"SequenceFieldSet\")]")
    private List<WebElement> ele_Option_form;
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-panel-header-text\") and contains(text(),\"Audit Details\")]")
    private WebElement ele_CA_AuditDetails;
    @FindBy(how=How.XPATH,using="//*[@class=\"dataTable\"]")
    private List<WebElement> ele_CA_AuditTable_Header;
    
    //Population page objects:
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-panel-header-text\") and contains(text(),\"Corporate Actions Event Details\")]")
    private WebElement ele_CA_EventDetails;
    @FindBy(how=How.XPATH,using="//*[@id=\"caDetailDataView\"]/table[1]/tbody/tr[1]/td[1]")
    private WebElement ele_CADetails_CAID;
    @FindBy(how=How.XPATH,using="//*[@id=\"excelLink\"]")
    private WebElement lnk_ExporttoExcel;
    
    @FindBy(how=How.XPATH,using="//*[@id=\"pageTitle\"]/h1")
    private WebElement ele_PageHeader;
    
    @FindBy(how=How.XPATH,using="//*[@class=\"loading-indicator\"]")
    private WebElement ele_PoplnPage_LoadingInd;
    @FindBy(how=How.XPATH,using="//*[@class=\"ext-el-mask-msg x-mask-loading\"]")
    private WebElement ele_Loading; 
//*********************************************************************************************************************  
//Performing Actions on objects in CA MAnager:
   //*********************************************************************************************************************   
//###################################################################################################################################################################  
//Function name		: Navigate_CAManager(int exclRowCnt,String strSheetName)
//Class name		: CAManager_Objs
//Description 		: Function to Navigate to "CA Manager" Page
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
	public boolean Navigate_CAManager(int exclRowCnt,String strSheetName){
		try{  			
			if(CommonUtils.isElementPresent(lnk_CAManager)){
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Navigating to 'CA Manager' page", "", "CA_Page",  driver, "HORIZONTALLY", strSheetName, null);
				lnk_CAManager.click();
				
				//Verify page heading header:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_PageHeader)){
					String strAct_Header = CBManager_Objs.ele_PageHeader.getAttribute("textContent");
					if(strAct_Header.contains("Corporate Actions - Manager")){
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CA Manager page header/title is as expected, Expected Header:Corporate Actions - Manager", "", "CA_Header",  driver, "HORIZONTALLY", strSheetName, null);
						return true;
					}
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager page header/title is not as expected, Actual header:"+strAct_Header+" and Expected Header:Corporate Actions - Manager", "", "CA_Header",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
				}//End of if condition to verify page heading
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager page header/title object is not displayed, please check..!", "", "CA_Header",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}
			}//End of IF condition to check if CA Manager link exists in left navigation panel.
			else{
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager link is not displayed in left navigation panel of page, please check..!", "", "CA_Link",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}
					
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <CAManager_Objs.Navigate_CAManager>: CA MAnager Link is not displayed in UI..!!!");
			reporter.reportStep(exclRowCnt, "Failed", "CA_Exception in <CAManager_Objs.Navigate_CAManager> Please Check..!!", "", "CA_Exception",  driver, "HORIZONTALLY", strSheetName, null);
			return false;
		}
	}//End of Navigate_CAManager Method	
	
//###################################################################################################################################################################  
//Function name		: CAManager_Errors(String strTabName,int exclRowCnt,String strSheetName)
//Class name		: CAManager_Objs
//Description 		: Function to verify if there are any error messages in CA Manager page
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################
	public boolean CAManager_Errors(int exclRowCnt,String strSheetName){
		try{
			//Check if there are any error messages:
			if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
				String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
				System.out.println("Checkpoint : Error in CA Manager page, Error Message is - "+strErMsg);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager page_ErrorMessage:"+strErMsg, "", "CA_Error",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of if condition to to see if there are errors.
			return true;
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <CAManager_Objs.CAManager_Errors>: CA Manager verifying for error messages.");
			return false;
		}//End of catch block
	}//End of <Method: CAManager_Errors>
	
//###################################################################################################################################################################  
//Function name		: VerifyPage_CAManager(int exclRowCnt,String strSheetName)
//Class name		: CAManager_Objs
//Description 		: Function to verify if there are any error messages in CA Manager page
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################
	public boolean VerifyPage_CAManager(int exclRowCnt,String strSheetName){
		try{
			//Check if any errors
			if(this.CAManager_Errors(exclRowCnt, strSheetName)){

				//Verify if 'Target Cusip/ISIN/Sedol text box exists
				if(!(CommonUtils.isElementPresent(txt_TargetCusip))){
					System.out.println("Checkpoint :[Failed] CA Manager page 'Target Cusip' textbox field is not displayed, please check.!!");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager page 'Target Cusip' textbox field is not displayed, please check.!!", "", "CA_TargetCusip",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}
				
				//Verify 'Event Type' drop down
				maxitFns.Compare_ArrayLists("EventType_Dropdown", this.getExp_CAManager_DropDowns("EVENTTYPE"), maxitFns.getAct_DropDowns("EVENTTYPE",lst_EventType,exclRowCnt, strSheetName),exclRowCnt, strSheetName);
				//Verify 'Status' drop down
				maxitFns.Compare_ArrayLists("STATUS_DropDown", this.getExp_CAManager_DropDowns("STATUS"), maxitFns.getAct_DropDowns("STATUS",lst_Status,exclRowCnt, strSheetName),exclRowCnt, strSheetName);
				
				//Verify 'Clear' button exists
				maxitFns.elementExists(btn_Clear,"Clear", exclRowCnt, strSheetName);
				//this.buttonExists("Clear", exclRowCnt, strSheetName);

				//Verify 'Submit' button exists
				maxitFns.elementExists(btn_Submit,"Submit", exclRowCnt, strSheetName);				

			}

			return false;
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <CAManager_Objs.VerifyPage_CAManager>: CA Manager verifying for error messages.");
			return false;
		}//End of catch block
	}//End of <Method: VerifyPage_CAManager>
	
	
//###################################################################################################################################################################  
//Function name		: getAct_CAManager_DropDowns(String strDropDown)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to compare expected & actual drop downs
//Parameters 		: strPopUpName : pop up name
//					: strDropDown: drop down name
//	  				: Excel row number for report and sheet name.
//					: strSheetName: Excel sheet name for reporting  
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public ArrayList<String> getAct_CAManager_DropDowns(String strDropDown){ 
	  try{
			ArrayList<String> arrAct_Values = new ArrayList<String>();
			WebElement eleActDropdown;
			
			if(strDropDown.equalsIgnoreCase("Status")) eleActDropdown = lst_Status;
			else if(strDropDown.equalsIgnoreCase("EventType")) eleActDropdown = lst_EventType;
			else eleActDropdown = null;
			
			List<WebElement> Act_List = eleActDropdown.findElements(By.tagName("option"));
			for (int i = 0; i < Act_List.size(); i++) {
				arrAct_Values.add(Act_List.get(i).getText());
			}

	        return arrAct_Values;
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: getAct_CAManager_DropDowns>,"+e.getMessage()+",please check..!!!");
		  return null;
	  }//End of catch block
	  
  }//End of <Method: getAct_CAManager_DropDowns>
	
//###################################################################################################################################################################  
//Function name		: getExp_CAManager_DropDowns(String strDropDownName)
//Class name		: CAManager_Objs
//Description 		: Function to get an array of expected Drop down values
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
    public ArrayList<String> getExp_CAManager_DropDowns(String strDropDownName){		
  		ArrayList<String> arrExpDropDown = new ArrayList<String>();
  		switch(strDropDownName.toUpperCase()){
  			case "STATUS":
  				arrExpDropDown.addAll(Arrays.asList("All","Match = 100%","Match < 100%","No Payment Found","Upcoming","Pending")); 
      			return arrExpDropDown;
  			case "EVENTTYPE":
  				arrExpDropDown.addAll(Arrays.asList("All","Cusip Change","Stock Dividend","Spin-off","Stock Split","Merger","Voluntary Merger","Tender","Exchange","Prerefunding","Reverse Split","Rights Distribution","Voluntary Exchange")); 
      			return arrExpDropDown;
  			case "REASONCODE":
  				//arrExpDropDown.addAll(Arrays.asList("All","201 - Missing Target Rad","301 - Missing New Rad","302 - Contra Not Found","303 - No Pair ID","304 - Expected New Qty ≠ New RAD Qty","999 - Other","Processed"));
  				arrExpDropDown.addAll(Arrays.asList("All","201 - Missing Target Rad","301 - Missing New Rad","302 - Contra Not Found","303 - No Pair ID","304 - Expected New Qty ? New RAD Qty","999 - Other","Processed")); 
      			return arrExpDropDown;
  			case "ADDNEWCA_EVENTTYPE":
  				arrExpDropDown.addAll(Arrays.asList("Cusip Change","Stock Dividend","Spin-off","Stock Split","Merger","Voluntary Merger","Tender","Exchange","Prerefunding","Reverse Split","Rights Distribution","Voluntary Exchange")); 
      			return arrExpDropDown;
  			case "ADDNEWCA_TAXABILITY":
  				arrExpDropDown.addAll(Arrays.asList("Capital","Cash","Gain Only","Gain to Cash Extent","Non-Taxable","Taxable"));
      			return arrExpDropDown;
  			case "ADDNEWCA_VOLUNTARY":
  				arrExpDropDown.addAll(Arrays.asList("Voluntary","Mandatory")); 
      			return arrExpDropDown;
  			case "ADDNEWCA_TARGETTYPE":
  				arrExpDropDown.addAll(Arrays.asList("CUSIP","ISIN","SEDOL")); 
      			return arrExpDropDown;
  				
  			default:
      			return null;
  		}//End of Switch case
    
    }//End of <Method: getExp_CAManager_DropDowns> 
    
//  //###################################################################################################################################################################  
//  //Function name		: Compare_CAManager_DropDowns(String strDropDownName)
//  //Class name			: CAManager_Objs
//  //Description 		: Function to gcompare Actual & expected dropdown values
//  //Parameters 			: 
//  //Assumption			: None
//  //Developer			: Kavitha Golla
//  //###################################################################################################################################################################		
//  public void Compare_CAManager_DropDowns(String strDropDown,ArrayList<String> arrExp_Values,ArrayList<String> arrAct_Values,int exclRowCnt,String strSheetName){		
//		
//        //Compare Actual & Expected Dropdown values
//        if(!(arrAct_Values.containsAll(arrExp_Values) && arrExp_Values.containsAll(arrAct_Values) )){
//        	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CAManager->"+strDropDown+".Expected Dropdown values = "+arrExp_Values.toString()+"; Actual Dropdown values = "+arrAct_Values.toString(),"", "CAManager_"+strDropDown,   driver, "BOTH_DIRECTIONS", strSheetName, null);
//        	System.out.println("Checkpoint :[Failed] CAManager->"+strDropDown+".Expected Dropdown values = "+arrExp_Values.toString()+"; Actual Dropdown values = "+arrAct_Values.toString());
//		}//End of IF condition to compare expected & actual dropdown values
//        else {
//        	reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CAManager->"+strDropDown+".Dropdown values = "+arrExp_Values.toString(),"", "CAManager_"+strDropDown,   driver, "BOTH_DIRECTIONS", strSheetName, null);
//        	System.out.println("Checkpoint :[Passed] CAManager->"+strDropDown+".Dropdown values = "+arrExp_Values.toString());
//        }
//  
//  }//End of <Method: Compare_CAManager_DropDowns>     

//  //###################################################################################################################################################################  
//  //Function name		: buttonExists(String strButton,int exclRowCnt,String strSheetName)
//  //Class name			: CAManager_Objs
//  //Description 		: Function to verify if button exists
//  //Parameters 			: 
//  //Assumption			: None
//  //Developer			: Kavitha Golla
//  //###################################################################################################################################################################		
//  public boolean buttonExists(String strButton,int exclRowCnt,String strSheetName){		
//		WebElement eleButton;
//		
//		if(strButton.equalsIgnoreCase("Submit")) eleButton = btn_Submit;
//		else if(strButton.equalsIgnoreCase("Clear")) eleButton = btn_Clear;
//		else if(strButton.equalsIgnoreCase("ReassessPopulation")) eleButton = btn_ReassessPopulation;
//		else if(strButton.equalsIgnoreCase("AddNewCA")) eleButton = btn_AddNewCA;
//		else if(strButton.equalsIgnoreCase("AddNewCA_Save")) eleButton = btn_Save;
//		else if(strButton.equalsIgnoreCase("AddNewCA_Cancel")) eleButton = btn_Cancel;
//		else eleButton = null;
//		
//		if(CommonUtils.isElementPresent(eleButton)){
//			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CAManager->"+strButton+" exists.","", "CAManager_"+strButton,   driver, "BOTH_DIRECTIONS", strSheetName, null);
//        	System.out.println("Checkpoint :[Passed] CAManager->"+strButton+" exists.");
//        	return true;
//		}
//		else{
//			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CAManager->"+strButton+" is not displayed,please check.","", "CAManager_"+strButton,   driver, "BOTH_DIRECTIONS", strSheetName, null);
//        	System.out.println("Checkpoint :[Failed] CAManager->"+strButton+" is not displayed,please check.!!.");
//        	return false;
//		}
//		
//  
//  }//End of <Method: buttonExists>  
  
//  //###################################################################################################################################################################  
//  //Function name			: get_CATableHeader(String strPageName,List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
//  //Class name				: CAManager_Objs
//  //Description 			: Function to return table header text
//  //Parameters 				: Web table as a list object, the index of the table if multiple are displayed, excel row count and excel sheet for reporting.
//  //Assumption				: None
//  //Developer				: Kavitha Golla
//  //###################################################################################################################################################################		
//  	public String get_CATableHeader(String strPageName,List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
//			List<WebElement> tbl_Header= eleTableName;
//  		String strHeaderRow="";
//  		try{  	    		
//  			if(eleTableName.isEmpty()){
//  				System.out.println("<Class: CAManager_Objs><Method: get_CATableHeader>: eleTableName parameter passed is empty, please check..!!");
//  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<CAManager_Objs.get_CATableHeader>!", "", "eleTableName",  driver, "HORIZONTALLY", strSheetName, null);
//  				return "";
//  			}
//  			if((tbl_Header.size()>0) && (tbl_Header.size()>=tbl_index)){
//  				List<WebElement> getCols = tbl_Header.get(tbl_index).findElements(By.tagName("th"));
//        			for (WebElement eachCol : getCols) {  
//        				strHeaderRow = strHeaderRow+eachCol.getAttribute("textContent").trim()+";";		
//        			}//End of FOR loop to read header column names
//        			return strHeaderRow;
//  			}//End of IF condition to check if table exists
//  			else{
//  				System.out.println("<Class: CAManager_Objs><Method: get_CATableHeader>: Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index);
//  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index+",please check method<CAManager_Objs.get_CATableHeader>!", "", "Tble_Header",  driver, "HORIZONTALLY", strSheetName, null);
//  				return "";
//  			}//End of IF condition to check if table exists
//  		}//End of Try block
//  		catch(Exception e){
//  			System.out.println("Exception in <CAManager_Objs.get_CATableHeader>: Table Header size in the browser is not as expected..!!!");
//  			e.printStackTrace();
//  			return null;
//  		}
//  	}//End of get_CATableHeader Method  
  //###################################################################################################################################################################  
  //Function name		: getExp_CATableHeaders(String strCATablename)
  //Class name			: CAManager_Objs
  //Description 		: Function to get an array of expected table header
  //Parameters 			: 
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
      public String getExp_CATableHeaders(String strCATablename){		
    		switch(strCATablename.toUpperCase()){
    			case "CA_SEARCHRES_HEADER"://"CA_SearchRes_Header":
    				return "CAID;Target Security;Target Desc;New Security;New Desc;Event;Age;Ex/Effective Date;Status;Population";
    			case "CATERMS_AUDITDETAILS": //"CATerms_AuditDetails":
    				return "Created By;Created Date;Last Modified By;Last Modified Date";
    			case "CADETAILS_POPULATION": //"CATerms_AuditDetails":
    				return ";Account;Event;Effective Target Position;New Security;Expected New Quantity;New RAD Quantity;Reason Code";
    				
    			default:
        			return null;
    		}//End of Switch case
      
      }//End of <Method: getExp_CATableHeaders> 

      
      
    //###################################################################################################################################################################  
    //Function name		: CAManager_Search(int exclRowCnt,String strSheetName,String strViewPage,String strCAID,String strBlankSearch,String strAddNewCA )
    //Class name		: CAManager_Objs
    //Description 		: Function to verify checkpoints in CA Manager search
    //Parameters 		: 
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################			
    	public void CAManager_SearchCheckpoints(int exclRowCnt,String strSheetName,String strViewPage,String strCAID,String strTargetCusip, String strBlankSearch,String strAddNewCA ) throws Exception{
    		SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
    		
    		String strSearchValues;
    		boolean strRet;
    		try{		
    			strSearchValues = "[CAManager;CAID="+strCAID+"TargetCusip="+strTargetCusip+";BlankSearch="+strBlankSearch+"]";
    			strRet = this.CAManager_Search(exclRowCnt, strSheetName,strCAID,strTargetCusip,strBlankSearch);
    			
    			//Search Results verification
				if(strRet){

					//Verify Results table header:
					String strAct_CA_Header = maxitFns.get_TableHeader("CA Manager", ele_ResTble_Header, 0, "td",exclRowCnt, strSheetName);
					String strExp_CA_Header = this.getExp_CATableHeaders("CA_SearchRes_Header");
					System.out.println("Search results Actual Header:"+strAct_CA_Header);
					System.out.println("Search results Expected Header:"+strExp_CA_Header);
					if(strAct_CA_Header.equalsIgnoreCase(strExp_CA_Header)){
						System.out.println("CA Manager Search Results table coulumns header verification pass.Header:"+strAct_CA_Header);
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'CA Manager' Search results header verification pass. Header columns:"+strAct_CA_Header, "", "SearchResults_header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					}//End of IF condition to verify results header
					else{
						System.out.println("Search results header verification failed,Actual Header:"+strAct_CA_Header+" and Expected Header:"+strExp_CA_Header);
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'CA Manager' Search results header verification failed. Actual Header:"+strAct_CA_Header+" and Expected Header:"+strExp_CA_Header, "", "SearchResults_header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					}//End of else condition to verify results header
					
					
					//Verify search results row number, if No Results returned fail the case.		
					int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
					System.out.println("appRowsReturned"+appRowsReturned);
					if(!(appRowsReturned>0)){
						if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Failed", "\nCheckpoint :[Warning] 'CA Manager' Search returned' No Results found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							//Verify 'Add New CA' page checkpoints
							this.CAManager_AddNewCA(exclRowCnt, strSheetName, strAddNewCA);
							
							return;
						}
						System.out.println("Search Results for "+strSearchValues+" is not >0");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'CAManager_SearchCheckpoints' Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}//End of IF condition to check No of rows returned
					else{
						System.out.println("Search Results for "+strSearchValues+" is >0");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'CA Manager' Search returned "+appRowsReturned+" rows successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						
						//Verify 'Add New CA' page checkpoints
						this.CAManager_AddNewCA(exclRowCnt, strSheetName, strAddNewCA);
						
						//Verify 'Event Details' page checkpoints
						this.EventLink_Checkpoints(exclRowCnt, strSheetName);
						
						//Verify 'Population' page checkpoints - Debugging code and unit testing pending:
						this.PopulationPage_Checkpoints(exclRowCnt, strSheetName);
						
						
					}//End of ELSE condition to check No of rows returned
				}//End of IF condition to check strRet from search
			
    		}//End of try block
    		catch(Exception e){
    			System.out.println("Exception in <Method: CAManager_SearchCheckpoints>,please check..!!");
    			reporter.reportStep(exclRowCnt, "Failed", "Exception in CAManager_SearchCheckpoints ", "", "CAManager_SearchCheckpoints",  driver, "HORIZONTALLY", strSheetName, null);
    			return;
    		}//End of Catch block
    	}//End of >Method: CAManager_SearchCheckpoints>      
    	
    //###################################################################################################################################################################  
    //Function name		: CAManager_Search(int exclRowCnt,String strSheetName,String strCAID,String strBlankSearch)
    //Class name		: CAManager_Objs
    //Description 		: Function to perform search in CA Manager page based on input( blank search/CAID search)
    //Parameters 		: 
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################			
    	public boolean CAManager_Search(int exclRowCnt,String strSheetName,String strCAID,String strTargetCusip,String strBlankSearch) throws Exception{
    		LocalDateTime strStartWaitTime,strEndWaitTime;    		
    		try{		
    			//Perform Search
    			if(strBlankSearch.equalsIgnoreCase("Y")){
    				btn_Submit.click();
    			}
    			else{
    				txt_CAID.sendKeys(strCAID);
    				txt_TargetCusip.sendKeys(strTargetCusip);
    				btn_Submit.click();
    			}
    			
    			//Dynamic wait for search results to display with a max threshold of 60sec
    			strStartWaitTime = LocalDateTime.now();
    			try{
    				System.out.println("Dynamic Wait time begins @"+strStartWaitTime);
    				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    										wait.withTimeout(60,TimeUnit.SECONDS)
    										.pollingEvery(2, TimeUnit.SECONDS)				 
    										//.ignoring(NoSuchElementException.class)
    										.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(CBManager_Objs.ele_Loading)));
    			}//End of try block for FluentWait
    			
    			catch(org.openqa.selenium.TimeoutException e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait[catch block] time Ends with a Max threshold wait of 60Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				
    				//Check if page is loading:			
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading)){
    					System.out.println("Checkpoint : CA Manager Blank Search - Stil Loading..[Failed]");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager Search :Failed,still Loading[Waited for search results for 60Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of IF condition to check for loading object.

    				//Checkpoint: Check for Errors:
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in NCB Search Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.				
    			}//End of Catch block for FluentWait
    			
    			catch(org.openqa.selenium.NoSuchElementException e){
        			//Check for 'Loading' object 'Not to be present'
        			if((!(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading))) && (CommonUtils.isElementPresent(ele_ResTble))){
        				strEndWaitTime = LocalDateTime.now();
        				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
        				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CA Manager Search Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);        				
        				return true;
        			}//End of IF condition to check Loading Object is not displayed.
    				
    			}
    			
    			catch(Exception e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Exception:"+e.getMessage());
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 60 Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				return false;
    			}//End of catch block with generic exception for Fluent Wait 

    			//Check for Results header
    			if(CommonUtils.isElementPresent(ele_ResTble)){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CA Manager Search Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return true;
    			}//End of IF condition to check for Results table header.

    			System.out.println("Checkpoint : CA Manager Search after Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager Search :Failed, Results header is not displayed", "", "CASearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    	
    					
    		}//End of try block
    		catch(Exception e){
    			System.out.println("Exception in <Method: CAManager_Search>,please check..!!");
    			reporter.reportStep(exclRowCnt, "Failed", "Exception in CAManager_Search ", "", "CAManager_Search",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    		}//End of Catch block
    	}//End of <Method: CAManager_Search>    	
    	
    //###################################################################################################################################################################  
    //Function name		: CAManager_AddNewCA(int exclRowCnt,String strSheetName,String strCAID,String strBlankSearch)
    //Class name		: CAManager_Objs
    //Description 		: Function to verify 'Add New CA' page
    //Parameters 		: 
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################			
    	public boolean CAManager_AddNewCA(int exclRowCnt,String strSheetName,String strAddNewCA) throws Exception{
    		CommonUtils utils = PageFactory.initElements(driver, CommonUtils.class);
    		boolean strRet;
    		try{		
    			//Verify if 'Add New CA' button exists
    			strRet = this.CA_AddNewCAExists(exclRowCnt, strSheetName, strAddNewCA);
    			
    			//Navigate to 'Add New CA' page
				if(strRet){
					btn_AddNewCA.click();
					Thread.sleep(10000);
					WebDriverWait waitPage = new WebDriverWait(driver, 180);
					if(waitPage.until(ExpectedConditions.numberOfWindowsToBe(2))){
						//Verify new browser is displayed - switch focus
	    				if(!(utils.switch_BrowserTabs(driver, "Corporate Action Terms"))) {
	    					System.out.println("'Corporate Action Terms' page is not displayed after clicking on 'Add New CA' button.");
	    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Corporate Action Terms' page is not displayed after clicking on 'Add New CA' button,please check..!!", "", "CA_AddNewCAPage",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    					return false;
	    				}//End of IF condition to check if 'Corporate Action Terms' page is displayed.
					}//End of IF condition to wait for new browser.
					else{
    					System.out.println("'Corporate Action Terms' page is not displayed after clicking on 'Add New CA' button.");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Corporate Action Terms' page is not displayed after clicking on 'Add New CA' button,please check..!!", "", "CA_AddNewCAPage",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return false;
					}
					driver.manage().window().maximize();
					//'Add New CA' page checkpoints
					this.AddNewCA_Checkpoints(exclRowCnt, strSheetName);
					
					
				}//End of IF condition to verify if 'Add New CA' button exists
    			
    			
    	
    			return false;		
    		}//End of try block
    		catch(Exception e){
    			System.out.println("Exception in <Method: CAManager_AddNewCA>,please check..!!");
    			reporter.reportStep(exclRowCnt, "Failed", "Exception in CAManager_AddNewCA ", "", "CAManager_AddNewCA",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    		}//End of Catch block
    	}//End of <Method: CAManager_AddNewCA>  
    	
	//###################################################################################################################################################################  
    //Function name		: CA_AddNewCAExists(int exclRowCnt,String strSheetName,String strCAID,String strBlankSearch)
    //Class name		: CAManager_Objs
    //Description 		: Function to verify 'Add New CA' page exists
    //Parameters 		: 
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################			
    	public boolean CA_AddNewCAExists(int exclRowCnt,String strSheetName,String strAddNewCA) throws Exception{
    		String addNewCA_PageExists=strAddNewCA.trim();
    		try{		
    			
    			if((addNewCA_PageExists.equalsIgnoreCase("Y")) && (CommonUtils.isElementPresent(btn_AddNewCA))){//(maxitFns.elementExists(btn_AddNewCA,"AddNewCA", exclRowCnt, strSheetName))){
						System.out.println("Checkpoint : CA Manager - Add New CA button exists");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CA Manager- 'Add New CA' button exists as expected.", "", "AddNewCA_Exsts",  driver, "HORIZONTALLY", strSheetName, null);
	    				//btn_AddNewCA.click();
	    				return true;
    			}//End of IF condition to verify if 'Add New CA' exists
	    				
	    		else if((addNewCA_PageExists.equalsIgnoreCase("Y")) && (!CommonUtils.isElementPresent(btn_AddNewCA))){	
						System.out.println("Checkpoint : CA Manager - Add New CA button button doesn't exists");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager- 'Add New CA' button is NOT displayed.Please Check..!", "", "AddNewCA_Exsts",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
    			}//End of ELSE condition to verify if 'Add New CA' exists

	    		else if((addNewCA_PageExists.equalsIgnoreCase("N")) && (!CommonUtils.isElementPresent(btn_AddNewCA))){	
						System.out.println("Checkpoint : CA Manager - 'Add New CA' button is NOT expected for this client.");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CA Manager- 'Add New CA' button is NOT expected for this client.", "", "AddNewCA_Exsts",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
    			}
	    		else if((addNewCA_PageExists.equalsIgnoreCase("N")) && (CommonUtils.isElementPresent(btn_AddNewCA))){	
						System.out.println("Checkpoint : CA Manager - Add New CA button button is NOT expected, but it is displayed");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Manager- 'Add New CA' button is NOT expected for this client, but it is displayed.", "", "AddNewCA_Exsts",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
    			}
    	
    			return false;		
    		}//End of try block
    		catch(Exception e){
    			System.out.println("Exception in <Method: CA_AddNewCAExists>,please check..!!");
    			reporter.reportStep(exclRowCnt, "Failed", "Exception in <CA_AddNewCAExists> ", "", "CA_AddNewCAExists",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    		}//End of Catch block
    	}//End of <Method: CA_AddNewCAExists>  
      
//###################################################################################################################################################################  
//Function name		: AddNewCA_Checkpoints(int exclRowCnt,String strSheetName,String strCAID,String strBlankSearch)
//Class name		: CAManager_Objs
//Description 		: Function to verify 'Add New CA' page checkpoints
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public boolean AddNewCA_Checkpoints(int exclRowCnt,String strSheetName) throws Exception{
		try{	
			CommonUtils utils = PageFactory.initElements(driver, CommonUtils.class);
			//Verify 'Details' section in 'Add New CA' page
			if(this.AddNewCA_VerifyDetails(exclRowCnt, strSheetName,"Add New CA")){
				this.AddNewCA_Details_Dropdowns(exclRowCnt, strSheetName, "ADD_NEW_CA");
			}
			
			//Verify 'Enable Event Details' section in 'Add New CA' page
			this.VerifyEnableEventDetails(exclRowCnt, strSheetName,"Add New CA");

			//Verify Save and Cancel exists
			maxitFns.elementExists(btn_Save, "AddNewCA_Save", exclRowCnt, strSheetName);
			//this.buttonExists("AddNewCA_Save", exclRowCnt, strSheetName);
			if(!(maxitFns.elementExists(btn_Cancel,"AddNewCA_Cancel", exclRowCnt, strSheetName))){
				//close tab since Cancel button doesn't exists
				driver.close();
				return false;
			}
			else btn_Cancel.click();
			utils.switch_BrowserTabs(driver,"Corporate Actions - Manager");
			//driver.switchTo().window("Corporate Actions - Manager");
			
			return true;		
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in <Method: AddNewCA_Checkpoints>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <AddNewCA_Checkpoints> ", "", "AddNewCA_Checkpoints",  driver, "HORIZONTALLY", strSheetName, null);
			return false;
		}//End of Catch block
		
	}//End of <Method: AddNewCA_Checkpoints>  
	
	//###################################################################################################################################################################  
	//Function name		: AddNewCA_VerifyDetails(int exclRowCnt,String strSheetName)
	//Class name		: CAManager_Objs
	//Description 		: Function to verify 'Details' section checkpoints in 'Add New CA' page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public boolean AddNewCA_VerifyDetails(int exclRowCnt,String strSheetName,String strPopUpName) throws Exception{
			try{	
				//Verify Details Header exists
				if(CommonUtils.isElementPresent(ele_CA_Details)){
					System.out.println("'Details' header is displayed in '"+strPopUpName+"' page.");
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->'Details' header is displayed in '"+strPopUpName+"' page.", "", "AddNewCA_Details",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					
					//Verify Details section form exists
					if(CommonUtils.isElementPresent(ele_termsHead_form)){
						System.out.println("'Details' section form is displayed in '"+strPopUpName+"' page.");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->'Details' section form is displayed in '"+strPopUpName+"' page.", "", "AddNewCA_Details",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						
						//Checkpoint to verify Redemption checkbox existence when EventTyep in (Merger,Exchange,Vol. Merger,Vol. Exchange)
						if(strPopUpName.equalsIgnoreCase("CA Terms")){
							String strEventType=lst_EventType.getAttribute("value");
							String strPayMethod = ele_PaymentMethod.get(0).getAttribute("textContent");
							String strRedmCondition = "[PayMethod="+strPayMethod+";EventyType="+strEventType+"]";
							
							try{
								//Verify Redemption in EventDetails page(CA Terms)->Details section is displayed
								if(ele_Redemption.isDisplayed()){
									reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Details Section->Redemption checkbox is displayed.", "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
								//return true;
								}
								else{ 
									reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Details Section->Redemption checkbox is NOT displayed,Please check..!!", "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
									//return false;
								}
							}//End of try block to check Redemption checkbox element existence
							catch(NoSuchElementException  e){
								System.out.println("NoSuchElementException in <Method: AddNewCA_VerifyDetails>,please check..!!");
								reporter.reportStep(exclRowCnt, "Failed", "CA Terms->Detials Section->Redemption checkbox is not displayed. [NoSuchElementException in <AddNewCA_VerifyDetails>]", "", "CATerms_VerifyRedemption",  driver, "HORIZONTALLY", strSheetName, null);
								return false;
							}//End of Catch block to check Redemption check box element existence
							
							//Verify if Redemption is enabled or not based on Event Type & PayMent Method
							switch(strPayMethod.toUpperCase()){
								case "CASH":
									if(strEventType.equalsIgnoreCase("Merger")||strEventType.equalsIgnoreCase("Exchange")||strEventType.equalsIgnoreCase("Voluntary Merger")||strEventType.equalsIgnoreCase("Voluntary Exchange")){
										if(ele_Redemption.isEnabled()){
											reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Details Section->Redemption checkbox is ENABLED."+strRedmCondition, "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
										return true;
										}
										else{ 
											reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Details Section->Redemption checkbox is NOT ENABLED "+strRedmCondition+",Please check..!!", "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
											return false;
										}
									}//End of IF condition to check if Event Type in (Merger,Exchange,Vol. Merger,Vol. Exchange)
									else{
										if(ele_Redemption.isEnabled()){
											reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Details Section->Redemption checkbox is ENABLED,but it is not expected for EventTypes other than 'Merger,Exchange,Vol Merger,Vol Exchnage' with PayMethod=CASH.'"+strRedmCondition, "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
										return true;
										}
										else{ 
											reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Details Section->Redemption checkbox is NOT ENABLED as expected."+strRedmCondition, "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
											return false;
										}
									}
								case "SHARES":
									if(ele_Redemption.isEnabled()){
										reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Details Section->Redemption checkbox is ENABLED,but it is not expected for EventTypes other than 'Merger,Exchange,Vol Merger,Vol Exchnage'.'"+strRedmCondition, "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
									return true;
									}
									else{ 
										reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Details Section->Redemption checkbox is NOT ENABLED as expected."+strRedmCondition, "", "Details_REDM",  driver, "HORIZONTALLY", strSheetName, null);
										return false;
									}
									
							}//End of Switch case
							

						}//End of IF condition to check if pop up=CA Terms
						
						return true;
					}//End of IF condition to check if 'Details' section form is displayed in 'Add New CA' page
					else{
					System.out.println("'Details' section form is NOT displayed in '"+strPopUpName+"' page.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Details' section form is NOT displayed in '"+strPopUpName+"' page,please check..!", "", "AddNewCA_Details",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
					}
					

					
				}//End of IF condition to check if 'Details' header is displayed in 'Add New CA' page
				else{
				System.out.println("'Details' header is NOT displayed in '"+strPopUpName+"' page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Details' header is NOT displayed in '"+strPopUpName+"' page,please check..!", "", "AddNewCA_Details",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;
				}//End of ELSE condition to check if 'Details' header is displayed in 'Add New CA' page
				
			}//End of try block
			catch(NoSuchElementException  e){
				System.out.println("NoSuchElementException in <Method: AddNewCA_VerifyDetails>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "NoSuchElementException in <AddNewCA_VerifyDetails>: "+e.getMessage(), "", "AddNewCA_VerifyDetails",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}
			
			catch(Exception e){
				System.out.println("Exception in <Method: AddNewCA_VerifyDetails>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in <AddNewCA_VerifyDetails>: "+e.getMessage(), "", "AddNewCA_VerifyDetails",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of Catch block
		}//End of <Method: AddNewCA_VerifyDetails>
	
	//###################################################################################################################################################################  
	//Function name		: AddNewCA_Details_Dropdowns(int exclRowCnt,String strSheetName)
	//Class name		: CAManager_Objs
	//Description 		: Function to verify 'Details' dropdowns in 'Add New CA' page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public void AddNewCA_Details_Dropdowns(int exclRowCnt,String strSheetName,String strPopUpName) throws Exception{
			try{	
				//Verify 'Add New CA' drop downs
				SearchRes_ActionMenu searchRes_Objs = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
				
				lst_EventType.click();
				searchRes_Objs.compareDropDown(strPopUpName, "ADDNEWCA_EVENTTYPE", exclRowCnt, strSheetName);
				lst_Taxability.click();
				searchRes_Objs.compareDropDown(strPopUpName, "ADDNEWCA_TAXABILITY", exclRowCnt, strSheetName);
				lst_VolMandatory.click();
				searchRes_Objs.compareDropDown(strPopUpName, "ADDNEWCA_VOLUNTARY", exclRowCnt, strSheetName);
				lst_TargetType.click();
				searchRes_Objs.compareDropDown(strPopUpName, "ADDNEWCA_TARGETTYPE", exclRowCnt, strSheetName);
				lst_TargetType.click();
				
				//Verify 'Add New Option' button is disabled.
				if(CommonUtils.isElementPresent(btn_AddNewOption_Disabled)){
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Add New CA->'Add New Option' button is disabled by default.","", "AddNewOption",   driver, "BOTH_DIRECTIONS", strSheetName, null);
		        	System.out.println("Checkpoint :[Passed] Add New CA->'Add New Option' button is disabled by default as expected.");		        	
				}//End of IF condition to verify 'Add New Option' disabled
				else{
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Add New CA->'Add New Option' button is NOT disabled by default.","", "AddNewOption",   driver, "BOTH_DIRECTIONS", strSheetName, null);
		        	System.out.println("Checkpoint :[Failed] Add New CA->'Add New Option' button is NOT disabled by default,please check..!");
				}//End of ELSE condition to verify 'Add New Option' disabled
				
				//Select EventType=Cusip Change, and verify 'Add New Option' button is enabled.
				lst_EventType.click();
				if(searchRes_Objs.selectValue_DropDown("Add_New_CA", "EventType", exclRowCnt, strSheetName, "Cusip Change")){
					//Verify 'Add New Option' button is Enabled.
					if(CommonUtils.isElementPresent(btn_AddNewOption_Enabled)){
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Add New CA->'Add New Option' button is ENABLED after selecting EventType=Cusip Change.","", "AddNewOption",   driver, "BOTH_DIRECTIONS", strSheetName, null);
			        	System.out.println("Checkpoint :[Passed] Add New CA->'Add New Option' button is ENABLED after selecting EventType=Cusip Change.");
			        	return;
					}//End of IF condition to verify 'Add New Option' enabled
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Add New CA->'Add New Option' button is NOT ENABLED after selecting EventType=Cusip Change","", "AddNewOption",   driver, "BOTH_DIRECTIONS", strSheetName, null);
			        	System.out.println("Checkpoint :[Failed] Add New CA->'Add New Option' button is NOT ENABLED after selecting EventType=Cusip Change,please check..!");
			        	return;
					}//End of ELSE condition to verify 'Add New Option' enabled
				}//End of IF Condition to Select Event Type drop down=Cusip Change
				
				
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Method: AddNewCA_Details_Dropdowns>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in <AddNewCA_Details_Dropdowns> ", "", "AddNewCA_Details_Dropdowns",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of Catch block
		}//End of <Method: AddNewCA_Details_Dropdowns>
	//###################################################################################################################################################################  
	//Function name		: VerifyEnableEventDetails(int exclRowCnt,String strSheetName,String strPopUpName)
	//Class name		: CAManager_Objs
	//Description 		: Function to verify 'Enable Event Details' section checkpoints in 'Add New CA' page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public void VerifyEnableEventDetails(int exclRowCnt,String strSheetName,String strPopUpName) throws Exception{
			try{	
				//Verify 'Enable Event Details' Header exists
				if(CommonUtils.isElementPresent(ele_CA_EnableEventDetails)){
					System.out.println("'Enable Event Details' header is displayed in '"+strPopUpName+"'  page.");
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->'Enable Event Details' header is displayed.", "", "EnableEventDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					
					//Verify 'Enable Event Details' section form exists
					if(CommonUtils.isElementPresent(ele_termsHead_form)){
						System.out.println("'Enable Event Details' section form is displayed in '"+strPopUpName+"' page.");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->'Enable Event Details' section form is displayed.", "", "EnableEventDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					}//End of IF condition to check if 'Details' section form is displayed in '"+strPopUpName+"' page
					else{
					System.out.println("'Enable Event Details' section form is NOT displayed in '"+strPopUpName+"' page.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Enable Event Details' section form is NOT displayed,please check..!", "", "EnableEventDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return ;
					}
					
				}//End of IF condition to check if 'Enable Event Details' header is displayed in '"+strPopUpName+"'  page
				else{
				System.out.println("'Enable Event Details' header is NOT displayed in '"+strPopUpName+"' page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Enable Event Details' header is NOT displayed,please check..!", "", "EnableEventDetails",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return ;
				}//End of ELSE condition to check if 'Enable Event Details' header is displayed in 'Add New CA' page
				
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Method: VerifyEnableEventDetails>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in <VerifyEnableEventDetails> ", "", "VerifyEnableEventDetails",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of Catch block
		}//End of <Method: VerifyEnableEventDetails>

		
	//###################################################################################################################################################################  
	//Function name		: VerifyCATerms_AuditDetails(int exclRowCnt,String strSheetName,String strPopUpName)
	//Class name		: CAManager_Objs
	//Description 		: Function to verify 'Option' sequence fieldSet section checkpoints
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public void VerifyCATerms_AuditDetails(int exclRowCnt,String strSheetName,String strPopUpName) throws Exception{
			StringManipulation strFns = PageFactory.initElements(driver, StringManipulation.class);
			String strExpAudit_Header = this.getExp_CATableHeaders("CATerms_AuditDetails");
			String strActAudit_Header ;
			try{	
				//Verify 'Audit Details' header
				if(maxitFns.elementExists(ele_CA_AuditDetails, "CA Terms->'Audit Details' header", exclRowCnt, strSheetName)){
					//reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->'Audit Details' header is displayed.", "", "AuditDetails",  driver, "HORIZONTALLY", strSheetName, null);
					//Verify 'Audit Details' header
					strActAudit_Header = maxitFns.get_TableHeader("CATerms_AuditDetails", ele_CA_AuditTable_Header, 0, "th",exclRowCnt, strSheetName);
					strFns.compareString_Equality(strActAudit_Header,strExpAudit_Header);
				}
				else
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Audit Details' header is NOT displayed,Please check..!!", "", "AuditDetails",  driver, "HORIZONTALLY", strSheetName, null);
							
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Method: VerifyCATerms_AuditDetails>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in <VerifyCATerms_AuditDetails> ", "", "VerifyCATerms_AuditDetails",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of Catch block
		}//End of <Method: VerifyCATerms_AuditDetails>	
		
	//###################################################################################################################################################################  
	//Function name		: EventLink_Checkpoints(int exclRowCnt,String strSheetName)
	//Class name		: CAManager_Objs
	//Description 		: Function to verify 'Event' link checkpoints
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public void EventLink_Checkpoints(int exclRowCnt,String strSheetName) throws Exception{
			try{	
				CommonUtils utils = PageFactory.initElements(driver, CommonUtils.class);
				SearchPage_Results Res_Objs = PageFactory.initElements(driver, SearchPage_Results.class);
				utils.switch_BrowserTabs(driver,"Corporate Actions - Manager");
				
	  			List<WebElement> app_rows = ele_ResTble.findElements(By.tagName("tr"));
	  			List<WebElement> app_cols = app_rows.get(0).findElements(By.tagName("td"));
	  			int colIndex = Res_Objs.searchRes_GetColIndexByColName("Event");
	  			String strEventType = app_cols.get(colIndex).getAttribute("textContent");
	  			System.out.println("Event type in row#1 of CA Manger search results is:"+strEventType);
	  			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Clicking on Event='"+strEventType+"' in Row1 of the results in CA Manager search.", "", "CA_Res_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	  			app_cols.get(colIndex).findElement(By.tagName("a")).click();
Thread.sleep(10000);
				
				WebDriverWait waitPage = new WebDriverWait(driver, 180);
				if(waitPage.until(ExpectedConditions.numberOfWindowsToBe(2))){
					//Verify new browser is displayed - switch focus
    				if(!(utils.switch_BrowserTabs(driver, "Corporate Action Terms"))) {
    					System.out.println("'Corporate Action Terms' page is not displayed after clicking on 'Event' link in row1.");
    					utils.switch_BrowserTabs(driver, "Corporate Actions - Manager");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Corporate Action Terms' page is not displayed after clicking on 'event' link in Row1,please check..!!", "", "CA_Res_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return ;
    				}//End of IF condition to check if 'Corporate Action Terms' page is displayed.
				}//End of IF condition to wait for new browser.
				else{
					System.out.println("'Corporate Action Terms' page is not displayed after clicking on 'Event' link in Row#1.");
					utils.switch_BrowserTabs(driver, "Corporate Actions - Manager");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Corporate Action Terms' page is not displayed after clicking on 'Event' link in Row#1,please check..!!", "", "CA_Res_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return ;
				}
				
				driver.manage().window().maximize();
				
				//Check for page loading:
				//wait for Loading object to be disappeared in CA Terms pop up page
				if(!(maxitFns.wait_ForObject_ToDisappear(ele_Loading, "Loading_Element", 60, 2, exclRowCnt, strSheetName))){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CA Terms page is still Loading even after 60secs,please check..!!", "","PageLoading",  driver, "HORIZONTALLY", strSheetName, null);
				}
				
				//Verify Details header in 'CA Terms' pop up page
				this.AddNewCA_VerifyDetails(exclRowCnt, strSheetName,"CA Terms");
				
				//Verify 'Enable Event Details' header in 'CA Terms' pop up page
				this.VerifyEnableEventDetails(exclRowCnt, strSheetName,"CA Terms");
				
				//Verify Option 1 in 'CA Terms' pop up page
				this.VerifyOption_CATerms(exclRowCnt, strSheetName, "CA Terms");
				
				//verify Audit Trail in 'CA Terms' pop up page
				this.VerifyCATerms_AuditDetails(exclRowCnt, strSheetName, "CA Terms");
				
				
				if(driver.getTitle().equalsIgnoreCase("Corporate Action Terms")){
					driver.close();
				}
				
				utils.switch_BrowserTabs(driver,"Corporate Actions - Manager");
				
				return ;		
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Method: EventLink_Checkpoints>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in <EventLink_Checkpoints> ", "", "EventLink_Checkpoints",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of Catch block
			
		}//End of <Method: EventLink_Checkpoints>  		
		
		
	//###################################################################################################################################################################  
	//Function name		: PopulationPage_Checkpoints(int exclRowCnt,String strSheetName)
	//Class name		: CAManager_Objs
	//Description 		: Function to verify 'Population' link checkpoints
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public void PopulationPage_Checkpoints(int exclRowCnt,String strSheetName) throws Exception{
			try{	
				CommonUtils utils = PageFactory.initElements(driver, CommonUtils.class);
				SearchPage_Results Res_Objs = PageFactory.initElements(driver, SearchPage_Results.class);
				utils.switch_BrowserTabs(driver,"Corporate Actions - Manager");
				
	  			List<WebElement> app_rows = ele_ResTble.findElements(By.tagName("tr"));
	  			List<WebElement> app_cols = app_rows.get(0).findElements(By.tagName("td"));
	  			
	  			//Read EventType ; CAID and Population Link of row#1
	  			int colIndex_Event = Res_Objs.searchRes_GetColIndexByColName("Event");
	  			String strEventType = app_cols.get(colIndex_Event).getAttribute("textContent");
	  			
	  			int colIndex_CAID = Res_Objs.searchRes_GetColIndexByColName("CAID");
	  			String strCAID = app_cols.get(colIndex_CAID).getAttribute("textContent");
	  			
	  			int colIndex_Popln = Res_Objs.searchRes_GetColIndexByColName("Population");
	  			String strPopln_Link = app_cols.get(colIndex_Popln).getAttribute("textContent");
	  			System.out.println("<CAID ;Event type;Population> in row#1 of CA Manger search results is:<"+strCAID+";"+strEventType+";"+strPopln_Link+">");
	  			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Clicking on Population link for CAID:"+strCAID+"' in Row1 of the results in CA Manager search.", "", "CA_Res_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	  			
	  			try{
	  			app_cols.get(colIndex_Popln).findElement(By.tagName("a")).click();
	  			Thread.sleep(10000);
	  			}
	  			catch(org.openqa.selenium.StaleElementReferenceException  e){
	  				System.out.println("Within 'StaleElementReferenceException' exception catch block to click on 'Population' link.");
	  				Thread.sleep(5000);
	  				if(app_cols.get(colIndex_Popln).isEnabled())
	  					app_cols.get(colIndex_Popln).click();
	  			}

	  			WebDriverWait waitPage = new WebDriverWait(driver, 180);
				if(waitPage.until(ExpectedConditions.numberOfWindowsToBe(2))){
					//Verify new browser is displayed - switch focus
    				if(!(utils.switch_BrowserTabs(driver, "Corporate Actions - Details"))) {
    					System.out.println("'Corporate Actions - Details' page is not displayed after clicking on 'Population' link in row1.");
    					utils.switch_BrowserTabs(driver, "Corporate Actions - Manager");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Corporate Actions - Details' page is not displayed after clicking on 'population' link in Row1,please check..!!", "", "CA_Res_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return ;
    				}//End of IF condition to check if 'Corporate Actions - Details' page is displayed.
				}//End of IF condition to wait for new browser.
				else{
					System.out.println("'Corporate Actions - Details' page is not displayed after clicking on 'Population' link in Row#1.");
					utils.switch_BrowserTabs(driver, "Corporate Actions - Manager");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Corporate Actions - Details' page is not displayed after clicking on 'Population' link in Row#1,please check..!!", "", "CA_Res_Event",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return ;
				}
				
				driver.manage().window().maximize();
				//Verify Population page checkpoints
				this.Verify_PopulationPage(strCAID, exclRowCnt, strSheetName,"PopulationPage");
				
				
				if(driver.getTitle().equalsIgnoreCase("Corporate Actions - Details")){
					driver.close();
				}
				
				utils.switch_BrowserTabs(driver,"Corporate Actions - Manager");
				
				return ;		
			}//End of try block
			catch(org.openqa.selenium.TimeoutException e){
				System.out.println("TimeoutException in <Method: PopulationPage_Checkpoints>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "TimeoutException in <PopulationPage_Checkpoints>: Population page is not displayed,please check.", "", "PopulationPage_Checkpoints",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}
			catch(Exception e){
				System.out.println("Exception in <Method: PopulationPage_Checkpoints>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in <PopulationPage_Checkpoints> ", "", "PopulationPage_Checkpoints",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of Catch block
			
		}//End of <Method: PopulationPage_Checkpoints>	
		
//###################################################################################################################################################################  
//Function name		: Verify_PopulationPage(int exclRowCnt,String strSheetName)
//Class name		: CAManager_Objs
//Description 		: Function to verify if there are any error messages in CA Manager page
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################
	public void Verify_PopulationPage(String strExpCAID,int exclRowCnt,String strSheetName,String strPopUpName){
		try{
			StringManipulation strFns = PageFactory.initElements(driver, StringManipulation.class);
			String strExpPopln_Header = this.getExp_CATableHeaders("CADETAILS_POPULATION");
			String strActPopln_Header ;
			String strActCAID;
			String strExpReasonCode, strActReasonCode;
			//wait for Loading object to be disappeared in Population Page->'Corporate Actions Event Detials' section
			if(!(maxitFns.wait_ForObject_ToDisappear(ele_PoplnPage_LoadingInd, "Loading_Element", 30, 2, exclRowCnt, strSheetName))){
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Corporate Actions Event Detials' section is still Loading.. So skipping CAID verification from CA Manager->Search Res page[CAID="+strExpCAID+"]", "","CA_CAID",  driver, "HORIZONTALLY", strSheetName, null);
			}
			
			else{
				//Verify CAID from CA Manager search results page to match with Population page CAID.
				strActCAID = ele_CADetails_CAID.getAttribute("textContent");
				strActCAID = strActCAID.replaceFirst("CAID:", "");
				if(strActCAID.equals(strExpCAID))
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->CAID' is same as row#1 in CA Manager->Search Res. CAID="+strExpCAID, "", "CA_CAID",  driver, "HORIZONTALLY", strSheetName, null);
				else
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->CAID' is NOT same as row#1 in CA Manager->Search Res. Expected CAID="+strExpCAID+" and Actual CAID="+strActCAID, "", "CA_CAID",  driver, "HORIZONTALLY", strSheetName, null);
				
			}
			
			//Check if any errors
			if(this.CAManager_Errors(exclRowCnt, strSheetName)){

				//Verify if 'Corporate Actions - Details' header is displayed.
				if(!(CommonUtils.isElementPresent(ele_PageHeader))){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Page header after clicking on 'Population' link is NOT displayed, please check.!!", "", "CA_Header",  driver, "HORIZONTALLY", strSheetName, null);
				}else{
					if(ele_PageHeader.getAttribute("textContent").contains("Corporate Actions - Details"))
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Page header after clicking on 'Population' link is 'Corporate Actions - Details'.", "", "CA_Header",  driver, "HORIZONTALLY", strSheetName, null);
				}
				
				//Verify if 'Corporate Actions Event Details' header is displayed.
				if(!(CommonUtils.isElementPresent(ele_CA_EventDetails))){
					System.out.println("Checkpoint :[Failed] 'Corporate Actions Event Details' header is not displayed in Population page, please check.!!");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Corporate Actions Event Details' header is not displayed in Population page, please check.!!", "", "CA_EventDetails",  driver, "HORIZONTALLY", strSheetName, null);
				}
				else{
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->'Corporate Actions Event Details' header is displayed in Population page.", "", "CA_EventDetails",  driver, "HORIZONTALLY", strSheetName, null);
				}
				
				//Verify 'Reason Code' drop down
				strExpReasonCode = this.getExp_CAManager_DropDowns("REASONCODE").toString();
				strActReasonCode = maxitFns.getAct_DropDowns("REASONCODE",lst_ReasonCode,exclRowCnt, strSheetName).toString();
				String strActReasonCode_New = strActReasonCode.replace("\u2260", "?");
				if(strFns.compareString_Equality(strActReasonCode_New,strExpReasonCode))
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Population page->'Reason Code' dropdown is as expected:"+strActReasonCode.toString(), "", "ReasonCode",  driver, "HORIZONTALLY", strSheetName, null);
				else
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Population page->'Reason Code' dropdown is NOT as expected. Actual:"+strActReasonCode_New.toString()+" and Expected:"+strExpReasonCode, "", "ReasonCode",  driver, "HORIZONTALLY", strSheetName, null);
				//maxitFns.Compare_ArrayLists("ReasonCode_Dropdown", this.getExp_CAManager_DropDowns("REASONCODE"), maxitFns.getAct_DropDowns("REASONCODE",lst_ReasonCode,exclRowCnt, strSheetName),exclRowCnt, strSheetName);
				
				
				//Verify 'Submit' button exists
				maxitFns.elementExists(btn_Submit,"Submit", exclRowCnt, strSheetName);				
				//Verify 'Reassess population' button exists
				maxitFns.elementExists(btn_ReassessPopulation,"Reassess_Population", exclRowCnt, strSheetName);	
				
				
				//Export to excel
				if(CommonUtils.isElementPresent(lnk_ExporttoExcel))
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Population page->'Export to Excel' link is displayed.", "", "CA_Excel",  driver, "HORIZONTALLY", strSheetName, null);
				else reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Population page->'Export to Excel' link is NOT displayed., please check.!!", "", "CA_Excel",  driver, "HORIZONTALLY", strSheetName, null);
				
				//Verify Table columns
				strActPopln_Header = maxitFns.get_TableHeader("CADETAILS_POPULATION", ele_ResTble_Header, 0, "td",exclRowCnt, strSheetName);
				if(strFns.compareString_Equality(strActPopln_Header,strExpPopln_Header)){
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Population page->'CA Details table' is displayed.", "", "PoplnPage_Table",  driver, "HORIZONTALLY", strSheetName, null);
				}
				else reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Population page->'CA Details table' is NOT displayed., please check.!!", "", "PoplnPage_Table",  driver, "HORIZONTALLY", strSheetName, null);
				
			}

			
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <CAManager_Objs.Verify_PopulationPage>: "+e.getMessage());
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exception in <CAManager_Objs.Verify_PopulationPage>: "+e.getMessage(), "", "CA_PoplnPage_Excp",  driver, "HORIZONTALLY", strSheetName, null);
			return ;
		}//End of catch block
	}//End of <Method: Verify_PopulationPage>		
		
	
	
	
	//###################################################################################################################################################################  
	//Function name		: VerifyOption_CATerms(int exclRowCnt,String strSheetName,String strPopUpName)
	//Class name		: CAManager_Objs
	//Description 		: Function to verify 'Option' sequence fieldSet section checkpoints
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public void VerifyOption_CATerms(int exclRowCnt,String strSheetName,String strPopUpName) throws Exception{
			try{	
				String strEventType=lst_EventType.getAttribute("value");
				String strPayMethod;

				//Verify 'Option' Header exists
				if(ele_CA_Option.size()>0){	
					for(int intEachOption=0;intEachOption<ele_CA_Option.size();intEachOption++){
						if(!(ele_CA_Option.get(intEachOption).isDisplayed())){
							System.out.println("'Option'"+(intEachOption+1)+" header is NOT displayed in '"+strPopUpName+"'  page.");
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Option'"+(intEachOption+1)+" header is NOT displayed,please check..!!", "", "Option_CA",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						}
						//Verify Sequence fields set = option.size:
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->'Option'"+(intEachOption+1)+" header is displayed.", "", "Option_CA",  driver, "BOTH_DIRECTIONS", strSheetName, null);						
					}//End of <For loop>
				}//End of IF condition to check if Option header objects are displayed in UI by verifying ele_Option.size>0
				else{
					System.out.println("'Option1' header/s is NOT displayed in '"+strPopUpName+"'  page.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->'Option1' header/s is NOT displayed,Please check..!!", "", "Option_CA",  driver, "HORIZONTALLY", strSheetName, null);
				}//End of ELSE condition to check if Option header objects are displayed in UI by verifying ele_Option.size>0

				
				//New code : 1) get seq objects , then get Paymethod from that and then redemption for each seq
				List<WebElement> ele_Seqs = driver.findElements(By.cssSelector("fieldset[id*='seq_']"));
				System.out.println(strPopUpName+"->Total number of sequences displayed:"+ele_Seqs.size());
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Total number of sequences displayed:"+ele_Seqs.size(), "", "total_Seqs",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				
				
				for(int intEachSeq=0; intEachSeq<ele_Seqs.size();intEachSeq++){
					WebElement eachSeq = ele_Seqs.get(intEachSeq);
				//for(WebElement eachSeq : ele_Seqs) {
					List<WebElement> ele_PayMethods = eachSeq.findElements(By.cssSelector("[name='cashShareFlag']"));
					System.out.println("SIze of PayMethods for each Seq is" + ele_PayMethods.size());
					
					for(int i=0;i<ele_PayMethods.size();i++){
						strPayMethod = ele_PayMethods.get(i).getAttribute("value");
						List<WebElement> ele_SeqRedemp = eachSeq.findElements(By.cssSelector("[name='seqPartRedm']"));
						int intSeq_Red = ele_SeqRedemp.size();//eachSeq.findElements(By.cssSelector("[name='seqPartRedm']")).size();
						
						System.out.println("PayMethod"+i+":"+strPayMethod);
						switch (strPayMethod.toUpperCase()){
						case "CASH":
							try{
								if(intSeq_Red>0){
									if(strEventType.equalsIgnoreCase("Merger")||strEventType.equalsIgnoreCase("Exchange")||strEventType.equalsIgnoreCase("Voluntary Merger")||strEventType.equalsIgnoreCase("Voluntary Exchange")){
										
										if(ele_SeqRedemp.get(i).isDisplayed() && ele_SeqRedemp.get(i).isEnabled())
											reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Seq"+(intEachSeq+1)+"->PayMethod="+strPayMethod+" Redemption checkbox is DISPLAYED & ENABLED as expected", "", "Option_CA_REDM",  driver, "HORIZONTALLY", strSheetName, null);
										else 
											reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Seq"+(intEachSeq+1)+"->PayMethod="+strPayMethod+" Redemption checkbox is NOT displayed/Enabled,Please check..!!", "", "Option_CA_REDM",  driver, "HORIZONTALLY", strSheetName, null);
									}//End of IF condition to check if Event Type in (Merger,Exchange,Vol. Merger,Vol. Exchange)
									else{
										if(ele_SeqRedemp.get(i).isDisplayed() && (!(ele_SeqRedemp.get(i).isEnabled())))
											reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Seq"+(intEachSeq+1)+"->PayMethod="+strPayMethod+" Redemption checkbox is DISPLAYED & NOT ENABLED as expected.", "", "Option_CA_REDM",  driver, "HORIZONTALLY", strSheetName, null);
										else
											reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Seq"+(intEachSeq+1)+"->PayMethod="+strPayMethod+" Redemption checkbox NOT displayed/Enabled,Please check.!!", "", "Option_CA_REDM",  driver, "HORIZONTALLY", strSheetName, null);
									}//End of Else condition to check if Event Type in (Merger,Exchange,Vol. Merger,Vol. Exchange)
									break;
								}//End of IF condition to check Redemption object size>0
								else{
									System.out.println("Redemption object is not displayed in UI for PayMethod=Cash.Please check..!! <Method: VerifyOption_CATerms>");
									reporter.reportStep(exclRowCnt, "Failed", "CA Terms->Verifying Redemption under each sequence. [Redemption object is not displayed in UI for PayMethod=Cash.Please check..!! <Method: VerifyOption_CATerms>]", "", "VerifyOption_CATerms",  driver, "HORIZONTALLY", strSheetName, null);
									return ;
								}
							}//End of try block to check Redemption checkbox element existence
							catch(NoSuchElementException  e){
								System.out.println("NoSuchElementException in <Method: VerifyOption_CATerms>,please check..!!");
								reporter.reportStep(exclRowCnt, "Failed", "CA Terms->Verifying Redemption under each sequence. [NoSuchElementException in <VerifyOption_CATerms>]", "", "VerifyOption_CATerms",  driver, "HORIZONTALLY", strSheetName, null);
								return ;
							}//End of Catch block to check Redemption check box element existence
						
						case "SHARES":
						//if(strPayMethod.equalsIgnoreCase("Shares")){
							if(!(intSeq_Red>0))
								reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->Seq"+(intEachSeq+1)+"->PayMethod="+strPayMethod+" Redemption checkbox is NOT displayed as expected", "", "Option_CA_REDM",  driver, "HORIZONTALLY", strSheetName, null);
							else
								reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Seq"+(intEachSeq+1)+"->PayMethod="+strPayMethod+" Redemption checkbox is displayed,but it is NOT expected,please check..!!", "", "Option_CA_REDM",  driver, "HORIZONTALLY", strSheetName, null);
							
						}//End of Switch case
						
					}//End of FOR loop for <Payment Methods within each sequence>
				}//End of FOR loop for <each sequence>


			}//End of try block
			catch(Exception e){
				System.out.println("Exception in <Method: VerifyOption_CATerms>,please check..!!"+e.getMessage());
				reporter.reportStep(exclRowCnt, "Failed", "Exception in <VerifyOption_CATerms> ", "", "VerifyOption_CATerms",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of Catch block
		}//End of <Method: VerifyOption_CATerms>	
	
	
	
	
}//End of <class: CAManager_Objs>
