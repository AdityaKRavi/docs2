package PageObjects.Maxit;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class SearchRes_MTM_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	SearchRes_ActionMenu SearchRes_Action = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
	
    //MTM Objects
    @FindBy(how=How.XPATH,using="//*[contains(label,\"MTM\")]") 	//"//*[@name=\"mtmSetting\"]")
	public WebElement ele_MTMSetting; 
    @FindBy(how=How.XPATH,using="//*[contains(label,\"Effective\")]")  //"//*[@name=\"mtmEffectiveDate\"]")
	public WebElement ele_MTMEffDate; 
  
    
  //###################################################################################################################################################################  
  //Function name		: verifyAcct_MTM_DropDowns(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_ActionMenu.java
  //Description 		: Function to verify the drop downs in FI Election Pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifyAcct_MTM_DropDowns(int exclRowCnt,String strSheetName){		
  	try{		
  		SearchRes_Action.ele_EditMethod.click();
  		
  		//MTM setting[On/Off] checkpoints
      	if(!CommonUtils.isElementPresent(ele_MTMSetting)){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] MTMPopUp->MTM Setting object is NOT displayed", "", "MTM_MTMSetting",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] MTMPopUp->MTM Setting is NOT displayed,please check..!!");
  		}//End of if condition to check if DROPDOWN is displayed
      	else{
      		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] MTMPopUp->MTM Setting object is displayed", "", "MTM_MTMSetting",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Passed] MTMPopUp->MTM Setting object is displayed");
      	}
      	ele_MTMSetting.click();    	
      	SearchRes_Action.compareDropDown("MTM","MTM",exclRowCnt,strSheetName);	    	
      	SearchRes_Action.ele_EditMethod.click();
      	
  		//Effective checkpoints
      	SearchRes_Action.ele_EditMethod.click();
      	if(!CommonUtils.isElementPresent(ele_MTMEffDate)){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] MTMPopUp->Effective object is NOT displayed", "", "MTM_Efective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] MTMPopUp->Effective Setting is NOT displayed,please check..!!");
  		}//End of if condition to check if DROPDOWN is displayed
      	else{
      		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] MTMPopUp->Effective object is displayed", "", "MTM_Efective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Passed] MTMPopUp->Effective object is displayed");
      	}
      	ele_MTMEffDate.click();    	
      	SearchRes_Action.compareDropDown("MTM","EFFECTIVE",exclRowCnt,strSheetName);	    	
      	SearchRes_Action.ele_EditMethod.click();
      	

      	
  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: verifyAcct_MTM_DropDowns>,please check..!!");
  	}//End of catch block
    }//End of <Method: verifyAcct_MTM_DropDowns>    
           
  //###################################################################################################################################################################  
  //Function name		: getExp_MTMPopUp_DropDowns(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_ActionMenu.java
  //Description 		: Function to verify the drop downs in MTM Pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public ArrayList<String> getExp_MTMPopUp_DropDowns(String strDropDown){		
  		ArrayList<String> arrExpDropDown = new ArrayList<String>();
  		switch(strDropDown.toUpperCase()){
  			case "MTM":
  				arrExpDropDown.addAll(Arrays.asList("On","Off"));
      			return arrExpDropDown;
  			case "EFFECTIVE":
  				arrExpDropDown.addAll(Arrays.asList("2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018")); //read only
      			return arrExpDropDown;
  			default:
      			return null;
  		}//End of Switch case
    
    }//End of <Method: getExp_MTMPopUp_DropDowns>

//###################################################################################################################################################################
}//End of <Class: Searchres_MTM_Objs>    

