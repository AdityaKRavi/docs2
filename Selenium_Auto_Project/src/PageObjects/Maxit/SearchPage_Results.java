package PageObjects.Maxit;


import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class SearchPage_Results {
	//static WebDriver driver;
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	Reporting reporter = new Reporting();
	WebDriverWait wait;
	//Variables declaration:
    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
    public WebElement ele_ResTable_Header; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-body\"]")
	public WebElement ele_ResultsTable; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"ux-maximgb-tg-elbow-active ux-maximgb-tg-elbow-plus\"]")
    private List<WebElement> ele_LedgerClickPlus; 

    @FindBy(how=How.XPATH,using="//*[@class=\"ux-maximgb-tg-elbow-active ux-maximgb-tg-elbow-end-plus\"]")
    private  List<WebElement> ele_LedgerClickPlusEnd; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"ux-maximgb-tg-elbow-active ux-maximgb-tg-elbow-minus\"]")
    private List<WebElement> ele_LedgerClickMinus; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"ux-maximgb-tg-elbow-active ux-maximgb-tg-elbow-end-minus\"]")
    private List<WebElement> ele_LedgerClickEndMinus; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"ext-el-mask-msg x-mask-loading\"]")
    private List<WebElement> ele_Loading; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid-empty\"]")
    public WebElement ele_NoResults;
    
    @FindBy(how=How.XPATH,using="//*[@class=\"xtb-text\" and contains(text(),\"Records\")]")
    public WebElement ele_RecordsNum;
  
    
    
//*********************************************************************************************************************  
    //Performing Actions on objects in Search Page Results:
//*********************************************************************************************************************   
  //###################################################################################################################################################################  
  //Function name		: searchRes_NumOfRows_app(String strViewPage)
  //Class name			: SearchPage_Results
  //Description 		: Function to read number of records retrieved from search result
  //Parameters 			: Search Page name
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
  	public int searchRes_NumOfRows_app(String strViewPage){
  		try{  	
  			Thread.sleep(5000);
  			wait = new WebDriverWait(driver,30);
  			wait.until(ExpectedConditions.visibilityOf(ele_ResultsTable));
  			if(!CommonUtils.isElementPresent(ele_ResultsTable)){
  				System.out.println("Search_Results Table is not displayed,please check..!!");
  				return 0;	
  			}//End of IF Condition to check if ele_ResultsTable element exists
  			
  			List<WebElement> app_rows = ele_ResultsTable.findElements(By.tagName("tr"));
  			if(strViewPage.equalsIgnoreCase("LEDGER")){
  				
  				List<WebElement> clickPlus = ele_LedgerClickPlus;
  				List<WebElement> clickPlusEnd = ele_LedgerClickPlusEnd;
  				if(clickPlus.size()>0){
  	  				for (WebElement clickRow : clickPlus) {
  	  					clickRow.click();  	  				
  	  					wait.until(ExpectedConditions.visibilityOfAllElements(ele_LedgerClickMinus));  	  					
  	  				}//End of FOR loop to click on PLUS in Ledger page Application  					
  				}
  				if(clickPlusEnd.size()>0){
  	  				for (WebElement clickRowEnd : clickPlusEnd) {
  	  					clickRowEnd.click();
  	  					wait.until(ExpectedConditions.visibilityOfAllElements(ele_LedgerClickEndMinus));  	 
  	  				}//End of FOR loop to click on PLUS in Ledger page Application 					
  				}  				
	  			//System.out.println("Total number of rows="+app_rows.size()); 			
  			
  			}//End of IF Condition to check if search page=LEDGER
  			Thread.sleep(5000);
  			return app_rows.size();  			
  			
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_Results.searchRes_NumOfRows_app>: Search Results Table is not displayed in UI..!!!");
  			e.printStackTrace();
  			return 0;
  		}
  	}//End of searchRes_NumOfRows_app Method	

 
//###################################################################################################################################################################  
//Function name			: searchRes_GetColIndexByColName(String strColName)
//Class name			: SearchPage_Results
//Description 			: Function to read number of records retrieved from search result
//Parameters 			: driver object
//Assumption			: None
//Developer				: Kavitha Golla
//###################################################################################################################################################################		
	public int searchRes_GetColIndexByColName(String strColName){
  		try{  			
  			if(!CommonUtils.isElementPresent(ele_ResTable_Header)){
  				System.out.println("Search_Results Table Header is not displayed,please check..!!");
  				return 0;	
  			}//End of IF Condition to check if ele_ResTable_Header element exists
  			
  			List<WebElement> getCols = ele_ResTable_Header.findElements(By.tagName("td"));
  			//System.out.println("<Class:SearchPage_Results><Method:searchRes_GetColIndexByColName>: Total number of columns in Results table"+getCols.size());
  			
  			int iColLoop = 0;
  			for (WebElement eachCol : getCols) {  		
  				//System.out.println("iColLoop column number is : "+iColLoop);
  				//if (eachCol.getText().equalsIgnoreCase(strColName)){
  				if (eachCol.getAttribute("textContent").trim().equalsIgnoreCase(strColName)){
  					//String strColClass = eachCol.getAttribute("class").trim();
  					//Integer colIndex = Integer.parseInt(StringUtils.substringAfterLast(strColClass, "-"));  					
  					Integer colIndex = iColLoop;
  					//System.out.println("Column Name:"+eachCol.getAttribute("textContent").trim()+" and class:"+eachCol.getAttribute("class")+"and colIndex:"+colIndex);  					
  					return colIndex;
  				}//End of IF condition to check column name 
  				iColLoop++;
  			}//End of FOR loop to get Column Index
  			
  			System.out.println("<SearchPage_Results><Method:searchRes_GetColIndexByColName>: Search Results Table HEADER, Expected Column Name:"+strColName+" is not displayed in UI..!!!");
  			return -1;
  		}//End of Try block
  		catch (NumberFormatException e) {
  			System.out.println("NumberFormatException in <SearchPage_Results><Method:searchRes_GetColIndexByColName>: Column Index for column name: "+strColName+" is not Integer");
  			return -1;
  		}
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_Results><Method:searchRes_GetColIndexByColName>: Search Results Table HEADER is not displayed in UI..!!!");
  			return -1;
  		}  		
		
	}//End of searchRes_GetColIndexByColName method
  	
  //###################################################################################################################################################################  
  //Function name		: searchRes_getColValue(int rowNumber,int colIndex)
  //Class name			: SearchPage_Results
  //Description 		: Function to get column value for a given column index
  //Parameters 			: 
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
	public String searchRes_getColValue(int rowNumber,int colIndex){
  		try{  			
  			if(!CommonUtils.isElementPresent(ele_ResultsTable)){
  				System.out.println("Search_Results Table is not displayed,please check..!!");
  				return null;	
  			}//End of IF Condition to check if ele_ResultsTable element exists
  			
  			List<WebElement> app_rows = ele_ResultsTable.findElements(By.tagName("tr"));
  			List<WebElement> app_cols = app_rows.get(rowNumber).findElements(By.tagName("td"));
  			String strColValue = app_cols.get(colIndex).getAttribute("textContent");
  			System.out.println("Column value is :"+strColValue);
  			if(strColValue.equals("\n\t\t\t\t-\n\t\t\t\t\n\t\t\t\t\tG/L not applicable to Transfer Events\n\t\t\t\t\t\t\n\t\t\t")){
  				strColValue = "-G/L not applicable to Transfer Events ";
  			}
  			
  			
  			return strColValue;
  			
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_Results.searchRes_getColValue>: Search Results Table is not displayed in UI..!!!");
  			return "";
  		}
	}// End of searchRes_getColValue method.
	
  //###################################################################################################################################################################  
  //Function name		: searchRes_getCellData(int rowNumber,String colName)
  //Class name			: SearchPage_Results
  //Description 		: Function to get cell data from Search results based on Row# and column name
  //Parameters 			: rowNumber
  //					: Column Name	
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
	public String searchRes_getCellData(int rowNumber,String colName){
  		try{  			
  			if(!CommonUtils.isElementPresent(ele_ResultsTable)){
  				System.out.println("Search_Results Table is not displayed,please check..!!");
  				return null;	
  			}//End of IF Condition to check if ele_ResultsTable element exists
  			
  			List<WebElement> app_rows = ele_ResultsTable.findElements(By.tagName("tr"));
  			List<WebElement> app_cols = app_rows.get(rowNumber).findElements(By.tagName("td"));
  			int colIndex = this.searchRes_GetColIndexByColName(colName);
  			String strColValue = app_cols.get(colIndex).getAttribute("textContent");
  			if(strColValue.equals("\n\t\t\t\t-\n\t\t\t\t\n\t\t\t\t\tG/L not applicable to Transfer Events\n\t\t\t\t\t\t\n\t\t\t")){
  				strColValue = "-G/L not applicable to Transfer Events ";
  			}
  			return strColValue;
  			
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_Results.searchRes_getCellData>: Search Results Table is not displayed in UI..!!!");
  			return "";
  		}
	}// End of searchRes_getCellData method.
//###################################################################################################################################################################  
//Function name			: searchRes_DragDropCol(String strColName)
//Class name			: SearchPage_Results
//Description 			: Function to drag & drop a column from search result table
//Parameters 			: driver object
//Assumption			: None
//Developer				: Kavitha Golla
//###################################################################################################################################################################		
	public WebElement searchRes_DragDropCol(String strColName){
  		try{  			
  			if(!CommonUtils.isElementPresent(ele_ResTable_Header)){
  				System.out.println("Search_Results Table Header is not displayed,please check..!!");
  				return null;	
  			}//End of IF Condition to check if ele_ResTable_Header element exists
  			
  			List<WebElement> getCols = ele_ResTable_Header.findElements(By.tagName("td"));
  			//System.out.println("<Class:SearchPage_Results><Method:searchRes_GetColIndexByColName>: Total number of columns in Results table"+getCols.size());
  			
  			//int iColLoop = 0;
  			for (WebElement eachCol : getCols) {  		
  				//System.out.println("iColLoop column number is : "+iColLoop);
  				//if (eachCol.getText().equalsIgnoreCase(strColName)){
  				if (eachCol.getAttribute("textContent").trim().equalsIgnoreCase(strColName)){ 					
  					return eachCol;
  				}//End of IF condition to check column name 
  				//iColLoop++;
  			}//End of FOR loop to get Column Index
  			
  			System.out.println("<SearchPage_Results><Method:searchRes_GetColIndexByColName>: Search Results Table HEADER, Expected Column Name:"+strColName+" is not displayed in UI..!!!");
  			return null;
  		}//End of Try block

  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_Results><Method:searchRes_GetColIndexByColName>: Search Results Table HEADER is not displayed in UI..!!!");
  			return null;
  		}  		
		
	}//End of searchRes_DragDropCol method	
	
	
	
  //###################################################################################################################################################################  
  //Function name		: searchRes_NoResultsReturned(String strViewPage)
  //Class name			: SearchPage_Results
  //Description 		: Function to check if No Results returned
  //Parameters 			: Search Page name
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
  	public String searchRes_NoResultsReturned(String strViewPage){
  		try{  	
  			String strResValue;  				
  			if(CommonUtils.isElementPresent(ele_NoResults)){
  				strResValue = ele_NoResults.getAttribute("textContent");
  				return 	strResValue;
  			}//End of IF Condition to check if ele_ResultsTable element exists
  			System.out.println("<Class: SearchPage_Results><Method: searchRes_NoResultsReturned>: No Results found checkpoint fail.");
  			strResValue = "No Results found checkpoint fail";
  			return strResValue;  			
  			
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_Results.searchRes_NoResultsReturned>: Search Results Table 'No Results Found' is not displayed in UI..!!!");
  			e.printStackTrace();
  			return null;
  		}
  	}//End of searchRes_NoResultsReturned Method		
  	
    //###################################################################################################################################################################  
    //Function name			: get_TableHeader(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
    //Class name			: SearchPage_Results
    //Description 			: Function to return table header text
    //Parameters 			: Web table as a list object, the index of the table if multiple are displayed, excel row count and excel sheet for reporting.
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
    	public String get_TableHeader(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
			List<WebElement> tbl_Header= eleTableName;
    		String strHeaderRow="";
    		try{  	    		
    			if(eleTableName.isEmpty()){
    				System.out.println("<Class: SearchPage_Results><Method: get_TableHeader>: eleTableName parameter passed is empty, please check..!!");
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<SearchPage_Results.get_TableHeader>!", "", "eleTableName",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return "";
    			}
    			if((tbl_Header.size()>0) && (tbl_Header.size()>=tbl_index)){
    				List<WebElement> getCols = tbl_Header.get(tbl_index).findElements(By.tagName("td"));
          			for (WebElement eachCol : getCols) {  
          				strHeaderRow = strHeaderRow+eachCol.getAttribute("textContent").trim()+";";		
          			}//End of FOR loop to read header column names
          			return strHeaderRow;
    			}//End of IF condition to check if table exists
    			else{
    				System.out.println("<Class: SearchPage_Results><Method: get_TableHeader>: Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index);
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index+",please check method<SearchPage_Results.get_TableHeader>!", "", "Tble_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return "";
    			}//End of IF condition to check if table exists
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <SearchPage_Results.get_TableHeader>: Table Header size in the browser is not as expected..!!!");
    			e.printStackTrace();
    			return null;
    		}
    	}//End of get_TableHeader Method	
  	
	  //###################################################################################################################################################################  
	  //Function name		: get_WebTableRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
	  //Class name			: SearchPage_Results
	  //Description 		: Function to total number of rows in the given webtable
	  //Parameters 			: Search Page name
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	  	public int get_WebTableRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
	  		try{  	
    			if(eleTableName.isEmpty()){
    				System.out.println("<Class: SearchPage_Results><Method: get_WebTableRowCount>: eleTableName parameter passed is empty, please check..!!");
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<SearchPage_Results.get_WebTableRowCount>!", "", "WebTableRowCount",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return 0;
    			}
	  			if(!((eleTableName.size()>0) && (eleTableName.size()>=tbl_index)) ){
    				System.out.println("<Class: SearchPage_Results><Method: get_WebTableRowCount>: Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index);
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index+",please check method<SearchPage_Results.get_WebTableRowCount>!", "", "WebTableRowCount",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	  				return 0;
	  			}//End of IF Condition to check if eleTableName element exists
	  			
	  			List<WebElement> app_rows = eleTableName.get(tbl_index-1).findElements(By.tagName("tr"));	  			
	  			return app_rows.size();  			
	  			
	  		}//End of Try block
	  		catch(Exception e){
	  			System.out.println("Exception in <SearchPage_Results.WebTableRowCount>,please check.!!");
	  			e.printStackTrace();
	  			return 0;
	  		}
	  	}//End of get_WebTableRowCount Method	    	
    	

	  	
	  //###################################################################################################################################################################  
	  //Function name		: SearchResult_Get_ClosingEvent_RowNo(int intTotRes_Rows,String strTxnAccType,int exclRowCnt,String strSheetName)
	  //Class name			: SearchPage_Results
	  //Description 		: Function to get first Closing Event row number from Search_results table
	  //Parameters 			: 
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	  	public int SearchResult_Get_ClosingEvent_RowNo(int intTotRes_Rows,String strTxnAccType,int exclRowCnt,String strSheetName){
	  		int intFirst_ClosingEvent_RowNo=0;
	  		try{  	
    			
    			for(int iLoop=0;iLoop<intTotRes_Rows;iLoop++){
    				//Check if account type is long or short and then compare close event:
    				if(strTxnAccType.equalsIgnoreCase("Long")){
    					if((this.searchRes_getCellData(iLoop, "Event").equalsIgnoreCase("Sell")) || (this.searchRes_getCellData(iLoop, "Event").equalsIgnoreCase("Sell To Close"))){
    						intFirst_ClosingEvent_RowNo = iLoop + 1;
    						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] First closing event(Sell/Sell To Close) transaction is found in Search_Results_Row="+intFirst_ClosingEvent_RowNo, "", "Res_GetClosingEvent_RowNum",  driver, "HORIZONTALLY", strSheetName, null);
    						break;
    					}
    				}//End of IF condition to check AccountType=Long
    				else{
    					if((this.searchRes_getCellData(iLoop, "Event").equalsIgnoreCase("Purchase")) || (this.searchRes_getCellData(iLoop, "Event").equalsIgnoreCase("Cover Short")) ||(this.searchRes_getCellData(iLoop, "Event").equalsIgnoreCase("Buy To Close")) ){
    						intFirst_ClosingEvent_RowNo = iLoop + 1;
    						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] First closing event(Purchase/Buy To Close/Cover Short) transaction is found in Search_Results_Row="+intFirst_ClosingEvent_RowNo, "", "Res_GetClosingEvent_RowNum",  driver, "HORIZONTALLY", strSheetName, null);
    						break;
    					}
    				}//End of Else condition to check AccountType
    			}//End of FOR loop

    			return intFirst_ClosingEvent_RowNo;
  			
	  			
	  		}//End of Try block
	  		catch(Exception e){
	  			System.out.println("Exception in <SearchPage_Results.SearchResult_Get_ClosingEvent_RowNo>,please check.!!");
	  			e.printStackTrace();
	  			return 0;
	  		}
	  	}//End of SearchResult_Get_ClosingEvent_RowNo Method.		  	
	
//###########################<< Actions on Objects >>######################################################################################################

	
	
	
//#################################################################################################################################
}//End of <Class: SearchPage_Results>
