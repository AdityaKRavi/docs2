package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.Maxit.SearchPageFns;

public class TxnError_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/TransactionError/enter.php']")
    private WebElement lnk_TxnError;
    @FindBy(how=How.NAME,using="errorType")
    public List<WebElement> lst_ErrType;
    @FindBy(how=How.NAME,using="dateFrom")
    public WebElement ele_DateFrom;
    @FindBy(how=How.NAME,using="dateTo")
    public WebElement ele_DateTo;
    @FindBy(how=How.XPATH,using="//*[@id=\"searchAccount\"]")
	public WebElement txtbx_Acct;
    @FindBy(how=How.ID,using="securityIDType")
    public WebElement lst_SecIDType;
    @FindBy(how=How.ID,using="securityID")
    public WebElement txtbx_SecurityID;
    @FindBy(how=How.XPATH,using="//*[@class=\"mediumButton\"]")
    public WebElement btn_Submit;
    @FindBy(how=How.NAME,using="recon")
    public List<WebElement> lst_Recon;
    @FindBy(how=How.NAME,using="errors")
    public List<WebElement> lst_Errors;
    
    @FindBy(how=How.ID,using="transactionErrorTable")
    public WebElement ele_ResTable;
    @FindBy(how=How.XPATH,using="//*[@id=\"transactionErrorTable\"]/thead")
    public List<WebElement> ele_ResTbl_Header;
    @FindBy(how=How.XPATH,using="//*[@id=\"transactionErrorTable\"]/tbody")
    public List<WebElement> ele_ResTbl_Body;
    @FindBy(how=How.XPATH,using="//*[@id=\"noData\"]")
    public WebElement ele_NoResults;
    
    
//*********************************************************************************************************************  
//Performing Actions on objects in Search Page:
   //*********************************************************************************************************************   
//###################################################################################################################################################################  
//Function name		: Navigate_TxnError(int exclRowCnt,String strSheetName)
//Class name		: TxnError_Objs
//Description 		: Function to Navigate to "Transaction Error" Page
//Parameters 		: None
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
	public boolean Navigate_TxnError(int exclRowCnt,String strSheetName){
		try{  			
			if(CommonUtils.isElementPresent(lnk_TxnError)){
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Navigating to 'Transaction Error' page", "", "TxnError_Page",  driver, "HORIZONTALLY", strSheetName, null);
				lnk_TxnError.click();
				
				//Verify page heading header:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_PageHeader)){
					String strAct_Header = CBManager_Objs.ele_PageHeader.getAttribute("textContent");
					if(strAct_Header.contains("Transaction Error")){
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Transaction Error page header/title is as expected, Expected Header:Transaction Error", "", "TxnError_Header",  driver, "HORIZONTALLY", strSheetName, null);
						return true;
					}
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error page header/title is not as expected, Actual header:"+strAct_Header+" and Expected Header:Transaction Error", "", "TxnError_Header",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}
				}//End of if condition to verify page heading
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error page header/title object is not displayed, please check..!", "", "TxnError_Header",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}
			}//End of IF condition to check if Transaction Error link exists in left navigation panel.
			else{
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error link is not displayed in left navigation panel of page, please check..!", "", "TxnError_Header",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}
					
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <TxnError_Objs.Navigate_TxnError>: 'Transaction Error Link is not displayed in UI..!!!");
			reporter.reportStep(exclRowCnt, "Failed", "TxnError_Exception in <TxnError_Objs.Navigate_TxnError> Please Check..!!", "", "TxnError_Exception",  driver, "HORIZONTALLY", strSheetName, null);
			return false;
		}
	}//End of Navigate_TxnError Method	
    
    //###################################################################################################################################################################  
    //Function name		: TxnError_Errors(int exclRowCnt,String strSheetName)
    //Class name		: TxnError_Objs
    //Description 		: Function to verify if there are any error messages in Transaction Error page
    //Parameters 		: 
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean TxnError_Errors(String strTabName,int exclRowCnt,String strSheetName){
    		try{
				//Check if there are any error messages:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in Transaction Error page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TransactionError_ErrorMessage:"+strErMsg, "", "TxnError_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.
				return true;
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <TxnError_Objs.TxnError_Errors>: 'Txn Error' verifying for error messages.");
    			return false;
    		}//End of catch block
    	}//End of <Method: TxnError_Errors>    

    //###################################################################################################################################################################  
    //Function name		: TxnError_Search(String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName)
    //Class name		: NoCostBasis_Objs
    //Description 		: Function to perform BLANK search in NCB page 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean TxnError_Search(String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName){
    		LocalDateTime strStartWaitTime,strEndWaitTime;
    		String strSearchValues;
    		try{
    			boolean blnSecTypeCheck = false;
    			
    			strSearchValues = "[Transaction Error;Acct:"+strAcct+";"+strSecType+";"+strSecValue+";FromDate:"+strFromDate+";ToDate:"+strToDate+"]";
    			
    			txtbx_Acct.sendKeys(strAcct);
    			List<WebElement> SecType_List = lst_SecIDType.findElements(By.tagName("option"));
    			for (WebElement option : SecType_List) {
    				if(option.getText().equals(strSecType)){
    					option.click();
    					blnSecTypeCheck=true;
    					break;
    				}
    			}//End of FOR loop
    			if(!blnSecTypeCheck){
    				for (WebElement option : SecType_List) {
    					if(option.getText().equals("Security No")){
    						System.out.println("Selecting default SecIDType as Security No");
    						option.click();
    						blnSecTypeCheck=true;
    						break;
    					}
    				}//End of FOR loop
    			}//End of IF condition to select default SecIDTpe= Security ID

    			txtbx_SecurityID.sendKeys(strSecValue);
    			
    			//From & To Date:
    			ele_DateFrom.sendKeys(strFromDate);
    			ele_DateTo.sendKeys(strToDate);
    			
    			btn_Submit.click();
    			
    			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
    			strStartWaitTime = LocalDateTime.now();
    			try{
    				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
    				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    										wait.withTimeout(35,TimeUnit.SECONDS)
    										.pollingEvery(2, TimeUnit.SECONDS)				 
    										.ignoring(NoSuchElementException.class)
    										.until(ExpectedConditions.visibilityOf(ele_ResTable));
    			}//End of try block for FluentWait
    			
    			catch(org.openqa.selenium.TimeoutException e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				
    				//Check if page is loading:			
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading)){
    					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error Search after input"+strSearchValues+":Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of IF condition to check for loading object.

    				//Checkpoint: Check for Errors:
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.				
    			}//End of Catch block for FluentWait
    			catch(Exception e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				return false;
    			}//End of catch block with generic exception for Fluent Wait 
    			
    			//Checkpoint: Check for Errors:
    			if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    				String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    				System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    				return false;
    			}//End of if condition to to see if there are errors.
    			
    			//Check for Results header
    			if(CommonUtils.isElementPresent(ele_ResTable)){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Checkpoint : NCB Page Navigation - [Passed]");
    				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Transaction Error Search After Input "+strSearchValues+":Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return true;
    			}//End of IF condition to check for Results table header.
    			
    			System.out.println("Checkpoint : Transaction Error Search After Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error Search after input:"+strSearchValues+"Failed, Results header is not displayed", "", "NCBSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    			
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <TxnError_Search>: Transaction Error-> verifying header title.");
    			return false;
    		}//End of catch block
    	}//End of <Method: TxnError_Search>     	
    
    //###################################################################################################################################################################  
    //Function name		: TxnError_BlankSearch(int exclRowCnt,String strSheetName)
    //Class name		: NoCostBasis_Objs
    //Description 		: Function to perform BLANK search in Transaction Error page 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean TxnError_BlankSearch(int exclRowCnt,String strSheetName){
    		LocalDateTime strStartWaitTime,strEndWaitTime;
    		String strSearchValues;
    		try{
    			strSearchValues = "[Transaction Error;BlankSearch]";
    			btn_Submit.click();
    			
    			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
    			strStartWaitTime = LocalDateTime.now();
    			try{
    				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
    				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    										wait.withTimeout(35,TimeUnit.SECONDS)
    										.pollingEvery(2, TimeUnit.SECONDS)				 
    										.ignoring(NoSuchElementException.class)
    										.until(ExpectedConditions.visibilityOf(ele_ResTable));
    			}//End of try block for FluentWait
    			
    			catch(org.openqa.selenium.TimeoutException e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				
    				//Check if page is loading:			
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading)){
    					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error Search after input"+strSearchValues+":Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of IF condition to check for loading object.

    				//Checkpoint: Check for Errors:
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.				
    			}//End of Catch block for FluentWait
    			catch(Exception e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				return false;
    			}//End of catch block with generic exception for Fluent Wait 
    			
    			//Checkpoint: Check for Errors:
    			if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    				String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    				System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    				return false;
    			}//End of if condition to to see if there are errors.
    			
    			//Check for Results header
    			if(CommonUtils.isElementPresent(ele_ResTable)){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Checkpoint : NCB Page Navigation - [Passed]");
    				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Transaction Error Search After Input "+strSearchValues+":Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return true;
    			}//End of IF condition to check for Results table header.
    			
    			System.out.println("Checkpoint : Transaction Error Search After Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Transaction Error Search after input:"+strSearchValues+"Failed, Results header is not displayed", "", "NCBSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    			
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <TxnError_Search>: Transaction Error-> verifying header title.");
    			return false;
    		}//End of catch block
    	}//End of <Method: TxnError_BlankSearch>   	
    	
	//###################################################################################################################################################################  
	//Function name		: TxnError_DataSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFromDate,String strToDate,String strBlankSearch )
	//Class name		: NoCostBasis_Objs
	//Description 		: Function to list the methods for verifying No Cost Basis page when there is "Blanks Search" in NCB page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public  void TxnError_DataSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFromDate,String strToDate,String strBlankSearch ) throws Exception{
			NoCostBasis_Objs NCB_Objs = PageFactory.initElements(driver, NoCostBasis_Objs.class);
			
			String strSheetName = "SmokeData";
			String strSearchValues;
			boolean strRet;
			try{		
				strSearchValues = "[Transaction Error;Acct:"+strAcct+";"+strSecType+";"+strSecValue+";FromDate:"+strFromDate+";ToDate:"+strToDate+"]";
				strRet = this.TxnError_Search(strAcct, strSecType, strSecValue, strFromDate, strToDate, exclRowCnt, strSheetName);

				//Check for Transaction Error Search results
				if(strRet){					
					//Verify search results row number, if No Results returned fail the case.		
					int appRowsReturned = NCB_Objs.get_NCB_ResRowCount(this.ele_ResTbl_Body, 0, exclRowCnt, strSheetName);
					if(!(appRowsReturned>0)){
						if(NCB_Objs.NCB_NoResultsReturned().equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Transaction Error' Search using "+strSearchValues+" returned: 'No Resuls found'", "", "TxnError_Rows",  driver, "HORIZONTALLY", strSheetName, null);
							
							//call method to perform blank search in Search_NCB page:
							this.Search_TrxError_Flow(exclRowCnt,"Trx Error",To_Execute,strAcct,strSecType, strSecValue,strFromDate,strToDate,strBlankSearch );
							return;
						}
						System.out.println("Search Results for "+strSearchValues+" is not >0");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Transaction Error' Search Results rows not >0, Not as expected 'No results Found'", "", "TxnError_Rows",  driver, "HORIZONTALLY", strSheetName, null);
						//return;
					}//End of IF condition to check No of rows returned

					//call method to perform blank search in Search_NCB page:
					this.Search_TrxError_Flow(exclRowCnt,"Trx Error",To_Execute,strAcct,strSecType, strSecValue,strFromDate,strToDate,strBlankSearch );
					return;
					
				}//End of IF condition to check strRet of search
						
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Smoke Test Flow <Method: TxnError_DataSearch_Flow>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in TxnError_DataSearch_Flow ", "", "TxnError_DataSearch_Flow",  driver, "HORIZONTALLY", strSheetName, null);
				return;
			}//End of Catch block
		}//End of <Method: TxnError_DataSearch_Flow>
    	
	//###################################################################################################################################################################  
	//Function name		: TxnError_BlankSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFromDate,String strToDate,String strBlankSearch )
	//Class name		: TxnError__Objs
	//Description 		: Function to list the methods for verifying Transaction Error page when there is "Blanks Search" in NCB page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################			
		public  void TxnError_BlankSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFromDate,String strToDate,String strBlankSearch ) throws Exception{
			NoCostBasis_Objs NCB_Objs = PageFactory.initElements(driver, NoCostBasis_Objs.class);
			
			String strSheetName = "SmokeData";
			String strSearchValues;
			boolean strRet;
			String strTxnAcct;
			try{		
				strSearchValues = "[Transaction Error;Blank Search]";
				strRet = this.TxnError_BlankSearch( exclRowCnt, strSheetName);

				//Check for Transaction Error Search results
				if(strRet){					
					//Verify search results row number, if No Results returned fail the case.		
					int appRowsReturned = NCB_Objs.get_NCB_ResRowCount(this.ele_ResTbl_Body, 0, exclRowCnt, strSheetName);
					if(!(appRowsReturned>0)){
						if(NCB_Objs.NCB_NoResultsReturned().equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Warning] 'Transaction Error' Blank Search returned: 'No Resuls found'. \n", "", "TxnError_Rows",  driver, "HORIZONTALLY", strSheetName, null);
							
							//call method to perform blank search in Search_NCB page:
							this.Search_TrxError_Flow(exclRowCnt,"Trx Error",To_Execute,strAcct,strSecType, strSecValue,strFromDate,strToDate,strBlankSearch );
							return;
						}
						System.out.println("Search Results for "+strSearchValues+" is not >0");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Transaction Error' Blank Search Results rows not >0, Not as expected 'No results Found'", "", "TxnError_Rows",  driver, "HORIZONTALLY", strSheetName, null);
						return;
					}//End of IF condition to check No of rows returned
					
					else{
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Transaction Error' Blank Search returned "+appRowsReturned+" rows.\n", "", "TxnError_Rows",  driver, "HORIZONTALLY", strSheetName, null);
						strTxnAcct = this.TxnError_Res_getCellData(1, "Account");
						strBlankSearch = "N";
						//call method to perform search with account read from Transaction Error page in 'Search_TxnError' page:
						this.Search_TrxError_Flow(exclRowCnt,"Trx Error",To_Execute,strTxnAcct,strSecType, strSecValue,strFromDate,strToDate,strBlankSearch );
						return;
					}//End of ELSE condition for appRowsReturned>0
					
				}//End of IF condition to check strRet of search
						
			}//End of try block
			catch(Exception e){
				System.out.println("Exception in Smoke Test Flow <Method: TxnError_BlankSearch_Flow>,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Exception in TxnError_BlankSearch_Flow ", "", "TxnError_DataSearch_Flow",  driver, "HORIZONTALLY", strSheetName, null);
				return;
			}//End of Catch block
		}//End of <Method: TxnError_BlankSearch_Flow>    	
		
    //###################################################################################################################################################################  
    //Function name			: TxnError_Res_getCellData(int rowNumber,String colName)
    //Class name			: TxnError_Objs
    //Description 			: Function to get cell data from 'Transaction Error' Search results based on Row# and column name
    //Parameters 			: rowNumber
    //						: Column Name	
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
  	public String TxnError_Res_getCellData(int rowNumber,String colName){
    		try{  			
    			if(!CommonUtils.isElementPresent(ele_ResTable)){
    				System.out.println("Transaction Error_Results Table is not displayed,please check..!!");
    				return null;	
    			}//End of IF Condition to check if ele_ResTable element exists
    			
    			int colIndex = this.TXNRes_GetColIndexByColName(colName);
    			List<WebElement> app_rows = ele_ResTbl_Body.get(0).findElements(By.tagName("tr"));
    			List<WebElement> app_cols = app_rows.get(rowNumber).findElements(By.tagName("td"));    			
    			String strColValue = app_cols.get(colIndex).getAttribute("textContent");
    			return strColValue;
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs.NCBRes_getCellData>: Search Results Table is not displayed in UI..!!!");
    			return "";
    		}
  	}// End of <Method: TxnError_Res_getCellData>		
		
    //###################################################################################################################################################################  
    //Function name				: TXNRes_GetColIndexByColName(String strColName)
    //Class name				: TxnError_Objs
    //Description 				: Function to read number of records retrieved from Transaction Error search result
    //Parameters 				: 
    //Assumption				: None
    //Developer					: Kavitha Golla
    //###################################################################################################################################################################		
    	public int TXNRes_GetColIndexByColName(String strColName){
      		try{  			
      			if(!(CommonUtils.isElementPresent(ele_ResTable) && (ele_ResTbl_Header.size()>0))){
      				System.out.println("Txn Error Page Search_Results Table Header is not displayed,please check..!!");
      				return 0;	
      			}//End of IF Condition to check if ele_ResTbl_Header element exists
      			
      			List<WebElement> getCols = ele_ResTbl_Header.get(0).findElements(By.tagName("th"));
      			
      			int iColLoop = 0;
      			for (WebElement eachCol : getCols) {  		
      				if (eachCol.getAttribute("textContent").trim().equalsIgnoreCase(strColName)){ 					
      					Integer colIndex = iColLoop;
      					return colIndex;
      				}//End of IF condition to check column name 
      				iColLoop++;
      			}//End of FOR loop to get Column Index
      			
      			System.out.println("<TxnError_Objs><TXNRes_GetColIndexByColName>: 'Transaction Error' Search Results Table HEADER, Expected Column Name:"+strColName+" is not displayed in UI..!!!");
      			return -1;
      		}//End of Try block
      		catch (NumberFormatException e) {
      			System.out.println("NumberFormatException in <TxnError_Objs><TXNRes_GetColIndexByColName>: Column Index for column name: "+strColName+" is not Integer");
      			return -1;
      		}
      		catch(Exception e){
      			System.out.println("Exception in <TxnError_Objs><TXNRes_GetColIndexByColName>: Search Results Table HEADER is not displayed in UI..!!!");
      			return -1;
      		}  		
    		
    	}//End of <Method: TXNRes_GetColIndexByColName> 	
  	
//###################################################################################################################################################################  
//Function name		: Search_TrxError_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
//Class name		: TxnError_Objs
//Description 		: Function to list the methods for verifying Search_AuditTrail_Flow page
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public  void Search_TrxError_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strFromDate,String strToDate,String strBlankSearch ) throws Exception{
		SearchPageFns objSearchPageFns = PageFactory.initElements(driver, SearchPageFns.class);
		SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			if(strBlankSearch.equalsIgnoreCase("Y")) {
				strSearchValues = "["+strViewPage+";Blank Search]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				reporter.reportStep(exclRowCnt, "Passed", "\n"+"Checkpoint :[Warning] Navigating to 'Search_TrnError' page to perform 'Blank Search'.", "", "Trn_BlankSearch",  driver, "HORIZONTALLY", strSheetName, null);
				strRet = objSearchPageFns.Search_Input_Blank(strViewPage , exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			else{
				strSearchValues = "["+strViewPage+";"+strAcct+";"+strSecType+";"+strSecValue+"]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				reporter.reportStep(exclRowCnt, "Passed", "\n"+"Checkpoint :[Passed] Navigating to 'Search_TrnError' page to perform 'Search using search values:"+strSearchValues+"'.", "", "Trn_AcctSearch",  driver, "HORIZONTALLY", strSheetName, null);				
				strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			
				if(strRet){
					//Verify search results row number, if No Results returned fail the case.		
					int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
					if(!(appRowsReturned>0)){
						if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Failed", "\nCheckpoint :[Failed] 'Search_TrxError' with data:"+strSearchValues+" returned - 'No Resuls found'.", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return;
						}
						System.out.println("Search Results for "+strSearchValues+" is not >0");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Search_TrxError' Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}//End of IF condition to check No of rows returned
					else{
						System.out.println("Search Results for "+strSearchValues+" is >0");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Search_TrxError' Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;	
					}


				}//End of IF condition to check Search_Input Checkpoint
					
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_TrxError_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_TrxError_Flow ", "", "Search_TxnError_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_TrxError_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_TrxError_Flow>			
		
}//End of <Class: TxnError_Objs>
