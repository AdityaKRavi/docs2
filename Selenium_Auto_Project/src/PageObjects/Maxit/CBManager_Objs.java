package PageObjects.Maxit;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class CBManager_Objs {

	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	//HistCorrections_Objs HistCorrec_Objs = PageFactory.initElements(driver, HistCorrections_Objs.class);
	
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/CostBasisImports/Report/enter.php']")
    private WebElement lnk_CBManager;
    @FindBy(how=How.XPATH,using="//*[@id=\"pageTitle\"]/h1")
    public WebElement ele_PageHeader;
    @FindBy(how=How.XPATH,using="//*[@id=\"searchAccount\"]")
	public WebElement txtbx_Acct;
    @FindBy(how=How.ID,using="securityIDType")
    public WebElement lst_SecIDType;
    @FindBy(how=How.ID,using="securityID")
    public WebElement txtbx_SecurityID;
    
   
    
    
    @FindBy(how=How.XPATH,using="//*[@class=\"mediumButton\"]")
    public WebElement btn_Submit;
    @FindBy(how=How.XPATH,using="//*[@class=\"ext-el-mask-msg x-mask-loading\"]")
    public WebElement ele_Loading;
    @FindBy(how=How.XPATH,using="//*[@class=\"errors\"]")
    public WebElement ele_CBM_Errors; 
    
    
  //*********************************************************************************************************************  
    //Performing Actions on objects in Search Page:
  //*********************************************************************************************************************  
    //###################################################################################################################################################################  
    //Function name		: Navigate_CBManager(String strTabName,int exclRowCnt,String strSheetName)
    //Class name		: Dashboard_Objs
    //Description 		: Function to Navigate to "Dashboard" Page
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
    	public boolean Navigate_CBManager(String strTabName,int exclRowCnt,String strSheetName,String strPageExists){
    		WebElement ele_TabName;
    		try{  			
    			if(CommonUtils.isElementPresent(lnk_CBManager)){
    				lnk_CBManager.click();
    				ele_TabName = getCBManager_TabElement(strTabName);
    				if(ele_TabName==null){
    	    			System.out.println("<CBManager_Objs.Navigate_CBManager> : TabName parameter is incorrect, please check");
    	    			reporter.reportStep(exclRowCnt, "Failed", "CB Manager_Cannot navigate to given tab, <CBManager_Objs.Navigate_CBManager> : TabName parameter["+strTabName+"] is incorrect, please check..!!", "", "CBMngr_IncorrectTabName",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    			return false;
    				}
    				
    				//Verify if give tab exists for this client from input test data
    				if(strPageExists.equalsIgnoreCase("Y")){
    	    			if(CommonUtils.isElementPresent(ele_TabName)){
    	    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager_"+strTabName+" tab is displayed.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    				ele_TabName.click();
    	    				//Verify for error messages & Header
    	    				if(!this.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;
    	    				if(!this.CBManager_Header(strTabName,exclRowCnt,strSheetName)) return false;    	    				   	    
    	    			}
    	    			else{
    	    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+" tab is expected, but NOT displayed in UI.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    				return false;
    	    			}//End of else condition to check if strTabName tab is displayed or not.
    	    				
    				}//End of IF condition to check if PageExists for given client
    				
    				else{
    					if(!CommonUtils.isElementPresent(ele_TabName)){
    						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager_"+strTabName+" tab is NOT displayed as expected.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    						return false;
    					}
    					else{
    	    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+" tab is NOT expected for this client, but displayed in UI.", "", strTabName,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    				return false;
    					}
    				}//End of Else condition to check if PageExists for given client
    				
    				
    				return true;	
    			}//End of IF Condition to check if CB Manager element exists
								
    			return false;	
    		}//End of Try block
    		catch(Exception e){
    			System.out.println(e.getMessage());
    			System.out.println("Exception in <CBManager_Objs.Navigate_CBManager>: CB Manager Link is not displayed in UI..!!!");
    			reporter.reportStep(exclRowCnt, "Failed", "CB Manager_Exception in<CBManager_Objs.Navigate_CBManager> Please Check..!!", "", "CBManager_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    		}
    	}//End of Navigate_CBManager Method	

    	
    //###################################################################################################################################################################  
    //Function name		: CBManager_Errors(int exclRowCnt,String strSheetName)
    //Class name		: CBManager_Objs
    //Description 		: Function to verify if there are any error messages
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean CBManager_Errors(String strTabName,int exclRowCnt,String strSheetName){
    		try{
				//Check if there are any error messages:
				if(CommonUtils.isElementPresent(ele_CBM_Errors)){
					String strErMsg = ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in CB Manager->"+strTabName+", Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+"_ErrorMessage:"+strErMsg, "", "CBManager_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.
				return true;
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <CBManager_Objs.CBManager_Errors>: CBManger->"+strTabName+" verifying for error messages.");
    			return false;
    		}//End of catch block
    	}//End of <Method: CBManager_Errors>

    //###################################################################################################################################################################  
    //Function name		: CBManager_Header(String strTabName,int exclRowCnt,String strSheetName)
    //Class name		: CBManager_Objs
    //Description 		: Function to verify Header in the corresponding tab of CB Manager
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean CBManager_Header(String strTabName,int exclRowCnt,String strSheetName){
    		String strExp_Header = this.CBManager_ExpectedPageTitle(strTabName);
    		try{
				//Check if the header is correct
				if(CommonUtils.isElementPresent(ele_PageHeader)){
					String strAct_Header = ele_PageHeader.getAttribute("textContent");
					if(strAct_Header.contains(strExp_Header)){
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager_"+strTabName+" page header/title is as expected, Expected Header:"+strExp_Header, "", "CBManager_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return true;
					}
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+" page header/title is not as expected, Actual header:"+strAct_Header+" and Expected Header:"+strExp_Header, "", "CBManager_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return false;
					}
				}//End of if condition to to see if there are errors.
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+" page header/title object is not displayed, please check..!", "", "CBManager_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <CBManager_Objs.CBManager_Header>: CBManger->"+strTabName+" verifying page header title.");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager_"+strTabName+":Exception in <CBManager_Objs.CBManager_Header>, while verifying page title/Header. Please Check..!!", "", "CBManager_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    		}//End of catch block
    	}//End of <Method: CBManager_Header>
    
    //###################################################################################################################################################################  
    //Function name		: CBManager_Search(String strTabName,int exclRowCnt,String strSheetName,acct , )
    //Class name		: CBManager_Objs
    //Description 		: Function to perform search based on input data under given CB Manager tab 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean CBManager_Search(String strTabName,String strBlankSearch,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName){
    		HistCorrections_Objs HistCorrec_Objs = PageFactory.initElements(driver, HistCorrections_Objs.class);
    		try{
    			boolean blnSecTypeCheck = false;
    			txtbx_Acct.sendKeys(strAcct);
    			List<WebElement> SecType_List = lst_SecIDType.findElements(By.tagName("option"));
    			for (WebElement option : SecType_List) {
    				if(option.getText().equals(strSecType)){
    					option.click();
    					blnSecTypeCheck=true;
    					break;
    				}
    			}//End of FOR loop
    			if(!blnSecTypeCheck){
    				for (WebElement option : SecType_List) {
    					if(option.getText().equals("Security No")){
    						System.out.println("Selecting default SecIDType as Security No");
    						option.click();
    						blnSecTypeCheck=true;
    						break;
    					}
    				}//End of FOR loop
    			}//End of IF condition to select default SecIDTpe= Security ID

    			txtbx_SecurityID.sendKeys(strSecValue);
    			
    			//Based on individual tab other fields change:  
    			
    			
    			
    			Thread.sleep(5000);			
    			btn_Submit.click();
    			Thread.sleep(5500);
    			
    			//Checkpoint: Check for Errors:
    			if(!this.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;
    			
    			//Check for Results table header
    			if(CommonUtils.isElementPresent(HistCorrec_Objs.ele_ResTble_Header)){
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Search After Input data insert:Passed", "", strTabName+"_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return true;
    			}//End of IF condition to check for Results table .			
    			
    			//Check if page is loading:			
    			if(CommonUtils.isElementPresent(ele_Loading)){
    				System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return false;
    			}//End of IF condition to check for Results table header.
    			
    			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Search after input:Failed, Results header is not displayed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    			
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <CBManager_Objs.CBManager_Search>: CBManger->"+strTabName+" verifying header title.");
    			return false;
    		}//End of catch block
    	}//End of <Method: CBManager_Search>    	
    	
    	
    	
    //###################################################################################################################################################################  
    //Function name		: CBManager_Header(String strTabName)
    //Class name		: CBManager_Objs
    //Description 		: Function to verify Header in the corresponding tab of CB Manager
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public String CBManager_ExpectedPageTitle(String strTabName){
    			switch(strTabName.toUpperCase()){
    			case "HIST_CORRECTION":
    				return "Historical Corrections";
    			case "AVG_COST_GIFT":
    				return "Average Cost For Gift";
    			case "RBIUPLOAD":
    				return "Cost Basis Imports";
    			case "BASIS_UPLOAD":
    				return "Basis Upload";
    			case "RRERUN":
    				return "Rollback Rerun";
    			case "CB_UPLOAD":
    				return "Cost Basis Imports";
    			case "STEPUP":
    				return "Step Ups";
    			case "IMPORTS":
    				return "Imports";
    			case "EXPORTS":
    				return "Exports";		
    			default:
    				return null;
    					
    			}

    	}//End of <Method: CBManager_ExpectedPageTitle>
	
    //###################################################################################################################################################################  
    //Function name		: CBManager_Header(String strTabName)
    //Class name		: CBManager_Objs
    //Description 		: Function to verify Header in the corresponding tab of CB Manager
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public WebElement getCBManager_TabElement(String strTabName){
    		HistCorrections_Objs HistCorrec_Objs = PageFactory.initElements(driver, HistCorrections_Objs.class);
    		StepUp_Objs StepUp_Objs = PageFactory.initElements(driver, StepUp_Objs.class);
    		Imports_Objs Imports_Objs = PageFactory.initElements(driver, Imports_Objs.class);
    		Exports_Objs Exports_Objs = PageFactory.initElements(driver, Exports_Objs.class);
    		AvgGiftCost_Objs AvgGift_Objs = PageFactory.initElements(driver, AvgGiftCost_Objs.class);
    		RollBackRerun_Objs RollbackRerun_Objs = PageFactory.initElements(driver, RollBackRerun_Objs.class);
    		RBIUpload_Objs RBIupload_Objs = PageFactory.initElements(driver, RBIUpload_Objs.class);
    		
    			switch(strTabName.toUpperCase()){
    			case "HIST_CORRECTION":
    				return HistCorrec_Objs.lnk_HistCorrec;
    			case "AVG_COST_GIFT":
    				return AvgGift_Objs.lnk_AvgGiftCost;
    			case "RBIUPLOAD":
    				return RBIupload_Objs.lnk_RBIUpload;
    			case "BASIS_UPLOAD":
    				return RBIupload_Objs.lnk_BasisUpload;
    			case "RRERUN":
    				return RollbackRerun_Objs.lnk_RollbackRerun;
    			case "CB_UPLOAD":
    				return RBIupload_Objs.lnk_CB_Upload;
    			case "STEPUP":
    				return StepUp_Objs.lnk_StepUp;
    			case "IMPORTS":
    				return Imports_Objs.lnk_Imports;
    			case "EXPORTS":
    				return Exports_Objs.lnk_Exports;	
    			default:
    				return null;
    					
    			}

    	}//End of <Method: getCBManager_TabElement>    	
    
    	//###################################################################################################################################################################  
    	//Function name		: VerifyExportToExcelLink(WebElement element, int exclRowCnt,String strSheetName)
    	//Class name		: Exports_Objs
    	//Description 		: Function to verify the row counts and the Export to Excel Link in Web page. 
    	//Parameters 		: None
    	//Assumption		: None
    	//Developer			: Dhanasekar Kumar
    	//###################################################################################################################################################################

    	public boolean VerifyExportToExcelLink(WebElement element, int exclRowCnt,String strSheetName, String className){
    		try
    		{
    			//Check if the Export to Excel Link  is displayed or hidden
    			if((CommonUtils.isElementPresent(element))){
    				System.out.println(" Checkpoint :[Passed] Export to Excel Link is available in "+ className + " Page.  " +element.getText());
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] " +element.getText() + " :: i.e Export to Excel Link is available in"+ className +" Page. " , "", "VerifyExportToExcelLink",  driver, "HORIZONTALLY", strSheetName, null);
    				return true;
    			}
    		}// End of Try block
    		catch(Exception e)
    		{
    			System.err.println("Exception in <CBManager_Objs.VerifyExportToExcelLink>: Export to Excel Link-> is not available. Please check !!. "+className);
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exception in <CBManager_Objs.VerifyExportToExcelLink>: Export to Excel Link-> is not available. Please check !!. "+className, "", "VerifyExportToExcelLink",  driver, "HORIZONTALLY", strSheetName, null);
    		} //End of Catch
    		return false;
    	}//End of <Method: VerifyExportToExcelLink>
    	
}//End of <class: CBManager_Objs>
