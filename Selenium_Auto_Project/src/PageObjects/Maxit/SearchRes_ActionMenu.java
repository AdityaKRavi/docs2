package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class SearchRes_ActionMenu {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
//	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
//	SearchRes_FIElection_Objs FI_Objs = PageFactory.initElements(driver, SearchRes_FIElection_Objs.class);
//	SearchRes_MTM_Objs MTM_Objs = PageFactory.initElements(driver, SearchRes_MTM_Objs.class);
	
	
    @FindBy(how=How.XPATH,using="//*[@class=\"ux-row-action-item icon-action \"]")
    private List<WebElement> ele_LeftAction;
    
    @FindBy(how=How.XPATH,using="//*[@class=\"ux-row-action-item icon-actionEnd \"]")
    private List<WebElement> ele_RightAction;
    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-menu-list\"]")
    private WebElement ele_ActionMenuBox;
    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-menu-list-item\"]")
	public List<WebElement> ele_ActionMenuList;     
    
//    //Account level CB Method:
//    @FindBy(how=How.XPATH,using="//*[@class=\"x-window-header-text\"]")
    @FindBy(how=How.XPATH,using="//*[@class=\"x-window-header x-unselectable x-window-draggable\"]")
    
	public WebElement ele_PopUpWinTitle; 
    @FindBy(how=How.XPATH,using="//*[@class=\"x-fieldset-header-text\"]")
	public WebElement ele_EditMethod; 
    @FindBy(how=How.XPATH,using="//*[@class=\"x-panel-header-text\"]")
	public WebElement ele_CurrentSelections; 

    
    //Current Selections:
    @FindBy(how=How.XPATH,using="//*[@id=\"historyGrid\"]//tr[@class=\"x-grid3-hd-row\"]")
	public WebElement tbl_EditHist_Header;    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
	public List<WebElement> tbl_CurrSelec_Header;
    
    @FindBy(how=How.XPATH,using="//*[@id=\"historyGrid\"]//*[@class=\"x-grid3-body\"]")
	public WebElement tbl_CurrSelec_Body;
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-body\"]")
	public List<WebElement> tbl_VSP_Body;
    
    //Save & Cancel buttons
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Save\")]")
	public WebElement btn_Save;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Cancel\")]")
	public WebElement btn_Cancel;  
    @FindBy(how=How.XPATH,using="//*[@class=\"x-tool x-tool-close\"]")
	public WebElement btn_XClose; 
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Back\")]")
	public WebElement btn_Back;

    
    @FindBy(how=How.XPATH,using="//*[contains(@class, \"x-combo-list\") and contains(@style, \"visibility: visible;\")]//*[contains(@class, \"x-combo-list-item\")]")
	public List<WebElement> ele_PopUpCombo;
    
    //Pop up errors:
    @FindBy(how=How.XPATH,using="//*[@id=\"main\" and contains(@class,\"errors svi-x\")]")
	public List<WebElement> ele_PopUpErrors; 
  
    @FindBy(how=How.XPATH,using="//*[@id=\"main\" and contains(@class,\"errors svi-x\")]")
	public WebElement ele_PopUpErrMsg; 
    
   
    
//*********************************************************************************************************************  
//Performing Actions on objects in Search Page Results Action Menu:
//*********************************************************************************************************************   
//Function name		:	searchRes_VerifyActionButtonExists()
    
    public boolean searchRes_VerifyActionButtonExists(String strWhichActionButton,int appRowNum){
    	try{   
    		List<WebElement> ele_ActionButton;
			if(strWhichActionButton.equalsIgnoreCase("rightaction")) ele_ActionButton = this.ele_RightAction;
			else ele_ActionButton = this.ele_LeftAction;
			
			if(ele_ActionButton.size()>0){
				if(!CommonUtils.isElementPresent(ele_ActionButton.get(appRowNum-1))){
	  				System.out.println("Checkpoint :[Failed] Search_Results Table "+strWhichActionButton+" Button on row "+appRowNum+" is NOT displayed,please check..!!");
	  				return false;
				}//End of if condition to check if Right Action button on specified row in UI is displayed.
				else System.out.println("Checkpoint :[Passed] Search_Results Table "+strWhichActionButton+" Button on row "+appRowNum+" is displayed.");

				ele_ActionButton.get(appRowNum-1).click();
				Thread.sleep(5000);
				if(!CommonUtils.isElementPresent(ele_ActionMenuBox)){
	  				System.out.println("Checkpoint :[Failed] Search_Results Table Action Menu List is NOT displayed,please check..!!");
	  				return false;						
				}//End of if condition to check if Action Menu List is displayed
				System.out.println("Checkpoint :[Passed] Search_Results Table Action Menu List is displayed");
				return true;
			}//End of if condition to check if Right Action button is displayed
			else{
  				System.out.println("Checkpoint :[Failed] Search_Results Table Action button is NOT displayed,please check..!!");
  				return false;
			}//End of else condition to check  if Action Button on right side is displayed
				
    	}//End of Try block
    	catch(Exception e){
    		System.out.println("Exception in <Class: SearchRes_ActionMenu ><Method: searchRes_VerifyActionButtonExists>Search_Results Table Action button on Left side is NOT displayed,please check..!!");
    		return false;    		
    	}//End of catch block
    	
    }//End of <Method: searchRes_VerifyActionButtonExists>
    
    private ArrayList<String> getExpectedActionMenuList(String strSearchPageName){
		ArrayList<String> expActionList = new ArrayList<String>();
    	switch(strSearchPageName.toUpperCase()){
    		case "ACCOUNT":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Trx Error","CB Method","Mark-To-Market","FI-Elections"));
    			return expActionList;
    		case "OPEN/CLOSED":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Unrealized","Realized","Open/Closed","Trx Error","CB Method","FI-Elections"));
    			return expActionList;
    		case "SECURITY XREF":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Unrealized","Realized","Open/Closed","Trx Error","CB Method","FI-Elections"));
    			return expActionList;
    		case "POS RECOM":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error"));
    			return expActionList;
    		case "TXN ERROR":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error"));
    			return expActionList;
    		case "RAW TRADES":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error","Details"));
    			return expActionList;
    		case "RAW TRADES_TLE":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error","Details","Tax Lot Edit"));
    			return expActionList;
    		case "LEDGER":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error"));
    			return expActionList;
    		case "UNREALIZED":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error","CB Method","FI-Elections"));
    			return expActionList;
    		case "REALIZED":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error","CB Method","FI-Elections"));
    			return expActionList;
    		case "NO COST BASIS":
    			expActionList.addAll(Arrays.asList("Ledger","Raw Trades","Trade Table","RAD Table","Seed Table","Consolidated Tablesbeta","Acct Sec Master","Unrealized","Realized","Open/Closed","Trx Error","Tax Lot Edit"));
    			return expActionList;
    		case "AUDIT TRAIL":
    			expActionList.addAll(Arrays.asList("Details"));
    			return expActionList;
    		default:
  				System.out.println("<Class: SearchRes_ActionMenu ><Method: getExpectedActionMenuList >strSearchPageName Parameter is incorrect,please check..!!");
  				return null;
    	}
    	
    	
    }//End of <Method: getExpectedActionMenuList>
    

    
    
    public boolean verifyActionMenuList(String strSearchPageName){
		List<String> actActionList = new ArrayList<String>();
		ArrayList<String> expActionList = this.getExpectedActionMenuList(strSearchPageName);
		
		if(this.ele_ActionMenuList.size()>0){	
			for (WebElement eachActionList : this.ele_ActionMenuList) { 
				actActionList.add(eachActionList.getAttribute("textContent").trim());
			}//End of for loop to read actual Action menu list
			if(!actActionList.toString().equals(expActionList.toString())){
				System.out.println("Checkpoint :[Failed] Verifying Action Menu list in "+strSearchPageName+" search Results.Expected Action menu List = "+expActionList.toString()+"; Actual Action menu List = "+actActionList.toString());
				return false;
			}//End of IF condition to check Expected Action Menu List & Actual Action Menu List
			System.out.println("Checkpoint :[Passed] Verifying Action Menu list in "+strSearchPageName+" search Results.Expected Action menu List = "+expActionList.toString()+"; Actual Action menu List = "+actActionList.toString());
			return true;
		}//End of IF condition to check Menu List object exists
		
		System.out.println("<Method: verifyActionMenuList>Checkpoint_Fail:Action Menu list object in "+strSearchPageName+" search Results is NOT displayed, please check...!!!");
		return false;
    }//End of <Method: verifyActionMenuList>
    
    public boolean verifyDesiredActionItem(String strDesiredPage){
		
		if(this.ele_ActionMenuList.size()>0){	
			for (WebElement eachActionList : this.ele_ActionMenuList) { 
				if(eachActionList.getAttribute("textContent").trim().equalsIgnoreCase(strDesiredPage)){
					System.out.println("Checkpoint :[Passed] Desired page "+strDesiredPage+" exists in the Action Menu.");
					return true;
				}
			}//End of for loop to read actual Action menu list

			System.err.println("Checkpoint :[Failed] Desired page "+strDesiredPage+" does NOT exists in the Action Menu, please check.!!");
			return false;
		}//End of IF condition to check Menu List object exists
		
		System.out.println("<Method: verifyActionMenuList>Checkpoint_Fail:Action Menu list object in "+strDesiredPage+" search Results is NOT displayed, please check...!!!");
		return false;
    }//End of <Method: verifyActionMenuList>
    
  //###################################################################################################################################################################  
  //Function name		: verifyPopUpExists(String strPopUpName)
  //Class name			: SearchRes_ActionMenu.java
  //Description 		: Function to verify if the desired pop up is dispalyed
  //Parameters 			: strPopUpName : Pop up name
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public boolean verifyPopUpExists(String strPopUpName){		
		String strExp_Title= this.getExpPopUp_Title(strPopUpName); 
		String strAct_Title;
		
		JavascriptExecutor js = ((JavascriptExecutor) driver);
	  	js.executeScript("arguments[0].scrollIntoView(true);",ele_PopUpWinTitle);
		
		//Dynamic wait for search results to display with a max threshold of 60sec
	  	LocalDateTime strStartWaitTime,strEndWaitTime;    		
		strStartWaitTime = LocalDateTime.now();
		try{
			System.out.println("Dynamic Wait time begins @"+strStartWaitTime);
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
									wait.withTimeout(60,TimeUnit.SECONDS)
									.pollingEvery(2, TimeUnit.SECONDS)				 
									.ignoring(NoSuchElementException.class)
									.until(ExpectedConditions.visibilityOf(ele_PopUpWinTitle));
		}//End of try block for FluentWait
		
		catch(org.openqa.selenium.TimeoutException e){
			strEndWaitTime = LocalDateTime.now();
			System.out.println("Dynamic Wait[catch block] for pop up window time Ends with a Max threshold wait of 60Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
			return false;				
		}//End of Catch block for FluentWait
	  	
	  	
    	if(!CommonUtils.isElementPresent(ele_PopUpWinTitle)){
				System.out.println("Checkpoint [Failed] : "+strPopUpName+" Pop up is NOT displayed,please check..!!");
				return false;						
		}//End of if condition to check if Pop up is displayed
    	
    	strAct_Title = ele_PopUpWinTitle.getAttribute("textContent");
    	strAct_Title = strAct_Title.replace("\u00A0", "").trim();
    	
    	if(strFns.compareString_Match(strAct_Title, strExp_Title)){    		
			System.out.println("Checkpoint :[Passed] "+strPopUpName+" Pop up is displayed");			
			return true;
    	}

    	else if((strAct_Title.trim().equals(strExp_Title.trim()))||(strAct_Title.trim()==strExp_Title.trim())||(strAct_Title.trim()=="" && strExp_Title.trim()=="")||(strAct_Title.trim().isEmpty() && strExp_Title.trim().isEmpty())||(strAct_Title.trim()==null && strExp_Title.trim()==null)){    		
			System.out.println("Checkpoint :[Passed] "+strPopUpName+" Pop up is displayed");			
			return true;
    	}
    	System.out.println("Checkpoint [Failed] : "+strPopUpName+" Pop up title is not as expected. Actual title is : "+strAct_Title+" and Expected title is : "+strExp_Title);
		return false;
    }//End of <Method: verifyPopUpExists>
    
    //###################################################################################################################################################################  
    //Function name		: getExpPopUp_Title(String strPopUpName)
    //Class name		: SearchRes_ActionMenu.java
    //Description 		: Function to get the expected window title for the desired pop up.
    //Parameters 		: strPopUpName : Pop up name
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
      public String getExpPopUp_Title(String strPopUpName){	
    	  String strExp_Title=""; 
  		if(strPopUpName.isEmpty() || strPopUpName=="" || strPopUpName.equals(null)){
			System.out.println("Parameter to getExpPopUp_Title method is incorrect,please check..!!");
			return null;	
  		}
    	
  		switch (strPopUpName.toUpperCase()){
	  		case "ACCTLEVEL_CBMETHOD":
	  			//Tax Strategy - Account #1131408
	  			strExp_Title = "Tax Strategy - Account #(.*)";
	  			return strExp_Title;
	  		case "ACCTLEVEL_FIELECTION":
	  			//Fixed Income Elections - Account #1131408
	  			strExp_Title = "Fixed Income Elections - Account #(.*)";
	  			return strExp_Title;
	  		case "ACCTLEVEL_MTM":
	  			//Mark-to-Market Settings - Account #1131408
	  			strExp_Title = "Mark-to-Market Settings - Account #(.*)";
	  			return strExp_Title;
	  		case "SECLEVEL_CBMETHOD":
	  			strExp_Title = "Cost Basis Method";
	  			return strExp_Title;
	  		case "SECLEVEL_FIELECTION":
	  			//Fixed Income Elections - Account #: 1131408 Security #: 161716
	  			strExp_Title = "Fixed Income Elections - Account #(.*) Security #:(.*)";
	  			return strExp_Title;
	  		case "RAWTRADES_DETAILS":
	  			strExp_Title = " ";
	  			return strExp_Title;
	  		case "RAWTRADES_VSP":
	  			strExp_Title = "";
	  			return strExp_Title;
	  		case "RAWTRADES_TLE":
	  		case "NCB_TAXLOT_EDIT":
	  			strExp_Title = "Tax Lot Edit";
	  			return strExp_Title;
//	  		case "NCB_TAXLOT_EDIT":
//	  			strExp_Title = "Tax Lot Edit";
//	  			return strExp_Title;	  			
	  		default:
  				strExp_Title = "";
  				return strExp_Title;
  		}
  		//return null;
      }//End of <Method: getExpPopUp_Title>
      

		
  //###################################################################################################################################################################  
  //Function name		: verifyPopUp_SaveCancel(int exclRowCnt,String strSheetName)
  //Class name			: SearchRes_ActionMenu.java
  //Description 		: Function to verify Save & Close buttonsin pop up
  //Parameters 			: Excel row number for report and sheet name.
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifyPopUp_SaveCancel(int exclRowCnt,String strSheetName,String strPopUpName){		
		try{
			
	    	if(!CommonUtils.isElementPresent(btn_Save)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed", "", "CBMethod_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed,please check..!!");
			}//End of if condition to check if SAVE button is displayed
	    		    
	    	if(!CommonUtils.isElementPresent(btn_Cancel)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed", "", "CBMethod_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed,please check..!!");
	    		btn_XClose.click();
	    		Thread.sleep(5000);
			}//End of if condition to check if SAVE button is displayed
	    	else{
	    		btn_Cancel.click();
	    		Thread.sleep(5000);
	    	}
	    	
		}//End of try block
		catch(Exception e){			
    		System.out.println("Exception in <Method: verifyPopUp_SaveCancel>,please check..!!");
		}//End of catch block
    }//End of <Method: verifyPopUp_SaveCancel>          
      
  //###################################################################################################################################################################  
  //Function name		: PopUp_EditHist(String strpopUpName,String strAcctSecLevel,int exclRowCnt,String strSheetName)
  //Class name			: SearchRes_ActionMenu.java
  //Description 		: Function to verify the Edit History section of Pop up
  //Parameters 			: Excel row number for report and sheet name.
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public void PopUp_EditHist(String strpopUpName,String strAcctSecLevel,int exclRowCnt,String strSheetName){		
  	try{
  		String strAct_EditHistHeader="";String strAct_EditHistRow ="";  		  			
  		
  		String[] arrExp_EditHist = PopUp_Exp_EditHist(strAcctSecLevel);
  		String strExp_EditHistHeader = arrExp_EditHist[0];
  		String strExp_EditHistRow = arrExp_EditHist[1];
  		
  		//tbl_EditHist_Header
      	if(!CommonUtils.isElementPresent(tbl_EditHist_Header)){
      		if((this.tbl_CurrSelec_Header.size()>0) &&  (this.tbl_CurrSelec_Header.size()==2)){
	      			if(!CommonUtils.isElementPresent(tbl_CurrSelec_Header.get(1))){
		      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->Edit History Header is NOT displayed", "", strpopUpName+"_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		      		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->Edit History Header is NOT displayed,please check..!!");
		      		return;
		  		}//End of if condition to check if Edit History Header is displayed
		      	else{
		      		List<WebElement> getCols = tbl_CurrSelec_Header.get(1).findElements(By.tagName("td"));
		      		for (WebElement eachCol : getCols) {  		
		      			strAct_EditHistHeader = strAct_EditHistHeader + ";"+eachCol.getAttribute("textContent").trim();
		    			}//End of FOR loop to get Header
		      		if(strAct_EditHistHeader.startsWith(";")){
		      			strAct_EditHistHeader = StringUtils.right(strAct_EditHistHeader, (strAct_EditHistHeader.length()-1));
		      		}
		      		
		      		if(!strFns.compareString_Match(strAct_EditHistHeader, strExp_EditHistHeader)){
		    	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->Edit History Header is NOT as expected.[Actual Header: "+strAct_EditHistHeader+" and Expected Header:"+strExp_EditHistHeader+"]", "", strpopUpName+"_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    	    		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->Edit History Header is NOT as expected.[Actual Header: "+strAct_EditHistHeader+" and Expected Header:"+strExp_EditHistHeader+"]");
		      		}//End of IF condition to check header's
		      		else{
		    	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strpopUpName+"->Edit History Header is as expected.[Expected Header:"+strExp_EditHistHeader+"]", "", strpopUpName+"_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    	    		System.out.println("Checkpoint :[Passed] "+strpopUpName+"->Edit History Header is as expected.[Expected Header:"+strExp_EditHistHeader+"]");
		      		}
		      		
		      	}//End of Else condition to verify header
      		}//End of IF condition to check size of elements for header
      	}//End of IF condition to check if Edit History Header is displayed
     
      	
      	//Checkpoint: Verify Edit History rows if the pop up is FI Election(both Account level/Cusip level)
      	if(strAcctSecLevel.equalsIgnoreCase("ACCTLEVEL_FIELECTION")||strAcctSecLevel.equalsIgnoreCase("SECLEVEL_FIELECTION")){
	      	if(!CommonUtils.isElementPresent(tbl_CurrSelec_Body)){
	      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->Edit History Table is NOT displayed", "", strpopUpName+"_EditHist_body",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	      		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->Edit History Table is NOT displayed,please check..!!");
	      		return;
	  		}//End of if condition to check if Edit History Body is displayed
	      	else{
	    			List<WebElement> editHist_rows = tbl_CurrSelec_Body.findElements(By.tagName("tr"));
	    			if(!(editHist_rows.size()>0)){
	    	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->Edit History ZERO rows displayed", "", strpopUpName+"_EditHist_body",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    	    		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->Edit History Table ZERO rows displayed,please check..!!");
	    			}
	    			else{
	    	  			List<WebElement> editHist_cols = editHist_rows.get(0).findElements(By.tagName("td"));
	    	    		for (WebElement eachHistCol : editHist_cols) {  		
	    	    			strAct_EditHistRow = strAct_EditHistRow + ";"+eachHistCol.getAttribute("textContent").trim();
	    	  			}//End of FOR loop to get Header
	    	    		if(strAct_EditHistRow.startsWith(";")){
	    	    			strAct_EditHistRow = StringUtils.right(strAct_EditHistRow, (strAct_EditHistRow.length()-1));
	    	    		}
	    	    		if(!strFns.compareString_Match(strAct_EditHistRow, strExp_EditHistRow)){
	    	  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->Edit History data is NOT as expected.[Actual Row: "+strAct_EditHistRow+" and Expected Row:"+strExp_EditHistRow+"]", "", strpopUpName+"_EditHist_Row",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    	  	    		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->Edit History data is NOT as expected.[Actual Row: "+strAct_EditHistRow+" and Expected Row:"+strExp_EditHistRow+"]");
	    	    		}//End of IF condition to check header's
	    	    		else{
	    	  	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strpopUpName+"->Edit History data is as expected.[Actual Row: "+strAct_EditHistRow+"]", "", strpopUpName+"_EditHist_Row",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    	  	    		System.out.println("Checkpoint :[Passed] "+strpopUpName+"->Edit History data is as expected.[Actual Row: "+strAct_EditHistRow+"]");
	    	    		}//End of Else condition to check header's
	    	    		
	    			}//End of Else condition to verify 1st row in the Edit History table.
	      		
	      	}//End of Else condition to verify 1st row in body
      	}//End of IF condition to verify Edit History 1st row
      	
      	
  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: PopUp_EditHist>,please check..!!");
  	}//End of catch block
    }//End of <Method: PopUp_EditHist>      
    
    
    
//###################################################################################################################################################################  
//Function name		: PopUp_Exp_EditHist(String strpopUpName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to get expected Edit History Header and 1st Row value
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public String[] PopUp_Exp_EditHist(String strpopUpName){		
		try{
			String[] strExp_EditHist = new String[2];
			
			switch(strpopUpName.toUpperCase()){
		  		case "ACCTLEVEL_CBMETHOD":
		  			strExp_EditHist[0] = "Effective Date;Created Date;Sell Method;Security Type";
		  			strExp_EditHist[1] = "(.*);(Mutual Fund|Stocks, Options, Bonds...|ETF RIC)";
		  			return strExp_EditHist;
		  			
		  		case "ACCTLEVEL_FIELECTION":
		  			strExp_EditHist[0] = "Election Type;Tax Payer Option;Taxable Year;Edited Date;Modified By";
		  			strExp_EditHist[1] = "(Bond Premium on Taxable Debt|Market Discount Computation Method|Market Discount Recognition|Income conversion to US dollars)(.*)";
		  			return strExp_EditHist;
		  			
		  		case "ACCTLEVEL_MTM":
		  			strExp_EditHist[0] = "Year;MTM;Date Modified;Modified by";
		  			strExp_EditHist[1] = "";
		  			return strExp_EditHist;
		  		case "SECLEVEL_CBMETHOD_CBM":
		  			strExp_EditHist[0] = "Effective Date;Created Date;Sell Method";
		  			strExp_EditHist[1] = "";
		  			return strExp_EditHist;
		  		case "SECLEVEL_CBMETHOD_CBM_AVG":
		  			strExp_EditHist[0] = "Effective Date;Sell Method;Date Modified;Modified by";
		  			strExp_EditHist[1] = "";
		  			return strExp_EditHist;
		  		case "SECLEVEL_CBMETHOD_DRP":
		  			strExp_EditHist[0] = "Enroll Date;Disenroll Date;Date Modified;Modified by";
		  			strExp_EditHist[1] = "";
		  			return strExp_EditHist;
		  		case "SECLEVEL_FIELECTION":
		  			//Fixed Income Elections - Account #: 1131408 Security #: 161716
		  			strExp_EditHist[0] = "Election Type;Tax Payer Option;Taxable Year;Edited Date;Modified By";
		  			strExp_EditHist[1] = "(Bond Premium on Taxable Debt|Market Discount Computation Method|Market Discount Recognition|Income conversion to US dollars)(.*)";
		  			return strExp_EditHist;
//		  		case "RAWTRADES_DETAILS":
//		  			strExp_Title = "";
//		  			return strExp_EditHist;
//		  		case "RAWTRADES_VSP":
//		  			strExp_Title = "";
//		  			return strExp_EditHist;
//		  		case "RAWTRADES_TAXLOT_EDIT":
//		  			strExp_Title = "Tax Lot Edit";
//		  			return strExp_EditHist;
//		  		case "NCB_TAXLOT_EDIT":
//		  			strExp_Title = "Tax Lot Edit";
//		  			return strExp_EditHist;	  			
		  		default:
	  				return strExp_EditHist;
			}
				
				
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Method: PopUp_Exp_EditHist>, please check..!!!");
			return null;
			
		}//End of Catch block
  }//End of <Method: PopUp_Exp_EditHist>
  
//###################################################################################################################################################################  
//Function name		: compareDropDown(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to compare expected & actual drop downs
//Parameters 		: strPopUpName : pop up name
//					: strDropDown: drop down name
//  				: Excel row number for report and sheet name.
//					: strSheetName: Excel sheet name for reporting  
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public void compareDropDown(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName){ 
	 SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	SearchRes_FIElection_Objs FI_Objs = PageFactory.initElements(driver, SearchRes_FIElection_Objs.class);
	SearchRes_MTM_Objs MTM_Objs = PageFactory.initElements(driver, SearchRes_MTM_Objs.class);
	TaxLotEdit_Objs TLE_Objs = PageFactory.initElements(driver, TaxLotEdit_Objs.class);
	  try{
			ArrayList<String> arrAct_Values = new ArrayList<String>();
			ArrayList<String> arrExp_Values = new ArrayList<String>();
			
			if(strPopUpName.equalsIgnoreCase("FIElection")) arrExp_Values = FI_Objs.getExp_FIPopUp_DropDowns(strDropDown);
			else if(strPopUpName.equalsIgnoreCase("MTM")) arrExp_Values = MTM_Objs.getExp_MTMPopUp_DropDowns(strDropDown);
			else if(strPopUpName.equalsIgnoreCase("ACCLEVEL_CBM")) arrExp_Values = CBM_Objs.getExp_AccLevel_CBM_DropDowns(strDropDown);
			else if(strPopUpName.equalsIgnoreCase("SecLevel_CBM")) arrExp_Values = CBM_Objs.getExp_SecLevel_CBM_DropDowns(strDropDown);
			else if(strPopUpName.equalsIgnoreCase("SecLevel_CBM_AvgCost")) arrExp_Values = CBM_Objs.getExp_SecLevel_AvgCost_CBM_DropDowns(strDropDown);
			else if(strPopUpName.equalsIgnoreCase("SecLevel_DRP")) arrExp_Values = CBM_Objs.getExp_SecLevel_DRP_DropDowns(strDropDown);
			else if(strPopUpName.equalsIgnoreCase("DETAILS_CBM")) arrExp_Values = CBM_Objs.getExp_Details_SellMethod(strDropDown);
			else if(strPopUpName.equalsIgnoreCase("TLE_TRNXTYPE")) arrExp_Values = TLE_Objs.getExp_TLE_TrnxType(strDropDown);
			
			else if(strPopUpName.equalsIgnoreCase("ADD_NEW_CA")) {
				CAManager_Objs CA_Objs = PageFactory.initElements(driver, CAManager_Objs.class);
				arrExp_Values = CA_Objs.getExp_CAManager_DropDowns(strDropDown);
			}
			
			if(strPopUpName.equalsIgnoreCase("SecLevel_CBM")) strPopUpName = "CBMethod_CBM";
			else if(strPopUpName.equalsIgnoreCase("SecLevel_CBM_AvgCost")) strPopUpName = "CBMethod_CBM_AvgCost";
			else if(strPopUpName.equalsIgnoreCase("SecLevel_DRP")) strPopUpName = "CBMethod_DRP";
			else if(strPopUpName.equalsIgnoreCase("ACCLEVEL_CBM")) strPopUpName = "CBMethod";
			else if(strPopUpName.equalsIgnoreCase("DETAILS_CBM")) strPopUpName = "RawTrades_Details";
			else if(strPopUpName.equalsIgnoreCase("TLE_TRNXTYPE")) strPopUpName = "RawTrades_TLE";
			
	    	if(!(this.ele_PopUpCombo.size()>0)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed", "", strPopUpName+"_"+strDropDown,  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed,please check..!!");
	    		return;
			}//End of if condition to check if dropdown list is displayed

	    	//Logic to read the ComboList:
	    	List<WebElement> comboItems = ele_PopUpCombo;
	        for (int i = 0; i < comboItems.size(); i++) {
	        	arrAct_Values.add(comboItems.get(i).getText());
	        };
	        
	        //Compare Actual & Expected Dropdown values
	        //if(!arrAct_Values.toString().equals(arrExp_Values.toString())){
	        if(!(arrAct_Values.containsAll(arrExp_Values) && arrExp_Values.containsAll(arrAct_Values) )){
	        	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+".Expected Dropdown values = "+arrExp_Values.toString()+"; Actual Dropdown values = "+arrAct_Values.toString(),"", strPopUpName+"_"+strDropDown,   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+".Expected Dropdown values = "+arrExp_Values.toString()+"; Actual Dropdown values = "+arrAct_Values.toString());
			}//End of IF condition to check Expected Action Menu List & Actual Action Menu List
	        else {
	        	reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->"+strDropDown+".Dropdown values = "+arrExp_Values.toString(),"", strPopUpName+"_"+strDropDown,   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Passed] "+strPopUpName+"->"+strDropDown+".Dropdown values = "+arrExp_Values.toString());
	        }
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: compareDropDown>,"+e.getMessage()+",please check..!!!");
	  }//End of catch block
	  
  }//End of <Method: compareDropDown>
  
//###################################################################################################################################################################  
//Function name		: verifyPopUpError(String strPopUpName,int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to compare expected & actual drop downs
//Parameters 		: strPopUpName : pop up name
//  				: Excel row number for report and sheet name.
//					: strSheetName: Excel sheet name for reporting  
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public boolean verifyPopUpError(String strPopUpName,int exclRowCnt,String strSheetName){ 
	  try{
		  String strPopError;
		  if(this.ele_PopUpErrors.size()>0){
			  strPopError = ele_PopUpErrMsg.getAttribute("textContent");
			  reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"-> Error displayed,"+strPopError, "", strPopUpName+"_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			  System.out.println("Checkpoint :[Failed] "+strPopUpName+"-> Error displayed,"+strPopError);
		    	
			  if(!CommonUtils.isElementPresent(btn_Cancel)){
		    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed", "", "CBMethod_Cancel",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed,please check..!!");
		    		btn_XClose.click();
		    		Thread.sleep(5000);
				}//End of if condition to check if SAVE button is displayed
		    	else{
		    		btn_Cancel.click();
		    		Thread.sleep(5000);
		    	}
			  return false;
		  }//End of IF condition to check if Error message objects are displayed.
		  return true;
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: verifyPopUpError>,please check..!!!");
		  return false;
	  }//End of catch block
	  
  }//End of <Method: verifyPopUpError>
  
//###################################################################################################################################################################  
//Function name		: selectValue_DropDown(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName,String strSelectValue)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to compare expected & actual drop downs
//Parameters 		: strPopUpName : pop up name
//					: strDropDown: drop down name
//  				: Excel row number for report and sheet name.
//					: strSheetName: Excel sheet name for reporting
//					: strSelectValue: Value to be selected from drop down
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public boolean selectValue_DropDown(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName,String strSelectValue){ 
	  String strActValue;
	  boolean blnSelect = false;
	  try{
			if(strPopUpName.equalsIgnoreCase("TLE_TRNXTYPE")) strPopUpName = "RawTrades_TLE";			
	    	if(!(this.ele_PopUpCombo.size()>0)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed", "", strPopUpName+"_"+strDropDown,  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed,please check..!!");
	    		return false;
			}//End of if condition to check if drop down list is displayed

	    	//Logic to Select value from the ComboList drop down
	    	List<WebElement> comboItems = ele_PopUpCombo;
	        for (int i = 0; i < comboItems.size(); i++) {
	        	strActValue = comboItems.get(i).getText();
	        	if(strActValue.equals(strSelectValue)) {
	        		comboItems.get(i).click();
	        		blnSelect = true;
	        		break;
	        	}
	        }
	        if(!blnSelect){
	        	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+"->"+strSelectValue+" value is not displayed in the drop down,please check..!!", "",strDropDown+"_"+strSelectValue,   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+"->"+strSelectValue+" value is not displayed in the drop down,please check..!!");
	        	return false;
	        }	        
	        else {
	        	reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->"+strDropDown+"->"+strSelectValue+" value is selected from the drop down", "",strDropDown+"_"+strSelectValue,   driver, "BOTH_DIRECTIONS", strSheetName, null);
	        	System.out.println("Checkpoint :[Passed] "+strPopUpName+"->"+strDropDown+"->"+strSelectValue+" value is selected from the drop down");
	        	return true;
	        }
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: selectValue_DropDown>,"+e.getMessage()+",please check..!!!");
		  return false;
	  }//End of catch block
	  
  }//End of <Method: selectValue_DropDown>
  
//###################################################################################################################################################################  
//Function name		: getAct_DropDown_Values(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to compare expected & actual drop downs
//Parameters 		: strPopUpName : pop up name
//					: strDropDown: drop down name
//  				: Excel row number for report and sheet name.
//					: strSheetName: Excel sheet name for reporting  
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public ArrayList<String> getAct_DropDown_Values(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName){ 
	  try{
			ArrayList<String> arrAct_Values = new ArrayList<String>();
			if(strPopUpName.equalsIgnoreCase("SecLevel_CBM")) strPopUpName = "CBMethod_CBM";
			else if(strPopUpName.equalsIgnoreCase("SecLevel_CBM_AvgCost")) strPopUpName = "CBMethod_CBM_AvgCost";
			else if(strPopUpName.equalsIgnoreCase("SecLevel_DRP")) strPopUpName = "CBMethod_DRP";
			else if(strPopUpName.equalsIgnoreCase("ACCLEVEL_CBM")) strPopUpName = "CBMethod";
			else if(strPopUpName.equalsIgnoreCase("DETAILS_CBM")) strPopUpName = "RawTrades_Details";
			else if(strPopUpName.equalsIgnoreCase("TLE_TRNXTYPE")) strPopUpName = "RawTrades_TLE";
			else if(strPopUpName.equalsIgnoreCase("AVGGIFT_TLE_TRNXTYPE")) strPopUpName = "AvgCostGift_TLE";
			
	    	if(!(this.ele_PopUpCombo.size()>0)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed", "", strPopUpName+"_"+strDropDown,  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->"+strDropDown+" object is NOT displayed,please check..!!");
	    		return null;
			}//End of if condition to check if dropdown list is displayed

	    	//Logic to read the ComboList:
	    	List<WebElement> comboItems = ele_PopUpCombo;
	        for (int i = 0; i < comboItems.size(); i++) {
	        	arrAct_Values.add(comboItems.get(i).getText());
	        };
	        
	        return arrAct_Values;
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: getAct_DropDown_Values>,"+e.getMessage()+",please check..!!!");
		  return null;
	  }//End of catch block
	  
  }//End of <Method: getAct_DropDown_Values>
  
  
//###################################################################################################################################################################  
//Function name		: compareDropDown(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to compare expected & actual drop downs
//Parameters 		: strPopUpName : pop up name
//					: strDropDown: drop down name
//  				: Excel row number for report and sheet name.
//					: strSheetName: Excel sheet name for reporting  
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public ArrayList<String> getExp_DropDown_Values(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName){ 
	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	SearchRes_FIElection_Objs FI_Objs = PageFactory.initElements(driver, SearchRes_FIElection_Objs.class);
	SearchRes_MTM_Objs MTM_Objs = PageFactory.initElements(driver, SearchRes_MTM_Objs.class);
	TaxLotEdit_Objs TLE_Objs = PageFactory.initElements(driver, TaxLotEdit_Objs.class);
	  try{
			ArrayList<String> arrExp_Values = new ArrayList<String>();
			switch(strPopUpName.toUpperCase()){
			case "FIELECTION":
				arrExp_Values = FI_Objs.getExp_FIPopUp_DropDowns(strDropDown);
				return arrExp_Values;
			case "MTM":
				arrExp_Values = MTM_Objs.getExp_MTMPopUp_DropDowns(strDropDown);
				return arrExp_Values;
			case "ACCLEVEL_CBM":
				arrExp_Values = CBM_Objs.getExp_AccLevel_CBM_DropDowns(strDropDown);
				return arrExp_Values;
			case "SECLEVEL_CBM":
				arrExp_Values = CBM_Objs.getExp_SecLevel_CBM_DropDowns(strDropDown);
				return arrExp_Values;
			case "SECLEVEL_CBM_AVGCOST":
				arrExp_Values = CBM_Objs.getExp_SecLevel_AvgCost_CBM_DropDowns(strDropDown);
				return arrExp_Values;
			case "SECLEVEL_DRP":
				arrExp_Values = CBM_Objs.getExp_SecLevel_DRP_DropDowns(strDropDown);
				return arrExp_Values;
			case "DETAILS_CBM":
				arrExp_Values = CBM_Objs.getExp_Details_SellMethod(strDropDown);
				return arrExp_Values;
			case "TLE_TRNXTYPE":
				arrExp_Values = TLE_Objs.getExp_TLE_TrnxType(strDropDown);
				return arrExp_Values;
			case "AVGCOSTGIFT_TRNXTYPE":
				arrExp_Values = TLE_Objs.getExp_TLE_TrnxType(strDropDown);
				return arrExp_Values;				
				default:
					return null;
			}//End of Switch case		
		  
	  }//End of try block
	  catch(Exception e){
		  System.out.println("Exception in <Method: getExp_DropDown_Values>,"+e.getMessage()+",please check..!!!");
		  return null;
	  }//End of catch block
	  
  }//End of <Method: getExp_DropDown_Values>
  
//###################################################################################################################################################################  
  //Function name		: PopUp_VSPData(String strpopUpName,String strAcctSecLevel,int exclRowCnt,String strSheetName)
  //Class name			: SearchRes_ActionMenu.java
  //Description 		: Function to verify the Edit History section of Pop up
  //Parameters 			: Excel row number for report and sheet name.
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public void PopUp_VSPData(int exclRowCnt,String strSheetName){		
  	try{
  		String strpopUpName = "RawTrades_VSP";
  		String strAct_VSPHeader=""; 		  			
  		String strExp_VSPHeader = "Security;Trade Date;Qty;Unit Cost;Cost;Qty to Apply;% to Apply;Cost to Apply";
  		
  		//VsPurchase Header is displayed checkpoint
      		if((this.tbl_CurrSelec_Header.size()>0) &&  (this.tbl_CurrSelec_Header.size()==2)){
	      			if(!CommonUtils.isElementPresent(tbl_CurrSelec_Header.get(1))){
		      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->VsPurchase Header is NOT displayed", "", strpopUpName+"_VSP_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		      		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->VsPurchase Header is NOT displayed,please check..!!");
		      		return;
		  		}//End of if condition to check if VsPurchase Header is displayed
		      	else{
		      		List<WebElement> getCols = tbl_CurrSelec_Header.get(1).findElements(By.tagName("td"));
		      		for (WebElement eachCol : getCols) {  		
		      			strAct_VSPHeader = strAct_VSPHeader + ";"+eachCol.getAttribute("textContent").trim();
		    			}//End of FOR loop to get Header
		      		if(strAct_VSPHeader.startsWith(";")){
		      			strAct_VSPHeader = StringUtils.right(strAct_VSPHeader, (strAct_VSPHeader.length()-1));
		      		}
		      		
		      		if(!strFns.compareString_Match(strAct_VSPHeader, strExp_VSPHeader)){
		    	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->VsPurchase Header is NOT as expected.[Actual Header: "+strAct_VSPHeader+" and Expected Header:"+strExp_VSPHeader+"]", "", strpopUpName+"_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    	    		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->VsPurchase Header is NOT as expected.[Actual Header: "+strAct_VSPHeader+" and Expected Header:"+strExp_VSPHeader+"]");
		      		}//End of IF condition to check header's
		      		else{
		    	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strpopUpName+"->VsPurchase Header is as expected.[Expected Header:"+strExp_VSPHeader+"]", "", strpopUpName+"_VSP_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    	    		System.out.println("Checkpoint :[Passed] "+strpopUpName+"->VsPurchase Header is as expected.[Expected Header:"+strExp_VSPHeader+"]");
		      		}
		      		
		      	}//End of Else condition to verify header
      		}//End of IF condition to check size of elements for header
     
      	
      	//Checkpoint: Verify atleast one row is displayed in VSP pop up)
	      	if(!(this.tbl_VSP_Body.size()>1)){
	      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->VsPurchase Data is NOT displayed", "", strpopUpName+"_VSP_body",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	      		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->VsPurchase Table is NOT displayed,please check..!!");
	      		return;
	  		}//End of if condition to check if VsPurchase Body is displayed
	      	else{
    			List<WebElement> editHist_rows = tbl_VSP_Body.get(1).findElements(By.tagName("tr"));
    			if(!(editHist_rows.size()>0)){
    	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->VsPurchase ZERO rows[No data] displayed", "", strpopUpName+"_VSP_body",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->VsPurchase ZERO rows[No data] displayed,please check..!!");
    			}
    			else{
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strpopUpName+"->VsPurchase pop up displays "+editHist_rows.size()+" rows.", "", strpopUpName+"_VSP_body",  driver, "HORIZONTALLY", strSheetName, null);
    			}
	      	}//End of Else condition to verify 1st row in body
      	
      	
  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: PopUp_VSPData>,please check..!!");
  	}//End of catch block
    }//End of <Method: PopUp_VSPData>    
  
    //###################################################################################################################################################################  
    //Function name		: verifyVSPPopUp_BackSaveCancel(int exclRowCnt,String strSheetName)
    //Class name			: SearchRes_ActionMenu.java
    //Description 		: Function to verify Save & Close buttonsin pop up
    //Parameters 			: Excel row number for report and sheet name.
    //Assumption			: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
      public boolean verifyVSPPopUp_BackSaveCancel(String strPopUpName,int exclRowCnt,String strSheetName){		
  		//String strPopUpName = "RawTrades_VSP";
    	  try{
 	    	
  	    	if(!CommonUtils.isElementPresent(btn_Save)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed", "", "VSP_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed,please check..!!");
  			}//End of if condition to check if SAVE button is displayed
  	    		    
  	    	if(!CommonUtils.isElementPresent(btn_Cancel)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed", "", "VSP_Cancel",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed,please check..!!");
  			}//End of if condition to check if CANCEL button is displayed
  	    	
  	    	if(!CommonUtils.isElementPresent(btn_Back)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->BACK button is NOT displayed, so Skipping 'Navigating to VsPurchase' pop up.", "", "VSP_Back",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->BACK button is NOT displayed so skipping 'Navigating to VsPurchase',please check..!!");
  	    		btn_XClose.click();
  	    		Thread.sleep(5000);
  	    		return false;
  			}//End of if condition to check if BACK button is displayed
  	    	
  	    	else{
  	    		btn_Back.click();
  	    		Thread.sleep(5000);
  	    		return true;
  	    	}
  	    	
  		}//End of try block
  		catch(Exception e){			
      		System.out.println("Exception in <Method: verifyVSPPopUp_BackSaveCancel>,please check..!!");
      		return false;
  		}//End of catch block
      }//End of <Method: verifyVSPPopUp_BackSaveCancel> 
  
      //###################################################################################################################################################################  
      //Function name		: VSPAdv_Header(String strVSPSection,int exclRowCnt,String strSheetName)
      //Class name			: SearchRes_ActionMenu.java
      //Description 		: Function to verify the header in VSP Advanced Pop up for given 'strVSPSection' 
      //Parameters 			: Excel row number for report and sheet name.
      //Assumption			: None
      //Developer			: Kavitha Golla
      //###################################################################################################################################################################		
        public void VSPAdv_Header(String strVSPSection,int exclRowCnt,String strSheetName){		
      	try{
      		int headerIndex=1;
      		String strpopUpName = "VSPAdvanced";
      		String strAct_VSPHeader=""; 
      		String strExp_VSPHeader="";
      		if(strVSPSection.equalsIgnoreCase("ReturnedResults")){
      			strExp_VSPHeader = "Security;Trade Date;Qty;Unit Cost;Cost";
      			headerIndex = 1;
      		}
      		else if(strVSPSection.equalsIgnoreCase("SelectedLots")){
      			strExp_VSPHeader = "Security;Trade Date;Qty;Unit Cost;Cost;Qty to Apply;% to Apply;Cost to Apply";
      			headerIndex=2;
      		}
      		
      		//VsPurchase Header is displayed checkpoint
          		if((this.tbl_CurrSelec_Header.size()>0) &&  (this.tbl_CurrSelec_Header.size()==3)){
          			
    	      			if(!CommonUtils.isElementPresent(tbl_CurrSelec_Header.get(headerIndex))){
    		      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->"+strVSPSection+" Header is NOT displayed", "", strpopUpName+"_"+strVSPSection,  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		      		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->"+strVSPSection+" Header is NOT displayed,please check..!!");
    		      		return;
    		  		}//End of if condition to check if VsPurchase Advanced 'strVSPSection' Header is displayed
    		      	else{
    		      		List<WebElement> getCols = tbl_CurrSelec_Header.get(headerIndex).findElements(By.tagName("td"));
    		      		for (WebElement eachCol : getCols) {  
    		      			if(!(eachCol.getAttribute("style").contains("display: none"))){
    		      				strAct_VSPHeader = strAct_VSPHeader + ";"+eachCol.getAttribute("textContent").trim();
    		    			}
    		      		}//End of FOR loop to get Header
    		      		if(strAct_VSPHeader.startsWith(";")){
    		      			strAct_VSPHeader = StringUtils.right(strAct_VSPHeader, (strAct_VSPHeader.length()-1));
    		      		}
    		      		
    		      		if(!strFns.compareString_Match(strAct_VSPHeader, strExp_VSPHeader)){
    		    	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strpopUpName+"->"+strVSPSection+" Header is NOT as expected.[Actual Header: "+strAct_VSPHeader+" and Expected Header:"+strExp_VSPHeader+"]", "", strpopUpName+"_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		    	    		System.out.println("Checkpoint :[Failed] "+strpopUpName+"->"+strVSPSection+" Header is NOT as expected.[Actual Header: "+strAct_VSPHeader+" and Expected Header:"+strExp_VSPHeader+"]");
    		      		}//End of IF condition to check header's
    		      		else{
    		    	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strpopUpName+"->"+strVSPSection+" Header is as expected.[Expected Header:"+strExp_VSPHeader+"]", "", strpopUpName+"_VSP_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		    	    		System.out.println("Checkpoint :[Passed] "+strpopUpName+"->"+strVSPSection+" Header is as expected.[Expected Header:"+strExp_VSPHeader+"]");
    		      		}
    		      		
    		      	}//End of Else condition to verify header
          		}//End of IF condition to check size of elements for header    
      
  		}//End of try block
  		catch(Exception e){			
      		System.out.println("Exception in <Method: VSPAdv_Header>,please check..!!");
      		return ;
  		}//End of catch block   
        }
        
        //###################################################################################################################################################################  
        //Function name		: verifyPopUp_SaveCancel(int exclRowCnt,String strSheetName)
        //Class name			: SearchRes_ActionMenu.java
        //Description 		: Function to verify Save & Close buttonsin pop up
        //Parameters 			: Excel row number for report and sheet name.
        //Assumption			: None
        //Developer			: Kavitha Golla
        //###################################################################################################################################################################		
          public void verifyDetails_SaveCancel(int exclRowCnt,String strSheetName,String strPopUpName){		
      		try{
      			
      	    	if(!CommonUtils.isElementPresent(btn_Save)){
      	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed", "", "CBMethod_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed,please check..!!");
      			}//End of if condition to check if SAVE button is displayed
      	    		    
      	    	if(!CommonUtils.isElementPresent(btn_Cancel)){
      	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed", "", "CBMethod_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->CANCEL button is NOT displayed,please check..!!");
      			}//End of if condition to check if SAVE button is displayed

      	    	
      		}//End of try block
      		catch(Exception e){			
          		System.out.println("Exception in <Method: verifyDetails_SaveCancel>,please check..!!");
      		}//End of catch block
          }//End of <Method: verifyDetails_SaveCancel>         
        
  //###################################################################################################################################################################      
}//End of Class <SearchRes_ActionMenu>		
