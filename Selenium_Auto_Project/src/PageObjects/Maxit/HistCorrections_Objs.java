package PageObjects.Maxit;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class HistCorrections_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	
    //Hist Correction:
	@FindBy(how=How.XPATH,using="//a[contains(@href, 'Maxit/HistCorrections/enter.php')]")
    public WebElement lnk_HistCorrec;
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
    public WebElement ele_ResTble_Header;
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-body\"]")
    public WebElement ele_ResTble;
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid-empty\"]")
    public List<WebElement> ele_NoResults;
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-row-expander\"]")
    public List<WebElement> ele_EditButtons;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Apply\")]")
    public WebElement btn_Apply;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Cancel\")]")
    public WebElement btn_Cancel;

   //*********************************************************************************************************************  
    //Performing Actions on objects in Search Page:
   //*********************************************************************************************************************      
    //###################################################################################################################################################################  
    //Function name		: HistCorrec_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName)
    //Class name		: HistCorrections_Objs
    //Description 		: Function to perform search based on input data under given CB Manager tab 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean HistCorrec_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName){
    		try{
    			boolean blnSecTypeCheck = false;
    			CBMngr_Objs.txtbx_Acct.sendKeys(strAcct);
    			List<WebElement> SecType_List = CBMngr_Objs.lst_SecIDType.findElements(By.tagName("option"));
    			for (WebElement option : SecType_List) {
    				if(option.getText().equals(strSecType)){
    					option.click();
    					blnSecTypeCheck=true;
    					break;
    				}
    			}//End of FOR loop
    			if(!blnSecTypeCheck){
    				for (WebElement option : SecType_List) {
    					if(option.getText().equals("Security No")){
    						System.out.println("Selecting default SecIDType as Security No");
    						option.click();
    						blnSecTypeCheck=true;
    						break;
    					}
    				}//End of FOR loop
    			}//End of IF condition to select default SecIDTpe= Security ID

    			CBMngr_Objs.txtbx_SecurityID.sendKeys(strSecValue);
    			
    			CBMngr_Objs.btn_Submit.click();
    			Thread.sleep(7000);
    			
    			//Checkpoint: Check for Errors:
    			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;
    				    			
    			//Check if page is loading:			
    			if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
    				System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return false;
    			}//End of IF condition to check for if page is still loading data.
    			
    			//Check for Results table header
    			if(CommonUtils.isElementPresent(ele_ResTble_Header)){
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Search After Input data insert:Passed", "", strTabName+"_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return true;
    			}//End of IF condition to check for Results table header.	
    			
    			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Search after input:Failed, Results header is not displayed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    			
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <CBManager_Objs.HistCorrec_Search>: CBManger->"+strTabName+" verifying header title.");
    			return false;
    		}//End of catch block
    	}//End of <Method: HistCorrec_Search> 
    	
    //###################################################################################################################################################################  
    //Function name		: HistCorrec_BlankSearch(String strTabName,int exclRowCnt,String strSheetName,acct , )
    //Class name		: HistCorrections_Objs
    //Description 		: Function to perform search based on input data under given CB Manager tab 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean HistCorrec_BlankSearch(String strTabName,int exclRowCnt,String strSheetName){
    		try{			
    			CBMngr_Objs.btn_Submit.click();
    			Thread.sleep(5500);
    			
    			//Checkpoint: Check for Errors:
    			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;
    			
    			//Check for Results table header
    			if(CommonUtils.isElementPresent(ele_ResTble_Header)){
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Search After Input data insert:Passed", "", strTabName+"_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return true;
    			}//End of IF condition to check for Results table .			
    			
    			//Check if page is loading:			
    			if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
    				System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return false;
    			}//End of IF condition to check for Results table header.
    			
    			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Search after input:Failed, Results header is not displayed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    			
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <CBManager_Objs.HistCorrec_BlankSearch>: CBManger->"+strTabName+" verifying header title.");
    			return false;
    		}//End of catch block
    	}//End of <Method: HistCorrec_BlankSearch>     	
    	
    //###################################################################################################################################################################  
    //Function name		: HistCorrec_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName)
    //Class name		: HistCorrections_Objs
    //Description 		: Function to perform search based on input data under given CB Manager tab 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public void HistCorrec_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName){
    		try{
    			if(ele_NoResults.size()>0){
    				System.out.println("Checkpoint : Search Results_ No Results found..!!");
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->Historic Corrections:Search Account["+strAcct+"] No Results Found", "", "HistCorrt_NoResFound",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				return;
    			}
    			
    			if(ele_ResTble.isDisplayed()){
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->Historic Corrections:Search Results rows displayed", "", "HistCorrt_Results",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	  			if(ele_EditButtons.size()>0){
    	  				ele_EditButtons.get(0).click();
    	  				Thread.sleep(5000);    	  				
    	  				//check for errors:
    	  				if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return;
    	  				
    	  				if(btn_Apply.isDisplayed())  reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->Historic Corrections:Edit Row1, 'Apply' button is displayed", "", "HistCorrt_EditApply",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	  				else reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->Historic Corrections:Edit Row1, 'Apply' button is NOT displayed,please check..!!", "", "HistCorrt_EditApply",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	  				
    	  				if(btn_Cancel.isDisplayed()){
    	  					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->Historic Corrections:Edit Row1, 'Cancel' button is displayed", "", "HistCorrt_EditCancel",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	  					btn_Cancel.click();
    	  					return;
    	  				}
    	  				else reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->Historic Corrections:Edit Row1, 'Cancel' button is NOT displayed,please check..!!", "", "HistCorrt_EditCancel",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	  			}//End of IF condition to check Edit Buttons count in Search Results rows.
    	  			else{
    	  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->Historic Corrections:Edit Row expnader is not displayed,please check..!!", "", "HistCorrt_EditRow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	  				return;
    	  			}
    			}//End of IF condition to check if Results table is displayed
    			else {
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->Historic Corrections:Results table is not displayed,please check..!!", "", "HistCorrt_Results",  driver, "BOTH_DIRECTIONS", strSheetName, null);    			
    				return;
    			}
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <CBManager_Objs.HistCorrec_CheckPoints>: CBManger->HistCOrrections verifying header title.");
    			reporter.reportStep(exclRowCnt, "Failed", "Exception in <CBManager_Objs.HistCorrec_CheckPoints>,please check..!!", "", "HistCorrt_Exception",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return;
    		}//End of catch block
    	}//End of <Method: HistCorrec_CheckPoints>     	
    
}//End of <Class: HistCorrections_Objs>
