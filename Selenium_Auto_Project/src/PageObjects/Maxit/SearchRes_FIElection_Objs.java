package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class SearchRes_FIElection_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	SearchRes_ActionMenu SearchRes_Action = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
	
    //FI-Elections:
    @FindBy(how=How.XPATH,using="//*[@id=\"taxPayerOptionsId_BPTD\"]")
	public WebElement ele_BPTD;
    @FindBy(how=How.XPATH,using="//*[@id=\"taxableYearId_BPTD\"]")
	public WebElement ele_BPTD_TaxYear;
    @FindBy(how=How.XPATH,using="//*[@id=\"taxPayerOptionsId_MDCM\"]")
	public WebElement ele_MDCM;
    @FindBy(how=How.XPATH,using="//*[@id=\"taxableYearId_MDCM\"]")
	public WebElement ele_MDCM_TaxYear;        
    @FindBy(how=How.XPATH,using="//*[@id=\"taxPayerOptionsId_MDR\"]")
	public WebElement ele_MDR;
    @FindBy(how=How.XPATH,using="//*[@id=\"taxableYearId_MDR\"]")
	public WebElement ele_MDR_TaxYear; 
    @FindBy(how=How.XPATH,using="//*[@id=\"taxPayerOptionsId_ICUS\"]")
	public WebElement ele_ICUS;
    @FindBy(how=How.XPATH,using="//*[@id=\"taxableYearId_ICUS\"]")
	public WebElement ele_ICUS_TaxYear; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"ext-el-mask-msg x-mask-loading\"]")
    private List<WebElement> ele_Loading; 
    
    //###################################################################################################################################################################  
    //Function name		: verifyAcct_FI_DropDowns(int exclRowCnt,String strSheetName)
    //Class name			: SearchRes_ActionMenu.java
    //Description 		: Function to verify the drop downs in FI Election Pop up
    //Parameters 			: Excel row number for report and sheet name.
    //Assumption			: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
      public void verifyAcct_FI_DropDowns(int exclRowCnt,String strSheetName){		
  		try{		
  			SearchRes_Action.ele_EditMethod.click();
  			
  			//BPTD checkpoints
  	    	if(!CommonUtils.isElementPresent(ele_BPTD)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->BPTD object is NOT displayed", "", "FIElection_BPTD",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->BPTD object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else{
  	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->BPTD object is displayed", "", "FIElection_BPTD",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Passed] FIElection->BPTD object is displayed");
  	    	}
  	    	ele_BPTD.click();    	
  	    	SearchRes_Action.compareDropDown("FIElection","BPTD",exclRowCnt,strSheetName);	    	
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	
  	    	if(!CommonUtils.isElementPresent(ele_BPTD_TaxYear)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->BPTD_TaxYear object is NOT displayed", "", "FIElection_BPTD_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->BPTD_TaxYear object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->BPTD_TaxYear object is displayed", "", "FIElection_BPTD_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		    	
  	    	ele_BPTD_TaxYear.click();    	
  	    	SearchRes_Action.compareDropDown("FIElection","BPTD_TaxYear",exclRowCnt,strSheetName);	    	
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	
  	    	//MDCM checkpoints
  	    	if(!CommonUtils.isElementPresent(ele_MDCM)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM object is NOT displayed", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM object is displayed", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    	
  	    	if(ele_MDCM.isEnabled()){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM dropdown is ENABLED", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM is ENABLED,please check..!!");
  	    	}
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM dropdown is DISABLED(READONLY)", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		    	
  	    	if(!CommonUtils.isElementPresent(ele_MDCM_TaxYear)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM_TaxYear object is NOT displayed", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM_TaxYear object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM_TaxYear object is displayed", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    	
  	    	if(ele_MDCM_TaxYear.isEnabled()){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM_TaxYear dropdown is ENABLED", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM_TaxYear is ENABLED,please check..!!");
  	    	}
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM_TaxYear dropdown is DISABLED(READONLY)", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    	
  	    	//MDR checkpoints
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	if(!CommonUtils.isElementPresent(ele_MDR)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDR object is NOT displayed", "", "FIElection_MDR",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->MDR object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else  reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDR object is displayed", "", "FIElection_MDR",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		
  	    	ele_MDR.click();    	
  	    	SearchRes_Action.compareDropDown("FIElection","MDR",exclRowCnt,strSheetName);	    	
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	
  	    	if(!CommonUtils.isElementPresent(ele_MDR_TaxYear)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDR_TaxYear object is NOT displayed", "", "FIElection_MDR_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->MDR_TaxYear object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDR_TaxYear object is displayed", "", "FIElection_MDR_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    	
  	    	ele_MDR_TaxYear.click();    	
  	    	SearchRes_Action.compareDropDown("FIElection","MDR_TaxYear",exclRowCnt,strSheetName);	    	
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	
  	    	//ICUS checkpoints
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	if(!CommonUtils.isElementPresent(ele_ICUS)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->ICUS object is NOT displayed", "", "FIElection_ICUS",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->ICUS object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->ICUS object is displayed", "", "FIElection_ICUS",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    	
  	    	ele_ICUS.click();    	
  	    	SearchRes_Action.compareDropDown("FIElection","ICUS",exclRowCnt,strSheetName);	    	
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	
  	    	if(!CommonUtils.isElementPresent(ele_ICUS_TaxYear)){
  	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->ICUS_TaxYear object is NOT displayed", "", "FIElection_ICUS_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    		System.out.println("Checkpoint :[Failed] FIElection->ICUS_TaxYear object is NOT displayed,please check..!!");
  			}//End of if condition to check if DROPDOWN is displayed
  	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->ICUS_TaxYear object is displayed", "", "FIElection_ICUS_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	    	
  	    	ele_ICUS_TaxYear.click();    	
  	    	SearchRes_Action.compareDropDown("FIElection","ICUS_TaxYear",exclRowCnt,strSheetName);	    	
  	    	SearchRes_Action.ele_EditMethod.click();
  	    	

  	    	
  		}//End of try block
  		catch(Exception e){			
      		System.out.println("Exception in <Method: verifyAcct_FI_DropDowns>,please check..!!");
  		}//End of catch block
      }//End of <Method: verifyAcct_FI_DropDowns>  

  //###################################################################################################################################################################  
  //Function name		: verifySec_FI_DropDowns(int exclRowCnt,String strSheetName)
  //Class name			: SearchRes_ActionMenu.java
  //Description 		: Function to verify the drop downs in FI Election Pop up
  //Parameters 			: Excel row number for report and sheet name.
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifySec_FI_DropDowns(int exclRowCnt,String strSheetName){	
    	LocalDateTime strStartWaitTime,strEndWaitTime;
		try{		
			SearchRes_Action.ele_EditMethod.click();		
	    	
			if(ele_Loading.size()>0){
				//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
				strStartWaitTime = LocalDateTime.now();
				try{
					System.out.println("FI Election Dynamic Wait time begins for @"+strStartWaitTime);
					FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
											wait.withTimeout(40,TimeUnit.SECONDS)
											.pollingEvery(2, TimeUnit.SECONDS)				 
											.ignoring(NoSuchElementException.class)
											.until(ExpectedConditions.not(ExpectedConditions.visibilityOfAllElements(ele_Loading)));
				}//End of try block for FluentWait
				
				catch(org.openqa.selenium.TimeoutException e){
					strEndWaitTime = LocalDateTime.now();
					System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
					
					//Check if page is loading:			
					if(ele_Loading.size()>0){
						System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FI Election pop up,still Loading[Waited for search results for 30Sec]", "", "FIElection",  driver, "HORIZONTALLY", strSheetName, null);
						return ;
					}//End of IF condition to check for loading object.
			
				}//End of Catch block for FluentWait
			}
			
	    	//MDCM checkpoints
	    	if(!CommonUtils.isElementPresent(ele_MDCM)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM object is NOT displayed", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM object is NOT displayed,please check..!!");
			}//End of if condition to check if DROPDOWN is displayed
	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM object is displayed", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    	
	    	if(ele_MDCM.isEnabled()){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM dropdown is ENABLED", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM is ENABLED,please check..!!");
	    	}
	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM dropdown is DISABLED(READONLY)", "", "FIElection_MDCM",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		    	
	    	if(!CommonUtils.isElementPresent(ele_MDCM_TaxYear)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM_TaxYear object is NOT displayed", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM_TaxYear object is NOT displayed,please check..!!");
			}//End of if condition to check if DROPDOWN is displayed
	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM_TaxYear object is displayed", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    	
	    	if(ele_MDCM_TaxYear.isEnabled()){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] FIElection->MDCM_TaxYear dropdown is ENABLED", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] FIElection->MDCM_TaxYear is ENABLED,please check..!!");
	    	}
	    	else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] FIElection->MDCM_TaxYear dropdown is DISABLED(READONLY)", "", "FIElection_MDCM_TaxYear",  driver, "BOTH_DIRECTIONS", strSheetName, null);

		}//End of try block
		catch(Exception e){			
    		System.out.println("Exception in <Method: verifySec_FI_DropDowns>,please check..!!");
		}//End of catch block
    }//End of <Method: verifySec_FI_DropDowns>        
      
    //###################################################################################################################################################################  
    //Function name		: getExp_FIPopUp_DropDowns(int exclRowCnt,String strSheetName)
    //Class name		: SearchRes_ActionMenu.java
    //Description 		: Function to verify the drop downs in FI Election Pop up
    //Parameters 		: Excel row number for report and sheet name.
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
      public ArrayList<String> getExp_FIPopUp_DropDowns(String strDropDown){		
    		ArrayList<String> arrExpDropDown = new ArrayList<String>();
    		switch(strDropDown.toUpperCase()){
    			case "BPTD":
    				arrExpDropDown.addAll(Arrays.asList("","Amortize","Do Not Amortize"));
        			return arrExpDropDown;
    			case "MDCM":
    				arrExpDropDown.addAll(Arrays.asList("")); //read only
        			return arrExpDropDown;
    			case "MDR":
    				arrExpDropDown.addAll(Arrays.asList("","At time of Sale/Redemption/Maturity","Current Inclusion"));
        			return arrExpDropDown;
    			case "ICUS":
    				arrExpDropDown.addAll(Arrays.asList("","Period Average","Spot rate on date of payment, credit or receipt"));
        			return arrExpDropDown;
    			case "BPTD_TAXYEAR":
    			case "MDCM_TAXYEAR":
    			case "MDR_TAXYEAR":
    			case "ICUS_TAXYEAR":
    				arrExpDropDown.addAll(Arrays.asList("2014","2015","2016","2017","2018","2019","2020","2021","","From Inception"));
        			return arrExpDropDown;
    			default:
        			return null;
    		}//End of Switch case
      
      }//End of <Method: getExp_FIPopUp_DropDowns>


//###################################################################################################################################################################
}//End of <Class: Searchres_FIELection_Objs> 