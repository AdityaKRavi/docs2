
package PageObjects.Maxit;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Maxit.Maxit_CommonFns;

public class RBIUpload_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	Reporting reporter = new Reporting();

	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	Maxit_CommonFns maxitFns = PageFactory.initElements(driver, Maxit_CommonFns.class);

	//RBI Upload Link	
		@FindBy(how=How.XPATH,using="//a[text()='RBI Upload']")
		public WebElement lnk_RBIUpload;
	
	//Refresh Button
	@FindBy(how=How.ID,using="uploadFile")
    public WebElement btn_ChooseFile;
	
	//CB_Upload_Objs
	@FindBy(how=How.XPATH,using="//a[contains(@href, 'CostBasisImports/Upload/enter.php')]")
	public WebElement lnk_CB_Upload;
	@FindBy(how=How.XPATH,using="//a[contains(@href, 'CostBasisImports/Exceptions/enter.php')]")
	public WebElement lnk_Exceptions;
    @FindBy(how=How.XPATH,using="//*[@class=\"mediumButton\"]")
    public WebElement btn_Upload;
    
    //Basis Upload:
	@FindBy(how=How.XPATH,using="//a[text()='Basis Upload']")
	public WebElement lnk_BasisUpload;
    @FindBy(how=How.XPATH,using="//*[@id=\"processTableTitle\"]")
	public List<WebElement> ele_UploadHeaders; 
	@FindBy(how=How.XPATH,using="//*[@id=\"BulkFileMasterSearchForm\"]")
	public WebElement ele_SearchForm;
    @FindBy(how=How.XPATH,using="//*[@class=\"dataTable\"]")
	public List<WebElement> ele_Upload_table; 
  
    


	//*********************************************************************************************************************  
	//Performing Actions on objects in Search Page:
	//*********************************************************************************************************************      
	//###################################################################################################################################################################  
	//Function name		: RBIUpload_CheckPoints(String strTabName, int exclRowCnt,String strSheetName)
	//Class name		: RBIUpload_Objs
	//Description 		: Function to verify the check points in RBI Upload  page 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
    //Modified by 		: Kavitha [ Added "Upload" button verification and tweeked this to test CB_Upload page also
	//###################################################################################################################################################################
	public void RBIUpload_CheckPoints(String strTabName, int exclRowCnt,String strSheetName){
		try{
			//Auto-583: Kavitha Added checkpoints to verify "Upload file" & "Search Error Report" headers
			if(!(ele_UploadHeaders.size()>0)){
				System.out.println("'Upload file' & 'Search Error Report' titles are NOT displayed in "+strTabName+" page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Upload file' & 'Search Error Report' titles are NOT displayed in "+strTabName+" page.", "", strTabName+"_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else{
				//Checkpoint to verify 'Upload File' title
				if(ele_UploadHeaders.get(0).getText().trim().contains("Upload File"))
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Upload file' title is displayed in "+strTabName+" page.", "", "UploadFile_title",  driver, "HORIZONTALLY", strSheetName, null);
				else
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Upload file' title is NOT displayed in "+strTabName+" page.Expected title='Upload File' and Actual title="+ele_UploadHeaders.get(0).getText().trim(), "", "UploadFile_title",  driver, "HORIZONTALLY", strSheetName, null);
			
				//Checkpoint to verify 'Search Error Report' title
				if(ele_UploadHeaders.get(1).getText().trim().contains("Search Error Report"))
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Search Error Report' title is displayed in "+strTabName+" page.", "", "SearchErrorReport_title",  driver, "HORIZONTALLY", strSheetName, null);
				else
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Search Error Report' title is NOT displayed in "+strTabName+" page.Expected title='Search Error Report' and Actual title="+ele_UploadHeaders.get(1).getText().trim(), "", "SearchErrorReport_title",  driver, "HORIZONTALLY", strSheetName, null);
			
			}//End of Else condition to verify titles.
			
			
			
			//Check if the Choose File button available in Basis Upload page  
			if((CommonUtils.isElementPresent(btn_ChooseFile))){
				System.out.println("'Choose File' Button is available in "+strTabName+" page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Choose File' Button is available in "+strTabName+" page.", "", strTabName+"_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("'Choose File' Button is not available in "+strTabName+" page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Choose File' Button is not available in "+strTabName+" page.", "", strTabName+"_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			
			//Check if the Upload button available in Basis Upload page  
			if((CommonUtils.isElementPresent(btn_Upload))){
				System.out.println("'Upload' Button is available in "+strTabName+" page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Upload' Button is available in "+strTabName+" page.", "", strTabName+"_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("'Upload' Button is not available in "+strTabName+" page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Upload' Button is not available in "+strTabName+" page.", "", strTabName+"_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
			}
			
			//Verify Exceptions link existence if page = CB_UPLOAD
			if(strTabName.equalsIgnoreCase("CB_UPLOAD")){
				if((CommonUtils.isElementPresent(lnk_Exceptions))){
					System.out.println("'Exceptions' Button is available in "+strTabName+" page.");
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Exceptions' Button is available in "+strTabName+" page.", "", strTabName+"_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
				}
				else
				{
					System.err.println("'Exceptions' Button is not available in "+strTabName+" page.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Exceptions' Button is not available in "+strTabName+" page.", "", strTabName+"_CheckPoints",  driver, "HORIZONTALLY", strSheetName, null);
				}
			}//End of IF condition to verify 'Exceptions' link in Upload page
			
			//Added by Kavitha 10/22/2018: AUTO-583 for MX18.6
				//1) Verify Search_Form is displayed
				//2) Verify table header="RBI Upload ID;File Name;Upload Date;Total Records;Success;Error;Process Status;Username"
			
			//Checkpoint: verify Search_Form is displayed
			if(CommonUtils.isElementPresent(ele_SearchForm)){
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Search Error Report' form is displayed in "+strTabName+" page.", "", "SearchErrorReport_Form",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else{
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Search Error Report' form is NOT displayed in "+strTabName+" page,please check..!!", "", "SearchErrorReport_Form",  driver, "HORIZONTALLY", strSheetName, null);
			}
			
			//Checkpoint: Verify table header
			String strAct_Header = maxitFns.get_TableHeader("Basis Upload", ele_Upload_table, 0, "th",exclRowCnt, strSheetName);
			String strExp_Header = this.getExp_TableHeaders("Basis_Upload");
			if(strAct_Header.equalsIgnoreCase(strExp_Header)){
				System.out.println("Upload table coulumns header verification pass.Header:"+strAct_Header);
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Basis Upload' table header verification pass. Header columns:"+strAct_Header, "", "UploadTable_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}//End of IF condition to verify table header
			else{
				System.out.println("Upload table coulumns header verification failed,Actual Header:"+strAct_Header+" and Expected Header:"+strExp_Header);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Basis Upload' table header verification failed. Actual Header:"+strAct_Header+" and Expected Header:"+strExp_Header, "", "UploadTable_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}//End of else condition to verify UploadTable_Header
			

		}//End of Try block
		catch(Exception e){
			System.err.println("Exception in <RBIUpload_Objs.RBIUpload_CheckPoints>: CBManger->RBIUpload page verifying header title.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in "+strTabName+":<RBIUpload_Objs.RBIUpload_CheckPoints>,please check..!!", "", strTabName+"_CheckPoints",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of catch block
	}//End of <Method: RBIUpload_CheckPoints>     	

	  //###################################################################################################################################################################  
	  //Function name		: getExp_TableHeaders(String strTablename)
	  //Class name			: RBI_Upload_Objs
	  //Description 		: Function to get an array of expected table header
	  //Parameters 			: 
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	      public String getExp_TableHeaders(String strTablename){		
	    		switch(strTablename.toUpperCase()){
	    			case "BASIS_UPLOAD":
	    				return "RBI Upload ID;File Name;Upload Date;Total Records;Success;Error;Process Status;Username";

	    			default:
	        			return null;
	    		}//End of Switch case
	      
	      }//End of <Method: getExp_TableHeaders> 	
	
	
}//End of <Class: RBIUpload_Objs>
