package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.Maxit.SearchPageFns;
import functionalLibrary.Maxit.SearchRes_PopUpFns;

public class NoCostBasis_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	//SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
//	SearchRes_PopUpFns objRes_Action = PageFactory.initElements(driver, SearchRes_PopUpFns.class);
	//SearchPageFns objSearchPageFns = PageFactory.initElements(driver, SearchPageFns.class);
	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/NoCostBasis/enter.php']")
    private WebElement lnk_NCB;
    @FindBy(how=How.ID,using="ageDays")
    public WebElement lst_Range;
    @FindBy(how=How.ID,using="seedAcatFilter")
    public WebElement lst_ACATFilter;
    @FindBy(how=How.XPATH,using="//*[@id=\"searchAccount\"]")
	public WebElement txtbx_Acct;
    @FindBy(how=How.ID,using="securityIDType")
    public WebElement lst_SecIDType;
    @FindBy(how=How.ID,using="securityID")
    public WebElement txtbx_SecurityID;
    @FindBy(how=How.XPATH,using="//*[@class=\"mediumButton\"]")
    public WebElement btn_Submit;
    @FindBy(how=How.ID,using="noCostBasisTable")
    public WebElement ele_ResTable;
    @FindBy(how=How.XPATH,using="//*[@id=\"noCostBasisTable\"]/thead")
    public List<WebElement> ele_ResTbl_Header;
    @FindBy(how=How.XPATH,using="//*[@id=\"noCostBasisTable\"]/tbody")
    public List<WebElement> ele_ResTbl_Body;
    @FindBy(how=How.XPATH,using="//*[@id=\"noData\"]")
    public WebElement ele_NoResults;
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverTarget_ncb\")]")
    public List<WebElement> ele_NCBAction;
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverTarget_ncb0\")]")
    public List<WebElement> ele_NCBAction_Row1;
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverMenu_ncb\")]")
    public List<WebElement> ele_NCBActionMenu;
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverMenu_ncb0\")]")
    public List<WebElement> ele_NCBActionMenu_Row1;
  
    
    //*********************************************************************************************************************  
    //Performing Actions on objects in Search Page:
   //*********************************************************************************************************************   
    //###################################################################################################################################################################  
    //Function name		: Navigate_NCB(int exclRowCnt,String strSheetName)
    //Class name		: NoCostBasis_Objs
    //Description 		: Function to Navigate to "No Cost Basis" Page
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
    	public boolean Navigate_NCB(int exclRowCnt,String strSheetName){
    		//CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
    		try{  			
    			if(CommonUtils.isElementPresent(lnk_NCB)){
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Navigating to 'No Cost Basis' page", "", "NCB_Page",  driver, "HORIZONTALLY", strSheetName, null);
    				lnk_NCB.click();
    				
    				//Verify page heading header:
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_PageHeader)){
    					String strAct_Header = CBManager_Objs.ele_PageHeader.getAttribute("textContent");
    					if(strAct_Header.contains("No Cost Basis")){
    						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] No Cost Basis page header/title is as expected, Expected Header:No Cost Basis", "", "NCB_Header",  driver, "HORIZONTALLY", strSheetName, null);
    						return true;
    					}
    					else{
    						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No Cost Basis page header/title is not as expected, Actual header:"+strAct_Header+" and Expected Header:No Cost Basis", "", "NCB_Header",  driver, "HORIZONTALLY", strSheetName, null);
    						return false;
    					}
    				}//End of if condition to verify page heading
    				else {
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No Cost Basis page header/title object is not displayed, please check..!", "", "NCB_Header",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}
    			}//End of IF condition to check if No Cost Basis link exists in left navigation panel.
    			else{
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No Cost Basis link is not displayed in left navigation panel of page, please check..!", "", "NCB_Header",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
    			}
    					
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs.Navigate_NCB>: No Cost Basis Link is not displayed in UI..!!!");
    			reporter.reportStep(exclRowCnt, "Failed", "NCB_Exception in <NoCostBasis_Objs.Navigate_NCB> Please Check..!!", "", "NCB_Exception",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    		}
    	}//End of Navigate_NCB Method	    
    
    
    //###################################################################################################################################################################  
    //Function name		: NCB_Errors(int exclRowCnt,String strSheetName)
    //Class name		: NoCostBasis_Objs
    //Description 		: Function to verify if there are any error messages in No Cost Basis page
    //Parameters 		: 
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean NCB_Errors(String strTabName,int exclRowCnt,String strSheetName){
    		try{
				//Check if there are any error messages:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in No Cost Basis page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No Cost Basis page_ErrorMessage:"+strErMsg, "", "NCB_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.
				return true;
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs.NCB_Errors>: NoCostBasis verifying for error messages.");
    			return false;
    		}//End of catch block
    	}//End of <Method: NCB_Errors>
    	
    //###################################################################################################################################################################  
    //Function name		: NCB_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName)
    //Class name		: NoCostBasis_Objs
    //Description 		: Function to perform BLANK search in NCB page 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean NCB_Search(String strAcct,String strSecType,String strSecValue,int exclRowCnt,String strSheetName){
    		LocalDateTime strStartWaitTime,strEndWaitTime;
    		String strSearchValues;
    		try{
    			boolean blnListCheck = false;
    			boolean blnFilterListCheck = false;
    			boolean blnSecTypeCheck = false;
    			
    			strSearchValues = "[NCB;Acct:"+strAcct+";"+strSecType+";"+strSecValue+"]";
    			txtbx_Acct.sendKeys(strAcct);
    			List<WebElement> SecType_List = lst_SecIDType.findElements(By.tagName("option"));
    			for (WebElement option : SecType_List) {
    				if(option.getText().equals(strSecType)){
    					option.click();
    					blnSecTypeCheck=true;
    					break;
    				}
    			}//End of FOR loop
    			if(!blnSecTypeCheck){
    				for (WebElement option : SecType_List) {
    					if(option.getText().equals("Security No")){
    						System.out.println("Selecting default SecIDType as Security No");
    						option.click();
    						blnSecTypeCheck=true;
    						break;
    					}
    				}//End of FOR loop
    			}//End of IF condition to select default SecIDTpe= Security ID

    			txtbx_SecurityID.sendKeys(strSecValue);

    			List<WebElement> Range_List = lst_Range.findElements(By.tagName("option"));
    			for (WebElement option : Range_List) {
    				if(option.getText().equals("All")){
    					option.click();
    					blnListCheck=true;
    					break;
    				}
    			}//End of FOR loop
    			if(!blnListCheck){
    				System.out.println("Range dropdown doesn't have 'All' in the list, default value selected.");
    			}//End of IF condition to select default Range

    			List<WebElement> Filter_List = lst_ACATFilter.findElements(By.tagName("option"));
    			for (WebElement option : Filter_List) {
    				if(option.getText().equals("All")){
    					option.click();
    					blnFilterListCheck=true;
    					break;
    				}
    			}//End of FOR loop
    			if(!blnFilterListCheck){
    				System.out.println("Filter dropdown doesn't have 'All' in the list, default value selected.");
    			}//End of IF condition to select default Filter
    			
    			btn_Submit.click();
    			
    			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
    			strStartWaitTime = LocalDateTime.now();
    			try{
    				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
    				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    										wait.withTimeout(35,TimeUnit.SECONDS)
    										.pollingEvery(2, TimeUnit.SECONDS)				 
    										.ignoring(NoSuchElementException.class)
    										.until(ExpectedConditions.visibilityOf(ele_ResTable));
    			}//End of try block for FluentWait
    			
    			catch(org.openqa.selenium.TimeoutException e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				
    				//Check if page is loading:			
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading)){
    					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB Search after input"+strSearchValues+":Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of IF condition to check for loading object.

    				//Checkpoint: Check for Errors:
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.				
    			}//End of Catch block for FluentWait
    			catch(Exception e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				return false;
    			}//End of catch block with generic exception for Fluent Wait 
    			
    			//Checkpoint: Check for Errors:
    			if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    				String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    				System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    				return false;
    			}//End of if condition to to see if there are errors.
    			
    			//Check for Results header
    			if(CommonUtils.isElementPresent(ele_ResTable)){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Checkpoint : NCB Page Navigation - [Passed]");
    				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] NCB Search After Input "+strSearchValues+":Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				//System.out.println(LocalDateTime.now());
    				return true;
    			}//End of IF condition to check for Results table header.
    			
    			
    			
    			
    			
    			
    			System.out.println("Checkpoint : NCB Search After Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB Search after input:"+strSearchValues+"Failed, Results header is not displayed", "", "NCBSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    			
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs.NCB_Search>: NoCostBasis-> verifying header title.");
    			return false;
    		}//End of catch block
    	}//End of <Method: NCB_Search> 


    //###################################################################################################################################################################  
    //Function name		: NCB_BlankSearch(int exclRowCnt,String strSheetName)
    //Class name		: NoCostBasis_Objs
    //Description 		: Function to perform BLANK search in NCB page 
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################
    	public boolean NCB_BlankSearch(int exclRowCnt,String strSheetName){
    		//CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
    		LocalDateTime strStartWaitTime,strEndWaitTime;
    		try{
    			btn_Submit.click();
    			
    			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
    			strStartWaitTime = LocalDateTime.now();
    			try{
    				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
    				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    										wait.withTimeout(35,TimeUnit.SECONDS)
    										.pollingEvery(2, TimeUnit.SECONDS)				 
    										.ignoring(NoSuchElementException.class)
    										.until(ExpectedConditions.visibilityOf(ele_ResTable));
    			}//End of try block for FluentWait
    			
    			catch(org.openqa.selenium.TimeoutException e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				
    				//Check if page is loading:			
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading)){
    					System.out.println("Checkpoint : NCB Blank Search - Stil Loading..[Failed]");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB Blank Search :Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of IF condition to check for loading object.

    				//Checkpoint: Check for Errors:
    				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in NCB Search Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.				
    			}//End of Catch block for FluentWait
    			catch(Exception e){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				return false;
    			}//End of catch block with generic exception for Fluent Wait 
    			
    			//Checkpoint: Check for Errors:
    			if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
    				String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
    				System.out.println("Checkpoint : Error in NCB Search Page, Error Message is - "+strErMsg);
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
    				return false;
    			}//End of if condition to to see if there are errors.
    			
    			//Check for Results header
    			if(CommonUtils.isElementPresent(ele_ResTable)){
    				strEndWaitTime = LocalDateTime.now();
    				System.out.println("Checkpoint : NCB Page Navigation - [Passed]");
    				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] NCB Blank Search Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    				//System.out.println(LocalDateTime.now());
    				return true;
    			}//End of IF condition to check for Results table header.

    			System.out.println("Checkpoint : NCB Search After Input data insert - [Failed]");
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB Blank Search :Failed, Results header is not displayed", "", "NCBSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
    			return false;
    			
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs.NCB_BlankSearch>: NoCostBasis-> verifying header title.");
    			return false;
    		}//End of catch block
    	}//End of <Method: NCB_BlankSearch> 

    //###################################################################################################################################################################  
    //Function name			: get_NCBTableHeader(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
    //Class name			: NoCostBasis_Objs
    //Description 			: Function to return table header text
    //Parameters 			: Web table as a list object, the index of the table if multiple are displayed, excel row count and excel sheet for reporting.
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
    	public String get_NCBTableHeader(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
			List<WebElement> tbl_Header= eleTableName;
    		String strHeaderRow="";
    		try{  	    		
    			if(eleTableName.isEmpty()){
    				System.out.println("<Class: NoCostBasis_Objs><Method: get_NCBTableHeader>: eleTableName parameter passed is empty, please check..!!");
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<NoCostBasis_Objs.get_NCBTableHeader>!", "", "eleTableName",  driver, "HORIZONTALLY", strSheetName, null);
    				return "";
    			}
    			if((tbl_Header.size()>0) && (tbl_Header.size()>=tbl_index)){
    				List<WebElement> getCols = tbl_Header.get(tbl_index).findElements(By.tagName("th"));
          			for (WebElement eachCol : getCols) {  
          				strHeaderRow = strHeaderRow+eachCol.getAttribute("textContent").trim()+";";		
          			}//End of FOR loop to read header column names
          			return strHeaderRow;
    			}//End of IF condition to check if table exists
    			else{
    				System.out.println("<Class: NoCostBasis_Objs><Method: get_NCBTableHeader>: Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index);
    				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Table Header size in the browser is "+tbl_Header.size()+" and given index to read the header is "+tbl_index+",please check method<NoCostBasis_Objs.get_NCBTableHeader>!", "", "Tble_Header",  driver, "HORIZONTALLY", strSheetName, null);
    				return "";
    			}//End of IF condition to check if table exists
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs.get_NCBTableHeader>: Table Header size in the browser is not as expected..!!!");
    			e.printStackTrace();
    			return null;
    		}
    	}//End of get_NCBTableHeader Method
    	
  //###################################################################################################################################################################  
  //Function name		: get_NCB_ResRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
  //Class name			: NoCostBasis_Objs
  //Description 		: Function to total number of rows in the given webtable
  //Parameters 			: Search Page name
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
  	public int get_NCB_ResRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
  		try{  	
  			if(eleTableName.isEmpty()){
  				System.out.println("<Class: NoCostBasis_Objs><Method: get_NCB_ResRowCount>: eleTableName parameter passed is empty, please check..!!");
  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<NoCostBasis_Objs.get_NCB_ResRowCount>!", "", "WebTableRowCount",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  				return 0;
  			}
  			if(!((eleTableName.size()>0) && (eleTableName.size()>=tbl_index)) ){
  				System.out.println("<Class: NoCostBasis_Objs><Method: get_NCB_ResRowCount>: Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index);
  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index+",please check method<NoCostBasis_Objs.get_NCB_ResRowCount>!", "", "WebTableRowCount",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  				return 0;
  			}//End of IF Condition to check if eleTableName element exists
  			
  			List<WebElement> app_rows = eleTableName.get(tbl_index).findElements(By.tagName("tr"));
  			try{
  				if(ele_NoResults.isDisplayed()) return 0;
  			}
  			catch(org.openqa.selenium.NoSuchElementException e){
  				return app_rows.size(); 
  			}
  			return app_rows.size();  			
  			
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <NoCostBasis_Objs.get_NCB_ResRowCount>,please check.!!");
  			e.printStackTrace();
  			return 0;
  		}
  	}//End of get_NCB_ResRowCount Method

    //###################################################################################################################################################################  
    //Function name			: NCB_NoResultsReturned(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
    //Class name			: NoCostBasis_Objs
    //Description 			: Function to total number of rows in the given webtable
    //Parameters 			: Search Page name
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
  	public String NCB_NoResultsReturned(){
  		try{  	
  			String strResValue;  				
  			if(CommonUtils.isElementPresent(ele_NoResults)){
  				strResValue = ele_NoResults.getAttribute("textContent");
  				if(strResValue.contains("No Results Found")) strResValue ="No Results Found";
  				return 	strResValue;
  			}//End of IF Condition to check if ele_ResultsTable element exists
  			System.out.println("<Class: NoCostBasis_Objs><Method: NCB_NoResultsReturned>: No Results found checkpoint fail.");
  			strResValue = "No Results found checkpoint fail";
  			return strResValue;  			
  			
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <NoCostBasis_Objs.NCB_NoResultsReturned>: NCB Search Results Table 'No Results Found' is not displayed in UI..!!!");
  			e.printStackTrace();
  			return null;
  		}
  	}//End of NCB_NoResultsReturned Method
  	
  	
    //###################################################################################################################################################################  
    //Function name			: NCBRes_getCellData(int rowNumber,String colName)
    //Class name			: NoCostBasis_Objs
    //Description 			: Function to get cell data from NCB Search results based on Row# and column name
    //Parameters 			: rowNumber
    //						: Column Name	
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
  	public String NCBRes_getCellData(int rowNumber,String colName){
    		try{  			
    			if(!CommonUtils.isElementPresent(ele_ResTable)){
    				System.out.println("NCB_Results Table is not displayed,please check..!!");
    				return null;	
    			}//End of IF Condition to check if ele_ResTable element exists
    			
    			int colIndex = this.NCBRes_GetColIndexByColName(colName);
    			List<WebElement> app_rows = ele_ResTbl_Body.get(0).findElements(By.tagName("tr"));
    			List<WebElement> app_cols = app_rows.get(rowNumber).findElements(By.tagName("td"));    			
    			String strColValue = app_cols.get(colIndex).getAttribute("textContent");
    			return strColValue;
    			
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs.NCBRes_getCellData>: Search Results Table is not displayed in UI..!!!");
    			return "";
    		}
  	}// End of searchRes_getCellData method.

  //###################################################################################################################################################################  
  //Function name			: NCBRes_GetColIndexByColName(String strColName)
  //Class name				: NoCostBasis_Objs
  //Description 			: Function to read number of records retrieved from search result
  //Parameters 				: 
  //Assumption				: None
  //Developer				: Kavitha Golla
  //###################################################################################################################################################################		
  	public int NCBRes_GetColIndexByColName(String strColName){
    		try{  			
    			if(!(CommonUtils.isElementPresent(ele_ResTable) && (ele_ResTbl_Header.size()>0))){
    				System.out.println("NCB Search_Results Table Header is not displayed,please check..!!");
    				return 0;	
    			}//End of IF Condition to check if ele_ResTbl_Header element exists
    			
    			List<WebElement> getCols = ele_ResTbl_Header.get(0).findElements(By.tagName("th"));
    			
    			int iColLoop = 0;
    			for (WebElement eachCol : getCols) {  		
    				if (eachCol.getAttribute("textContent").trim().equalsIgnoreCase(strColName)){ 					
    					Integer colIndex = iColLoop;
    					return colIndex;
    				}//End of IF condition to check column name 
    				iColLoop++;
    			}//End of FOR loop to get Column Index
    			
    			System.out.println("<NoCostBasis_Objs><NCBRes_GetColIndexByColName>: NCB Search Results Table HEADER, Expected Column Name:"+strColName+" is not displayed in UI..!!!");
    			return -1;
    		}//End of Try block
    		catch (NumberFormatException e) {
    			System.out.println("NumberFormatException in <NoCostBasis_Objs><NCBRes_GetColIndexByColName>: Column Index for column name: "+strColName+" is not Integer");
    			return -1;
    		}
    		catch(Exception e){
    			System.out.println("Exception in <NoCostBasis_Objs><NCBRes_GetColIndexByColName>: Search Results Table HEADER is not displayed in UI..!!!");
    			return -1;
    		}  		
  		
  	}//End of NCBRes_GetColIndexByColName method
  	
  //###################################################################################################################################################################  
  //Function name		: NCBRes_ClickAction(String strDesiredPageLink)
  //Class name			: NoCostBasis_Objs
  //Description 		: Function to click on the action menu item
  //Parameters 			: Desired action menu item & which side of the action button to be clicked
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
  		public boolean NCBRes_ClickAction(String strDesiredPageLink){
  			try{
  				if(!this.NCBRes_VerifyActionButtonExists()){
  					System.out.println("<Method: NCBRes_ClickAction >NCB Search_Results Table Action button is NOT displayed,please check..!!");
  					return false;
  				}//End of IF condition to check if ActionButton Exists

  				//Modified [ 03/20/2018] : Removing Verifying Action menu list as per AUTO-417, instead adding checkpoint to verify if desired page exists or not in the Action Menu
//  				if(!this.NCB_verifyActionMenuList()){
//  					System.out.println("<Method: NCBRes_ClickAction >NCB Search_Results Table Action Menu list is NOT displayed,please check..!!");
//  					return false;
//  				}//End of IF condition to check if Action menu List Exists
  		
  				//Click on desired page link from Action menu list:
  				for (WebElement eachActionList : this.ele_NCBActionMenu_Row1.get(0).findElements(By.tagName("li"))) { 
  					if(eachActionList.getAttribute("textContent").equalsIgnoreCase(strDesiredPageLink)){
  						eachActionList.click();
  						Thread.sleep(3000);						
  						return true;
  					}
  				}//End of for loop to check Desired Page Link from Action Menu list
  				System.out.println("<Method: NCBRes_ClickAction >Action menu List doesn't display desired page link "+strDesiredPageLink+" ,please check..!!");
  				return false;
  		
  			}//End of Try block
  			catch(Exception e){
  				System.out.println("Exception in <Class: NoCostBasis_Objs ><Method: NCBRes_ClickAction > Please check..!!");
  				return false;
  			}//End of catch block
  		}//End of <Method: NCBRes_ClickAction>

  		
  		
  //###################################################################################################################################################################  
  //Function name		: NCBRes_VerifyActionButtonExists(){
  //Class name			: NoCostBasis_Objs
  //Description 		: Function to verify if Action button exists
  //Parameters 			: 
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		  	  		  		
    public boolean NCBRes_VerifyActionButtonExists(){
    	try{   
    		List<WebElement> ele_ActionButton;
			ele_ActionButton = this.ele_NCBAction_Row1;
			
			if(ele_ActionButton.size()>0){
				if(!CommonUtils.isElementPresent(ele_ActionButton.get(0))){
	  				System.out.println("Checkpoint :[Failed] NCB Search_Results Table,Left Action button on row1 is NOT displayed,please check..!!");
	  				return false;
				}//End of if condition to check if Left Action button on row1 in UI is displayed.
				else System.out.println("Checkpoint :[Passed] NCB Search_Results Table,Left Action button on row1 is displayed.");

				ele_ActionButton.get(0).click();
				Thread.sleep(5000);
				if(!(ele_NCBActionMenu.size()>0)){
	  				System.out.println("Checkpoint :[Failed] NCB Search_Results Table Action Menu List is NOT displayed,please check..!!");
	  				return false;						
				}//End of if condition to check if Action Menu List is displayed
				System.out.println("Checkpoint :[Passed] NCB Search_Results Table Action Menu List is displayed");
				return true;
			}//End of if condition to check if Right Action button is displayed
			else{
  				System.out.println("Checkpoint :[Failed] NCB Search_Results Table Action button is NOT displayed,please check..!!");
  				return false;
			}//End of else condition to check  if Action Button on right side is displayed
				
    	}//End of Try block
    	catch(Exception e){
    		System.out.println("Exception in <Class: NoCostBasis_Objs ><Method: NCBRes_VerifyActionButtonExists>NCB Search_Results Table Action button on Left side is NOT displayed,please check..!!");
    		return false;    		
    	}//End of catch block
    	
    }//End of <Method: NCBRes_VerifyActionButtonExists>  		
  		
//###################################################################################################################################################################  
//Function name			: NCB_verifyActionMenuList(){
//Class name			: NoCostBasis_Objs
//Description 			: Function to verify if Action Menu exists and verify Action Menu
//Parameters 			: 
//Assumption			: None
//Developer				: Kavitha Golla
//###################################################################################################################################################################		  	  		  		
    public boolean NCB_verifyActionMenuList(){
		List<String> actActionList = new ArrayList<String>();
		ArrayList<String> expActionList = new ArrayList<String>();
		expActionList.addAll(Arrays.asList("Ledger","TRD","RAD","Tax Lot Edit"));
		
		if(this.ele_NCBActionMenu_Row1.size()>0){	
			List<WebElement> app_ActionList = this.ele_NCBActionMenu_Row1.get(0).findElements(By.tagName("li"));
			for (WebElement eachActionList : app_ActionList) { 
				actActionList.add(eachActionList.getAttribute("textContent").trim());
			}//End of for loop to read actual Action menu list
			if(!actActionList.toString().equals(expActionList.toString())){
				System.out.println("Checkpoint :[Failed] NCB Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString()+"; Actual Action menu List = "+actActionList.toString());
				return false;
			}//End of IF condition to check Expected Action Menu List & Actual Action Menu List
			System.out.println("Checkpoint :[Passed] NCB Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString());
			return true;
		}//End of IF condition to check Menu List object exists
		
		System.out.println("<Method: NCB_verifyActionMenuList>Checkpoint_Fail:Action Menu list object in NCB search Results is NOT displayed, please check...!!!");
		return false;
    }//End of <Method: NCB_verifyActionMenuList>    
  		


//###################################################################################################################################################################  
//Function name		: NCB_BlankSearch_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
//Class name		: NoCostBasis_Objs
//Description 		: Function to list the methods for verifying No Cost Basis page when there is "Blanks Search" in NCB page
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public  void NCB_BlankSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strClient ) throws Exception{
		SearchRes_PopUpFns objRes_Action = PageFactory.initElements(driver, SearchRes_PopUpFns.class);
		
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		String NCB_strAcct;
		try{		
			strSearchValues = "[NCB;Blank Search]";
			//Perform Blank search in NCB page
			strRet = this.NCB_BlankSearch(exclRowCnt,strSheetName);

			//Check for NCB Search results
			if(strRet){					
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = this.get_NCB_ResRowCount(this.ele_ResTbl_Body, 0, exclRowCnt, strSheetName);
				if(!(appRowsReturned>0)){
					if(this.NCB_NoResultsReturned().equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Warning] NCB Blank Search using is: 'No Resuls found',so TLE Verification is skipped.!", "", "NCBSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
						NCB_strAcct = "";
						strBlankSearch = "Y";
						
						//call method to perform blank search in Search_NCB page:
						this.Search_NCB_Flow(exclRowCnt,"No Cost Basis",To_Execute,NCB_strAcct,strSecType, strSecValue,strBlankSearch );
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB Blank Search Results rows not >0, Not as expected 'No results Found'", "", "NCBSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				
				
				
				else{
					//Read account & Sec number from NCB search results
					ArrayList<String> arrNCBAcct = new ArrayList<String>();
					for(int i=0;i<appRowsReturned;i++){
						arrNCBAcct.add(this.NCBRes_getCellData(i, "Account"));
						//NCB_strAcct = this.NCBRes_getCellData(0, "Account");
					}//End of For loop to read total Accounts from NCB Search results.
					
					//Get unique list from the accounts array list.
					arrNCBAcct = new ArrayList<String>(new LinkedHashSet<String>(arrNCBAcct));
					
					//Navigate to TLE and perform TLE Generic checkpoints
						objRes_Action.Navigate_TLE(exclRowCnt, strSheetName, 1, "Tax Lot Edit", "leftaction", "No Cost Basis", "",strClient);
					//To ensure the pop up is closed.
		        	if(CBM_Objs.btn_CBMXClose.size()>0){
		        		CBM_Objs.btn_CBMXClose.get(0).click();
		        	}
					//Call Search_NCB_Flow() with search account
					this.Search_NCB_Flow(exclRowCnt,"No Cost Basis",To_Execute,arrNCBAcct,strSecType, strSecValue,"N" );
				}//End of ELSE condition to check No of rows returned>0
			}//End of IF condition to check NCB Blank Checkpoint
					
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: NCB_BlankSearch_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in NCB_BlankSearch_Flow ", "", "NCB_BlankSearch_Flow",  driver, "HORIZONTALLY", strSheetName, null);
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: NCB_BlankSearch_Flow>	

//###################################################################################################################################################################  
//Function name		: NCB_DataSearch_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
//Class name		: NoCostBasis_Objs
//Description 		: Function to list the flow of NCB & Search_NCB when there is test data
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public  void NCB_DataSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch,String strClient ) throws Exception{
		SearchRes_PopUpFns objRes_Action = PageFactory.initElements(driver, SearchRes_PopUpFns.class);
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			//Perform Data search in NCB page
			strSearchValues = "[NCB;"+strAcct+";"+strSecType+";"+strSecValue+"]";
			strRet = this.NCB_Search(strAcct, strSecType, strSecValue, exclRowCnt, strSheetName);

			//Check for NCB Search results
			if(strRet){					
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = this.get_NCB_ResRowCount(this.ele_ResTbl_Body, 0, exclRowCnt, strSheetName);
				if(!(appRowsReturned>0)){
					if(this.NCB_NoResultsReturned().equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Warning] NCB Search for "+strSearchValues+" is: 'No Resuls found',So TLE verification is skipped. Please test Manually.", "", "NCBSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
						
						//call method to perform Data search in Search_NCB page:
						this.Search_NCB_Flow(exclRowCnt,"No Cost Basis",To_Execute,strAcct,strSecType, strSecValue,strBlankSearch );
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] NCB Search for "+strSearchValues+": Results rows not >0, Not as expected 'No results Found'", "", "NCBSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned
				
				
				
				else{
					//Navigate to TLE and perform TLE Generic checkpoints
					objRes_Action.Navigate_TLE(exclRowCnt, strSheetName, 1, "Tax Lot Edit", "leftaction", "No Cost Basis", "",strClient);
					//To ensure the pop up is closed.
		        	if(CBM_Objs.btn_CBMXClose.size()>0){
		        		CBM_Objs.btn_CBMXClose.get(0).click();
		        	}
					//Call Search_NCB_Flow() with search account
		        	this.Search_NCB_Flow(exclRowCnt,"No Cost Basis",To_Execute,strAcct,strSecType, strSecValue,strBlankSearch );
				}//End of ELSE condition to check No of rows returned>0
			}//End of IF condition to check NCB Blank Checkpoint
					
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: NCB_DataSearch_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in NCB_DataSearch_Flow ", "", "NCB_DataSearch_Flow",  driver, "HORIZONTALLY", strSheetName, null);
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: NCB_DataSearch_Flow>		
	
//###################################################################################################################################################################  
//Function name		: Search_NCB_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
//Class name		: NoCostBasis_Objs
//Description 		: Function to list the methods for verifying Search_NCB_Flow page
//Parameters 		: 
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public  void Search_NCB_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		SearchPageFns objSearchPageFns = PageFactory.initElements(driver, SearchPageFns.class);
		SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
		
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			//objSearchPageFns = new SearchPageFns(); 
			
			if(strBlankSearch.equalsIgnoreCase("Y")) {
				strSearchValues = "["+strViewPage+";Blank Search]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				reporter.reportStep(exclRowCnt, "Passed", "\n"+"Checkpoint :[Warning] Navigating to 'Search_NoCostBasis' page to perform 'Blank Search'.", "", "NCB_BlankSearch",  driver, "HORIZONTALLY", strSheetName, null);
				strRet = objSearchPageFns.Search_Input_Blank(strViewPage , exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			else{
				strSearchValues = "["+strViewPage+";"+strAcct+"]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				reporter.reportStep(exclRowCnt, "Passed", "\n"+"Checkpoint :[Passed] Navigating to 'Search_NoCostBasis' page to perform 'Search using search values:"+strSearchValues+"'.", "", "NCB_AcctSearch",  driver, "HORIZONTALLY", strSheetName, null);
				strRet = objSearchPageFns.Search_Input(strViewPage , strAcct , strSecType,strSecValue,exclRowCnt,strSheetName);
			}//End of IF condition to check BlankSearch=Y
			
				if(strRet){
					//Verify search results row number, if No Results returned fail the case.		
					int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
					if(!(appRowsReturned>0)){
						if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Failed", "\nCheckpoint :[Failed] 'Search_NoCostBasis' No Results found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return;
						}
						System.out.println("Search Results for "+strSearchValues+" is not >0");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Search_NoCostBasis' Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}//End of IF condition to check No of rows returned
					else{
						System.out.println("Search Results for "+strSearchValues+" is >0");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Search_NoCostBasis' Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;	
					}


				}//End of IF condition to check Search_Input Checkpoint
					
			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_NCB_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_NCB_Flow ", "", "Search_NCB_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_NCB_Flow>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_NCB_Flow>		

//###################################################################################################################################################################  
//Function name		: Search_NCB_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
//Class name		: NoCostBasis_Objs
//Description 		: Function to list the methods for verifying Search_NCB_Flow page
//Parameters 		: 
//Assumption		: <Method Overloading> with Array list for account number.
//Developer			: Kavitha Golla
//###################################################################################################################################################################			
	public  void Search_NCB_Flow(int exclRowCnt,String strViewPage,String To_Execute,ArrayList<String> strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
		SearchPageFns objSearchPageFns = PageFactory.initElements(driver, SearchPageFns.class);
		
		String strSheetName = "SmokeData";
		String strSearchValues,strNCBAcct;
		int iLoopMax;
		boolean strRet;
		try{	
			//Loop for max of 5 accounts in strAcct ArrayList to perform search in Search_NoCostBasis page.
			if(strAcct.size()>5) iLoopMax = 5; 
			else iLoopMax=strAcct.size();
			
			for(int i=0;i<iLoopMax;i++){
				strNCBAcct = strAcct.get(i);
			//for(String strNCBAcct:strAcct){				
				strSearchValues = "["+strViewPage+";"+strNCBAcct+"]";
				strRet = objSearchPageFns.Search_Input(strViewPage , strNCBAcct , strSecType,strSecValue,exclRowCnt,strSheetName);
				if(strRet){
					//Verify search results row number, if No Results returned fail the case.		
					int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);
					if(!(appRowsReturned>0)){
						if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Failed", "\n Checkpoint :[Failed] 'Search_NoCostBasis' for "+strSearchValues+" is: No Results found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						}
					}//End of IF condition to check No of rows returned
					else{
						System.out.println("Search Results for "+strSearchValues+" is >0");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] 'Search_NoCostBasis' Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						break;
					}
				}//End of IF condition to check Search_Input Checkpoint
				
			}//End of FOR loop to traverse thru each account until data is found or all accounts are searched.

		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: Search_NCB_Flow[Method Overload with StrAccount Array List]>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in Search_NCB_Flow[Method Overload with StrAccount Array List]", "", "Search_NCB_Flow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			System.out.println("Exception in Smoke Test Flow <Method: Search_NCB_Flow[Method Overload with StrAccount Array List]>,please check..!!");
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: Search_NCB_Flow>		
	
}//End of <class: NoCostBasis_Objs>
