package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.Maxit.SearchPageFns;

public class PositionRecon_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	String posReconAccountNum;

	public static String POSITION_RECON_PAGE = "Position Recon";
	public static String SEARCH_POSITION_RECON_VIEW = "Pos Recon";

	CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/PositionRecon/enter.php']")
	private WebElement lnk_posRecon;

	@FindBy(how=How.XPATH,using="//*[@id=\"searchAccount\"]")
	public WebElement txtbx_Acct;
	@FindBy(how=How.ID,using="securityIDType")
	public WebElement lst_SecIDType;
	@FindBy(how=How.ID,using="securityID")
	public WebElement txtbx_SecurityID;
	@FindBy(how=How.XPATH,using="//*[@class=\"mediumButton\"]")
	public WebElement btn_Submit;

	@FindBy(how=How.XPATH,using="//*[@id=\"noData\"]")
	public WebElement ele_NoResults;

	@FindBy(how=How.XPATH,using="(//table[@class='dataTable'])[1]")
	public WebElement ele_PositionReconSummaryTbl;

	@FindBy(how=How.XPATH,using="(//table[@class='dataTable']/tbody)[1]")
	public List<WebElement> ele_PositionReconSummaryTbl_Body;

	@FindBy(how=How.XPATH,using="(//table[@class='dataTable']/thead)[1]")
	public List<WebElement> ele_PositionReconSummaryTbl_Header;

	@FindBy(how=How.XPATH,using="//span[text()='Position Recon Summary']")
	public WebElement ele_PositionReconSummaryText;

	@FindBy(how=How.ID,using="positionReconTable")
	public WebElement ele_PosReconResTable;
	@FindBy(how=How.XPATH,using="//*[@id=\"positionReconTable\"]/thead")
	public List<WebElement> ele_PosReconResTbl_Header;
	@FindBy(how=How.XPATH,using="//*[@id=\"positionReconTable\"]/tbody")
	public List<WebElement> ele_PosReconResTbl_Body;



	//*********************************************************************************************************************  
	//Performing Actions on objects in Position Recon Page:
	//*********************************************************************************************************************   
	//###################################################################################################################################################################  
	//Function name		: Navigate_PosRecon(int exclRowCnt,String strSheetName)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to Navigate to "Position Recon" Page
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public boolean Navigate_PosRecon(int exclRowCnt,String strSheetName){
		try{  			
			System.out.println(" Inside Navigate_PosRecon : ");
			// Checking the position Recon link in Web page
			if(CommonUtils.isElementPresent(lnk_posRecon)){
				reporter.reportStep(exclRowCnt, "Passed", "Navigating to 'Position Recon' page", "", "PosRecon_Page",  driver, "HORIZONTALLY", strSheetName, null);
				//Clicking the link
				lnk_posRecon.click();

				//Verify page heading header:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_PageHeader)){
					String strAct_Header = CBManager_Objs.ele_PageHeader.getAttribute("textContent");
					if(strAct_Header.contains("Position Recon")){
						reporter.reportStep(exclRowCnt, "Passed", "Position Recon page header/title is as expected, Expected Header:Position Recon", "", "PosRecon_Header",  driver, "HORIZONTALLY", strSheetName, null);
						return true;
					}
					else{
						reporter.reportStep(exclRowCnt, "Failed", "Position Recon page header/title is not as expected, Actual header:"+strAct_Header+" and Expected Header:Position Recon", "", "PosRecon_Header",  driver, "HORIZONTALLY", strSheetName, null);
						System.out.println("Unexpected Header available");
						return false;
					}
				}//End of if condition to verify page heading
				else {
					reporter.reportStep(exclRowCnt, "Failed", "Position Recon page header/title object is not displayed, please check..!", "", "PosRecon_Header",  driver, "HORIZONTALLY", strSheetName, null);
					System.out.println("Header un available");
					return false;
				}
			}//End of IF condition to check if Position Recon link exists in left navigation panel.
			else{
				reporter.reportStep(exclRowCnt, "Failed", "Position Recon link is not displayed in left navigation panel of page, please check..!", "", "PosRecon_Header",  driver, "HORIZONTALLY", strSheetName, null);
				System.out.println("Link not available.");
				return false;
			}

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <PositionRecon_Objs.Navigate_PosRecon>: Position Recon Link is not displayed in UI..!!!");
			reporter.reportStep(exclRowCnt, "Failed", "PosRecon_Exception in <PositionRecon_Objs.Navigate_PosRecon> Please Check..!!", "", "PosRecon_Exception",  driver, "HORIZONTALLY", strSheetName, null);
			return false;
		}
	}//End of Navigate_PosRecon Method	    

	//###################################################################################################################################################################  
	//Function name		: VerifyPositionRecon_SummaryWidget(int exclRowCnt,String strSheetName)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to verify if the position Recon Summary is Displayed or not.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean VerifyPositionRecon_SummaryWidget(int exclRowCnt,String strSheetName){
		try{
			//Check if there Summary widget is displayed or not 
			if(!(CommonUtils.isElementPresent(ele_PositionReconSummaryText))){
				String summaryText = ele_PositionReconSummaryText.getText();
				System.err.println("Checkpoint : Error in Position Recon summary, Text is - "+summaryText);
				reporter.reportStep(exclRowCnt, "Failed", "Position Recon Summary widget not available :"+summaryText, "", "VerifyPositionRecon_SummaryWidget",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of if condition

			reporter.reportStep(exclRowCnt, "Passed", "Position Recon summary widget available ", "", "VerifyPositionRecon_SummaryWidget",  driver, "HORIZONTALLY", strSheetName, null);

			//Verifying summary table in Widget. Clicking on it if the table is hidden
			this.VerifyPositionRecon_SummaryTable(exclRowCnt, strSheetName);

			//Get account number from Position Recon summary table in Runtime 
			this.posReconAccountNum = getAccountFromPosReconSummaryWidget(exclRowCnt, strSheetName);
			System.out.println(" Account Number from summary : " + this.posReconAccountNum);
			return true;

		}//End of Try block
		catch(Exception e){
			System.err.println("Exception in <PositionRecon_Objs.VerifyPositionRecon_SummaryWidget>: PositionRecon verifying summary.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <PositionRecon_Objs.VerifyPositionRecon_SummaryWidget>: PositionRecon verifying summary.", "", "VerifyPositionRecon_SummaryWidget",  driver, "HORIZONTALLY", strSheetName, null);
			return false;
		}//End of catch block
	}//End of <Method: VerifyPositionRecon_SummaryWidget>

	//###################################################################################################################################################################  
	//Function name		: VerifyPositionRecon_SummaryTable(int exclRowCnt,String strSheetName)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to verify if the position Recon Summary table is Displayed or not.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public void VerifyPositionRecon_SummaryTable(int exclRowCnt,String strSheetName){
		//Method to check the posRecon widget is available for selection. Else clicking on it to expand
		try
		{
			//Check if the Summary widget is displayed or hidden
			if(!(CommonUtils.isElementPresent(ele_PositionReconSummaryTbl))){
				System.out.println(" Get Attribute : " +ele_PositionReconSummaryText.getAttribute("textContent"));
				//Click on summary widget to expand/show
				ele_PositionReconSummaryText.click();
				System.out.println(" Checkpoint :[Passed] Position Recon page_Summary widget expanded by driver .");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] Position Recon page_Summary widget expanded by driver.", "", "Expanding_PositionReconSummaryWidget",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				//Summary widget has already displayed in web page. 
				System.out.println(" Checkpoint :[Passed] Position Recon page_Summary widget  available in page by default.");
				reporter.reportStep(exclRowCnt, "Passed", "Position Recon page_Summary widget  available in page by default !!..", "", "PositionRecon_SummaryWidget",  driver, "HORIZONTALLY", strSheetName, null);
			}
		} // End of Try block
		catch(Exception e)
		{
			System.err.println("Exception in <PositionRecon_Objs.VerifyPositionRecon_SummaryTable>: PositionReconWidget-> is hidden. Please check !!.");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exception in <PositionRecon_Objs.VerifyPositionRecon_SummaryTable>: PositionReconWidget-> is hidden. Please check !!..", "", "VerifyPositionRecon_SummaryTable",  driver, "HORIZONTALLY", strSheetName, null);
		} //End of Catch
	} ////End of <Method: VerifyPositionRecon_SummaryTable> 

	//###################################################################################################################################################################  
	//Function name		: getAccountFromPosReconSummaryWidget(int exclRowCnt, String strSheetName)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to get the Account number from position Recon summary web table in run time.
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################		

	public String getAccountFromPosReconSummaryWidget(int exclRowCnt, String strSheetName) throws Exception{
		String accountNo = null;
		try
		{	
			//Get the No of rows count available in position Recon summary web table.
			int accountBreaksRowCount = this.get_PosRecon_ResRowCount(this.ele_PositionReconSummaryTbl_Body, 0, exclRowCnt, strSheetName);
			System.out.println("No of rows returned : " + accountBreaksRowCount);

			if(accountBreaksRowCount > 1)
			{
				accountNo = this.PosReconTbl_getCellData(0, "Account");
				System.out.println("Account number  : " + accountNo);
				reporter.reportStep(exclRowCnt, "Passed", "Position Recon page have Breaks, 1st Account from summary widget : "+accountNo, "", "PosRecon_BreakCheck",  driver, "HORIZONTALLY", strSheetName, null);
			} //End of If
			else if((accountBreaksRowCount == 1) && (this.PosReconTbl_getCellData(0, "Account").equalsIgnoreCase("Account Summary")))
			{
				accountNo = null;
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] Position Recon page have no Breaks, No Accounts available. ", "", "PosRecon_BreakCheck",  driver, "HORIZONTALLY", strSheetName, null);
				System.out.println("Account number  : " + accountNo);
			} // End of Else if
		} //End of Try block
		catch(Exception e){
			System.out.println("Exception in <PositionRecon_Objs.getAccountFromPosReconSummaryWidget>: Please check !.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <PositionRecon_Objs.getAccountFromPosReconSummaryWidget>: Please check !!.", "", "PosRecon_BreakCheck",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		}//End of catch block
		return accountNo;
	}//End of <Method: getAccountFromPosReconSummaryWidget>

	//###################################################################################################################################################################  
	//Function name		: get_PosRecon_ResRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to get total number of rows in the given webtable
	//Parameters 		: Search Page name
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public int get_PosRecon_ResRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
		try{  	
			if(eleTableName.isEmpty()){
				System.out.println("<Class: PositionRecon_Objs><Method: get_PosRecon_ResRowCount>: eleTableName parameter passed is empty, please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<PositionRecon_Objs.get_PosRecon_ResRowCount>!", "", "WebTableRowCount",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return 0;
			}
			if(!((eleTableName.size()>0) && (eleTableName.size()>=tbl_index)) ){
				System.out.println("<Class: PositionRecon_Objs><Method: get_PosRecon_ResRowCount>: Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index+",please check method<PositionRecon_Objs.get_PosRecon_ResRowCount>!", "", "WebTableRowCount",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return 0;
			}//End of IF Condition to check if eleTableName element exists

			List<WebElement> app_rows = eleTableName.get(tbl_index).findElements(By.tagName("tr"));
			try{
				if(ele_NoResults.isDisplayed()) return 0;
			}
			catch(org.openqa.selenium.NoSuchElementException e){
				return app_rows.size(); 
			}
			return app_rows.size();  			

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <PositionRecon_Objs.get_PosRecon_ResRowCount>,please check.!!");
			e.printStackTrace();
			return 0;
		}
	}//End of get_PosRecon_ResRowCount Method

	//###################################################################################################################################################################  
	//Function name			: PosReconTbl_getCellData(int rowNumber,String colName)
	//Class name			: PositionRecon_Objs
	//Description 			: Function to get cell data from Position Recon table
	//Parameters 			: rowNumber
	//						: Column Name	
	//Assumption			: None
	//Developer				: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public String PosReconTbl_getCellData(int rowNumber,String colName){
		try{  	
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(10,TimeUnit.SECONDS)
			.pollingEvery(2, TimeUnit.SECONDS)				 
			.ignoring(NoSuchElementException.class)
			.until(ExpectedConditions.visibilityOf(ele_PositionReconSummaryTbl));

			if(!CommonUtils.isElementPresent(ele_PositionReconSummaryTbl)){
				System.out.println("Position Recon Table is not displayed,please check..!!");
				return null;	
			}//End of IF Condition to check if ele_ResTable element exists

			int colIndex = this.PosRecon_GetColIndexByColName(colName);
			List<WebElement> app_rows = ele_PositionReconSummaryTbl_Body.get(0).findElements(By.tagName("tr"));
			List<WebElement> app_cols = app_rows.get(rowNumber).findElements(By.tagName("td"));    			
			String strColValue = app_cols.get(colIndex).getAttribute("textContent");
			return strColValue;

		}//End of Try block
		catch(NoSuchElementException e)
		{
			System.out.println("Exception in <PositionRecon_Objs.PosReconTbl_getCellData>:Position Recon Table Element Not available..!!!");
			return "";
		}
		catch(Exception e){
			System.out.println("Exception in <PositionRecon_Objs.PosReconTbl_getCellData>:Position Recon Table is not displayed in UI..!!!");
			return "";
		}
	}// End of PosReconTbl_getCellData method.

	//###################################################################################################################################################################  
	//Function name		: PositionRecon_SearchFlow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch, String strSheetName)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to perform search and verify results in Position Recon page.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public void PositionReconPage_SearchFlow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch, String strSheetName){
		try{
			System.out.println("Account number from Position Recon Summary table : "+ this.posReconAccountNum);
			
			if(this.posReconAccountNum == null || this.posReconAccountNum.isEmpty())
			{
				this.PositionRecon_BlankSearch_Flow(exclRowCnt, To_Execute, strAcct, strSecType, strSecValue, strBlankSearch);
			}
			else if(this.posReconAccountNum.length() > 0)
			{
				this.PositionRecon_DataSearch_Flow(exclRowCnt, To_Execute, this.posReconAccountNum, strSecType, strSecValue, strBlankSearch);
			}
		}//End of Try block

		catch(NullPointerException e){
			System.err.println("Null Exception in <PositionRecon_Objs.PositionReconPage_SearchFlow>: Please check !.");
			System.err.println(e.getMessage());
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <PositionRecon_Objs.PositionReconPage_SearchFlow>: Please check !.", "", "PositionReconPage_SearchFlow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		}
		catch(Exception e){
			System.err.println("Exception in <PositionRecon_Objs.PositionRecon_SearchFlow>: Please check !.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <PositionRecon_Objs.PositionReconPage_SearchFlow>: Please check !.", "", "PositionReconPage_SearchFlow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		}//End of catch block
	}//End of <Method: PositionReconPage_SearchFlow>

	//###################################################################################################################################################################  
	//Function name		: PosRecon_SearchFlow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch, String strSheetName, String strViewPage)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to perform search and verify results in Pos Recon Search view.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public void PosRecon_SearchFlow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch, String strSheetName, String strViewPage)
	{
		SearchPageFns objSearchPageFns = PageFactory.initElements(driver, SearchPageFns.class);
		SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);

		String strSearchValues = null;
		boolean strRet = false;
		try{		
			System.out.println("Account number from Position Recon Summary table : " + this.posReconAccountNum);

			if(this.posReconAccountNum == null || this.posReconAccountNum.isEmpty())
			{
				//Blank Search
				strSearchValues = "["+strViewPage+";Blank Search]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] Navigating to 'PosRecon_SearchFlow' page to perform 'Blank Search'.", "", "PosRecon_BlankSearch",  driver, "HORIZONTALLY", strSheetName, null);
				strRet = objSearchPageFns.Search_Input_Blank(strViewPage , exclRowCnt,strSheetName);

				if(strRet){
					//Verify search results row number		
					int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);

					if(!(appRowsReturned>0)){
						if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] 'PositionRecon_SearchFlow' No Results found. Since No Breaks are available. Expected to see 'No Results Found' message.", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return;
						}
						System.out.println("Search Results for "+strSearchValues+" is not >0");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'PositionRecon_SearchFlow' Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}//End of IF condition to check No of rows returned
					else{
						System.err.println("Search Results for "+strSearchValues+" is >0");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'PositionRecon_SearchFlow' Results rows >0. But No breaks are available. Expected to see 'No Results found' message.", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;	
					}
				}//End of IF condition to check Search_Input Checkpoint
				System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
			} //End of IF for Blank Search
			
			else if(this.posReconAccountNum.length() > 0)
			{
				//Account search
				strSearchValues = "["+strViewPage+";"+this.posReconAccountNum+"]";
				System.out.println("\n#########################["+strViewPage+"]Smoke Test: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Navigating to '"+strViewPage+"' view to perform 'Search using search values:"+strSearchValues+"'.", "", "PosReconSearchView_AcctSearch",  driver, "HORIZONTALLY", strSheetName, null);
				strRet = objSearchPageFns.Search_Input(strViewPage , this.posReconAccountNum , strSecType,strSecValue,exclRowCnt,strSheetName);

				if(strRet){
					//Verify search results row number		
					int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strViewPage);

					if(!(appRowsReturned>0)){
						if(objSearchRes.searchRes_NoResultsReturned(strViewPage).equals("No Results Found")){
							System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
							reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'PositionRecon_SearchFlow' No Results found. But expected to see data for Breaks Account", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return;
						}
						System.out.println("Search Results for "+strSearchValues+" is not >0");
						reporter.reportStep(exclRowCnt, "Failed", "'PositionRecon_SearchFlow' Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;
					}//End of IF condition to check No of rows returned
					else{
						System.out.println("Search Results for "+strSearchValues+" is >0");
						reporter.reportStep(exclRowCnt, "Passed", "'PositionRecon_SearchFlow' Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						return;	
					}

				}//End of IF condition to check Search_Input Checkpoint
			} // End of Else If for Account Search 
			
		}//End of try block
		catch(Exception e){
			System.err.println("Exception in Smoke Test Flow <Method: PositionRecon_SearchFlow>,please check..!!");
			System.err.println("Exception in <PositionRecon_Objs.PositionRecon_SearchFlow>: Please check !.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <PositionRecon_Objs.PositionRecon_SearchFlow>: Please check !.", "", "PositionRecon_page_SearchFlow",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of Catch block
	}//End of <Method: PositionRecon_SearchFlow>

	//###################################################################################################################################################################  
	//Function name			: PosRecon_NoResultsReturned
	//Class name			: PositionRecon_Objs
	//Description 			: Function to total number of rows in the given webtable
	//Parameters 			: Search Page name
	//Assumption			: None
	//Developer				: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public String PosRecon_NoResultsReturned(){
		try{  	
			String strResValue;  				
			if(CommonUtils.isElementPresent(ele_NoResults)){
				strResValue = ele_NoResults.getAttribute("textContent");
				if(strResValue.contains("No Results Found")) strResValue ="No Results Found";
				return 	strResValue;
			}//End of IF Condition to check if ele_ResultsTable element exists
			System.out.println("<Class: PositionRecon_Objs><Method: PosRecon_NoResultsReturned>: No Results found checkpoint fail.");
			strResValue = "No Results found checkpoint fail";
			return strResValue;  			

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <PositionRecon_Objs.PosRecon_NoResultsReturned>: Position Recon Search Results Table 'No Results Found' is not displayed in UI..!!!");
			e.printStackTrace();
			return null;
		}
	}//End of PosRecon_NoResultsReturned Method


	//###################################################################################################################################################################  
	//Function name			: PosRecon_GetColIndexByColName(String strColName)
	//Class name			: PositionRecon_Objs
	//Description 			: Function to read number of records retrieved from search result
	//Parameters 			: None 
	//Assumption			: None
	//Developer				: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public int PosRecon_GetColIndexByColName(String strColName){
		try{  			
			if(!(CommonUtils.isElementPresent(ele_PositionReconSummaryTbl) && (ele_PositionReconSummaryTbl_Header.size()>0))){
				System.out.println("Position Recon Table Header is not displayed,please check..!!");
				return 0;	
			}//End of IF Condition to check if ele_PositionReconTbl_Header element exists

			List<WebElement> getCols = ele_PositionReconSummaryTbl_Header.get(0).findElements(By.tagName("th"));

			int iColLoop = 0;
			for (WebElement eachCol : getCols) {  		
				if (eachCol.getAttribute("textContent").trim().equalsIgnoreCase(strColName)){ 					
					Integer colIndex = iColLoop;
					return colIndex;
				}//End of IF condition to check column name 
				iColLoop++;
			}//End of FOR loop to get Column Index

			System.out.println("<PositionRecon_Objs><PosRecon_GetColIndexByColName>: Position Recon Table HEADER, Expected Column Name:"+strColName+" is not displayed in UI..!!!");
			return -1;
		}//End of Try block
		catch (NumberFormatException e) {
			System.out.println("NumberFormatException in <PositionRecon_Objs><PosRecon_GetColIndexByColName>: Column Index for column name: "+strColName+" is not Integer");
			return -1;
		}
		catch(Exception e){
			System.out.println("Exception in <PositionRecon_Objs><PosRecon_GetColIndexByColName>: Position Recon Table HEADER is not displayed in UI..!!!");
			return -1;
		}  		

	}//End of PosRecon_GetColIndexByColName method

	//###################################################################################################################################################################  
	//Function name		: PosRecon_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName)
	//Class name		: PositionRecon_ObjsPosRecon_Search
	//Description 		: Function to perform search in PosRecon page 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean PosRecon_Search(String strAcct,String strSecType,String strSecValue,int exclRowCnt,String strSheetName){
		LocalDateTime strStartWaitTime,strEndWaitTime;
		String strSearchValues;
		try{
			boolean blnSecTypeCheck = false;

			strSearchValues = "[PosRecon;Acct:"+strAcct+";"+strSecType+";"+strSecValue+"]";
			txtbx_Acct.sendKeys(strAcct);

			List<WebElement> SecType_List = lst_SecIDType.findElements(By.tagName("option"));
			for (WebElement option : SecType_List) {
				if(option.getText().equals(strSecType)){
					option.click();
					blnSecTypeCheck=true;
					break;
				}
			}//End of FOR loop
			if(!blnSecTypeCheck){
				for (WebElement option : SecType_List) {
					if(option.getText().equals("Security No")){
						System.out.println("Selecting default SecIDType as Security No");
						option.click();
						blnSecTypeCheck=true;
						break;
					}
				}//End of FOR loop
			}//End of IF condition to select default SecIDTpe= Security ID

			txtbx_SecurityID.sendKeys(strSecValue);


			btn_Submit.click();
			
			//Checkpoint: Check for Errors:
			if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
				String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
				System.out.println("Checkpoint : Error in PosRecon Search Page, Error Message is - "+strErMsg);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] PositionRecon SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of if condition to to see if there are errors.

			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
			strStartWaitTime = LocalDateTime.now();
			try{
				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
				wait.withTimeout(35,TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)				 
				.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.visibilityOf(ele_PosReconResTable));
			}//End of try block for FluentWait

			catch(org.openqa.selenium.TimeoutException e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());

				//Check if page is loading:			
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading)){
					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Pos Recon Search after input"+strSearchValues+":Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of IF condition to check for loading object.

				//Checkpoint: Check for Errors:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Pos_Recon SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.				
			}//End of Catch block for FluentWait

			catch(Exception e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return false;
			}//End of catch block with generic exception for Fluent Wait 

			//Checkpoint: Check for Errors:
			if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
				String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
				System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Pos Recon SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of if condition to to see if there are errors.

			//Check for Results header
			if(CommonUtils.isElementPresent(ele_PositionReconSummaryTbl)){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Checkpoint : Position Recon Page Navigation - [Passed]");
				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Pos Recon Search After Input "+strSearchValues+":Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				//System.out.println(LocalDateTime.now());
				return true;
			}//End of IF condition to check for Results table header.

			System.out.println("Checkpoint : Pos Recon Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] PosRecon Search after input:"+strSearchValues+"Failed, Results header is not displayed", "", "PosReconSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
			return false;


		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <PositionRecon_Objs.PosRecon_Search>: PositionRecon-> verifying header title.");
			return false;
		}//End of catch block
	}//End of <Method: PosRecon_Search> 

	//###################################################################################################################################################################  
	//Function name		: PositionRecon_DataSearch_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to list the flow of Position Recon 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public  void PositionRecon_DataSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;

		try{		
			//Perform Data search in Position Recon page
			strSearchValues = "[PosRecon;"+strAcct+";"+strSecType+";"+strSecValue+"]";
			strRet = this.PosRecon_Search(strAcct, strSecType, strSecValue, exclRowCnt, strSheetName);

			//Check for Position Recon Search results
			if(strRet){					
				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = this.get_PosRecon_ResRowCount(this.ele_PosReconResTbl_Body, 0, exclRowCnt, strSheetName);

				if(!(appRowsReturned>0)){
					if(this.PosRecon_NoResultsReturned().equals("No Results Found"))
					{
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Failed", "Position Recon Search for "+strSearchValues+" is: 'No Resuls found'. Expected to see data for Breaks Account", "", "PosReconSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Failed", "Position Recon Search for "+strSearchValues+": Results rows not >0, Not as expected 'No results Found'. Expected to see data for Breaks Account", "", "PosReconSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned

				else{
					reporter.reportStep(exclRowCnt, "Passed", "'Position Recon' Results rows >0 retrieved successfully", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}//End of ELSE condition to check No of rows returned>0
			}//End of IF condition

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: PositionRecon_DataSearch_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in PositionRecon_DataSearch_Flow ", "", "PositionRecon_DataSearch_Flow",  driver, "HORIZONTALLY", strSheetName, null);
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: PositionRecon_DataSearch_Flow>		

	//###################################################################################################################################################################  
	//Function name		: PositionRecon_BlankSearch_Flow(int exclRowCnt,String strViewPage,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch)
	//Class name		: PositionRecon_Objs
	//Description 		: Function to list the methods for verifying Position Recon page when there is "Blanks Search" in Position Recon page
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################			
	public  void PositionRecon_BlankSearch_Flow(int exclRowCnt,String To_Execute,String strAcct,String strSecType, String strSecValue,String strBlankSearch ) throws Exception{
		String strSheetName = "SmokeData";
		String strSearchValues;
		boolean strRet;
		try{		
			strSearchValues = "[PosRecon;Blank Search]";
			//Perform Blank search in PosRecon page
			strRet = this.PositionRecon_BlankSearch(exclRowCnt,strSheetName);

			//Check for Position Recon page Search results
			if(strRet){	

				//Verify search results row number, if No Results returned fail the case.		
				int appRowsReturned = this.get_PosRecon_ResRowCount(this.ele_PosReconResTbl_Body, 0, exclRowCnt, strSheetName);

				if(!(appRowsReturned>0)){
					if(this.PosRecon_NoResultsReturned().equals("No Results Found")){
						System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] Pos Recon using Blank Search  retrieves: 'No Resuls found'. Expected to see this message. Since no breaks are available.!", "", "PosReconSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
						return;
					}
					System.out.println("Search Results for "+strSearchValues+" is not >0");
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] Pos Recon Blank Search Results rows not >0", "", "PosReconSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
					return;
				}//End of IF condition to check No of rows returned

				else{
					//Read account & Sec number from Position Recon search results
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Warning] Pos Recon Blank Search Results rows >0 , But No Breaks are Available. Please check !!.", "", "PositionRecon_BlankSearch_Flow",  driver, "HORIZONTALLY", strSheetName, null);
				}//End of ELSE condition to check No of rows returned>0
			}//End of IF condition to check PosRecon Blank Checkpoint

			System.out.println("###########################################################[End of Smoke Test for Row iteration:["+exclRowCnt+"]###########################################################");
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in Smoke Test Flow <Method: PositionRecon_BlankSearch_Flow>,please check..!!");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in PositionRecon_BlankSearch_Flow ", "", "PositionRecon_BlankSearch_Flow",  driver, "HORIZONTALLY", strSheetName, null);
			return;
		}//End of Catch block
		//return false;
	}//End of >Method: PositionRecon_BlankSearch_Flow>	
	
	//###################################################################################################################################################################  
		//Function name		: PositionRecon_BlankSearch(int exclRowCnt,String strSheetName)
		//Class name		: PositionRecon_Objs
		//Description 		: Function to perform BLANK search in PositionRecon page 
		//Parameters 		: None
		//Assumption		: None
		//Developer			: Dhanasekar Kumar 
		//###################################################################################################################################################################
		public boolean PositionRecon_BlankSearch(int exclRowCnt,String strSheetName){
			LocalDateTime strStartWaitTime,strEndWaitTime;
			try{
				btn_Submit.click();
				
				//Checkpoint: Check for Errors:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in PosRecon Search Page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] PositionRecon SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.
				
				//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
				strStartWaitTime = LocalDateTime.now();
				try{
					System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
					FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
					wait.withTimeout(35,TimeUnit.SECONDS)
					.pollingEvery(2, TimeUnit.SECONDS)				 
					.ignoring(NoSuchElementException.class)
					.until(ExpectedConditions.visibilityOf(ele_PosReconResTable));
				}//End of try block for FluentWait

				catch(org.openqa.selenium.TimeoutException e){
					strEndWaitTime = LocalDateTime.now();
					System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());

					//Check if page is loading:			
					if(CommonUtils.isElementPresent(CBManager_Objs.ele_Loading)){
						System.out.println("Checkpoint : PosRecon Blank Search - Stil Loading..[Failed]");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] PositionRecon Blank Search :Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of IF condition to check for loading object.

					//Checkpoint: Check for Errors:
					if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
						String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
						System.out.println("Checkpoint : Error in PosRecon Search Page, Error Message is - "+strErMsg);
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] PositionRecon SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
						return false;
					}//End of if condition to to see if there are errors.				
				}//End of Catch block for FluentWait
				catch(Exception e){
					strEndWaitTime = LocalDateTime.now();
					System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
					return false;
				}//End of catch block with generic exception for Fluent Wait 

				//Checkpoint: Check for Errors:
				if(CommonUtils.isElementPresent(CBManager_Objs.ele_CBM_Errors)){
					String strErMsg = CBManager_Objs.ele_CBM_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in PositionRecon Search Page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] PositionRecon SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.

				//Check for Results header
				if(CommonUtils.isElementPresent(ele_PosReconResTable)){
					strEndWaitTime = LocalDateTime.now();
					System.out.println("Checkpoint : Position Recon Page Navigation - [Passed]");
					System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Position Recon Blank Search Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return true;
				}//End of IF condition to check for Results table header.

				System.out.println("Checkpoint : PosRecon Search After Input data insert - [Failed]");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Position Recon Blank Search :Failed, Results header is not displayed", "", "PosReconSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
				return false;

			}//End of Try block
			catch(Exception e){
				System.out.println("Exception in <PositionRecon_Objs.PositionRecon_BlankSearch>: PositionRecon-> verifying header title.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Position Recon Blank Search :Failed, Exception in <PositionRecon_Objs.PositionRecon_BlankSearch>: PositionRecon-> verifying header title.", "", "PosReconSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of catch block
		}//End of <Method: PositionRecon_BlankSearch> 
	
}//End of <class: PositionRecon_Objs>
