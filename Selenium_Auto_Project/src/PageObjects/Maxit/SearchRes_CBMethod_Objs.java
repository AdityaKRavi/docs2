package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.Maxit.Maxit_CommonFns;

public class SearchRes_CBMethod_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	SearchRes_ActionMenu SearchRes_Action = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
	Maxit_CommonFns maxitFns = PageFactory.initElements(driver, Maxit_CommonFns.class);
	
	//Account level CB Method:
    @FindBy(how=How.XPATH,using="//*[@id=\"secTypeCombo\"]")
	public WebElement ele_AcctSecType;
    @FindBy(how=How.XPATH,using="//*[@id=\"sellMethodCombo\"]")
	public WebElement ele_AcctSellMethod;          
    @FindBy(how=How.XPATH,using="//*[contains(label,\"Strategy:\")]")
	public WebElement ele_Strategy;    

 
    //SecLevel CBMethod:
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-tab-strip-text \") and contains(text(),\"CBM\")]")
	public WebElement ele_CBMTab; 
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-fieldset-header-text\") and contains(text(),\"Edit Method\")]")
	public List<WebElement> ele_CBM_EditMethod;
    @FindBy(how=How.XPATH,using="//*[@id=\"sellMethod\"]")
	public WebElement ele_CBM_Method; 
    @FindBy(how=How.XPATH,using="//*[contains(label,\"Effective:\")]")
	public List<WebElement> ele_CBM_Effective;     
    //SecLevel_AvgCost
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-fieldset-header-text\") and contains(text(),\"Select Shares\")]")
	public WebElement ele_CBMAvgCost_SelectShares;
    @FindBy(how=How.XPATH,using="//*[contains(@name,\"bucketYear\") and contains(@value,\"prior\")]")
	public WebElement ele_CBMAvgCost_NonCov;
    @FindBy(how=How.XPATH,using="//*[contains(@name,\"bucketYear\") and contains(@value,\"after\")]")
	public WebElement ele_CBMAvgCost_Cov;
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-fieldset-header-text\") and contains(text(),\"Exit/Revoke Average Cost Shares\")]")
	public WebElement ele_CBMAvgCost_ExitLabel;    
    @FindBy(how=How.XPATH,using="//*[contains(@name,\"revokeAvg\") and contains(@value,\"N\")]")
	public WebElement ele_CBMAvgCost_ExitAvgCost;
    @FindBy(how=How.XPATH,using="//*[contains(@name,\"revokeAvg\") and contains(@value,\"Y\")]")
	public WebElement ele_CBMAvgCost_RevokeAvgCost;
    @FindBy(how=How.XPATH,using="//*[contains(@name,\"applyDateExit\")]")
	public WebElement ele_CBMAvgCost_Date;
    
    
    
    
    //SecLevel DRP Tab
    @FindBy(how=How.XPATH,using="//*[contains(@class,\"x-tab-strip-text \") and contains(text(),\"DRP\")]")
	public WebElement ele_DRP;     
    @FindBy(how=How.XPATH,using="//*[@id=\"drpFieldset\"]//*[@class=\"x-fieldset-header-text\"]")
	public WebElement ele_DRPEditMethod; 
    @FindBy(how=How.XPATH,using="//*[@id=\"drpMethod\"]")
	public WebElement ele_DRPMethod; 
    @FindBy(how=How.XPATH,using="//*[@id=\"drpFieldset\"]//*[contains(label,\"Effective:\")]")
	public WebElement ele_DRPEffecLabel;   
    
    //RawTrades->Details
    @FindBy(how=How.XPATH,using="//*[contains(label,\"current sell method:\")]")
	public List<WebElement> ele_Det_SellMethod;
    
    
    //Back , Save & Cancel buttons
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Back\")]")
	public List<WebElement> btn_CBMBack;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Save\")]")
	public List<WebElement> btn_CBMSave;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Cancel\")]")
	public List<WebElement> btn_CBMCancel;  
    @FindBy(how=How.XPATH,using="//*[@class=\"x-tool x-tool-close\"]")
	public List<WebElement> btn_CBMXClose;
  
    //RawTrades->Details->VSP
    @FindBy(how=How.ID,using="percentToApply")
	public List<WebElement> ele_PercToApply;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Apply to all lots\")]")
	public List<WebElement> btn_Vsp_ApplyAll;
    
    //VSP_Advanced->Filter VSP lots section
    @FindBy(how=How.XPATH,using="//*[@class=\"x-panel-header-text\" and contains(text(),\"Filter VSP Lots\")]")
	public WebElement ele_FilterLabel;
    @FindBy(how=How.NAME,using="startDate")
	public WebElement ele_startDate;
    @FindBy(how=How.NAME,using="endDate")
	public WebElement ele_endDate;
    @FindBy(how=How.NAME,using="maxCost")
	public WebElement ele_MaxCost;
    @FindBy(how=How.NAME,using="minCost")
	public WebElement ele_MinCost;
    @FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Search\")]")
	public List<WebElement> btn_VSPSearch;
    
    //VSP_Advanced->Returned Results section   
    @FindBy(how=How.XPATH,using="//*[@class=\"x-panel-header-text\" and contains(text(),\"Returned Results\")]")
	public WebElement ele_RetResults;
    
  //VSP_Advanced->Selected Lots section   
    @FindBy(how=How.XPATH,using="//*[@class=\"x-panel-header-text\" and contains(text(),\"Selected Lots\")]")
	public WebElement ele_SelectedLots;
    
    @FindBy(how=How.ID,using="vspError")
	public WebElement ele_VSPError;
    @FindBy(how=How.XPATH,using="//*[@class=\"ext-el-mask-msg x-mask-loading\"]")
    public WebElement ele_Loading; 
    @FindBy(how=How.XPATH,using="//*[@class=\"ext-el-mask\"]")
    public WebElement ele_LRMSaveLoading; 
    
    @FindBy(how=How.XPATH,using="//*[@id=\"main\" and contains(@class,\"errors\")]")
	public List<WebElement> ele_OrangeErrors; 
  //###################################################################################################################################################################  
  //Function name	: verifyAccLevel_CBM_DropDowns(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_ActionMenu.java
  //Description 	: Function to verify the drop downs in Account level CB Method pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: Account LevelCBMethod pop up is displayed.
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifyAccLevel_CBM_DropDowns(int exclRowCnt,String strSheetName,String strClient){		
    	ArrayList<String> arrAct_Values , arrExp_Values;
  	try{	
  		if(ele_CBM_EditMethod.size()>0) 	this.ele_CBM_EditMethod.get(0).click();
  		else {
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBM Tab->Edit Method header label is NOT displayed", "", "CBM_EditMethod",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] CBMethod->CBM Tab->Edit Method header label is NOT displayed,so not validating dropdowns 'Method' & 'Effective',please check..!!");
      		return;
  		}
  		
  		//Sec Type drop down verification
      	if(!CommonUtils.isElementPresent(ele_AcctSecType)){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->Sec Type object is NOT displayed", "", "CBM_SecType",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] CBMethod->Sec Type object is NOT displayed,please check..!!");
  		}//End of if condition to check if DROPDOWN is displayed
      	else{
      		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->Sec Type object is displayed", "", "CBM_SecType",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Passed] CBMethod->Sec Type object is displayed");
      		ele_AcctSecType.click();    	
          	//SearchRes_Action.compareDropDown("ACCLEVEL_CBM","SECTYPE",exclRowCnt,strSheetName);
      		arrAct_Values = maxitFns.getAct_ComboList("SECTYPE", SearchRes_Action.ele_PopUpCombo, exclRowCnt, strSheetName);
      		if(strClient.equalsIgnoreCase("VGI")) arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("VGI_SECTYPE");
      		else arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("SECTYPE");
      		maxitFns.Compare_ArrayLists("CBMethod->SecType Dropdown", arrExp_Values, arrAct_Values, exclRowCnt, strSheetName);
      		
          	this.ele_CBM_EditMethod.get(0).click();
      	}

  		//SELL METHOD drop down verification
      	if(!CommonUtils.isElementPresent(ele_AcctSellMethod)){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->Sell Method object is NOT displayed", "", "CBM_SellMethod",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] CBMethod->Sell Method object is NOT displayed,please check..!!");
  		}//End of if condition to check if DROPDOWN is displayed
      	else{
      		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->Sell Method object is displayed", "", "CBM_SellMethod",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Passed] CBMethod->Sell Method object is displayed");
      		ele_AcctSellMethod.click();    	
          	//SearchRes_Action.compareDropDown("ACCLEVEL_CBM","SELLMETHOD",exclRowCnt,strSheetName);
      		arrAct_Values = maxitFns.getAct_ComboList("SELLMETHOD", SearchRes_Action.ele_PopUpCombo, exclRowCnt, strSheetName);
      		if(strClient.equalsIgnoreCase("SCB")) arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("SCB_SELLMETHOD");
      		else arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("SELLMETHOD");
      		maxitFns.Compare_ArrayLists("CBMethod->SellMethod Dropdown", arrExp_Values, arrAct_Values, exclRowCnt, strSheetName);
      		
          	this.ele_CBM_EditMethod.get(0).click();
      	}
      	
  		//STRATEGY drop down verification
      	if(!CommonUtils.isElementPresent(ele_Strategy)){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->Startegy object is NOT displayed", "", "CBM_Startegy",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] CBMethod->Startegy object is NOT displayed,please check..!!");
  		}//End of if condition to check if DROPDOWN is displayed
      	else{
      		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->Startegy object is displayed", "", "CBM_Startegy",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Passed] CBMethod->Startegy object is displayed");
      		ele_Strategy.click();    	
          	//SearchRes_Action.compareDropDown("ACCLEVEL_CBM","STRATEGY",exclRowCnt,strSheetName);
      		arrAct_Values = maxitFns.getAct_ComboList("STRATEGY", SearchRes_Action.ele_PopUpCombo, exclRowCnt, strSheetName);
      		if(strClient.equalsIgnoreCase("USA")) arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("USA_STRATEGY");
      		else arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("STRATEGY");
      		maxitFns.Compare_ArrayLists("CBMethod->Strategy Dropdown", arrExp_Values, arrAct_Values, exclRowCnt, strSheetName);
          	this.ele_CBM_EditMethod.get(0).click();
      	}

  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: verifyAccLevel_CBM_DropDowns>Exception:"+e.getMessage()+",please check..!!");
  	}//End of catch block
    }//End of <Method: verifyAccLevel_CBM_DropDowns>        
    
  //###################################################################################################################################################################  
  //Function name	: verifyCBMethod_EditHist(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_ActionMenu.java
  //Description 	: Function to verify the Strategy drop down in CB Method pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifyCBMethod_EditHist(int exclRowCnt,String strSheetName){		
  	try{
  		String strAct_EditHistHeader="",strExp_EditHistHeader = "Effective Date;Created Date;Sell Method;Security Type";
  		//String strAct_EditHistRow ="" , strExp_EditHistRow = "(.*);(Mutual Fund|Stocks, Options, Bonds...|ETF RIC)";
  		
      	if(!CommonUtils.isElementPresent(SearchRes_Action.tbl_CurrSelec_Header.get(0))){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Method->Edit History Header is NOT displayed", "", "CBMethod_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] CB Method->Edit History Header is NOT displayed,please check..!!");
      		return;
  		}//End of if condition to check if Edit History Header is displayed
      	else{
      		List<WebElement> getCols = SearchRes_Action.tbl_CurrSelec_Header.get(0).findElements(By.tagName("td"));
      		for (WebElement eachCol : getCols) {  		
      			strAct_EditHistHeader = strAct_EditHistHeader + ";"+eachCol.getAttribute("textContent").trim();
    			}//End of FOR loop to get Header
      		if(strAct_EditHistHeader.startsWith(";")){
      			strAct_EditHistHeader = StringUtils.right(strAct_EditHistHeader, (strAct_EditHistHeader.length()-1));
      		}
      		
      		if(!strFns.compareString_Match(strAct_EditHistHeader, strExp_EditHistHeader)){
    	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Method->Edit History Header is NOT as expected.[Actual Header: "+strAct_EditHistHeader+" and Expected Header:"+strExp_EditHistHeader+"]", "", "CBMethod_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    		System.out.println("Checkpoint :[Failed] CB Method->Edit History Header is NOT as expected.[Actual Header: "+strAct_EditHistHeader+" and Expected Header:"+strExp_EditHistHeader+"]");
      		}//End of IF condition to check header's
      		else{
    	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Method->Edit History Header is as expected.[Expected Header:"+strExp_EditHistHeader+"]", "", "CBMethod_EditHist_Header",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	    		System.out.println("Checkpoint :[Passed] CB Method->Edit History Header is as expected.[Expected Header:"+strExp_EditHistHeader+"]");
      		}
      		
      	}//End of Else condition to verify header     	
      	
  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: verifyCBMethod_EditHist>,please check..!!");
  	}//End of catch block
    }//End of <Method: verifyCBMethod_EditHist>   
    
  //###################################################################################################################################################################  
  //Function name	: verifySecLevel_CBM_Tab(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_CBMethod.java
  //Description 	: Function to verify if CB Method->DRP tab is displayed
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
    public boolean verifySecLevel_CBM_Tab(int exclRowCnt,String strSheetName){		
  	try{	
  		//Verify if CBM Tab is displayed.
      	if(!CommonUtils.isElementPresent(ele_CBMTab)){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBM Tab object is NOT displayed", "", "CBM_CBMTab",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] CBMethod->CBM Tab object is NOT displayed,please check..!!");
      		return false;
  		}//End of if condition to check if CBM Tab is present
      	else{
      		//ele_DRP.click();
      		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->CBM Tab object is displayed", "", "CBM_CBMTab",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Passed] CBMethod->CBM Tab object is displayed");
      		return true;
      	}
  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: verifySecLevel_CBM_Tab>"+e.getMessage()+",please check..!!");
    		return false;
  	}//End of catch block
    }//End of <Method: verifySecLevel_CBM_Tab>    
    
//###################################################################################################################################################################  
//Function name		: verifySecLevel_CBM_DropDowns(int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to verify the drop downs in CB Method->CBM tab dropdowns
//Parameters 		: Excel row number for report and sheet name.
//Assumption		: CBMethod pop up with CBM tab is displayed.
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public void verifySecLevel_CBM_DropDowns(int exclRowCnt,String strSheetName,String strClient){		
	  ArrayList<String> arrAct_Values , arrExp_Values;
	  try{		
		if(ele_CBM_EditMethod.size()>0) 	this.ele_CBM_EditMethod.get(0).click();
		else {
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBM Tab->Edit Method header label is NOT displayed", "", "CBM_EditMethod",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->CBM Tab->Edit Method header label is NOT displayed,so not validating dropdowns 'Method' & 'Effective',please check..!!");
    		return;
		}
		
		//Method checkpoints
    	if(!CommonUtils.isElementPresent(ele_CBM_Method)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBM Tab->Method object is NOT displayed", "", "CBM_Method",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->CBM Tab->Method object is NOT displayed,please check..!!");
		}//End of if condition to check if DROPDOWN is displayed
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->CBM Tab->Method object is displayed", "", "CBM_Method",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Passed] CBMethod->CBM Tab->Method object is displayed");
        	ele_CBM_Method.click();    	
        	//SearchRes_Action.compareDropDown("SECLEVEL_CBM","METHOD",exclRowCnt,strSheetName);
      		arrAct_Values = maxitFns.getAct_ComboList("METHOD", SearchRes_Action.ele_PopUpCombo, exclRowCnt, strSheetName);
      		if(strClient.equalsIgnoreCase("SCB")) arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("SCB_SELLMETHOD");
      		else arrExp_Values = this.getExp_AccLevel_CBM_DropDowns("SELLMETHOD");
      		maxitFns.Compare_ArrayLists("CBMethod->SellMethod Dropdown", arrExp_Values, arrAct_Values, exclRowCnt, strSheetName);
        	
        	this.ele_CBM_EditMethod.get(0).click();
    	}

		//Effective checkpoints
    	this.ele_CBM_EditMethod.get(0).click();
    	if(!(ele_CBM_Effective.size()>0)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBM Tab->Effective object is NOT displayed", "", "CBM_Effective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->CBM Tab->Effective Setting is NOT displayed,please check..!!");
		}//End of if condition to check if DROPDOWN is displayed
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->CBM Tab->Effective object is displayed", "", "CBM_Effective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Passed] CBMethod->CBM Tab->Effective object is displayed");
        	ele_CBM_Effective.get(0).click();    	
        	SearchRes_Action.compareDropDown("SECLEVEL_CBM","EFFECTIVE",exclRowCnt,strSheetName);	    	
        	this.ele_CBM_EditMethod.get(0).click();
    	}

	}//End of try block
	catch(Exception e){			
  		System.out.println("Exception in <Method: verifySecLevel_CBM_DropDowns>Exception:"+e.getMessage()+",please check..!!");
	}//End of catch block
  }//End of <Method: verifySecLevel_CBM_DropDowns>    
  
//###################################################################################################################################################################  
//Function name		: verifySecLevel_CBM_AvgCost_DropDowns(int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to verify the drop downs in CB Method->CBM tab dropdowns
//Parameters 		: Excel row number for report and sheet name.
//Assumption		: CBMethod pop up with CBM tab is displayed.
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public void verifySecLevel_CBM_AvgCost_DropDowns(int exclRowCnt,String strSheetName){		
	try{	
		//UnCovered Shares checkpoints
    	if(!CommonUtils.isElementPresent(ele_CBMAvgCost_SelectShares)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBMAvgCost Tab->Select Shares object is NOT displayed", "", "CBM_AvgCost_SelectShares",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->CBMAvgCost Tab->Select Shares object is NOT displayed,please check..!!");
    		return;
    	}//End of if condition to check if Select Shares label is displayed in CBM Tab for AVg Cost
		
    	if(!CommonUtils.isElementPresent(ele_CBMAvgCost_NonCov)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->NonCov Shares radio button is NOT displayed", "", "CBM_AvgCost_NonCov",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBM AvgCost->NonCov Shares radio button is NOT displayed,please check..!!");
    		//return;
    	}//End of if condition to check if Non Covered shares radio button is displayed in CBM Tab for AVg Cost
    	else{
    		reporter.reportStep(exclRowCnt, "pASSED", "Checkpoint :[pASSED] CBM AvgCost->NonCovered Shares radio button is displayed", "", "CBM_AvgCost_NonCov",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		if(!ele_CBMAvgCost_NonCov.isSelected()) ele_CBMAvgCost_NonCov.click();
    		//Method checkpoints
    		this.verifyCBM_AvgCostMethodDropdown(exclRowCnt,strSheetName);
        	
    		//Effective checkpoints
    		this.verifyCBM_AvgCostEffecDropdown(exclRowCnt, strSheetName);
    		    		
    	}//End of ELSE condition to check if Non Covered shares radio button is displayed in CBM Tab for AVg Cost
    	
    	//Covered Shares checkpoints
    	if(!CommonUtils.isElementPresent(ele_CBMAvgCost_Cov)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->Covered Shares radio button is NOT displayed", "", "CBM_AvgCost_Cov",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBM AvgCost->Covered Shares radio button is NOT displayed,please check..!!");
    		//return;
    	}//End of if condition to check if Covered shares radio button is displayed in CBM Tab for AVg Cost
    	else{
    		if(!ele_CBMAvgCost_Cov.isSelected()) ele_CBMAvgCost_Cov.click();
    		//Exit AvgCost checkpoints
    		this.verifyCBM_AvgCost_Exit(exclRowCnt,strSheetName);

    		//Revoke Avg Cost
    		this.verifyCBM_AvgCost_Revoke(exclRowCnt,strSheetName);
    		
			//Method checkpoints
			this.verifyCBM_AvgCostMethodDropdown(exclRowCnt,strSheetName);

    		
    	}//End of ELSE condition to check if Covered shares radio button is displayed in CBM Tab for AVg Cost    	
    	
    	

	}//End of try block
	catch(Exception e){			
  		System.out.println("Exception in <Method: verifySecLevel_CBM_AvgCost_DropDowns>Exception:"+e.getMessage()+",please check..!!");
	}//End of catch block
  }//End of <Method: verifySecLevel_CBM_AvgCost_DropDowns>      
    
  //###################################################################################################################################################################  
  //Function name	: verifyCBM_AvgCostMethodDropdown(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_CBMethod_Objs.java
  //Description 	: Function to verify Method drop down in CBMethod->CBM pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
  public void verifyCBM_AvgCostMethodDropdown(int exclRowCnt,String strSheetName){		
	try{
		//Method checkpoints
    	if(!CommonUtils.isElementPresent(ele_CBM_Method)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBM Tab->Method object is NOT displayed", "", "CBM_AvgCost_EditMethod",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->CBM Tab->Method object is NOT displayed,please check..!!");
		}//End of if condition to check if DROPDOWN is displayed
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->CBM Tab->Method object is displayed", "", "CBM_AvgCost_Method",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Passed] CBMethod->CBM Tab->Method object is displayed");
        	ele_CBM_Method.click();    	
        	SearchRes_Action.compareDropDown("SECLEVEL_CBM_AVGCOST","METHOD",exclRowCnt,strSheetName);	    	
        	this.ele_CBMAvgCost_SelectShares.click();
    	}
    	
	}//End of try block
	catch(Exception e){			
		System.out.println("Exception in <Method: verifyCBM_AvgCostMethodDropdown>"+e.getMessage()+",please check..!!");
	}//End of catch block
  }//End of <Method: verifyCBM_AvgCostMethodDropdown>
  
  //###################################################################################################################################################################  
  //Function name	: verifyCBM_AvgCostEffecDropdown(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_CBMethod_Objs.java
  //Description 	: Function to verify Effective drop down in CBMethod->CBM pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
  public void verifyCBM_AvgCostEffecDropdown(int exclRowCnt,String strSheetName){		
	try{
		//Effective checkpoints
    	this.ele_CBMAvgCost_SelectShares.click();
    	if(!(ele_CBM_Effective.size()>0)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->CBM Tab->Effective object is NOT displayed", "", "CBM_Effective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->CBM Tab->Effective Setting is NOT displayed,please check..!!");
		}//End of if condition to check if DROPDOWN is displayed
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->CBM Tab->Effective object is displayed", "", "CBM_Effective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Passed] CBMethod->CBM Tab->Effective object is displayed");
        	ele_CBM_Effective.get(0).click();    	
        	SearchRes_Action.compareDropDown("SECLEVEL_CBM","EFFECTIVE",exclRowCnt,strSheetName);	    	
        	this.ele_CBMAvgCost_SelectShares.click();
    	}
    	
	}//End of try block
	catch(Exception e){			
		System.out.println("Exception in <Method: verifyCBM_AvgCostEffecDropdown>"+e.getMessage()+",please check..!!");
	}//End of catch block
}//End of <Method: verifyCBM_AvgCostEffecDropdown>   
  
  //###################################################################################################################################################################  
  //Function name	: verifyCBM_AvgCost_Exit(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_CBMethod_Objs.java
  //Description 	: Function to verify Effective drop down in CBMethod->CBM pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
  public void verifyCBM_AvgCost_Exit(int exclRowCnt,String strSheetName){		
	try{
		//Exit Avg Cost checkpoints
    	if(!CommonUtils.isElementPresent(ele_CBMAvgCost_ExitAvgCost)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost radio button is NOT displayed", "", "CBM_AvgCost_Cov_Exit",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost radio button is NOT displayed,please check..!!");
    		//return;
    	}//End of if condition to check if Covered shares->Exit Avg Cost radio button is displayed in CBM Tab for AVg Cost
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBM AvgCost->Covered Shares->Exit Avg Cost radio button is displayed", "", "CBM_AvgCost_Cov_Exit",  driver, "BOTH_DIRECTIONS", strSheetName, null);        		
    		if(!ele_CBMAvgCost_ExitAvgCost.isSelected()) ele_CBMAvgCost_ExitAvgCost.click();
        	if(!CommonUtils.isElementPresent(ele_CBMAvgCost_Date)){
        		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date object is NOT displayed", "", "CBM_AvgCost_Cov_Exit_Date",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        		System.out.println("Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is NOT displayed,please check..!!");
        		//return;
        	}//End of if condition to check if Covered shares->Exit Avg Cost->Date field is displayed in CBM Tab for AVg Cost
        	else {
        		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is displayed", "", "CBM_AvgCost_Cov_ExitDate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        		if(!ele_CBMAvgCost_Date.isEnabled()){
            		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is 'DISABLED'", "", "CBM_AvgCost_Cov_Exit_Date",  driver, "BOTH_DIRECTIONS", strSheetName, null);
            		System.out.println("Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is 'DISABLED',please check..!!");
        		}
        		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is 'ENABLED'", "", "CBM_AvgCost_Cov_ExitDate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        	}//End of else condition to check if AvgCost date field is displayed
    	}//End of Else condition to check if ele_CBMAvgCost_ExitAvgCost is present.
    	
	}//End of try block
	catch(Exception e){			
		System.out.println("Exception in <Method: verifyCBM_AvgCost_Exit>"+e.getMessage()+",please check..!!");
	}//End of catch block
}//End of <Method: verifyCBM_AvgCost_Exit>   
  //###################################################################################################################################################################  
  //Function name	: verifyCBM_AvgCost_Revoke(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_CBMethod_Objs.java
  //Description 	: Function to verify Effective drop down in CBMethod->CBM pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
  public void verifyCBM_AvgCost_Revoke(int exclRowCnt,String strSheetName){		
	try{
		//Exit Avg Cost checkpoints
    	if(!CommonUtils.isElementPresent(ele_CBMAvgCost_RevokeAvgCost)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->Covered Shares->Revoke Avg Cost radio button is NOT displayed", "", "CBM_AvgCost_Cov_Revoke",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBM AvgCost->Covered Shares->Revoke Avg Cost radio button is NOT displayed,please check..!!");
    		//return;
    	}//End of if condition to check if Covered shares->Revoke Avg Cost radio button is displayed in CBM Tab for AVg Cost
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBM AvgCost->Covered Shares->Revoke Avg Cost radio button is displayed", "", "CBM_AvgCost_Cov_Revoke",  driver, "BOTH_DIRECTIONS", strSheetName, null);        		
    		if(!ele_CBMAvgCost_RevokeAvgCost.isSelected()) ele_CBMAvgCost_RevokeAvgCost.click();
        	if(!CommonUtils.isElementPresent(ele_CBMAvgCost_Date)){
        		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date object is NOT displayed", "", "CBM_AvgCost_Cov_Revoke_Date",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        		System.out.println("Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is NOT displayed,please check..!!");
        		//return;
        	}//End of if condition to check if Covered shares->Revoke Avg Cost->Date field is displayed in CBM Tab for AVg Cost
        	else {
        		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is displayed", "", "CBM_AvgCost_Cov_RevokeDate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        		if(ele_CBMAvgCost_Date.isEnabled())   reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is 'ENABLED'", "", "CBM_AvgCost_Cov_Revoke_Date",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBM AvgCost->Covered Shares->Exit Avg Cost->Date field is 'DISABLED'", "", "CBM_AvgCost_Cov_RevokeDate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        	}//End of else condition to check if AvgCost date field is displayed
    	}//End of Else condition to check if ele_CBMAvgCost_RevokeAvgCost is present.
    	
	}//End of try block
	catch(Exception e){			
		System.out.println("Exception in <Method: verifyCBM_AvgCost_Revoke>"+e.getMessage()+",please check..!!");
	}//End of catch block
}//End of <Method: verifyCBM_AvgCost_Revoke>
  //###################################################################################################################################################################  
  //Function name	: verifyCBM_SaveCancel(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_CBMethod_Objs.java
  //Description 	: Function to verify Save & Close buttons in CBMethod->CBM pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifyCBM_SaveCancel(int exclRowCnt,String strSheetName,String strPopUpName){		
		try{
	    	if(!(btn_CBMSave.size()>0)){
	    		if(!(btn_CBMCancel.get(0).isDisplayed())){
		    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->CBM SAVE button is NOT displayed", "", "CBMethod_CBM_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->CBM SAVE button is NOT displayed,please check..!!");
	    		}
			}//End of if condition to check if SAVE button is displayed in DRP tab
	    	
	    	if(!(btn_CBMCancel.size()>0)){
	    		if(!(btn_CBMCancel.get(0).isDisplayed())){
		    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->CBM CANCEL button is NOT displayed", "", "CBMethod_CBM_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->CBM CANCEL button is NOT displayed,please check..!!");
		    		Thread.sleep(5000);
	    		}
			}//End of if condition to check if Cancel button is displayed
	    	
		}//End of try block
		catch(Exception e){			
    		System.out.println("Exception in <Method: verifyCBM_SaveCancel>"+e.getMessage()+",please check..!!");
		}//End of catch block
    }//End of <Method: verifyCBM_SaveCancel>      
//###################################################################################################################################################################  
//Function name		: verifySecLevel_DRP_Tab(int exclRowCnt,String strSheetName)
//Class name		: SearchRes_CBMethod.java
//Description 		: Function to verify if CB Method->DRP tab is displayed
//Parameters 		: Excel row number for report and sheet name.
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public boolean verifySecLevel_DRP_Tab(int exclRowCnt,String strSheetName){		
	try{	
		//Verify if DRP Tab is displayed.
    	if(!CommonUtils.isElementPresent(ele_DRP)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->DRP Tab object is NOT displayed", "", "CBM_DRPTab",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->DRP Tab object is NOT displayed,please check..!!");
    		return false;
		}//End of if condition to check if DRP Tab is present
    	else{
    		ele_DRP.click();
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->DRP Tab object is displayed", "", "CBM_DRPTab",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Passed] CBMethod->DRP Tab object is displayed");
    		return true;
    	}
	}//End of try block
	catch(Exception e){			
  		System.out.println("Exception in <Method: verifySecLevel_DRP_Tab>"+e.getMessage()+",please check..!!");
  		return false;
	}//End of catch block
  }//End of <Method: verifySecLevel_DRP_Tab>
  
//###################################################################################################################################################################  
//Function name		: verifySecLevel_DRP_DropDowns(int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to verify the drop downs in CB Method->DRP tab dropdowns
//Parameters 		: Excel row number for report and sheet name.
//Assumption		: CBMethod pop up with DRP tab is displayed.
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public void verifySecLevel_DRP_DropDowns(int exclRowCnt,String strSheetName){		
	try{	
    	this.ele_DRPEditMethod.click();
		
		//DRP Method checkpoints
    	if(!CommonUtils.isElementPresent(ele_DRPMethod)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->DRP Tab->Method object is NOT displayed", "", "DRP_Method",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->DRP Tab->Method object is NOT displayed,please check..!!");
		}//End of if condition to check if DROPDOWN is displayed
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->DRP Tab->Method object is displayed", "", "DRP_Method",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Passed] CBMethod->DRP Tab->Method object is displayed");
    	}
    	ele_DRPMethod.click();    	
    	SearchRes_Action.compareDropDown("SECLEVEL_DRP","DRP",exclRowCnt,strSheetName);	    	
    	this.ele_DRPEditMethod.click();
    	
		//Effective checkpoints
    	this.ele_DRPEditMethod.click();
    	if(!CommonUtils.isElementPresent(ele_DRPEffecLabel)){
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CBMethod->DRP Tab->Effective object is NOT displayed", "", "DRP_Effective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] CBMethod->DRP Tab->Effective Setting is NOT displayed,please check..!!");
		}//End of if condition to check if DROPDOWN is displayed
    	else{
    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CBMethod->DRP Tab->Effective object is displayed", "", "DRP_Effective",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Passed] CBMethod->DRP Tab->Effective object is displayed");
    	}
    	ele_DRPEffecLabel.click();    	
    	SearchRes_Action.compareDropDown("SECLEVEL_DRP","EFFECTIVE",exclRowCnt,strSheetName);	    	
    	this.ele_DRPEditMethod.click();
  	
	}//End of try block
	catch(Exception e){			
  		System.out.println("Exception in <Method: verifySecLevel_DRP_DropDowns>"+e.getMessage()+",please check..!!");
	}//End of catch block
  }//End of <Method: verifySecLevel_DRP_DropDowns>     
  
  //###################################################################################################################################################################  
  //Function name		: verifyDRP_SaveCancel(int exclRowCnt,String strSheetName)
  //Class name			: SearchRes_CBMethod_Objs.java
  //Description 		: Function to verify Save & Close buttons in CBMethod->DRP pop up
  //Parameters 			: Excel row number for report and sheet name.
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifyDRP_SaveCancel(int exclRowCnt,String strSheetName,String strPopUpName){		
		try{
	    	if(!(btn_CBMSave.size()>0)){
	    		if(!(btn_CBMCancel.get(1).isDisplayed())){
		    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->DRP SAVE button is NOT displayed", "", "CBMethod_DRP_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->DRP SAVE button is NOT displayed,please check..!!");
	    		}
			}//End of if condition to check if SAVE button is displayed in DRP tab
	    	
	    	if(!(btn_CBMCancel.size()>0)){
	    		if(!(btn_CBMCancel.get(1).isDisplayed())){
		    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->DRP CANCEL button is NOT displayed", "", "CBMethod_DRP_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
		    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->DRP CANCEL button is NOT displayed,please check..!!");
		        	if(btn_CBMXClose.size()>0){
		        		btn_CBMXClose.get(0).click();
		        	}
	    		}
			}//End of if condition to check if Cancel button is displayed
	    	else{
	    		if(btn_CBMCancel.get(1).isDisplayed()) btn_CBMCancel.get(1).click();	    		
	    		Thread.sleep(5000);
	    	}
	    	
		}//End of try block
		catch(Exception e){			
    		System.out.println("Exception in <Method: verifyDRP_SaveCancel>"+e.getMessage()+",please check..!!");
		}//End of catch block
    }//End of <Method: verifyDRP_SaveCancel>    
  
   
    //###################################################################################################################################################################  
    //Function name		: verifyDetails_Checkpoints(int exclRowCnt,String strSheetName)
    //Class name		: SearchRes_CBMethod_Objs.java
    //Description 		: Function to verify the drop downs in Raw Trades->Details pop up
    //Parameters 		: Excel row number for report and sheet name.
    //					: Excel sheet name
    //Assumption		: RawTrades->Details pop up is displayed.
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
      public void verifyDetails_Checkpoints(int exclRowCnt,String strSheetName){		
    	try{	
    		//SELL METHOD drop down verification
    		if(ele_Det_SellMethod.size()>0) 	this.ele_Det_SellMethod.get(0).click();
    		else {
        		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->Details->Current Sell Method label is NOT displayed", "", "Det_SellMethod",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        		System.out.println("Checkpoint :[Failed] RawTrades->Details->Current Sell Method label is NOT displayed,so not validating dropdown,please check..!!");
        		return;
    		}
        	SearchRes_Action.compareDropDown("DETAILS_CBM","SELLMETHOD",exclRowCnt,strSheetName);	    	
        	this.ele_Det_SellMethod.get(0).click();
        	
        	//Verify Save & Cancel Exists
        	SearchRes_Action.verifyDetails_SaveCancel(exclRowCnt,strSheetName,"RawTrades_Details");
//        	if(btn_CBMXClose.size()>0){
//        		btn_CBMXClose.get(0).click();
//        	}
        	
    	}//End of try block
    	catch(Exception e){			
      		System.out.println("Exception in <Method: verifyDetails_Checkpoints>Exception:"+e.getMessage()+",please check..!!");
    	}//End of catch block
      }//End of <Method: verifyDetails_Checkpoints>         
    
      
  //###################################################################################################################################################################  
  //Function name		: verifyVSP_Checkpoints(int exclRowCnt,String strSheetName)
  //Class name			: SearchRes_CBMethod_Objs.java
  //Description 		: Function to verify the drop downs in Raw Trades->VSP pop up
  //Parameters 			: Excel row number for report and sheet name.
  //					: Excel sheet name
  //Assumption			: RawTrades->VSP pop up is displayed.
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public void verifyVSP_Checkpoints(int exclRowCnt,String strSheetName){		
    	LocalDateTime strStartWaitTime,strEndWaitTime;
    	try{	
  		//Dynamic wait for 3minutes for VSP data to display.
		strStartWaitTime = LocalDateTime.now();
		try{
			System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
									wait.withTimeout(180,TimeUnit.SECONDS)
									.pollingEvery(2, TimeUnit.SECONDS)				 
									.ignoring(NoSuchElementException.class)
									.until(ExpectedConditions.visibilityOfAllElements(SearchRes_Action.tbl_VSP_Body));
		}//End of try block for FluentWait
		
		catch(org.openqa.selenium.TimeoutException e){
			strEndWaitTime = LocalDateTime.now();
			System.out.println("Dynamic Wait time Ends with a Max threshold wait of 3minutes @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
			
			//Check if page is loading:			
			if(CommonUtils.isElementPresent(ele_Loading)){
				System.out.println("Checkpoint : VSP pop data - Stil Loading..[Failed]");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] VSP pop data:Failed,still Loading[Waited for Max threshold of 3minutes]", "", "VSP_Loading",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of IF condition to check for loading object.

			//Checkpoint: Check for Errors:
			if(CommonUtils.isElementPresent(ele_VSPError)){
				String strErMsg = ele_VSPError.getAttribute("textContent");
				System.out.println("Checkpoint : Error in VSP Pop Up, Error Message is - "+strErMsg);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] VSP Pop Up_ErrorMessage:"+strErMsg, "", "VSP_Error",  driver, "HORIZONTALLY", strSheetName, null);
				return ;
			}//End of if condition to to see if there are errors.				
		}//End of Catch block for FluentWait
		catch(Exception e){
			strEndWaitTime = LocalDateTime.now();
			System.out.println("Dynamic Wait time Ends with a Max threshold wait of 3minutes @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
			return ;
		}//End of catch block with generic exception for Fluent Wait 
  		
  		
  		//Checkpoint: Verify if there are errors in VSP Pop up:
		if(CommonUtils.isElementPresent(ele_VSPError)){
			String strErMsg = ele_VSPError.getAttribute("textContent");
			System.out.println("Checkpoint : Error in VSP Pop Up, Error Message is - "+strErMsg);
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] VSP Pop Up_ErrorMessage:"+strErMsg, "", "VSP_Error",  driver, "HORIZONTALLY", strSheetName, null);
			return ;
		}//End of if condition to to see if there are errors.
  		
		Thread.sleep(5000);
  		//Dynamic wait for 60sec for VSP data to display.
		strStartWaitTime = LocalDateTime.now();
		try{
			System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
									wait.withTimeout(60,TimeUnit.SECONDS)
									.pollingEvery(2, TimeUnit.SECONDS)				 
									.ignoring(NoSuchElementException.class)
									.until(ExpectedConditions.visibilityOfAllElements(ele_PercToApply));
		}//End of try block for FluentWait
		
		catch(org.openqa.selenium.TimeoutException e){
			strEndWaitTime = LocalDateTime.now();
			System.out.println("[VSP->PercToApply element]Dynamic Wait time Ends with a Max threshold wait of 60Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
							
		}//End of Catch block for FluentWait
		catch(Exception e){
			strEndWaitTime = LocalDateTime.now();
			System.out.println("[VSP->PercToApply element]Dynamic Wait time Ends with a Max threshold wait of 60Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
			return ;
		}//End of catch block with generic exception for Fluent Wait 
		
		
  		//Checkpoint: Verify VSP '%to Apply' field displayed
  		if(!(ele_PercToApply.size()>0)) 	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSP->'% to Apply' field is NOT displayed", "", "VSP_PerApply",  driver, "HORIZONTALLY", strSheetName, null);
  		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSP->'% to Apply' field is displayed", "", "VSP_PerApply",  driver, "HORIZONTALLY", strSheetName, null);

  		//Checkpoint: Verify VSP 'Apply to all Lots button' displayed
  		if(!(btn_Vsp_ApplyAll.size()>0)) 	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSP->'Apply to all Lots' button is NOT displayed", "", "VSP_ApplyAllLots",  driver, "HORIZONTALLY", strSheetName, null);  		
  		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSP->'Apply to all Lots' button is displayed", "", "VSP_ApplyAllLots",  driver, "HORIZONTALLY", strSheetName, null);
  		//Checkpoint: Verify VSP header and atleast one row displayed
  		SearchRes_Action.PopUp_VSPData(exclRowCnt, strSheetName);
      	
      	//Verify Back , Save & Cancel Exists
      	if(!SearchRes_Action.verifyVSPPopUp_BackSaveCancel("VSP",exclRowCnt, strSheetName));
          	if(btn_CBMXClose.size()>0){
          		btn_CBMXClose.get(0).click();
          	}
      	
  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: verifyVSP_Checkpoints>Exception:"+e.getMessage()+",please check..!!");
  	}//End of catch block
    }//End of <Method: verifyVSP_Checkpoints>       
      
    //###################################################################################################################################################################  
    //Function name		: verifyVSPAdvanced_Checkpoints(int exclRowCnt,String strSheetName)
    //Class name			: SearchRes_CBMethod_Objs.java
    //Description 		: Function to verify the drop downs in Raw Trades->VSP pop up
    //Parameters 			: Excel row number for report and sheet name.
    //					: Excel sheet name
    //Assumption			: RawTrades->VSP pop up is displayed.
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
      public boolean verifyVSPAdvanced_Checkpoints(int exclRowCnt,String strSheetName){		
    	try{	   		
    		
    		//Checkpoints : Filter VSP Lots 
    		//Checkpoint: Verify 'Filter VSP lots' section is displayed and Verify "Start Date/End date / Max Unit Cost / Min Unit Cost feilds and Search button" is displayed, else write a failed step in report.
    		if(!CommonUtils.isElementPresent(ele_FilterLabel))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Filter VSP Lots' section label is NOT displayed", "", "VSP_FilterLabel",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Filter VSP Lots' section label is displayed", "", "VSP_FilterLabel",  driver, "HORIZONTALLY", strSheetName, null); 
    		
    		if(!CommonUtils.isElementPresent(ele_startDate))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Filter VSP Lots->Start Date field' is NOT displayed", "", "VSP_StartDate",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Filter VSP Lots->Start Date field' is displayed", "", "VSP_StartDate",  driver, "HORIZONTALLY", strSheetName, null); 
    		
    		if(!CommonUtils.isElementPresent(ele_endDate))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Filter VSP Lots->End Date field' is NOT displayed", "", "VSP_EndDate",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Filter VSP Lots->End Date field' is displayed", "", "VSP_EndDate",  driver, "HORIZONTALLY", strSheetName, null); 
    		
    		if(!CommonUtils.isElementPresent(ele_MaxCost))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Filter VSP Lots->Max Cost field' is NOT displayed", "", "VSP_MaxCost",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Filter VSP Lots->Max Cost field' is displayed", "", "VSP_MaxCost",  driver, "HORIZONTALLY", strSheetName, null); 

    		if(!CommonUtils.isElementPresent(ele_MinCost))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Filter VSP Lots->Min Cost field' is NOT displayed", "", "VSP_MinCost",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Filter VSP Lots->Min Cost field' is displayed", "", "VSP_MinCost",  driver, "HORIZONTALLY", strSheetName, null); 
    		
    		if(!(btn_VSPSearch.size()>0))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Filter VSP Lots->Search button' is NOT displayed", "", "VSP_Search_Btn",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Filter VSP Lots->Search button' is displayed", "", "VSP_Search_Btn",  driver, "HORIZONTALLY", strSheetName, null); 
    		
    		
    		//Checkpoint:Verify 'Returned Results' section is displayed , else write a failed step in report and skip header verification. and Verify header = "Security;TradeDate;Qty;Unit Cost;Cost" is displayed, else write a failed step in report.
    		if(!CommonUtils.isElementPresent(ele_RetResults))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Returned Results' section label is NOT displayed", "", "VSP_RetResultsLabel",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Returned Results' section label is displayed", "", "VSP_RetResults",  driver, "HORIZONTALLY", strSheetName, null); 
    		
    		//Verify header:
    		SearchRes_Action.VSPAdv_Header("ReturnedResults",exclRowCnt,strSheetName);
    		
    		
    		//Checkpoint: Verify 'Selected Lots' section is displayed , else write a failed step in report and skip header verification. and Verify % to Apply" field, and "Applyt to all lots" button is displayed, else write a failed step in report.
    		if(!CommonUtils.isElementPresent(ele_SelectedLots))   	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSPAdvanced->'Selected Lots' section label is NOT displayed", "", "VSP_SelectedLots",  driver, "HORIZONTALLY", strSheetName, null);
    		else reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] RawTrades->VSPAdvanced->'Selected Lots' section label is displayed", "", "VSP_SelectedLots",  driver, "HORIZONTALLY", strSheetName, null); 

    		//Checkpoint: Verify VSP '%to Apply' field displayed
    		if(!(ele_PercToApply.size()>0)) 	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSP->'% to Apply' field is NOT displayed", "", "VSP_PerApply",  driver, "HORIZONTALLY", strSheetName, null);

    		//Checkpoint: Verify VSP 'Apply to all Lots button' displayed
    		if(!(btn_Vsp_ApplyAll.size()>0)) 	reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] RawTrades->VSP->'Apply to all Lots' button is NOT displayed", "", "VSP_ApplyAllLots",  driver, "HORIZONTALLY", strSheetName, null);  		
    		//Verify header:
    		SearchRes_Action.VSPAdv_Header("SelectedLots",exclRowCnt,strSheetName);

    		
        	//Verify Back , Save & Cancel Exists
        	if(!SearchRes_Action.verifyVSPPopUp_BackSaveCancel("VSPAdvanced",exclRowCnt, strSheetName)) return false;
//            	if(btn_CBMXClose.size()>0){
//            		btn_CBMXClose.get(0).click();
//            	}
        	return true;
    	}//End of try block
    	catch(Exception e){			
      		System.out.println("Exception in <Method: verifyVSPAdvanced_Checkpoints>Exception:"+e.getMessage()+",please check..!!");
      		return false;
    	}//End of catch block
      }//End of <Method: verifyVSPAdvanced_Checkpoints>      
      
//###################################################################################################################################################################  
//Function name		: getExp_AccLevel_CBM_DropDowns(int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to verify the drop downs in CB Method->CBM Pop up
//Parameters 		: Excel row number for report and sheet name.
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public ArrayList<String> getExp_AccLevel_CBM_DropDowns(String strDropDown){		
		ArrayList<String> arrExpDropDown = new ArrayList<String>();
		switch(strDropDown.toUpperCase()){
			case "SECTYPE":
				arrExpDropDown.addAll(Arrays.asList("Stocks, Options, Bonds...","Mutual Fund","ETF RIC"));
				return arrExpDropDown;
			case "VGI_SECTYPE":
				arrExpDropDown.addAll(Arrays.asList("Stocks, Options, Bonds...","Mutual Fund","ETF RIC", "Dividend Reinvestment Plan Shares"));
    			return arrExpDropDown;
			case "SELLMETHOD":
				arrExpDropDown.addAll(Arrays.asList("Maximum Gain","Last In First Out","Highest In - First Out","First In - First Out","Minimum Tax")); //read only
    			return arrExpDropDown;
			case "SCB_SELLMETHOD":
				arrExpDropDown.addAll(Arrays.asList("Maximum Gain","Last In First Out","Highest In - First Out","First In - First Out","Minimum Tax","Modified MINTAX")); //read only
    			return arrExpDropDown;
			case "STRATEGY":
				arrExpDropDown.addAll(Arrays.asList("Apply strategy change to all sells going forward from today","Apply strategy change to entire portfolio and entire year","Apply strategy change from prior date going forward")); //read only
    			return arrExpDropDown;
			case "USA_STRATEGY":
				arrExpDropDown.addAll(Arrays.asList("Apply strategy change to all sells going forward from today")); //read only
    			return arrExpDropDown;    			
			default:
    			return null;
		}//End of Switch case
  
  }//End of <Method: getExp_AccLevel_CBM_DropDowns>
  //###################################################################################################################################################################  
  //Function name	: getExp_SecLevel_CBM_DropDowns(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_ActionMenu.java
  //Description 	: Function to verify the drop downs in CB Method->CBM Pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
    public ArrayList<String> getExp_SecLevel_CBM_DropDowns(String strDropDown){		
  		ArrayList<String> arrExpDropDown = new ArrayList<String>();
  		switch(strDropDown.toUpperCase()){
  			case "METHOD":
  				arrExpDropDown.addAll(Arrays.asList("First In - First Out","Highest In - First Out","Last In First Out","Maximum Gain","Minimum Tax"));
      			return arrExpDropDown;
  			case "SCB_METHOD":
  				arrExpDropDown.addAll(Arrays.asList("First In - First Out","Highest In - First Out","Last In First Out","Maximum Gain","Minimum Tax","Modified MINTAX"));
      			return arrExpDropDown;
  			case "EFFECTIVE":
  				arrExpDropDown.addAll(Arrays.asList("Edit from today's date forward","Edit from a previous date")); //read only
      			return arrExpDropDown;
  			default:
      			return null;
  		}//End of Switch case
    
    }//End of <Method: getExp_SecLevel_CBM_DropDowns>
    
//###################################################################################################################################################################  
//Function name		: getExp_SecLevel_AvgCost_CBM_DropDowns(int exclRowCnt,String strSheetName)
//Class name		: SearchRes_ActionMenu.java
//Description 		: Function to verify the drop downs in CB Method->CBM Pop up
//Parameters 		: Excel row number for report and sheet name.
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  public ArrayList<String> getExp_SecLevel_AvgCost_CBM_DropDowns(String strDropDown){		
		ArrayList<String> arrExpDropDown = new ArrayList<String>();
		switch(strDropDown.toUpperCase()){
			case "METHOD":
				arrExpDropDown.addAll(Arrays.asList("Average Cost","First In - First Out","Highest In - First Out","Last In First Out","Maximum Gain","Minimum Tax"));
    			return arrExpDropDown;

			default:
    			return null;
		}//End of Switch case
  
  }//End of <Method: getExp_SecLevel_AvgCost_CBM_DropDowns>
  //###################################################################################################################################################################  
  //Function name	: getExp_SecLevel_DRP_DropDowns(int exclRowCnt,String strSheetName)
  //Class name		: SearchRes_ActionMenu.java
  //Description 	: Function to verify the drop downs in CB Method->CBM Pop up
  //Parameters 		: Excel row number for report and sheet name.
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
    public ArrayList<String> getExp_SecLevel_DRP_DropDowns(String strDropDown){		
  		ArrayList<String> arrExpDropDown = new ArrayList<String>();
  		switch(strDropDown.toUpperCase()){
  			case "DRP":
  				arrExpDropDown.addAll(Arrays.asList("Enroll","Disenroll"));
      			return arrExpDropDown;
  			case "EFFECTIVE":
  				arrExpDropDown.addAll(Arrays.asList("Edit from today's date forward","Edit from a previous date")); //read only
      			return arrExpDropDown;
  			default:
      			return null;
  		}//End of Switch case
    
    }//End of <Method: getExp_SecLevel_DRP_DropDowns>
    
   
  //###################################################################################################################################################################  
  //Function name		: getExp_Details_SellMethod(String strDropDown)
  //Class name			: SearchRes_CBMethod_Objs
  //Description 		: Function to verify the RawTrades->Details pop up Sell method drop down values 
  //Parameters 			: Drop down name to be verified from the pop up
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public ArrayList<String> getExp_Details_SellMethod(String strDropDown){		
  		ArrayList<String> arrExpDropDown = new ArrayList<String>();
  		switch(strDropDown.toUpperCase()){
  			case "SELLMETHOD":
  				arrExpDropDown.addAll(Arrays.asList("MinTax","FIFO","LIFO","HIFO","MaxGain","VsPurchase","VsPurchase - Advanced Search")); //read only
      			return arrExpDropDown;
  			default:
      			return null;
  		}//End of Switch case
    
    }//End of <Method: getExp_Details_SellMethod>    
    
//###################################################################################################################################################################
}//End of <Class: SearchRes_CBMethod_Objs>
