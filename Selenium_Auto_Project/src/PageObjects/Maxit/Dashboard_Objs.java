package PageObjects.Maxit;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class Dashboard_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	//SearchRes_ActionMenu SearchRes_Action = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
	
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/MetricsAdmin/enter.php']")
    private WebElement lnk_Dashboard;
	
//    @FindBy(how=How.XPATH,using="//*[contains(@class,\"metricsAdmin\")]")
//	public List<WebElement> ele_MetricsHeaders;
	
    @FindBy(how=How.XPATH,using="//*[@id=\"rightColumn\"]/table/thead/tr/th")
	public WebElement ele_DailyStatus;
    @FindBy(how=How.XPATH,using="//*[@id=\"content\"]/table/tbody/tr[4]/td/table/thead/tr/th")
	public WebElement ele_SysMetrics;
    @FindBy(how=How.XPATH,using="//*[@id=\"content\"]/table/tbody/tr[5]/td/table/thead/tr/th")
	public WebElement ele_TPStatus;
    //@FindBy(how=How.PARTIAL_LINK_TEXT,using="CorporateActions")
    @FindBy(how=How.PARTIAL_LINK_TEXT,using="Details")
	public List<WebElement> lnk_Details;
    
    @FindBy(how=How.XPATH,using="//*[@class=\"errors\"]")
    private WebElement ele_Dash_Errors; 
	
  //*********************************************************************************************************************  
    //Performing Actions on objects in Search Page:
  //*********************************************************************************************************************   
  //###################################################################################################################################################################  
  //Function name	: VerifyDashboardLinkExists()
  //Class name		: Dashboard_Objs
  //Description 	: Function to verify if "Dashboard" link exists
  //Parameters 		: None
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
  	public boolean VerifyDashboardLinkExists(){
  		try{  			
  			if(CommonUtils.isElementPresent(lnk_Dashboard)){
  				return true;	
  			}//End of IF Condition to check if lnk_Dashboard element exists
  			return false;	
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <Dashboard_Objs.VerifyDashboardLinkExists>: Dashboard Link is not displayed in UI..!!!");
  			return false;
  		}
  	}//End of VerifyDashboardLinkExists Method	    
	
    //###################################################################################################################################################################  
    //Function name		: Navigate_DashboardPage(int exclRowCnt,String strSheetName)
    //Class name		: Dashboard_Objs
    //Description 		: Function to Navigate to "Dashboard" Page
    //Parameters 		: None
    //Assumption		: None
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
    	public boolean Navigate_DashboardPage(int exclRowCnt,String strSheetName){
    		try{  			
    			if(CommonUtils.isElementPresent(lnk_Dashboard)){
    				lnk_Dashboard.click();
    				//Checkpoint: Check for Errors:
    				if(CommonUtils.isElementPresent(ele_Dash_Errors)){
    					String strErMsg = ele_Dash_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in Dashboard Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DashboardPage_ErrorMessage:"+strErMsg, "", "DashboardPage_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.
    				
    				return true;	
    			}//End of IF Condition to check if lnk_Dashboard element exists
								
    			return false;	
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <Dashboard_Objs.Navigate_DashboardPage>: Dashboard Link is not displayed in UI..!!!");
    			reporter.reportStep(exclRowCnt, "Failed", "Dashboard Page_Exception in<Dashboard_Objs.Navigate_DashboardPage> Please Check..!!", "", "DashboardPage_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			return false;
    		}
    	}//End of Navigate_DashboardPage Method	
    	
        //###################################################################################################################################################################  
        //Function name		: Dashboard_Checkpoints(int exclRowCnt,String strSheetName)
        //Class name		: Dashboard_Objs
        //Description 		: Function to verify all checkpoints in "Dashboard" page
        //Parameters 		: None
        //Assumption		: None
        //Developer			: Kavitha Golla
        //###################################################################################################################################################################		
        	public void Dashboard_Checkpoints(int exclRowCnt,String strSheetName){
        		try{  			
        			if(CommonUtils.isElementPresent(ele_DailyStatus)){
						System.out.println("Checkpoint : Daily Status header is displayed");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] DashboardPage_Daily Status header is displayed", "", "DailyStatus",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        			}//End of if condition to to check if Daily  Status Header is displayed. 
        			else{
						System.out.println("Checkpoint : Daily Status header is NOT displayed");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DashboardPage_Daily Status header is NOT displayed", "", "DailyStatus",  driver, "BOTH_DIRECTIONS", strSheetName, null);        			
        			}//End of ELSE Condition to check if Daily Status Header exists
    							
        			if(CommonUtils.isElementPresent(ele_SysMetrics)){
						System.out.println("Checkpoint : System Metrics header is displayed");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] DashboardPage_System Metrics header is displayed", "", "SystemMetrics ",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        			}//End of if condition to to check if SystemMetrics Header is displayed. 
        			else{
						System.out.println("Checkpoint : System Metrics header is NOT displayed");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DashboardPage_System Metrics header is NOT displayed", "", "SystemMetrics",  driver, "BOTH_DIRECTIONS", strSheetName, null);        			
        			}//End of ELSE Condition to check if SystemMetrics Header exists
        			
        			if(CommonUtils.isElementPresent(ele_TPStatus)){
						System.out.println("Checkpoint : TP Status header is displayed");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] DashboardPage_TP Status header is displayed", "", "TPStatus",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        			}//End of if condition to to check if TP  Status Header is displayed. 
        			else{
						System.out.println("Checkpoint : TP Status header is NOT displayed");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DashboardPage_TP Status header is NOT displayed", "", "TPStatus",  driver, "BOTH_DIRECTIONS", strSheetName, null);        			
        			}//End of ELSE Condition to check if TP Status Header exists
        			
        			if(lnk_Details.size()==2){
						System.out.println("Checkpoint : Total Details links displayed are 2");
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] DashboardPage_Total 2 'Details' links are displayed", "", "Details_Link",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        			}//End of if condition to to check if there are 2 Details links displayed
        			else{
						System.out.println("Checkpoint : Total Details links displayed are NOT 2,pleae check..!!");
						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DashboardPage_'Details' links are displayed are not 2,please check.!", "", "Details_Link",  driver, "BOTH_DIRECTIONS", strSheetName, null);        			
        			}//End of ELSE Condition to check if there are 2 Details links displayed
        			

        			
        			
        		}//End of Try block
        		
        		
        		catch(Exception e){
        			System.out.println("Exception in <Dashboard_Objs.Dashboard_Checkpoints>: Please Check..!!!");
        			reporter.reportStep(exclRowCnt, "Failed", "Dashboard Page_Exception in<Dashboard_Objs.Dashboard_Checkpoints> Please check..!!", "", "DashboardPage_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        		}
        	}//End of Dashboard_Checkpoints Method	 	
    	
	
}//End of <Class:Dashboard_Objs>
