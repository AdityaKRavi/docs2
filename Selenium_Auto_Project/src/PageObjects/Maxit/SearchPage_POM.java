package PageObjects.Maxit;


import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.NoSuchElementException;


import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class SearchPage_POM {
	//WebDriver driver;
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	//WebDriverWait wait = new WebDriverWait(driver,30);
	Reporting reporter = new Reporting();
	//Variables declaration:
    
//    @FindBy(how=How.XPATH,using="//*[contains(@class,'navMenuBullet') and contains(@text,'Search')]")
	@FindBy(how=How.XPATH,using="//a[@href ='/Modules/Accounts/Maxit/Search/enter.php']")
    private WebElement lnk_Search;
    
    @FindBy(how=How.ID,using="initialView")
    private WebElement lst_View;
    
    @FindBy(how=How.ID,using="account")
    public WebElement txtbx_Account;
    
    @FindBy(how=How.ID,using="securityIDType")
    private WebElement lst_SecIDType;
   
    @FindBy(how=How.ID,using="securityID")
    private WebElement txtbx_SecurityID;
    
    @FindBy(how=How.XPATH,using="//*[@id=\"maxitSearchForm\"]/div[6]/div/input")
    private WebElement btn_Submit;

//    @FindBy(how=How.XPATH,using=".//*[@class='x-panel-header x-unselectable']")
    @FindBy(how=How.XPATH,using=".//*[@class='x-panel-header-text']")
    private WebElement ele_SearchPagTitle;   
    
    @FindBy(how=How.XPATH,using="//*[@class=\"ext-el-mask-msg x-mask-loading\"]")
    private WebElement ele_Loading; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
    private WebElement ele_ResTbl_Header; 
    
    @FindBy(how=How.XPATH,using="//*[@class=\"errors\"]")
    private WebElement ele_Errors; 
    
    
//*********************************************************************************************************************  
    //Performing Actions on objects in Search Page:
//*********************************************************************************************************************   
  //###################################################################################################################################################################  
  //Function name		: VerifySearchLinkExists(WebDriver driver)
  //Class name		: SearchPage
  //Description 		: Function to verify if Search link exists
  //Parameters 		: driver object
  //Assumption		: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
  	public boolean VerifySearchLinkExists(){
  		try{  			
  			if(CommonUtils.isElementPresent(lnk_Search)){
  				return true;	
  			}//End of IF Condition to check if lnk_Search element exists
  			return false;	
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_POM.VerifySearchLinkExists>: Search Link is not displayed in UI..!!!");
  			return false;
  		}
  	}//End of VerifySearchLinkExists Method	

  //###################################################################################################################################################################  
  //Function name		: ClickSearchLink(WebDriver driver)
  //Class name		: SearchPage
  //Description 		: Function to Navigate to Search Page
  //Parameters 		: driver object
  //Assumption		: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
  	public boolean ClickSearchLink(){
  		try{
  			if(CommonUtils.isElementPresent(lnk_Search)){
  				//String path = CommonUtils.captureScreenshot("Search Page_Click Search Link", "Search Page_Click Search Link", "HORIZONTALLY", lnk_Search);
  				//System.out.println("Search Page_Click Search Link[Passed]- Image Path is:"+path);
  				lnk_Search.click();
  				return true;	
  			}//End of IF Condition to check if lnk_Search element exists

  			//String path = CommonUtils.captureScreenshot("Search Page_Click Search Link[Failed]", "Search Page_Click Search Link", "HORIZONTALLY", lnk_Search);
  			//System.out.println("Search Page_Click Search Link[Failed]- Image Path is:"+path);
  			return false;	
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_POM.ClickSearchLink>: Search Link is not displayed in UI..!!!");
  			e.printStackTrace();
  			return false;
  		}
  	}//End of ClickSearchLink Method	
  		
  	
//###################################################################################################################################################################  
//Function name		: VerifyViewList()
//Class name		: SearchPage
//Description 		: Function to verify the View list dropdown in Search Page
//Parameters 		: driver object
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
	public boolean VerifyViewList(){ 
		List<String> actViewList = new ArrayList<String>();
		ArrayList<String> expViewList = new ArrayList<String>();
		expViewList.addAll(Arrays.asList("Account","Open/Closed","Security Xref","Pos Recon","Trx Error","Raw Trades","Ledger","Unrealized","Realized","No Cost Basis","Audit Trail"));
		//System.out.println("Expected View List is : "+expViewList.toString());		
		
		List<WebElement> ViewList = lst_View.findElements(By.tagName("option"));
		for (WebElement option : ViewList) {
			actViewList.add(option.getText());
		}//End of FOR loop to read View List from Application
		//System.out.println("Actual View List is : "+actViewList.toString());
		
		if(!actViewList.toString().equals(expViewList.toString())){
			//CommonUtils.captureScreenshot("Search Page_View dropdown[Failed]", "Search Page_View dropdown - Actual View List ["+actViewList.toString()+"] and Expected View list ["+expViewList.toString()+"]", "HORIZONTALLY", null);
			System.out.println("Checkpoint_Fail:Verifying View weblist in Search Page.Expected View List = "+expViewList.toString()+"; Actual View List = "+actViewList.toString());
			return false;
		}//End of IF condition to check Expected View List & Actual View List
		return true;
}//End of VerifyViewList method    	  	
  	
//###################################################################################################################################################################  
//Function name		: VerifySearchPageTitle(WebDriver driver)
//Class name		: SearchPage
//Description 		: Function to that the page is Navigated to Search Page
//Parameters 		: driver object
//Assumption		: None
//Developer			: Kavitha Golla
//###################################################################################################################################################################		
  	public boolean VerifySearchPageTitle(){
  		try{
  			
  			if(CommonUtils.isElementPresent(ele_SearchPagTitle)){
  				if(ele_SearchPagTitle.getAttribute("textContent").trim().equals("Search Criteria")){
  					return true;
  				}
  				else{
  					System.out.println("Search Page title is not correct, Expected:Search Criteria and Actual:"+ele_SearchPagTitle.getText());
  					return false;
  				}
  			}//End of IF Condition to check if ele_SearchPagTitle element exists
  			return false;	
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception in <SearchPage_POM.VerifySearchPageTitle>: Search Page is not displayed in UI..!!!");
  			return false;
  		}
  	}//End of VerifySearchPageTitle Method	
  
  //###################################################################################################################################################################  
  //Function name		: SelectViewPage()
  //Class name			: SearchPage_POM
  //Description 		: Function to select the page view from "View List"
  //Parameters 			: 
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    	public boolean SelectViewPage(String strViewPage){
    		try{
    			if(!VerifyViewList()){    				
    				return false;
    			}
    			List<WebElement> ViewList = lst_View.findElements(By.tagName("option"));
    			for (WebElement option : ViewList) {
    				if(option.getText().equals(strViewPage)){
    					System.out.println("Navigating to Search Page View : "+strViewPage.toString());
    					option.click();    					
    					return true;
    				}    				
    			}//End of FOR loop to read View List from Application
    			return false;	
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <SearchPage_POM.SelectViewPage>: Search Page is not displayed in UI..!!!");
    			return false;
    		}
    	}//End of SelectViewPage Method  	
 
  //###################################################################################################################################################################  
  //Function name		: SearchPage_Input()
  //Class name			: SearchPage_POM
  //Description 		: Function to input data into Search Page
  //Parameters 			: 
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
	public boolean SearchPage_Input(String strAccount,String strSecIDType,String strSecValue,int exclRowCnt,String strSheetName){
		LocalDateTime strStartWaitTime,strEndWaitTime;
		String strSearchValues = strAccount+";"+strSecIDType+";"+strSecValue;
		try{
			boolean blnSecTypeCheck = false;
			txtbx_Account.sendKeys(strAccount.replace("\u00A0","").trim());
			List<WebElement> SecType_List = lst_SecIDType.findElements(By.tagName("option"));
			for (WebElement option : SecType_List) {
				if(option.getText().equals(strSecIDType)){
					//System.out.println("Selecting SecIDTYpe : "+strSecIDType.toString());
					option.click();
					blnSecTypeCheck=true;
					break;
				}
			}//End of FOR loop
			if(!blnSecTypeCheck){
				for (WebElement option : SecType_List) {
					if(option.getText().equals("Security No")){
						System.out.println("Selecting default SecIDType as Security No");
						option.click();
						blnSecTypeCheck=true;
						break;
					}
				}//End of FOR loop
			}//End of IF condition to select default SecIDTpe= Security ID

			txtbx_SecurityID.sendKeys(strSecValue.replace("\u00A0","").trim());
			//Thread.sleep(5000);			
			btn_Submit.click();

			
			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
			strStartWaitTime = LocalDateTime.now();
			try{
				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
										wait.withTimeout(35,TimeUnit.SECONDS)
										.pollingEvery(2, TimeUnit.SECONDS)				 
										.ignoring(NoSuchElementException.class)
										.until(ExpectedConditions.visibilityOf(ele_ResTbl_Header));
			}//End of try block for FluentWait
			
			catch(org.openqa.selenium.TimeoutException e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				
				//Check if page is loading:			
				if(CommonUtils.isElementPresent(ele_Loading)){
					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search after input values["+strSearchValues+"]:Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of IF condition to check for loading object.

				//Checkpoint: Check for Errors:
				if(CommonUtils.isElementPresent(ele_Errors)){
					String strErMsg = ele_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.				
			}//End of Catch block for FluentWait
			catch(Exception e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return false;
			}//End of catch block with generic exception for Fluent Wait 
			
			//Checkpoint: Check for Errors:
			if(CommonUtils.isElementPresent(ele_Errors)){
				String strErMsg = ele_Errors.getAttribute("textContent");
				System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of if condition to to see if there are errors.
			
			//Check for Results header
			if(CommonUtils.isElementPresent(ele_ResTbl_Header)){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Checkpoint : Search Page Navigation - [Passed]");
				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Search After Input data values["+strSearchValues+"] :Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				//System.out.println(LocalDateTime.now());
				return true;
			}//End of IF condition to check for Results table header.			
						
			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Search after input values["+strSearchValues+"]:Failed, Results header is not displayed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <SearchPage_POM.SearchPage_Input>: Search Page is not displayed in UI..!!!");
			System.out.println("Checkpoint : Search Page Navigation - [Failed]");
			e.printStackTrace();
			return false;
		}
	}//End of SearchPage_Input Method      	
   	
  //###################################################################################################################################################################  
  //Function name		: SearchPage_InputBlank()
  //Class name			: SearchPage_POM
  //Description 		: Function to input data into Search Page
  //Parameters 			: 
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
	public boolean SearchPage_InputBlank(int exclRowCnt,String strSheetName){
		LocalDateTime strStartWaitTime,strEndWaitTime;
		try{
			btn_Submit.click();
			Thread.sleep(5500);
			//driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
			//wait.until(ExpectedConditions.visibilityOf(ele_ResTbl_Header));
			
			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
			strStartWaitTime = LocalDateTime.now();
			try{
				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
										wait.withTimeout(35,TimeUnit.SECONDS)
										.pollingEvery(2, TimeUnit.SECONDS)				 
										.ignoring(NoSuchElementException.class)
										.until(ExpectedConditions.visibilityOf(ele_ResTbl_Header));
			}//End of try block for FluentWait
			
			catch(org.openqa.selenium.TimeoutException e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				
				//Check if page is loading:			
				if(CommonUtils.isElementPresent(ele_Loading)){
					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
					reporter.reportStep(exclRowCnt, "Failed", "Search after input:Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of IF condition to check for loading object.

				//Checkpoint: Check for Errors:
				if(CommonUtils.isElementPresent(ele_Errors)){
					String strErMsg = ele_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "HORIZONTALLY", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.				
			}//End of Catch block for FluentWait
			catch(Exception e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return false;
			}//End of catch block with generic exception for Fluent Wait
			
			
			//Checkpoint: Check for Errors:
			if(CommonUtils.isElementPresent(ele_Errors)){
				String strErMsg = ele_Errors.getAttribute("textContent");
				System.out.println("Checkpoint : Error in Search Page, Error Message is - "+strErMsg);
				reporter.reportStep(exclRowCnt, "Failed", "SearchPage_ErrorMessage:"+strErMsg, "", "SearchPage_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;
			}//End of if condition to to see if there are errors.
			
			//Check for Results header
			if(CommonUtils.isElementPresent(ele_ResTbl_Header)){
				System.out.println("Checkpoint : Search Page Navigation - [Passed]");
				reporter.reportStep(exclRowCnt, "Passed", "Search After Input data insert:Passed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return true;
			}//End of IF condition to check for Results table header.			
			
			
			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Search after input:Failed, Results header is not displayed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <SearchPage_POM.SearchPage_InputBlank>: Search Page is not displayed in UI..!!!");
			System.out.println("Checkpoint : Search Page Navigation - [Failed]");
			e.printStackTrace();
			return false;
		}
	}//End of SearchPage_InputBlank Method   	
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
  //###################################################################################################################################################################  
  //Function name	: VerifySearchLinkExists(WebDriver driver)
  //Class name		: SearchPage
  //Description 	: Function to verify if Search link exists
  //Parameters 		: driver object
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
//	public static boolean VerifySearchLinkExists(WebDriver driver) throws Exception{ 
//		try{
//			lnk_Search = driver.findElement(By.partialLinkText("Search")); //xpath : //*[@id="navMenu"]/ul/li[3]/ul/li[2]/a
//			if(lnk_Search.isDisplayed()){
//				lnk_Search.click();
//				return true;
//			}
//			else{
//				System.out.println("SearchPage.lnk_Search():(Search link is not displayed in browser.");
//				return false;
//			}//End of Else condition
//		}//End of Try block
//		catch(Exception e){
//			return false;
//		}
//	}//End of lnk_Search method
	
////*********************************************************************************************************************
//	public static boolean list_View(WebDriver driver,String strViewPage){ 
//		element = driver.findElement(By.id("initialView"));
//		WebElement select = driver.findElement(By.id("initialView"));
//		List<WebElement> ViewList = select.findElements(By.tagName("option"));
//		for (WebElement option : ViewList) {
//			if(strViewPage.equals(option.getText().trim())){
//			 option.click(); 
//			 System.out.println("View dropdown in Search Page is selected to: "+strViewPage);
//			 return true;
//			}//End of IF condition		
//			else {
//				System.out.println("SearchPage->listView(). There is no element as "+strViewPage+" under View dropdown in Search Page");
//				return false;
//			}//End of Else condition
//		}//End of For loop to loop through each element in the View web list.	
//		return false;
//	}//End of list_View method	
//	
////*********************************************************************************************************************
////	public WebElement txtbx_Account(WebDriver driver){ 
////		element = driver.findElement(By.id("account")); 
////		return element;
////	}//End of txtbx_Account method		
//
////*********************************************************************************************************************
//	public static boolean list_SecType(WebDriver driver,String strSecType){ 
//		WebElement select = driver.findElement(By.id("securityIDType"));
//		List<WebElement> SecTypeList = select.findElements(By.tagName("option"));
//		for (WebElement option : SecTypeList) {
//			if(strSecType.equals(option.getText().trim())){
//				option.click();   
//				System.out.println("SecType dropdown in Search Page is selected to: "+strSecType);				
//			}//End of IF condition
//			else {
//				System.out.println("SearchPage->list_SecType(). There is no element as "+strSecType+" under SecurityID dropdown in Search Page");
//				return false;				
//			}//End of Else condition
//			}//End of For loop to loop through each element in the SecType web list.
//		return false;
//	}//End of list_SecType method

//*********************************************************************************************************************	
	
	
	
//###########################<< End of Objects identification >>######################################################################################################
//###########################<< Actions on Objects >>######################################################################################################

	
	
	
//#################################################################################################################################
}//End of <Class: SearchPage>
