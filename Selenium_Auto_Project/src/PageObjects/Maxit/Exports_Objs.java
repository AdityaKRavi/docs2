
package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class Exports_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	WebDriverWait wait;

	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);

	//Imports
	@FindBy(how=How.XPATH,using="//a[text()='Exports']")
	public WebElement lnk_Exports;

	@FindBy(how=How.LINK_TEXT,using="Export to Excel")
	public WebElement lnk_ExportToExcel;	

	@FindBy(how=How.ID,using="cbiExportsTable")
	public WebElement ele_Exports_ResTable;

	@FindBy(how=How.XPATH,using="//*[@id=\"cbiExportsTable\"]/thead")
	public List<WebElement> ele_Exports_ResTable_Header;

	@FindBy(how=How.XPATH,using="//*[@id=\"cbiExportsTable\"]/tbody")
	public List<WebElement> ele_Exports_ResTable_Body;

	@FindBy(how=How.XPATH,using="//*[@id=\"noData\"]")
	public WebElement ele_NoResults;


	@FindBy(how=How.ID,using="status")
	public WebElement lst_ExportStatus;

	@FindBy(how=How.ID,using="dateFrom")
	public WebElement txtbx_dateFrom;

	@FindBy(how=How.ID,using="dateTo")
	public WebElement txtbx_dateTo;


	@FindBy(how=How.ID,using="selectAllBtnTop")
	public WebElement ele_SelectAllEligibleRecords_btn;

	@FindBy(how=How.ID,using="releaseBtnTop")
	public WebElement ele_ReleaseSelectedRecords_btn;


	@FindBy(how=How.XPATH,using="//*[contains(@id,'rolloverTarget_exports0')]")
	public List<WebElement> ele_ExportsAction_Row1;


	@FindBy(how=How.XPATH,using="//*[contains(@id,'rolloverMenu_exports')]")
	public List<WebElement> ele_ExportsActionMenu;
	@FindBy(how=How.XPATH,using="(//*[contains(@id,'rolloverMenu_exports0')])[1]")
	public List<WebElement> ele_ExportsActionMenu_Row1;
	
	public static String EXPORTS_PAGE_STATUS_ACCEPTED = "Accepted";
	public static String EXPORTS_PAGE_STATUS_REJECTED = "Rejected";

	LocalDateTime strStartWaitTime,strEndWaitTime;


	//*********************************************************************************************************************  
	//Performing Actions on objects in Search Page:
	//*********************************************************************************************************************      
	//###################################################################################################################################################################  
	//Function name		: Exports_Data_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName)
	//Class name		: Exports_Objs
	//Description 		: Function to perform search based on input data.
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean Exports_Data_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName){
		try{
			String strSearchValues = strTabName+";"+strAcct+";"+strSecType+";"+strSecValue+";"+strFromDate+";"+strFromDate;
			System.out.println("Inside Exports_Data_Search : " +strSearchValues);
			boolean blnSecTypeCheck = false;

			// Account number			
			CBMngr_Objs.txtbx_Acct.sendKeys(strAcct);

			//Logic to select the security type dropdown based on the parameter available in input sheet
			if(strSecType != null)
			{
				List<WebElement> SecType_List = CBMngr_Objs.lst_SecIDType.findElements(By.tagName("option"));
				for (WebElement option : SecType_List) {
					if(option.getText().equals(strSecType)){
						option.click();
						blnSecTypeCheck=true;
						break;
					}
				}//End of FOR loop
				if(!blnSecTypeCheck){
					for (WebElement option : SecType_List) {
						if(option.getText().equals("Security No")){
							System.out.println("Selecting default SecIDType as Security No");
							option.click();
							blnSecTypeCheck=true;
							break;
						}
					}//End of FOR loop
				}//End of IF condition to select default SecIDType= Security ID

				CBMngr_Objs.txtbx_SecurityID.sendKeys(strSecValue);

			}//End of IF condition null check

			//Logic to select the status dropdown as All by default.
			List<WebElement> Status_List = lst_ExportStatus.findElements(By.tagName("option"));
			System.out.println(" Status_List size : " +Status_List.size());
			for (WebElement option : Status_List) {
				if(option.getText().equals("All")){
					option.click();
					break;
				}
			}//End of FOR loop

			txtbx_dateFrom.sendKeys(strFromDate);
			txtbx_dateTo.sendKeys(strToDate);
			//Performing Data search
			CBMngr_Objs.btn_Submit.click();

			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			strStartWaitTime = LocalDateTime.now();
			try{
				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
				wait.withTimeout(50,TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)				 
				.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.visibilityOf(ele_Exports_ResTable));
			}//End of try block for FluentWait

			catch(org.openqa.selenium.TimeoutException e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());

				//Check if page is loading:			
				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"_1",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}//End of IF condition to check for loading object.

				///Checkpoint: Check for Errors
				if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			}//End of Catch block for FluentWait
			catch(Exception e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return false;
			}//End of catch block with generic exception for Fluent Wait 

			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			//Check for Results header
			if(CommonUtils.isElementPresent(ele_Exports_ResTable)){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Checkpoint : Search After Input data ["+strSearchValues+"] insert:Passed");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Search After Input data ["+strSearchValues+"] insert:Passed", "", strTabName+"_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return true;
			}//End of IF condition to check for Results table header.			

			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Warning] CB Manager->"+strTabName+" Search After Input data , Results header is not displayed. Header is not expected for all clients, so ignore this fail.", "", strTabName+"_2",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;

		}//End of Try block
		catch(Exception e){
			System.err.println("Exception in <Exports_Objs.Exports_Data_Search>: CBManger->"+strTabName+" verifying header title.");
			return false;
		}//End of catch block
	}//End of <Method: Exports_Data_Search> 

	//###################################################################################################################################################################  
	//Function name		: Exports_BlankSearch(String strTabName,int exclRowCnt,String strSheetName)
	//Class name		: Exports_Objs
	//Description 		: Function to perform Blank search in CB Manager Imports tab 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean Exports_BlankSearch(String strTabName,int exclRowCnt,String strSheetName){
		try{			
			System.out.println("Inside Exports_BlankSearch : ");

			//Performing blank Search
			CBMngr_Objs.btn_Submit.click();

			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			strStartWaitTime = LocalDateTime.now();
			try{
				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
				wait.withTimeout(180,TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS)				 
				.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.visibilityOf(ele_Exports_ResTable));
			}//End of try block for FluentWait

			catch(org.openqa.selenium.TimeoutException e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());

				//Check if page is loading:			
				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
					System.out.println("Checkpoint : Blank Search - Stil Loading..[Failed]");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"_3",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}//End of IF condition to check for loading object.

				//Checkpoint: Check for Errors
				if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			}//End of Catch block for FluentWait
			catch(Exception e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return false;
			}//End of catch block with generic exception for Fluent Wait 

			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			//Check for Results header
			if(CommonUtils.isElementPresent(ele_Exports_ResTable)){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Checkpoint : CB Manager->\"+strTabName+\" Performing : Blank Search");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Performing : Blank Search", "", strTabName+"_BlankSearch",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return true;
			}//End of IF condition to check for Results table header.			

			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Warning] CB Manager->"+strTabName+" Blank Search - Results header is not displayed[Ignore this warning], Header is expected only for few clients.", "", strTabName+"_4",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Exports_Objs.Exports_BlankSearch>: CBManger->"+strTabName+" verifying header title.");
			return false;
		}//End of catch block
	}//End of <Method: Exports_BlankSearch>     	

	//###################################################################################################################################################################  
	//Function name		: Exports_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName)
	//Class name		: Exports_Objs
	//Description 		: Function to verify the row counts and the Export to Excel Link in Web page. 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public void Exports_CheckPoints(int exclRowCnt,String strSheetName){
		try{
			SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
			System.out.println("Inside Exports_CheckPoints : ");

			//Getting the row count from Dynamic search results table.
			int appRowsReturned = objSearchRes.get_WebTableRowCount(ele_Exports_ResTable_Body,1,exclRowCnt,strSheetName);
			//int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strTabName);
			System.out.println("appRowsReturned : " +appRowsReturned);
			
			if(!(appRowsReturned>0)){
				if(ele_NoResults.getText().equals("No Results Found")){
					System.out.println("Search Results for Import page is 'No Results Found'.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Exports search Results' No Results found.", "", "Exports_Res",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition
				System.out.println("Search Results for Import page is not >0");
				reporter.reportStep(exclRowCnt, "Failed", "'Exports search Results' rows not >0, Not as expected 'No results Found'", "", "Exports_Res",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of IF condition to check No of rows returned
			else{
				System.out.println("Search Results for Import page is >0 " +appRowsReturned);
				reporter.reportStep(exclRowCnt, "Passed", "'Imports search Results' rows >0 i.e "+appRowsReturned+" record(s) retrieved successfully", "", "Exports_Res",  driver, "BOTH_DIRECTIONS", strSheetName, null);

				if(!(CBMngr_Objs.VerifyExportToExcelLink(lnk_ExportToExcel, exclRowCnt, strSheetName, "CB-Manager Exports"))){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] --> Export to Excel Link is not available in CB-Imports Page. Please check !!..", "", "Exports_ExceLink",  driver, "HORIZONTALLY", strSheetName, null);
					System.out.println("Link not available.");
				}//End of IF condition to check for the Export Link

				//Verify the buttons in Exports page.
				this.VerifySelectReleaseButtonsInExportsPage(exclRowCnt, strSheetName);

				//Verifying the status of 1st Record in Exports table. # List starts from 0
				String firstRecordStatus = this.Exports_Res_getCellData(0, "Status");
				System.out.println("firstRecordStatus : "+firstRecordStatus);
				
				//Verify the Action Menu List based on statsu
				this.VerifyActionMenuListInExportsPage(exclRowCnt, strSheetName, firstRecordStatus);

			}//End of Else

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Exports_Objs.Exports_CheckPoints>: CBManger->Imports verifying header title.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <Exports_Objs.Exports_CheckPoints>,please check..!!", "", "Exports_Exception",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of catch block
	}//End of <Method: Exports_CheckPoints>   

	//###################################################################################################################################################################  
	//Function name		: VerifyActionMenuListInExportsPage(int exclRowCnt,String strSheetName, String status)
	//Class name		: Exports_Objs
	//Description 		: Function to verify the Select All Eligible Records and Release Selected Records button in Exports page. 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public void VerifyActionMenuListInExportsPage(int exclRowCnt,String strSheetName, String status){
		try
		{

			if(!this.ExportsRes_VerifyActionButtonExists(exclRowCnt, strSheetName, status)){
				System.out.println("<Method: VerifyActionMenuListInExportsPage >Exports Search_Results Table Action button is NOT displayed,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "<Method: VerifyActionMenuListInExportsPage >Exports Search_Results Table Action button is NOT displayed,please check..!! ", "", "Exports_Action",  driver, "HORIZONTALLY", strSheetName, null);
			}//End of IF condition to check if ActionButton Exists

			if(!this.Exports_verifyActionMenuList(exclRowCnt, strSheetName, status)){
				System.out.println("<Method: VerifyActionMenuListInExportsPage >Exports Search_Results Table Action Menu list is NOT displayed,please check..!!");
				reporter.reportStep(exclRowCnt, "Failed", "<Method: VerifyActionMenuListInExportsPage >Exports Search_Results Table Action Menu list is NOT displayed,please check..!!", "", "Exports_Action",  driver, "HORIZONTALLY", strSheetName, null);
			}//End of IF condition to check if Action menu List Exists


		}// End of Try block
		catch(Exception e)
		{
			System.err.println("Exception in <Exports_Objs.VerifyActionMenuListInExportsPage>:  Please check !!.");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exception in <Exports_Objs.VerifyActionMenuListInExportsPage>: Please check !!. ", "", "Exports_Action",  driver, "HORIZONTALLY", strSheetName, null);
		} //End of Catch
	}//End of <Method: VerifyActionMenuListInExportsPage>


	//###################################################################################################################################################################  
	//Function name		: VerifySelectReleaseButtons(int exclRowCnt,String strSheetName)
	//Class name		: Exports_Objs
	//Description 		: Function to verify the Select All Eligible Records and Release Selected Records button in Exports page. 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public void VerifySelectReleaseButtonsInExportsPage(int exclRowCnt,String strSheetName){
		try
		{

			//Check if the Export to Excel Link  is displayed 
			if((CommonUtils.isElementPresent(ele_SelectAllEligibleRecords_btn))){
				System.out.println("Button - Select All Eligible Records button is available in Exports page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Button - Select All Eligible Records button is available in Exports page.", "", "VerifySelectReleaseButtons",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("Button - Select All Eligible Records button is not available in Exports page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Button - Select All Eligible Records button is not available in Exports page. :", "", "Exports_Button",  driver, "HORIZONTALLY", strSheetName, null);
			}

			//Check if the Export to Excel Link  is displayed 
			if((CommonUtils.isElementPresent(ele_ReleaseSelectedRecords_btn))){
				System.out.println("Button - Release Selected Records button is available in Exports page.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Button - Release Selected Records button is available in Exports page.", "", "Exports_Button",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.err.println("Button - Release Selected Records button is not available in Exports page.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Release Selected Records button is not available in Exports page.", "", "Exports_Button",  driver, "HORIZONTALLY", strSheetName, null);
			}


		}// End of Try block
		catch(Exception e)
		{
			System.err.println("Exception in <Exports_Objs.VerifySelectReleaseButtons>:  Please check !!.");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exception in <Exports_Objs.VerifySelectReleaseButtons>: Please check !!. ", "", "Exports_Button",  driver, "HORIZONTALLY", strSheetName, null);
		} //End of Catch
	}//End of <Method: VerifySelectReleaseButtons>


	//###################################################################################################################################################################  
	//Function name			: Exports_Res_getCellData(int rowNumber,String colName)
	//Class name			: Exports_Objs
	//Description 			: Function to get cell data from 'Transaction Error' Search results based on Row# and column name
	//Parameters 			: rowNumber
	//						: Column Name	
	//Assumption			: None
	//Developer				: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public String Exports_Res_getCellData(int rowNumber,String colName){
		try{  			
			if(!CommonUtils.isElementPresent(ele_Exports_ResTable)){
				System.out.println("Transaction Error_Results Table is not displayed,please check..!!");
				return null;	
			}//End of IF Condition to check if ele_ResTable element exists

			int colIndex = this.ExportsRes_GetColIndexByColName(colName);
			List<WebElement> app_rows = ele_Exports_ResTable_Body.get(0).findElements(By.tagName("tr"));
			List<WebElement> app_cols = app_rows.get(rowNumber).findElements(By.tagName("td"));    			
			String strColValue = app_cols.get(colIndex).getAttribute("textContent");
			return strColValue;

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Exports_Objs.Exports_Res_getCellData>: Search Results Table is not displayed in UI..!!!");
			return "";
		}
	}// End of <Method: Exports_Res_getCellData>

	//###################################################################################################################################################################  
	//Function name				: ExportsRes_GetColIndexByColName(String strColName)
	//Class name				: Exports_Objs
	//Description 				: Function to read number of records retrieved from Transaction Error search result
	//Parameters 				: 
	//Assumption				: None
	//Developer					: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public int ExportsRes_GetColIndexByColName(String strColName){
		try{  			
			if(!(CommonUtils.isElementPresent(ele_Exports_ResTable) && (ele_Exports_ResTable_Header.size()>0))){
				System.out.println("Exports Page Search_Results Table Header is not displayed,please check..!!");
				return 0;	
			}//End of IF Condition to check if ele_ResTbl_Header element exists

			List<WebElement> getCols = ele_Exports_ResTable_Header.get(0).findElements(By.tagName("th"));

			int iColLoop = 0;
			for (WebElement eachCol : getCols) {  		
				if (eachCol.getAttribute("textContent").trim().equalsIgnoreCase(strColName)){ 					
					Integer colIndex = iColLoop;
					return colIndex;
				}//End of IF condition to check column name 
				iColLoop++;
			}//End of FOR loop to get Column Index

			System.out.println("<Exports_Objs><ExportsRes_GetColIndexByColName>: 'Exports' Search Results Table HEADER, Expected Column Name:"+strColName+" is not displayed in UI..!!!");
			return -1;
		}//End of Try block
		catch (NumberFormatException e) {
			System.out.println("NumberFormatException in <Exports_Objs><ExportsRes_GetColIndexByColName>: Column Index for column name: "+strColName+" is not Integer");
			return -1;
		}
		catch(Exception e){
			System.out.println("Exception in <Exports_Objs><ExportsRes_GetColIndexByColName>: Search Results Table HEADER is not displayed in UI..!!!");
			return -1;
		}  		

	}//End of <Method: ExportsRes_GetColIndexByColName> 	


	//###################################################################################################################################################################  
	//Function name		: ExportsRes_VerifyActionButtonExists(){
	//Class name		: Exports_Objs
	//Description 		: Function to verify if Action button exists
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################		  	  		  		
	public boolean ExportsRes_VerifyActionButtonExists(int exclRowCnt, String strSheetName, String status){
		try{   
			List<WebElement> ele_ActionButton;
			ele_ActionButton = this.ele_ExportsAction_Row1;

			if(ele_ActionButton.size()>0){
				if(!CommonUtils.isElementPresent(ele_ActionButton.get(0))){
					System.out.println("Checkpoint :[Failed] Exports Search_Results Table,Left Action button on row1 is NOT displayed,please check..!!");
					return false;
				}//End of if condition to check if Left Action button on row1 in UI is displayed.
				else System.out.println("Checkpoint :[Passed] Exports Search_Results Table,Left Action button on row1 is displayed.");

				ele_ActionButton.get(0).click();
				Thread.sleep(5000);
				if(!(ele_ExportsActionMenu.size()>0)){
					System.out.println("Checkpoint :[Failed] Exports Search_Results Table Action Menu List is NOT displayed,please check..!!");
					return false;						
				}//End of if condition to check if Action Menu List is displayed
				System.out.println("Checkpoint :[Passed] Exports Search_Results Table Action Menu List is displayed");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Exports Search_Results Table Action Menu List is displayed", "", "ExportsRes_VerifyActionButtonExists",  driver, "HORIZONTALLY", strSheetName, null);
				return true;
			}//End of if condition to check if Right Action button is displayed
			else{
				System.out.println("Checkpoint :[Failed] Exports Search_Results Table Action button is NOT displayed,please check..!!");
				return false;
			}//End of else condition to check  if Action Button on right side is displayed

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Class: Exports_Objs ><Method: ExportsRes_VerifyActionButtonExists>Exports Search_Results Table Action button on Left side is NOT displayed,please check..!!");
			return false;    		
		}//End of catch block

	}//End of <Method: ExportsRes_VerifyActionButtonExists>  


	//###################################################################################################################################################################  
	//Function name			: Exports_verifyActionMenuList(String status){
	//Class name			: Exports_Objs
	//Description 			: Function to verify if Action Menu exists and verify Action Menu list based on the status retrieved from Exports table.
	//Parameters 			: 
	//Assumption			: None
	//Developer				: Dhanasekar Kumar
	//###################################################################################################################################################################		  	  		  		
	public boolean Exports_verifyActionMenuList(int exclRowCnt, String strSheetName, String status){
		List<String> actActionList = new ArrayList<String>();
		ArrayList<String> expActionList = new ArrayList<String>();
		if(status.equalsIgnoreCase(EXPORTS_PAGE_STATUS_ACCEPTED) || status.equalsIgnoreCase(EXPORTS_PAGE_STATUS_REJECTED))
		{
		expActionList.addAll(Arrays.asList("RAD", "Ledger" , "Open/Closed", "Lot Detail"));
		}
		else
		{
		expActionList.addAll(Arrays.asList("RAD", "Ledger" , "Open/Closed"));
		}
		if(this.ele_ExportsActionMenu_Row1.size()>0){	
			List<WebElement> app_ActionList = this.ele_ExportsActionMenu_Row1.get(0).findElements(By.tagName("li"));
			for (WebElement eachActionList : app_ActionList) { 
				actActionList.add(eachActionList.getAttribute("textContent").trim());
			}//End of for loop to read actual Action menu list
			if(!actActionList.toString().equals(expActionList.toString())){
				System.out.println("Checkpoint :[Failed] Exports Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString()+"; Actual Action menu List = "+actActionList.toString());
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exports Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString()+"; Actual Action menu List = "+actActionList.toString() + " for Status parameter : " + status, "", "Exports_Action",  driver, "HORIZONTALLY", strSheetName, null);
				return false;
			}//End of IF condition to check Expected Action Menu List & Actual Action Menu List
			System.out.println("Checkpoint :[Passed] Exports Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString());
			reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Exports Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString() + " for Status parameter : " + status, "", "Exports_verifyActionMenuList",  driver, "HORIZONTALLY", strSheetName, null);
			return true;
		}//End of IF condition to check Menu List object exists

		System.out.println("<Method: Exports_verifyActionMenuList>Checkpoint_Fail:Action Menu list object in Exports search Results is NOT displayed, please check...!!!");
		reporter.reportStep(exclRowCnt, "Failed", "<Method: Exports_verifyActionMenuList>Checkpoint_Fail:Action Menu list object in Exports search Results is NOT displayed, please check...!!", "", "Exports_Action",  driver, "HORIZONTALLY", strSheetName, null);
		return false;
	}//End of <Method: Exports_verifyActionMenuList>    

}//End of <Class: Exports_Objs>
