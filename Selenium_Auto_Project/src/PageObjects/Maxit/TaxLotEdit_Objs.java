package PageObjects.Maxit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class TaxLotEdit_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
	//SearchRes_PopUpFns Res_PopUp_Objs = PageFactory.initElements(driver, SearchRes_PopUpFns.class);
	SearchRes_ActionMenu SearchRes_Action = PageFactory.initElements(driver, SearchRes_ActionMenu.class);
	SearchPage_Results Res_Objs = PageFactory.initElements(driver, SearchPage_Results.class);
	
    //TLE Objects
	@FindBy(how=How.XPATH,using="//*[@class=\"x-window-header-text\"]")
    public WebElement ele_TLETitle;
	@FindBy(how=How.NAME,using="cbeAcatCombo")
    public List<WebElement> drpdwn_TxnType;
	@FindBy(how=How.XPATH,using="//*[@class=\"cbeLabel\" and contains(text(),\"Transaction Type:\")]")
    public WebElement lbl_TrxnType;
	
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
    public List<WebElement> TLE_Header;
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-body\"]")
    public List<WebElement> TLE_ResTable;
	@FindBy(how=How.ID,using="UnknownStepup")
    public WebElement chckbx_UnknownStepUp;

	@FindBy(how=How.XPATH,using="//*[@class=\" errorPanel\" and contains(@style,\"visibility: visible\")]")
    public List<WebElement> TLE_Errors;
	
	@FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Start Over\")]")
    public WebElement btn_StartOver;
	@FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Save\")]")
    public WebElement btn_Save;
	
    //###################################################################################################################################################################  
    //Function name		: RawTrades_TLECheckpoints(int exclRowCnt,String strSheetName)
    //Class name		: SearchRes_CBMethod_Objs.java
    //Description 		: Function to verify the drop downs in Raw Trades->Details pop up
    //Parameters 		: Excel row number for report and sheet name.
    //					: Excel sheet name
    //Assumption		: RawTrades->Details pop up is displayed.
    //Developer			: Kavitha Golla
    //###################################################################################################################################################################		
      public void RawTrades_TLECheckpoints(int exclRowCnt,String strSheetName){		
    	  try{	
    		  //TLE Generic Checkpoints like Verify pop up eixsts, No error message,Txn Type drop down, StartOver/Save buttons and atleast one row of data displayed. 
    		  if(!(this.Generic_TLECheckpoints(exclRowCnt, strSheetName,"Raw Trades"))){
          		return; 
    		  }
    		  
    		//Checkpoint: Select GIFT from Trxn Type drop down and verify header for FMV/Share existence   
    		  this.TLE_SelectGift(exclRowCnt, strSheetName);

    		//Checkpoint: Select INHERITANCE from Trxn Type drop down and verify "Stepup % UnKnown" checkbox is displayed.
    		  this.TLE_SelectInh(exclRowCnt, strSheetName);
        	
        	if(CBM_Objs.btn_CBMXClose.size()>0){
        		CBM_Objs.btn_CBMXClose.get(0).click();
        	}
        	
    	}//End of try block
    	catch(Exception e){			
      		System.out.println("Exception in <Method: RawTrades_TLECheckpoints>Exception:"+e.getMessage()+",please check..!!");
    	}//End of catch block
      }//End of <Method: RawTrades_TLECheckpoints>  	
 
  //###################################################################################################################################################################  
  //Function name		: Generic_TLECheckpoints(int exclRowCnt,String strSheetName,String strSearchPageName)
  //Class name			: SearchRes_CBMethod_Objs.java
  //Description 		: Function to verify the drop downs in Raw Trades->Details pop up
  //Parameters 			: Excel row number for report and sheet name.
  //					: Excel sheet name
  //Assumption			: RawTrades->Details pop up is displayed.
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public boolean Generic_TLECheckpoints(int exclRowCnt,String strSheetName,String strSearchPageName){		
  	  try{	
  		  // Verify for no error messages:
  		  if(!(this.TLE_CheckErrors(exclRowCnt,strSheetName))){
  			  return false;
  		  }  		  
  		//Transaction Type drop down verification
  		if(drpdwn_TxnType.size()>0) 	this.drpdwn_TxnType.get(0).click();
  		else {
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Transaction type dropdown is NOT displayed", "", "TLE_TrxType",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] TLE->Transaction type dropdown is NOT displayed,so not validating dropdown,please check..!!");
      		return false;
  		}
  		
  		//Checkpoint: Verify Transaction Type dropdown values
  		if(strSearchPageName.equalsIgnoreCase("Avg Cost Gift")){
  	  		if(!compare_TLE_TrnxType("AVGCOSTGIFT_TRNXTYPE","AVGCOSTGIFT_TRNXTYPE",exclRowCnt,strSheetName)){
  	      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Transaction type dropdown is NOT as expected", "", "TLE_TrxType",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  	      		System.out.println("Checkpoint :[Failed] TLE->Transaction type dropdown is NOT as expected,please check..!!");
  	      		return false;
  	  		} 
  		}//End of IF condition to check search page name= AVgCostGift
  		else{
	  		if(!compare_TLE_TrnxType("TLE_TRNXTYPE","TRNXTYPE",exclRowCnt,strSheetName)){
	      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Transaction type dropdown is NOT as expected", "", "TLE_TrxType",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	      		System.out.println("Checkpoint :[Failed] TLE->Transaction type dropdown is NOT as expected,please check..!!");
	      		return false;
	  		}
  		}//End of Else condition to check search page name= AVgCostGift
  		
  		lbl_TrxnType.click();
	  //Verify atleast one row of data is displayed:  
		if(strSearchPageName.equalsIgnoreCase("Raw Trades")){
			  if(!(Res_Objs.get_WebTableRowCount(TLE_ResTable,2,exclRowCnt,strSheetName)>0)){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Atleast one row of data is NOT displayed in TLE pop up table", "", "TLE_Data",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] TLE->Atleast one row of data is NOT displayed in TLE pop up table,please check..!!");
					return false; 
				  }
			  else{
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] TLE->Atleast one row of data is displayed in TLE pop up table", "", "TLE_Data",  driver, "HORIZONTALLY", strSheetName, null);
					System.out.println("Checkpoint :[Passed] TLE->Atleast one row of data is displayed in TLE pop up table,please check..!!");					
			  }
		}
		if((strSearchPageName.equalsIgnoreCase("No Cost Basis"))||(strSearchPageName.equalsIgnoreCase("Average Cost Gift"))){
			  if(!(Res_Objs.get_WebTableRowCount(TLE_ResTable,1,exclRowCnt,strSheetName)>0)){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Atleast one row of data is NOT displayed in TLE pop up table", "", "TLE_Data",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Failed] TLE->Atleast one row of data is NOT displayed in TLE pop up table,please check..!!");
					return false; 
				  }
			  else{
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] TLE->Atleast one row of data is displayed in TLE pop up table", "", "TLE_Data",  driver, "HORIZONTALLY", strSheetName, null);
					System.out.println("Checkpoint :[Passed] TLE->Atleast one row of data is displayed in TLE pop up table,please check..!!");					
			  }
		}
		
    	//Verify Start Over & Save Exists
	  return this.TLE_StartOverCancel(exclRowCnt,strSheetName,"TaxLotEdit");
  	}//End of try block
  	catch(Exception e){			
    		System.out.println("Exception in <Method: Generic_TLECheckpoints>Exception:"+e.getMessage()+",please check..!!");
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exception in <Method: Generic_TLECheckpoints>Exception:"+e.getMessage()+",please check..!!", "", "TLE_Exception",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		return false;
  	}//End of catch block
    }//End of <Method: Generic_TLECheckpoints>  
      
//###################################################################################################################################################################  
//Function name			: TLE_SelectGift(int exclRowCnt,String strSheetName,String strTrxnType)
//Class name			: TaxLotEdit_Objs.java
//Description 			: Function to select GIFT from Transaction Type drop down in TLE and verify Smoke Checkpoints
//Parameters 			: Excel row number for report and sheet name.
//Assumption			: None
//Developer				: Kavitha Golla
//###################################################################################################################################################################		
  public void TLE_SelectGift(int exclRowCnt,String strSheetName){		
	String strActTLEHeader;
	  try{			
		//Checkpoint: Select GIFT from Trxn Type drop down and verify header for FMV/Share existence   
		  
		if(drpdwn_TxnType.size()>0) 	this.drpdwn_TxnType.get(0).click();
		else{
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Transaction Type drop down is not displayed to Select 'Gift' for more TLE_>Smoke Checkpoints,please check.!", "", "TLE_GiftHeader",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] TLE->Transaction Type drop down is not displayed to Select 'Gift' for more TLE_>Smoke Checkpoints,please check.!");
    		return ;
		}
    	if(SearchRes_Action.selectValue_DropDown("TLE_TRNXTYPE", "TRNXTYPE", exclRowCnt, strSheetName, "Gift")){
    		//Verify tle header
    		if(TLE_Header.size()>0){
    			strActTLEHeader = Res_Objs.get_TableHeader(TLE_Header,1,exclRowCnt,strSheetName);
    			if(!(strActTLEHeader.toLowerCase().contains("fair market value")) && (strActTLEHeader.toLowerCase().contains("fmv/share"))){
            		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Gift Header does not contain [Fair Market Value] or [FMV/Share], Actual TLE Gift Header:["+strActTLEHeader+"]", "", "TLE_GiftHeader",  driver, "BOTH_DIRECTIONS", strSheetName, null);
            		System.out.println("Checkpoint :[Failed] TLE->Gift Header does not contain [Fair Market Value] or [FMV/Share].Actual TLE Gift Header:["+strActTLEHeader+"],please check..!!");
            		return ;
    			}//End of IF condition to check if FMV is displayed in TLE Pop up header for GIFT transaction type
    			else{
            		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] TLE->Gift Header contain's [Fair Market Value] or [FMV/Share], Actual TLE Gift Header:["+strActTLEHeader+"]", "", "TLE_GiftHeader",  driver, "BOTH_DIRECTIONS", strSheetName, null);
            		return;
    			}//End of Else condition to check if FMV is displayed in TLE Pop up header for GIFT transaction type
    		}//End of IF condition to check header is displayed or not        	        	
    		else{
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Gift Header is not displayed,please check.!!", "", "TLE_GiftHeader",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		}//End of ELSE condition to check header is displayed or not     
    		this.lbl_TrxnType.click();
    	}//End of IF condition to select GIFT from dropdown
	}//End of try block
	catch(Exception e){			
  		System.out.println("Exception in <Method: TLE_SelectGift>,please check..!!");
  		return;
	}//End of catch block
  }//End of <Method: TLE_SelectGift>        
    
  
//###################################################################################################################################################################  
//Function name			: TLE_SelectInh(int exclRowCnt,String strSheetName,String strTrxnType)
//Class name			: TaxLotEdit_Objs.java
//Description 			: Function to select INHERITANCE from Transaction Type drop down in TLE and verify Smoke Checkpoints
//Parameters 			: Excel row number for report and sheet name.
//Assumption			: None
//Developer				: Kavitha Golla
//###################################################################################################################################################################		
  public void TLE_SelectInh(int exclRowCnt,String strSheetName){	
	  String strActTLEHeader;
	  try{			
		//Checkpoint: Select INHERITANCE from Trxn Type drop down and verify "Stepup % UnKnown" checkbox is displayed.		  
		if(drpdwn_TxnType.size()>0) 	this.drpdwn_TxnType.get(0).click();
		else{
    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Transaction Type drop down is not displayed to Select 'Gift' for more TLE_>Smoke Checkpoints,please check.!", "", "TLE_GiftHeader",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    		System.out.println("Checkpoint :[Failed] TLE->Transaction Type drop down is not displayed to Select 'Gift' for more TLE_>Smoke Checkpoints,please check.!");
    		return ;
		}
    	if(SearchRes_Action.selectValue_DropDown("TLE_TRNXTYPE", "TRNXTYPE", exclRowCnt, strSheetName, "Inheritance")){
    		//Verify tle header
    		if(TLE_Header.size()>0){

    			//Checkpoint to verify "Step %" in Inh_TLE Header
    			strActTLEHeader = Res_Objs.get_TableHeader(TLE_Header,1,exclRowCnt,strSheetName);
    			if(!(strActTLEHeader.toLowerCase().contains("step %"))){
            		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Inh Header does not contain [Step %] , Actual TLE Inh Header:["+strActTLEHeader+"]", "", "TLE_InhHeader",  driver, "HORIZONTALLY", strSheetName, null);
            		System.out.println("Checkpoint :[Failed] TLE->Inh Header does not contain [Step %].Actual TLE Inh Header:["+strActTLEHeader+"],please check..!!");
    			}//End of IF condition to check if Step % is displayed in TLE Pop up header for INH transaction type
    			else{
            		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] TLE->Inh Header contain's [Step %] , Actual TLE Inh Header:["+strActTLEHeader+"]", "", "TLE_InhHeader",  driver, "HORIZONTALLY", strSheetName, null);
    			}//End of Else condition to check if Step % is displayed in TLE Pop up header for INH transaction type
    			
    			/*
    			Commented the below code to stop looking for Unknown steup checkbox in smoke test script. Refer AUTO-469, for more information.
    			
    			//Check point to verify "Stepup % UnKnown" checkbox is displayed
    			if(chckbx_UnknownStepUp.isDisplayed()) reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] TLE->Inheritance -> 'Stepup % UnKnown' checkbox is displayed.", "", "TLE_stepPer",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    			else reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Inheritance -> 'Stepup % UnKnown' checkbox is NOT displayed,please check.!", "", "TLE_stepPer",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    	
    			*/    			
    		}
	        else {
    			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Header is not displayed,please check.!!", "", "TLE_Header",  driver, "HORIZONTALLY", strSheetName, null);
    		}//End of ELSE condition to check header is displayed or not
    		this.lbl_TrxnType.click();
    	}//End of IF condition to select INHERITANCE from dropdown
	}//End of try block
	catch(Exception e){			
  		System.out.println("Exception in <Method: TLE_SelectInh>,please check..!!");
  		return;
	}//End of catch block
  }//End of <Method: TLE_SelectGift>     
  //###################################################################################################################################################################  
  //Function name		: TLE_StartOverCancel(int exclRowCnt,String strSheetName,String strPopUpName)
  //Class name			: SearchRes_ActionMenu.java
  //Description 		: Function to verify Save & Close buttonsin pop up
  //Parameters 			: Excel row number for report and sheet name.
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public boolean TLE_StartOverCancel(int exclRowCnt,String strSheetName,String strPopUpName){		
		try{			
	    	if(!CommonUtils.isElementPresent(btn_Save)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed", "", "Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->SAVE button is NOT displayed,please check..!!");
	    		return false;
			}//End of if condition to check if SAVE button is displayed
	    	else{
	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->SAVE button is displayed", "", "Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    	}
	    		    
	    	if(!CommonUtils.isElementPresent(btn_StartOver)){
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] "+strPopUpName+"->Start Over button is NOT displayed", "", "CBMethod_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] "+strPopUpName+"->Start Over button is NOT displayed,please check..!!");
	    		return false;
			}//End of if condition to check if STARTOVER button is displayed
	    	else {
	    		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] "+strPopUpName+"->STARTOVER button is displayed", "", "STARTOVER",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    	}
	    	return true;
		}//End of try block
		catch(Exception e){			
    		System.out.println("Exception in <Method: verifyPopUp_SaveCancel>,please check..!!");
    		return false;
		}//End of catch block
    }//End of <Method: TLE_StartOverCancel>         
      
      //###################################################################################################################################################################  
      //Function name		: getExp_TLE_TrnxType(String strDropDown)
      //Class name			: SearchRes_CBMethod_Objs
      //Description 		: Function to get RawTrades->TLE pop up expected drop down values for Transaction Type
      //Parameters 			: Drop down name to be verified from the pop up
      //Assumption			: None
      //Developer			: Kavitha Golla
      //###################################################################################################################################################################		
        public ArrayList<String> getExp_TLE_TrnxType(String strDropDown){		
      		ArrayList<String> arrExpDropDown = new ArrayList<String>();
      		switch(strDropDown.toUpperCase()){
      			case "TRNXTYPE":
      				arrExpDropDown.addAll(Arrays.asList("Gift","Inheritance")); 
          			return arrExpDropDown;
      			case "AVGCOSTGIFT_TRNXTYPE":
      				arrExpDropDown.addAll(Arrays.asList("Gift","Inheritance","Average Cost For Gift")); 
          			return arrExpDropDown;          			
      			default:
          			return null;
      		}//End of Switch case
        
        }//End of <Method: getExp_TLE_TrnxType> 
        
    //###################################################################################################################################################################  
    //Function name			: compare_TLE_TrnxType(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName)
    //Class name			: SearchRes_CBMethod_Objs
    //Description 			: Function to verify the RawTrades->TLE pop up Transaction Type drop down values 
    //Parameters 			: Drop down name to be verified from the pop up
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
      public boolean compare_TLE_TrnxType(String strPopUpName,String strDropDown,int exclRowCnt,String strSheetName){
    	  ArrayList<String> arrAct_Values = new ArrayList<String>();
    	  ArrayList<String> arrExp_Values = new ArrayList<String>();
  		if(strDropDown.equalsIgnoreCase("AVGCOSTGIFT_TRNXTYPE")){
      	  arrAct_Values = SearchRes_Action.getAct_DropDown_Values("AVGCOSTGIFT_TRNXTYPE","AVGCOSTGIFT_TRNXTYPE",exclRowCnt,strSheetName);
      	  arrExp_Values = SearchRes_Action.getExp_DropDown_Values("AVGCOSTGIFT_TRNXTYPE","AVGCOSTGIFT_TRNXTYPE",exclRowCnt,strSheetName);
  		}
  		else{
    	  arrAct_Values = SearchRes_Action.getAct_DropDown_Values("TLE_TRNXTYPE","TRNXTYPE",exclRowCnt,strSheetName);
    	  arrExp_Values = SearchRes_Action.getExp_DropDown_Values("TLE_TRNXTYPE","TRNXTYPE",exclRowCnt,strSheetName);
  		}
  		if((arrAct_Values.isEmpty() || arrAct_Values==null) || (arrExp_Values.isEmpty() || arrExp_Values==null)){
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Transaction type dropdown values is not as expected.Actual:["+arrAct_Values+"]and Expected:["+arrExp_Values+"]", "", "TrxType_compare",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] TLE->Transaction type dropdown values is not as expected.Actual:["+arrAct_Values+"]and Expected:["+arrExp_Values+"],please check..!!");
      		return false;
  		}
  		if(arrAct_Values.containsAll(arrExp_Values)){
      		reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] TLE->Transaction type dropdown values is as expected.Actual:["+arrAct_Values+"]and Expected:["+arrExp_Values+"]", "", "TrxType_compare",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Passed] TLE->Transaction type dropdown values is as expected.Actual:["+arrAct_Values+"]and Expected:["+arrExp_Values+"]");
      		return true;
  		}//Compare TLE Transaction Type drop down values.
  		else{
      		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE->Transaction type dropdown values is not as expected.Actual:["+arrAct_Values+"]and Expected:["+arrExp_Values+"]", "", "TrxType_compare",  driver, "BOTH_DIRECTIONS", strSheetName, null);
      		System.out.println("Checkpoint :[Failed] TLE->Transaction type dropdown values is not as expected.Actual:["+arrAct_Values+"]and Expected:["+arrExp_Values+"],please check..!!");
      		return false;
  		}
      
      }//End of <Method: compare_TLE_TrnxType> 
        
  //###################################################################################################################################################################  
  //Function name		: TLE_CheckErrors(int exclRowCnt,String strSheetName)
  //Class name			: TaxLotEdit_Objs.java
  //Description 		: Function to verify if there are any error messages in TLE pop up
  //Parameters 			: Excel row number for report and sheet name.
  //Assumption			: None
  //Developer			: Kavitha Golla
  //###################################################################################################################################################################		
    public boolean TLE_CheckErrors(int exclRowCnt,String strSheetName){
    	String strTLEError;
		try{			
			if(TLE_Errors.size()>0){
				strTLEError = TLE_Errors.get(0).getAttribute("textContent");
				System.out.println("textContent :"+TLE_Errors.get(0).getAttribute("textContent"));
				//System.out.println("getText :"+TLE_Errors.get(0).getText());
	    		reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] TLE Error message dispalyed. Error ["+strTLEError+"]", "", "CBMethod_Save",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	    		System.out.println("Checkpoint :[Failed] TLE Error message dispalyed. Error ["+strTLEError+"],please check..!!");
	        	//Close the pop up
	    		if(CBM_Objs.btn_CBMXClose.size()>0){
	        		CBM_Objs.btn_CBMXClose.get(0).click();
	        	}	    		
	    		return false;
			}
			return true;
			
		}//End of try block
		catch(Exception e){			
    		System.out.println("Exception in <Method: TLE_CheckErrors>,please check..!!");
    		return false;
		}//End of catch block
    }//End of <Method: TLE_CheckErrors>       
      
      
    
    
}//End of <Class: TaxLotEdit_Objs>
