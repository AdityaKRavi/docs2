package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.Maxit.SearchRes_PopUpFns;

public class AvgGiftCost_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	
	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	
    //Hist Correction:
	@FindBy(how=How.XPATH,using="//a[contains(@href, 'AvgCostGft/enter.php')]")
    public WebElement lnk_AvgGiftCost;
    @FindBy(how=How.ID,using="ageDays")
    public WebElement ele_Age;
    @FindBy(how=How.ID,using="dateFrom")
    public WebElement ele_FromDate;
    @FindBy(how=How.ID,using="dateTo")
    public WebElement ele_ToDate;
    
	
    @FindBy(how=How.XPATH,using="//*[@class=\"mediumButton\"]")
    public WebElement btn_Submit;
    @FindBy(how=How.ID,using="cbiAvgGftTable")
    public WebElement ele_ResTable;
    @FindBy(how=How.XPATH,using="//*[@id=\"cbiAvgGftTable\"]/thead")
    public List<WebElement> ele_ResTbl_Header;
    @FindBy(how=How.XPATH,using="//*[@id=\"cbiAvgGftTable\"]/tbody")
    public List<WebElement> ele_ResTbl_Body;
    @FindBy(how=How.XPATH,using="//*[@id=\"noData\"]")
    public WebElement ele_NoResults;
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverTarget_recon\")]")
    public List<WebElement> ele_AvgGiftAction;
	
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverTarget_recon0\")]")
    public List<WebElement> ele_AvgGitAction_Row1;
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverMenu_recon\")]")
    public List<WebElement> ele_AvgGitActionMenu;
    @FindBy(how=How.XPATH,using="//*[contains(@id,\"rolloverMenu_recon0\")]")
    public List<WebElement> ele_AvgGitActionMenu_Row1;
	
    
    //*********************************************************************************************************************  
    //Performing Actions on objects in Avg Gift Cost Page:
   //*********************************************************************************************************************   
//    //###################################################################################################################################################################  
//    //Function name		: Navigate_AvgGift(int exclRowCnt,String strSheetName)
//    //Class name		: AvgGiftCost_Objs
//    //Description 		: Function to Navigate to "Avg Gift Cost" Page
//    //Parameters 		: 
//    //Assumption		: None
//    //Developer			: Kavitha Golla
//    //###################################################################################################################################################################		
//    	public boolean Navigate_AvgGift(int exclRowCnt,String strSheetName){
//    		//CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
//    		try{  			
//    			if(CommonUtils.isElementPresent(lnk_AvgGiftCost)){
//    				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Navigating to 'Avg Gift Cost' page", "", "AvgGift_Page",  driver, "HORIZONTALLY", strSheetName, null);
//    				lnk_AvgGiftCost.click();
//    				
//    				//Verify page heading header:
//    				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_PageHeader)){
//    					String strAct_Header = CBMngr_Objs.ele_PageHeader.getAttribute("textContent");
//    					if(strAct_Header.contains("Average Cost For Gift")){
//    						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Avg Gift Cost page header/title is as expected, Expected Header:Average Cost For Gift", "", "AvgGift_Header",  driver, "HORIZONTALLY", strSheetName, null);
//    						return true;
//    					}
//    					else{
//    						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Average Cost Gift page header/title is not as expected, Actual header:"+strAct_Header+" and Expected Header:Average Cost For Gift", "", "AvgGift_Header",  driver, "HORIZONTALLY", strSheetName, null);
//    						return false;
//    					}
//    				}//End of if condition to verify page heading
//    				else {
//    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Average Cost Gift page header/title object is not displayed, please check..!", "", "AvgGift_Header",  driver, "HORIZONTALLY", strSheetName, null);
//    					return false;
//    				}
//    			}//End of IF condition to check if Average Cost Gift link exists in left navigation panel.
//    			else{
//					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Average Cost Gift link is not displayed in left navigation panel of page, please check..!", "", "AvgGift_Link",  driver, "HORIZONTALLY", strSheetName, null);
//					return false;
//    			}
//    					
//    		}//End of Try block
//    		catch(Exception e){
//    			System.out.println("Exception in <Navigate_AvgGift>: Average Cost Gift Link is not displayed in UI..!!!");
//    			reporter.reportStep(exclRowCnt, "Failed", "AvgGift_Exception in <Navigate_AvgGift> Please Check..!!", "", "AvgGift_Exception",  driver, "HORIZONTALLY", strSheetName, null);
//    			return false;
//    		}
//    	}//End of Navigate_AvgGift Method	    
    
    	
        //###################################################################################################################################################################  
        //Function name		: AvgGiftCost_BlankSearch(int exclRowCnt,String strSheetName)
        //Class name		: AvgGiftCost_Objs
        //Description 		: Function to perform BLANK search in AvgGiftCost page 
        //Parameters 		: None
        //Assumption		: None
        //Developer			: Kavitha Golla
        //###################################################################################################################################################################
        	public boolean AvgGiftCost_BlankSearch(String strTabName,int exclRowCnt,String strSheetName){
        		//CBManager_Objs CBManager_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
        		LocalDateTime strStartWaitTime,strEndWaitTime;
        		try{
        			btn_Submit.click();
        			
        			//Checkpoint: Check for Errors:
        			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;
        			
        			//Dynamic wait for search results to display with a max threshold of 35sec [ AUTO-396]
        			strStartWaitTime = LocalDateTime.now();
        			try{
        				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
        				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
        										wait.withTimeout(35,TimeUnit.SECONDS)
        										.pollingEvery(2, TimeUnit.SECONDS)				 
        										.ignoring(NoSuchElementException.class)
        										.until(ExpectedConditions.visibilityOf(ele_ResTable));
        			}//End of try block for FluentWait
        			
        			catch(org.openqa.selenium.TimeoutException e){
        				strEndWaitTime = LocalDateTime.now();
        				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
        				
        				//Check if page is loading:			
        				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
        					System.out.println("Checkpoint : Avg Gift Cost Blank Search - Stil Loading..[Failed]");
        					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Avg Gift Cost Blank Search :Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
        					return false;
        				}//End of IF condition to check for loading object.

            			//Checkpoint: Check for Errors:
            			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;				
        			}//End of Catch block for FluentWait
        			catch(Exception e){
        				strEndWaitTime = LocalDateTime.now();
        				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
        				return false;
        			}//End of catch block with generic exception for Fluent Wait 
        			
        			
        			//Check for Results header
        			if(CommonUtils.isElementPresent(ele_ResTable)){
        				strEndWaitTime = LocalDateTime.now();
        				System.out.println("Checkpoint : AvgGiftCost Page Navigation - [Passed]");
        				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
        				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] AvgGiftCost Blank Search Passed", "", "AvgGift_AfterSearch",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        				//System.out.println(LocalDateTime.now());
        				return true;
        			}//End of IF condition to check for Results table header.

        			System.out.println("Checkpoint : AvgGiftCost Search After Input data insert - [Failed]");
        			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] AvgGiftCost Blank Search :Failed, Results header is not displayed", "", "AvgGiftSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
        			return false;
        			
        			
        		}//End of Try block
        		catch(Exception e){
        			System.out.println("Exception in <AvgGiftCost_BlankSearch>: AvgGiftCost-> verifying header title.");
        			return false;
        		}//End of catch block
        	}//End of <Method: AvgGiftCost_BlankSearch>     	
    
        //###################################################################################################################################################################  
        //Function name		: AvgGiftCost_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName)
        //Class name		: HistCorrections_Objs
        //Description 		: Function to perform search based on input data under given CB Manager tab 
        //Parameters 		: None
        //Assumption		: None
        //Developer			: Kavitha Golla
        //###################################################################################################################################################################
        	public boolean AvgGiftCost_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName){
        		LocalDateTime strStartWaitTime,strEndWaitTime;
        		try{
        			boolean blnSecTypeCheck = false;
        			CBMngr_Objs.txtbx_Acct.sendKeys(strAcct);
        			List<WebElement> SecType_List = CBMngr_Objs.lst_SecIDType.findElements(By.tagName("option"));
        			for (WebElement option : SecType_List) {
        				if(option.getText().equals(strSecType)){
        					option.click();
        					blnSecTypeCheck=true;
        					break;
        				}
        			}//End of FOR loop
        			if(!blnSecTypeCheck){
        				for (WebElement option : SecType_List) {
        					if(option.getText().equals("Security No")){
        						System.out.println("Selecting default SecIDType as Security No");
        						option.click();
        						blnSecTypeCheck=true;
        						break;
        					}
        				}//End of FOR loop
        			}//End of IF condition to select default SecIDTpe= Security ID

        			CBMngr_Objs.txtbx_SecurityID.sendKeys(strSecValue);
        			//Enter Age=ALL:
        			List<WebElement> Age_List = ele_Age.findElements(By.tagName("option"));
        			for (WebElement option : Age_List) {
        				if(option.getText().equals("All")){
        					option.click();
        					blnSecTypeCheck=true;
        					break;
        				}
        			}//End of FOR loop

        			ele_FromDate.sendKeys(strFromDate);
        			ele_ToDate.sendKeys(strToDate);
        			
        			CBMngr_Objs.btn_Submit.click();
        			
        			//Checkpoint: Check for Errors:
        			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;
        				    			
        			//Dynamic wait for search results to display with a max threshold of 60Sec for AvgGiftCost page
        			strStartWaitTime = LocalDateTime.now();
        			try{
        				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
        				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
        										wait.withTimeout(60,TimeUnit.SECONDS)
        										.pollingEvery(2, TimeUnit.SECONDS)				 
        										.ignoring(NoSuchElementException.class)
        										.until(ExpectedConditions.visibilityOf(ele_ResTable));
        			}//End of try block for FluentWait
        			
        			catch(org.openqa.selenium.TimeoutException e){
        				strEndWaitTime = LocalDateTime.now();
        				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
        				
        				//Check if page is loading:			
        				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
        					System.out.println("Checkpoint : Avg Gift Cost Blank Search - Stil Loading..[Failed]");
        					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Avg Gift Cost Blank Search :Failed,still Loading[Waited for search results for 30Sec]", "", "SearchAfter_Input",  driver, "HORIZONTALLY", strSheetName, null);
        					return false;
        				}//End of IF condition to check for loading object.

            			//Checkpoint: Check for Errors:
            			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;				
        			}//End of Catch block for FluentWait
        			catch(Exception e){
        				strEndWaitTime = LocalDateTime.now();
        				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
        				return false;
        			}//End of catch block with generic exception for Fluent Wait 
        			
        			
        			//Check for Results header
        			if(CommonUtils.isElementPresent(ele_ResTable)){
        				strEndWaitTime = LocalDateTime.now();
        				System.out.println("Checkpoint : AvgGiftCost Page Navigation - [Passed]");
        				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
        				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] AvgGiftCost Blank Search Passed", "", "AvgGift_AfterSearch",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        				//System.out.println(LocalDateTime.now());
        				return true;
        			}//End of IF condition to check for Results table header.

        			System.out.println("Checkpoint : AvgGiftCost Search After Input data insert - [Failed]");
        			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] AvgGiftCost Blank Search :Failed, Results header is not displayed", "", "AvgGiftSearch_Input",  driver, "HORIZONTALLY", strSheetName, null);
        			return false;
        			
        		}//End of Try block
        		catch(Exception e){
        			System.out.println("Exception in <AvgGiftCost_Search>: CBManger->"+strTabName+" verifying header title.");
        			return false;
        		}//End of catch block
        	}//End of <Method: AvgGiftCost_Search>         	
        	
        //###################################################################################################################################################################  
        //Function name		: AvgGiftCost_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName)
        //Class name		: AvgGiftCost_Objs
        //Description 		: Function to smoke checkpoints for Av Gift Cost page
        //Parameters 		: 
        //Assumption		: None
        //Developer			: Kavitha Golla
        //###################################################################################################################################################################
        	public void AvgGiftCost_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName,String strClient){
          	  SearchRes_PopUpFns objRes_Action = PageFactory.initElements(driver, SearchRes_PopUpFns.class);
        	  SearchRes_CBMethod_Objs CBM_Objs = PageFactory.initElements(driver, SearchRes_CBMethod_Objs.class);
        		try{
        			
    				//Verify search results row number, if No Results returned fail the case.		
    				int appRowsReturned = this.get_AvgGift_ResRowCount(this.ele_ResTbl_Body, 0, exclRowCnt, strSheetName);
    				if(!(appRowsReturned>0)){
    					if(this.AvgGift_NoResultsReturned().equals("No Results Found")){
    						System.out.println("Search Results returned: 'No Results Found'.");
    						reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Warning] Avg Gift Cost Search returned: 'No Resuls found',so TLE Verification is skipped.!", "", "AvgGiftSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
    						return;
    					}
    					System.out.println("Avg Gift Cost Search Results rows is not >0");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Avg Gift Cost Search Results rows not >0, Not as expected 'No results Found'", "", "AvgGiftSearchResults_Rows",  driver, "HORIZONTALLY", strSheetName, null);
    					return;
    				}//End of IF condition to check No of rows returned
        			
    				//Navigate to TLE and perform TLE Generic checkpoints
    				objRes_Action.Navigate_TLE(exclRowCnt, strSheetName, 1, "Tax Lot Edit", "", "Avg Cost Gift", "",strClient);
    				//To ensure the pop up is closed.
    		      	if(CBM_Objs.btn_CBMXClose.size()>0){
    		      		CBM_Objs.btn_CBMXClose.get(0).click();
    		      	}
    				
        			
        			
        		}//End of Try block
        		catch(Exception e){
        			System.out.println("Exception in <AvgGiftCost_CheckPoints>: CBManger->AvgGiftCost verifying header title.");
        			reporter.reportStep(exclRowCnt, "Failed", "Exception in <AvgGiftCost_CheckPoints>,please check..!!", "", "AvgGiftCost_Exception",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        			return;
        		}//End of catch block
        	}//End of <Method: AvgGiftCost_CheckPoints>          	
    
        	
	  //###################################################################################################################################################################  
	  //Function name		: get_AvgGift_ResRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
	  //Class name			: AvgGiftCost_Objs
	  //Description 		: Function to total number of rows in the given webtable
	  //Parameters 			: 
	  //Assumption			: None
	  //Developer			: Kavitha Golla
	  //###################################################################################################################################################################		
	  	public int get_AvgGift_ResRowCount(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName){
	  		try{  	
	  			if(eleTableName.isEmpty()){
	  				System.out.println("<Method: get_AvgGift_ResRowCount>: eleTableName parameter passed is empty, please check..!!");
	  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] eleTableName parameter passed is empty,please check method<get_AvgGift_ResRowCount>!", "", "WebTableRowCount",  driver, "HORIZONTALLY", strSheetName, null);
	  				return 0;
	  			}
	  			if(!((eleTableName.size()>0) && (eleTableName.size()>=tbl_index)) ){
	  				System.out.println("<Method: get_AvgGift_ResRowCount>: Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index);
	  				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Web Table object list size in the browser is "+eleTableName.size()+" and given index to read the row count is "+tbl_index+",please check method<get_AvgGift_ResRowCount>!", "", "WebTableRowCount",  driver, "BOTH_DIRECTIONS", strSheetName, null);
	  				return 0;
	  			}//End of IF Condition to check if eleTableName element exists
	  			
	  			List<WebElement> app_rows = eleTableName.get(tbl_index).findElements(By.tagName("tr"));
	  			try{
	  				if(ele_NoResults.isDisplayed()) return 0;
	  			}
	  			catch(org.openqa.selenium.NoSuchElementException e){
	  				return app_rows.size(); 
	  			}
	  			return app_rows.size();  			
	  			
	  		}//End of Try block
	  		catch(Exception e){
	  			System.out.println("Exception in <get_AvgGift_ResRowCount>,please check.!!");
	  			e.printStackTrace();
	  			return 0;
	  		}
	  	}//End of get_AvgGift_ResRowCount Method

	    //###################################################################################################################################################################  
	    //Function name			: AvgGift_NoResultsReturned(List<WebElement> eleTableName,int tbl_index,int exclRowCnt,String strSheetName)
	    //Class name			: NoCostBasis_Objs
	    //Description 			: Function to total number of rows in the given webtable
	    //Parameters 			: Search Page name
	    //Assumption			: None
	    //Developer				: Kavitha Golla
	    //###################################################################################################################################################################		
	  	public String AvgGift_NoResultsReturned(){
	  		try{  	
	  			String strResValue;  				
	  			if(CommonUtils.isElementPresent(ele_NoResults)){
	  				strResValue = ele_NoResults.getAttribute("textContent");
	  				if(strResValue.contains("No Results Found")) strResValue ="No Results Found";
	  				return 	strResValue;
	  			}//End of IF Condition to check if ele_ResultsTable element exists
	  			System.out.println("<Method: AvgGift_NoResultsReturned>: No Results found checkpoint fail.");
	  			strResValue = "No Results found checkpoint fail";
	  			return strResValue;  			
	  			
	  		}//End of Try block
	  		catch(Exception e){
	  			System.out.println("Exception in <AvgGift_NoResultsReturned>: Avg Gift Cost Search Results Table 'No Results Found' is not displayed in UI..!!!");
	  			e.printStackTrace();
	  			return null;
	  		}
	  	}//End of AvgGift_NoResultsReturned Method        	
        	
    //###################################################################################################################################################################  
    //Function name			: AvgGiftRes_ClickAction(String strDesiredPageLink)
    //Class name			: AvgGiftCost_Objs
    //Description 			: Function to click on the action menu item
    //Parameters 			: Desired action menu item & which side of the action button to be clicked
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		
    		public boolean AvgGiftRes_ClickAction(String strDesiredPageLink){
    			try{
    				if(!this.AvgGiftRes_VerifyActionButtonExists()){
    					System.out.println("<Method: AvgGiftRes_ClickAction >AvgGiftCost Search_Results Table Action button is NOT displayed,please check..!!");
    					return false;
    				}//End of IF condition to check if ActionButton Exists
    				
    				if(!this.AvgGift_verifyActionMenuList()){
    					System.out.println("<Method: AvgGiftRes_ClickAction >AvgGiftCost Search_Results Table Action Menu list is NOT displayed,please check..!!");
    					return false;
    				}//End of IF condition to check if Action menu List Exists
    		
    				//Click on desired page link from Action menu list:
    				for (WebElement eachActionList : this.ele_AvgGitActionMenu_Row1.get(0).findElements(By.tagName("li"))) { 
    					if(eachActionList.getAttribute("textContent").equalsIgnoreCase(strDesiredPageLink)){
    						eachActionList.click();
    						Thread.sleep(3000);						
    						return true;
    					}
    				}//End of for loop to check Desired Page Link from Action Menu list
    				System.out.println("<Method: AvgGiftRes_ClickAction >Action menu List doesn't display desired page link "+strDesiredPageLink+" ,please check..!!");
    				return false;
    		
    			}//End of Try block
    			catch(Exception e){
    				System.out.println("Exception in <Class: AvgGiftCost_Objs ><Method: AvgGiftRes_ClickAction > Please check..!!");
    				return false;
    			}//End of catch block
    		}//End of <Method: AvgGiftRes_ClickAction>

    		
    		
    //###################################################################################################################################################################  
    //Function name			: AvgGiftRes_VerifyActionButtonExists(){
    //Class name			: NoCostBasis_Objs
    //Description 			: Function to verify if Action button exists
    //Parameters 			: 
    //Assumption			: None
    //Developer				: Kavitha Golla
    //###################################################################################################################################################################		  	  		  		
      public boolean AvgGiftRes_VerifyActionButtonExists(){
      	try{   
      		List<WebElement> ele_ActionButton;
  			ele_ActionButton = this.ele_AvgGitAction_Row1;
  			
  			if(ele_ActionButton.size()>0){
  				if(!CommonUtils.isElementPresent(ele_ActionButton.get(0))){
  	  				System.out.println("Checkpoint :[Failed] AvgGiftCost Search_Results Table,Action button on row1 is NOT displayed,please check..!!");
  	  				return false;
  				}//End of if condition to check if Action button on row1 in UI is displayed.
  				else System.out.println("Checkpoint :[Passed] AvgGiftCost Search_Results Table,Action button on row1 is displayed.");

  				ele_ActionButton.get(0).click();
  				Thread.sleep(5000);
  				if(!(ele_AvgGitActionMenu.size()>0)){
  	  				System.out.println("Checkpoint :[Failed] AvgGiftCost Search_Results Table Action Menu List is NOT displayed,please check..!!");
  	  				return false;						
  				}//End of if condition to check if Action Menu List is displayed
  				System.out.println("Checkpoint :[Passed] AvgGiftCost Search_Results Table Action Menu List is displayed");
  				return true;
  			}//End of if condition to check if Right Action button is displayed
  			else{
    				System.out.println("Checkpoint :[Failed] AvgGiftCost Search_Results Table Action button is NOT displayed,please check..!!");
    				return false;
  			}//End of else condition to check  if Action Button on right side is displayed
  				
      	}//End of Try block
      	catch(Exception e){
      		System.out.println("Exception in <Method: AvgGiftRes_VerifyActionButtonExists>AvgGiftCost Search_Results Table Action button is NOT displayed,please check..!!");
      		return false;    		
      	}//End of catch block
      	
      }//End of <Method: AvgGiftRes_VerifyActionButtonExists>  		
    		
  //###################################################################################################################################################################  
  //Function name			: AvgGift_verifyActionMenuList(){
  //Class name				: AvgGiftCost_Objs
  //Description 			: Function to verify if Action Menu exists and verify Action Menu
  //Parameters 				: 
  //Assumption				: None
  //Developer				: Kavitha Golla
  //###################################################################################################################################################################		  	  		  		
      public boolean AvgGift_verifyActionMenuList(){
  		List<String> actActionList = new ArrayList<String>();
  		ArrayList<String> expActionList = new ArrayList<String>();
  		expActionList.addAll(Arrays.asList("Tax Lot Edit"));
  		
  		if(this.ele_AvgGitActionMenu_Row1.size()>0){	
  			List<WebElement> app_ActionList = this.ele_AvgGitActionMenu_Row1.get(0).findElements(By.tagName("li"));
  			for (WebElement eachActionList : app_ActionList) { 
  				actActionList.add(eachActionList.getAttribute("textContent").trim());
  			}//End of for loop to read actual Action menu list
  			if(!actActionList.toString().equals(expActionList.toString())){
  				System.out.println("Checkpoint :[Failed] AvgGiftCost Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString()+"; Actual Action menu List = "+actActionList.toString());
  				return false;
  			}//End of IF condition to check Expected Action Menu List & Actual Action Menu List
  			System.out.println("Checkpoint :[Passed] AvgGiftCost Verifying Action Menu list in search Results.Expected Action menu List = "+expActionList.toString());
  			return true;
  		}//End of IF condition to check Menu List object exists
  		
  		System.out.println("<Method: AvgGift_verifyActionMenuList>Checkpoint_Fail:Action Menu list object in AvgGiftCost search Results is NOT displayed, please check...!!!");
  		return false;
      }//End of <Method: AvgGift_verifyActionMenuList> 	  	
        	
      
      
      
}//End of <Class: AvgGiftCost_Objs>
