package PageObjects.Maxit;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.DateUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.Maxit.SearchPageFns;
import scripts.Maxit.StepUpUpdate_Flow;

public class StepUp_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	SearchPageFns SearchPagefns = PageFactory.initElements(driver, SearchPageFns.class);

	SearchPage_Results SearchRes_Objs = PageFactory.initElements(driver, SearchPage_Results.class);
	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);
	StepUpUpdate_Flow stepupUpdateFlow = PageFactory.initElements(driver, StepUpUpdate_Flow.class);

	@FindBy(how=How.XPATH,using="//a[contains(@href, 'Maxit/CostBasisImports/StepUp2/enter.php')]")
	public WebElement lnk_StepUp;
	@FindBy(how=How.NAME,using="dateOfDeath")
	public WebElement txt_DoD;
	@FindBy(how=How.NAME,using="aVDate")
	public WebElement txt_AltValDate;
	@FindBy(how=How.NAME,using="stepPct")
	public WebElement txt_StepPct;
	@FindBy(how=How.ID,using="applyToPositions")
	public WebElement btn_Apply;
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-checker\"]")
	public WebElement chkbx_HdrCheckbox;
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-row-checker\"]")
	public List<WebElement> chkbx_RowsCheckbox;
	@FindBy(how=How.ID,using="submitStepUpData")
	public WebElement btn_StepUpSubmit;
	@FindBy(how=How.ID,using="stepUpError")
	public List<WebElement> ele_StepUpError;
	@FindBy(how=How.XPATH,using="//*[@id=\"stepUpError\" and contains(@class,\" x-hide-display\")]")
	public List<WebElement> ele_StepUpNoError;
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-cell-inner x-grid3-col-11\"]")
	public List<WebElement> ele_FMVColor;

	@FindBy(how=How.XPATH,using="//div[@class='x-grid3-cell-inner x-grid3-col-11']")
	public WebElement ele_FMVPerShare;


	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
	public WebElement ele_ResTble_Header;

	@FindBy(how=How.XPATH,using="//*[@class='x-grid3-body']")
	public WebElement ele_ResultsTable; 

	@FindBy(how=How.XPATH,using="//div[@class='x-grid3-body']")
	public WebElement ele_ResultsTableBody;


	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid-empty\"]")
	public WebElement ele_NoResults;

	public int intfmvValue;

	public String fmvPerShareWebTable;

	@FindBy(how=How.XPATH,using="//div[@id='stepUpError']")
	public WebElement ele_StepUpMessage;

	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/CostBasisImports/Report/enter.php']")
	private WebElement lnk_CBManager;

	@FindBy(how=How.XPATH,using="//input[@id='unknownStepup']")
	private WebElement ele_unknownCheckBox;

	public boolean stepUpApplied;

	@FindBy(how=How.XPATH,using="//*[@class='x-grid3-cell-inner x-grid3-col-9']")
	public WebElement ele_StepUpField;

	@FindBy(how=How.XPATH,using="//*[@class='x-grid3-cell-inner x-grid3-col-9']")
	public List<WebElement> ele_ListStepUpField;

	FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
	
	//*********************************************************************************************************************  
	//Performing Actions on objects in Search Page:
	//*********************************************************************************************************************      
	//###################################################################################################################################################################  
	//Function name		: StepUp_Search(String strTabName,int exclRowCnt,String strSheetName,acct , )
	//Class name		: StepUp_Objs
	//Description 		: Function to perform search based on input data in Step up page
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################
	public boolean StepUp_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strDoD, int exclRowCnt,String strSheetName){
		try{
			boolean blnSecTypeCheck = false;
			CBMngr_Objs.txtbx_Acct.sendKeys(strAcct);
			List<WebElement> SecType_List = CBMngr_Objs.lst_SecIDType.findElements(By.tagName("option"));
			for (WebElement option : SecType_List) {
				if(option.getText().equals(strSecType)){
					option.click();
					blnSecTypeCheck=true;
					break;
				}
			}//End of FOR loop
			if(!blnSecTypeCheck){
				for (WebElement option : SecType_List) {
					if(option.getText().equals("Security No")){
						System.out.println("Selecting default SecIDType as Security No");
						option.click();
						blnSecTypeCheck=true;
						break;
					}
				}//End of FOR loop
			}//End of IF condition to select default SecIDTpe= Security ID

			CBMngr_Objs.txtbx_SecurityID.sendKeys(strSecValue);
			txt_DoD.sendKeys(strDoD); 
			CBMngr_Objs.btn_Submit.click();
			Thread.sleep(5500);

			//Checkpoint: Check for Errors:
			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;

			//Check for Results table header
			if(CommonUtils.isElementPresent(SearchRes_Objs.ele_ResTable_Header)){
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Search After Input data insert:Passed", "", strTabName+"_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return true;
			}//End of IF condition to check for Results table .			

			//Check if page is loading:			
			if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
				System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;
			}//End of IF condition to check for Results table header.

			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Search after input:Failed, Results header is not displayed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.StepUp_Search>: CBManger->"+strTabName);
			return false;
		}//End of catch block
	}//End of <Method: StepUp_Search> 

	//###################################################################################################################################################################  
	//Function name		: StepUp_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName)
	//Class name		: HistCorrections_Objs
	//Description 		: Function to perform search based on input data under given CB Manager tab 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Kavitha Golla
	//###################################################################################################################################################################
	public void StepUp_CheckPoints(String strTabName,String strAcct,String strSecValue, String strDoD,int exclRowCnt,String strSheetName){
		String strSearchValues = "["+strAcct+";"+strSecValue+";"+strDoD+"]";
		String strFMVColor;
		try{
			//Checkpoint: verify if there are step up errors:
			if(!(ele_StepUpNoError.size()>0)){
				if(ele_StepUpError.size()>0){
					String strStepUPError = ele_StepUpError.get(0).getText();
					System.out.println("Step Up error after search for "+strSearchValues);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Step up error displayed: '"+strStepUPError+"'", "", "StepUp_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition to check for step up errors after search
			}

			//Checkpoint : Verify if Results rows are displayed
			int appRowsReturned = SearchRes_Objs.searchRes_NumOfRows_app(strTabName);
			if(!(appRowsReturned>0)){
				if(SearchRes_Objs.searchRes_NoResultsReturned(strTabName).equals("No Results Found")){
					System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Search values[Account;SecValue;DoD] - ["+strSearchValues+"] - No Results found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}
				System.out.println("Search Results for "+strSearchValues+" is not >0");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Search values[Account;SecValue;DoD] - ["+strSearchValues+"] - Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of IF condition to check No of rows returned


			//Checkpoint: to verify if Step Percentage field is displayed
			if(CommonUtils.isElementPresent(txt_StepPct)){
				System.out.println("Checkpoint : Step Up ->Step Percentage field is displayed");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->StepUp:Step Percentage field is displayed", "", "StepUp_StepPrcnt",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}//End of IF Condition to check if txt_StepPct element exists
			else reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Step Percentage field is NOT displayed,,Please check..!!", "", "StepUp_StepPrcnt",  driver, "BOTH_DIRECTIONS", strSheetName, null);

			//Checkpoint: to verify if Apply button is displayed
			if(CommonUtils.isElementPresent(btn_Apply)){
				System.out.println("Checkpoint : Step Up ->Step Apply field is displayed");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] StepUp->Step Apply button is displayed", "", "StepUp_Apply",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			}//End of IF Condition to check if txt_StepPct element exists		
			else reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Step Apply button is displayed", "", "StepUp_Apply",  driver, "BOTH_DIRECTIONS", strSheetName, null);

			//Checkpoint: Verify FMV/Share is populated
			if(ele_FMVColor.size()==appRowsReturned){
				for(int appEachRow=0;appEachRow<appRowsReturned;appEachRow++){
					String strActFMV = ele_FMVColor.get(appEachRow).getAttribute("textContent").trim();
					strActFMV = strActFMV.replace("\u00A0", "");

					if(!(strActFMV.trim()=="")){
						strFMVColor = ele_FMVColor.get(appEachRow).getAttribute("style");
						if(strFMVColor.toLowerCase().contains("green")){
							reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] StepUp->FMV/Share on row"+(appEachRow+1)+" is "+strActFMV+" and background color is 'GREEN'", "", "StepUp_FMVColor",  driver, "BOTH_DIRECTIONS", strSheetName, null);
							return;
						}//End of iF condition to verify FMV background color
					}//End of IF condition to check FMV value is populated or not
				}//End for for loop
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->FMV/Share background color on none of the rows is NOT 'GREEN',please check!!", "", "StepUp_FMVColor",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Total number of rows:"+appRowsReturned+"is not same as ele_FMVColor.size():"+ele_FMVColor.size()+",please check <StepUp_Objs.StepUp_CheckPoints>", "", "StepUp_FMVColor",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.StepUp_CheckPoints>");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <StepUp_Objs.StepUp_CheckPoints>,please check..!!", "", "StepUp_Exception",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of catch block
	}//End of <Method: StepUp_CheckPoints> 


	//###################################################################################################################################################################  
	//Function name		: StepUp_SearchForUpdateScript
	//Class name		: StepUp_Objs
	//Description 		: Function to perform search based on input data in Step up update case. Generic search flow doesn't have altValuation date parameter.
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean StepUp_SearchForUpdateScript(String strTabName, String strAcct, String strSecType,
			String strSecValue, String strStepUp_DoD, String altValuationDate, int exclRowCnt, String strSheetName) {
		try{
			boolean blnSecTypeCheck = false;
			CBMngr_Objs.txtbx_Acct.sendKeys(strAcct);
			List<WebElement> SecType_List = CBMngr_Objs.lst_SecIDType.findElements(By.tagName("option"));
			for (WebElement option : SecType_List) {
				if(option.getText().equals(strSecType)){
					option.click();
					blnSecTypeCheck=true;
					break;
				}
			}//End of FOR loop
			if(!blnSecTypeCheck){
				for (WebElement option : SecType_List) {
					if(option.getText().equals("Security No")){
						System.out.println("Selecting default SecIDType as Security No");
						option.click();
						blnSecTypeCheck=true;
						break;
					}
				}//End of FOR loop
			}//End of IF condition to select default SecIDTpe= Security ID

			CBMngr_Objs.txtbx_SecurityID.sendKeys(strSecValue);
			txt_DoD.sendKeys(strStepUp_DoD); 
			txt_AltValDate.sendKeys(altValuationDate);
			CBMngr_Objs.btn_Submit.click();
			Thread.sleep(5500);

			//Checkpoint: Check for Errors:
			if(!CBMngr_Objs.CBManager_Errors(strTabName, exclRowCnt, strSheetName)) return false;

			//Check for Results table header
			if(CommonUtils.isElementPresent(SearchRes_Objs.ele_ResTable_Header)){
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Search After Input data insert:Passed", "", strTabName+"_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return true;
			}//End of IF condition to check for Results table .			

			//Check if page is loading:			
			if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
				System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;
			}//End of IF condition to check for Results table header.

			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Search after input:Failed, Results header is not displayed", "", "SearchAfter_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.StepUp_Search>: CBManger->"+strTabName);
			return false;
		}//End of catch block
	}//End of stepup search method

	//###################################################################################################################################################################  
	//Function name		: StepUp_Update
	//Class name		: StepUp_Objs
	//Description 		: Function to perform stepup update in Step up page
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean StepUp_Update(String strTabName, String strAcct, String strSecType, String strSecValue, String strStepUp_DoD, String altValuationDate, 
			String rowNoToEdit, String fmvPerShare, String stepPercentageSheet, int exclRowCnt, String bondSecNo, String strSheetName, String strSearchValues, String stepUpLevel)
	{
		System.out.println("Inside StepUp_Update : " +strSheetName + " : Level " +stepUpLevel + " strSearchValues : "+strSearchValues);
		try{

			//Checkpoint: verify if there are step up errors:
			if(!(ele_StepUpNoError.size()>0)){
				if(ele_StepUpError.size()>0){
					String strStepUPError = ele_StepUpError.get(0).getText();
					System.out.println("Step Up error after search for "+strSearchValues);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Step up error displayed: '"+strStepUPError+"'", "", "StepUp_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}//End of IF condition to check for step up errors after search
			}

			//Checkpoint : Verify if Results rows are displayed
			int appRowsReturned = SearchRes_Objs.searchRes_NumOfRows_app(strTabName);
			if(!(appRowsReturned>0)){
				if(SearchRes_Objs.searchRes_NoResultsReturned(strTabName).equals("No Results Found")){
					System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Search values[Account;SecValue;DoD] - ["+strSearchValues+"] - No Results found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}
				System.out.println("Search Results for "+strSearchValues+" is not >0");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Search values[Account;SecValue;DoD] - ["+strSearchValues+"] - Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;
			}//End of IF condition to check No of rows returned

			System.out.println("rowNoToEdit sheet: " +rowNoToEdit +" || No of rows returned : " +appRowsReturned);

			//Verifying the rowCount from DT sheet and Application row count
			if(!(this.compareRowValues(rowNoToEdit , appRowsReturned)))
			{
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Row no: provided for update is greater then the total no: of rows."
						+ "Total Rows in Web Table : " +appRowsReturned
						+ "No of Rows to Edit from Sheet : "+rowNoToEdit, "", "Step-Up_testdatacheck",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				System.out.println("Checkpoint :[Failed] Row no: provided for update is greater then the total no: of rows."
						+ "Total Rows in Web Table : " +appRowsReturned
						+ "No of Rows to Edit from Sheet : "+rowNoToEdit);
				return false;
			}

			//Verify the stepup field in Application
			if(!(checkForStepUpValueInApp(exclRowCnt, strSheetName, stepUpLevel, strSearchValues, appRowsReturned, stepPercentageSheet)))
			{

				System.err.println("Checkpoint :[Failed] There is some issue with updating stepup for ["+strSearchValues+"], Not as expected. Skipping the stepup update. Please check.!!");
				return false;
			}

			//check for check box and click on it to select all rows
			this.VerifyCheckBoxAndClick(stepUpLevel, exclRowCnt, strSheetName, appRowsReturned);

			for(int eachRow=0;eachRow<appRowsReturned;eachRow++)
			{
				//Get Sec_no value from Web table
				String strSecNo = this.StepUp_Res_getCellData(eachRow, "Sec No");

				//Check the secno is a bond and then calculate the fmv value has below
				if(strSecNo != null && strSecNo.equalsIgnoreCase(bondSecNo))
				{
					this.intfmvValue = Integer.parseInt(fmvPerShare) * 10;
					System.out.println(" this.fmvValueForBond :  " + this.intfmvValue + " fmvPerShare from sheet : " +fmvPerShare);

				}
				else if(strSecNo != null && strSecNo != bondSecNo)
				{
					this.intfmvValue = Integer.parseInt(fmvPerShare);
					System.out.println(" this.intfmvValue :  " + this.intfmvValue + " fmvPerShare from sheet : " +fmvPerShare);

				}
				else if (strSecNo == null)
				{
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] secNo is null, Not as expected", "", "handleBondSecurities",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}

				//Get Sec_no value from Web table
				this.fmvPerShareWebTable = this.StepUp_Res_getCellData(eachRow, "FMV/Share");
				this.fmvPerShareWebTable = this.fmvPerShareWebTable.replace("\u00A0", "").trim();

				//Function to verify the basic FMV check points
				this.VerifyStepUp_FMVCheckPoints(exclRowCnt, strSheetName, bondSecNo, eachRow, this.fmvPerShareWebTable, this.intfmvValue, strSecNo, strSearchValues);	
			}

			System.out.println("stepPercentageSheet : " + stepPercentageSheet);

			//this logic is for multiple stepup case
			if(stepPercentageSheet.contains("#") && stepPercentageSheet.contains("unknown"))
			{
				if(ApplyMultipleStepupUpdate(exclRowCnt, strSheetName, appRowsReturned, stepPercentageSheet)) 
				{
					System.out.println("Multiple stepup has been applied. Yet to submit the stepup update");
				}
				else
				{
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Issue with applying multiple stepup update. Please check.!!", "", "MultipleStepupUpdate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.err.println("Checkpoint :[Failed] Issue with applying multiple stepup update. Please check.!!");
					return false;
				}
			}
			//check for the step % value from DT and perform stepup update
			else if(stepPercentageSheet.equalsIgnoreCase("unknown"))
			{
				if(ApplyUnknownStepupUpdate(exclRowCnt, strSheetName, appRowsReturned, stepPercentageSheet)) 
				{
					System.out.println("Unknown stepup has been applied. Yet to submit the stepup update");
				}
				else
				{
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Issue with applying unknown stepup update. Please check.!!", "", "UnknownStepupUpdate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.err.println("Checkpoint :[Failed] Issue with applying unknown stepup update. Please check.!!");
					return false;
				}
			}
			else
			{
				if(ApplyKnownStepupUpdate(exclRowCnt, strSheetName, appRowsReturned, stepPercentageSheet)) 
				{
					System.out.println("Stepup has been applied. Yet to submit the stepup update");
				}
				else
				{
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Issue with applying stepup update. Please check.!!", "", "StepupUpdate",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.err.println("Checkpoint :[Failed] Issue with applying stepup update. Please check.!!");
					return false;
				}
				
				
			}
			//Function to compare stepup applied values with DT
			if(compareAppliedStepUpDataWithDT(exclRowCnt, strSheetName, stepPercentageSheet, strStepUp_DoD, altValuationDate, appRowsReturned, stepUpLevel))
			{
				boolean stepUpApplied = performSubmitAndVerifyMessage(exclRowCnt, strSheetName, stepUpLevel, strSearchValues);
				System.out.println("StepUpApplied : " +stepUpApplied);
			}	

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.StepUp_Update>: CBManger->"+strTabName);
			System.out.println("Exception:"+e.getMessage());
			return false;
		}//End of catch block
		return stepUpApplied;
	}//End of <Method: StepUp_Update> 

	//###################################################################################################################################################################  
	//Function name		: checkForStepUpValueInApp
	//Class name		: StepUp_Objs
	//Description 		: Function to verify the stepup % in application.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public boolean checkForStepUpValueInApp(int exclRowCnt, String strSheetName, String strStepUpLevel, 
			String strSearchValues, int appRowsReturned, String stepPercentageSheet) { 
		boolean stepupCheck = false;
		try{
			//Looping through out the search result and check for the FMV value
			for(int i=0;i<appRowsReturned;i++)
			{
				String stepupPercentageApp = this.StepUp_Res_getCellData(i , "Step %");
				//Removing blank space
				stepupPercentageApp = stepupPercentageApp.replace("\u00A0", "").trim();
				
				//Check for the stepup value from Web table, if the value is blank assign it to zero.
				if(stepupPercentageApp.length() == 0)
				{
					stepupPercentageApp = "0";
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] StepUp % is '0' as expected, proceeding further to perform stepup process!! for "+strStepUpLevel + "step up update - ["+strSearchValues+"]", "", "StepUpCheck",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.out.println("Checkpoint :[Passed] StepUp % is '0' as expected for "+strStepUpLevel + "step up update - ["+strSearchValues+"]");
					stepupCheck = true;
				}
				//check for the step up value from Web table, if the value is unknown and the stepup value to apply 
				//is known & not a case for multiple stepup from data table, then enter in to the loop to revert the unknown stepup
				else if(stepupPercentageApp.equalsIgnoreCase("unknown") && (!(stepPercentageSheet.equalsIgnoreCase("unknown")) && !(stepPercentageSheet.contains("#"))))
				{
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] StepUp % is not equal to 0. "
							+ "Since this case is for Reverting the unknown stepup proceeding further to perform reverting the stepup process!! for "
							+ strStepUpLevel + "step up update - ["+strSearchValues+"]", "", "StepUpCheck",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.err.println("Checkpoint :[Warning] StepUp % is not equal to 0."+ 
							"Since this case is for Reverting the unknown stepup proceeding further to perform reverting the stepup process!! for " 
							+ strStepUpLevel + "step up update - ["+strSearchValues+"]");
					stepupCheck = true;
				}
				//check for the step up value from Web table, if the value is known stepup and the stepup value to apply 
				//is unknown from data table, then enter in to the loop to perform unknown stepup for already stepped up lot.
				else if(!(stepupPercentageApp.equalsIgnoreCase("unknown")) && (stepupPercentageApp.length() >1) && (stepPercentageSheet.equalsIgnoreCase("unknown")))
				{
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] StepUp % is not equal to 0. "
							+ "Since this case is for applying unknown stepup % for already stepped up lot, proceeding further to perform stepup update!! for "
							+ strStepUpLevel + "step up update - ["+strSearchValues+"]", "", "StepUpCheck",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					
					System.err.println("Checkpoint :[Warning] StepUp % is not equal to 0."+ 
							"Since this case is for applying unknown stepup % for already stepped up lot, proceeding further to perform stepup update!! for " 
							+ strStepUpLevel + "step up update - ["+strSearchValues+"]");
					stepupCheck = true;
				}

				//check for the step up value from Web table, if the value is known/unknown stepup, then exit the test
				else if (stepupPercentageApp != "0")
				{
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp % is not equal to '0' & stepup has been applied already '"+stepupPercentageApp+ "' for ["+strSearchValues+"], Not as expected. Skipping the stepup update. Please check.!!", "", "StepUpCheck",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					System.err.println("Checkpoint :[Failed] StepUp % is not equal to '0' & stepup has been applied already '"+stepupPercentageApp+ "' for ["+strSearchValues+"], Not as expected. Skipping the stepup update. Please check.!!");
					stepupCheck = false;
					return stepupCheck;
				}	
			}
		}
		catch(NoSuchElementException e)
		{
			System.err.println("Exception in <StepUp_Objs.checkForStepUpValueInApp>:Element Not available..!!!");
			return stepupCheck;
		}
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.checkForStepUpValueInApp>:Pease check..!!!");
			return stepupCheck;
		}
		return stepupCheck;
	}
	
	//###################################################################################################################################################################  
	//Function name		: ApplyMultipleStepupUpdate
	//Class name		: StepUp_Objs
	//Description 		: Function to perform multiple stepup update
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public boolean ApplyMultipleStepupUpdate(int exclRowCnt, String strSheetName, int appRowsReturned, String stepPercentageSheet) { 
		boolean stepUpApplyCheck = false;
		try{
			String[] multipleStepUp = stepPercentageSheet.split("#");

			//comparing the row count with the multiple stepup count. it should match.
			if(appRowsReturned == multipleStepUp.length)
			{
				//clicking on unknown check box
				ele_unknownCheckBox.click();

				//Clicking on apply button
				btn_Apply.click();

				//Wait till the applied stepup is getting populated in web table under stepup column
				wait.withTimeout(10,TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)				 
				.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.textToBePresentInElement(ele_StepUpField, "unknown"));

				//Looping through each row and update the stepup %
				for(int eachRow=0;eachRow<appRowsReturned;eachRow++)
				{
					System.out.println("multipleStepUp : " + multipleStepUp[eachRow]);

					//if the stepup is unknown then ignore this loop since the unknown stepup is already applied
					if(!(multipleStepUp[eachRow].equalsIgnoreCase("unknown")))
					{
						Actions actions = new Actions(driver);

						//Move on to stepup % field
						actions.moveToElement(ele_ListStepUpField.get(eachRow));

						actions.click();

						Thread.sleep(2000);

						//using actions class, entering input value in Web table 
						actions.sendKeys(multipleStepUp[eachRow]);
						Thread.sleep(2000);

						actions.build().perform();

						//clicking out side of the webtable to release the value in stepup% field. It will not impact the flow of code
						txt_StepPct.click();
					}
				}
				stepUpApplyCheck = true;
			}
			else
			{
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] No of rows returned in app doesn't match with the stepup Delimitor from sheet,"
				+"Not as expected. Skipping Applying the stepup update. Please check.!!", "", "StepUpCheck",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				stepUpApplyCheck = false;
			}
		}
		catch(NoSuchElementException e)
		{
			System.err.println("Exception in <StepUp_Objs.checkForStepUpValueInApp>:Element Not available..!!!");
			return stepUpApplyCheck;
		}
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.checkForStepUpValueInApp>:Pease check..!!!");
			return stepUpApplyCheck;
		}
		return stepUpApplyCheck;
	}

	//###################################################################################################################################################################  
	//Function name		: ApplyUnknownStepupUpdate
	//Class name		: StepUp_Objs
	//Description 		: Function to perform Unknown stepup update
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public boolean ApplyUnknownStepupUpdate(int exclRowCnt, String strSheetName, int appRowsReturned, String stepPercentageSheet) { 
		boolean stepUpApplyCheck = false;
		try{

			//clicking on unknown check box
			ele_unknownCheckBox.click();

			//Clicking on apply button
			btn_Apply.click();

			System.out.println("StepUpField value : " + ele_StepUpField.getText() + " sheet value  : " +stepPercentageSheet);


			//Wait till the applied stepup is getting populated in web table under stepup column
			wait.withTimeout(10,TimeUnit.SECONDS)
			.pollingEvery(2, TimeUnit.SECONDS)				 
			.ignoring(NoSuchElementException.class)
			.until(ExpectedConditions.textToBePresentInElement(ele_StepUpField, stepPercentageSheet));

			stepUpApplyCheck = true;
		}
		catch(NoSuchElementException e)
		{
			System.err.println("Exception in <StepUp_Objs.ApplyUnknownStepupUpdate>:Element Not available..!!!");
			return stepUpApplyCheck;
		}
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.ApplyUnknownStepupUpdate>:Pease check..!!!");
			return stepUpApplyCheck;
		}
		return stepUpApplyCheck;
	}


	//###################################################################################################################################################################  
	//Function name		: ApplyKnownStepupUpdate
	//Class name		: StepUp_Objs
	//Description 		: Function to perform Known stepup update
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public boolean ApplyKnownStepupUpdate(int exclRowCnt, String strSheetName, int appRowsReturned, String stepPercentageSheet) { 
		boolean stepUpApplyCheck = false;
		try{

			//Enter step up % from sheet
			txt_StepPct.sendKeys(stepPercentageSheet);

			//Clicking on Apply button
			btn_Apply.click();

			System.out.println("StepUpField value : " + ele_StepUpField.getText() + " sheet value  : " +stepPercentageSheet);

			//Wait till the applied stepup is getting populated in web table under stepup column
			wait.withTimeout(10,TimeUnit.SECONDS)
			.pollingEvery(2, TimeUnit.SECONDS)				 
			.ignoring(NoSuchElementException.class)
			.until(ExpectedConditions.textToBePresentInElement(ele_StepUpField, stepPercentageSheet));

			stepUpApplyCheck = true;
		}
		catch(NoSuchElementException e)
		{
			System.err.println("Exception in <StepUp_Objs.ApplyKnownStepupUpdate>:Element Not available..!!!");
			return stepUpApplyCheck;
		}
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.ApplyKnownStepupUpdate>:Pease check..!!!");
			return stepUpApplyCheck;
		}
		return stepUpApplyCheck;
	}	


	//###################################################################################################################################################################  
	//Function name		: compareAppliedStepUpDataWithDT
	//Class name		: StepUp_Objs
	//Description 		: Function to compare the stepup applied in web table value with DT.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public boolean compareAppliedStepUpDataWithDT(int exclRowCnt, String strSheetName, String stepPercentageSheet, 
			String strStepUp_DoDSheet, String altValuationDateSheet, int appRowsReturned, String strStepUpLevel) {
		boolean verifyStepUpData = true;
		String tempstepPercentage;
		try{		
			for(int eachRow=0;eachRow<appRowsReturned;eachRow++)
			{
				//check for multiple stepup case and compare each row.
				if(stepPercentageSheet.contains("#") && stepPercentageSheet.contains("unknown"))
				{
					String[] multipleStepUp = stepPercentageSheet.split("#");
					tempstepPercentage = multipleStepUp[eachRow];
				}
				else
				{
					tempstepPercentage = stepPercentageSheet;
				}

				//Get values from web table for the below mentioned columns
				String strAppDoD = this.StepUp_Res_getCellData(eachRow, "Date of Death");
				String strAppStepPercentage = this.StepUp_Res_getCellData(eachRow, "Step %");
				String strAppAltValDate = this.StepUp_Res_getCellData(eachRow, "Valuation Date");
				String strAppsecNo = this.StepUp_Res_getCellData(eachRow, "Sec No");

				strAppAltValDate = strAppAltValDate.replace("\u00A0", "").trim();

				if(altValuationDateSheet == null || altValuationDateSheet.trim().equals(""))
				{
					altValuationDateSheet = "";
				}

				System.out.println(strAppDoD+ " / "+ strStepUp_DoDSheet +" ; " + strAppStepPercentage+ " / "+ tempstepPercentage +" ;"+ strAppAltValDate.trim()+ "/"+ altValuationDateSheet);

				//Checking the applied stepup values with data table(Actual & expected)
				if((strAppDoD.equalsIgnoreCase(strStepUp_DoDSheet)) && (strAppStepPercentage.equalsIgnoreCase(tempstepPercentage)) && 
						Objects.equals(strAppAltValDate,altValuationDateSheet))
				{
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] StepUp Page:" + strStepUpLevel+ " StepUp Update : Verify Row num:" +eachRow
							+ "Security= ["+strAppsecNo+"] - Pass, Step-Up page : "+strStepUpLevel+ 
							"StepUp check ,Row num : "+eachRow +": Security= ["+strAppsecNo+"]:  DOD, Step%, AltValuation Date are correctly displayed in Step-up page"
							, "", "compareAppliedStepUpDataWithDT",  driver, "HORIZONTALLY", strSheetName, null);

					System.out.println("Checkpoint :[Passed] StepUp Page:" + strStepUpLevel+ " StepUp Update : Verify Row num:" +eachRow
							+ "Security= ["+strAppsecNo+"] - Pass, Step-Up page : "+strStepUpLevel+ 
							"StepUp check ,Row num : "+eachRow +": Security= ["+strAppsecNo+"]:  DOD, Step%, AltValuation Date are correctly displayed in Step-up page" + verifyStepUpData);
				}
				else
				{
					verifyStepUpData = false;
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp Page:" + strStepUpLevel+ " StepUp Update : Verify Row num:" +eachRow
							+ "Security= ["+strAppsecNo+"] - Fail, Incorrect in Step-up result on Row num# : "+eachRow+": Security= ["+strAppsecNo+"].  "
							+ "Values Actual/Expected - " +strAppDoD+ " / "+ strStepUp_DoDSheet +" ; " 
							+ strAppStepPercentage+ " / "+ tempstepPercentage +" ; "
							+ strAppAltValDate+ " / "+ altValuationDateSheet, "", "compareAppliedStepUpDataWithDT",  driver, "HORIZONTALLY", strSheetName, null);

					System.out.println("Checkpoint :[Failed] StepUp Page:" + strStepUpLevel+ " StepUp Update : Verify Row num:" +eachRow
							+ "Security= ["+strAppsecNo+"] - Fail, Incorrect in Step-up result on Row num# : "+eachRow+": Security= ["+strAppsecNo+"].  "
							+ "Values Actual/Expected - " +strAppDoD+ " / "+ strStepUp_DoDSheet +" ; " 
							+ strAppStepPercentage+ " / "+ tempstepPercentage +" ; "
							+ strAppAltValDate+ " / "+ altValuationDateSheet);
					System.err.println("Please check the value !! Step up update is skipped !!" + verifyStepUpData);
					return verifyStepUpData;
				}
			}
		}
		catch(NoSuchElementException e)
		{
			System.err.println("Exception in <StepUp_Objs.compareAppliedStepUpDataWithDT>:Element Not available..!!!");
			return false;
		}
		catch(Exception e){
			System.err.println("Exception in <StepUp_Objs.compareAppliedStepUpDataWithDT>:Pease check..!!!");
			return false;
		}
		return verifyStepUpData;
	}//End of compareAppliedStepUpDataWithDT method

	//###################################################################################################################################################################  
	//Function name		: performSubmitAndVerifyMessage
	//Class name		: StepUp_Objs
	//Description 		: Function to perform the stepup and verify the message.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public boolean performSubmitAndVerifyMessage(int exclRowCnt, String strSheetName, String strStepUpLevel, String strSearchValues) { 
		try{

			//Click on Submit
			btn_StepUpSubmit.click();

			//Wait till the message gets populated in stepup page
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
			wait.withTimeout(30,TimeUnit.SECONDS)
			.pollingEvery(2, TimeUnit.SECONDS)				 
			.ignoring(NoSuchElementException.class)
			.until(ExpectedConditions.visibilityOf(ele_StepUpMessage));

			//Check for the error message
			if(!CommonUtils.isElementPresent(ele_StepUpMessage)){
				System.out.println("Steup Message is not displayed,please check..!!");
			}//End of IF Condition to check if ele_ResTable element exists

			String stepupMessage = ele_StepUpMessage.getText();
			//Check for the stepup successful message and return the status
			if(stepupMessage.equalsIgnoreCase("Your request has been submitted and is currently being processed"))
			{
				this.stepUpApplied = true;
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] StepUp has been applied sucessfully for : "+strSearchValues +" by " + strStepUpLevel+ " Message : "+stepupMessage, "", "StepUpMessageCheck",  driver, "HORIZONTALLY", strSheetName, null);
				System.out.println("Checkpoint :[Passed] StepUp has been applied sucessfully for : "+strSearchValues +" by " + strStepUpLevel+ " Message : "+stepupMessage);	
			}
			else
			{
				this.stepUpApplied = false;
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Issue with StepUp update for : " +strSearchValues +" by " +  strStepUpLevel+ " Message : "+stepupMessage, "", "StepUpMessageCheck",  driver, "HORIZONTALLY", strSheetName, null);
				System.out.println("Checkpoint :[Failed] Issue with StepUp update for : " +strSearchValues +" by " +  strStepUpLevel+ " Message : "+stepupMessage);	
			}

			//Wait till the cb manager page gets visible after step up is applied
			Thread.sleep(13000);
			wait.withTimeout(30,TimeUnit.SECONDS)
			.pollingEvery(2, TimeUnit.SECONDS)				 
			.ignoring(NoSuchElementException.class)
			.until(ExpectedConditions.visibilityOf(lnk_CBManager));
		}
		catch(NoSuchElementException e)
		{
			System.out.println("Exception in <StepUp_Objs.verifyStepUp_AppliedAndSubmit>:Element Not available..!!!");
			return false;
		}
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.verifyStepUp_AppliedAndSubmit>:Pease check..!!!");
			return false;
		}
		return this.stepUpApplied;
	}

	//###################################################################################################################################################################  
	//Function name		: VerifyCheckBoxAndClick(String stepUpLevel, int exclRowCnt,String strSheetName)
	//Class name		: StepUp_Objs
	//Description 		: Function to verify if the position Recon Summary table is Displayed or not.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public void VerifyCheckBoxAndClick(String stepUpLevel, int exclRowCnt, String strSheetName, int appRowsReturned){
		try
		{
			//Check if the Header check box is available for selection
			if(CommonUtils.isElementPresent(chkbx_HdrCheckbox)){
				chkbx_HdrCheckbox.click();
				System.out.println(" Checkpoint :[Passed] step up page has header check box and clicked on it to select all rows.");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] step up page has header check box and clicked on it to select all rows.", "", "ClickingAllCheckBox",  driver, "HORIZONTALLY", strSheetName, null);
			}
			else
			{
				System.out.println(" Checkpoint :[Failed] Check box not available to select Lots.");
				reporter.reportStep(exclRowCnt, "Passed", " Checkpoint :[Failed] Check box not available to select Lots.", "", "HeaderCheckBox",  driver, "HORIZONTALLY", strSheetName, null);
			}
		} // End of Try block
		catch(Exception e)
		{
			System.err.println("Exception in <StepUp_Objs.VerifyCheckBoxAndClick>: check box-> is not available. Please check !!.");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Exception in <StepUp_Objs.VerifyCheckBoxAndClick>: Check box-> is not available. Please check !.", "", "CheckBox_Check",  driver, "HORIZONTALLY", strSheetName, null);
		} //End of Catch
	} ////End of <Method: VerifyCheckBoxAndClick> 

	//###################################################################################################################################################################  
	//Function name			: StepUp_Res_getCellData(int rowNumber,String colName)
	//Class name			: StepUp_Objs
	//Description 			: Function to get cell data from 'step up' Search results based on Row# and column name
	//Parameters 			: rowNumber
	//						: Column Name	
	//Assumption			: None
	//Developer				: Dhanasekar Kumar
	//###################################################################################################################################################################		
	public String StepUp_Res_getCellData(int rowNumber,String colName){
		try{  		
			System.out.println("StepUp_Res_getCellData" +driver.getTitle() +rowNumber + colName);
			if(!CommonUtils.isElementPresent(ele_ResultsTable)){
				System.out.println("Stepup results Table is not displayed,please check..!!");
				return null;	
			}//End of IF Condition to check if ele_ResTable element exists


			System.out.println("StepUp_Res_getCellData");
			if(!CommonUtils.isElementPresent(ele_ResultsTableBody)){
				System.out.println("Search Results Table ele_ResultsTableBody is not displayed in UI..!!!!");
				return null;	
			}//End of IF Condition to check if ele_ResTable element exists
			else
			{
				List<WebElement> app_rows = ele_ResultsTableBody.findElements(By.tagName("tr"));
				List<WebElement> app_cols = app_rows.get(rowNumber).findElements(By.tagName("td"));


				//Get the column index for Step %
				int colIndex = this.stepUp_GetColIndexByColName(colName);
				System.out.println("colIndex: " +colIndex);

				//Fetch the value from Webtable based on the column provided
				String strColValue = app_cols.get(colIndex).getAttribute("textContent");
				System.out.println("Value : " + strColValue);
				return strColValue;
			}

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <StepUp_Objs.StepUp_Res_getCellData>: Search Results Table is not displayed in UI..!!!" +e.getMessage());
			return "";
		}
	}// End of <Method: StepUp_Res_getCellData>


	//###################################################################################################################################################################  
	//Function name			: stepUp_GetColIndexByColName(String strColName)
	//Class name			: SearchPage_Results
	//Description 			: Function to read number of records retrieved from search result
	//Parameters 			: driver object
	//Assumption			: None
	//Developer				: Kavitha Golla
	//###################################################################################################################################################################		
	public int stepUp_GetColIndexByColName(String strColName){
		try{  			
			if(!CommonUtils.isElementPresent(ele_ResTble_Header)){
				System.out.println("Search_Results Table Header is not displayed,please check..!!");
				return 0;	
			}//End of IF Condition to check if ele_ResTable_Header element exists

			List<WebElement> getCols = ele_ResTble_Header.findElements(By.tagName("td"));
			//System.out.println("<Class:SearchPage_Results><Method:searchRes_GetColIndexByColName>: Total number of columns in Results table"+getCols.size());

			int iColLoop = 0;
			for (WebElement eachCol : getCols) {  		
				if (eachCol.getAttribute("textContent").trim().equalsIgnoreCase(strColName)){
					Integer colIndex = iColLoop;
					return colIndex;
				}//End of IF condition to check column name 
				iColLoop++;
			}//End of FOR loop to get Column Index

			System.out.println("<SearchPage_Results><Method:searchRes_GetColIndexByColName>: Search Results Table HEADER, Expected Column Name:"+strColName+" is not displayed in UI..!!!");
			return -1;
		}//End of Try block
		catch (NumberFormatException e) {
			System.out.println("NumberFormatException in <SearchPage_Results><Method:searchRes_GetColIndexByColName>: Column Index for column name: "+strColName+" is not Integer");
			return -1;
		}
		catch(Exception e){
			System.out.println("Exception in <SearchPage_Results><Method:searchRes_GetColIndexByColName>: Search Results Table HEADER is not displayed in UI..!!!");
			return -1;
		}  		
	}//End of stepUp_GetColIndexByColName method	

	//###################################################################################################################################################################  
	//Function name		: VerifyStepUp_FMVCheckPoints
	//Class name		: StepUp_Objs
	//Description 		: Function to verify the basic FMV check points.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################

	public void VerifyStepUp_FMVCheckPoints(int exclRowCnt, String strSheetName, String bondSecNo, int appEachRow,
			String fmvPerShareWT, int intfmvValue, String strSecNo, String strSearchValues) {
		try
		{	
			//Check for the security is not a bond
			if(!(strSecNo.equalsIgnoreCase(bondSecNo)))
			{
				String strActFMV = ele_FMVColor.get(appEachRow).getAttribute("textContent").trim();
				strActFMV = strActFMV.replace("\u00A0", "");
				System.out.println("strActFMV  :" +strActFMV);
				if(!(strActFMV.trim()=="")){
					String strFMVColor = ele_FMVColor.get(appEachRow).getAttribute("style");
					if(strFMVColor.toLowerCase().contains("green")){
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] StepUp->FMV/Share on row"+(appEachRow+1)+" is "+strActFMV+" and background color is 'GREEN'", "", "StepUp_FMVColor",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						System.out.println("Checkpoint :[Passed] StepUp->FMV/Share on row"+(appEachRow+1)+" is "+strActFMV+" and background color is 'GREEN'" +strSearchValues);
					}//End of iF condition to verify FMV background color
					else
					{
						reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Warning] StepUp->FMV/Share background color on none of the rows is NOT 'GREEN',please check!!", "", "StepUp_FMVColor",  driver, "BOTH_DIRECTIONS", strSheetName, null);
						System.err.println("Checkpoint :[Warning] StepUp->FMV/Share background color on none of the rows is NOT 'GREEN',please check!!" +strSearchValues);
						return;
					}
				}
			}
			//If the fmvpershare value from WT is epmty then assign it to 0 has below format
			if(fmvPerShareWT.trim().equals("") || fmvPerShareWT.trim().length()==0)
			{
				fmvPerShareWT = "$0.0000000000";
				System.out.println("fmvPerShareWT : "+fmvPerShareWT + " Calculated FMV: " +intfmvValue);
			}

			if(fmvPerShareWT.trim().equals("$0.0000000000"))
			{
				if(CommonUtils.isElementPresent(ele_FMVPerShare) && ele_FMVColor.size() >0)
				{
					Actions actions = new Actions(driver);
					actions.moveToElement(ele_FMVColor.get(appEachRow));

					actions.click();

					Thread.sleep(2000);
					String inputValue = Integer.toString(intfmvValue);

					//using actions class, entering input value in Web table 
					actions.sendKeys(inputValue);
					Thread.sleep(2000);
					actions.build().perform();
				}
			}
			else
			{
				System.out.println("fmvPerShare value is available in Web table : "+fmvPerShareWT);
				return;
			}
		}
		catch (Exception e)
		{
			System.out.println("Exception in <SteUp_Objs><VerifyStepUp_FMVCheckPoints>: Please check..!!!");
			System.out.println(e.getMessage());
		}

	}

	//###################################################################################################################################################################  
	//Function name		: stepUp_DataValidation
	//Class name		: StepUp_Objs
	//Description 		: Function to perform stepup data validation process in stepup page.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################


	public void stepUp_DataValidation(String strViewPage, String to_Execute, String strAcct, String strSecType,
			String strSecValue, String strRowCount, String strColNames, String strStepUp_DoD, String altValuationDate,
			String[] strExpRows, int exclRowCnt, String strSheetName, String strSearchValues) {
		String strTabName = "STEPUP";
		String[] aActualArr;
		int expectedRowCount = Integer.parseInt(strRowCount);
		System.out.println("\n#########################["+strTabName+"]Data Valiadtion: Executing Row iteration:["+exclRowCnt+"] with test data:"+strSearchValues+"#########################");

		//Checkpoint: verify if there are step up errors:
		if(!(ele_StepUpNoError.size()>0)){
			if(ele_StepUpError.size()>0){
				String strStepUPError = ele_StepUpError.get(0).getText();
				System.out.println("Step Up error after search for "+strSearchValues);
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Step up error displayed: '"+strStepUPError+"'", "", "StepUp_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of IF condition to check for step up errors after search
		}

		//Checkpoint : Verify if Results rows are displayed
		int appRowsReturned = SearchRes_Objs.searchRes_NumOfRows_app(strTabName);
		if(!(appRowsReturned>0)){
			if(SearchRes_Objs.searchRes_NoResultsReturned(strTabName).equals("No Results Found")){
				System.out.println("Search Results for "+strSearchValues+" is 'No Results Found'.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Search values[Account;SecValue;DoD] - ["+strSearchValues+"] - No Results found", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}
			System.out.println("Search Results for "+strSearchValues+" is not >0");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] StepUp->Search values[Account;SecValue;DoD] - ["+strSearchValues+"] - Results rows not >0, Not as expected 'No results Found'", "", "SearchResults_Rows",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of IF condition to check No of rows returned

		if(expectedRowCount!=0){
			aActualArr = this.StepUpSearchRes_GetActualRowArray(strColNames, expectedRowCount);

			//Compare the actual and expected arrays
			this.StepUpSearchResult_CompareArrays(exclRowCnt,strExpRows, aActualArr, strColNames, strTabName, strSearchValues,strSheetName);
		}//Check for RowCount not zero to call comparison method.


		System.out.println("###########################################################[End of Data Valiadtion: Row iteration:["+exclRowCnt+"]###########################################################");

		//return false;
	}// End of stepUp_DataValidation method

	//###################################################################################################################################################################  
	//Function name		: StepUpSearchRes_GetActualRowArray
	//Class name		: StepUp_Objs
	//Description 		: Function to get the actualvalues from webtable and store it in array.
	//Parameters 		: 
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################


	public  String[] StepUpSearchRes_GetActualRowArray(String strColNames,int rowCnt){

		System.out.println("StepUpSearchRes_GetActualRowArray" + strColNames +rowCnt);
		String[] actColList = strColNames.split(";");
		String [] aActualArr = new String[rowCnt];

		for(int i=1;i<=rowCnt;i++){
			String sActualValues ="";
			for(int iLoop=0;iLoop< actColList.length;iLoop++){
				String strColName = actColList[iLoop].trim();
				String strColValue = this.StepUp_Res_getCellData(i-1, strColName);

				if(iLoop==actColList.length){
					sActualValues = (sActualValues.toString() + strColValue.toString()).trim();
				}
				else{
					sActualValues = (sActualValues.toString() + strColValue.toString() + ";").trim();
				}				
			}// End of FOR Loop to loop through ColumnsList
			//Format each row value, add it to the result array-list 
			if(sActualValues.endsWith(";")){
				sActualValues = StringUtils.left(sActualValues, sActualValues.length()-1);	
			}
			aActualArr[i - 1] = sActualValues;			
		}// End of FOR Loop to loop through RowCount in Search Results table


		return aActualArr;
	}//End of SearchRes_GetActualRowArray method


	public  void StepUpSearchResult_CompareArrays(int exclRowCnt,String[] expArray, String[] actArray, String strColNames,String strViewPage, String strSearchValues,String strSheetName){
		boolean blnFound=false;
		String strExpArrayVal="";
		String strActArrayVal = "";
		try{
			for(String expArrayVal : expArray){
				strExpArrayVal = expArrayVal.toString().trim();
				strExpArrayVal = strExpArrayVal.replaceAll("\u00A0", "");
				for(int actArryLoop=0;actArryLoop<actArray.length;actArryLoop++){
					strActArrayVal = actArray[actArryLoop].toString().trim();
					strActArrayVal = strActArrayVal.replaceAll("\u00A0", "");
					System.out.println("Actual array value actArray["+actArryLoop+"].trim() is ["+ strActArrayVal+"]");
					System.out.println("Expected array value expArray is "+ strExpArrayVal);

					if(strExpArrayVal.equals(strActArrayVal)){
						blnFound = true;
						break;
					}//End of IF condition to compare Each Expected value with Actual Array value
				}//For loop for each expectedArray value

				if(blnFound){
					System.out.println(strViewPage+": Compare row values for " + strSearchValues +"[Passed]. Expected Row Value = ["+ strExpArrayVal +"] as Actual Row Value.");
					reporter.reportStep(exclRowCnt, "Passed", "Row Compare Passed", "", "Row Compare Passed",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}
				else{
					System.out.println(strViewPage+": Compare row values for " + strSearchValues +"[Failed]. Expected Row Value = ["+ strExpArrayVal +"] NOT as Actual Row Value.Column Names compared are ["+strColNames+"]. Please check screenshot.");	
					reporter.reportStep(exclRowCnt, "Failed", "Row_Compare_Failed", "", "Row_Compare",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				}
			}//For loop for each expectedArray value
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in <SearchResult_CompareArrays> method..!!");
			e.printStackTrace();
		}
	}//End of SearchResult_CompareArrays method


	//*****************************************************************************************************************************************************************
	public void UGL_Checkpoint_CheckTerm(int intExclRowNum,String strSheetName,int expectedRowCount){
		try{
			String strActualTerm, strActOpenDate,strActualQTy , strExpectedTerm;

			//Read Actual Term from UI
			strActualTerm = this.StepUp_Res_getCellData(expectedRowCount-1, "Term");


			//Read Actual Term from UI
			strActOpenDate = this.StepUp_Res_getCellData(expectedRowCount-1, "Open Date");

			//Read Actual Term from UI
			strActualQTy = this.StepUp_Res_getCellData(expectedRowCount-1, "Qty");

			//Calculate expected Term by  function
			strExpectedTerm = UGL_CalculateTerm(strActOpenDate, strActualQTy) ;
			if(strActualTerm.equalsIgnoreCase(strExpectedTerm)){
				System.out.println("UGL page[Row#"+expectedRowCount+"]: Term checkpoint_Passed. [Actual Term:"+strActualTerm+"] and Expected G/L:["+strExpectedTerm+"]");
				reporter.reportStep(intExclRowNum, "Passed", "Unrealized:Term checkpoint Pass. Actual ["+strActualTerm+"] and Expected ["+strExpectedTerm+"].", "", "UGL Term_Pass", driver, "HORIZONTALLY", strSheetName, null);
			}
			else{
				System.out.println("UGL page[Row#"+expectedRowCount+"]: Term checkpoint_Failed. [Actual Term:"+strActualTerm+"] and Expected G/L:["+strExpectedTerm+"]");
				reporter.reportStep(intExclRowNum, "Passed", "Unrealized:Term checkpoint Fail. Actual ["+strActualTerm+"] and Expected ["+strExpectedTerm+"].", "", "UGL Term_Fail", driver, "HORIZONTALLY", strSheetName, null);
			}
		}//End of try block
		catch(Exception e){
			System.out.println("Exception in <Method: UGL_Checkpoint_CheckTerm>, please check details below.");
			e.printStackTrace();
		}

	}//End of <Method: UGL_Checkpoint_CheckTerm >


	//*****************************************************************************************************************************************************************
	public String UGL_CalculateTerm(String strActOpenDate,String strActualQTy ){
		DateUtils dtUtils = new DateUtils();
		Date Current_date = new Date();
		Date dtActualOpenDate = dtUtils.convertString_to_Date(strActOpenDate);

		if(strActualQTy.substring(0, 1).equals("-"))	return "Short";		
		else if(dtUtils.dateDiff(dtActualOpenDate, Current_date, "m")> 12) return "Long";	
		else if (dtUtils.dateDiff(dtActualOpenDate, Current_date, "m")< 12) return "Short";
		//Handling Leap year:
		else if(dtUtils.dateDiff(dtActualOpenDate, Current_date, "m")==12){
			if(dtUtils.getDatePart(dtActualOpenDate, "d") <= dtUtils.getDatePart(Current_date, "d")) return "Short";	
			else return "Long";
		}//End of Else IF for handling Leap year

		return null;
	}

	public boolean compareRowValues(String rowNoToEdit, int appRowsReturned)
	{
		boolean rowCheck = false;
		if(Integer.parseInt(rowNoToEdit) > appRowsReturned)
		{
			rowCheck = false;
		}
		else
		{
			System.out.println("Checkpoint :[Passed] Row no: provided for update is matches with total no: of rows."
					+ "Total Rows in Web Table : " +appRowsReturned
					+ "No of Rows to Edit from Sheet : "+rowNoToEdit);
			rowCheck = true;
		}
		return rowCheck;
	}

}//End of <Class: StepUp_Objs>
