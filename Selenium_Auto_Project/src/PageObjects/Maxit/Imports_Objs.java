
package PageObjects.Maxit;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class Imports_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	StringManipulation strFns = new StringManipulation();
	Reporting reporter = new Reporting();
	WebDriverWait wait;

	CBManager_Objs CBMngr_Objs = PageFactory.initElements(driver, CBManager_Objs.class);

	//Imports
	@FindBy(how=How.XPATH,using="//a[text()='Imports']")
	public WebElement lnk_Imports;

	@FindBy(how=How.LINK_TEXT,using="Export to Excel")
	public WebElement lnk_ExportToExcel1;

	@FindBy(how=How.XPATH,using="//div[@class='pageLinks']/a")
	public WebElement lnk_ExportToExcel;	



	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-hd-row\"]")
	public WebElement ele_ResTble_Header;
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-body\"]")

	public WebElement ele_ResTble;
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid-empty\"]")
	public WebElement ele_NoResults;
	@FindBy(how=How.XPATH,using="//*[@class=\"x-grid3-row-expander\"]")
	public List<WebElement> ele_EditButtons;
	@FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Apply\")]")
	public WebElement btn_Apply;
	@FindBy(how=How.XPATH,using="//*[@class=\" x-btn-text\" and contains(text(),\"Cancel\")]")
	public WebElement btn_Cancel;

	@FindBy(how=How.ID,using="importType")
	public WebElement lst_ImportType;

	@FindBy(how=How.ID,using="status")
	public WebElement lst_ImportStatus;

	@FindBy(how=How.ID,using="dateFrom")
	public WebElement txtbx_dateFrom;

	@FindBy(how=How.ID,using="dateTo")
	public WebElement txtbx_dateTo;

	LocalDateTime strStartWaitTime,strEndWaitTime;


	//*********************************************************************************************************************  
	//Performing Actions on objects in Search Page:
	//*********************************************************************************************************************      
	//###################################################################################################################################################################  
	//Function name		: Imports_Data_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName)
	//Class name		: Imports_Objs
	//Description 		: Function to perform search based on input data.
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean Imports_Data_Search(String strTabName,String strAcct,String strSecType,String strSecValue,String strFromDate,String strToDate,int exclRowCnt,String strSheetName){
		try{
			String strSearchValues = strTabName+";"+strAcct+";"+strSecType+";"+strSecValue+";"+strFromDate+";"+strFromDate;
			System.out.println("Inside CBImports_Data_Search : " +strSearchValues);
			boolean blnSecTypeCheck = false;

			// Account number			
			CBMngr_Objs.txtbx_Acct.sendKeys(strAcct);

			//Logic to select the security type dropdown based on the parameter available in input sheet
			if(strSecType != null)
			{
				List<WebElement> SecType_List = CBMngr_Objs.lst_SecIDType.findElements(By.tagName("option"));
				for (WebElement option : SecType_List) {
					if(option.getText().equals(strSecType)){
						option.click();
						blnSecTypeCheck=true;
						break;
					}
				}//End of FOR loop
				if(!blnSecTypeCheck){
					for (WebElement option : SecType_List) {
						if(option.getText().equals("Security No")){
							System.out.println("Selecting default SecIDType as Security No");
							option.click();
							blnSecTypeCheck=true;
							break;
						}
					}//End of FOR loop
				}//End of IF condition to select default SecIDType= Security ID

				CBMngr_Objs.txtbx_SecurityID.sendKeys(strSecValue);

			}//End of IF condition null check

			//Logic to select the Import type dropdown as All by default.
			List<WebElement> ImportType_List = lst_ImportType.findElements(By.tagName("option"));
			System.out.println(" ImportType_List size : " +ImportType_List.size());
			for (WebElement option : ImportType_List) {
				if(option.getText().equals("All")){
					option.click();
					break;
				}
			}//End of FOR loop

			//Logic to select the status dropdown as All by default.
			List<WebElement> Status_List = lst_ImportStatus.findElements(By.tagName("option"));
			System.out.println(" Status_List size : " +Status_List.size());
			for (WebElement option : Status_List) {
				if(option.getText().equals("All")){
					option.click();
					break;
				}
			}//End of FOR loop

			//Providing From & To dates
			writeInputAfterClearing(txtbx_dateFrom,strFromDate);
			writeInputAfterClearing(txtbx_dateTo,strToDate);

			//Performing Data search
			CBMngr_Objs.btn_Submit.click();
			
			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			strStartWaitTime = LocalDateTime.now();
			try{
				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
				wait.withTimeout(180,TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)				 
				.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.visibilityOf(ele_ResTble_Header));
			}//End of try block for FluentWait

			catch(org.openqa.selenium.TimeoutException e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());

				//Check if page is loading:			
				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
					System.out.println("Checkpoint : Search After Input data insert - Stil Loading..[Failed]");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"_1",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}//End of IF condition to check for loading object.

				//Checkpoint: Check for Errors
				if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;
				
			}//End of Catch block for FluentWait
			catch(Exception e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return false;
			}//End of catch block with generic exception for Fluent Wait 

			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			//Check for Results header
			if(CommonUtils.isElementPresent(ele_ResTble_Header)){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Checkpoint : Search After Input data ["+strSearchValues+"] insert:Passed");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Search After Input data ["+strSearchValues+"] insert:Passed", "", strTabName+"_Input",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return true;
			}//End of IF condition to check for Results table header.			

			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search After Input data Failed, Results header is not displayed", "", strTabName+"_2",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;

		}//End of Try block
		catch(Exception e){
			System.err.println("Exception in <Imports_Objs.Imports_Data_Search>: CBManger->"+strTabName+" verifying header title.");
			return false;
		}//End of catch block
	}//End of <Method: Imports_Data_Search> 

	//###################################################################################################################################################################  
	//Function name		: Imports_BlankSearch(String strTabName,int exclRowCnt,String strSheetName)
	//Class name		: Imports_Objs
	//Description 		: Function to perform Blank search in CB Manager Imports tab 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public boolean Imports_BlankSearch(String strTabName,int exclRowCnt,String strSheetName){
		try{			
			System.out.println("Inside CBImports_BlankSearch : ");

			//Performing blank Search
			CBMngr_Objs.btn_Submit.click();
			
			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			strStartWaitTime = LocalDateTime.now();
			try{
				System.out.println("Dynamic Wait time begins for @"+strStartWaitTime);
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
				wait.withTimeout(180,TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS)				 
				.ignoring(NoSuchElementException.class)
				.until(ExpectedConditions.visibilityOf(ele_ResTble_Header));
			}//End of try block for FluentWait

			catch(org.openqa.selenium.TimeoutException e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());

				//Check if page is loading:			
				if(CommonUtils.isElementPresent(CBMngr_Objs.ele_Loading)){
					System.out.println("Checkpoint : Blank Search - Stil Loading..[Failed]");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Search after input:Failed,still Loading", "", strTabName+"_3",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}//End of IF condition to check for loading object.

				//Checkpoint: Check for Errors
				if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;
				
			}//End of Catch block for FluentWait
			catch(Exception e){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Dynamic Wait time Ends with a Max threshold wait of 35Sec @"+strEndWaitTime+".The difference in start & End time is"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return false;
			}//End of catch block with generic exception for Fluent Wait 

			//Checkpoint: Check for Errors
			if(!CBMngr_Objs.CBManager_Errors(strTabName,exclRowCnt,strSheetName)) return false;

			//Check for Results header
			if(CommonUtils.isElementPresent(ele_ResTble_Header)){
				strEndWaitTime = LocalDateTime.now();
				System.out.println("Checkpoint : CB Manager->\"+strTabName+\" Performing : Blank Search");
				reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] CB Manager->"+strTabName+" Performing : Blank Search", "", strTabName+"_BlankSearch",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				System.out.println("Dynamic Wait time Ends @"+strEndWaitTime+".Difference between start & end time in SECONDS is:"+Duration.between(strStartWaitTime, strEndWaitTime).getSeconds());
				return true;
			}//End of IF condition to check for Results table header.			

			System.out.println("Checkpoint : Search After Input data insert - [Failed]");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] CB Manager->"+strTabName+" Blank Search - Failed, Results header is not displayed", "", strTabName+"_4",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return false;
		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Imports_Objs.Imports_BlankSearch>: CBManger->"+strTabName+" verifying header title.");
			return false;
		}//End of catch block
	}//End of <Method: Imports_BlankSearch>     	

	//###################################################################################################################################################################  
	//Function name		: Imports_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName)
	//Class name		: Imports_Objs
	//Description 		: Function to verify the row counts and the Export to Excel Link in Web page. 
	//Parameters 		: None
	//Assumption		: None
	//Developer			: Dhanasekar Kumar
	//###################################################################################################################################################################
	public void Imports_CheckPoints(String strTabName,String strAcct,int exclRowCnt,String strSheetName){
		try{
			SearchPage_Results objSearchRes = PageFactory.initElements(driver, SearchPage_Results.class);
			System.out.println("Inside Imports_CheckPoints : ");

			//Getting the row count from Dynamic search results table.
			int appRowsReturned = objSearchRes.searchRes_NumOfRows_app(strTabName);
			System.out.println("appRowsReturned : " +appRowsReturned);
			if(!(appRowsReturned>0)){
				if(ele_NoResults.getText().equals("No Results Found")){
					System.out.println("Search Results for Import page is 'No Results Found'.");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] 'Imports search Results' No Results found.", "", "Imports_Res",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return;
				}//End of IF condition
				System.out.println("Search Results for Import page is not >0");
				reporter.reportStep(exclRowCnt, "Failed", "'Imports search Results' rows not >0, Not as expected 'No results Found'", "", "Imports_Res",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return;
			}//End of IF condition to check No of rows returned
			else{
				System.out.println("Search Results for Import page is >0 " +appRowsReturned);
				reporter.reportStep(exclRowCnt, "Passed", "'Imports search Results' rows >0 i.e "+appRowsReturned+" record(s) retrieved successfully", "", "Imports_Res",  driver, "BOTH_DIRECTIONS", strSheetName, null);

				if(!(CBMngr_Objs.VerifyExportToExcelLink(lnk_ExportToExcel, exclRowCnt, strSheetName, "CB-Manager Imports"))){
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] --> Export to Excel Link is not available in CB-Imports Page. Please check !!..", "", "Imports_ExceLink",  driver, "HORIZONTALLY", strSheetName, null);
					System.out.println("Link not available.");
				}//End of IF condition to check for the Export Link
			}//End of Else

		}//End of Try block
		catch(Exception e){
			System.out.println("Exception in <Imports_CheckPoints.Imports_CheckPoints>: CBManger->Imports verifying header title.");
			reporter.reportStep(exclRowCnt, "Failed", "Exception in <Imports_CheckPoints.Imports_CheckPoints>,please check..!!", "", "Imports_Exception",  driver, "BOTH_DIRECTIONS", strSheetName, null);
			return;
		}//End of catch block
	}//End of <Method: Imports_CheckPoints>     	

	public void writeInputAfterClearing(WebElement element, String input)
	{
		element.clear();
		element.sendKeys(input);
	}

}//End of <Class: Imports_Objs>
