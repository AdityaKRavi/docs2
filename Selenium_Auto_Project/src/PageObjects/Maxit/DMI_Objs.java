package PageObjects.Maxit;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_Maxit;
import functionalLibrary.Global.Reporting;

public class DMI_Objs {
	WebDriver driver = ManageDriver_Maxit.getManageDriver().getDriver();
	Reporting reporter = new Reporting();
	Actions action = new Actions(driver);
	CommonUtils util = new CommonUtils();
	
	@FindBy(how=How.XPATH,using="//a[@href='/Modules/Accounts/Maxit/DMI/sso.php']")
    private WebElement lnk_DMI;
	@FindBy(how=How.LINK_TEXT,using="Data Management")	
	private WebElement lnk_DataManagement;	
	@FindBy(how=How.ID,using="menuItemHilite14")	
	private WebElement lnk_Review;
	@FindBy(how=How.ID,using="menuItemText10")	
	private WebElement lnk_Transaction;
	
	 
	@FindBy(how=How.XPATH,using="//*[contains(@class,\"subheading\") and contains(text(),\"Transaction (TXN)\")]")	
	private WebElement Txn_Header;
	@FindBy(how=How.NAME,using="accountNo")	
	private WebElement Txn_AcctNo;
	@FindBy(how=How.NAME,using="submit")	
	private WebElement Txn_Submit;
    //@FindBy(how=How.XPATH,using="//*[@id=\"record\"]/tbody")
	
	@FindBy(how=How.XPATH,using="//*[@id=\"record\" and @class=\"fixedHeaderTable\"]")
    private WebElement ele_DMI_Results;
    @FindBy(how=How.XPATH,using="//*[@class=\"emptyResultsText\"]")
    private WebElement ele_DMI_NoRecords;
	
    @FindBy(how=How.XPATH,using="//*[@class=\"errors\"]")
    private WebElement ele_DMI_Errors; 
    @FindBy(how=How.XPATH,using="//*[@class=\"systemMessage error\"]")
    private WebElement ele_TXN_Errors;
    
//    @FindBy(how=How.XPATH,using="//*[@id=\"contentContainer\"]/tbody/tr[1]/td")
    @FindBy(how=How.XPATH,using="//*[@id=\"contentContainer\"]")
    private WebElement ele_DMI_DashBoard_Header; 
	
	  //*********************************************************************************************************************  
    //Performing Actions on objects in Search Page:
  //*********************************************************************************************************************   
  //###################################################################################################################################################################  
  //Function name	: VerifyandClick_Transaction(int exclRowCnt,String strSheetName)
  //Class name		: DMI_Objs
  //Description 	: Function to verify if Trade link exsts, by Mouse hovering "Data Management->Review and Export->Trade"
  //Parameters 		: None
  //Assumption		: None
  //Developer		: Kavitha Golla
  //###################################################################################################################################################################		
  	public boolean VerifyandClick_Transaction(int exclRowCnt,String strSheetName){
  		try{  			
  			if(CommonUtils.isElementPresent(lnk_DataManagement)){
  		       action.moveToElement(lnk_DataManagement).perform();
  		       action.moveToElement(lnk_Review).perform();
  		       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
  		       action.moveToElement(lnk_Transaction);
  		       driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  		       action.click().build().perform();
 		       
				//Check if there are any error messages:
				if(CommonUtils.isElementPresent(ele_DMI_Errors)){
					String strErMsg = ele_DMI_Errors.getAttribute("textContent");
					System.out.println("Checkpoint : Error in DMI->Transaction Page, Error Message is - "+strErMsg);
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMI_TransactionPage_ErrorMessage:"+strErMsg, "", "DMI_TransactionPage_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return false;
				}//End of if condition to to see if there are errors.
  		       
				//Verify if Transaction page is displayed.
				if(CommonUtils.isElementPresent(Txn_Header)){
					System.out.println("Checkpoint : Successfully navigated to DMI->Transaction page");
					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] Successfully navigated to DMI->Transaction Page.", "", "DMI_TxnPage",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return true;
				}
				else{
					System.out.println("Checkpoint : Page is NOT navigated to DMI->Transaction page,please check!!");
					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Page is NOT navigated to DMI->Transaction page,please check!!", "", "DMI_TxnPage",  driver, "BOTH_DIRECTIONS", strSheetName, null);
					return true;
				}
  			}//End of IF Condition to check if lnk_DMI element exists
			System.out.println("Checkpoint : Data Mangement link is not displayed to mousehover to TXN page,please check!!");
			reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] Data Mangement link is not displayed to mousehover to TXN page,please check!!", "", "DMI_Page",  driver, "BOTH_DIRECTIONS", strSheetName, null);
  			return false;	
  		}//End of Try block
  		catch(Exception e){
  			System.out.println("Exception notes: "+e.getMessage());
  			System.out.println("Exception in <DMI_Objs.VerifyandClick_Transaction>: Issue in MouseHover,Transaction Link is not displayed in UI..!!!");
  			return false;
  		}
  	}//End of VerifyandClick_Transaction Method		
    //###################################################################################################################################################################  
    //Function name	: DMITxn_search(int exclRowCnt,String strSheetName,String strAccount)
    //Class name	: DMI_Objs
    //Description 	: Function to perform search on DMI->Transaction page
    //Parameters 	: None
    //Assumption	: None
    //Developer		: Kavitha Golla
    //###################################################################################################################################################################		
    	public boolean DMITxn_search(int exclRowCnt,String strSheetName,String strAccount){
    		try{  			
    			if(CommonUtils.isElementPresent(Txn_AcctNo)){
    				Txn_AcctNo.sendKeys(strAccount);   	
    				Txn_Submit.click();
    				Thread.sleep(10000);
    				
    				//Check if there are any error messages:
    				if(CommonUtils.isElementPresent(ele_TXN_Errors)){
    					String strErMsg = ele_TXN_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in DMI->Transaction Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMI_Txn Search_ErrorMessage:"+strErMsg, "", "DMI_Txn_Search_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.
    				
    				//Check for Number of records displayed.
    				if((CommonUtils.isElementPresent(ele_DMI_NoRecords)) && (ele_DMI_NoRecords.getAttribute("textContent").equalsIgnoreCase("Records Not Found."))){
    					System.out.println("Checkpoint : DMI->Transaction Page: Records Not Found.");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMI_Txn Search: Records Not Found.", "", "DMI_Txn_Search",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return false;
    				}//End of IF condition to check No Results returned.
    				else if(CommonUtils.isElementPresent(ele_DMI_Results)){
    					List<WebElement> app_rows = ele_DMI_Results.findElements(By.tagName("tr"));
    					System.out.println("Checkpoint : DMI->Transaction Search[Account:"+strAccount+"], total rows dispalyed:"+app_rows.size());
    					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] DMI_Txn Search[Account:"+strAccount+"]:Total "+app_rows.size()+" are records displayed.", "", "DMI_Txn_Search",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return true;
    				}//End of IF condition to check if results table is displayed


    			}//End of IF Condition to check if Account number field is displayed 
				System.out.println("Checkpoint : DMI->Transaction Page: Account Number field is not dispalyed.");
				reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMI_Txn  Account Number field is not dispalyed.", "", "DMI_Txn_Acct",  driver, "BOTH_DIRECTIONS", strSheetName, null);
				return false;
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <DMI_Objs.DMITxn_search>: DMI->Transaction Page->Account Number field is not displayed in UI..!!!");
    			return false;
    		}
    	}//End of DMITxn_search Method	
  	
    //###################################################################################################################################################################  
    //Function name	: VerifyandClick_DMILink(int exclRowCnt,String strSheetName)
    //Class name	: DMI_Objs
    //Description 	: Function to verify if DMI link exists
    //Parameters 	: None
    //Assumption	: None
    //Developer		: Kavitha Golla
    //###################################################################################################################################################################		
    	public boolean VerifyandClick_DMILink(int exclRowCnt,String strSheetName){
    		try{  			
    			if(CommonUtils.isElementPresent(lnk_DMI)){
    				lnk_DMI.click();    	
    				Thread.sleep(5000);
    				if(!((util.switch_BrowserTabs(driver,"DMI"))||(util.switch_BrowserTabs(driver,"Data Management Interface")) || (util.switch_BrowserTabs(driver,"Maxit: Broker Dashboard")))) {
    					System.out.println("Script focus to DMI page is not successful, please check");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMIPage_SwtichFocus is NOT Successful", "", "DMIPage_Focus",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        				
    					//Service Temporarily Unavailable
        				if((util.switch_BrowserTabs(driver,"503 Service Temporarily Unavailable"))) {
        					System.out.println("Script focus to DMI page is not successful, please check");
        					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMIPage_Navigation failed,'503 Service Temporarily Unavailable',please check..!!", "", "DMIPage_Focus",  driver, "BOTH_DIRECTIONS", strSheetName, null);
        					return false;
        				}
    				}

    				//Check if there are any error messages:
    				if(CommonUtils.isElementPresent(ele_DMI_Errors)){
    					String strErMsg = ele_DMI_Errors.getAttribute("textContent");
    					System.out.println("Checkpoint : Error in DMI Page, Error Message is - "+strErMsg);
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMIPage_ErrorMessage:"+strErMsg, "", "DMIPage_Error",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return false;
    				}//End of if condition to to see if there are errors.
    				
    				//Verify Dashboard text in DMI page:
    				//ele_DMI_DashBoard_Header
    				if(CommonUtils.isElementPresent(ele_DMI_DashBoard_Header)){
//    					System.out.println("border color:"+ele_DMI_DashBoard_Header.getCssValue("border-color"));
//    					//#f67911 -> orange color
//    					if(!((ele_DMI_DashBoard_Header.getCssValue("border-color")).equals("#f67911") || (ele_DMI_DashBoard_Header.getCssValue("border-color")).equals("rgb(246, 121, 17)"))){
//	    					System.out.println("Checkpoint : DMI Page is blank(Orange text container doesn't exixts),please check..!");
//	    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMIPage_Blank:DMI Page is blank(Orange text container doesn't exixts),please check..!", "", "DMIPage_Blank",  driver, "BOTH_DIRECTIONS", strSheetName, null);
//	    					return false;
//    					}
    					System.out.println("DMI Page loaded succesfully - Dashboard header exists");
    					reporter.reportStep(exclRowCnt, "Passed", "Checkpoint :[Passed] DMI Page loaded succesfully - Dashboard header exists", "", "DMIPage_Load",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return true;
    				}//End of if condition to to see if DMI page is blank.
    				else{
    					System.out.println("Checkpoint : DMI Page is blank-Dashboard header is NOT displayed ,please check..!");
    					reporter.reportStep(exclRowCnt, "Failed", "Checkpoint :[Failed] DMIPage_Blank:DMI Page is blank-Dashboard header is NOT displayed,please check..!", "", "DMIPage_Blank",  driver, "BOTH_DIRECTIONS", strSheetName, null);
    					return false;
    				}//End of else condition to to see if DMI page is blank.
    				
    			}//End of IF Condition to check if lnk_DMI link exisis and click element exists
    			return false;	
    		}//End of Try block
    		catch(Exception e){
    			System.out.println("Exception in <DMI_Objs.VerifyandClick_DMILink>: Data Management Link is not displayed in UI..!!!");
    			return false;
    		}
    	}//End of VerifyandClick_DMILink Method		

	
  	
  	
	
	
	
	
	
}//End of <Class: DMI_Objs>
