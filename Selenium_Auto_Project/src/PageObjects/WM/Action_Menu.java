package PageObjects.WM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;

public class Action_Menu {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	ResultTable_Section ResultTable_Section = new ResultTable_Section();
	StringManipulation StringManipulation = new StringManipulation();
	Trade_Widget tw = new Trade_Widget();
	HoldingsZoom_Page hp = new HoldingsZoom_Page();
	WebDriverWait wait;
	
	
	//************************   Test Objects   ************************
	//******************************************************************
	@FindBy(how = How.XPATH, using = ".//ul[contains(@class, 'dropdown-menu-right')]")
	public WebElement am_dropdown;
	
	@FindBy(how = How.XPATH, using = "table tbody tr:nth-child(1) td:nth-child(14) .btn")
	public WebElement usb_am_icon1;
	
	
	@FindBy(how = How.CSS, using = ".modal-dialog.modal-lg")
	public WebElement am_HZoom_Popup;
	
	@FindBy(how = How.ID, using = "sviHoldingZoomModalLabelTitle")
	public WebElement am_HZoom_Title;
	
	@FindBy(how = How.CSS, using = ".svi-holding-zoom h5 span:nth-child(1) >> symbol")
	public WebElement am_HZoom_Symbol;

	@FindBy(how = How.XPATH, using = ".//*[@id='svi-holding-zoom']//div [contains(@class, \"row\")]/h1")
	public WebElement am_HZoom_SymbolDesc;

	
	//************************ Other page  Test Objects   ************************
	//******************************************************************

	@FindBy(how = How.XPATH, using = ".//div[contains(@class,'symbol-class-input')]")
	public WebElement symbol_Report;
	
	
	@FindBy(how = How.XPATH, using = ".//*[@id='optionChainQuote']/dd[1]")
	public WebElement symbol_OptionChains;
	
	@FindBy(how = How.XPATH, using = ".//span[@class='mainChart-tickerName']")
	public WebElement symbol_Charts;
	

	//--------------  Constructor to initialize all elements  --------------
	public Action_Menu() {
		this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
	}
	//--------------  Constructor to initialize all elements  --------------
	
	//##############################################     FUNCTIONS       ###################################
	//######################################################################################################
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_ActionMenu
	//Description - This function verifies Action Menu features.
	//Argument - row no:, clients name, acnt no, symbol, exp descripton, filter name, sec value, exp options and exp trade links
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void verify_ActionMenu(int rowNum, String strClient, String acntNo, String symbol, String expDesc, String filterName, String secValue, String expOptionLinks, String expTradeLinks) throws InterruptedException {
		try {	
				//apply sec value filter and check for no results found
			 	if(!ResultTable_Section.applyFilter_VerifyNoReuslt(rowNum, "All Asset Types", secValue)) {
			 		return;
			 	}
				//click action menu icon and verify it opens up	
				if(! clickAndVerify_AMPopup( rowNum, symbol)) {
					return;
				}
				
				//Verify all the expected dropdown options are present
				String expLinks = expOptionLinks + ";" +  expTradeLinks;
				int colNum =  ResultTable_Section.get_ColIndex("Symbol");
				int resltRow = ResultTable_Section.get_rowNum_ForMatchingValue(symbol, colNum);
				int reqColNum = ResultTable_Section.get_ColCount();
				List<String> ddList = get_eleList(resltRow, reqColNum);
				
				if(verifyStringsMatchesList(expLinks,ddList)) {
					System.out.println("PASSED - All dropdown Values of quick quote have matched");
					Reporting.reportStep(rowNum, "Passed", "Quick Quote has all exp dropdown values." , "", "", driver, "", "", null);
				}else {
					System.out.println("FAILED - Quick quote dropdown values are missing expected values");
					Reporting.reportStep(rowNum, "Failed", "Exception found in >> @method = verify_ActionMenu  and @class = ActionMenu " , "", "holding_actionMenu", driver, "", "", null);
				}
				//Click and verify Trade Widget links
				verifyTradeWidget(rowNum, expTradeLinks, resltRow, reqColNum, symbol);
				
				
				//Click and verify other option pages
				verify_QQ_otherPageLinks(rowNum, secValue, expOptionLinks, resltRow, reqColNum, symbol);
				
				System.out.println("###############################################################");
				System.out.println("###############################################################");
		}catch(Exception e) {
			Reporting.reportStep(rowNum, "Failed", "Exception found in >> @method = verify_ActionMenu  and @class = ActionMenu " , "", "holding_actionMenu", driver, "", "", null);
			System.out.println("Exception found in >> @method = verify_ActionMenu  and @class = ActionMenu");
			e.printStackTrace();
		}
		
	}//end of function
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - click_QuickQuote_VerifyPopup
	//Description - This function verifies Quick Quote popup opened up.
	//Argument - row no:, symbol 
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean clickAndVerify_AMPopup(int rowNum, String symbol) throws Exception {
		try{// find the row with symbol and click on the Action Menu
			TimeUnit.SECONDS.sleep(5);
			WM_CommonFunctions.wait_ForPageLoad(rowNum);
			//usb_am_icon1
			clickActionMenu(rowNum, symbol, "Symbol");
			TimeUnit.SECONDS.sleep(5);
			 WM_CommonFunctions.wait_ForPageLoad(rowNum);
			//if action menu is missing or does not have data >> Fail the test case and return
			if(CommonUtils.isElementPresent(am_dropdown)) {
				//Action Menu has successfully opened up
				Reporting.reportStep(rowNum, "Passed", "Action Menu window has successfully opened up." , "", "holding_actionMenu", driver, "", "", null);
				System.out.println("Passed - Action Menu window has successfully opened up for symbol - " + symbol);
			}else {
				Reporting.reportStep(rowNum, "Failed", "Action Menu window did not open successfully for symbol - " + symbol + "." , "", "holding_actionMenu", driver, "", "", null);
				System.out.println("Failed - Action Menu window did not open for symbol  =>> " + symbol);
				return false;
			}
			
			return true;
		}catch(Exception e) {
			Reporting.reportStep(rowNum, "Failed", "Exception found in >> @method = clickAndVerify_AMPopup  and @class = ActionMenu " , "", "holding_actionMenu", driver, "", "", null);
			System.out.println("Exception found in >> @method = clickAndVerify_AMPopup  and @class = ActionMenu");
			e.printStackTrace();
			return false;
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_QQ_otherPageLinks
	//Description - This function verifies links of other pages in Quick Quote dropdown.
	//Argument - row no:, sec value, exp page links, test row no, test col num, symbol 
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	
	void verify_QQ_otherPageLinks(int rowNum, String secValue, String expOptionLinks, int resltRow, int reqColNum, String symbol) throws Exception {
		try {
			String[] expPageLink = expOptionLinks.split(";");
			for(String item : expPageLink) {
				
				WebElement reqListItem = driver.findElement(By.cssSelector(".dropdown-menu.dropdown-menu-right li a[aria-label='" + item + "']"));
				reqListItem.click();
				//verify Page
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				//get page title
				TimeUnit.SECONDS.sleep(5);
				verify_PageTitle_And_Symbol(rowNum, item, symbol);
				
				//Navigate Back
				if(item.equals("Holdings Zoom")) {
					TimeUnit.SECONDS.sleep(3);
					hp.HZoom_Modal_Close.click();
				}
					
				driver.navigate().back();
				TimeUnit.SECONDS.sleep(3);
				if(item.equals("Holdings Zoom")) {
					driver.navigate().forward();
					WM_CommonFunctions.wait_ForPageLoad(rowNum);
				}
				ResultTable_Section.apply_DropDownFilter(rowNum, "All Asset Types", secValue);
				TimeUnit.SECONDS.sleep(5);
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				
				//open Action Menu again
				clickActionMenu(rowNum, symbol, "Symbol");
				TimeUnit.SECONDS.sleep(3);
			}//end of for
		}catch(Exception e) {
			Reporting.reportStep(rowNum, "Failed", "Exception found in >> @method = verify_QQ_otherPageLinks  and @class = ActionMenu " , "", "holding_actionMenu", driver, "", "", null);
			System.out.println("Exception found in >> @method = verify_QQ_otherPageLinks  and @class = ActionMenu");
			e.printStackTrace();
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_PageTitle_And_Symbol
	//Description - This function verifies page title and symbol on all options pages from Action Menu.
	//Argument - row no:, page name, symbol
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void verify_PageTitle_And_Symbol(int rowNum, String optionVal, String symbol) throws InterruptedException {
		try {
			//verify page title
			SummaryPage SummaryPage = new SummaryPage();
			String expTitle = "";
			String actualTitle;
			switch(optionVal) {
			case "Chain":
				expTitle = "Option Chains";
				break;
			case "Stock Report":
				expTitle = "Stock Fundamentals Report";
				break;
			case "Chart":
				expTitle = "Interactive Chart";
				break;
			case "Fund Report":
				expTitle = "Mutual Fund / ETF Fundamentals Report";
				break;
			case "Holdings Zoom":
				expTitle = "Holdings Zoom";
				break;
			}//end of switch
			TimeUnit.SECONDS.sleep(3);
			 WM_CommonFunctions.wait_ForPageLoad(rowNum);
			// WM_CommonFunctions.wait_dynamicFluentWait( rowNum,  SummaryPage.page_Title,  "Page Title");
			 
			 if(optionVal.equals("Holdings Zoom")) {
				actualTitle = hp.HZoom_Modal_PageTitle.getText().trim();
			}else {
				actualTitle = (SummaryPage.page_Title.getText()).trim();
			}
			if(expTitle.equals(actualTitle)) {
				Reporting.reportStep(rowNum, "Passed", "Action Menu - Page title is correct for >> " + optionVal , "", "", driver, "", "", null);
				System.out.println("Passed : Action Menu - Page title is correct for >> " + optionVal);
			}else {
				Reporting.reportStep(rowNum, "Failed", "Action Menu - Page title is incorrect for >> " + optionVal + ". Expected/Actual = " + expTitle + "/" + actualTitle, "", "holding_actionMenu", driver, "", "", null);
				System.out.println("Failed :Action Menu - Page title is incorrect for >> " + optionVal);
				//return;
			}
			
			//verify symbol
			
			String actualSymbol = "";
			switch(optionVal) {
			case "Chain":
				actualSymbol = symbol_OptionChains.getText();
				break;
			case "Stock Report":
			case "Fund Report":
				actualSymbol = symbol_Report.getAttribute("data-value");
				//System.out.println("Actual Symbol = " + actualSymbol);
				break;
			case "Chart":
				actualSymbol = symbol_Charts.getText();
				break;
			case "Holdings Zoom":
				actualSymbol = hp.am_HZoom_Symbol.getText();
				break;
			}
			//System.out.print("exp Symbol = " + symbol);
			if(actualSymbol.equals(symbol)) {
				Reporting.reportStep(rowNum, "Passed", "Correct Symbol displayed." , "", "", driver, "", "", null);
				System.out.println("Passed - Correct symbol is displayed in given page.");
			}else {
				Reporting.reportStep(rowNum, "Failed", "Symbol Value doest match. Actual/Expected = "+  actualSymbol + "/"  + symbol, "", "holding_actionMenuSymbol", driver, "", "", null);
				System.out.println("Failed - Expected symbol is not displayed Actual / Exp - " + actualSymbol + "/" + symbol);
			}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception thrown in method - verify_PageTitle_And_Symbol in @ ActionMenu");
			Reporting.reportStep(rowNum, "Failed", "Exception thrown in method - verify_PageTitle_And_Symbol", "", "holding_actionMenu", driver, "", "", null);
		}//end of try-catch
	}//end of function
	//######################################################################################################################
	//######################################################################################################################
	//Name - verifyTradeWidget
	//Description - This function verifies each options for Trade Widget window.
	//Argument - row no, exp Trade links to be found, test row, test col, symbol
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	void verifyTradeWidget(int rowNum, String expTradeLinks, int resltRow, int reqColNum, String symbol) throws Exception {
		String[] expTrade = expTradeLinks.split(";");
		
		for(String item : expTrade) {
			
			WebElement reqListItem = driver.findElement(By.cssSelector(".dropdown-menu.dropdown-menu-right li a[aria-label='" + item + "']"));
			reqListItem.click();
			
			//verify trade Widget
			verify_TradeWidget_Popup(rowNum, item, symbol);
			
			//close trade widget
			tw.tw_close.click();
			TimeUnit.SECONDS.sleep(3);
			
			//open Action Menu again
			clickActionMenu(rowNum, symbol, "Symbol");
			TimeUnit.SECONDS.sleep(3);
			
			
		}
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_TradeWidget_Popup
	//Description - This function verifies Trade Widget pop-up window fields.
	//Argument - row no, exp Action value, symbol
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	void verify_TradeWidget_Popup(int rowNum, String expActionVal, String symbol) throws Exception{
		try{
			//verify trade widget window opens
			WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tw.tw_popup_TradeWidget, "Action menu - Trade widget popup");
			if(CommonUtils.isElementPresent(tw.tw_popup_TradeWidget)){
				Reporting.reportStep(rowNum, "Passed", "Trade Widget has successfully opened up." , "", "", driver, "", "", null);
				System.out.println("Passed - Trade widget window has successfully opened up for symbol - " + symbol);
			}else {
				Reporting.reportStep(rowNum, "Failed", "trade widget widnow did not open successfully for symbol - " + symbol + "." , "", "holding_actionMenu", driver, "", "", null);
				System.out.println("Failed - Trade widget did not open for symbol  =>> " + symbol);
				return;
			}
			//verify symbol is correct
			if(tw.tw_Symbol.getText().equals(symbol)) {
				Reporting.reportStep(rowNum, "Passed", "Trade Widget symbol is correct" , "", "", driver, "", "", null);
				System.out.println("Passed - Trade widget window dispays correct symbol - " + symbol);
			}else {
				Reporting.reportStep(rowNum, "Failed", "trade widget window does not display expected symbol - " + symbol + "." , "", "actionMenu_TW_Action", driver, "", "", null);
				System.out.println("Failed - Trade widget does not display expected symbol  =>> " + symbol);
			}
			//verify Action = Buy, Sell, Sell All etc is correct
			String currActionName = tw.tw_Action.getText();
			//sell all will only display sell
			if(expActionVal.contains(currActionName)){
				Reporting.reportStep(rowNum, "Passed", "Trade Widget Action is correct - " + expActionVal, "", "", driver, "", "", null);
				System.out.println("Passed - Trade widget window dispays correct Action - " + expActionVal);
			}else {
				Reporting.reportStep(rowNum, "Failed", "Trade widget window does not display expected Action - " + expActionVal + "." , "", "actionMenu_TW_Action", driver, "", "", null);
				System.out.println("Failed - Trade widget does not display expected Action  =>> " + expActionVal);
			}
		}//end of try-catch
		catch(Exception e) {
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", "verify_TradeWidget_Popup - exception found" , "", "actionMenu_TW_Action", driver, "", "", null);
			System.out.println("Failed - Exception found in @verify_TradeWidget_Popup");
		
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - verifyStringsMatchesList
	//Description - This function clicks on Action Menu for a given symbol
	//Argument - symbol, col name
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	
	boolean verifyStringsMatchesList(String expValueList, List<String> actValList){
		if(expValueList==null || actValList == null || expValueList=="") {
			System.out.println("method - verifyStringsMatchesList >> List to compare are null. Please check!!");
			return false;
		}
		System.out.println("Exp = " + expValueList);
		//System.out.println("Actual = " + destList1);
		List actualList = actValList;
		List<String> sourceLst = Arrays.stream(expValueList.split(";")).collect(Collectors.toList());
		Collections.sort(sourceLst);
	    Collections.sort(actValList); 
	    
	    System.out.println("Expected options = " + sourceLst.toString());
		System.out.println("Actual options = " + actValList.toString());
		return sourceLst.equals(actValList);
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - clickActionMenu
	//Description - This function clicks on Action Menu for a given symbol
	//Argument - symbol, col names
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void clickActionMenu(int rowNum, String strSymbol, String colName) throws Exception {
		TimeUnit.SECONDS.sleep(5);
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		int colNum =  ResultTable_Section.get_ColIndex(colName);
		int colCount = ResultTable_Section.get_ColCount();
		int resltRow = ResultTable_Section.get_rowNum_ForMatchingValue(strSymbol, colNum);
		System.out.println(colCount + " / " + resltRow);
		WebElement symbolIcon = driver.findElement(By.cssSelector("table tbody tr:nth-child(" + resltRow + ") td:nth-child(" + colCount + ") .btn"));
		symbolIcon.click();
		
	}//end of function
	//######################################################################################################################
	//######################################################################################################################
	//Name - clickActionMenu
	//Description - This function clicks on Action Menu for a given symbol
	//Argument - symbol, col name
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public List<String> get_eleList(int resltRow, int reqColNum) {
		//int colNum =  ResultTable_Section.get_ColIndex(colName);
		//int colCount = ResultTable_Section.get_ColCount();
		//int resltRow = ResultTable_Section.get_rowNum_ForMatchingValue(strValue, colNum);
		System.out.println(resltRow + " /" + reqColNum);
		List<WebElement> reqList = driver.findElements(By.xpath("//table/tbody/tr[" + resltRow + "]/td[" + reqColNum +"]/div/ul/li/a[@aria-label!= '']"));
		List<String> strList = new ArrayList<String>();
		for(WebElement b : reqList) {
			if(b.getText().trim()==null || b.getText().trim()=="" || b.getText()==" ") {
				reqList.remove(b);
			}else {
				strList.add(b.getText());
			}
		}
		System.out.println("Converted String list > " + strList.toString());
		return strList;
	}//end of function
//######################################################################################################################
//######################################################################################################################
	

}//end of class
