package PageObjects.WM;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class OrderStatus_Page {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	
	// ************* Filter dropdowns Elements ************************************
	//***************************************************************************
	//Panel to be used for screenshot highlight
	@FindBy(how = How.CSS, using = "#pageTitle>.form-inline")
	public WebElement panel_AllFilters;
	
	
	// Alert message 
	@FindBy(how = How.XPATH, using = ".//*[@id='pageBody']/div[9]/div/div/div[1]/div/div/div[1]/div[1]/p")
	public WebElement alert_NoResult_Message;
	
	//--------------  Constructor to initialize all elements  --------------
	public OrderStatus_Page(){
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }


	//########################     FUNCTIONS       #############
	//Description - This functions sets the object for screenshot purpose. If Filter panel 
	//itself is not present, then null object is returned in which case full page screenshot is taken.
	//###########################################################
	public  WebElement Set_ParentObjForImg() throws Exception{
		if(CommonUtils.isElementPresent(panel_AllFilters) == false){
			panel_AllFilters = null;
		}
		return panel_AllFilters;
	}
	
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - checkAllNavTabs_arePresent
	//Description - This function verifies that all parent navigation tabs are present.
	//Argument - row no: of test data
	//Returns - This function returns a String variable with the names of all failed / missing tabs seprated by semi colon.
	//Date - Oct 2017
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean checkFilters_ValueAndFunctioning( int rowNum, String strKeyword, String strFilterName, String strAllAssetTypeValue, String strStatusesValue, String strAllOrderTypeValue, String strClient) throws Exception{
		//Set screenshot object as Filter panel
		WebElement screenshot_Object = Set_ParentObjForImg();
		try{
			//wait for page title to load
			
			wait = new WebDriverWait(driver,15);
			wait.until(ExpectedConditions.titleContains("Order Status - Investor"));
			System.out.println("Expected Page title has loaded for Order Status page.");
			System.out.println("Order Status Page Filter test   |     Filter name = " + strFilterName + "    |    For Row = " + rowNum);
			
			//###################################################################################################################################
				//Checkpoint 1 - If keyword is UI Test, verify drop down filters are present with expected values.
				//Test case will fail if a filter is missing or has incorrect/missing dropdown value.
			//###################################################################################################################################
			
			List<WebElement> strDropDown = null;
			WebElement DropdownBtn = null;
			String strExpectedDropDownValues = "";
			ResultTable_Section ResultTable_Section = new ResultTable_Section();
			//Assigning drop down button and list for UI test case
			if(strFilterName.equals("All Asset Types")){
				strDropDown = ResultTable_Section.dd_AllAssetTypes;
				DropdownBtn = ResultTable_Section.ddTab_AllAssetTypeFilter;
				strExpectedDropDownValues = strAllAssetTypeValue;
			}else if(strFilterName.equals("All Statuses")){
				strDropDown = ResultTable_Section.dd_AllStatuses;
				DropdownBtn = ResultTable_Section.ddTab_AllStatusesFilter;
				strExpectedDropDownValues = strStatusesValue;
			}else if(strFilterName.equals("All Order Types")){
				strDropDown = ResultTable_Section.dd_AllOrderTypes;
				DropdownBtn = ResultTable_Section.ddTab_AllOrderTypesFilter;
				strExpectedDropDownValues = strAllOrderTypeValue;
			}//End of IF-Else
			
			//Verifying drop down options for keyword = UI test
			if(strKeyword.equals("UI Test")){
				//get actual dropdown values and expected dropdown values
				String strActualDropDownOption = WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, strDropDown, DropdownBtn);
				System.out.println("Order Status page - Actual DropDown option for " + strFilterName + " = " + strActualDropDownOption);
				System.out.println("Order Status Page - Expected DropDown option for " + strFilterName + " = " + strExpectedDropDownValues);
				//compare and set result
				if(StringManipulation.compareString_Match(strActualDropDownOption, strExpectedDropDownValues)){
					System.out.println("Order Status Page - Filter UI test has Passed for - "+ strFilterName);
					Reporting.reportStep(rowNum, "Passed", strFilterName + " dropdown list has expected values - " + strActualDropDownOption, "", "", driver, "", "", null);
				}else{
					System.out.println("Order Status Page - Filter UI test has Failed for - "+ strFilterName);
					if(strActualDropDownOption.equals("")){
						Reporting.reportStep(rowNum, "Failed", strFilterName + " dropdown is empty/missing. ", "", "Page_ddCheck", driver, "", "", screenshot_Object);
					}else{
						Reporting.reportStep(rowNum, "Failed", strFilterName + " dropdown value check failed. Actual // Expected = " + strActualDropDownOption + "//" + strExpectedDropDownValues, "", "Page_ddCheck", driver, "", "", screenshot_Object);
					}
				}//End of IF-Else
			}//End of IF
			//###################################################### END of 1st Checkpoint ######################################################
			//###################################################################################################################################
			
			//###################################################################################################################################
				//Checkpoint 2 - If keyword is not UI Test then apply the filter value for one or multiple combination of filters and verify 
				//result table has only those rows that match or qualify applied filter. 
				//Test case will fail if a filter is missing OR filter value to be applied is missing OR no results are displayed in result table.
			//###################################################################################################################################
			else{
				//###################################################################################################################################
													//Apply filters >> Asset, Term or Year
				//###################################################################################################################################
				//Asset filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Asset Types", strAllAssetTypeValue)){
					 System.out.println("All Asset Types = " + strAllAssetTypeValue + " >> dropdown filter could not be applied. Skiping further execution.");
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF
				 //Term filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Statuses", strStatusesValue)){
					 System.out.println("All Term Types = " + strStatusesValue + " >> dropdown filter could not be applied. Skiping further execution.");
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF
				 //Year Filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Order Types", strAllOrderTypeValue)){
					 System.out.println("Year = " + strAllOrderTypeValue + " >> dropdown filter could not be applied. Skiping further execution.");
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF			 
				//###################################################################################################################################
				//###################################################################################################################################
				
				//###################################################################################################################################
				 			// Wait for page load, check for no result message and then expand all rows.
				//###################################################################################################################################
				//Wait for page to load with expected result table rows, in case spinning "loading" icon is displayed in UI
				 Thread.sleep(2000);
				 WM_CommonFunctions.wait_ForPageLoad(rowNum);
					
					
				//Check if "No Results found" is displayed and thus result table is missing.Add Failed to the report.
				 Thread.sleep(1000);
				 String msg = alert_NoResult_Message.getText();
				if(alert_NoResult_Message.getText().equalsIgnoreCase("There are no orders to display.")){
					System.out.println("Order Status Page - No result found for applied filter testcase- " + strFilterName);
					Reporting.reportStep(rowNum, "Failed", "No Result displayed for this Filter -  " + strFilterName , "", "OrS_ddCheck", driver, "", "", screenshot_Object);
					return false;	
				}//End of IF
				 //Expand all rows in result table.
				ResultTable_Section.expandAll_Rows(rowNum);
				//###################################################################################################################################
				//###################################################################################################################################
				
				//###################################################################################################################################
							//Test all rows for - Asset Type, Term Type and Year filter test if applicable
				//###################################################################################################################################
				//Note - Asset type filter cannot be verified for USB as it does not have a column for it in Result table.
				//###################################  verify All Asset Type filter  ###################################
				if((strFilterName.contains("All Asset Types"))&&(!strClient.equalsIgnoreCase("USB"))){
					boolean flg_FilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Security Type", strAllAssetTypeValue);
					if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "All rows for Asset Type col, have applied expected value = " + strAllAssetTypeValue, "", "", driver, "", "", null);
						System.out.println("PASSED  >>  Order Status page - All rows have same value for col - Asset Types ==>> " + strAllAssetTypeValue);
					}else{
						System.out.println("FAILED  >>  Order Status Page - All rows for Asset type column, do not have expected applied filter value -  "+ strAllAssetTypeValue);
						Reporting.reportStep(rowNum, "Failed", "All Asset type column values do not have expected applied filter value -  "+ strAllAssetTypeValue , "", "OrderStatus_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-Else
				}//End of IF
				if((strFilterName.contains("All Asset Types"))&&(strClient.equalsIgnoreCase("USB"))){
					Reporting.reportStep(rowNum, "Not Executed >> Asset type cannot be verified in USB.", "", "", "", driver, "", "", null);
				}
				////###################################  verify All Term Type filter  ###################################
				if(strFilterName.contains("All Statuses")){
					//******check DateDiff is 1yr or more******
					boolean flg_TermFilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Status", strStatusesValue);
					if(flg_TermFilterPass){
						Reporting.reportStep(rowNum, "Passed", "All displayed rows match the applied Term per Acquired date i.e. = " + strStatusesValue, "", "", driver, "", "", null);
						System.out.println("PASSED  >>  Order Status Page - All displayed rows match the applied Term per Acquired date i.e. = " + strStatusesValue);
					}else{
						System.out.println("FAILED  >>  Order Status Page - All row for Term type column do not match the applied  filter value -  "+ strStatusesValue);
						Reporting.reportStep(rowNum, "Failed", "All Statuses type column values do not have expected applied filter value ="+ strStatusesValue , "", "OrderStatus_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-else
				}//End of IF
				//###################################  verify Year filter  ###################################
				if(strFilterName.contains("All Order Types")){
					//column name per client name
					String colName_OrderType = "";
					if(strClient.equalsIgnoreCase("STF")){
						colName_OrderType ="Price";
					}else{
						colName_OrderType = "Order Type";
					}
					boolean flg_FilterPass = false;
					if(strClient.equalsIgnoreCase("USB")){
						flg_FilterPass = check_OrderType(strClient, rowNum, colName_OrderType, strAllOrderTypeValue);
					}else{
						flg_FilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, colName_OrderType, strAllOrderTypeValue);
					}
						if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "All rows for match the applied filter for value = " + strAllOrderTypeValue, "", "", driver, "", "", null);
						System.out.println("PASSED  >>  Order Status page - All rows match the applied filter on col - " + strAllOrderTypeValue);
					}else{
						System.out.println("FAILED  >>  Order Status Page - all rows do not have values per expected applied filter value -  "+ strAllOrderTypeValue);
						Reporting.reportStep(rowNum, "Failed", "All Order type column values do not have expected applied filter value -  "+ strAllOrderTypeValue , "", "OrderStatus_ddCheck", driver, "", "", screenshot_Object);	
					}//End of IF-Else
				}//End of IF
				//###################################################################################################################################
				//###################################################################################################################################
				
			}//End of ELSE
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Exception thrown. Order Status page filter could not be verified for row - " + rowNum, "", "OrderStatus_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>checkFilters_ValueAndFunctioning, class = ResultTable_Section. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}

	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - check_OrderType
	//Description - This function verifies Order type filter applied specifically for USB whihc is different from generic check.
	//Argument - row no: of test data
	//Returns - true is the resulttables rows matc applied filter.
	//Date - Feb 2018
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
		
	public boolean check_OrderType(String strClient, int rowNum, String colName, String orderTypeValue ){
		ResultTable_Section ResultTable_Section = new ResultTable_Section();
		//check for client name
		if(strClient.equalsIgnoreCase("USB")){
			//Initialize variables
			int i = 0;
			boolean resultOptionsList= false;
			String failedRows = "";
			int colNum =  ResultTable_Section.get_ColIndex(colName);
			//Create a list of values from all rows in result table for given column.
			List list_ActualColValues= driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ")"));
			List<WebElement> allOptions = list_ActualColValues;
			String actual_RowValue = "";
			
			//Create array for multiple checkbox-ed selection, Example with a column can have 2 Order types - Stop and Market
			String[] strSelectOptions;
			if (orderTypeValue.contains(";")) {
				strSelectOptions = orderTypeValue.split(";");
			}else{
				strSelectOptions = new String[1];
				strSelectOptions[0] = orderTypeValue;
			}
			//apply for loop to ierate over each row
			for(WebElement option:allOptions){
				actual_RowValue = option.getText().trim();
				//apply For loop to iterate over each checkbox selection if it any of the selected ones.
				for(String strFilterOption:strSelectOptions){
					if(strFilterOption.equalsIgnoreCase("Limit")){
						
					}else if(strFilterOption.equalsIgnoreCase("Limit")){
						
					}
					//replace actua row value with Order Type value using contains as UI has numeric text as well.
					switch (strFilterOption){
					case "Limit":
						if(actual_RowValue.contains("Limit")&& !actual_RowValue.contains("Stop")){
							actual_RowValue = "Limit";
						}
						break;
					case "Stop":
						if(!actual_RowValue.contains("Limit")&& actual_RowValue.contains("Stop")){
							actual_RowValue = "Stop";
						}
						break;
					case "Stop Limit":
						if(actual_RowValue.contains("Limit")&& actual_RowValue.contains("Stop")){
							actual_RowValue = "Stop Limit";
						}
						break;
					case "Market":
							actual_RowValue = "Market";
							break;
					case "Market On Close":
						actual_RowValue = "Market On Close";
						break;
					}
					//Match and verify the value
					if(actual_RowValue.equalsIgnoreCase(strFilterOption.trim())){
						resultOptionsList = true;
						break;
					}
				}//End of Nested For
				if(!resultOptionsList){
					//add failed row number to this variable for reporting purpose
					failedRows = failedRows + ";" + Integer.toString(i);
					System.out.println("Col value does not match with applied filter - Result table Row value = "+ actual_RowValue + " || BUT Filter applied = " + orderTypeValue);
				
				}//End of IF
				//increment counter for row number 
				i++;
			}//End of Outer For
			return resultOptionsList;
		}//End of Client name If
		
		return false;
		
	}
	//************************  End of functions **********************************
	//************************************************************************************************************
}
 