package PageObjects.WM;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class STF_NavigationPanel {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	// ************* Top Navigation Elements ************************************
	//***************************************************************************
	//main navigation panel and element used for screenshot
	@FindBy(how = How.ID, using = "privateNav")
	public WebElement panel_Navigation;
	

	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Home')]")
	public WebElement tab_Home;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Accounts')]")
	public WebElement tab_Accounts;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Trading')]")
	public WebElement tab_Trading;
	
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'My Net Worth')]")
	public WebElement tab_MyNetWorth;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Quotes & Markets')]")
	public WebElement tab_QuotesAndMkt;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Research')]")
	public WebElement tab_Research;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Preferences')]")
	public WebElement tab_Prefrences;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'eDocuments')]")
	public WebElement tab_eDocuments;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Cash Management')]")
	public WebElement tab_CashMgmnt;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'eBill')]")
	public WebElement tab_eBill;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Contact Us')]")
	public WebElement tab_ContatUs;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Logout')]")
	public WebElement tab_Logout;
	
	
	//Dropdown objects for wait purpose
//	@FindBy(how = How.CSS, using = ".nav-accounts-history")
//	public WebElement obj_dd_Account;

	//*************************  DropwDown List  ***********************************
	//Navigation panel list
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li")
	public List<WebElement> dd_NavPanel_List;
	
	
	@FindBy(how = How.CSS, using = ".nav-accounts li")
	public List<WebElement> dd_Accounts;
 
	@FindBy(how = How.CSS, using = ".nav-trading li")
	public List<WebElement> dd_Trading;
	
	@FindBy(how = How.CSS, using = ".nav-eDocs li")
	public List<WebElement> dd_eDocs;
	
	//*************************  DropwDown options  ***********************************
	@FindBy(how = How.LINK_TEXT, using = "Portfolio At A Glance")
	public WebElement ddItem_Portfolio;
	
	
	
	//--------------  Constructor to initialize all elements  --------------
	public STF_NavigationPanel(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	SummaryPage SummaryPage = new SummaryPage();
//*************************************************   FUNCTIONS   **********************************************************
//**************************************************************************************************************************
	
		
	//######################################################################################################################
	//######################################################################################################################
	//Name - STF_ActualNavigationPanelDDList
	//Description - This function returns a string of all actual dropdown values for given field .
	//Parameters - dropdown list object, element to click for dropdown to be displayed.
	//Returns - This function returns a String variable containing all Actual dropdown values from ui.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String STF_ActualNavigationPanelDDList(List ddList, WebElement toClick) throws InterruptedException{
		toClick.click();
		Thread.sleep(1000);
		String resultOptionsList="";
		List<WebElement> allOptions = ddList;
		String valueTest = "";
		for(WebElement option:allOptions){
			valueTest = option.getText().trim();	
			if(StringManipulation.is_StringNotEmpty(valueTest)){
				resultOptionsList = resultOptionsList  +  valueTest + ";";	
			}//End of Nested If
		}//End of For Loop
		resultOptionsList = StringManipulation.string_RemoveFromStringEnd(resultOptionsList, ";");
		System.out.println("Actual values = " + resultOptionsList);
		return resultOptionsList;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - STF_ExpectedNavigationPanelDDList
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - Dropdown name under test.
	//Returns - This function returns a String variable containing all Actual dropdown values from UI.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String STF_ExpectedNavigationPanelDDList(String strDropdownName, String tradingValues){
		String strExpValues = "";
		switch (strDropdownName){
		case "Accounts":
			strExpValues = "Portfolio At A Glance;Balances;Holdings;Holdings Zoom;Activity;Unrealized Gain/Loss;Realized Gain/Loss;Asset Allocation;Household Overview;Projected Monthly Income;Income Summary;Download;Awards;Profile;View Orders";
			break;
		case "Tradings":
			if(tradingValues.equalsIgnoreCase("")){
				strExpValues = "Stock;Order Status";
			}else{
				strExpValues = tradingValues;
			}
			break;
		case "eDocuments":
			strExpValues = "Delivery Preferences;Statements;Confirms;Tax Forms;Advisory Reports;Check Images;Third-Party Disbursements";
			break;
		default:
			strExpValues = "";
			break;
		}
		//System.out.println(strActualValues);
		return strExpValues;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - STF_CompareNavigationPanelDDList
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - row num, dropdown name, dropdown list object, object that displas dropdown on click.
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################	
	public void STF_CompareNavigationPanelDDList(int rowNum, String strDropdownName, List ddList, WebElement toClick, String expValues) throws Exception {
		String expectedValue = STF_ExpectedNavigationPanelDDList(strDropdownName, expValues).trim();
		String actualValue = STF_ActualNavigationPanelDDList(ddList, toClick).trim();
		if(StringManipulation.compareString_Match(actualValue, expectedValue)){
			Reporting.reportStep(rowNum, "Passed", strDropdownName + " dropdown list has expected values", "", "", driver, "", "", null);
		}else{
			Reporting.reportStep(rowNum, "Failed", strDropdownName + " dropdown value check failed. Actual // Expected = " + actualValue + "//" + expectedValue, "", "Failed_Home_NavTab", driver, "", "", panel_Navigation);
			
		}
		
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - STF_Check_AllNavigationPanelObjects
	//Description - Parent function that verifies all navigation panel element in STF .
	//Parameters - row num
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################	
	public void STF_Check_AllNavigationPanelObjects(int rowNum, String tradingValues){
		try{
			WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
			//panel_Navigation = WM_CommonFunctions.Set_ParentObjForImg(panel_Navigation);
			WebElement[] arrBrandingElements = new WebElement[]{
					tab_Home,tab_Accounts,tab_Trading,tab_QuotesAndMkt,tab_Research,tab_Prefrences,tab_eDocuments,tab_CashMgmnt,tab_eBill,tab_ContatUs,tab_Logout};
			String[] arrPanelKeyword = new String[]{"Home", "Accounts", "Trading", "Quotes and Mkt", "Research", "Preferences", "eDocuments", "Cash Management", "eBill", "Contact Us", "Logout"}; 
			System.out.println("Checking nav. panel tabs for STF");
			for(int i = 0; i< arrBrandingElements.length; i++){
				//String elementName = (arrBrandingElements[i]).getText();
				if(!(CommonUtils.isElementPresent(arrBrandingElements[i]))){
					System.out.println( arrPanelKeyword[i] + " - Tab not found on navigation panel - ");
					Reporting.reportStep(rowNum, "Failed", arrPanelKeyword[i] + "-  is missing from navigation panel.", "", "STF_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					
				}else{
					System.out.println(arrPanelKeyword[i] + " tab has passed");
					Reporting.reportStep(rowNum, "Passed", arrPanelKeyword[i] + " - tab is present.", "", "", driver, "", "", null);
				}//End of If-Else
				rowNum = rowNum + 1;
			}//End of FOR
			
			//Check dropdown list for Accounts, trading and eDocuments
			if(CommonUtils.isElementPresent(tab_Accounts)){
				STF_CompareNavigationPanelDDList(rowNum, "Accounts", dd_Accounts, tab_Accounts, "");
			}else{
				Reporting.reportStep(rowNum, "Failed", "Accounts tab is missing. cant check dropdowns", "", "STF_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
			rowNum = rowNum + 1;
			if(CommonUtils.isElementPresent(tab_Trading)){
				STF_CompareNavigationPanelDDList(rowNum, "Tradings", dd_Trading, tab_Trading, tradingValues);}
			else{
				Reporting.reportStep(rowNum, "Failed", "Tradings tab is missing. cant check dropdowns", "", "STF_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
			rowNum = rowNum + 1;
			if(CommonUtils.isElementPresent(tab_eDocuments)){
				STF_CompareNavigationPanelDDList(rowNum, "eDocuments", dd_eDocs, tab_eDocuments, "");}
			else{
				Reporting.reportStep(rowNum, "Failed", "eDocuments tab is missing. cant check dropdowns", "", "STF_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
				
		}//End of TRY
		catch(Exception e){
			System.out.println("N - Excpetion found in method - USB_Check_AllBrandingPanelObjects");
			e.printStackTrace();
			}//End of Catch
	}//END OF FUNCTION
	//######################################################################################################################
	//######################################################################################################################
	//Name - check_PageNavigation_viaNavPanel
	//Description -This function verifies that a page loads successfully when navigated via Navigation panel
	//Parameters - row num, page name
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
		//######################################################################################################################
	public void check_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception {
		// TODO Auto-generated method stub
		//SummaryPage SummaryPage = new SummaryPage();
		wait = new WebDriverWait(driver,40);
		String pageTitle = "";
		strPageName = strPageName.trim();
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		//Switch stmnt for each page option
		switch (strPageName){
		//navigate to the page
		case "Account Summary":
			Thread.sleep(500);
			WM_CommonFunctions.clickItem(tab_Home, "Dropdown", rowNum);
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();
			break;
		//########  Start of Dropdowns for Accounts  ########
		case "Portfolio At A Glance":			
		//For all the option below, same code will work thus the required code has been added under case "View Orders"	
		case "Balances":case "Holdings":case "Holdings Zoom":case "Activity":case "Unrealized Gain/Loss": case "Realized Gain/Loss":
		case "Asset Allocation":case "Household Overview":case "Projected Monthly Income":case "Income Summary":
		case "Download":case "Profile":case "View Orders":
			Thread.sleep(100);
			//wait for Accounts tab
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_Accounts, "Page Nav - Account Tab ")){
				Reporting.reportStep(rowNum, "Failed", "Account Details navigation panel object is not visible.", "", "PageNavg_AcntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Account Details tab not found. Too slow to load.");
			}
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Accounts, strPageName, tab_Accounts, "Account", "Click", "USB")){
				pageTitle = "";
			}else{
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();
			}
			/*WM_CommonFunctions.clickItem(tab_Accounts, "Dropdown", rowNum);
			Thread.sleep(60);
			
			//try 3 times if option under test is not present
			for(int i = 1; i<=3; i++ ){
				if(!WM_CommonFunctions.dropwdown_IsItemPresent_InList(rowNum, dd_Accounts, strPageName)){
					WM_CommonFunctions.clickItem(tab_Accounts, "Dropdown", rowNum);
					Thread.sleep(30);
				}else{
					break;
				}
			}
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Accounts, strPageName);*/
			/*wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();*/
			break;
		case "Awards":
			Thread.sleep(100);
			/*if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_Accounts, "Page Nav - Account Tab ")){
				Reporting.reportStep(rowNum, "Failed", "Account Details navigation panel object is not visible.", "", "PageNavg_AcntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Account Details tab not found. Too slow to load.");
			}*/
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Accounts, strPageName, tab_Accounts, "Account", "Click", "USB")){
				pageTitle = "";
			}else{
				if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, SummaryPage.page_Awards_Cart, "Page Nav - Awards Profile bar ")){
					Reporting.reportStep(rowNum, "Failed", "Awards Profile bar object is not visible.", "", "PageNavg_Awards", driver, "", "", null);	
					System.out.println("FAILED  >> Page Navigation - Awards Profile bar not found. Too slow to load.");
					pageTitle = "Slow Loading";
				}
				pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Awards_Cart, rowNum)).trim();
			}
			//wait for Accounts tab
			/*if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_Accounts, "Page Nav - Account Tab ")){
				Reporting.reportStep(rowNum, "Failed", "Account Details navigation panel object is not visible.", "", "PageNavg_AcntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Account Details tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_Accounts, "Dropdown", rowNum);
			Thread.sleep(60);
			
			if(!WM_CommonFunctions.dropwdown_IsItemPresent_InList(rowNum, dd_Accounts, strPageName)){
				Thread.sleep(100);
				WM_CommonFunctions.clickItem(tab_Accounts, "Dropdown", rowNum);
			}
			
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Accounts, strPageName);
			Thread.sleep(30);*/
			/*if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, SummaryPage.page_Awards_Cart, "Page Nav - Awards Profile bar ")){
				Reporting.reportStep(rowNum, "Failed", "Awards Profile bar object is not visible.", "", "PageNavg_Awards", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Awards Profile bar not found. Too slow to load.");
				pageTitle = "Slow Loading";
			}
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Awards_Cart, rowNum)).trim();*/
			System.out.println("Awards page cart title = " + pageTitle);
			break;
		//END of Accounts drop down  list	
		//########  Start of Dropdowns for Tradings  ########
		case "Stock":case "Order Status":
			//Unit test >>check test case wen this tab is not present so we can see if iteration got skipped if click failed	
			Thread.sleep(100);
			/*if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_Trading, "Page Nav - Trading Tab ")){
				Reporting.reportStep(rowNum, "Failed", "Trading navigation panel object is not visible.", "", "PageNavg_AcntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Trading tab not found. Too slow to load.");
			}*/
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Trading, strPageName, tab_Trading, "Trading", "Click", "USB")){
				pageTitle = "";
			}else{
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();
			}
			//wait for Tradings tab
			/*if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_Trading, "Page Nav - Tradins Tab ")){
				Reporting.reportStep(rowNum, "Failed", "Tradins navigation panel object is not visible.", "", "PageNavg_AcntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Tradins tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_Trading, "Dropdown", rowNum);
			Thread.sleep(60);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, obj_dd_Trading, "Page Nav - Tradins ")){
				Reporting.reportStep(rowNum, "Failed", "Tradins navigation panel object is not visible.", "", "PageNavg_AcntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Tradins tab not found. Too slow to load.");
			}
			for(int i = 1; i<=3; i++ ){
				if(!WM_CommonFunctions.dropwdown_IsItemPresent_InList(rowNum, dd_Trading, strPageName)){
					WM_CommonFunctions.clickItem(tab_Trading, "Dropdown", rowNum);
					Thread.sleep(30);
				}else{
					break;
				}
				
			}*/
			
			/*WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Trading, strPageName);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();*/
			break;
		//end of Trading drop down list	
		
		case "Quotes & Markets":
			Thread.sleep(100);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_QuotesAndMkt, "Page Nav - QuotesAndMkt ")){
				Reporting.reportStep(rowNum, "Failed", "QuotesAndMkt navigation panel object is not visible.", "", "PageNavg_QuotesAndMktTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - QuotesAndMkt tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_QuotesAndMkt, "Dropdown", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();
			break;
		case "Research":
			Thread.sleep(100);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_Research, "Page Nav - Research ")){
				Reporting.reportStep(rowNum, "Failed", "Research navigation panel object is not visible.", "", "PageNavg_ResearchTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Research tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_Research, "Dropdown", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title1, rowNum)).trim();
			break;
		case "Preferences":
			Thread.sleep(100);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_Prefrences, "Page Nav - Prefrences ")){
				Reporting.reportStep(rowNum, "Failed", "Prefrences navigation panel object is not visible.", "", "PageNavg_PrefrencesTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Prefrences tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_Prefrences, "Dropdown", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();
			break;
		//#######  #Start of Dropdowns for e-Documents  ########	
		case "Delivery Preferences":case "Statements":case "Confirms":case "Tax Forms":case "Advisory Reports":case "Check Images":
		case "Third-Party Disbursements":
			Thread.sleep(100);
			/*if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_eDocuments, "Page Nav - Account Tab ")){
				Reporting.reportStep(rowNum, "Failed", "Account Details navigation panel object is not visible.", "", "PageNavg_AcntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - Account Details tab not found. Too slow to load.");
			}*/
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_eDocs, strPageName, tab_eDocuments, "eDocs", "Click", "USB")){
				pageTitle = "";
			}else{
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title1, rowNum)).trim();
			}
			/*if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_eDocuments, "Page Nav - eDocuments ")){
				Reporting.reportStep(rowNum, "Failed", "eDocuments navigation panel object is not visible.", "", "PageNavg_eDocumentsTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - eDocuments tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_eDocuments, "Dropdown", rowNum);
			Thread.sleep(50);
			for(int i = 1; i<=3; i++ ){
				if(!WM_CommonFunctions.dropwdown_IsItemPresent_InList(rowNum, dd_eDocs, strPageName)){
					Thread.sleep(100);
				}else{
					break;
				}
			}*/
			/*WM_CommonFunctions.clickItem(tab_eDocuments, "Dropdown", rowNum);
			
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_eDocs, strPageName);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title1, rowNum)).trim();*/
			break;
		//END of e-Documents list dd_NavPanel_List	
		case "Cash Management":
			Thread.sleep(100);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_CashMgmnt, "Page Nav - eDocuments ")){
				Reporting.reportStep(rowNum, "Failed", "CashMgmnt navigation panel object is not visible.", "", "PageNavg_CashMgmntTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - CashMgmnt tab not found. Too slow to load.");
			}
			if(!WM_CommonFunctions.dropwdown_IsItemPresent_InList(rowNum, dd_NavPanel_List, strPageName)){
				Thread.sleep(100);
				WM_CommonFunctions.clickItem(tab_CashMgmnt, "Dropdown", rowNum);
				Thread.sleep(30);
			}
			WM_CommonFunctions.clickItem(tab_CashMgmnt, "Dropdown", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title, rowNum)).trim();
			break;
		case "eBill":
			Thread.sleep(100);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_eBill, "Page Nav - eBill ")){
				Reporting.reportStep(rowNum, "Failed", "eBill navigation panel object is not visible.", "", "PageNavg_eBillTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - eBill tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_eBill, "Dropdown", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title1, rowNum)).trim();
			break;
		case "Contact Us":
			Thread.sleep(100);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tab_ContatUs, "Page Nav - ContatUs ")){
				Reporting.reportStep(rowNum, "Failed", "ContatUs navigation panel object is not visible.", "", "PageNavg_ContatUsTab", driver, "", "", null);	
				System.out.println("FAILED  >> Page Navigation - ContatUs tab not found. Too slow to load.");
			}
			WM_CommonFunctions.clickItem(tab_ContatUs, "Dropdown", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(SummaryPage.page_Title_ContactUs, rowNum)).trim();
			break;
		default:
			System.out.println("Drop down option not found in switch -case, please check");
			Reporting.reportStep(rowNum, "Failed", strPageName + " - not found in expected list of option names. Check Swich stmnt.", "", "", driver, "", "", null);	
			break;
		}
		Thread.sleep(100);
		
		//match the page title with page name
		//add passed or failed in the report accordingly
		if(!pageTitle.equals("")){
			
			if(strExpectedPageTitle.contains(pageTitle)){
				System.out.println("PASSED - Page navigation check Passed for - " + strPageName);
				Reporting.reportStep(rowNum, "Passed", strPageName + " - has loaded successfully.", "", "", driver, "", "", null);	
			}else{
				System.out.println("FAILED - Page navigation check failed for - " + strPageName + " Expected / Actual = " + strExpectedPageTitle + "/" +pageTitle);
				Reporting.reportStep(rowNum, "Failed", strPageName + " - page did not load successfully Actual / Expected = " + pageTitle + "/" +  strExpectedPageTitle, "", "STF_PageLoadCheck", driver, "", "", null);	
			}//End of nested If-Else
		}else{
			Reporting.reportStep(rowNum, "Failed", strPageName + " - page title could not be located by the script.", "", "STF_PageLoadCheck", driver, "", "", null);	
		}//End of If-Else
		
	}
//************************  End of functions **********************************
//************************************************************************************************************


}
	