package PageObjects.WM;

import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class HoldingsZoom_Page {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	
	@FindBy(how = How.CSS, using = "#sviHoldingZoomModalLabelTitle")
	public WebElement HZoom_Modal_PageTitle;
	
	@FindBy(how = How.CSS, using = "#svi-holding-zoom-modal .btn-primary")
	public WebElement HZoom_Modal_Close;
	
	@FindBy(how = How.CSS, using = ".svi-holding-zoom h5 span:nth-child(1)")
	public WebElement am_HZoom_Symbol;
	
	
	
	//--------------  Constructor to initialize all elements  --------------
	public HoldingsZoom_Page(){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }


	//************************  End of functions **********************************
	//************************************************************************************************************
}
 