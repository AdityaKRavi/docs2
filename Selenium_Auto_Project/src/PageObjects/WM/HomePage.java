package PageObjects.WM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;


public class HomePage {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	 
	// creating webelements for objects - Trading tab menu >> Stocks from dropdown, Transfers tab menu >> External Transfer from dropdown
	
	/*@FindBy (how = How.CSS, using =  ".pageTitleHead")
	public WebElement element_PageTitle;
	*/
	
	//*********    Trade Menu details   ************
	@FindBy(how = How.ID, using = "divWMenu_Trading")
	public WebElement lnk_TradingTab;
	
	//Stocks dropdown link
	
	@FindBy (how = How.CSS, using =  "#liWMenu_TradingStocks")
	public WebElement lnk_TradeMenuStocksLink;
	
	//Options Dropdown Link
	@FindBy(how = How.CSS, using =  "#liWMenu_TradingOptions")
	public WebElement lnk_TradeMenuOptionsLink;
	
	//MF dropdown link
	@FindBy(how = How.CSS, using =  "#liWMenu_TradingMutualFunds")
	public WebElement lnk_TradeMenuMFLink;
	
	 
	//*********      Transfer Menu details      *********
	@FindBy(how = How.ID, using = "divWMenu_TransferMoney")
	public WebElement lnk_ExTransfersTab;
 
	@FindBy(how = How.CSS, using = "#liWMenu_ExternalTransfers")
	public WebElement lnk_ExTransfersMenuLink;
	

	// This is a constructor
	public HomePage(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        //new AjaxElementLocatorFactory(driver, 20), this
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
    }

	
	
	
	
	//########################     FUNCTION       #############
	

	//######################################################################################################################
	//######################################################################################################################
	//Description - Click on a link / tab present on Home Page.
	//Argument - tab or link name in string format.
	//Created - Jan 26th 2017
	//By  - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean clickOn(String strTabName){
		 
		switch(strTabName)
		{
		case "lnk_TradingTab" :
			if(CommonUtils.isElementPresent(lnk_TradingTab) == true){
				lnk_TradingTab.click();
				return true;
			}else{
				return false;
			}
		case "lnk_ExTransfersTab" :
			if(CommonUtils.isElementPresent(lnk_ExTransfersTab) == true){
				lnk_ExTransfersTab.click();
				return true;
			}else{
				return false;
			}
		default :
			System.out.println("Invalid Tab name provided");
			return false;
		}
	}
	//************************  End of function **********************************
 
	
}
