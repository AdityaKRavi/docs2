package PageObjects.WM;

import java.util.List;
import functionalLibrary.Global.DateUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;

public class Branding_Panel {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	
	//##########################################################################################
	//##########################################################################################
	//********************************    Object Declaration    ********************************

	//-----------------   STF   -----------------

	@FindBy(how = How.CSS, using = ".logo")
	public WebElement STF_Header;
	
	@FindBy(how = How.CSS, using = "#currentDateTitle")
	public WebElement STF_CurrentDateTitle;
	
	@FindBy(how = How.CSS, using = "#currentDate")
	public WebElement STF_CurrentDateValue;
	
	@FindBy(how = How.CSS, using = ".lastLoginTitle")
	public WebElement STF_LastLoginTitle;
	
	@FindBy(how = How.CSS, using = ".lastLogin span:nth-child(2)")
	public WebElement STF_LastLoginValue;
	
	@FindBy(how = How.CSS, using = ".headerShortCuts>a")
	public WebElement STF_Sitemap;
	
	@FindBy(how = How.CSS, using = "li[ data-ng-include*=\"fastFindMenuItem.tpl\"]>a" )
	public List <WebElement> STF_Sitemap_FastFindMenu;
	
	
	//-----------------   USB Icons   -----------------
	@FindBy(id = "WMenu-brokgBackGroundDiv")
	public WebElement USB_BrandingHeader;
	//******  USB Logo  *******
	@FindBy(id = "WMenu-BrokgLogo")
	public WebElement USB_Logo;
	
	@FindBy(id = "WMenu-BrokgLogoutText")
	public WebElement LogoutButton_USB;
	
	@FindBy(id = "WMenu-Brokg-Mysettings-shown-text")
	public WebElement USB_MySettings;
	
	@FindBy(id = "WMenu-Brokg-NeedHelp-shown-text")
	public WebElement USB_Help;
	
	@FindBy(xpath = "//*[contains(@class,'ng-valid form-control ng-empty ng-valid-required tt-input')]")
	public WebElement USB_Symbol;
	
	@FindBy(xpath = "*//input[@class='svi-directive svi-typeahead ng-pristine ng-untouched ng-valid form-control ng-empty ng-valid-required tt-input']/following::button[@class='btn btn-sm btn-primary quote-button']")
	public WebElement USB_GoBtn;
	
	//##########################################################################################
	//##########################################################################################
	//********************************    Inits    ********************************
	public Branding_Panel(){
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
	
	StringManipulation StringManipulation = new StringManipulation();
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	Reporting Reporting = new Reporting();
	DateUtils DateUtils = new DateUtils();
	
	//##########################################################################################
	//##########################################################################################
	
	
	//********************************    Methods    ********************************
	//##################################################################################
		//Function name	: STF_Set_ParentObjForImg()
		//Description 	: this function sets the parent object that will be highlighted
						// >> in screenshot. if that object itself is missing then it passes null to report.
		//Parameters 	: None
		//Assumption	: None
		//Created By	: Neelesh Vatsa
		//##################################################################################
	public WebElement Set_ParentObjForImg(WebElement objName) throws Exception{
		if(!CommonUtils.isElementPresent(objName)){
			objName = null;
		}
		return objName;
	}
	//##################################################################################
	//Function name	: STF_Check_AllBrandingPanelObjects()
	//Description 	: check all branding elements for STF
	//Parameters 	: Row Number for report file
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################
	public void STF_Check_AllBrandingPanelObjects(int rowNum) throws Exception{
		System.out.println("Checking all brandingpanel elements exists");
		STF_Header = Set_ParentObjForImg(STF_Header);
		try{
			WebElement[] arrBrandingElements = new WebElement[]{STF_Header,STF_CurrentDateTitle,STF_CurrentDateValue,STF_LastLoginTitle,STF_LastLoginValue,STF_Sitemap};
			String[] arrBrandingKeyword = new String[]{"Header","Current DateTitle","Current Date Value","Last Login Title","Last Login Value","SiteMap"}; 
			for(int i = 0; i< arrBrandingElements.length; i++){
				//String elementName = (arrBrandingElements[i]).getText();
				if(!(CommonUtils.isElementPresent(arrBrandingElements[i]))){
					System.out.println("Element not found on Login page - " + arrBrandingKeyword[i]);
					Reporting.reportStep(rowNum, "Failed", arrBrandingKeyword[i] + "-  is missing on branding panel.", "", "STF_BrandingPanelElelemntCheck" + i, driver, "", "", STF_Header);
				}else{
				Reporting.reportStep(rowNum, "Passed", arrBrandingKeyword[i] + " - is present.", "", "", driver, "", "", null);}
				//End of If-Else
				rowNum = rowNum + 1;
			}//End of For Loop
			//Check Sitemap values
			if(CommonUtils.isElementPresent(STF_Sitemap)){
				branding_STF_VerifyAllSiteMapValues(rowNum, STF_Header);
			}else{
				Reporting.reportStep(rowNum, "Failed", "Sitemap elemets cannot be tested due to missing Sitemap link", "", "STF_SiteMapLinksCheck", driver, "", "", STF_Header);
			}
			
			//Check Current date value matches matches machine time
			rowNum = rowNum + 1;
			if(CommonUtils.isElementPresent(STF_CurrentDateValue)){
				String time1 = DateUtils.minusMin_ToDate(0);
			    String time2 = DateUtils.minusMin_ToDate(1);
			    System.out.println(time1);
			    //System.out.println(time2);
			    //String actualDate =  DateUtils.getCurrentDateTime_Parameters("WeekDayName") + ", " + DateUtils.getCurrentDateTime_Parameters("MonthName") +" " + DateUtils.getCurrentDateTime_Parameters("Day") + ", " + DateUtils.getCurrentDateTime_Parameters("Year") + " " + time1 + DateUtils.getCurrentDateTime_Parameters("AmPm") + " ET";;
				
				String actualDate1 =  DateUtils.getCurrentDateTime_Parameters("WeekDayName") + ", " + DateUtils.getCurrentDateTime_Parameters("MonthName") +" " + DateUtils.getCurrentDateTime_Parameters("Day") + ", " + DateUtils.getCurrentDateTime_Parameters("Year") + " " + time1 + DateUtils.getCurrentDateTime_Parameters("AmPm") + " ET";;
				String actualDate2 =  DateUtils.getCurrentDateTime_Parameters("WeekDayName") + ", " + DateUtils.getCurrentDateTime_Parameters("MonthName") +" " + DateUtils.getCurrentDateTime_Parameters("Day") + ", " + DateUtils.getCurrentDateTime_Parameters("Year") + " " +time2 + DateUtils.getCurrentDateTime_Parameters("AmPm") + " ET";;
				
				String expectedDate = STF_CurrentDateValue.getText();
				System.out.println(expectedDate);
			
				if(actualDate1.equals(expectedDate)||actualDate2.equals(expectedDate)){
				//if(actualDate.equals(expectedDate)){
					Reporting.reportStep(rowNum, "Passed", "Date displayed is correct.", "", "", driver, "", "", null);
				}else{
					Reporting.reportStep(rowNum, "Failed",  "Expected/Actual = " + expectedDate  + "/" + actualDate1, "", "STF_BrandingPanelCheckDateValue", driver, "", "", STF_Header);				
				}
			}else{
				Reporting.reportStep(rowNum, "Failed",  "Current date field does not exists thus cannot verify date value", "", "STF_BrandingPanelCheckDateValue", driver, "", "", STF_Header);							
			}
		}//End of TRY
		catch(Exception e){
			System.out.println("Branding_Panel - Excpetion found in method - STF_Check_AllBrandingPanelObjects");
			e.printStackTrace();
		}//End of CATCH
	}//END OF FUNCTION
	
	//##################################################################################
	//Function name	: branding_STF_VerifyAllSiteMapValues()
	//Description 	: check all SiteMap elements/links for STF
	//Parameters 	: Row Number for report file
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################
	public void branding_STF_VerifyAllSiteMapValues(int rowNum, WebElement STF_Header) throws Exception{
		//List <String> siteMapNames;
		String actual_SiteMapValues = "";
		try{
			
			STF_Sitemap.click();
			Thread.sleep(500);
			for (int i = 0; i < STF_Sitemap_FastFindMenu.size(); i++) {

				actual_SiteMapValues = actual_SiteMapValues + STF_Sitemap_FastFindMenu.get(i).getText() + ";";
				System.out.println(STF_Sitemap_FastFindMenu.get(i).getText());
			}//End of FOR
			actual_SiteMapValues = StringManipulation.string_RemoveFromStringEnd(actual_SiteMapValues, ";");

				//Added for AUTO-592
				try{
					actual_SiteMapValues = actual_SiteMapValues.substring(actual_SiteMapValues.indexOf("Home"));
				}
				catch(java.lang.StringIndexOutOfBoundsException Exception){
					System.out.println("StringIndexOutOfBoundsException found in : Branding_Panel page >> @method - branding_STF_GetAllSiteMapValues.");
					Reporting.reportStep(rowNum, "Failed", "Sitemap check failed due to 'StringIndexOutOfBoundsException' in SIteMap values", "", "STF_SiteMapLinksCheck", driver, "", "", STF_Header);				
				}//End of catch block for StringIndexOutOfBoundsException
				catch(Exception e){
					System.out.println("Exception found in : Branding_Panel page >> @method - branding_STF_GetAllSiteMapValues.");
					Reporting.reportStep(rowNum, "Failed", "Sitemap check failed due to 'Exception' in SIteMap values", "", "STF_SiteMapLinksCheck", driver, "", "", STF_Header);				
				}//End of catch block for Generic Exception
			
			System.out.println("Actual = " + actual_SiteMapValues);
			
			String expected_SiteMapValues = "Home;Accounts;Portfolio At A Glance;Balances;Holdings;Holdings Zoom;Activity;Unrealized Gain/Loss;Realized Gain/Loss;Asset Allocation;Household Overview;Projected Monthly Income;Income Summary;Download;Awards;Profile;View Orders;Trading;Stock;Order Status;Quotes & Markets;Research;Preferences;eDocuments;Delivery Preferences;Statements;Confirms;Tax Forms;Advisory Reports;Check Images;Third-Party Disbursements;Cash Management;eBill;Contact Us;Logout";
			System.out.println("Expeced = " + expected_SiteMapValues);
			
			if(actual_SiteMapValues.equals(expected_SiteMapValues)){
				Reporting.reportStep(rowNum, "Passed", "All sitemap elements are present", "", "", driver, "", "", null);
			}else {
				Reporting.reportStep(rowNum, "Failed", "Sitemap elemets do not match expected value", "", "STF_SiteMapLinksCheck", driver, "", "", STF_Header);
			}//End of If-Else
		}//End of TRY
		catch (Exception e){
			System.out.println("Exception found in : Branding_Panel page >> @method - branding_STF_GetAllSiteMapValues.");
			Reporting.reportStep(rowNum, "Failed", "Sitemap check failed due to exception", "", "STF_SiteMapLinksCheck", driver, "", "", STF_Header);
			e.printStackTrace();
		}//End of CATCH
		
	}//END OF FUNCTION
	
	//##################################################################################
	//Function name	: check_AllBrandingPanelElements()
	//Description 	: check all branding elemenets based on client name.
	//Parameters 	: Client name, Row Number for report file and test keyword
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################
	public void check_AllBrandingPanelElements(String clientName, int rowNum) throws Exception {
		Thread.sleep(500);
		switch (clientName) {
		case "USB":
				USB_Check_AllBrandingPanelObjects(rowNum);
				break;
		case "RWB":
				//RWB_Check_AllBrandingPanelElements(rowNum, strKeyword);
				break;
		case "STF":
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				STF_Check_AllBrandingPanelObjects(rowNum);
				break;
		default:
			System.out.println("Branding_Panel >> check_LogoIsPresent - Invalid client name passed. Pease check");
			throw new SkipException("check_AllBrandingPanelElements >> Testing skipped due to invalid cient name.");
		}//End of Switch
		
	}//END OF FUNCTION
	
	//##################################################################################
	//Function name	: USB_Check_AllBrandingPanelObjects()
	//Description 	: check all SiteMap elements/links for USB
	//Parameters 	: Row Number for report file
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	//##################################################################################	
	public void USB_Check_AllBrandingPanelObjects(int rowNum){
		try{
			USB_BrandingHeader = Set_ParentObjForImg(USB_BrandingHeader);
			
			/*AUTO-593, Removed Symbol look up link element 
			from both expected and actual lists due to  latest front end changes for USB.*/
			
			/*WebElement[] arrBrandingElements = new WebElement[]{USB_Logo, USB_Help, USB_MySettings, LogoutButton_USB, USB_Symbol, USB_GoBtn, USB_SymbolLookup_Link};
			String[] arrBrandingKeyword = new String[]{"Logo", "Help", "MySettings", "LogoutBtn", "Symbol SearchBox", "Go Btn", "Symbol Lookup Link"}; */
			
			WebElement[] arrBrandingElements = new WebElement[]{USB_Logo, USB_Help, USB_MySettings, LogoutButton_USB, USB_Symbol, USB_GoBtn};
			String[] arrBrandingKeyword = new String[]{"Logo", "Help", "MySettings", "LogoutBtn", "Symbol SearchBox", "Go Btn"}; 
			System.out.println("check elements -- after creating list");
			
			for(int i = 0; i< arrBrandingElements.length; i++){
				//String elementName = (arrBrandingElements[i]).getText();
				if(!(CommonUtils.isElementPresent(arrBrandingElements[i]))){
					System.out.println("Element not found on Login page - " + arrBrandingKeyword[i]);
					Reporting.reportStep(rowNum, "Failed", arrBrandingKeyword[i] + "-  is missing on branding panel.", "", "USB_BrandingPanelElelemntCheck" + i, driver, "", "", USB_BrandingHeader);
					
				}else{
				Reporting.reportStep(rowNum, "Passed", arrBrandingKeyword[i] + " - is present.", "", "", driver, "", "", null);}//End of If-Else
				rowNum = rowNum + 1;
			}//End of FOR
				
		}//End of TRY
		catch(Exception e){
			System.out.println("Branding_Panel - Excpetion found in method - USB_Check_AllBrandingPanelObjects");
			e.printStackTrace();
			}//End of Catch
	}//END OF FUNCTION

}//END OF CLASS

	

