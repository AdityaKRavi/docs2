package PageObjects.WM;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.DateUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class ResultTable_Section {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	WebDriverWait wait;
	StringManipulation StringManipulation = new StringManipulation();
	
	//
	@FindBy(how = How.CSS, using =".fa.fa-2x.fa-spinner.fa-spin" )
	public WebElement pageLoad_spinner;
	
	@FindBy(how = How.CSS, using = ".glyphicon.glyphicon-plus")
	public List<WebElement> Btn_ExpandIcon;
	
	@FindBy(how = How.CSS, using = ".glyphicon.glyphicon-minus")
	public List<WebElement> Btn_CollapseIcon;
	//*************Result table ************************************
	//  This is a common section where we display totals on top of Result table
	@FindBy(how = How.CSS, using = ".col-md-4.pull-right.currency")
	public WebElement resultTable_TotalsSection;
	
	// ******************************** Filters ************************************
	//******************************************************************************
	
	//Account filter
	//order status page - usb, bow
	@FindBy(how = How.CSS, using = ".dropdown.svi-account-dropdown")
	public WebElement ddTab_AllAccountsFilter1;
	
	@FindBy(how = How.CSS, using = ".dropdown.svi-account-dropdown ul li a")
	public List<WebElement> acnt1;
	//ex portfolio page - rwb
	@FindBy(how = How.CSS, using = ".svi-group-account-dropdown button")
	public WebElement ddTab_AllAccountsFilter2;
	
	@FindBy(how = How.CSS, using = ".svi-group-account-dropdown ul li a")
	public List<WebElement> acnt2;
	//Home page- lpl, stf
	@FindBy(how = How.ID, using = "currentAccount")
	public WebElement ddTab_AllAccountsFilter3;
	
	@FindBy(how = How.CSS, using = "#currentAccount option")
	public List<WebElement> acnt3;
	
	// Asset filter tab and dropdown element object
	//Note space after Asset is important in case of Held Away filter
	@FindBy(how = How.CSS, using = "div[data-title^=\"All Asset \"] .btn-group")
	public WebElement ddTab_AllAssetTypeFilter;
	
	@FindBy(how = How.CSS, using = "[data-title^=\"All Asset\"] ul li.ng-scope")
	public List<WebElement> dd_AllAssetTypes;
	
	
	//All term types filter tab and dropdown elements
	
	@FindBy(how = How.CSS, using = ".term-class-dropdown")
	public WebElement ddTab_AllTermTypeFilter;
	
	@FindBy(how = How.CSS, using = ".term-class-dropdown ul li")
	public List<WebElement> dd_AllTermTypes;
	
	//Prior and Current year(RGL)
	@FindBy(how = How.CSS, using = ".year-class-dropdown")
	public WebElement ddTab_YearFilter;
	
	@FindBy(how = How.CSS, using = ".year-class-dropdown ul li")
	public List<WebElement> dd_YearTypes;
	
	//Symbol searchbox
	@FindBy(how = How.CSS, using = ".symbol-class-input input")
	public WebElement txtBox_Symbol;
	
	//Held By USBI
	@FindBy(how = How.CSS, using = ".heldaway-dropdown .btn")
	public WebElement ddTab_HeldAwayFilter;
	//Held Away List
	@FindBy(how = How.CSS, using = ".heldaway-dropdown ul li")
	public List<WebElement> dd_HeldAway;
	
	//*****************    Order Status Page filters    **********************
	// Statues filter tab and dropdown element object
	@FindBy(how = How.CSS, using = "div[data-title=\"All Statuses\"] .btn-group")
	public WebElement ddTab_AllStatusesFilter;
	
	@FindBy(how = How.CSS, using = "[data-title=\"All Statuses\"] ul li.ng-scope")
	public List<WebElement> dd_AllStatuses;
	
	// Order Types filter tab and dropdown element object
	@FindBy(how = How.CSS, using = "div[data-title=\"All Order Types\"] .btn-group")
	public WebElement ddTab_AllOrderTypesFilter;
	
	@FindBy(how = How.CSS, using = "[data-title=\"All Order Types\"] ul li.ng-scope")
	public List<WebElement> dd_AllOrderTypes;
	
	//*****************    Positions Page filters    **********************
	// Asset type filter under Asset view
	@FindBy(how = How.CSS, using = "h3.ng-scope>span")
	public List<WebElement> list_AllAssetClasses;
	// ************* Other Elements ************************************
	//***************************************************************************
	@FindBy(how = How.CSS, using = ".table")
	public WebElement obj_ResultTable;
	
	@FindBy(how = How.XPATH, using = ".svi-column-header")
	public WebElement obj_ColHeader;
	
	// Column Names
	//@FindBy(how = How.XPATH, using = ".//*[@id='svi-unrealized-gain-loss']/div[2]/div/div[4]/div/div/div/table/thead/tr/th")
	@FindBy(how = How.CSS, using = "table thead tr th span.header-text")
	public List<WebElement> list_ColNames;
	
	@FindBy(how = How.CSS, using = "#positionsTable thead>tr th")
	public List<WebElement> list_ColNames_1DB;	
	
	//Rows
	@FindBy(how = How.CSS, using = "table thead tr th")
	public List<WebElement> list_allTableRows;
	
	//Sort icons
	@FindBy(how = How.CSS, using = ".glyphicon-chevron-up")
	public WebElement obj_AcsenSortIcon;
	
	@FindBy(how = How.CSS, using = ".glyphicon-chevron-down")
	public WebElement obj_DescenSortIcon;
	
	//Pagination
	@FindBy(how=How.CSS, using = ".svi-paging-control")
	public WebElement obj_PaginationBar;
	
	@FindBy(how = How.CSS, using = ".row.paging-top ul li a")
	public List<WebElement> pagination_Values;
	//--------------  Constructor to initialize all elements  --------------
	public ResultTable_Section(){
        
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }


	//########################     FUNCTIONS       #############
	//Decription - This functions sets the object for screenshot purpose. If Filter panel 
	//itself is not present, then null object is returned in which case full page screenshot is taken.
	public  WebElement Set_ParentObjForImg() throws Exception{
		if(CommonUtils.isElementPresent(obj_ResultTable) == false){
			obj_ResultTable = null;
		}
		return obj_ResultTable;
	}
	
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();

	//######################################################################################################################
	//######################################################################################################################
	//Name - get_ColumnNames
	//Description - This function returns all column names.
	//Argument - none
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  String get_ColumnNames(int rowNum) throws Exception{
		try{
			String actualValues = "";
			actualValues = WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, list_ColNames, null);
			return actualValues;
		}catch(Exception e){
			System.out.println("Exception found in method >>get_ColumnNames, class = ResultTable_Section. Please check!!! ");
  			e.printStackTrace();
  			return "";
		}
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_RowCount
	//Description - This function returns result table row count, excluding totals row and first row is an empty padding.
	//Argument - none
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  int get_RowCount(){
		try{
			int actulRowCount = (list_allTableRows.size() - 1);
			return actulRowCount;
		}catch(Exception e){
			System.out.println("Exception found in method >>get_RowCount, class = ResultTable_Section. Please check!!! ");
  			e.printStackTrace();
  			return -1;
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_RowCount
	//Description - This function returns result table column count.
	//Argument - none
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  int get_ColCount(){
		try{
			int actulColCount = list_ColNames.size();
			return actulColCount;
		}catch(Exception e){
			System.out.println("Exception found in method >>get_ColCount, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return -1;
		}
	}

	//######################################################################################################################
	//######################################################################################################################
	//Name - get_ColIndex
	//Description - This function returns column index of given col name.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  int get_ColIndex(String strColName){ 
		try{
			//System.out.println("Col hearder list size = "+ list_ColNames.size());
			String actualColName = "";
			for(int iLoop=0; iLoop<list_ColNames.size(); iLoop++ ){
				//System.out.println("Col names = " + list_ColNames.get(iLoop).getText());
				
				actualColName = list_ColNames.get(iLoop).getText();
				actualColName = actualColName.replaceAll("\n", "");
				if((strColName.equals("Value") && actualColName.equals("Value")) || ((!strColName.equals("Value")) && actualColName.contains(strColName))){
					//System.out.println("Col Num for " + strColName + " = " + Integer.toString(iLoop+1));
					return iLoop+1;
				}
			}
			System.out.println("Column Name = " + strColName + " >> Could not be found in table header. Pease check!!!.");
			return -1;
		}catch(Exception e){
			System.out.println("Exception found in method >>get_ColIndex, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return -1;
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_rowNum_ForMatchingValue
	//Description - This function returns column cells value for given column name
	//Argument - result table row num, column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
		
	public int get_rowNum_ForMatchingValue(String strValueToMatch, int colNum) {
		List<WebElement> testValues = driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ") a.svi-symbol.ignoreToggle"));
		String testSymbol="";
		int i = 0;
		//int row=0;
		
		while(!(testSymbol.equals(strValueToMatch)) && i<testValues.size()) {
			testSymbol =  testValues.get(i).getText().trim();
			//row = i;
			i = i+1;
		}
		return i;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_rowNum_ofValue_fromTable
	//Description - This function returns row no: of given value for given passed table object.
	//Argument - result table row num, column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
		
	public int get_rowNum_ofValue_fromTable(String strValueToMatch, int colNum, List<WebElement> objTable) {
		List<WebElement> testValues = driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ")" ));
		String testSymbol="";
		int i = 0;
		//int row=0;
		
		while(!(testSymbol.equals(strValueToMatch)) && i<testValues.size()) {
			testSymbol =  testValues.get(i).getText().trim();
			//row = i;
			i = i+1;
		}
		return i;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_CellValue
	//Description - This function returns column cells value for given column name
	//Argument - result table row num, column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  String get_CellValueForColName(int rowNum, String colName){
		try{
			int colNum =  get_ColIndex(colName);
			WebElement obj_colValue= driver.findElement(By.cssSelector("table tbody tr:nth-child(" + rowNum + ") td:nth-child(" +colNum+ ")"));
			String colValue = obj_colValue.getText();
			return colValue;
		}catch(Exception e){
			System.out.println("Exception found in method >>get_CellValueForColName, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return "";
		}
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_CellValue
	//Description - This function returns cell value of given col number.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  String get_CellValueForColIndex(int rowNum, int colNum){
		try{
			WebElement obj_colValue= driver.findElement(By.cssSelector("table tbody tr:nth-child(" + rowNum + ") td:nth-child(" +colNum+ ")"));
			String colValue = obj_colValue.getText();
			return colValue;
		}catch(Exception e){
			System.out.println("Exception found in method >>get_CellValueForColIndex, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return "";
		}
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - apply_ColSort
	//Description - This function applies sort on given column name.
	//Argument - row num, column name, sort type and client name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean apply_ColumnSort(int rowNum, String colName, String sortType, String strClientName){
		try{
			String actualValues = "";
			int colNum =  get_ColIndex(colName);
			String currentColName = "";
			boolean flgColNameFound = false;
			for(int iLoop=0; iLoop<list_ColNames.size(); iLoop++ ){
				//to handle col name with new line char in Dav
				currentColName = list_ColNames.get(iLoop).getText();
				currentColName = currentColName.replace("\n", "");
				currentColName = currentColName.replace(" ", "");
				colName = colName.replace(" ", "");
				
				if((colName.equals("Value") && currentColName.equals("Value")) ||(!colName.equals("Value") && currentColName.contains(colName))){
					flgColNameFound = true;
					//click on the col name
					list_ColNames.get(iLoop).click();
					
					TimeUnit.SECONDS.sleep(5);
					WM_CommonFunctions.wait_ForPageLoad(rowNum);
					//click twice for descending order sort
					if(sortType.contains("Desc")) {
						list_ColNames.get(iLoop).click();
					}//End of if
					if(CommonUtils.isElementPresent(obj_AcsenSortIcon) && sortType.contains("Asc")) {
						System.out.println("Ascending order sort applied for - " + colName);
						
					}else if(CommonUtils.isElementPresent(obj_DescenSortIcon)&& sortType.contains("Desc")) {
						System.out.println("Descending order sort applied for - " + colName);
						
					}else {
						Reporting.reportStep(rowNum, "Failed","Sorting icon not found on Column name - " + colName, "", "sort_iconNotFound", driver, "", "", null);
					}
					return true;
				}//end of if
			}//end of for
			
			System.out.println("Column name could not be found in UI" + colName);
			Reporting.reportStep(rowNum, "Failed","Sorting could not be applied on Column name - " + colName, "", "sort_NotWorking", driver, "", "", null);
			return false;
		}catch(Exception e){
			System.out.println("Exception found in method >>apply_ColumnSort, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", "Exception found. @method[apply_ColumnSort], @class[ResultTable_Section]" , "", "Apply_ColSorting", driver, "", "", null);
			return false;
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - is_ColumnType_String
	//Description - This function verifies sort on given column name.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean is_ColumnType_String(String colName) {
		//System.out.println("***************  Is col name string - " + colName);
		if(colName.contains("%")) {
			//adding this to avoid %asset in DAV
			return false;
		}else if(colName.contains("Symbol")|| (colName.contains("Type"))|| (colName.contains("Description"))||(colName.contains("Asset"))||(colName.contains("Account")||(colName.contains("Security")))) {
			return true;
		}
		return false;
		
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_ColumnSort
	//Description - This function verifies sort on given column name.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public ArrayList<Double> ConvertToDoubleList(ArrayList<String> allElements) throws IOException, InterruptedException, InvocationTargetException {
		ArrayList<Double> ListOfDoubleArr = new ArrayList<Double>();
		for (String item : allElements) {
		    // Apply formatting to the string if necessary
			ListOfDoubleArr.add(Double.parseDouble(item));
		}
 
        return ListOfDoubleArr;
 
    }//end method
	//######################################################################################################################
	//######################################################################################################################
	//Name - compareNumberListElement_forSorting
		//Description - This function compares values in adjacent rows for column with String values
		//Argument - row num, list of values, sort type, col name n client name.
		//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void compareStringListElement_forSorting(int rowNum, ArrayList<String> valueList, String sortType, String colName, String strClient) {
		try {
			boolean sortPassed = false;
			int loopSize = 0;
			loopSize = valueList.size();
			boolean hasTotalsRow = hasTotals_In_LastRow(colName, strClient);
			if(hasTotalsRow) {
				loopSize = loopSize -1 ;
			}//end of if
			if(sortType.contains("Asc")) {
				for(int i = 1; i < loopSize; i++)  {
					if ((valueList.get(i-1).toUpperCase()).compareTo((valueList.get(i).toUpperCase())) <= 0) {
				    	sortPassed = true;
				    	//do nothing
				    }else if(colName.toLowerCase().contains("symbol") || colName.toLowerCase().contains("description")){
				    	//ignore invalid data for Margin and Cash being displayed as symbol or description
				    	if(valueList.get(i-1).equalsIgnoreCase("cash")|| valueList.get(i-1).equalsIgnoreCase("margin")) {
				    		continue;
				    	}
				    }
				    else {
				    	System.out.println("FAILED - Ascending order Sort has failed at row number - " + i + "  for values = " + valueList.get(i-1) + " and " +  valueList.get(i));
					    Reporting.reportStep(rowNum, "Failed","Ascending order sorting for col - " + colName + " has failed verification at row - " + (i) + ", value = "  + valueList.get(i-1) +" And " + valueList.get(i), "", "sort_Failed", driver, "", "", null);
					    sortPassed = false;
					    break;
				    }
				 }//end of for
				if(sortPassed) {
					Reporting.reportStep(rowNum, "Passed","Ascending order Sort has passed for - " + colName, "", "", driver, "", "", null);
				    System.out.println("PASSED - Ascending order Sort has passed for - " + colName);
				    
				}//end of If
				 
			}//ENd of if for Asc
			else if(sortType.contains("Desc")) {
				for(int i = 1; i < loopSize; i++)  {
					//System.out.println("Value 2 = " + valueList.get(i));
					//System.out.println("Value 1 = " + valueList.get(i-1));
					
				    if ((valueList.get(i-1).toUpperCase()).compareTo((valueList.get(i).toUpperCase())) >= 0) {
				    	sortPassed = true;
				    	//do nothing
				    }else {
				    	System.out.println("FAILED - Descending order Sort has failed at row number - " + i + "  for values = " + valueList.get(i-1) + " and " +  valueList.get(i));
				    	Reporting.reportStep(rowNum, "Failed","Descending order sorting for col - " + colName + " has failed verification at row - " + (i) + ",  between = " + valueList.get(i-1) +" And "+ valueList.get(i), "", "sorting_Failed", driver, "", "", null);
				    	sortPassed = false;
				    	break;
			    	
				    }//end of if-else
				}//end of For
					if(sortPassed) {
						Reporting.reportStep(rowNum, "Passed"," Descending order Sort has passed for - " + colName, "", "", driver, "", "", null);
					    System.out.println("PASSED - Descending order Sort has passed for - " + colName);
					    
					}
			}else {	
				Reporting.reportStep(rowNum, "Failed","Sort type value not valid.", "", "sorting_Failed", driver, "", "", null);
		    	System.out.println("Sorting type value not found. Please check");
			}
		}catch(Exception e) {
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", "Exception Found. @method[compareNumberListElement_forSorting, @class[ResultTable_Section]" , "", "actionMenu_TW_Action", driver, "", "", null);
			System.out.println("FAILED - Exception found in @compareNumberListElement_forSorting");
		}//end of try-catch	
		
	}//end of function
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - compareNumberListElement_forSorting
	//Description - This function compares values in adjacent rows for column with number values
	//Argument - row num, list of values, sort type, col name n client name.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void compareNumberListElement_forSorting(int rowNum, ArrayList<Double> valueList, String sortType, String colName, String strClient) {
		try{
			System.out.println("Sort type is = " + sortType);
			boolean sortPassed = false;
			int loopSize = 0;
			loopSize = valueList.size();
			//check if col has last extra row for totals values
			boolean hasTotalsRow = hasTotals_In_LastRow(colName, strClient);
			if(hasTotalsRow) {
				//reduce loop size so that comparison is not done for last row with totals values
				loopSize = loopSize -1 ;
			}
			if(sortType.contains("Asc")) {
				for(int i = 1; i < loopSize; i++)  {
					if (valueList.get(i-1).compareTo(valueList.get(i)) <= 0) {
				    	 sortPassed = true;
				    	//do nothing
				    }else {
				    	Reporting.reportStep(rowNum, "Failed","Ascending order sorting for col - " + colName + " has failed verification at row - " + i, "", "sort_Failed", driver, "", "", null);
					    System.out.println("FAILED - Ascending order Sort has failed at row number - " + i + "  for values = " + valueList.get(i-1) + " and " +  valueList.get(i));
					    sortPassed = false;
					    break;
				    }
				 }//end of for
				if(sortPassed) {
					Reporting.reportStep(rowNum, "Passed"," Ascending order Sort has passed for - " + colName, "", "", driver, "", "", null);
				    System.out.println("PASSED - Ascending order Sort has passed for - " + colName);
				    
				}
				 
			}else if(sortType.contains("Desc")) {
					for(int i = 1; i < loopSize; i++)  {
						//System.out.println("value 2 = " + valueList.get(i));
						//System.out.println("value 1 = " + valueList.get(i-1));
					    if (valueList.get(i-1).compareTo(valueList.get(i)) >= 0) {
					    	sortPassed = true;
					    	//do nothing
					    }else {
					    	Reporting.reportStep(rowNum, "Failed","Descending order sorting for col - " + colName + " has failed verification at row - " + i, "", "sorting_Failed", driver, "", "", null);
					    	System.out.println("FAILED - Descending order Sort has failed at row number - " + i + "  for values = " + valueList.get(i-1) + " and " +  valueList.get(i));
					    	sortPassed = false;
					    	break;
					    }//end of if-else
					}//end of for
					if(sortPassed) {
						Reporting.reportStep(rowNum, "Passed"," Descending order Sort has passed for - " + colName, "", "", driver, "", "", null);
					    System.out.println("Passed - Descending order Sort has passed for - " + colName);
					}//end of If
			}else {	
				System.out.println("FAILED - Sorting type value not found. Please check");
				Reporting.reportStep(rowNum, "FAILED"," Sort type value is incorrect. Please check!", "", "", driver, "", "", null);
			    
			}//end of if-elseif-else
		}catch(Exception e) {
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", "Exception Found. @method[compareNumberListElement_forSorting, @class[ResultTable_Section]" , "", "actionMenu_TW_Action", driver, "", "", null);
			System.out.println("FAILED - Exception found in @compareNumberListElement_forSorting");
		
		}//end of try-catch
	}//end of function
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_ColumnSort
	//Description - This function verifies sort on given column name.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	boolean hasTotals_In_LastRow(String colName, String strClient) {
		boolean flgValue = false;
		switch(strClient) {
		case "USB":
			if(colName.contains( "Symbol / CUSIP")|| colName.contains("% Chg")|| colName.contains("Day Chg") ||colName.contains("Market Value")) {
				flgValue = true;
			}else {
				flgValue = false;
			}
			break;
		case "STF":
			if(colName.contains( "Asset Type")|| colName.contains("Market Value")|| colName.contains("Daily Value Change") ||colName.contains("Daily % Change")||colName.contains("Unrealized G/L")) {
				flgValue = true;
			}else {
				flgValue = false;
			}
			break;
		case "DAV":
			if(colName.contains( "Symbol / CUSIP")|| colName.contains("Gain/Loss") ||colName.contains("% Gain/Loss")||colName.contains( "Value")) {
				flgValue = true;
			}else {
				flgValue = false;
			}
			break;
		case "BOW":
			if(colName.contains( "Symbol / CUSIP")||colName.contains( "Value")) {
				flgValue = true;
			}else {
				flgValue = false;
			}
			break;
		case "RWB":
			if(colName.equals( "Value")) {
				flgValue = true;
			}else {
				flgValue = false;
			}
			break;
		}
		
		return flgValue;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_ColumnSort
	//Description - This function verifies sort on given column name.
	//Argument - row num, column name, sort type and client name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  void verify_ColumnSort(int rowNum, String colName, String sortType, String strClient){
		try{
			//replace strings/chars is we need to check sort on number fields
			//get_AllListOptions(rowNum, strClient, "", items)
			String allValues = get_AllColValues_WithoutColName(rowNum, colName);
			allValues = allValues.replaceAll(";-;", ";");
			//System.out.println("All Col Values - " + allValues);
			boolean flgNumComp = false;
			//replace all spl. symbols that are not required for comparison
			if(!is_ColumnType_String(colName)) {
				flgNumComp = true;
				allValues = allValues.replace("$", "");
				allValues = allValues.replace("%", "");
				allValues = allValues.replace(",", "");
				allValues = allValues.replace(",", "");
				allValues = allValues.replace("-;", "");
				allValues = allValues.replace(";N/A", "");
				allValues = allValues.replace("N/A;", "");
			}
			if(allValues.equals("-") || allValues.trim().equals("")) {
				Reporting.reportStep(rowNum, "Passed","Sorting Passed - all row values are null for - " + colName, "", "", driver, "", "", null);
			    System.out.println("Passed - all row values are null. Ascending/Descending order Sort has passed for - " + colName);
			}else {
				String[] arrValues = allValues.split(";");
				ArrayList<String> arrStringList = new ArrayList<String>();
				Collections.addAll(arrStringList, arrValues); 
				arrStringList.remove("-");
				if(flgNumComp) {
					//call function that creates double type list and then call compare method on list
					 ArrayList<Double> ListOfDoubleArr = ConvertToDoubleList(arrStringList);
					compareNumberListElement_forSorting(rowNum, ListOfDoubleArr, sortType, colName, strClient);
				}else {
					//call function that creates String based list and then call compare method on list
					compareStringListElement_forSorting(rowNum, arrStringList, sortType, colName, strClient) ;
				}//end of if-else
			}	
		}catch(Exception e){
			System.out.println("Exception found in method >>verify_ColumnSort, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", "Exception found. @method[verify_ColumnSort], @class[ResultTable_Section]" , "", "ColSorting", driver, "", "", null);
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_AllColValues
	//Description - This function returns all the row values for given column name.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  String get_AllColValues(int rowNum, String colName){
		try{
			String actualValues = "";
			int colNum =  get_ColIndex(colName);
			//System.out.println("col num = "+ colNum);
			List col_allValues= driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ")"));
			actualValues = WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, col_allValues, null);
			return actualValues;
			
		}catch(Exception e){
			System.out.println("Exception found in method >>get_AllColValues, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return "";
		}
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_AllColValues
	//Description - This function returns all the row values for given column name.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  String get_AllColValues_WithoutColName(int rowNum, String colName){
		try{
			String actualValues = "";
			int colNum =  get_ColIndex(colName);
			//System.out.println("col num = "+ colNum);
			List col_allValues= driver.findElements(By.cssSelector(".container.tableContainer table tbody tr td:nth-child(" + colNum + ")"));
			actualValues = WM_CommonFunctions.get_AllListOptions(rowNum, "", "", col_allValues);
					//WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, col_allValues, null);
			return actualValues;
			
		}catch(Exception e){
			System.out.println("Exception found in method >>get_AllColValues, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return "";
		}
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_AllColValues_MatchThisValue
	//Description - This function returns column index of given col name.
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean verify_AllColValues_MatchThisValue(int rowNum, String colName, String colValue){
		try{
			boolean flg_ValuesMatch = false;
			int colNum =  get_ColIndex(colName);
			List list_allColValues= driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ")"));
			
			//System.out.println("Mthod : verify_AllColValues_MatchThisValue >> Col value list size = " + list_allColValues.size());
			flg_ValuesMatch = WM_CommonFunctions.list_MatchAllColValues_WithThis(rowNum, list_allColValues, colValue, colName);
			return flg_ValuesMatch;
			
		}catch(Exception e){
			System.out.println("Exception found in method >>get_AllColValues, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return false;
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - expandAll_Rows
	//Description - This function expand all rows if plus icon is present in the result table..
	//Argument - column name
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void expandAll_Rows(int rowNum) throws InterruptedException{
		
			TimeUnit.SECONDS.sleep(5);
  			wait = new WebDriverWait(driver,30);
			//js.ExecuteScript("arguments[0].scrollIntoView(true);" + "window.scrollBy(0,-100);", e); for IE and Firefox 
			//and js.ExecuteScript("arguments[0].scrollIntoViewIfNeeded(true);", e); for Chrome
			//get a list of all expand icons
			//Btn_ExpandBtn
			List<WebElement> list_AllExpandIcons = Btn_ExpandIcon;
			//System.out.println("Size of expand button list = " + list_AllExpandIcons.size());
			if(list_AllExpandIcons.size()>0){
	  				for (WebElement clickExpand : list_AllExpandIcons) {
	  					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded(true);",clickExpand);
	  					try{
		  					if(clickExpand.isDisplayed() && clickExpand.isEnabled()){
		  						clickExpand.click(); 	
		  						wait.until(ExpectedConditions.visibilityOfAllElements(Btn_CollapseIcon));  	
		  					}
	  					}catch(Exception e){
	  						JavascriptExecutor ex=(JavascriptExecutor)driver;
	  						ex.executeScript("arguments[0].click()", clickExpand);
	  						//System.out.println("Exception was found in method >>expandAll_Rows, class = ResultTable_Section. Please check!!! ");
	  						//e.printStackTrace();
	  						//Reporting.reportStep(rowNum, "Failed", "Expand icon could nit be clicked. Exception found in - expandAll_Rows" , "", "GL_ddCheck", driver, "", "", null);
	  						
	  					}//End of catch
		  					//clickExpand.click(); 	  				
		  					 	  					
		  				}//End of For				
					}//End of If
		//}//End of try
		
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - apply_DropDownFilter
	//Description - This function applies specific filter value for either term or asset ype..
	//Argument - row num, filter name, asset type value, term type value
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	//public boolean apply_DropDownFilter(int rowNum, String filterName, String assetType_filterValue, String termType_filterValue){
	public boolean apply_DropDownFilter(int rowNum, String filterName, String filterValue){
				
		try{
			if((filterValue.equals("")) || (filterValue.equals("##"))){
				//System.out.println("ResultTable_Section >> apply_DropDownFilter >> Filter Value is blank thus not applied.");
				return true;
			}
			//create an array if filter names to be applied are more than 1 seprated by semi colon.
			//This part of multiple filter is not used anymore. made it more generic
			String[] strSelectOptions;
			if (filterName.contains(";")) {
				strSelectOptions = filterName.split(";");
				System.out.println("apply filter >> Multiple Filter scenario - " + filterName);
			}else{
			//System.out.println("apply filter >> Single Filter scenario " + filterName);
			strSelectOptions = new String[1];
			strSelectOptions[0] = filterName;
			}
			//for each filter types asset type n term type, apply the filter options
			for(String strFilterOption:strSelectOptions){
				switch (strFilterOption.trim()){
				case "All Asset Types": 
						WM_CommonFunctions.clickItem(ddTab_AllAssetTypeFilter, "Asset DropDown", rowNum);
						//ddTab_AllAssetTypeFilter.click();
						if(WM_CommonFunctions.dropwdown_ClickOn_CheckboxOptions(ddTab_AllAssetTypeFilter, dd_AllAssetTypes, filterValue, rowNum)){
							System.out.println("apply filter >> All asset types filter applied with values = " + filterValue);
						}else{
							System.out.println("apply filter >> All asset type filter = " + filterValue + " could not be applied due to missing flter tab or filter value");
							Reporting.reportStep(rowNum, "Failed", "All Asset Types = " + filterValue + ", could not be applied", "", "", driver, "", "", null);
							return false;
						}
						break;
				case "All Term Types":
					WM_CommonFunctions.clickItem(ddTab_AllTermTypeFilter, "All Term DropDown", rowNum);
					//ddTab_AllTermTypeFilter.click();
					if(WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_AllTermTypes, filterValue)){
						//All Term Types filter does not requires any other click after selecting an option. It automatically displays the results, once an option is clicked.
						System.out.println("apply filter >> All term types filter applied with value = " + filterValue);
					}else{
						System.out.println("apply filter >> All Term type filter = " + filterValue + " could not be applied due to missing flter tab or filter value");
						Reporting.reportStep(rowNum, "Failed", "All Term Types = " + filterValue + ", could not be applied", "", "", driver, "", "", null);
						return false;
					}
					break;
				case "Year":
					WM_CommonFunctions.clickItem(ddTab_YearFilter, "Year - Current/Prior DropDown", rowNum);
					//ddTab_YearFilter.click();
					if(WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_YearTypes, filterValue)){
						//Year filter does not requires any other click after selecting an option. It automatically displays the results, once an option is clicked.
						System.out.println("apply filter >> Year -Prior/Current filter applied with value = "+ filterValue);
					}else{
						System.out.println("apply filter >> Year - Prior/Current filter = " + filterValue + " could not be applied due to missing flter tab or filter value");
						Reporting.reportStep(rowNum, "Failed", "Year = Prior OR Current filter = " + filterValue + ", could not be applied", "", "", driver, "", "", null);
						return false;
					}
					break;	
				case "All Statuses":
					WM_CommonFunctions.clickItem(ddTab_AllStatusesFilter, "All Statuses", rowNum);
					//ddTab_AllStatusesFilter.click();
					if(WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_AllStatuses, filterValue)){
						//All Statuses filter requires some place to be clicked after selecting an option
						ddTab_AllStatusesFilter.click();
						System.out.println("apply filter >> All Statuses filter applied with value = "+ filterValue);
					}else{
						System.out.println("apply filter >> All Statuses = " + filterValue + " could not be applied due to missing flter tab or filter value");
						Reporting.reportStep(rowNum, "Failed", "All Statuses filter = " + filterValue + ", could not be applied", "", "", driver, "", "", null);
						return false;
					}
					break;
				case "All Order Types":
					WM_CommonFunctions.clickItem(ddTab_AllOrderTypesFilter, "All Order Type", rowNum);
					//ddTab_AllOrderTypesFilter.click();
					if(WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_AllOrderTypes, filterValue)){
						//All Order Type filter requires some place to be clicked after selecting an option
						ddTab_AllOrderTypesFilter.click();
						System.out.println("apply filter >> All Order Types filter applied with value = "+ filterValue);
					}else{
						System.out.println("apply filter >> All Order Typest filter = " + filterValue + " could not be applied due to missing flter tab or filter value");
						Reporting.reportStep(rowNum, "Failed", "All Order Types = " + filterValue + ", could not be applied", "", "", driver, "", "", null);
						return false;
					}
					break;
				case "Held at USBI":
					WM_CommonFunctions.clickItem(ddTab_HeldAwayFilter, "Held at USBI", rowNum);
					//ddTab_AllOrderTypesFilter.click();
					if(WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_HeldAway, filterValue)){
						System.out.println("apply filter >> Held Away with value = "+ filterValue);
					}else{
						System.out.println("apply filter >> Held Away filter = " + filterValue + " could not be applied due to missing flter tab or filter value");
						Reporting.reportStep(rowNum, "Failed", "Held Away = " + filterValue + ", could not be applied", "", "", driver, "", "", null);
						return false;
					}
					break;
				default: 
				System.out.println(strFilterOption + " - Filter name does not exists. Method : apply_DropDownFilter | Class : ResultTable_Section. Please check");
				Reporting.reportStep(rowNum, "Failed", filterName + " - not found in expected list of filter names. Check Swich stmnt.", "", "", driver, "", "", null);	
				return false;
				}// End of switch stmnt
			}// End of For loop
			//Thread.sleep(3000);
			return true;
		}// End of Try
		catch(Exception e){
			System.out.println("Exception found in method >>apply_DropDownFilter, class = ResultTable_Section. Please check!!! ");
			//e.printStackTrace();
			return false;
		}// End of catch
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_AllColValues_QualifyThisTermType
	//Description - This function verifies if the Term of a row matches the Term Type filter applied based on Acquired date value.
	//Argument - Applied Term type filter (Long Temm or Short term)
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean verify_AllColValues_QualifyThisTermType(int rowNum, String strAllTermTypeValue){
		try{
			String failedTermRows = "";
			
			//Get current date
			Date currentDate = new Date();
			
			//Get all values from acquired date column in a list
			String str_ActualTerm = "";
			int colNum =  get_ColIndex("Date Acquired");
			List list_allColValues= driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ")"));
			List<WebElement> allOptions = list_allColValues;
			//use a for loop to check if each acquired date value qualifies the applied term
			Date fromDate = null;
			boolean flg_termtestPass = false;
			String actualDate="";
			for(int iLoop=0; iLoop<allOptions.size()-1; iLoop++){
				
				actualDate = (allOptions.get(iLoop).getText()).replace("\n", " ");
				//System.out.println("Verifying Term column >> Date from ui = " + actualDate);
				//do nothing if the cell value is Multiple
				if(!actualDate.equalsIgnoreCase("Multiple")&& !actualDate.equals("-") ){
					fromDate = WM_CommonFunctions.convert_StringTo_Date(actualDate);
					//System.out.println("From Date = " + fromDate.toString());
					//based on acquired date and current date, verify if a txn is long term or short term
					str_ActualTerm = WM_CommonFunctions.return_LongTermShortTerm_DateDiff(fromDate, currentDate);
					if(strAllTermTypeValue.equalsIgnoreCase(str_ActualTerm)){
						System.out.println("Termtypes check has Passed. Actual / Expected = " + str_ActualTerm + " / " + strAllTermTypeValue);
						if(failedTermRows.equals("")){
							flg_termtestPass = true;
						}
					}else{
						failedTermRows = failedTermRows + "; " + Integer.toString(iLoop);
						System.out.println("Term type test has failed");
						flg_termtestPass = false;
					}// End of nested if-else
				}// End of Parent If stmnt
	
			}// End of For Loop
			if(!failedTermRows.equals("")){
				Reporting.reportStep(rowNum, "Failed", "Failed rows for incorrect Term Type filter - " + strAllTermTypeValue + " are " + failedTermRows , "", "", driver, "", "", null);		
			}
			return flg_termtestPass;
		}//End of Try
		catch(Exception e){
			System.out.println("Exception found in method >>get_AllColValues, class = ResultTable_Section. Please check!!! ");
			e.printStackTrace();
			return false;
		}// End of Catch
		
			
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_AllColValues_QualifyYearValue
	//Description - This function verifies if the Term of a row matches the Term Type filter applied based on Acquired date value.
	//Argument - Applied Term type filter (Long Temm or Short term)
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean verify_AllColValues_QualifyYearValue(int rowNum, String strYearValue) {
		// TODO Auto-generated method stub
		try{
			//get current Year
			DateUtils DateUtils = new DateUtils();
			String strCurrentYear = DateUtils.getCurrentDateTime_Parameters("Year");
			String failedYearRows = "";
			

			//Get all values from acquired date column in a list			
			int colNum =  get_ColIndex("Date Sold");
			List list_allColValues= driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ")"));
			List<WebElement> allOptions = list_allColValues;
			//use a for loop to check if each acquired date value qualifies the applied term
			
			String strActualDateSold = "";
			String [] str_ActualSoldYear = null;
			String actulSoldYr = "";
			
			
			boolean flg_yearTestPass = false;
			String actualDate="";
			for(int iLoop=0; iLoop<allOptions.size()-1; iLoop++){
				str_ActualSoldYear = null;
				strActualDateSold = (allOptions.get(iLoop).getText()).replace("\n", " ");
				strActualDateSold = strActualDateSold.trim();
				//System.out.println("Verifying Term column >> Date Sold from ui = " + actualDate);
				//do nothing if the cell value is Multiple
				if(!strActualDateSold.equalsIgnoreCase("Multiple")&& !strActualDateSold.equals("")){
					str_ActualSoldYear = strActualDateSold.split(" ");
					//get Year value
					actulSoldYr = str_ActualSoldYear[2];
					//compare is yer value frm Date Sold is smaller or equal to current year
					if(strYearValue.equalsIgnoreCase("Current Year")){
						if(actulSoldYr.equalsIgnoreCase(strCurrentYear)){
							//System.out.println("Year filter check has Passed for Current Year. Actual / Expected = " + actulSoldYr + " / " + strCurrentYear);
							if(failedYearRows.equals("")){
								flg_yearTestPass = true;
							}
						}else{
							failedYearRows = failedYearRows + Integer.toString(iLoop);
							System.out.println("Year >> current/prior -  test has failed for Result table row - " + iLoop);
							flg_yearTestPass = false;
						}// End of nested if-else for current year check
					}else if(strYearValue.equalsIgnoreCase("Prior Year")){
						if(Integer.parseInt(actulSoldYr) < Integer.parseInt(strCurrentYear)){
							System.out.println("Year filter check has Passed for Prior Year. Actual / Expected = " + actulSoldYr + " / " + strCurrentYear);
							if(failedYearRows.equals("")){
								flg_yearTestPass = true;
							}
						}else{
							failedYearRows = failedYearRows + "; " + Integer.toString(iLoop);
							System.out.println("Year >> current/prior - test has failed for Result table row - " + iLoop);
							flg_yearTestPass = false;
						}// End of nested if-else for prior year check
					}
					
				}// End of Parent If stmnt
	
			}// End of For Loop
			if(!failedYearRows.equals("")){
				Reporting.reportStep(rowNum, "Failed", "Failed rows for incorrect Year filter - " + strYearValue + " are " + failedYearRows , "", "", driver, "", "", null);		
			}
			return flg_yearTestPass;
		}//End of Try
		catch(Exception e){
			System.out.println("Exception found in method >>verify_AllColValues_QualifyYearValue, class = ResultTable_Section. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception found in Year = Current or Prior filter check " , "", "", driver, "", "", null);		
			e.printStackTrace();
			return false;
		}// End of Catch

	}
	//######################################################################################################################
		//######################################################################################################################
		//Name - verify_AllColValues_QualifyYearValue
		//Description - This function verifies if a given transaction is result table is within a date range.
		//Argument - row num, to and from date
		//Created By - Neelesh
		//######################################################################################################################
		//######################################################################################################################
		public boolean verify_AllColValues_QualifyYearRange(int rowNum, String fromDate, String toDate) {
			// TODO Auto-generated method stub
			try{
				DateUtils DateUtils = new DateUtils();
				String failedYearRows = "";
				boolean flg_yearTestPass = false;
				
				//Get all values from Sold Date column in a list			
				int colNum =  get_ColIndex("Date Sold");
				List list_allColValues= driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ")"));
				List<WebElement> allOptions = list_allColValues;
				String testDate="";
				int monthNum;
				String arrDate[] = new String[3];
				boolean isBetweenRange;
				//String arrDate[]= new String[3];
				//Start a for loop and get actual date value from list and verify if its in a range
				for(int iLoop=0; iLoop<allOptions.size()-1; iLoop++){
					//replace new line from date value with space
					testDate = (allOptions.get(iLoop).getText()).replace("\n", " ");
					testDate = testDate.trim();
					//skip if value is either blank or Multiple
					if(!testDate.contains("Multiple") && !(testDate.trim().equals(""))){
						//Split the date string, get the Month name value and get the Month number from  that
						arrDate = testDate.split(" ");
						monthNum = DateUtils.getMonth_Number_FromName(arrDate[0]);
						// convert to date to mm/dd/yyyy type string and then convert that to a date type
						testDate = monthNum + "/" + arrDate[1] + "/" + arrDate[2];
						//convert to date type
						Date newTestDate = DateUtils.convertString_to_Date(testDate);
						//System.out.println("Test date - " + newTestDate);
						//get toDate and convert to Date type
						Date newToDate = DateUtils.convertString_to_Date(toDate);
						//System.out.println("to date - " + newToDate);
						//get from date and convert to Date type
						Date newFromDate = DateUtils.convertString_to_Date(fromDate);
						//System.out.println("from date -" + newFromDate);
						//use the function to check if the test date is in range
						isBetweenRange = DateUtils.isWithinRange(newTestDate, newFromDate, newToDate);
					//return true or false
						//if((newTestDate.compareTo(newFromDate) >= 0 && newTestDate.compareTo(newToDate) <= 0)){
						if(isBetweenRange){	
							//System.out.println("Passed - Date Sold >> " + testDate + " is within required date range.");
							//return true;
						}else{
							System.out.println("Failed - Date Sold >> " + testDate + " is not within required Date Range");
							failedYearRows = failedYearRows + ";" + Integer.toString(iLoop+1);
							
						}
					
					}//End of If loop to check - Multiple
				}// End of For Loop
				if(!failedYearRows.equals("")){
					Reporting.reportStep(rowNum, "Failed", "Failed rows for incorrect Date Range filter are - "  + failedYearRows , "", "", driver, "", "", null);		
					return false;
				}
				return true;
			}//End of Try
			catch(Exception e){
				System.out.println("Exception found in method >>verify_AllColValues_QualifyYearRange, class = ResultTable_Section. Please check!!! ");
				Reporting.reportStep(rowNum, "Failed", "Exception found in Date Range filter." , "", "", driver, "", "", null);		
				//e.printStackTrace();
				return false;
			}// End of Catch

		}
		//######################################################################################################################
		//Name - applyFilter
		//Description - 
		//Argument - row num, to and from date
		//Created By - Neelesh
		//######################################################################################################################
		/*public void applyFilter(int rowNum, String filterName, String filterVal, String strClient) throws Exception{
		try{
		
			if( filterName.equals("Account")){
				WebElement dd_Btn  = null;
				if(strClient.equals("LPL")){
					 dd_Btn = ddTab_AllAccountsFilter3;
				}
				WM_CommonFunctions.clickItem(dd_Btn, "Account DropDown", rowNum);
			}//end of if
			else if(!apply_DropDownFilter(rowNum, filterName, filterVal)){
				 System.out.println(filterName + " = " + filterVal + " >> dropdown filter could not be applied. Skiping further execution.");
				 throw new SkipException("Dropdown filter could not be applied");
			 }//End of IF
			
		}//Try
		catch(Exception e){
			System.out.println("Exception found in method >>applyFilter, class = ResultTable_Section. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception found in [applyFilter] mthod." , "", "", driver, "", "", null);		
			//e.printStackTrace();
		}
			
	}*/
	//######################################################################################################################
	//Name - Pagination
	//Description - this function checks pagination by clicking numbers, Next and Previous
	//Argument - row num, acnt num, test name, test value and client name
	//Created By - Neelesh
	//######################################################################################################################
	public void Pagination(String indexVal, int rowNum) throws Exception {
		try {
			String currMktVal, currSymbol, preMktVal, preSymbol;
			
			// getting mkt value and symbol object to compare change with pagination progress
			WebElement obj_mkval = driver.findElement(By.xpath("//table/tbody/tr[@data-toggle-row='0']/td[@data-col-name = 'marketValue']"));
			WebElement obj_Symbolval = driver.findElement(By.xpath("//table/tbody/tr[@data-toggle-row='0']/td[@data-col-name = 'symbol']"));	
			//getting size of pagination bar
			int listSize = pagination_Values.size();
			int index = 0;
			if(indexVal.equals("Next")) {
				index = listSize-1;
			}else if(indexVal.equals("Previous")){
				index = 0;
			}
			//comparing 1st cell value for mkt Value n Symbol. 
			preMktVal = obj_mkval.getText();
			preSymbol = obj_Symbolval.getText();
			
			currMktVal = "";
			currSymbol = "";
			
			for(int i = 2; i<(listSize-1); i++) {
				//verifying by clicking on index value
				if(indexVal.trim().equals("index")) {
					System.out.println("Pagination check - Using index number.");
					index = i;
				}
				TimeUnit.SECONDS.sleep(5);
				
				WM_CommonFunctions.clickItem(pagination_Values.get(index), "Pagination index", rowNum);
				Thread.sleep(50);
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				 
				 //catching stale element ref when moving to next page
				 try {
					currSymbol = obj_Symbolval.getText();
					currMktVal =obj_mkval.getText();
				 }catch(org.openqa.selenium.StaleElementReferenceException ex) {
					obj_mkval = driver.findElement(By.xpath("//table/tbody/tr[@data-toggle-row='0']/td[@data-col-name = 'marketValue']"));
					obj_Symbolval = driver.findElement(By.xpath("//table/tbody/tr[@data-toggle-row='0']/td[@data-col-name = 'symbol']"));	
					currSymbol = obj_Symbolval.getText();
					currMktVal =obj_mkval.getText();
				 }
				
				if(currSymbol.equals(preSymbol) && currMktVal.equals(preMktVal)) {
					System.out.println("FAILED - Page Navigation check - Previous page and current page values are same. Please check Navigation manually!!");
					Reporting.reportStep(rowNum, "Failed", "Pagination failed after clicking on page index = " + indexVal +" >> iteration - " + i +". Please check!!", "", "", driver, "", "", null);	
				}else {
					System.out.println("PASSED - Page Navigation check has Passed for " + indexVal + " >> " + i);
					Reporting.reportStep(rowNum, "Passed", "Pagination has Passed for - " + indexVal +" >> iteration - " + i, "", "", driver, "", "", null);	
				}
				preMktVal = currMktVal;
				preSymbol = currSymbol;
			}
		}catch(Exception e) {
			System.out.println("FAILED - Exception found in method >Pagination, class = Result table. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception Found. @mthod[Pagination], @class[ResulTable_Section]. Please check!!", "", "", driver, "", "", null);	
			e.printStackTrace();
  			
		}
	}
	//######################################################################################################################
	//Name - verify_PaginationWorks
	//Description - this function checks pagination by clicking numbers, Next and Previous
	//Argument - row num
	//Created By - Neelesh
	//######################################################################################################################
	public void	verify_PaginationWorks(int rowNum) throws Exception {
		//check Pagination for numbers
		Pagination("index",  rowNum);
		
		//go to default 1st page
		pagination_Values.get(1).click();
		
		//check Pagination with Next button
		Pagination("Next",  rowNum);
		
		//check Pagination with Previous button
		Pagination("Previous",  rowNum);
		
	}
	//######################################################################################################################
	//Name - checkCol_OrderSortAndPagination
	//Description - this function checks result table columns, its order, col sort and pagination
	//Argument - row num, acnt num, test name, test value and client name
	//Created By - Neelesh
	//######################################################################################################################
		public void checkCol_OrderSortAndPagination(int rowNum, String accountNo, String strTestName, String strTestValue, String strClient) {
		try {
			
			switch(strTestName) {
			case "Sort Ascending":
			case "Sort Descending":
				checkCol_Sorting(rowNum, strTestName, strTestValue, strClient);
				break;	
			case "Pagination":
				TimeUnit.SECONDS.sleep(10);
				System.out.println("Verifying Pagnation..");
				if(CommonUtils.isElementPresent(obj_PaginationBar)) {
					verify_PaginationWorks(rowNum);
				}else {
					
					Reporting.reportStep(rowNum, "Failed", "Pagination bar is not displayed. Please check!!", "", "", driver, "", "", null);	
				}
				break;
			case "Column Names":
				//get actual col names
				TimeUnit.SECONDS.sleep(10);
				String actualNames = "";
				if(strClient!="1DB") {
					actualNames = get_ColumnNames(1);
				}else {
					actualNames = WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, list_ColNames_1DB, null);
				}
				actualNames = actualNames.replace("\n", "");
				actualNames = actualNames.replace(" ", "");
				strTestValue = strTestValue.replace(" ", "");
				//compare expected col name list
				System.out.print("Actual names = " + actualNames);
				if(actualNames.equals(strTestValue)) {
					System.out.println("PASSED - ALL Col names are present in expected order.");
					Reporting.reportStep(rowNum, "Passed", "Col Names check has Passed. >> " + actualNames , "", "", driver, "", "", null);		
					
				}else {
					System.out.println("FAILED - Col names check has failed. Actual values/Exp = " + actualNames + "/ " + strTestValue);
					Reporting.reportStep(rowNum, "Failed", "Column Names are not in Expected order. Please check!! Actual / Expected = " + actualNames + " / " +  strTestValue, "", "", driver, "", "", null);		
					
				}
				break;
				
			}
		}catch(Exception e) {
			System.out.println("FAILED - Exception found:  @method = checkCol_OrderSortAndPagination, class = ResultTable_Section. Please check!!! ");
			Reporting.reportStep(rowNum, "Failed", "Exception found in [checkCol_OrderSortAndPagination] method." , "", "", driver, "", "", null);		
			e.printStackTrace();
			}
		}
		
		//######################################################################################################################
		//Name - checkCol_Sorting
		//Description - this function checks sorting on column names
		//Argument - row num, test name, test value and client name
		//Created By - Neelesh
		//######################################################################################################################
		public boolean checkCol_Sorting(int rowNum, String strTestName, String strTestValue, String strClient) {
			try {
				String colName = "";
				String sortType = "";
					
				//get col names to be sorted
				 TimeUnit.SECONDS.sleep(10);
				if(strTestValue.equals("All")) {
					colName = WM_CommonFunctions.get_DefaultColNames(strClient);
				}else {
					colName = strTestValue;
				}
				//create an array of col names
				String[]colNames = colName.split(";");
				//get sort type
				if(strTestName.contains("Ascending")) {
					 sortType = "Asc";
				}else {
					 sortType = "Desc";
				}
				//apply sort on "Description" col to avoid default sorted issues
				if(!strClient.equals("RWB")) {
					apply_ColumnSort(rowNum, "Description",  sortType, strClient);
				}else {
					TimeUnit.SECONDS.sleep(10);
					apply_ColumnSort(rowNum, "Security Name",  sortType, strClient);
				}
				TimeUnit.SECONDS.sleep(20);	
				
				//for each col name, first apply the sort and then verify the sort
				for(String col : colNames) {
					
					if(col.contains("Symbol") && (strClient!="1DB")) {
						//filter out cash equivalent if col name is symbol
						String strAllAssetTypeValue = get_AssetFilterValues_WithoutCash(strClient);
						apply_DropDownFilter(rowNum, "All Asset Types", strAllAssetTypeValue);
					}//end of if
					//apply sort
					//System.out.println("Applying Sort on col name - " + col + " ...");
					if(apply_ColumnSort(rowNum, col,  sortType, strClient)) {
						 TimeUnit.SECONDS.sleep(20);	
						WM_CommonFunctions.wait_ForPageLoad(rowNum);
					//verify sort
						System.out.println("Verifying Sort on col - " + col);
						verify_ColumnSort(rowNum, col, sortType, strClient);
					}
					if(col.contains("Symbol")) {
						//filter out cash equivalent if col name is symbol
						String strAllAssetTypeValue = get_AssetFilterValues_WithoutCash(strClient);
						apply_DropDownFilter(rowNum, "All Asset Types", strAllAssetTypeValue);
					}
				}
				return false;
				
			}catch(Exception e) {
				e.printStackTrace();
				Reporting.reportStep(rowNum, "Failed", "Exception found. @method[checkCol_Sorting], @class[ResultTable_Section]" , "", "ColSorting", driver, "", "", null);
				System.out.println("FAILED - Exception found in @checkCol_Sorting, @class[Resulttable_Section]");
				return false;
			}
			
		}//end of function
		
	int get_Totals_RowNum(int rowNum, String strClient) {
		int value = -1;
		String colName = "";
		switch(strClient) {
		case "STF":
			colName = "Asset";
			break;
		case "USB":
		case "BOW":
		case "DAV":
			colName = "Symbol";
			break;
		}
		String allValues = get_AllColValues(rowNum, colName);
		String[] arr = allValues.split(";");
		for(int i = 0; i < arr.length; i++) {
			if(arr[i].contains("Totals")) {
				return i;
			}
		}
		
		return value;
	}
	String	get_AssetFilterValues_WithoutCash(String strClient) {
		String result = "";
		switch(strClient) {
		case "USB":
			result = "Equities;Options;Mutual Funds;Fixed Income;Other";
			break;
		case "STF":
			result = "Stocks;Options;Bonds;Mutual Funds;Other Investments";
			break;
		case "DAV":
			result = "Stocks;Options;Preferred Stocks;Tax-Exempt Bonds;Taxable Bonds;Mutual Funds;Unit Investment Trusts;Annuities;Other Investments";
			break;
		case "RWB":
			result = "Alternatives;Equities;Fixed Income;Multi-Asset;Other;Satellites";
			break;
		case "BOW":
			result = "Equities;Options;Preferred Stocks;Tax-Exempt Bonds;Taxable Bonds;Mutual Funds;Unit Investment Trusts;Annuities;Other Investments";
			break;
		default:
			System.out.println("FAILED : @method -get_AssetFilterValues_WithoutCash>> Valid switch case optionis missing.");
			result = "";
		}
			
		return result;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - clickActionMenu
	//Description - This function clicks on Action Menu for a given symbol
	//Argument - symbol, col name
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	boolean applyFilter_VerifyNoReuslt(int rowNum, String filterName, String filterValue) throws InterruptedException {	
		//apply sec value filter and check for no results found
		if(!apply_DropDownFilter(rowNum, filterName, filterValue)){
			 return false;
		 }//End of IF
		if(WM_CommonFunctions.noResultFound()) {
			Reporting.reportStep(rowNum, "Failed", "No Results found for filter =>> " + filterValue + "." , "", "holding_QuickQuote", driver, "", "", null);
			System.out.println("No Result found thus exiting test for sec value =>> " + filterValue);
			return false;
		}
		return true;
	}
		
	
}//end of class
 