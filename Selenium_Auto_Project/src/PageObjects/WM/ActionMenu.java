package PageObjects.WM;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;

public class ActionMenu {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	ResultTable_Section ResultTable_Section = new ResultTable_Section();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	
	
	//************************   Test Objects   ************************
	//******************************************************************
	
	
	//pop-up window
	@FindBy(how = How.XPATH, using = ".//div[@class='popover-content']")
	public WebElement popup_QQuote;
	
	
	
	@FindBy(how = How.CSS, using = ".large.bright.desc")
	public WebElement symbol_Descripton;
	
	@FindBy(how = How.XPATH, using = ".//*[contains(@class, 'symbol large')]")
	public WebElement qq_symbol_title;
	
	@FindBy(how = How.XPATH, using = ".//dt[contains(@class, 'symbol')]")
	public WebElement qq_col_Symbol;
	
	@FindBy(how = How.XPATH, using = ".//dt[contains(@class, 'lastPrice')]")
	public WebElement qq_col_lastPrice;
	
	@FindBy(how = How.XPATH, using = ".//dt[contains(@class, 'change')]")
	public WebElement qq_col_DayChange;
	
	
	@FindBy(how = How.CSS, using = ".svi-detailed-quote .row dl dt")
	public List<WebElement> qq_col_Names;
	
	@FindBy(how = How.XPATH, using = ".//div//ul[contains(@class, 'svi-action-bar nav')]/li/a")
	public List<WebElement> qq_nav_bar;
	
	@FindBy(how = How.CSS, using = ".actionBar ul.dropdown-menu li")
	public List<WebElement> qq_dd_Trade;
	
	@FindBy(how = How.XPATH, using = ".//ul[contains(@class, 'svi-action-bar')]/li[1]/a")
	public WebElement trade_Link;
	
	/*@FindBy(how = How.XPATH, using = ".//ul[contains(@class, 'sal-nav-horizontal')]/li[1]/a")
	public WebElement symbol_Report;*/
	
	@FindBy(how = How.XPATH, using = ".//div[contains(@class,'symbol-class-input')]")
	public WebElement symbol_Report;
	
	
	@FindBy(how = How.XPATH, using = ".//*[@id='optionChainQuote']/dd[1]")
	public WebElement symbol_OptionChains;
	
	@FindBy(how = How.XPATH, using = ".//span[@class='mainChart-tickerName']")
	public WebElement symbol_Charts;
	
	
	@FindBy(how = How.XPATH, using = ".//ul[contains(@class,'svi-action-bar')]//li/a[contains(text(),'Chain')]")
	public WebElement qq_Chains;
	
	@FindBy(how = How.XPATH, using = ".//ul[contains(@class,'svi-action-bar')]//li/a[contains(text(),'Report')]")
	public WebElement qq_Report;
	
	@FindBy(how = How.XPATH, using = ".//ul[contains(@class,'svi-action-bar')]//li/a[contains(text(),'Chart')]")
	public WebElement qq_Chart;
	
	
	//--------------  Constructor to initialize all elements  --------------
	public ActionMenu() {
		this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
	}
	//--------------  Constructor to initialize all elements  --------------
	
	//##############################################     FUNCTIONS       ###################################
	//######################################################################################################
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_ActionMenu
	//Description - This function verifies Quick Quote features.
	//Argument - row no:, clients name, acnt no, symbol, exp descripton, filter name, sec value, exp options and exp trade links
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void verify_QuickQuote(int rowNum, String strClient, String acntNo, String symbol, String expDesc, String filterName, String secValue, String expOptionLinks, String expTradeLinks) throws InterruptedException {
		try {
				//apply sec value filter and check for no results found
				if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Asset Types", secValue)){
					 return;
				 }//End of IF
				if(WM_CommonFunctions.noResultFound()) {
					Reporting.reportStep(rowNum, "Failed", "No Results found for filter =>> " + secValue + "." , "", "holding_QQuoteMenu", driver, "", "", null);
					System.out.println("No Result found thus exiting test for sec value =>> " + secValue);
					return;
				}
				Thread.sleep(100);
				 WM_CommonFunctions.wait_ForPageLoad(rowNum);	
				
				// find the row with symbol and click on the Qucik Quote menu
				 clickQuickQuote(symbol, "Symbol");
				Thread.sleep(1500);
				Thread.sleep(1000);
				 WM_CommonFunctions.wait_ForPageLoad(rowNum);
				//if Qucik Quote is missing or does not have data >> Fail the test case and return
				if(CommonUtils.isElementPresent(popup_QQuote)&& CommonUtils.isElementPresent(qq_symbol_title)) {
					//Qucik Quote has successfully opened up
					Reporting.reportStep(rowNum, "Passed", "Quick Quote window has successfully opened up." , "", "holding_QQuote", driver, "", "", null);
					System.out.println("Passed - Quick Quote window has successfully opened up for symbol - " + symbol);
				}else {
					Reporting.reportStep(rowNum, "Failed", "Quick Quote window did not open successfully for symbol - " + symbol + "." , "", "holding_QQuote", driver, "", "", null);
					System.out.println("Failed - Quick Quote window did not open for symbol  =>> " + symbol);
					return;
				}
				
				//function to check - Symbol and Description
				System.out.println("***************   Quick Quote >> Verifying option menu Symbol and Description   ***************");
				check_QQ_Symbol(rowNum, symbol, expDesc);
				Thread.sleep(500);
				//verify col names, option and trade links
				System.out.println("***************   Quick Quote >> Now verifying Col Names, Trade links and Option Links   ***************");
				verify_ColNames_Options_TradeLinks(rowNum, strClient, expOptionLinks, expTradeLinks);
				Thread.sleep(500);
				
				//verify each option link >> click and page title  & symbol value
				System.out.println("***************   Quick Quote >> Now verifying all Option Links   ***************");
				verify_eachOptionPage(rowNum, strClient, symbol, expOptionLinks, secValue);
				Thread.sleep(500);
				
				//verify trade widget for each transaction type
				System.out.println("***************   Quick Quote >> Now verifying Trade Widget   ***************");
				verify_AM_TradeWidget(rowNum, symbol, expTradeLinks);
				
				System.out.println("###############################################################");
				System.out.println("###############################################################");
		}catch(Exception e) {
			Reporting.reportStep(rowNum, "Failed", "Exception found in >> @method = verify_QuickQuote  and @class = ActionMenu " , "", "holding_QQuote", driver, "", "", null);
			System.out.println("Exception found in >> @method = verify_QuickQuote  and @class = ActionMenu");
			e.printStackTrace();
		}
		
	}//end of function
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_AM_TradeWidget
	//Description - This function verifies Trade Widget from Quick Quote.
	//Argument - row no:, symbol and exp trade links
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void verify_AM_TradeWidget(int rowNum, String symbol, String expTradeLinks) throws InterruptedException {
		try {
			Trade_Widget tw = new Trade_Widget();
			//click on Trade Link
			trade_Link.click();
			List<WebElement> Options = qq_dd_Trade;
			WebElement option=null;
			StringBuffer sb = new StringBuffer();
			sb = sb.append("");
			String currOption = "";
			String currActionName="";
			for(int i = 0; i < Options.size(); i++) {
				Options=qq_dd_Trade;
				option = Options.get(i);
				WM_CommonFunctions.wait_dynamicFluentWait(rowNum, option, "Quick Quote - Trade Link");
				//click on the option
				currOption = option.getText();
				System.out.println("		Verifying Trade Widget for - " + currOption);
				sb = sb.append(currOption+";");
				option.click();
				Thread.sleep(1000);
				
				//verify trade widget window opens
				WM_CommonFunctions.wait_dynamicFluentWait(rowNum, tw.tw_popup_TradeWidget, "Quick Quote - Trade widget popup");
				if(CommonUtils.isElementPresent(tw.tw_popup_TradeWidget)){
					Reporting.reportStep(rowNum, "Passed", "Trade Widget has successfully opened up." , "", "", driver, "", "", null);
					System.out.println("Passed - Trade widget window has successfully opened up for symbol - " + symbol);
				}else {
					Reporting.reportStep(rowNum, "Failed", "trade widget widnow did not open successfully for symbol - " + symbol + "." , "", "holding_QQuote", driver, "", "", null);
					System.out.println("Failed - Trade widget did not open for symbol  =>> " + symbol);
					return;
				}
				//verify symbol is correct
				if(tw.tw_Symbol.getText().equals(symbol)) {
					Reporting.reportStep(rowNum, "Passed", "Trade Widget symbol is correct" , "", "", driver, "", "", null);
					System.out.println("Passed - Trade widget window dispays correct symbol - " + symbol);
				}else {
					Reporting.reportStep(rowNum, "Failed", "trade widget window does not display expected symbol - " + symbol + "." , "", "QQuote_TW_Action", driver, "", "", null);
					System.out.println("Failed - Trade widget does not display expected symbol  =>> " + symbol);
				}
				//verify Action = Buy, Sell, Sell All etc is correct
				currActionName = tw.tw_Action.getText();
				if(currActionName.equals(currOption)){
					Reporting.reportStep(rowNum, "Passed", "Trade Widget Action is correct - " + currOption, "", "", driver, "", "", null);
					System.out.println("Passed - Trade widget window dispays correct Action - " + currOption);
				}else {
					Reporting.reportStep(rowNum, "Failed", "Trade widget window does not display expected Action - " + currOption + "." , "", "QQuote_TW_Action", driver, "", "", null);
					System.out.println("Failed - Trade widget does not display expected Action  =>> " + currOption);
				}
				
				//close trade widget
				tw.tw_close.click();
				Thread.sleep(500);
				//open Quick Quote
				clickQuickQuote(symbol, "Symbol");
				Thread.sleep(500);
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				trade_Link.click();
				Thread.sleep(500);
			}//end of for	
			trade_Link.click();
			//verify all options were verified
			
			if(!(sb.toString().contains(expTradeLinks))) {
				Reporting.reportStep(rowNum, "Failed", "Quick Quote - All Tarde Links were not verified. Actual/Exp >> "  + sb.toString() + "/" + expTradeLinks, "", "QQuote_TW", driver, "", "", null);
				System.out.println("Failed :Quick Quote - All Page Options were not verified");
			}else {
				System.out.println("All expected Trade link have been tested - " + expTradeLinks);
			}//end of if-else
			
		}catch(Exception e){
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", "Exception found in -  method-verify_AM_TradeWidget", "", "QQuote_TW", driver, "", "", null);
			System.out.println("Exception Thrown in method-verify_AM_TradeWidget @class ActionMenu");
		}//end of try-catch
	}//end of function


	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_eachOptionPage
	//Description - This function verifies all options pages from Quick Quote.
	//Argument - row no:, client, symbol and exp page options
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void verify_eachOptionPage(int rowNum, String strClient, String symbol, String strExpPgOptions, String secValue) throws InterruptedException {
		try {
			//add allexpected option links in a list
			List<WebElement> Options = new ArrayList<WebElement>();
			if(strExpPgOptions.contains("Chain")) {
				Options.add(qq_Chains);
			}
			if(strExpPgOptions.contains("Chart")) {
				Options.add(qq_Chart);
			}
			if(strExpPgOptions.contains("Report")) {
				Options.add(qq_Report);
			}

			String strOptionName = "";
			for(WebElement option : Options) {
				//if(CommonUtils.isElementPresent(option)){
				Thread.sleep(500);
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
					strOptionName = option.getText();
					Thread.sleep(1000);
					if(strOptionName.contains("Trade")) {
						 continue;
					}
					option.click();
				//}
				Thread.sleep(100);
				
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				//get page title
				Thread.sleep(1000);
				verify_PageTitle_And_Symbol(rowNum, strOptionName, symbol);
				//navigate back
				driver.navigate().back();
				Thread.sleep(500);
				
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				ResultTable_Section.apply_DropDownFilter(rowNum, "All Asset Types", secValue);
				Thread.sleep(500);
				
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				clickQuickQuote(symbol, "Symbol");
				Thread.sleep(1000);
			}//end of For
		}catch(Exception e) {
		e.printStackTrace();
		System.out.println("Exception thrown in method - verify_eachOptionPage in @ ActionMenu");
		Reporting.reportStep(rowNum, "Failed", "Exception thrown in method - verify_eachOptionPage", "", "holding_QQuote", driver, "", "", null);
		}//end of try-catch
		
	}//end of function

	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_PageTitle_And_Symbol
	//Description - This function verifies page title and symbol on all options pages from Quick Quote.
	//Argument - row no:, page name, symbol
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void verify_PageTitle_And_Symbol(int rowNum, String optionVal, String symbol) throws InterruptedException {
		try {
			//verify page title
			SummaryPage SummaryPage = new SummaryPage();
			String expTitle = "";
			String actualTitle;
			switch(optionVal) {
			case "Chain":
				expTitle = "Option Chains";
				break;
			case "Stock Report":
				expTitle = "Stock Fundamentals Report";
				break;
			case "Chart":
				expTitle = "Interactive Chart";
				break;
			case "Fund Report":
				expTitle = "Mutual Fund / ETF Fundamentals Report";
				break;
			}//end of switch
			Thread.sleep(1000);
			 WM_CommonFunctions.wait_ForPageLoad(rowNum);
			// WM_CommonFunctions.wait_dynamicFluentWait( rowNum,  SummaryPage.page_Title,  "Page Title");
			actualTitle = (SummaryPage.page_Title.getText()).trim();
			
			if(expTitle.equals(actualTitle)) {
				Reporting.reportStep(rowNum, "Passed", "Quick Quote - Page title is correct for >> " + optionVal , "", "", driver, "", "", null);
				System.out.println("Passed : Quick Quote - Page title is correct for >> " + optionVal);
			}else {
				Reporting.reportStep(rowNum, "Failed", "Quick Quote - Page title is incorrect for >> " + optionVal + ". Expected/Actual = " + expTitle + "/" + actualTitle, "", "holding_QQuote", driver, "", "", null);
				System.out.println("Failed :Quick Quote - Page title is incorrect for >> " + optionVal);
				//return;
			}
			
			//verify symbol
			
			String actualSymbol = "";
			switch(optionVal) {
			case "Chain":
				actualSymbol = symbol_OptionChains.getText();
				break;
			case "Stock Report":
			case "Fund Report":
				actualSymbol = symbol_Report.getAttribute("data-value");
				//System.out.println("Actual Symbol = " + actualSymbol);
				break;
			case "Chart":
				actualSymbol = symbol_Charts.getText();
				break;
			}
			//System.out.print("exp Symbol = " + symbol);
			if(actualSymbol.equals(symbol)) {
				Reporting.reportStep(rowNum, "Passed", "Correct Symbol displayed." , "", "", driver, "", "", null);
				System.out.println("Passed - Correct symbol is displayed in given page.");
			}else {
				Reporting.reportStep(rowNum, "Failed", "Symbol Value doest match. Actual/Expected = "+  actualSymbol + "/"  + symbol, "", "holding_QQuoteSymbol", driver, "", "", null);
				System.out.println("Failed - Expected symbol is not displayed Actual / Exp - " + actualSymbol + "/" + symbol);
			}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception thrown in method - verify_PageTitle_And_Symbol in @ ActionMenu");
			Reporting.reportStep(rowNum, "Failed", "Exception thrown in method - verify_PageTitle_And_Symbol", "", "holding_QQuote", driver, "", "", null);
		}//end of try-catch
	}//end of function
	//######################################################################################################################
	//######################################################################################################################
	//Name - verify_ColNames_Options_TradeLinks
	//Description - This function verifies presence of all links for pages and trade widget listed in Quick Quote
	//Argument - row no:, client, exp page options and trade links
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void verify_ColNames_Options_TradeLinks(int rowNum, String strClient, String expOptions, String expTradeLinks) throws InterruptedException {
		//verify col names - Symbol, Price, Day Change
		String actualValue = "";
		actualValue = WM_CommonFunctions.get_AllListOptions( rowNum,  strClient,  "Quick Quote - Col name",  qq_col_Names);
		String expValues = "Symbol;Price;Day Change";
		
		if(actualValue.contains(expValues)) {
			Reporting.reportStep(rowNum, "Passed", "All col names are present in Quick Quote." , "", "", driver, "", "", null);
			System.out.println("Passed - Quick Quote window displays all expected col names.");
		}else {
			Reporting.reportStep(rowNum, "Failed", "Quick Quote window does not display all col names. Actual/Expected = "+  actualValue + "/"  + expValues, "", "holding_QQuoteCol", driver, "", "", null);
			System.out.println("Failed - Quick Quote window has missing col names");
		}
		//**********************************************************************************
		//**********************************************************************************
		
		//verify Options >> am_nav_ba;
		actualValue = WM_CommonFunctions.get_AllListOptions( rowNum,  strClient,  "Quick Quote - Nav Option",  qq_nav_bar);
		expValues = expOptions;
		
		if(actualValue.contains(expValues)) {
			Reporting.reportStep(rowNum, "Passed", "All col names are present in Quick Quote." , "", "", driver, "", "", null);
			System.out.println("Passed - Quick Quote window displays all expected Option names.");
		}else {
			Reporting.reportStep(rowNum, "Failed", "Quick Quote window does not display all option names. Actual/Expected = "+  actualValue + "/"  + expValues, "", "holding_QQuoteCol", driver, "", "", null);
			System.out.println("Failed - Quick Quote window has missing option names");
		}
		//**********************************************************************************
		//**********************************************************************************
				
		//verify trade link
		trade_Link.click();
		Thread.sleep(30);
		actualValue = WM_CommonFunctions.get_AllListOptions( rowNum,  strClient,  "Quick Quote - Nav Option",  qq_dd_Trade);
		expValues = expTradeLinks;
		
		if(actualValue.contains(expValues)) {
			Reporting.reportStep(rowNum, "Passed", "All col names are present in Quick Quote." , "", "", driver, "", "", null);
			System.out.println("Passed - Quick Quote window displays all expected Trade link names.");
		}else {
			Reporting.reportStep(rowNum, "Failed", "Quick Quote window does not display all Trade Link names. Actual/Expected = "+  actualValue + "/"  + expValues, "", "holding_QQuoteCol", driver, "", "", null);
			System.out.println("Failed - Quick Quote window has missing Trade names");
		}
		
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - clickQQuote
	//Description - This function clicks on Quick Quote for a given symbol
	//Argument - symbol, col name
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void clickQuickQuote(String strSymbol, String colName) {
		int colNum =  ResultTable_Section.get_ColIndex(colName);
		List<WebElement> symbolNames = driver.findElements(By.cssSelector("table tbody tr td:nth-child(" + colNum + ") a.svi-symbol.ignoreToggle"));
		String testSymbol;
		for(WebElement option : symbolNames) {
			//System.out.println(option.getText().trim());
			testSymbol =  option.getText().trim();
			
			if(testSymbol.equals(strSymbol)) {
				option.click();
  				break;
			}//End of if
		}//end of for
	}//end of function
	//######################################################################################################################
	//######################################################################################################################
	//Name - check_AM_Symbol
	//Description - This function verifiessymbol in Quick Quote
	//Argument - row num, symbol, desc
	//Returns - void
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################	
	public void check_QQ_Symbol(int rowNum, String expSymbol, String expDesc) {
		try {
			if(StringManipulation.compareString_Equality(qq_symbol_title.getText(), expSymbol)) {
				Reporting.reportStep(rowNum, "Passed", "Quick Quote window displays correct symbol >> " + expSymbol , "", "holding_QQuote", driver, "", "", null);
				System.out.println("Passed >> Quick Quote window dispays correct symbol - " + expSymbol);
				
				if(StringManipulation.compareString_Equality(symbol_Descripton.getText(), expDesc)) {
					Reporting.reportStep(rowNum, "Passed", "Quick Quote window displays correct symbol description>> " + expDesc , "", "holding_QQuote", driver, "", "", null);
					System.out.println("Passed >> Quick Quote window dispays correct symbol description - " + expDesc);
				}else {
					Reporting.reportStep(rowNum, "Failed", "Quick Quote window displays incorrect symbol description >> " + expDesc , "", "holding_QQuote", driver, "", "", null);
					System.out.println("Failed >> Quick Quote window dispays incorrect symbol descriptiom - " + expDesc);
				}//end of nested if-else
			}//end of out if
			else {
				Reporting.reportStep(rowNum, "Failed", "Quick Quote window displays incorrect symbol and description >> " + expSymbol , "", "holding_QQuote", driver, "", "", null);
				System.out.println("Failed >> Quick Quote window dispays incorrect symbol and description - " + expSymbol);
			}//end of Else
		}catch(Exception e) {
			Reporting.reportStep(rowNum, "Failed", "Exception found in >> @method = check_QQ_Symbol  and @class = QQuote " , "", "holding_QQuote", driver, "", "", null);
			System.out.println("Exception found in >> @method = check_QQ_Symbol  and @class = ActionMenu");
			e.printStackTrace();
		}
		
	}//end of function

}//end of class
