package PageObjects.WM;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.DateUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class Trade_Widget {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	WebDriverWait wait;
	StringManipulation StringManipulation = new StringManipulation();
	
	//Objects
	
	@FindBy(how = How.CSS, using =".tradeWidget.modal .modal-body" )
	public WebElement tw_popup_TradeWidget;
	
	@FindBy(how = How.CSS, using =".svi-detailed-quote .large.bright" )
	public WebElement tw_Symbol;
	
	@FindBy(how = How.XPATH, using = ".//div[@class='modal-header quote']/div[@data-ng-click = 'close()']")
	public WebElement tw_close;
	
	@FindBy(how = How.CSS, using =".svi-account-dropdown .button-text")
	public WebElement tw_dd_Account;
	
	@FindBy(how = How.XPATH, using =".//select[contains(@id,'tradewidget')][@name='side']/option[contains(@selected, 'selected')]")
	public WebElement tw_Action;
	
	public Trade_Widget(){
        
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
	
	


	
}//end of class
 