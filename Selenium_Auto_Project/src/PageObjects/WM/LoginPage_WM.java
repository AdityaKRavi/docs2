package PageObjects.WM;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;

public class LoginPage_WM {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	//WebDriver driver;
	
	//Login Id is same for sso n non sso
	@FindBy(id = "loginId")
	public WebElement strLoginId;
	
	//username / login if for FDB Pro investor
	@FindBy(id = "user_name")
	public WebElement strLoginId_FDBPro;
	
	@FindAll(@FindBy(how = How.XPATH, using = ("//*[starts-with(@id, 'account_')]")))
	public List<WebElement> textField_allSsoAccounts;

	@FindBy(id = "profile_userSegment")
	public WebElement strProfile;
	
	@FindBy(id = "account_0")
	public WebElement strAccountNo0;
	
	
	@FindBy(css = "input[value='Log In']")
	public WebElement logInButton;
	
	//##################### For NON SSO login page #####################

	@FindBy(css = "input[value='Submit']")
	public WebElement nonSsoSubmitButton;
	
	@FindBy(id = "password")
	public WebElement strNonSsoPwd;
	
	@FindBy(css = "input[value='login']")
	public WebElement nonSsologInButton;
	
	@FindBy(css = "input[name ='login_id']")
	public WebElement nonSsologInId_FDBProAdmin;
	
	@FindBy(css = "input[name ='password']")
	public WebElement strNonSsoPwd_FDBProAdmin;
	
	@FindBy(css = "input[type='Submit']")
	public WebElement nonSsoEnterButton_FDBPro;
	
	@FindBy(css = ".x-btn-mc button")
	public WebElement nonSsologInButton_FDBPro;
	
	
	
	
	
	// **********************************    Constructor    ***************************
	// *********************************************************************************
	public LoginPage_WM(){
    //this.driver = driver;
    //This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
    }

	// *********************************************************************************
	// *********************************************************************************
	
	
	// **********************************    Page Methods    ***************************
	// *********************************************************************************
	
	//*************   CONSTRUCTOR   ***************
	//Note - cant use this for login page bcoz STF uses 2 page login verification
	/*public LoginPage(WebDriver driver) {
    this.driver = driver;
    //  Wait 20 Second To Find Element If Element Is Not Present
    PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
	}*/
	
	//####################################################################################
	  //###################################################################################
	  //Function name	: submitSso_Login(String userId, String account1)
	  //Class name		: LoginPage
	  //Description 	: Submit SSO login credential and click Login
	  //Parameters 		: userid and account
	  //Assumption		: Login elements are present
	  //Developer		: Neelesh Vatsa
	  //##################################################################################

	public void submitSso_Login(String userId, String account1, String strClientName) {
		strLoginId.sendKeys(userId);
		//check if acnt no has semi colon for multiple entry
		if((account1.indexOf(';')>0)){
			String[] arrAcntSplit = account1.split(";");
			//Handle if no: of accounts passed are more than 20 as there are max 20 text boxes on login page
			int maxAcntSize = 0;
			if(arrAcntSplit.length > 20){
				System.out.println("Number of accounts passed for SSO login is more than 20. Only first 20 will be executed");
				maxAcntSize = 20;
			}else{
				maxAcntSize = arrAcntSplit.length;
			}
			// iterate via "for loop"
			for (int i = 0; i < textField_allSsoAccounts.size(); i++) {
				textField_allSsoAccounts.get(i).sendKeys(arrAcntSplit[i]);
				if(i == maxAcntSize - 1){
					//logInButton.click();
					break;
				}
			}

		}else{
			textField_allSsoAccounts.get(0).sendKeys(account1);
			//strAccountNo0.sendKeys(account1);
		}
		
		if(strClientName.equals("USB_Trust")) {
			Select select = new Select(strProfile);
			select.selectByVisibleText("2 - Trust");
			
		}
		
		//login
		logInButton.click();
		
	}
	  //##################################################################################
	  //Function name	: submitNonSso_Login(String userId, String password)
	  //Class name		: LoginPage
	  //Description 	: Summit Non SSO login credentials and click login
	  //Parameters 		: userid and pwd
	  //Assumption		: Login elements are present
	  //Developer		: Neelesh Vatsa
	  //##################################################################################
	public boolean submitNonSso_Login(String userId, String password, String strClient) throws InterruptedException {
			if(strClient.equals("USB")){
				USB_Prod_LoginPage USB_Prod_LoginPage = new USB_Prod_LoginPage();
				return USB_Prod_LoginPage.usb_PROD_SubmitLogin(userId, password);
			}
			//******************   FDB PRO    ******************
			//***************************************************
			if(strClient.equals("FDBPro_Admin")){
				//enter username
				if(CommonUtils.isElementPresent(nonSsologInId_FDBProAdmin)){
					nonSsologInId_FDBProAdmin.sendKeys(userId);
				}else{
					System.out.println("LoginId field is not present");
					return false;
				}
				//enter pwd
				if(CommonUtils.isElementPresent(strNonSsoPwd_FDBProAdmin)){
					strNonSsoPwd_FDBProAdmin.sendKeys(password);
				}else{
					System.out.println("Password field is not present");
					return false;
				}
				//click enter
				if(CommonUtils.isElementPresent(nonSsoEnterButton_FDBPro)){
					nonSsoEnterButton_FDBPro.click();
				}else{
					System.out.println("Enter button is not present");
					return false;
				}
				
				return true;
			}else if(strClient.equals("FDBPro")){
				//enter username
				if(CommonUtils.isElementPresent(strLoginId_FDBPro)){
					strLoginId_FDBPro.sendKeys(userId);
				}else{
					System.out.println("LoginId field is not present");
					return false;
				}
				
				//enter pwd
				if(CommonUtils.isElementPresent(strNonSsoPwd)){
					strNonSsoPwd.sendKeys(password);
				}else{
					System.out.println("Password field is not present");
					return false;
				}
				
				//click login btn
				if(CommonUtils.isElementPresent(nonSsologInButton_FDBPro)){
					nonSsologInButton_FDBPro.click();
				}else{
					System.out.println("Login button is not present");
					return false;
				}
				return true;
				//***************************************************
				//***************************************************
			}else{
				//Check and fill login id.
				if(CommonUtils.isElementPresent(strLoginId)){
					strLoginId.sendKeys(userId);
				}else{
					System.out.println("LoginId field is not present");
					return false;
				}
				//Check and click Submit Button for STF.
				if (strClient.equals("STF") || strClient.equals("LPL")){
					WebElement btnName = null;
					if (strClient.equals("STF")){
						 btnName = nonSsoSubmitButton;
					}else{
						 btnName = nonSsologInButton;
					}
					if(CommonUtils.isElementPresent(btnName)){
						btnName.click();
					}else{
						System.out.println("Login button of Non SSO login page is not present");
						return false;
					}
				}
				//Check and fill login password.
				if(CommonUtils.isElementPresent(strNonSsoPwd)){
					strNonSsoPwd.sendKeys(password);
				}else{
					System.out.println("Password field is not present");
					return false;
				}
				
				//Check and click Login Button.
				if(CommonUtils.isElementPresent(nonSsologInButton)){
					nonSsologInButton.click();
				}else{
					System.out.println("Login button is not present");
					return false;
				}
			}

			

		return true;
	}
	
	  //##################################################################################
	  //Function name	: loginPage_SSO_CheckElements_ArePresent()
	  //Class name		: LoginPage
	  //Description 	: Check login page elements - userid, account no and Login button.
	  //Parameters 		: None
	  //Assumption		: SSO Login page elements are present
	  //Developer		: Neelesh Vatsa
	  //##################################################################################
	public boolean loginPage_SSO_CheckElements_ArePresent() {
		boolean flgElementPresent = true;
		//System.out.println("Checking SSO Login page element exists");
		try{
			WebElement[] arrSSOLogin = new WebElement[]{strLoginId,textField_allSsoAccounts.get(0),logInButton};
			//System.out.println("check elemtns -- after creating list");
			for(int i = 0; i< arrSSOLogin.length; i++){
				if(!(CommonUtils.isElementPresent(arrSSOLogin[i]))){
					flgElementPresent = false;
					System.out.println("Element not found on Login page - " + (arrSSOLogin[i]).getText());
				}
			}
			if(flgElementPresent == false){
				return false;
			}
			return true;	
		}catch(Exception e){
			System.out.println("Login page element could not be verified");
			//e.printStackTrace();}
			return false;
		}
	}
	  //#################################################################################

}
