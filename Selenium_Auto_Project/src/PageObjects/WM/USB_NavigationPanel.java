package PageObjects.WM;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.WM.WM_CommonFunctions;


public class USB_NavigationPanel {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	WebDriverWait wait;
	// ************* Top Navigation Elements ************************************
	//***************************************************************************
	
	@FindBy(how = How.ID, using = "topTrustNavigation")
	public WebElement panel_Navigation;
	//My Accounts tab
	@FindBy(how = How.ID, using = "divMyAccount")
	public WebElement tab_MyAccounts;
	
	//Brokerage tab
	@FindBy(how = How.ID, using = "WMenu-BrokerageTxt")
	public WebElement tab_Brokerage;
	
	//Brokerage >> Account details tab
	@FindBy(how = How.ID, using = "divWMenu_AccountDetails")
	public WebElement tab_AccountDetails;
	
	//Brokerage >> Tradings tab
	@FindBy(how = How.ID, using = "divWMenu_Trading")
	public WebElement tab_Trading;
	
	//Brokerage >>  Transfers tab
	@FindBy(how = How.ID, using = "divWMenu_TransferMoney")
	public WebElement tab_Transfers;
	
	
	//Brokerage >>  My Documents tab
	@FindBy(how = How.ID, using = "divWMenu_MyDocuments")
	public WebElement tab_MyDocuments;
	
	
	//Brokerage >>  Market and Research tab
	@FindBy(how = How.ID, using = "divWMenu_MarketAndResearch")
	public WebElement tab_MktAndResearch;
	
	
	//Brokerage >>  Education Center tab
	@FindBy(how = How.ID, using = "divWMenu_EducationCenter")
	public WebElement tab_EducationCenter;
	
	
	//Brokerage >>  Page title Accounts
	@FindBy(how = How.CSS, using = "#pageTitle>h1")
	public WebElement page_Title;
	
	
	//Brokerage >>  Re-clicking the Account dropdown when the DD list is zero
	@FindBy(how = How.XPATH, using = ".//*[@id='liWMenu_AccountViews']/a")
	public WebElement re_clickingAccountDD;
	
	
	//Brokerage >>  Re-clicking the trading dropdown when the DD list is zero
	@FindBy(how = How.XPATH, using = ".//*[@id='liWMenu_Trading']/a")
	public WebElement re_clickingTradingDD;
	
	
	//Brokerage >>  Re-clicking the MarketResearch dropdown when the DD list is zero
	@FindBy(how = How.XPATH, using = ".//*[@id='liWMenu_MarketResearch']/a")
	public WebElement re_clickingMrktRsrchDD;
	
	
	//Brokerage >>  Re-clicking the Education center dropdown when the DD list is zero
	@FindBy(how = How.XPATH, using = ".//*[@id='liWMenu_EducationCenter']/a")
	public WebElement re_clickingEduCenterDD;
	
	

	//************* Drop down - Navigation Elements **************
	@FindBy(how = How.XPATH, using = ".//*[@id='ulAccountViews']//li")
	public List<WebElement> dd_AccountDetails;

	@FindBy(how = How.XPATH, using = ".//*[@id='ulTrading']//li")
	public List<WebElement> dd_Trading;

	@FindBy(how = How.XPATH, using = ".//*[@id='ulTransfers']//li")
	public List<WebElement> dd_Transfers;

	@FindBy(how = How.XPATH, using = ".//*[@id='ulMyDocuments']//li")
	public List<WebElement> dd_MyDoc;

	@FindBy(how = How.XPATH, using = ".//*[@id='ulMarketResearch']//li")
	public List<WebElement> dd_MktResearch;

	@FindBy(how = How.XPATH, using = ".//*[@id='ulEducationCenter']//li")
	public List<WebElement> dd_EduCenter;


	
	//--------------  Constructor to initialize all elements  --------------
	public USB_NavigationPanel(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }


	//########################     FUNCTIONS       #############
	
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	
	public  void checkTopPanel_isPresent() throws Exception{
		if(!CommonUtils.isElementPresent(panel_Navigation) == true){
			panel_Navigation = null;
		}
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - checkAllNavTabs_arePresent
	//Description - This function verifies that all parent navigation tabs are present.
	//Argument - row no: of test data
	//Returns - This function returns a String variable with the names of all failed / missing tabs seprated by semi colon.
	//Date - Oct 2017
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public void  checkAllNavTabs_arePresent(int rowNum) throws Exception{
		String passedValues = "";
		String failedTab = "";
		
		if(CommonUtils.isElementPresent(tab_MyAccounts) == true){
			passedValues = passedValues + "My Accounts";
		}else{
			failedTab = failedTab + "My Accounts";
		}
		
		if(CommonUtils.isElementPresent(tab_Brokerage) == true){
			passedValues = passedValues + "; Brokerage";
		}else{
			failedTab = failedTab + "; Brokerage";
		}
		
		if (!passedValues.equals("")){
			Reporting.reportStep(rowNum, "Passed", "", "", "", driver, "", "", null);
			System.out.println("Passed Tabs = " + passedValues );
		}
		
		if (!failedTab.equals("")){
			checkTopPanel_isPresent();
			Reporting.reportStep(rowNum, "Failed", failedTab + " - tab/s are not present in the Navigation Panel.", "", "Failed_Nav_Tab", driver, "", "", panel_Navigation);
			System.out.println("Failed Tabs = " + failedTab );
		}
	}//End of Function
	//************************************************************************************************************
	//************************************************************************************************************
	//######################################################################################################################
	//######################################################################################################################
	//Name - getNavigation_DropDownItems
	//Description - This function fetches all the drop down values pfrom UI, present under a Navigation tab.
	//Argument - Navigation tab name like Trading
	//Returns - This function returns a String variable with the names of all drop down options separated by semi colon.
	//Date - Oct 2017
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String  getNavigation_DropDownItems(String strTabName, int rowNum) throws Exception{
		List<WebElement> allOptions = null;
		checkTopPanel_isPresent();
		switch (strTabName){
		case "Account Details":
			if(CommonUtils.isElementPresent(tab_AccountDetails) == true){
				tab_AccountDetails.click();
				allOptions = dd_AccountDetails;
			}else{
				Reporting.reportStep(rowNum, "Failed", " Account Details tab is not present in the Navigation Panel.", "", "Failed_AccntDetails_NavTab", driver, "", "", panel_Navigation);
				
			}
			break;
		case "Trading":
			if(CommonUtils.isElementPresent(tab_Trading) == true){
				tab_Trading.click();
				allOptions = dd_Trading;
			}else{
				Reporting.reportStep(rowNum, "Failed", " Tradings tab is not present in the Navigation Panel.", "", "Failed_Tradings_NavTab", driver, "", "", panel_Navigation);
				
			}
			break;
		case "Transfers":
			if(CommonUtils.isElementPresent(tab_Transfers) == true){
				tab_Transfers.click();
				allOptions = dd_Transfers;
			}else{
				Reporting.reportStep(rowNum, "Failed", " Transfers tab is not present in the Navigation Panel.", "", "Failed_Transfer_NavTab", driver, "", "", panel_Navigation);
				
			}
			break;
		case "My Documents":
			if(CommonUtils.isElementPresent(tab_MyDocuments) == true){
				tab_MyDocuments.click();
				allOptions = dd_MyDoc;
			}else{
				Reporting.reportStep(rowNum, "Failed", " My Documents tab is not present in the Navigation Panel.", "", "Failed_MyDocs_NavTab", driver, "", "", panel_Navigation);
				
			}
			break;
		case "Markets & Research":
			if(CommonUtils.isElementPresent(tab_MktAndResearch) == true){
				tab_MktAndResearch.click();
				allOptions = dd_MktResearch;
			}else{
				Reporting.reportStep(rowNum, "Failed", " Mkt and Research tab is not present in the Navigation Panel.", "", "Failed_MktNResearch_NavTab", driver, "", "", panel_Navigation);
				
			}
			break;
		case "Education Center":
			if(CommonUtils.isElementPresent(tab_EducationCenter) == true){
				tab_EducationCenter.click();
				allOptions = dd_EduCenter;
			}else{
				Reporting.reportStep(rowNum, "Failed", " Education Center tab is not present in the Navigation Panel.", "", "Failed_EduCntr_NavTab", driver, "", "", panel_Navigation);
				
			}
			break;
		}

		String resultOptionsList = "";
		if(!allOptions.equals(null)){
			for(WebElement option:allOptions){
				String valueTest = option.getText().trim();
				
				if(!valueTest.equals(null)){
					resultOptionsList = resultOptionsList + ";" +  valueTest;
					
				}//Enf of Nested If
			}//End of For Loop
			//Note - there are some empty options returned by FE which causes more then 1 semicolon to be placed in the string.
			resultOptionsList = resultOptionsList.replaceAll("[;]{2,}",";");
			resultOptionsList = (resultOptionsList.replaceFirst(";", ""));
		}//End of If
		
		return resultOptionsList;
		
	}//End of function - getNavigation_DropDownItem
		
	//************************  End of functions **********************************
	public void check_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception {
		String pageTitle = "";
		boolean optionClicked = true;
		strPageName = strPageName.trim();
		wait = new WebDriverWait(driver,60);
		//WM_CommonFunctions.wait_ForPageLoad(rowNum);
		wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
		//Switch stmnt for each page option
		switch (strPageName){
		//navigate to the page
		case "Account Details":
			Thread.sleep(100);
			WM_CommonFunctions.clickItem(tab_AccountDetails, "Dropdown", rowNum);
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			break;
		//########  Start of Dropdowns for Accounts  ########
		case "Summary":	case "Balances":case "Holdings": case "Asset Allocation": case "Account Activity":case "Download":
		case "Unrealized Gain/Loss": case "Realized Gain/Loss": case "Portfolio At A Glance":case "Household Overview":
			Thread.sleep(100);
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_AccountDetails, strPageName, tab_AccountDetails, "Account Details", "Click", "USB")){
				pageTitle = "";
			}else{
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			}
			//NOTE - updated old code from Dhana with new one. keeping this section till we run 2 cycles
			/*int i = 1;
			while(i<=3){
				WM_CommonFunctions.clickItem(tab_AccountDetails, "Dropdown", rowNum);
				Thread.sleep(60);
				optionClicked = WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_AccountDetails, strPageName);
				if(optionClicked == false)
				{
					reClickingDropdown(re_clickingAccountDD,driver,rowNum);
				}
				if(optionClicked)
				{
					break;
				}
				System.out.println("dropdown not clicked in atempt - " + i + ". Trying again..");
				i++;
				
			}*/
			//wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			//WM_CommonFunctions.wait_ForPageLoad(rowNum);
			//pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			System.out.println("Accounts page title = " + pageTitle);
			break;
		//END of Accounts drop down  list	
		//########  Start of Dropdowns for Tradings  ########
		case "Stocks":case "Mutual Funds":case "Options":case "Order Status":
			Thread.sleep(100);
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Trading, strPageName, tab_Trading, "Trading", "Click", "USB")){
				pageTitle = "";
			}else{
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			}
			System.out.println("Trading page title = " + pageTitle);
			
			break;
			
		//########  Start of Dropdowns for Markets & Research  ########
		case "Market Overview":case "Market Events Calendar":case "Screeners":case "Stock Report":
		case "Mutual Fund / ETF Report":case "Option Chains":case "Interactive Chart":case "Stock Alerts":
			Thread.sleep(100);
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_MktResearch, strPageName, tab_MktAndResearch, "Mkt and Research", "Click", "USB")){
				pageTitle = "";
			}else{
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			}
			
			System.out.println("Markets & Research page title = " + pageTitle);
			break;
			
		//end of Markets & Research drop down list
			
			
		//########  Start of Dropdowns for Education Center  ########	
		case "Help Center":case "Calculators":case "Fixed Income Investing":case "General Investing":
		case "International Investing": case "Life Insurance/ Annuities":case "Mutual Funds/ETFs":
		case "Personal Finance":case "Retirement":case "Saving for College": case "Tax Planning":
			Thread.sleep(100);
			if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_EduCenter, strPageName, tab_EducationCenter, "Mkt and Research", "Click", "USB")){
				pageTitle = "";
			}else{
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			}
			System.out.println("Education Center page title = " + pageTitle);
			break;
			
		//########  Start of Dropdowns for Education Center  ########
			
		default:
			System.out.println("Drop down option not found in switch -case, please check");
			Reporting.reportStep(rowNum, "Failed", strPageName + " - not found in expected list of option names. Check Swich stmnt.", "", "", driver, "", "", null);	
			break;
		}
		Thread.sleep(100);
		
		//match the page title with page name
		//add passed or failed in the report accordingly
		if(!pageTitle.equals("")){
			if(strExpectedPageTitle.contains(pageTitle)){
				System.out.println("Page navigation check Passed for - " + strPageName);
				System.out.println("Actual - " + pageTitle);
				System.out.println("Expected - " + strExpectedPageTitle);
				Reporting.reportStep(rowNum, "Passed", strPageName + " - has loaded successfully.", "", "", driver, "", "", null);	
			}else{
				System.out.println("Page navigation check failed for - " + strPageName + " Expected / Actual = " + strExpectedPageTitle + "/" +pageTitle);
				Reporting.reportStep(rowNum, "Failed", strPageName + " - page did not load successfully Actual / Expected = ." + pageTitle + "/" +  strExpectedPageTitle, "", "USB_PageLoadCheck", driver, "", "", null);	
			}//End of nested If-Else
		}else{
			Reporting.reportStep(rowNum, "Failed", strPageName + " - page title could not be located by the script.", "", "USB_PageLoadCheck", driver, "", "", null);	
		}//End of If-Else
		
	}
	//************************************************************************************************************
	public void reClickingDropdown(WebElement elementToReClick, WebDriver driv, int rowNum) throws Exception
	{
		WM_CommonFunctions.clickItem(elementToReClick, "Dropdown", rowNum);
	}
}
