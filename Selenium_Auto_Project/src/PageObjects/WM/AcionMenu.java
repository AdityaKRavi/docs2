package PageObjects.WM;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;

public class AcionMenu {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	
	
	//************************   Test Objects   ************************
	//******************************************************************
	
	
	//pop-up window
	@FindBy(how = How.XPATH, using = ".//div[@class='popover-content']")
	private WebElement popup_ActionMenu;
	
	
	
	@FindBy(how = How.CSS, using = ".large.bright.desc span")
	private WebElement symbol_Descripton;
	
	@FindBy(how = How.XPATH, using = ".//*[contains(@class, 'symbol large')]")
	private WebElement am_symbol_title;
	
	@FindBy(how = How.XPATH, using = ".//dt[contains(@class, 'symbol')]")
	private WebElement am_col_Symbol;
	
	@FindBy(how = How.XPATH, using = ".//dt[contains(@class, 'lastPrice')]")
	private WebElement am_col_lastPrice;
	
	@FindBy(how = How.XPATH, using = ".//dt[contains(@class, 'change')]")
	private WebElement am_col_DayChange;
	
	
	@FindBy(how = How.XPATH, using = ".//div//ul[contains(@class, 'svi-action-bar nav')]/li/a")
	private List<WebElement> am_nav_bar;
	
	@FindBy(how = How.XPATH, using = ".//ul[contains(@class, 'svi-action-bar')]//ul[@class = 'dropdown-menu']/li/a")
	private List<WebElement> am_dd_Trade;
	
	
	
	
	
	
	
	
	
	
	
	public AcionMenu() {
		this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
	}

}
