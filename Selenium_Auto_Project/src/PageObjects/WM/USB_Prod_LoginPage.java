package PageObjects.WM;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import org.openqa.selenium.NoSuchElementException;


public class USB_Prod_LoginPage {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	// ************* Top Navigation Elements ************************************
	//***************************************************************************
	
	//##################### Web Elements for  USB PROD login pages #####################

	@FindBy(how = How.CSS, using = ".usbAuthDropdown")
	public WebElement dd_LoginType;
	
	@FindBy(id = "txtPersonalId")
	public WebElement txtbox_LoginId;

	@FindBy(id = "btnContinue")
	public WebElement btn_Login;
	
	@FindBy(how = How.CSS, using = ".usbAuthDropdown-menu>li:first-child")
	public WebElement ddOption_OnlineBanking;
	
	@FindBy(how = How.CSS, using = ".lw-passwordImagePhrase")
	public WebElement img_Pwd;
	
	//@FindBy(how = How.CSS, using = ".LLBlocked.ng-valid-maxlength")
	@FindBy(how = How.CSS, using = "#ans")
	public WebElement txtbox_SecAns;
	
	//@FindBy(how = How.CSS, using = ".lw-idquestion-label.ng-binding")
	@FindBy(how = How.CSS, using = "#customUI > div.authenticator-body.ng-scope > form > label")
	public WebElement text_Question;
	
	@FindBy(id = "txtPassword")
	public WebElement txtbox_Pwd;
	
	@FindBy(id = "lnkRetirement_1")
	public WebElement dd_ChooseAcnt;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='acsMainInvite']/div/a[1]")
	public WebElement btn_DeclineFeedback;
	
	
	

	// **********************************    Constructor    ***************************
	// *********************************************************************************
	public USB_Prod_LoginPage(){
    //This initElements method will create all WebElements
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
    }

	
	//***********************************************  FUNCTIONS *************************************************
	//************************************************************************************************************
	//##################################################################################
	//Function name	: usb_PROD_SubmitLogin(String userId, String password)
	//Description 	: Submit USB prod login details and navigate to Summary page
	//Parameters 	: userid and pwd
	//Assumption	: None
	//Created By	: Neelesh Vatsa
	  //##################################################################################	
	public boolean usb_PROD_SubmitLogin(String userId, String password){
		try{
				if (!CommonUtils.isElementPresent(dd_LoginType)){
						System.out.println("USB PROD - Login type dropdown is not present to select Online Banking. Please check");
						return false;
					}//End of If - checkpoint >> if login type dropdown is present
				
				//Click on Login type drop down and select Online Banking
				dd_LoginType.click();
				if(!CommonUtils.isElementPresent(ddOption_OnlineBanking)){
						System.out.println("USB PROD - Online Banking dropdown object is not present. Please check");
						return false;
				}//End of If - checkpoint >> if drop down options object is present
				
				
				if(!ddOption_OnlineBanking.getText().equals("Online Banking")){
						System.out.println("USB PROD - First item of dropdown is not - Online Banking");
							return false;
				}//End of If - Checkpoint >> Check is the option selected is Online Banking or not
							
				ddOption_OnlineBanking.click();
				//enter username
				if(CommonUtils.isElementPresent(txtbox_LoginId)){
					txtbox_LoginId.clear();
					txtbox_LoginId.sendKeys(userId);
				}else{
					System.out.println("USB PROD - LoginId field is not present");
					return false;
				}//End of If-Else
				//click on login
				if(CommonUtils.isElementPresent(btn_Login)){
					btn_Login.click();
				}else{
					System.out.println("USB PROD - Login button is not present");
					return false;
				}//End of If-Else
				System.out.println("USB PROD - Login has been clicked for Online Banking selection");			
				//enter security ans if present
				Thread.sleep(4000);
				if(CommonUtils.isElementPresent(text_Question)){
					if(usb_SecurityQuestion_Fillup()){
						//System.out.println("USB PROD - Security question step is complete");
					}else{
						System.out.println("USB PROD - Security Ans step has failed");
						return false;
					}//End of nested If-Else
					
				}else{
					System.out.println("USB PROD - Security Question step not prompted");
					//continue with login if sec question was not asked
				}//End of If-Else				
				//enter password
				Thread.sleep(4000);
				//password text box seems to be visible even if application hasnt reached that page
				if(CommonUtils.isElementPresent(img_Pwd) && CommonUtils.isElementPresent(txtbox_Pwd)){
					txtbox_Pwd.clear();
					txtbox_Pwd.sendKeys(password);
					txtbox_Pwd.sendKeys(Keys.ENTER);
					System.out.println("USB PROD - Pwd has been entered");
				}else{
					System.out.println("USB PROD - Pwd textbox missing.");
					return false;
					}//End of If-Else
				//Close feedback popup if displayed
				Thread.sleep(500);
				try{
					if(btn_DeclineFeedback.isDisplayed()){
						System.out.println("USB PROD - Feedback window is present");
						btn_DeclineFeedback.click();
					}
				}catch(NoSuchElementException e){
					System.out.println("Feedback window not displayed.");
				}//End of sub Try-Catch
				//Select an Account no to navigate to Summary page
				if(CommonUtils.isElementPresent(dd_ChooseAcnt)){
						System.out.println("USB PROD - Click on Account no: link.");
						dd_ChooseAcnt.click();
						Thread.sleep(4000);
				}else{
					System.out.println("USB PROD - Account number link not found in USB prod home page.");
					return false;
				}//End of If-Else
				return true;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Exception found in method >> usb_PROD_SubmitLogin. Please check");
			return false;
			
		}//End of Try-Catch

	}
	//##################################################################################
	//Function name	: usb_SecurityQuestion_Fillup
	//Description 	: Submit security answers for USb prod login.
	//Parameters 	: none
	//Assumption	: none
	//Created By	: Neelesh Vatsa
	//##################################################################################	
	public boolean usb_SecurityQuestion_Fillup(){
		//check the question text to get the keyword
		String strQuestion = text_Question.getText();
		System.out.println("question is = " + strQuestion);
		String strQuestionText = "";
		if(StringManipulation.find_SubStringMatch(strQuestion, "landmark")){
			strQuestionText = "landmark";
		}else if(StringManipulation.find_SubStringMatch(strQuestion, "hero")){
			strQuestionText = "hero";
		}else if(StringManipulation.find_SubStringMatch(strQuestion, "book")){
			strQuestionText = "book";
		}else if(StringManipulation.find_SubStringMatch(strQuestion, "street")){
			strQuestionText = "street";
		}else{
			System.out.println("USB PROD - Unexpected Question. Please check.");
			return false;
		}
		//enter the answer based on keyword present in question
		if(CommonUtils.isElementPresent(txtbox_SecAns)){
			txtbox_SecAns.clear();
			switch (strQuestionText){
			case "hero":
				txtbox_SecAns.sendKeys("Superman");
				break;
			case "landmark":
				txtbox_SecAns.sendKeys("Statue of Liberty");
				break;
			case "book":
				txtbox_SecAns.sendKeys("Jungle Book");
				break;
			case "street":
				txtbox_SecAns.sendKeys("otonka");
				break;
			}
		}else{
			System.out.println("USB PROD - Answer textbox is not present. Please check.");
			return false;
		}
		//Press enter for continue button
		txtbox_SecAns.sendKeys(Keys.ENTER);
		return true;		
	}
	//******************************************  End of functions ***********************************************
	//************************************************************************************************************
}
