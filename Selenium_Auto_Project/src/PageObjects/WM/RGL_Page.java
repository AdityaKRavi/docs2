package PageObjects.WM;

import java.util.Date;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class RGL_Page {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	
	// ************* Filter dropdowns Elements ************************************
	//***************************************************************************
	//Panel to be used for screenshot highlight
	@FindBy(how = How.CSS, using = "#pageTitle>.form-inline")
	public WebElement panel_AllFilters;

	// Alert message 
	@FindBy(how = How.XPATH, using = ".//*[@id='svi-realized-gain-loss']/div[1]/div/div/div[1]/div[1]/p")
	public WebElement alert_NoResult_Message;
	
	//From and To date range objects
	@FindBy(how = How.CSS, using = "div [name=\"toDate\"]")
	public WebElement dateRange_ToDate;
	
	@FindBy(how = How.CSS, using = "div [name=\"fromDate\"]")
	public WebElement dateRange_FromDate;
	
	//--------------  Constructor to initialize all elements  --------------
	public RGL_Page(){
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
       
    }

	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	SummaryPage All_Investor_PageTitles = new SummaryPage();
	
	//########################     FUNCTIONS       #############
	//Description - This functions sets the object for screenshot purpose. If Filter panel 
	//itself is not present, then null object is returned in which case full page screenshot is taken.
	//###########################################################
	public  WebElement Set_ParentObjForImg() throws Exception{
		if(CommonUtils.isElementPresent(panel_AllFilters) == false){
			panel_AllFilters = null;
		}
		return panel_AllFilters;
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - checkAllNavTabs_arePresent
	//Description - This function verifies that all parent navigation tabs are present.
	//Argument - row no: of test data
	//Returns - This function returns a String variable with the names of all failed / missing tabs seprated by semi colon.
	//Date - Oct 2017
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean checkFilters_ValueAndFunctioning( int rowNum, String strKeyword, String strFilterName, String strAllAssetTypeValue, String strAllTermTypeValue, String strYearValue, String fromDate, String toDate, String strClient) throws Exception{
		//Set screenshot object as Filter panel
		WebElement screenshot_Object = Set_ParentObjForImg();
		try{
			//wait for page title to load
			
			wait = new WebDriverWait(driver,15);
			wait.until(ExpectedConditions.titleContains("Realized Gain/Loss"));
			System.out.println("Expected Page title has loaded for RGL page.");
			System.out.println("RGL Page Filter test   |     Filter name = " + strFilterName + "    |    For Row = " + rowNum);
			
			//###################################################################################################################################
				//Checkpoint 1 - If keyword is UI Test, verify drop down filters are present with expected values.
				//Test case will fail if a filter is missing or has incorrect/missing dropdown value.
			//###################################################################################################################################
			
			List<WebElement> strDropDown = null;
			WebElement DropdownBtn = null;
			String strExpectedDropDownValues = "";
			ResultTable_Section ResultTable_Section = new ResultTable_Section();
			//Assigning drop down button and list for UI test case
			if(strFilterName.contains("All Asset ")){
				strDropDown = ResultTable_Section.dd_AllAssetTypes;
				DropdownBtn = ResultTable_Section.ddTab_AllAssetTypeFilter;
				strExpectedDropDownValues = strAllAssetTypeValue;
			}else if(strFilterName.equals("All Term Types")){
				strDropDown = ResultTable_Section.dd_AllTermTypes;
				DropdownBtn = ResultTable_Section.ddTab_AllTermTypeFilter;
				strExpectedDropDownValues = strAllTermTypeValue;
			}else if(strFilterName.equals("Year")){
				strDropDown = ResultTable_Section.dd_YearTypes;
				DropdownBtn = ResultTable_Section.ddTab_YearFilter;
				strExpectedDropDownValues = strYearValue;
			}//End of If-Else
			
			//Verifying drop down options for keyword = UI test
			if(strKeyword.equals("UI Test")){
				if(strExpectedDropDownValues.equals("")){
					Reporting.reportStep(rowNum, "Failed", "Filter name is not correct. Does not match expected options -  " + strFilterName , "", "RGL_ddCheck", driver, "", "", screenshot_Object);
					return false;
				}
				//get actual dropdown values and expected dropdown values
				String strActualDropDownOption = WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, strDropDown, DropdownBtn);
				System.out.println("RGL page - Actual DropDown option for " + strFilterName + " = " + strActualDropDownOption);
				System.out.println("RGL Page - Expected DropDown option for " + strFilterName + " = " + strExpectedDropDownValues);
				//compare and set result
				if(StringManipulation.compareString_Match(strActualDropDownOption, strExpectedDropDownValues)){
					System.out.println("RGL Page - Filter UI test has Passed for - "+ strFilterName);
					Reporting.reportStep(rowNum, "Passed", strFilterName + " dropdown list has expected values - " + strActualDropDownOption, "", "", driver, "", "", null);
				}else{
					System.out.println("RGL Page - Filter UI test has Failed for - "+ strFilterName);
					Reporting.reportStep(rowNum, "Failed", strFilterName + " dropdown value check failed. Actual // Expected = " + strActualDropDownOption + "//" + strExpectedDropDownValues, "", "RGL_ddCheck", driver, "", "", screenshot_Object);
					
				}//End of IF-Else
			}//End of IF
			//###################################################### END of 1st Checkpoint ######################################################
			//###################################################################################################################################
			
			//###################################################################################################################################
				//Checkpoint 2 - If keyword is not UI Test then apply the filter value for one or multiple combination of filters and verify 
				//result table has only those rows that match or qualify applied filter. 
				//Test case will fail if a filter is missing OR filter value to be applied is missing OR no results are displayed in result table.
			//###################################################################################################################################
			else{
				//###################################################################################################################################
													//Apply filters >> Asset, Term or Year
				//###################################################################################################################################
				//Asset filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Asset Types", strAllAssetTypeValue)){
					 System.out.println("All Asset Types = " + strAllAssetTypeValue + " >> dropdown filter could not be applied. Skiping further execution.");
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF
				 //Term filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Term Types", strAllTermTypeValue)){
					 System.out.println("All Term Types = " + strAllTermTypeValue + " >> dropdown filter could not be applied. Skiping further execution.");
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF
				 //Year Filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "Year", strYearValue)){
					 System.out.println("Year = " + strYearValue + " >> dropdown filter could not be applied. Skiping further execution.");
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF			 
				//###################################################################################################################################
				//###################################################################################################################################
				
				//###################################################################################################################################
				 			// Wait for page load, check for no result message and then expand all rows.
				//###################################################################################################################################
				//Wait for page to load with expected result table rows, in case spinning "loading" icon is displayed in UI
				 System.out.println("Test wait");
				 Thread.sleep(1000);
				 WM_CommonFunctions.wait_ForPageLoad(rowNum);
					
					
				//Check if "No Results found" is displayed and thus result table is missing.Add Failed to the report.
				String msg = alert_NoResult_Message.getText();
				if(alert_NoResult_Message.getText().equalsIgnoreCase("There is no realized gain/loss to display.")){
					System.out.println("RGL Page - No result found for applied filter testcase- " + strFilterName);
					Reporting.reportStep(rowNum, "Failed", "No Result displayed for this Filter -  " + strFilterName , "", "RGL_ddCheck", driver, "", "", screenshot_Object);
					return false;	
				}//End of IF
				 //Expand all rows in result table.
				ResultTable_Section.expandAll_Rows(rowNum);
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				
				//###################################################################################################################################
				//###################################################################################################################################
				
				//###################################################################################################################################
							//Test all rows for - Asset Type, Term Type and Year filter test if applicable
				//###################################################################################################################################
				
				//###################################  verify All Asset Type filter  ###################################
				if((strFilterName.contains("All Asset "))&& (!strClient.equalsIgnoreCase("USB"))){
					boolean flg_FilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Asset Type", strAllAssetTypeValue);
					if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "All rows for Asset Type col, have applied expected value = " + strAllAssetTypeValue, "", "", driver, "", "", null);
						System.out.println("PASSED  >>  RGL page - All rows have same value for col - Asset Types ==>> " + strAllAssetTypeValue);
					}else{
						System.out.println("FAILED  >>  RGL Page - All rows for Asset type column, do not have expected applied filter value -  "+ strAllAssetTypeValue);
						Reporting.reportStep(rowNum, "Failed", "All Asset type column values do not have expected applied filter value -  "+ strAllAssetTypeValue , "", "RGL_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-Else
				}//End of IF
				//Add Report for client = USB, s All asset filter can not be verified for it
				if(strClient.equalsIgnoreCase("USB") && strFilterName.contains("All Asset ")){
					Reporting.reportStep(rowNum, "N/A", "", "", "RGL_ddCheck", driver, "", "", screenshot_Object);
					
				}
				////###################################  verify All Term Type filter  ###################################
				if(strFilterName.contains("All Term Types")){
					//******check DateDiff is 1yr or more******
					boolean flg_TermFilterPass = false;
					if(strClient.equalsIgnoreCase("USB")){
						String strTermType = strAllTermTypeValue.replace(" Term", "");
						flg_TermFilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Term", strTermType);
					}else{
						flg_TermFilterPass = ResultTable_Section.verify_AllColValues_QualifyThisTermType(rowNum, strAllTermTypeValue);
					}
					if(flg_TermFilterPass){
						Reporting.reportStep(rowNum, "Passed", "All displayed rows match the applied Term per Acquired date i.e. = " + strAllTermTypeValue, "", "", driver, "", "", null);
						System.out.println("PASSED  >>  RGL Page - All displayed rows match the applied Term per Acquired date i.e. = " + strAllTermTypeValue);
					}else{
						System.out.println("FAILED  >>  RGL Page - All row for Term type column do not match the applied  filter value -  "+ strAllTermTypeValue);
						Reporting.reportStep(rowNum, "Failed", "All displayed rows do not match the applied Term per Acquired date i.e. ="+ strAllTermTypeValue , "", "RGL_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-else
				}//End of IF
				//###################################  verify Year filter  ###################################
				if(strFilterName.contains("Year")&& (!strYearValue.contains("Range"))){
					
					boolean flg_FilterPass = ResultTable_Section.verify_AllColValues_QualifyYearValue(rowNum, strYearValue);
					if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "All rows for Date Sold col match the applied filter for value = " + strYearValue, "", "", driver, "", "", null);
						System.out.println("PASSED  >>  RGL page - All rows match the applied filter (Current / Prior year ) on col - Date Sold ==>> " + strYearValue);
					}else{
						System.out.println("FAILED  >>  RGL Page - Date Sold column do not have values per expected applied filter value -  "+ strYearValue);
						Reporting.reportStep(rowNum, "Failed", "Date Sold column values do not qualify the applied filter value -  "+ strYearValue , "", "RGL_ddCheck", driver, "", "", screenshot_Object);	
					}//End of IF-Else
				}//End of IF
				if(strFilterName.contains("Year")&& (strYearValue.contains("Range"))){
					//clear textbox and enter dates
					
					dateRange_FromDate.sendKeys("");
					dateRange_FromDate.sendKeys(fromDate);
					dateRange_ToDate.sendKeys("");
					dateRange_ToDate.sendKeys(toDate);
					Thread.sleep(100);
					//click outside of date textbox else it continues to display calendar which fails the test.
					All_Investor_PageTitles.page_TitleHead.click();
					//page_Title.click();
					Thread.sleep(1000);
					WM_CommonFunctions.wait_ForPageLoad(rowNum);
					//verify Date Range
					boolean flg_FilterPass = ResultTable_Section.verify_AllColValues_QualifyYearRange(rowNum, fromDate, toDate);
					if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "Date Sold is within the date range of  From date and To Date = " + fromDate + " and " + toDate, "", "RGL_dateRangeCheck", driver, "", "", null);
						System.out.println("PASSED  >>  RGL page - All rows match the applied date range filter From date / To Date = ==>> " + fromDate + " / " + toDate);
					}else{
						System.out.println("FAILED  >>  RGL Page - All rows dont match the applied date range filter From date / To Date = ==>> " + fromDate + " / " + toDate);
						Reporting.reportStep(rowNum, "Failed", "Date Sold is not within the date range of  From date and To Date = " + fromDate + " and " + toDate, "", "RGL_dateRangeCheck", driver, "", "", screenshot_Object);	
					}//End of IF-Else
				}
				//###################################################################################################################################
				//###################################################################################################################################
				
			}//End of ELSE
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "RGL page filter could not be verified. Filter Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "RGL_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>checkFilters_ValueAndFunctioning, class = RGL_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}

	}
	//************************  End of functions **********************************
	//************************************************************************************************************
}
 