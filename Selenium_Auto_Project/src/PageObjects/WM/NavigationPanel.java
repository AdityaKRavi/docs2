package PageObjects.WM;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class NavigationPanel extends SummaryPage {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	// ************* Top Navigation Elements ************************************
	//***************************************************************************
	//main navigation panel and element used for screenshot
	@FindBy(how = How.ID, using = "privateNav")
	public WebElement panel_Navigation;
	
	
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Home')]")
	public WebElement tab_Home;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Accounts')]")
	public WebElement tab_Accounts;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Trading')]")
	public WebElement tab_Trading;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Research')]")
	public WebElement tab_Research;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Portfolio')]")
	public WebElement tab_Portfolio;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Tools')]")
	public WebElement tab_Tools;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Banking')]")
	public WebElement tab_Banking;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Tools & Calculator')]")
	public WebElement tab_ToolsNCal;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Client Services')]")
	public WebElement tab_ClientServices;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Contact Us')]")
	public WebElement tab_ContactUs;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Account Services')]")
	public WebElement tab_AccountServices;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Client Documents & Research Reports')]")
	public WebElement tab_ClientReports;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Shareholder Library')]")
	public WebElement tab_ShareholderLibrary;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Documents')]")
	public WebElement tab_Documents;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'About Us')]")
	public WebElement tab_AboutUs;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Quotes & Markets')]")
	public WebElement tab_QuotesAndMarket;
	

	//###################################################################################################################
	//###################################################################################################################
	
	//*************************  Navigation Panel  ***********************************
	//Navigation panel list
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li")
	public List<WebElement> dd_NavPanel_List;
	
	//*************************  DropwDown List  ***********************************
	@FindBy(how = How.CSS, using = ".nav-accounts li")
	public List<WebElement> dd_Accounts;
	
	@FindBy(how = How.CSS, using = ".nav-trading li")
	public List<WebElement> dd_Trading;
	
	@FindBy(how = How.CSS, using = ".nav-research li")
	public List<WebElement> dd_Research;
	
	@FindBy(how = How.CSS, using = ".nav-research li ul.dropdown-submenu li")
	public List<WebElement> dd_Research_Calculator;
	
	@FindBy(how = How.CSS, using = ".nav-sqope li")
	public List<WebElement> dd_Portfolio;
	
	@FindBy(how = How.CSS, using = ".nav-settings li")
	public List<WebElement> dd_Tools;
	
	@FindBy(how = How.CSS, using = ".nav-clientServices li")
	public List<WebElement> dd_ClientServices;
	
	@FindBy(how = How.CSS, using = ".nav-about li")
	public List<WebElement> dd_AboutUs;
	
	@FindBy(how = How.CSS, using = ".nav-calculators li")
	public List<WebElement> dd_ToolsNCal;
	
	//Page Titles
	//*************************  Web Element ***********************************
	@FindBy(how = How.CSS, using = "#pageTitle>h1>span")
	public WebElement page_TitleLPLMStar;

	
	//--------------  Constructor to initialize all elements  --------------
	public NavigationPanel(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
	
	//*************************************************   Inner Class   **********************************************************
	//**************************************************************************************************************************
	public class rwb_NavigationPanel {
	// #####################   RWB Object    ########################
	//###############################################################
		@FindBy(how = How.ID, using = "ClientHomeanchor")
		public WebElement tab_RwbHome;
		
		@FindBy(how = How.ID, using = "MyPortfolioanchor")
		public WebElement tab_RwbPortfolio;
		
		@FindBy(how = How.ID, using = "MyAccountanchor")
		public WebElement tab_RwbAccountDetails;
		
		@FindBy(how = How.ID, using = "InvestingToolsanchor")
		public WebElement tab_RwbTools;

	
	// ------------- Dropdown list for RWB -----------
	//###############################################################
		@FindBy(how = How.CSS, using = "#MyPortfolioLevel2 li")
		public List<WebElement> dd_RWBMyPortfolio;
		
		@FindBy(how = How.CSS, using = "#MyAccountLevel2 li")
		public List<WebElement> dd_RWBAccount;
		
		@FindBy(how = How.CSS, using = "#InvestingToolsLevel2 li")
		public List<WebElement> dd_RWBTools;
		
		
		
		//--------------  Constructor to initialize all elements  --------------
		public rwb_NavigationPanel(){
			//This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        System.out.println("Constructor of Nested RWB Class. All objects initialized.");
	    }	
		
		
		//######################################################################################################################
		//######################################################################################################################
		//Name - checkRWB_PageNavigation_viaNavPanel
		//Description - This function returns a string of all expected dropdown values for given field .
		//Parameters - Dropdown name under test.
		//Returns - This function returns a String variable containing all Actual dropdown values from UI.
		//Created By - Neelesh Vatsa
		//######################################################################################################################
		//######################################################################################################################
		public void checkRWB_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception{
		try{
			wait = new WebDriverWait(driver,40);
			String pageTitle = "";
			strPageName = strPageName.trim();
			WM_CommonFunctions.wait_ForPageLoad(rowNum);
			// Action Required for hover over
			Actions action = new Actions(driver);
			
			//Apply switch case for each page
			switch (strPageName){
			case"Home":
				WM_CommonFunctions.clickItem(tab_RwbHome, "Home / Dashboard tab", rowNum);
				pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
				break;
			//*****************   My Portfolio dropdown   *****************
			case "Portfolio at a Glance": case "Household Overview": case "Household Change in Value":case "Asset Allocation": 
				pageTitle = pageNavigation_getPageTitle(rowNum, "click", page_Title, false, tab_RwbPortfolio, dd_RWBMyPortfolio, "My Portfolio Tab", strPageName, "RWB");
				WM_CommonFunctions.clickItem(tab_RwbPortfolio, "Portfolio tab", rowNum);
				break;
			case "Historical Values": 
				pageTitle = pageNavigation_getPageTitle(rowNum, "click", page_Title2, false, tab_RwbPortfolio, dd_RWBMyPortfolio, "My Portfolio Tab", strPageName, "RWB");
				WM_CommonFunctions.clickItem(tab_RwbPortfolio, "Portfolio tab", rowNum);
				break;
			//*****************   Account Details dropdown   *****************
			case "Positions": case "Positions Zoom": case "Activity": case "Balances":
			case "Unrealized Gain/Loss": case "Realized Gain/Loss": 
			
				pageTitle = pageNavigation_getPageTitle(rowNum, "click", page_Title, false, tab_RwbAccountDetails, dd_RWBAccount, "Account DetailsTab", strPageName, "RWB");
				WM_CommonFunctions.clickItem(tab_RwbAccountDetails, "Account Details tab", rowNum);
				break;
				
			case "Order Status":
				pageTitle = pageNavigation_getPageTitle(rowNum, "click", page_TitleHead, false, tab_RwbAccountDetails, dd_RWBAccount, "Account DetailsTab", strPageName, "RWB");
				WM_CommonFunctions.clickItem(tab_RwbAccountDetails, "Account Details tab", rowNum);
				break;
			case "Cash Flow":
				pageTitle = pageNavigation_getPageTitle(rowNum, "click", page_Title2, false, tab_RwbAccountDetails, dd_RWBAccount, "Account DetailsTab", strPageName, "RWB");
				WM_CommonFunctions.clickItem(tab_RwbAccountDetails, "Account Details tab", rowNum);
				break;
				
			//*****************   Investing Tools Dropdown	   *****************
			case "US Markets":  case "Market Briefing": case "Portfolio Tracker": case "Stocks": 
			case "Mutual Funds": case "News": case "Alerts":
				pageTitle = pageNavigation_getPageTitle(rowNum, "click", page_Title4, false, tab_RwbTools, dd_RWBTools, "Investng Tools Tab", strPageName, "RWB");
				WM_CommonFunctions.clickItem(tab_RwbTools, "Home / Dashboard tab", rowNum);
				break;
			
			//	*****************   Default case	   *****************	
			default:
				System.out.println("Method : checkRWB_PageNavigation_viaNavPanel >> No matching Select-Case string found.");
				pageTitle = "";
				break;
			}

			pageNavigation_checkTitle (rowNum,  pageTitle,  strPageName,  strExpectedPageTitle,  "RWB");
			
		}//End of Try block
		catch (Exception e){
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", strPageName + " - Exception thrown while locating page.", "", "RWB_PageNavCheck", driver, "", "", null);	
			
		}//End of catch block
		}
		//************************************************************************************************************
			
	
}
	
	
	
	
//*************************************************   FUNCTIONS   **********************************************************
//**************************************************************************************************************************
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - DAV_Check_AllNavigationPanelObjects
	//Description - Parent function that verifies all navigation panel element in DAV .
	//Parameters - row num and Trading dd Value
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################	
	public void DAV_Check_AllNavigationPanelObjects(int rowNum, String tradingValues){
		try{
			WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
			
			WebElement[] arrNavElements = new WebElement[]{
					tab_Accounts,tab_Research,tab_Tools,tab_ContactUs,tab_ClientReports,tab_ShareholderLibrary};
			
			String[] arrPanelKeyword = new String[]{"Accounts", "Research", "Tools", "Contact Us", "Client Documents & Reports", "Shareholder Library"}; 
			
			System.out.println("Checking Navigation panel tabs for - DAV");
			
			
			for(int i = 0; i< arrNavElements.length; i++){
				if(!(CommonUtils.isElementPresent(arrNavElements[i]))){
					System.out.println( arrPanelKeyword[i] + " - Tab not found on navigation panel.");
					Reporting.reportStep(rowNum, "Failed", arrPanelKeyword[i] + "-  is missing from navigation panel.", "", "DAV_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					
				}else{
					System.out.println(arrPanelKeyword[i] + " tab has Passed");
					Reporting.reportStep(rowNum, "Passed", arrPanelKeyword[i] + " - tab is present.", "", "", driver, "", "", null);}//End of If-Else
				rowNum = rowNum + 1;
			}//End of FOR
			
			//#######################   Check dropdown list for Accounts    #######################, 
			String[] arrDropDownName =  {"Accounts", "Research", "Tools"};
			List [] objDropDown = {dd_Accounts, dd_Research, dd_Tools};
			WebElement [] objNavTabs = {tab_Accounts, tab_Research, tab_Tools};
			for(int i = 0; i<arrDropDownName.length; i++){
				String str_ExpValues = "";
				System.out.println("");
				System.out.println("Checking Dropdown Values for " + arrDropDownName[i] + " tab:");
				if(CommonUtils.isElementPresent(objNavTabs[i])){
					str_ExpValues = DAV_ExpectedNavigationPanelDDList(arrDropDownName[i], tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, arrDropDownName[i], objDropDown[i], objNavTabs[i], str_ExpValues, panel_Navigation, "hover");
				}else{
					System.out.println(arrDropDownName[i] + " tab object not found in Navigation Panel. Please check!!");
					Reporting.reportStep(rowNum, "Failed", arrDropDownName[i] + " tab is missing. cant check dropdowns", "", "DAV_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					//Navigate to home page if tab not found
				}//End of If-Else
				rowNum = rowNum + 1;
			}//End of FOR
			
		}//End of TRY
		catch(Exception e){
			System.out.println("Excpetion found in method - DAV_Check_AllNavigationPanelObjects");
			e.printStackTrace();
			}//End of Catch
	}//END OF FUNCTION
	//######################################################################################################################
	//######################################################################################################################
	//######################################################################################################################
	//######################################################################################################################
	//Name - DAV_ExpectedNavigationPanelDDList
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - Dropdown name under test.
	//Returns - This function returns a String variable containing all Actual dropdown values from UI.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String DAV_ExpectedNavigationPanelDDList(String strDropdownName, String tradingValues){
		String strExpValues = "";
		switch (strDropdownName){
		case "Accounts":
			strExpValues = "Summary;Balances;Positions;Positions Zoom;Activity;Unrealized Gain/Loss;Realized Gain/Loss;Household Overview;Download;Order Status;Client Documents;Shareholder Library";
			break;
		case "Research":
			strExpValues = "Find Symbol;Option Chains;Market Overview;Advanced Chart;Market Events Calendar;Stock Alerts;Stock Fundamentals Report;Mutual Fund / ETF Fundamentals Report;Screener;Calculators";
			break;
			
		case "Tools":
			strExpValues = "Share Accounts;Preferences;Message Center";
			break;
		default:
			System.out.println("Method : DAV_ExpectedNavigationPanelDDList >> No matching Select-Case string found.");
			strExpValues = "";
			break;
		}
		//System.out.println(strActualValues);
		return strExpValues;
	}	
	//######################################################################################################################
	//######################################################################################################################
	//Name - LPL_Check_AllNavigationPanelObjects
	//Description - Parent function that verifies all navigation panel tabs and dropdown list.
	//Parameters - row num and trading values
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################	
	public void LPL_Check_AllNavigationPanelObjects(int rowNum, String tradingValues){
		try{
			WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
			
			WebElement[] arrNavElements = new WebElement[]{
					tab_Accounts,tab_Trading,tab_Research,tab_Tools,tab_ClientServices,tab_Documents,tab_AboutUs};
			
			String[] arrPanelKeyword = new String[]{"Accounts", "Trading", "Research", "Tools", "Client Services", "Documents", "About Us"}; 
			
			System.out.println("Checking Navigation panel tabs for - LPL");
			
			
			for(int i = 0; i< arrNavElements.length; i++){
				if(!(CommonUtils.isElementPresent(arrNavElements[i]))){
					System.out.println( arrPanelKeyword[i] + " - Tab not found on navigation panel.");
					Reporting.reportStep(rowNum, "Failed", arrPanelKeyword[i] + "-  is missing from navigation panel.", "", "LPL_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					
				}else{
					System.out.println(arrPanelKeyword[i] + " tab has Passed");
					Reporting.reportStep(rowNum, "Passed", arrPanelKeyword[i] + " - tab is present.", "", "", driver, "", "", null);}//End of If-Else
				rowNum = rowNum + 1;
			}//End of FOR
			
			//#######################   Check dropdown list for Accounts    #######################, 
			String[] arrDropDownName =  {"Accounts", "Trading", "Research", "Tools", "Client Services", "About Us"};
			List [] objDropDown = {dd_Accounts, dd_Trading, dd_Research, dd_Tools,dd_ClientServices,dd_AboutUs};
			WebElement [] objNavTabs = {tab_Accounts, tab_Trading, tab_Research, tab_Tools, tab_ClientServices, tab_AboutUs};
			for(int i = 0; i<arrDropDownName.length; i++){
				String str_ExpValues = "";
				System.out.println("");
				System.out.println("Checking Dropdown Values for " + arrDropDownName[i] + " tab:");
				if(CommonUtils.isElementPresent(objNavTabs[i])){
					str_ExpValues = LPL_ExpectedNavigationPanelDDList(arrDropDownName[i], tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, arrDropDownName[i], objDropDown[i], objNavTabs[i], str_ExpValues, panel_Navigation, "hover");
				}else{
					System.out.println(arrDropDownName[i] + " tab object not found in Navigation Panel. Please check!!");
					Reporting.reportStep(rowNum, "Failed", arrDropDownName[i] + " tab is missing. cant check dropdowns", "", "LPL_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					//Navigate to home page if tab not found
				}
				rowNum = rowNum + 1;
			}

		}//End of TRY
		catch(Exception e){
			System.out.println("Excpetion found in method - LPL_Check_AllNavigationPanelObjects");
			e.printStackTrace();
			}//End of Catch
	}//END OF FUNCTION
		//######################################################################################################################
		//######################################################################################################################
		
	//######################################################################################################################
	//######################################################################################################################
	//######################################################################################################################
	//######################################################################################################################
	//Name - LPL_ExpectedNavigationPanelDDList
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - Dropdown name under test.
	//Returns - This function returns a String variable containing all Actual dropdown values from UI.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String LPL_ExpectedNavigationPanelDDList(String strDropdownName, String tradingValues){
		String strExpValues = "";
		switch (strDropdownName){
		case "Accounts":
			strExpValues = "Summary;Account Info;Balances;Positions;History;Gain/Loss;Asset Allocation";
			break;
		case "Trading":
			strExpValues = "Stocks + ETFs;Options;Mutual Funds;Order Status";
			break;
		case "Research":
			strExpValues = "Find Symbol;Market Overview;Advanced Chart;Market Events Calendar;Stock Alerts;Stock Fundamentals Report;Mutual Fund / ETF Fundamentals Report;Screener;Option Chains;Calculators";
			break;
		case "Tools":
			strExpValues = "Add Account;Share Accounts;Preferences;Message Center";
			break;
		case "Client Services":
			strExpValues = "Forms;Resources";
			break;
		case "About Us":
			strExpValues = "About LPL Financial";
			break;
		default:
			System.out.println("Method : LPL_ExpectedNavigationPanelDDList >> No matching Select-Case string found.");
			strExpValues = "";
			break;
		}
		//System.out.println(strActualValues);
		return strExpValues;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - FDB_Check_AllNavigationPanelObjects
	//Description - Parent function that verifies all navigation panel tabs and dropdown list.
	//Parameters - row num and trading values
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################	
	public void FDB_Check_AllNavigationPanelObjects(int rowNum, String tradingValues){
		try{
			WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
			
			WebElement[] arrNavElements = new WebElement[]{
					tab_Home, tab_Accounts, tab_Trading, tab_QuotesAndMarket, tab_Documents, tab_Tools, tab_ContactUs};
			
			String[] arrPanelKeyword = new String[]{"Home", "Accounts", "Trading", "Quotes&Mkt", "Documents", "Tools", "ContactUS"}; 
			
			System.out.println("Checking Navigation panel tabs for - FDB");
			
			
			for(int i = 0; i< arrNavElements.length; i++){
				if(!(CommonUtils.isElementPresent(arrNavElements[i]))){
					System.out.println( arrPanelKeyword[i] + " - Tab not found on navigation panel.");
					Reporting.reportStep(rowNum, "Failed", arrPanelKeyword[i] + "-  is missing from navigation panel.", "", "FDB_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					
				}else{
					System.out.println(arrPanelKeyword[i] + " tab has Passed");
					Reporting.reportStep(rowNum, "Passed", arrPanelKeyword[i] + " - tab is present.", "", "", driver, "", "", null);}//End of If-Else
				rowNum = rowNum + 1;
			}//End of FOR
			
			//#######################   Check dropdown list for Accounts    #######################, 
			String[] arrDropDownName =  {"Accounts", "Trading", "Quotes & Markets", "Tools"};
			List [] objDropDown = {dd_Accounts, dd_Trading, dd_Research, dd_Tools};
			WebElement [] objNavTabs = {tab_Accounts, tab_Trading, tab_QuotesAndMarket, tab_Tools};
			for(int i = 0; i<arrDropDownName.length; i++){
				String str_ExpValues = "";
				System.out.println("");
				System.out.println("Checking Dropdown Values for " + arrDropDownName[i] + " tab:");
				if(CommonUtils.isElementPresent(objNavTabs[i])){
					str_ExpValues = FDB_ExpectedNavigationPanelDDList(arrDropDownName[i], tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, arrDropDownName[i], objDropDown[i], objNavTabs[i], str_ExpValues, panel_Navigation, "hover");
				}else{
					System.out.println(arrDropDownName[i] + " tab object not found in Navigation Panel. Please check!!");
					Reporting.reportStep(rowNum, "Failed", arrDropDownName[i] + " tab is missing. cant check dropdowns", "", "FDB_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					//Navigate to home page if tab not found
				}
				rowNum = rowNum + 1;
			}

		}//End of TRY
		catch(Exception e){
			System.out.println("Excpetion found in method - FDB_Check_AllNavigationPanelObjects");
			e.printStackTrace();
			}//End of Catch
	}//END OF FUNCTION
	//######################################################################################################################
	//######################################################################################################################
			
	//######################################################################################################################
	//######################################################################################################################
	//Name - LPL_ExpectedNavigationPanelDDList
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - Dropdown name under test.
	//Returns - This function returns a String variable containing all Actual dropdown values from UI.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String FDB_ExpectedNavigationPanelDDList(String strDropdownName, String tradingValues){
		String strExpValues = "";
		switch (strDropdownName){
		case "Accounts":
			strExpValues = "Account Information;Balances;Positions;Transactions;Gain/Loss;Asset Allocation;Download";
			break;
		case "Trading":
			strExpValues = "Stocks;Options;Mutual Funds;Order Status";
			break;
		case "Quotes & Markets":
			strExpValues = "Find Symbol;Option Chains";
			break;
		case "Tools":
			strExpValues = "Share Accounts;Preferences;Message Center;Application Status";
			break;
		default:
			System.out.println("Method : FDB_ExpectedNavigationPanelDDList >> No matching Select-Case string found.");
			strExpValues = "";
			break;
		}
		//System.out.println(strActualValues);
		return strExpValues;
	}
	//************************************************************************************************************
	//######################################################################################################################
	//######################################################################################################################
	//Name - checkLPL_PageNavigation_viaNavPanel
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - Dropdown name under test.
	//Returns - This function returns a String variable containing all Actual dropdown values from UI.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public void checkLPL_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception{
	try{
		wait = new WebDriverWait(driver,40);
		String pageTitle = "";
		strPageName = strPageName.trim();
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		// Action Required for hover over
		Actions action = new Actions(driver);
		
		//Apply switch case for each page
		switch (strPageName){
		
		//*****************   Accounts dropdown   *****************
		case "Summary": case "Account Info": case "Balances":case "Positions": case "History": case "Gain/Loss": case "Asset Allocation":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleLPL, false, tab_Accounts, dd_Accounts, "Account Tab", strPageName, "LPL");
			break;
			
		//*****************   Trading dropdown   *****************
		case "Stocks + ETFs": case "Options": case "Mutual Funds": case "Order Status":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleLPL, false, tab_Trading, dd_Trading, "Trading Tab", strPageName, "LPL");
			break;
		//*****************   Research Dropdown	(Non-Morning star pages)  *****************
		case "Find Symbol":case "Option Chains":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleLPL, false, tab_Research, dd_Research, "Research Tab", strPageName, "LPL");
			break;
		//*****************   Research Dropdown	(Morning star pages)   *****************
		case "Market Overview": case "Advanced Chart": case "Market Events Calendar": case "Stock Alerts": 
		case "Stock Fundamentals Report": case "Mutual Fund / ETF Fundamentals Report": case "Screener":
		pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleLPLMStar, false, tab_Research, dd_Research, "Research Tab", strPageName, "LPL");
		break;	
		//*****************   Research >> Checking Calculators Page   *****************
		case "College Cost": case "IRA Comparison": case "IRA Eligibility": case "IRA Roth Conversion": case "Retirement Cost":
			Thread.sleep(500);
			//Perform hover-over action to see dropdown list   *****************
			WM_CommonFunctions.hoverOver_Item(tab_Research, "Research Tab", rowNum);
			//now click on Calculators to show another dropdown
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Research, "Calculators");
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Research_Calculator, strPageName);
			
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			Thread.sleep(2000);
			driver.switchTo().frame(0);
			Thread.sleep(1000);
			pageTitle = (WM_CommonFunctions.object_getText(page_TitleTools, rowNum)).trim();
			//System.out.println("title is " + pageTitle);
			driver.switchTo().defaultContent();
			break;
			//end of Research drop down list dd_Research_Calculator
		//*****************    Tools dropdown	
		case "Add Account": case "Share Accounts": case "Preferences": case "Message Center":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleLPL, false, tab_Tools, dd_Tools, "Tools Tab", strPageName, "LPL");
			break;
			//end of Tools & Calculators drop down list
		//*****************   Client Services dropdown   *****************
		case "Forms": case "Resources":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleLPL, false, tab_ClientServices, dd_ClientServices, "Client Services Tab", strPageName, "LPL");
			break;
		//*****************   About Us dropdown	   *****************
		case "About LPL Financial":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleLPL, false, tab_AboutUs, dd_AboutUs, "About Tab", strPageName, "LPL");
			break;
			
		case "Documents":
			Thread.sleep(500);
			//Perform hover-over action to see dropdown list
			WM_CommonFunctions.clickItem(tab_Documents, "Documents tab", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_TitleLPL, rowNum)).trim();
			break;
		//	*****************   Default case	   *****************	
		default:
			System.out.println("Method : checkLPL_PageNavigation_viaNavPanel >> No matching Select-Case string found.");
			pageTitle = "";
			break;
		}

		pageNavigation_checkTitle (rowNum,  pageTitle,  strPageName,  strExpectedPageTitle,  "LPL");
		
	}//End of Try block
	catch (Exception e){
		e.printStackTrace();
		Reporting.reportStep(rowNum, "Failed", strPageName + " - Exception thrown while locating page.", "", "LPL_PageNavCheck", driver, "", "", null);	
		
	}//End of catch block
	}
	//************************************************************************************************************
		//######################################################################################################################
		//######################################################################################################################
		//Name - checkFDB_PageNavigation_viaNavPanel
		//Description - This function returns a string of all expected dropdown values for given field .
		//Parameters - Dropdown name under test.
		//Returns - This function returns a String variable containing all Actual dropdown values from UI.
		//Created By - Neelesh Vatsa
		//######################################################################################################################
		//######################################################################################################################
		public void checkFDB_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception{
		try{
			wait = new WebDriverWait(driver,40);
			String pageTitle = "";
			strPageName = strPageName.trim();
			WM_CommonFunctions.wait_ForPageLoad(rowNum);
			// Action Required for hover over
			Actions action = new Actions(driver);
			
			//Apply switch case for each page
			switch (strPageName){
			case "Home":
				Thread.sleep(100);
				//Perform hover-over action to see dropdown list
				WM_CommonFunctions.clickItem(tab_Home, "Contact Us tab", rowNum);
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
				break;
			//*****************   Accounts dropdown   *****************
			case "Account Information": case "Balances":case "Positions": case "Transactions": case "Gain/Loss": case "Asset Allocation": case "Download":
				pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Accounts, dd_Accounts, "Account Tab", strPageName, "FDB");
				break;
				
			//*****************   Trading dropdown   *****************
			case "Stocks": case "Options": case "Mutual Funds": case "Order Status":
				pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Trading, dd_Trading, "Trading Tab", strPageName, "FDB");
				break;
			//*****************   Research Dropdown	   *****************
			case "Find Symbol":  case "Option Chains": 
				pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_QuotesAndMarket, dd_Research, "Research Tab", strPageName, "LPL");
				break;
			case "E-Documents":
				Thread.sleep(500);
				pageTitle = "";
				String handle= driver.getWindowHandle();
				System.out.println(handle);
				WM_CommonFunctions.clickItem(tab_Documents, "E-Documents tab", rowNum);
				Thread.sleep(1000);
				// switch to new window
				driver.switchTo().window("eDocs");
				Thread.sleep(5000);
				pageTitle = (WM_CommonFunctions.object_getText(page_FDBeDoc, rowNum)).trim();
				//switch back to main window
				driver.switchTo().window(handle);
				
				break;
			//*****************   Research >> Checking Calculators Page   *****************
			case "Contact Us":
				Thread.sleep(100);
				//Perform hover-over action to see dropdown list
				WM_CommonFunctions.clickItem(tab_ContactUs, "Contact Us tab", rowNum);
				wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
				pageTitle = (WM_CommonFunctions.object_getText(page_Title_Type3, rowNum)).trim();
				break;
				//end of Research drop down list dd_Research_Calculator
			//*****************    Tools dropdown	
			case "Share Accounts": case "Preferences": case "Message Center": case "Application Status": 
				pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Tools, dd_Tools, "Tools Tab", strPageName, "FDB");
				break;
				//end of Tools & Calculators drop down list
			
			//	*****************   Default case	   *****************	
			default:
				System.out.println("Method : checkFDB_PageNavigation_viaNavPanel >> No matching Select-Case string found.");
				pageTitle = "";
				break;
			}
			System.out.println("actual title = " + pageTitle);
			pageNavigation_checkTitle (rowNum,  pageTitle,  strPageName,  strExpectedPageTitle,  "FDB");
			
		}//End of Try block
		catch (Exception e){
			e.printStackTrace();
			Reporting.reportStep(rowNum, "Failed", strPageName + " - Exception thrown while locating page.", "", "FDB_PageNavCheck", driver, "", "", null);	
			
		}//End of catch block
		}
	//######################################################################################################################
	//######################################################################################################################
	//Name - checkLPL_PageNavigation_viaNavPanel
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - Dropdown name under test.
	//Returns - This function returns a String variable containing all Actual dropdown values from UI.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public void checkDAV_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception{
	try{
		wait = new WebDriverWait(driver,40);
		String pageTitle = "";
		strPageName = strPageName.trim();
		WM_CommonFunctions.wait_ForPageLoad(rowNum);

		
		//Apply switch case for each page
		switch (strPageName){
		
		//*****************   Accounts dropdown   *****************
		case "Summary": case "Balances":case "Positions": case "Positions Zoom": case "Activity": 
		case "Unrealized Gain/Loss": case "Realized Gain/Loss": case "Household Overview": case "Download":
		case "Order Status": case "Client Documents": case "Shareholder Library":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Accounts, dd_Accounts, "Account Tab", strPageName, "DAV");
			break;
			
		//*****************   Research Dropdown	   *****************
		case "Find Symbol": case "Option Chains":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Research, dd_Research, "Research Tab", strPageName, "DAV");
			break;
		
		//*****************    Tools dropdown   *****************
		case "Share Accounts": case "Preferences": case "Message Center":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Tools, dd_Tools, "Tools Tab", strPageName, "DAV");
			break;
		case "Contact Us":
			pageTitle = "";
			String handle= driver.getWindowHandle();
			System.out.println(handle);
			WM_CommonFunctions.clickItem(tab_ContactUs, "Contact tab", rowNum);
			Thread.sleep(1000);
			// switch to new window
			driver.switchTo().window("contactus");
			Thread.sleep(5000);
			pageTitle = (WM_CommonFunctions.object_getText(page_DAVContactUsTools, rowNum)).trim();
			//switch back to main window
			driver.switchTo().window(handle);
			//pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_ContactUs, dd_AboutUs, "About Tab", strPageName, "DAV");
			break;
			
		case "Client Documents & Research Reports":
			Thread.sleep(500);
			WM_CommonFunctions.clickItem(tab_ClientReports, "Client Documents & Research Reports tab", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			break;
		case "Account Shareholder Library":
			Thread.sleep(500);
			WM_CommonFunctions.clickItem(tab_ShareholderLibrary, "Shareholder Library tab", rowNum);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			break;
		//	*****************   Default case	   *****************	
		default:
			
			System.out.println("Method : checkLPL_PageNavigation_viaNavPanel >> No matching Select-Case string found.");
			pageTitle = "";
			break;
		}

		pageNavigation_checkTitle (rowNum,  pageTitle,  strPageName,  strExpectedPageTitle,  "DAV");
		
	}//End of Try block
	catch (Exception e){
		e.printStackTrace();
		Reporting.reportStep(rowNum, "Failed", strPageName + " - Exception thrown while locating page.", "", "LPL_PageNavCheck", driver, "", "", null);	
		
	}//End of catch block
	}
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - check_PageNavigation_viaNavPanel
	//Description -This function verifies that a page loads successfully when navigated via Navigation panel
	//Parameters - row num, page name
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
		//######################################################################################################################
	public void checkBOW_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception {
	try{// TODO Auto-generated method stub
		wait = new WebDriverWait(driver,40);
		String pageTitle = "";
		strPageName = strPageName.trim();
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		
		//Switch stmnt for each page option
		switch (strPageName){
		//navigate to the page
		case "Summary": case "Balances": case "Positions": case "Positions Zoom": case "Activity": case "Unrealized Gain/Loss":	
		case "Realized Gain/Loss": case "Asset Allocation": case "Download": case "Statements & Documents": case "Account Info":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Accounts, dd_Accounts, "Accounts Tab", strPageName, "BOW");
			break;
		
		//END of Accounts drop down  list	
		//########  Start of Dropdowns for Tradings  ########
		case "Equities": case "Mutual Funds": case "Options": case "Order Status":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Trading, dd_Trading, "Tradings Tab", strPageName, "BOW");
			break;
		//end of Trading drop down list	
		case "Dashboard": case "Investments": case "Performance": case "Volatility": case "Account Statistics": case "Educational Insights":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitlePortfolio, false, tab_Portfolio, dd_Portfolio, "Portfolio Tab", strPageName, "BOW");
			break;
			//end of Portfolio drop down list
			
		case "Find Symbol" : case "Option Chains" : case "Market Overview" : case "Advanced Chart" : case "Market Events Calendar" : 
		case "Stock Alerts" : case "Stock Fundamentals Report" : case "Mutual Fund / ETF Report" : case "Screener" :
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_Research, dd_Research, "Research Tab", strPageName, "BOW");
			break;
			//end of Research drop down list
			
		case "College Cost Calculator" : case "IRA Comparison Calculator" : case "IRA Eligibility Calculator" : case "IRA Roth Conversion Calculator" : 
		case "Retirement Cost Calculator" : 
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_TitleTools, true, tab_ToolsNCal, dd_ToolsNCal, "Tools and calculator Tab", strPageName, "BOW");
			break;
			//end of Tools & Calculators drop down list
			
		case "Transfer Money" : case "Secure Message Center" : case "Preferences" : case "Forms" : case "Help":
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_AccountServices, dd_Tools, "Account Services Tab", strPageName, "BOW");
			break;
			
		case "Open a New Account" : 
			pageTitle = pageNavigation_getPageTitle(rowNum, "hover", page_Title, false, tab_AccountServices, dd_Tools, "Account Services Tab", strPageName, "BOW");
			driver.navigate().back();
			break;
			//end of Account Services drop down list
		default:
			System.out.println("Drop down option not found in switch -case, please check");
			Reporting.reportStep(rowNum, "Failed", strPageName + " - not found in expected list of option names. Check Switch stmnt.", "", "", driver, "", "", null);	
			break;
		}
		Thread.sleep(100);
		
		//match the page title with page name and add passed or failed in the report accordingly
		pageNavigation_checkTitle (rowNum,  pageTitle,  strPageName,  strExpectedPageTitle,  "BOW");
		
	}//End of Try block
	catch (Exception e){
		e.printStackTrace();
		Reporting.reportStep(rowNum, "Failed", strPageName + " - Exception thrown while locating page.", "", "BOW_PageLoadCheck", driver, "", "", null);	
		
	}//End of catch block
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - pageNavigation_checkTitle
	//Description - This function checks if the Page Title is as expected for page navigation Scripts .
	//Parameters - row num, current page title, page name, expected page title and client name .
	//Returns - void.s
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public void pageNavigation_checkTitle(int rowNum, String pageTitle, String strPageName, String strExpectedPageTitle, String strClientName) throws Exception{
		Thread.sleep(100);
		
		//match the page title with page name and add passed or failed in the report accordingly
		if(!pageTitle.equals("")){
			
			//if(StringManipulation.compareString_Equality( pageTitle, strExpectedPageTitle.trim())){
			if(strExpectedPageTitle.contains(pageTitle)) {
				System.out.println("PASSED - Page navigation check Passed for - " + strPageName);
				Reporting.reportStep(rowNum, "Passed", strPageName + " - has loaded successfully.", "", "", driver, "", "", null);	
			}else{
				System.out.println("FAILED - Page navigation check failed for - " + strPageName + " Expected / Actual = " + strExpectedPageTitle + "/" +pageTitle);
				Reporting.reportStep(rowNum, "Failed", strPageName + " - page did not load successfully Actual / Expected = " + pageTitle + "/" +  strExpectedPageTitle, "", strClientName + "_PageTitleCheck", driver, "", "", null);		
				
			}//End of nested If-Else
		}else{
			Reporting.reportStep(rowNum, "Failed", strPageName + " - page title could not be located by the script.", "", strClientName + "_PageTitleCheck", driver, "", "", null);	
		}//End of If-Else
	}
	
	
	//######################################################################################################################
		//######################################################################################################################
		//Name - pageNavigation_getPageTitle
		//Description - This function returns the Actual Page Title after navigating to required page.
		//Parameters - row num, hover or click, navigation tab object, dropdown list name, navigation tab name, page name n client name
		//Returns - string with current page title vaulue
		//Created By - Neelesh Vatsa
		//######################################################################################################################
		//######################################################################################################################
		public String pageNavigation_getPageTitle(int rowNum, String hoverOrClick, WebElement pageTitleObj, boolean iFrame, WebElement navigationTabObject, List objDropdown, String tabName, String strPageName, String strClientName) throws Exception{
			 try{
				// pageNavigation_getPageTitle(rowNum, "hover", true, tab_Accounts, dd_Accounts, "Account Tab", strPageName, "LPL")
					Thread.sleep(100);
					System.out.println("Before clicking on option");
					if(! WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, objDropdown, strPageName, navigationTabObject, tabName, hoverOrClick, strClientName)){
						return "";
					}
					System.out.println("After clicking on option");
					if(strClientName!="RWB"){
						System.out.println("Nav panel visib");
						wait.ignoring(StaleElementReferenceException.class)
						.until(ExpectedConditions.visibilityOf(panel_Navigation));
					}
					String pageActualTitle = "";
					
					// if title is contained in a frame then switch to it
					int intTitleTry = 0;
					Thread.sleep(100);
					while(intTitleTry <=2){
						if(iFrame){
							Thread.sleep(2000);
							driver.switchTo().frame(0);
							pageActualTitle = (WM_CommonFunctions.object_getText(pageTitleObj, rowNum)).trim();
							driver.switchTo().defaultContent();
							 
						}else{
							
							pageActualTitle = (WM_CommonFunctions.object_getText(pageTitleObj, rowNum)).trim();
							System.out.println("Title = " + pageActualTitle);
						}//end of if-else
						if(pageActualTitle!=""){
							break;
						}
						System.out.println("Page Title not found in attempt - " + intTitleTry + ". Trying again.");
						intTitleTry++;
					}//end of while loop -- trying to loacte title more than once if not found
					return pageActualTitle;
			 }catch(Exception e){
				 System.out.println("Exception found in method - pageNavigation_getPageTitle");
				Reporting.reportStep(rowNum, "Failed", "Exception found in @Method- pageNavigation_getPageTitle", "", strClientName + "_PageTitleCheck", driver, "", "", null);		
				e.printStackTrace();	
				 return "";
			 }//End of try-catch
		}//End of function
		
		
		
		//######################################################################################################################
		//######################################################################################################################
		//Name - BOW_Check_AllNavigationPanelObjects
		//Description - Parent function that verifies all navigation panel element in BOW .
		//Parameters - row num
		//Returns - Void
		//Created By - Neelesh Vatsa
		//######################################################################################################################
		//######################################################################################################################	
		public void BOW_Check_AllNavigationPanelObjects(int rowNum, String tradingValues){
			try{
				WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
				
				WebElement[] arrNavElements = new WebElement[]{
						tab_Accounts,tab_Trading,tab_Portfolio,tab_Research,tab_Tools,tab_AccountServices,tab_Banking};
				
				String[] arrPanelKeyword = new String[]{"Accounts", "Trading", "Portfolio", "Research", "Tools & Calculators", "Account Services", "Banking"}; 
				
				System.out.println("Checking Navigation panel tabs for BOW");
				
				
				for(int i = 0; i< arrNavElements.length; i++){
					if(!(CommonUtils.isElementPresent(arrNavElements[i]))){
						System.out.println( arrPanelKeyword[i] + " - Tab not found on navigation panel - ");
						Reporting.reportStep(rowNum, "Failed", arrPanelKeyword[i] + "-  is missing from navigation panel.", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
						
					}else{
						System.out.println(arrPanelKeyword[i] + " tab has Passed");
						Reporting.reportStep(rowNum, "Passed", arrPanelKeyword[i] + " - tab is present.", "", "", driver, "", "", null);}//End of If-Else
					rowNum = rowNum + 1;
				}//End of FOR
				
				//#######################   Check dropdown list for Accounts    #######################
				String str_ExpValues = "";
				System.out.println("");
				System.out.println("Checking Dropdown Values for Accounts tab:");
				if(CommonUtils.isElementPresent(tab_Accounts)){
					str_ExpValues = BOW_ExpectedNavigationPanelDDList("Accounts", tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Accounts", dd_Accounts, tab_Accounts, str_ExpValues, panel_Navigation, "hover");
				}else{
					System.out.println("Accounts tab object not found in Navigation Panel. Please check!!");
					Reporting.reportStep(rowNum, "Failed", "Accounts tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					//Navigate to home page if tab not found
				}
				rowNum = rowNum + 1;
				//#######################   Check dropdown list for Tradings    #######################
				System.out.println("");
				System.out.println("Checking Dropdown Values for Trading tab:");
				if(CommonUtils.isElementPresent(tab_Trading)){
					str_ExpValues = BOW_ExpectedNavigationPanelDDList("Tradings", tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Tradings", dd_Trading, tab_Trading, str_ExpValues, panel_Navigation, "hover");
				}else{
					Reporting.reportStep(rowNum, "Failed", "Tradings tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
				}
				rowNum = rowNum + 1;
				
				//#######################   Check dropdown list for Portfolio    #######################
				System.out.println("");
				System.out.println("Checking Dropdown Values for Portfolio tab:");
				if(CommonUtils.isElementPresent(tab_Portfolio)){
					str_ExpValues = BOW_ExpectedNavigationPanelDDList("Portfolio", tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Portfolio", dd_Portfolio, tab_Portfolio, str_ExpValues, panel_Navigation, "hover");
				}else{
					Reporting.reportStep(rowNum, "Failed", "Portfolio tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
				}
				rowNum = rowNum + 1;
				
				//#######################   Check dropdown list for Research    #######################
				System.out.println("");
				System.out.println("Checking Dropdown Values for Research tab:");
				if(CommonUtils.isElementPresent(tab_Research)){
					str_ExpValues = BOW_ExpectedNavigationPanelDDList("Research", tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Research", dd_Research, tab_Research, str_ExpValues, panel_Navigation, "hover");
				}else{
					Reporting.reportStep(rowNum, "Failed", "Research tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
				}
				rowNum = rowNum + 1;
				
				//#######################   Check dropdown list for Tools & Calculators    #######################
				System.out.println("");
				System.out.println("Checking Dropdown Values for tools & Calculator tab:");
				if(CommonUtils.isElementPresent(tab_Tools)){
					str_ExpValues = BOW_ExpectedNavigationPanelDDList("Tools & Calculators", tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Tools & Calculators", dd_ToolsNCal, tab_Tools, str_ExpValues, panel_Navigation, "hover");
				}else{
					Reporting.reportStep(rowNum, "Failed", "Tools And Calculator tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
				}
				rowNum = rowNum + 1;
				
				//#######################   Check dropdown list for Account Services    #######################
				System.out.println("");
				System.out.println("Checking Dropdown Values for Account Services tab:");
				if(CommonUtils.isElementPresent(tab_AccountServices)){
					str_ExpValues = BOW_ExpectedNavigationPanelDDList("Account Services", tradingValues);
					System.out.println("Expected dropdown Values = " + str_ExpValues);
					WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "AccountServices", dd_Tools, tab_AccountServices, str_ExpValues, panel_Navigation, "hover");
				}else{
					Reporting.reportStep(rowNum, "Failed", "AccountServices tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
				}
				rowNum = rowNum + 1;
						
			}//End of TRY
			catch(Exception e){
				System.out.println("Excpetion found in method - BOW_Check_AllNavigationPanelObjects");
				e.printStackTrace();
				}//End of Catch
		}//END OF FUNCTION
		//######################################################################################################################
		//######################################################################################################################
		//######################################################################################################################
		//######################################################################################################################
		//Name - BOW_ExpectedNavigationPanelDDList
		//Description - This function returns a string of all expected dropdown values for given field .
		//Parameters - Dropdown name under test.
		//Returns - This function returns a String variable containing all Actual dropdown values from UI.
		//Created By - Neelesh Vatsa
		//######################################################################################################################
		//######################################################################################################################
		public String BOW_ExpectedNavigationPanelDDList(String strDropdownName, String tradingValues){
			String strExpValues = "";
			switch (strDropdownName){
			case "Accounts":
				strExpValues = "Summary;Balances;Positions;Positions Zoom;Activity;Unrealized Gain/Loss;Realized Gain/Loss;Asset Allocation;Download;Statements & Documents;Account Info";
				break;
			case "Tradings":
				if(tradingValues.equalsIgnoreCase("")){
					strExpValues = "Equities;Mutual Funds;Options;Order Status";
				}else{
					strExpValues = tradingValues;
				}
				break;
			case "Portfolio":
				strExpValues = "Dashboard;Investments;Performance;Volatility;Account Statistics;Educational Insights";
				break;
				
			case "Research":
				strExpValues = "Find Symbol;Option Chains;Market Overview;Advanced Chart;Market Events Calendar;Stock Alerts;Stock Fundamentals Report;Mutual Fund / ETF Report;Screener";
				break;
				
			case "Tools & Calculators":
				strExpValues = "College Cost Calculator;IRA Comparison Calculator;IRA Eligibility Calculator;IRA Roth Conversion Calculator;Retirement Cost Calculator";
				break;
				
			case "Account Services":
				strExpValues = "Transfer Money;Secure Message Center;Preferences;Open a New Account;Change Address;Change Email Address;Register for Mobile Access;Change Mobile Password;Change Login Password;Account Nicknames;Forms;Help";
				break;
			default:
				System.out.println("Method : BOW_ExpectedNavigationPanelDDList >> No matching Select-Case string found.");
				strExpValues = "";
				break;
			}
			//System.out.println(strActualValues);
			return strExpValues;
		}
//************************  End of functions **********************************	
//******************************************************************************
}
	