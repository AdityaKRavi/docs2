package PageObjects.WM;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;

public class SummaryPage {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	
	// ************* Top Navigation Elements ************************************
		//***************************************************************************
		//main navigation panel and element used for screenshot
		@FindBy(how = How.ID, using = "privateNav")
		public WebElement panel_Navigation;
		
		// Page Titles

		//used by BOW	
		@FindBy(how = How.CSS, using = ".pr-header-top-title")
		public WebElement page_TitlePortfolio;
		
		// Page Titles
		@FindBy(how = How.CSS, using = "#pageTitle>h1")
		public WebElement page_Title;
		
		//used in FDB for Contact us page title
		@FindBy(how = How.CSS, using = ".page-title")
		public WebElement page_Title_Type3;
		
		@FindBy(how = How.CSS, using = "#pageTitle>h1 .titlePage")
		public WebElement page_TitleLPL;
		
		//Page title where calculator pages have iframe in use
		@FindBy(how = How.CSS, using = "#toolTitle div span")
		public WebElement page_TitleTools;
		
		//used by RWB 2 and 4
		@FindBy(how = How.CSS, using = ".pageTitle")
		public WebElement page_Title2;
		
		@FindBy(how = How.CSS, using = ".idc-pageTitle>h2")
		public WebElement page_Title4;
		
		//FDB pro Investor Page title
		@FindBy(css = ".img-heading>img[src*='recent_activity_title.gif']")
		public WebElement pageTitleFDBPro;

		//Page title for FDB eDoc window
		@FindBy(how = How.CSS, using = "html>body>table>tbody>tr>td>font>b")
		public WebElement page_FDBeDoc;
		
		@FindBy(how = How.CSS, using = ".field-item.even>div div:nth-child(1) strong:nth-child(1)")
		public WebElement page_DAVContactUsTools;
		
		//used by RGL Script
		@FindBy(how = How.CSS, using = ".pageTitleHead")
		public WebElement page_TitleHead;
		
		//used in STF nav panel
		@FindBy(how = How.CSS, using = "#pageTitle>h1[style*=\"margin-left\"]")
		public WebElement page_Title_ContactUs;
		
		@FindBy(how = How.CSS, using = ".fleft.pcart")
		public WebElement page_Awards_Cart;
		
		@FindBy(how = How.CSS, using = ".SNpagetitle")
		public WebElement page_Title1;
		
	
		//USB and STF Summary Page title
		@FindBy(how = How.XPATH, using = ".//*[@id='pageTitle']/h1")
		public WebElement pageTitle;
		
		//Download button for WDB
		@FindBy(css = ".mediumButton[value='Download']")
		public WebElement downloadButton;
		
		//LPL Page title
		@FindBy(css = ".titlePage")
		public WebElement pageTitleLpl;
		
		@FindBy(how = How.XPATH, using = "//a[contains(text(), 'Logout')]")
		public WebElement fdbPro_Logout;
		
	
		
		//FDB pro Admin Page title
		@FindBy(css = "input[name='search_term']")
		public WebElement pageTitleFDBPro_Admin;
			
		
	public SummaryPage(){
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }


	//BOW popup close button
	@FindBy(how = How.CSS, using = ".x-tool.x-tool-close")
	public WebElement page_BOW_Popup_Btn;
	@FindBy(how = How.XPATH, using = "//*[@class=\"close dim glyphicon glyphicon-remove pointer\"]")
	public List<WebElement> page_BOW_Popup_Close;
	

	//********************************    Methods    ********************************
	//##################################################################################
	  //Function name	: loginPage_SSO_CheckElements_ArePresent()
	  //Class name		: LoginPage
	  //Description 	: Check login page elements - userid, account no and Login button.
	  //Parameters 		: None
	  //Assumption		: SSO Login page elements are present
	  //Developer		: Neelesh Vatsa
	  //##################################################################################
	public boolean checkPageTitle_IsPresent(String clientName) throws InterruptedException {
		//boolean flgElementPresent = true;
		String pageName = "";
		switch (clientName) {
		case "USB":
		case "USB_Trust":
			if((CommonUtils.isElementPresent(pageTitle))){
				pageName = (pageTitle).getText().trim();
				if(pageName.equals("Summary")){
					System.out.println("PASSED | Homepage check >> USB - Page title is Summary..");
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title is not Summary for USB.");
				return false;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for USB.");
			break;
		case "STF":
			if((CommonUtils.isElementPresent(pageTitle))){
				pageName = (pageTitle).getText().trim();
				if(pageName.equals("Account Summary")){
					System.out.println("PASSED | Homepage check >> STF - Page title is Account Summary.");
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title is not Account Summary for STF.");
				return false;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for STF.");
			break;
		case "WDB":
			if((CommonUtils.isElementPresent(downloadButton))){
				System.out.println("PASSED | Homepage check >> WDB - Download button is present on the Home page.");
				return true;
			}
			System.out.println("FAILED | Homepage check - Download button is not found on WDB home page.");
			break;
		case "LPL":
			if((CommonUtils.isElementPresent(pageTitleLpl))){
				pageName = (pageTitleLpl).getText().trim();
				if(pageName.equals("Summary")){
					System.out.println("PASSED | Homepage check >> LPL - Page title is Summary.");
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title is not Summary for LPL.");
				return false;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for LPL.");
			break;
			
		case "1DB":
			if((CommonUtils.isElementPresent(pageTitle))){
				pageName = (pageTitle).getText().trim();
				if(pageName.equals("Home")){
					System.out.println("PASSED | Homepage check >> FDB - Page title is Home");
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title is not Home for 1DB.");
				return false;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for 1DB.");
			break;
			
		case "FDBPro":	
			if((CommonUtils.isElementPresent(pageTitleFDBPro))){
				System.out.println("PASSED | Homepage check >> FDB Pro Investor - Page heading 'my recent activity' is present.");
				
				return true;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for FDB Pro Investor.");
			break;
		
		case "FDBPro_Admin":	
			if((CommonUtils.isElementPresent(pageTitleFDBPro_Admin))){
				System.out.println("PASSED | Homepage check >>  Search Term textbox is present for FDBPro Admin.");
				//logout
				fdbPro_Logout.click();
				TimeUnit.SECONDS.sleep(3);
				return true;
				
			}
			System.out.println("FAILED | Homepage check -  Search Term textbox is not present for FDBPro Admin");
			break;
		
		case "RWB":
			if((CommonUtils.isElementPresent(pageTitle))){
				pageName = (pageTitle).getText().trim();
				if(pageName.equals("Dashboard")){
					System.out.println("PASSED | Homepage check >> RWB - Page title is Dashboard.");
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title is not Dashboard for RWB.");
				return false;
			}
			System.out.println("FAILED | Homepage checkILED - Page title object not found for RWB.");
			break;
		case "BOW":	
			if((CommonUtils.isElementPresent(pageTitle))){
				pageName = (pageTitle).getText().trim();
				if(pageName.equals("Accounts - Summary")){
					System.out.println("PASSED | Homepage check >> BOW - Page title is Accounts - Summary.");
					Thread.sleep(1000);
					if((CommonUtils.isElementPresent(page_BOW_Popup_Btn))){
						page_BOW_Popup_Btn.click();
					}//End of IF condition to check if Pop UP (Close) button exists as per AUTO-591
					if((page_BOW_Popup_Close.size()>0)){
						page_BOW_Popup_Close.get(4).click();
					}//End of IF condition to check if Pop UP (Close) button exists as per AUTO-591
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title is not Accounts - Summary for BOW.");
				return false;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for BOW.");
			break;
		case "DAV":	
			if((CommonUtils.isElementPresent(pageTitle))){
				pageName = (pageTitle).getText().trim();
				if(pageName.equals("Summary")){
					System.out.println("PASSED | Homepage check >> DAV - Page title is Summary.");
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title is not Summary for DAV.");
				return false;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for DAV.");
			break;
		case "USB_Admin":
		case "STF_Admin":
		case "LPL_Admin":
		case "1DB_Admin":
		case "BOW_Admin":
		case "DAV_Admin":
		case "RWB_Admin":
			if((CommonUtils.isElementPresent(pageTitle))){
				pageName = (pageTitle).getText().trim();
				if(pageName.equals("Investor Users � Search")){
					System.out.println("PASSED | Homepage check >> " + clientName + " - Page title is 'Investor Users � Search'.");
					return true;
				}
				System.out.println("FAILED | Homepage check - Page title does not match expected value - 'Investor Users � Search' for " + clientName);
				return false;
			}
			System.out.println("FAILED | Homepage check - Page title object not found for " + clientName);
			break;
		default:
			System.out.println("FAILED - <METHOD: checkPageTitle_IsPresent><CLASS: SummaryPage> - Given client name is not present.");
			return false;
		}
		return false;
	}
	







	

}
