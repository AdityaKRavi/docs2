package PageObjects.WM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;

public class Misc_pgLinks {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	ResultTable_Section ResultTable_Section = new ResultTable_Section();
	StringManipulation StringManipulation = new StringManipulation();
	Trade_Widget tw = new Trade_Widget();
	HoldingsZoom_Page hp = new HoldingsZoom_Page();
	WebDriverWait wait;
	
	
	//************************   Test Objects   ************************
	//******************************************************************
	@FindBy(how = How.ID, using = "shortcutExcelExport")
	public WebElement link_ExpprtToExcel;
	
	@FindBy(how = How.ID, using = "shortcutPrint")
	public WebElement link_Print;
	
	@FindBy(how = How.CSS, using = "#shortcutMenu>a:first-child")
	public WebElement link_Alerts;
	
	//--------------  Constructor to initialize all elements  --------------
		public Misc_pgLinks(){
	        //This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	    }

	//******************************************************************
	//***********************    Functions   ***************************
	
	public void verify_linkPresence(String expLinks, int rowNum){
		String[] AllLinks = expLinks.split(";");
		for(String link : AllLinks ) {
			switch (link) {
			case "Export To Excel":
				if(CommonUtils.isElementPresent(link_ExpprtToExcel)){
					System.out.println("Export to Excel link is present.");
					Reporting.reportStep(rowNum, "Passed", "Export to excel is present. ", "", "", driver, "", "", null);	
					
				}else {
					System.out.println("Export to Excel link is missing from the page.");
					Reporting.reportStep(rowNum, "Failed", "Export to excel link is missing. ", "", "Excel_Linkcheck", driver, "", "", null);	
					
				}
				break;
			case "Alert":
			case "Alerts":
				if(CommonUtils.isElementPresent(link_Alerts)){
					System.out.println("Alert link is present.");
					Reporting.reportStep(rowNum, "Passed", "Alert link is present. ", "", "", driver, "", "", null);	
					
				}else {
					System.out.println("Alert link is missing from the page.");
					Reporting.reportStep(rowNum, "Failed", "Alert link is missing. ", "", "Excel_Linkcheck", driver, "", "", null);	
					
				}
				break;
			case "Print":
				if(CommonUtils.isElementPresent(link_Print)){
					System.out.println("Print link is present.");
					Reporting.reportStep(rowNum, "Passed", "Print link is present. ", "", "", driver, "", "", null);	
					
				}else {
					System.out.println("Print link is missing from the page.");
					Reporting.reportStep(rowNum, "Failed", "Print link is missing. ", "", "Excel_Linkcheck", driver, "", "", null);	
					
				}
				break;
			}
		}
		
	}
}//end of class
