package PageObjects.WM;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class BOW_NavigationPanel {
	
	/// DELETE THIS
	
	
	
	/*WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	// ************* Top Navigation Elements ************************************
	//***************************************************************************
	//main navigation panel and element used for screenshot
	@FindBy(how = How.ID, using = "privateNav")
	public WebElement panel_Navigation;
	

	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Accounts')]")
	public WebElement tab_Accounts;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Trading')]")
	public WebElement tab_Trading;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Portfolio')]")
	public WebElement tab_Portfolio;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Research')]")
	public WebElement tab_Research;

	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Tools & Calculator')]")
	public WebElement tab_Tools;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Account Services')]")
	public WebElement tab_AccountServices;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li/a[contains(text(),'Banking')]")
	public WebElement tab_Banking;
	
	// Page Titles
	@FindBy(how = How.CSS, using = "#pageTitle>h1")
	public WebElement page_Title;
	
	@FindBy(how = How.CSS, using = "#toolTitle div span")
	public WebElement page_TitleTools;
	
	@FindBy(how = How.CSS, using = ".pr-header-top-title")
	public WebElement page_TitlePortfolio;
	
	
	//*************************  Navigation Panel  ***********************************
	//Navigation panel list
	@FindBy(how = How.XPATH, using = ".//*[@id='mainNav']/li")
	public List<WebElement> dd_NavPanel_List;
	
	//*************************  DropwDown List  ***********************************
	@FindBy(how = How.CSS, using = ".nav-accounts li")
	public List<WebElement> dd_Accounts;
 
	@FindBy(how = How.CSS, using = ".nav-trading li")
	public List<WebElement> dd_Trading;
	
	@FindBy(how = How.CSS, using = ".nav-sqope li")
	public List<WebElement> dd_Portfolio;
	
	@FindBy(how = How.CSS, using = ".nav-research li")
	public List<WebElement> dd_Research;
	
	@FindBy(how = How.CSS, using = ".nav-calculators li")
	public List<WebElement> dd_ToolsNCal;
	
	@FindBy(how = How.CSS, using = ".nav-settings li")
	public List<WebElement> dd_AccountServices;
	
	//--------------  Constructor to initialize all elements  --------------
	public BOW_NavigationPanel(WebDriver driver){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
//*************************************************   FUNCTIONS   **********************************************************
//**************************************************************************************************************************
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - BOW_Check_AllNavigationPanelObjects
	//Description - Parent function that verifies all navigation panel element in BOW .
	//Parameters - row num
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################	
	public void BOW_Check_AllNavigationPanelObjects(int rowNum, String tradingValues){
		try{
			WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
			
			WebElement[] arrNavElements = new WebElement[]{
					tab_Accounts,tab_Trading,tab_Portfolio,tab_Research,tab_Tools,tab_AccountServices,tab_Banking};
			
			String[] arrPanelKeyword = new String[]{"Accounts", "Trading", "Portfolio", "Research", "Tools & Calculators", "Account Services", "Banking"}; 
			
			System.out.println("Checking Navigation panel tabs for BOW");
			
			
			for(int i = 0; i< arrNavElements.length; i++){
				if(!(CommonUtils.isElementPresent(arrNavElements[i]))){
					System.out.println( arrPanelKeyword[i] + " - Tab not found on navigation panel - ");
					Reporting.reportStep(rowNum, "Failed", arrPanelKeyword[i] + "-  is missing from navigation panel.", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
					
				}else{
					System.out.println(arrPanelKeyword[i] + " tab has Passed");
					Reporting.reportStep(rowNum, "Passed", arrPanelKeyword[i] + " - tab is present.", "", "", driver, "", "", null);}//End of If-Else
				rowNum = rowNum + 1;
			}//End of FOR
			
			//#######################   Check dropdown list for Accounts    #######################
			String str_ExpValues = "";
			System.out.println("");
			System.out.println("Checking Dropdown Values for Accounts tab:");
			if(CommonUtils.isElementPresent(tab_Accounts)){
				str_ExpValues = BOW_ExpectedNavigationPanelDDList("Accounts", tradingValues);
				System.out.println("Expected dropdown Values = " + str_ExpValues);
				WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Accounts", dd_Accounts, tab_Accounts, str_ExpValues, panel_Navigation, "hover");
			}else{
				System.out.println("Accounts tab object not found in Navigation Panel. Please check!!");
				Reporting.reportStep(rowNum, "Failed", "Accounts tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
				//Navigate to home page if tab not found
			}
			rowNum = rowNum + 1;
			//#######################   Check dropdown list for Tradings    #######################
			System.out.println("");
			System.out.println("Checking Dropdown Values for Trading tab:");
			if(CommonUtils.isElementPresent(tab_Trading)){
				str_ExpValues = BOW_ExpectedNavigationPanelDDList("Tradings", tradingValues);
				System.out.println("Expected dropdown Values = " + str_ExpValues);
				WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Tradings", dd_Trading, tab_Trading, str_ExpValues, panel_Navigation, "hover");
			}else{
				Reporting.reportStep(rowNum, "Failed", "Tradings tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
			rowNum = rowNum + 1;
			
			//#######################   Check dropdown list for Portfolio    #######################
			System.out.println("");
			System.out.println("Checking Dropdown Values for Portfolio tab:");
			if(CommonUtils.isElementPresent(tab_Portfolio)){
				str_ExpValues = BOW_ExpectedNavigationPanelDDList("Portfolio", tradingValues);
				System.out.println("Expected dropdown Values = " + str_ExpValues);
				WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Portfolio", dd_Portfolio, tab_Portfolio, str_ExpValues, panel_Navigation, "hover");
			}else{
				Reporting.reportStep(rowNum, "Failed", "Portfolio tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
			rowNum = rowNum + 1;
			
			//#######################   Check dropdown list for Research    #######################
			System.out.println("");
			System.out.println("Checking Dropdown Values for Research tab:");
			if(CommonUtils.isElementPresent(tab_Research)){
				str_ExpValues = BOW_ExpectedNavigationPanelDDList("Research", tradingValues);
				System.out.println("Expected dropdown Values = " + str_ExpValues);
				WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Research", dd_Research, tab_Research, str_ExpValues, panel_Navigation, "hover");
			}else{
				Reporting.reportStep(rowNum, "Failed", "Research tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
			rowNum = rowNum + 1;
			
			//#######################   Check dropdown list for Tools & Calculators    #######################
			System.out.println("");
			System.out.println("Checking Dropdown Values for tools & Calculator tab:");
			if(CommonUtils.isElementPresent(tab_Tools)){
				str_ExpValues = BOW_ExpectedNavigationPanelDDList("Tools & Calculators", tradingValues);
				System.out.println("Expected dropdown Values = " + str_ExpValues);
				WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "Tools & Calculators", dd_ToolsNCal, tab_Tools, str_ExpValues, panel_Navigation, "hover");
			}else{
				Reporting.reportStep(rowNum, "Failed", "Tools And Calculator tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
			rowNum = rowNum + 1;
			
			//#######################   Check dropdown list for Account Services    #######################
			System.out.println("");
			System.out.println("Checking Dropdown Values for Account Services tab:");
			if(CommonUtils.isElementPresent(tab_AccountServices)){
				str_ExpValues = BOW_ExpectedNavigationPanelDDList("Account Services", tradingValues);
				System.out.println("Expected dropdown Values = " + str_ExpValues);
				WM_CommonFunctions.Compare_DDList_ActualExpectedValues(rowNum, "AccountServices", dd_AccountServices, tab_AccountServices, str_ExpValues, panel_Navigation, "hover");
			}else{
				Reporting.reportStep(rowNum, "Failed", "AccountServices tab is missing. cant check dropdowns", "", "BOW_NavPanelElelemntCheck", driver, "", "", panel_Navigation);
			}
			rowNum = rowNum + 1;
					
		}//End of TRY
		catch(Exception e){
			System.out.println("Excpetion found in method - BOW_Check_AllNavigationPanelObjects");
			e.printStackTrace();
			}//End of Catch
	}//END OF FUNCTION
	//######################################################################################################################
	//######################################################################################################################
	//######################################################################################################################
	//######################################################################################################################
	//Name - BOW_ExpectedNavigationPanelDDList
	//Description - This function returns a string of all expected dropdown values for given field .
	//Parameters - Dropdown name under test.
	//Returns - This function returns a String variable containing all Actual dropdown values from UI.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	public String BOW_ExpectedNavigationPanelDDList(String strDropdownName, String tradingValues){
		String strExpValues = "";
		switch (strDropdownName){
		case "Accounts":
			strExpValues = "Summary;Balances;Positions;Positions Zoom;Activity;Unrealized Gain/Loss;Realized Gain/Loss;Asset Allocation;Download;Statements & Documents;Account Info";
			break;
		case "Tradings":
			if(tradingValues.equalsIgnoreCase("")){
				strExpValues = "Equities;Mutual Funds;Options;Order Status";
			}else{
				strExpValues = tradingValues;
			}
			break;
		case "Portfolio":
			strExpValues = "Dashboard;Investments;Performance;Volatility;Account Statistics;Educational Insights";
			break;
			
		case "Research":
			strExpValues = "Find Symbol;Option Chains;Market Overview;Advanced Chart;Market Events Calendar;Stock Alerts;Stock Fundamentals Report;Mutual Fund / ETF Report;Screener";
			break;
			
		case "Tools & Calculators":
			strExpValues = "College Cost Calculator;IRA Comparison Calculator;IRA Eligibility Calculator;IRA Roth Conversion Calculator;Retirement Cost Calculator";
			break;
			
		case "Account Services":
			strExpValues = "Transfer Money;Secure Message Center;Preferences;Open a New Account;Change Address;Change Email Address;Register for Mobile Access;Change Mobile Password;Change Login Password;Account Nicknames;Forms;Help";
			break;
		default:
			System.out.println("Method : BOW_ExpectedNavigationPanelDDList >> No matching Select-Case string found.");
			strExpValues = "";
			break;
		}
		//System.out.println(strActualValues);
		return strExpValues;
	}	*/

	//######################################################################################################################
	//######################################################################################################################
	//Name - check_PageNavigation_viaNavPanel
	//Description -This function verifies that a page loads successfully when navigated via Navigation panel
	//Parameters - row num, page name
	//Returns - Void
	//Created By - Neelesh Vatsa
	//######################################################################################################################
		//######################################################################################################################
	/*public void check_PageNavigation_viaNavPanel(int rowNum, String strPageName, String strExpectedPageTitle) throws Exception {
	try{// TODO Auto-generated method stub
		
		wait = new WebDriverWait(driver,50);
		String pageTitle = "";
		strPageName = strPageName.trim();
		WM_CommonFunctions.wait_ForPageLoad(rowNum);
		//wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
		//Actions action = new Actions(driver);
		//Switch stmnt for each page option
		switch (strPageName){
		//navigate to the page
		case "Summary": case "Balances": case "Positions": case "Positions Zoom": case "Activity": case "Unrealized Gain/Loss":	
		case "Realized Gain/Loss": case "Asset Allocation": case "Download": case "Statements & Documents": case "Account Info":
			Thread.sleep(50);
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(
			        ExpectedConditions.elementToBeClickable(tab_Accounts)));
			//Perform hover-over action to see dropdown list
			WM_CommonFunctions.hoverOver_Item(tab_Accounts, "Account Tab", rowNum);
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Accounts, strPageName);
			//wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			Thread.sleep(50);
			WM_CommonFunctions.wait_ForPageLoad(rowNum);
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			break;
		
		//END of Accounts drop down  list	
		//########  Start of Dropdowns for Tradings  ########
		case "Equities": case "Mutual Funds": case "Options": case "Order Status":
			Thread.sleep(50);
			//Perform hover-over action to see dropdown list
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tab_Trading)));
			WM_CommonFunctions.hoverOver_Item(tab_Trading, "Trading Tab", rowNum);
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Trading, strPageName);
			Thread.sleep(50);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			break;
		//end of Trading drop down list	
		case "Dashboard": case "Investments": case "Performance": case "Volatility": case "Account Statistics": case "Educational Insights":
			Thread.sleep(50);
			//Perform hover-over action to see dropdown list
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tab_Portfolio)));
			WM_CommonFunctions.hoverOver_Item(tab_Portfolio, "Portfolio Tab", rowNum);
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Portfolio, strPageName);
			Thread.sleep(50);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_TitlePortfolio, rowNum)).trim();
			break;
			//end of Portfolio drop down list
			
		case "Find Symbol" : case "Option Chains" : case "Market Overview" : case "Advanced Chart" : case "Market Events Calendar" : 
		case "Stock Alerts" : case "Stock Fundamentals Report" : case "Mutual Fund / ETF Report" : case "Screener" :
			Thread.sleep(50);
			//Perform hover-over action to see dropdown list
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tab_Portfolio)));
			WM_CommonFunctions.hoverOver_Item(tab_Research, "Research Tab", rowNum);
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_Research, strPageName);
			Thread.sleep(50);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			break;
			//end of Research drop down list
			
		case "College Cost Calculator" : case "IRA Comparison Calculator" : case "IRA Eligibility Calculator" : case "IRA Roth Conversion Calculator" : 
		case "Retirement Cost Calculator" : 
			Thread.sleep(50);
			//Perform hover-over action to see dropdown list
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tab_Tools)));
			WM_CommonFunctions.hoverOver_Item(tab_Tools, "Tools Tab", rowNum);
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_ToolsNCal, strPageName);
			Thread.sleep(50);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			Thread.sleep(2000);
			driver.switchTo().frame(0);
			pageTitle = (WM_CommonFunctions.object_getText(page_TitleTools, rowNum)).trim();
			System.out.println("title is " + pageTitle);
			driver.switchTo().defaultContent();
			break;
			//end of Tools & Calculators drop down list
			
		case "Transfer Money" : case "Secure Message Center" : case "Preferences" : case "Forms" : case "Help":
			Thread.sleep(50);
			//Perform hover-over action to see dropdown list
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tab_AccountServices)));
			WM_CommonFunctions.hoverOver_Item(tab_AccountServices, "Account Services Tab", rowNum);
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_AccountServices, strPageName);
			Thread.sleep(50);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			break;
			
		case "Open a New Account" : 
			Thread.sleep(1000);
			//Perform hover-over action to see dropdown list
			wait.ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tab_AccountServices)));
			WM_CommonFunctions.hoverOver_Item(tab_AccountServices, "Account Services Tab", rowNum);
			WM_CommonFunctions.dropwdown_ClickOnOption(rowNum, dd_AccountServices, strPageName);
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(panel_Navigation));
			pageTitle = (WM_CommonFunctions.object_getText(page_Title, rowNum)).trim();
			driver.navigate().back();
			break;
			//end of Account Services drop down list
		default:
			System.out.println("Drop down option not found in switch -case, please check");
			Reporting.reportStep(rowNum, "Failed", strPageName + " - not found in expected list of option names. Check Swich stmnt.", "", "", driver, "", "", null);	
			break;
		}
		Thread.sleep(100);
		
		//match the page title with page name and add passed or failed in the report accordingly
		if(!pageTitle.equals("")){
			if(pageTitle.equals(strExpectedPageTitle.trim())){
				System.out.println("PASSED - Page navigation check Passed for - " + strPageName);
				Reporting.reportStep(rowNum, "Passed", strPageName + " - has loaded successfully.", "", "", driver, "", "", null);	
			}else{
				System.out.println(pageTitle);
				System.out.println(strExpectedPageTitle);
				System.out.println("FAILED - Page navigation check failed for - " + strPageName + " Expected / Actual = " + strExpectedPageTitle + "/" +pageTitle);
				Reporting.reportStep(rowNum, "Failed", strPageName + " - page did not load successfully Actual / Expected = " + pageTitle + "/" +  strExpectedPageTitle, "", "BOW_PageLoadCheck", driver, "", "", null);	
			}//End of nested If-Else
		}else{
			Reporting.reportStep(rowNum, "Failed", strPageName + " - page title could not be located by the script.", "", "BOW_PageLoadCheck", driver, "", "", null);	
		}//End of If-Else
	}//End of Try block
	catch (Exception e){
		e.printStackTrace();
		Reporting.reportStep(rowNum, "Failed", strPageName + " - Exception thrown while locating page.", "", "BOW_PageLoadCheck", driver, "", "", null);	
		
	}//End of catch block
	}*/
//************************  End of functions **********************************
//************************************************************************************************************


}
	