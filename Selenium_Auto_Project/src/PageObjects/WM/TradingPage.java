package PageObjects.WM;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.DateUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;

public class TradingPage {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	WebDriverWait wait;
	
	// Objects
	//Entire Trading form
	@FindBy(how = How.ID, using = "enterForm")
	public WebElement screenshot_Object;

	//Transaction/Action Type dropdown
	@FindBy(how = How.ID, using = "transaction")
	public WebElement obj_Action;
	
	@FindBy(how = How.CSS, using = "#transaction option")
	public WebElement ddObj_Action;

	//Amount or Shares textbox
	@FindBy(how = How.ID, using = "amount")
	public WebElement obj_Shares;
	
	//Amount error message (path wen Preview is clicked without filling any value
	@FindBy(how = How.CSS, using = "#main>li:nth-child(2)")
	public WebElement obj_SharesError;
	
	//Symbol textbox
	@FindBy(how = How.ID, using = "symbol")
	public WebElement obj_Symbol;

	//Order Type dropdown
	@FindBy(how = How.ID, using = "ordType")
	public WebElement obj_OrderType;
	
	//Limit Price textbox
	@FindBy(how = How.ID, using = "limitPrice")
	public WebElement obj_limitPrice;
	
	//Stop Price textbox
	@FindBy(how = How.ID, using = "stopPrice")
	public WebElement obj_stopPrice;
	
	//StopLimit = Stop Price textbox
	@FindBy(how = How.ID, using = "stopLimitStopPrice")
	public WebElement obj_stopLimitStopPrice;
	//StopLimit = Limit Price textbox
	@FindBy(how = How.ID, using = "stopLimitLimitPrice")
	public WebElement obj_stopLimitLimitPrice;
	
	
	//DropDown for order type
	@FindBy(how = How.CSS, using = "#ordType option")
	public WebElement ddObj_OrderType;
	
	//Duration dropdown object and list
	@FindBy(how = How.ID, using = "timeInForce")
	public WebElement obj_Duration;
	
	@FindBy(how = How.CSS, using = "#timeInForce option")
	public WebElement ddObj_Duration;
	
	//Account Type dd object and list
	@FindBy(how = How.ID, using = "acctType")
	public WebElement obj_AccountType;
	
	@FindBy(how = How.CSS, using = "#acctType option")
	public WebElement ddObj_AccountType;
	
	//Qualifier dd object and list
	@FindBy(how = How.ID, using = "execInst")
	public WebElement obj_Qualifier;
	
	@FindBy(how = How.CSS, using = "#execInst option")
	public WebElement ddObj_Qualifier;
	
	//Symbol lookup link
	@FindBy(how = How.ID, using = "symbol-slqLink")
	public WebElement obj_SymbolLookup;
			
	//Symbol lookup text field
	@FindBy(how = How.CSS, using = ".tradeTicket input[name=fastQuoteSymbolLookup]" )
	public WebElement symbolLookup_textbox;
	
	
	//Symbol lookup pop-up
	@FindBy(how = How.CSS, using = ".x-layer.x-combo-list")
	public WebElement trading_SymbolLookup_Widget;
	
	//Symbol lookup - first cell symbol in pop-up
	@FindBy(how = How.CSS, using = ".x-combo-selected .symbolLookup .companyColumn")
	public WebElement trading_SymbolLookup_Symbol;
	
	
	//RealTime Quote box
	@FindBy(how = How.CSS, using = ".quoteTable")
	public WebElement trading_QuoteBox;
	
	//quote box symbol
	@FindBy(how = How.ID, using = "quotePanelSymbol0")
	public WebElement obj_QuoteSymbol;
	
	// Quote box - Your Positions for existing symbol
	@FindBy(how = How.CSS, using = "#positions_table_0 .x-grid3-cell-inner.x-grid3-col-symbol.x-unselectable>span")
	public WebElement quoteBox_YourPositions;
	
	// Quote box - Your Positions for existing symbol
	@FindBy(how = How.CSS, using = "#orders_table_0 .x-grid3-cell-inner.x-grid3-col-symbol.x-unselectable")
	public WebElement quoteBox_YourOrders;
	
	//Preview Button
	@FindBy(how = How.ID, using = "action_preview")
	public WebElement obj_PreviewOrderBtn;
	
	//Clear Button
	@FindBy(how = How.ID, using = "action_clearForm")
	public WebElement obj_ClearBtn;
	
	//Account balance Section
	@FindBy(how = How.CSS, using = ".balances>h2")
	public WebElement obj_AccountBalances;
	
	//QuoteWidget Wait
	//
	@FindBy(how = How.CSS, using = "#positionsGrid0 .ext-el-mask-msg")
	public WebElement obj_QuoteWidget_urPositions_Loading;
	
	
	//--------------  Constructor to initialize all elements  --------------
	public TradingPage(){
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }
	//Description - This functions sets the object for screenshot purpose. If Filter panel 
	//itself is not present, then null object is returned in which case full page screenshot is taken.
	//###########################################################
	public  WebElement Set_ParentObjForImg() throws Exception{
		if(CommonUtils.isElementPresent(screenshot_Object) == false){
			screenshot_Object = null;
		}
		return screenshot_Object;
	}
	
//*************************************************   FUNCTIONS   **********************************************************
//**************************************************************************************************************************
		WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
		
	//######################################################################################################################
	//######################################################################################################################
	//Name - tradingPage_UITests
	//Description - This function verifies the UI of Trading >> Preview Order page. Its only applicable for rows with keyword = UI Testing.
	//Argument - row no:, filed name, expected dropdown values, field value to be used.
	//Returns - It returns true if verification passed else returns false.
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
		
	public boolean tradingPage_UITests(int rowNum, String fieldName, String expected_drodownValue, String fields_Value, String strClient) throws Exception {
		WebElement screenshot_Object = Set_ParentObjForImg();
		try{
			//1 - First check the presence of object
			//***************************************************************************
			//use generic function to get object name under test
			String objectName = "obj_" + fieldName;
			WebElement testObject = get_ObjectOfGivenName(objectName);
			String default_Value = "";
			//Verify its present
			if(CommonUtils.isElementPresent(testObject) == true){
				//Report
				Reporting.reportStep(rowNum, "Passed", fieldName + " - field is present.", "", "Trading_UICheck", driver, "", "", null);
				System.out.println("PASSED  >> " + fieldName + " - field is present in Tradings page." );
			}else{
				Reporting.reportStep(rowNum, "Failed", fieldName + " - field is not present. ", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> " + fieldName + " - field is not present in Tradings page." );
				return false;
			}//End of verification if-else 
			//2 - If dropdown cell value is not blank (ie object under test is  a dropdown)>> check dropwdown option values and default selection
			//*********************************************************************************************
			if(!(expected_drodownValue.trim().equals("##"))){
				//Blank cell value is represented by "##" in data sheet thus above check.
				// check expected drop down option values
				String strActualDD_Value;
				strActualDD_Value = WM_CommonFunctions.selectDD_GetAllActualOptions(rowNum, testObject);
				if(strActualDD_Value.equals(expected_drodownValue)){
					Reporting.reportStep(rowNum, "Passed", fieldName + " - dropdown values are as expected.", "", "Trading_UICheck", driver, "", "", null);
					System.out.println("PASSED  >> " + fieldName + " - Dropdown values are correct." );
				}else{
					Reporting.reportStep(rowNum, "Failed", fieldName + " - dropdown values dont match expected result . Actul / Expected = " + strActualDD_Value + "/" + expected_drodownValue, "", "Trading_UICheck", driver, "", "", screenshot_Object);	
					System.out.println("FAILED  >> " + fieldName + " - Dropdown values dont match expected result. Actul / Expected = " + strActualDD_Value + "/" + expected_drodownValue);
					return false;
				}//End of verification if-else 
			}//End of If - to check Dropdown values
			//Check default selected value
			//get default value from UI for textbox or dropdown field.
			if(!(expected_drodownValue.trim().equals("##"))){
				//for dropdown
				default_Value = new Select(testObject).getFirstSelectedOption().getText();
			}else{
				//for textbox
				default_Value = testObject.getText().trim();
			}//End of If-else to get default data
				String expected_DefaultValue = get_DefaultDdSelection(fieldName, strClient);
				//Verify if 2 values are match
				if(default_Value.equals(expected_DefaultValue)){
					Reporting.reportStep(rowNum, "Passed", fieldName + " - dropdown has expected default value selected.", "", "Trading_UICheck", driver, "", "", null);
					System.out.println("PASSED  >> " + fieldName + " - default dropdown values is as expected." );
				}else if((fieldName.equals("Symbol")|| fieldName.equals("Shares"))){
					Reporting.reportStep(rowNum, "Failed", fieldName + " - default dropdown values dont match expected result . Actul / Expected = " + default_Value + "/" + expected_DefaultValue, "", "Trading_UICheck", driver, "", "", screenshot_Object);	
					System.out.println("FAILED  >> " + fieldName + " - default Dropdown values dont match expected result. Actul / Expected = " + default_Value + "/" + expected_DefaultValue);
					return false;
				}//End of verification if-else
			
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page fields could not be verified. Test Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "RGL_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>tradingPage_UITests, class = Trading Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch block
	}//End of Function
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_DefaultDdSelection
	//Description - This function returns expected default value selected in a dropdown.
	//Argument - Dropdown name
	//Created By - Neelesh Vatsa
	//######################################################################################################################
	//######################################################################################################################
	
	public String get_DefaultDdSelection(String filed_Name, String strClient){
		String deafultValue = "";
		switch (filed_Name) {
		case "Action": 
			deafultValue = "Select One";
			break;
		case "OrderType": 
			if(strClient.equals("STF")){
				deafultValue = "Select One";
			}else{
				deafultValue = "Market";
			}
			break;
		case "Price": 
			deafultValue = "Select One";
			break;
		case "Duration":
			if(strClient.equals("STF")){
				deafultValue = "Select One";
			}else{
				deafultValue = "Day Order";
			}
			break;
		case "AccountType": 
			deafultValue = "Select One";
			break;
		case "Qualifier": 
			deafultValue = "None";
			break;
		case "Symbol": 
			deafultValue = "";
			break;
		case "Shares": 
			deafultValue = "";
			break;
		default:
			deafultValue = "Invalid Field Name";
			break;
		}
		return deafultValue;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - get_ObjectOfGivenName
	//Description - This function maps a object name to its object on trading page. 
	//Argument -  Name of the field.
	//Returns - This function returns a test object whose name matches with the given string .
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	
	public WebElement get_ObjectOfGivenName(String strName){
		WebElement returnObj = null;
		switch (strName) {
		case "obj_Action": 
			returnObj = obj_Action;
			break;
		/*case "ddObj_Action":
			returnObj =ddObj_Action;
			break;*/
		case "obj_Shares":
			returnObj = obj_Shares;
			break;
		case "obj_Symbol":
			returnObj = obj_Symbol;
			break;
		case "obj_OrderType":
			returnObj = obj_OrderType;
			break;	
		/*case "ddObj_OrderType":
			returnObj = ddObj_OrderType;
			break;*/
		case "obj_Duration":
			returnObj = obj_Duration;
			break;
		/*case "ddObj_Duration":
			returnObj = ddObj_Duration;
			break;*/
		case "obj_AccountType":
			returnObj = obj_AccountType;
			break;
		case "obj_Qualifier":
			returnObj = obj_Qualifier;
			break;
		case "obj_SymbolLookup":
			returnObj = obj_SymbolLookup;
			break;
		case "obj_PreviewOrderBtn":
			returnObj = obj_PreviewOrderBtn;
			break;
		case "obj_ClearBtn":
			returnObj = obj_ClearBtn;
			break;
		case "obj_AccountBalances":
			returnObj = obj_AccountBalances;
			break;
		default:
			System.out.println("Trading >> get_ObjectOfGivenName >> Object name Passed not found. Please check!!");
			returnObj = null;
			break;
		}
		return returnObj;
	}
	//######################################################################################################################
	//######################################################################################################################
	//Name - tradingPage_FunctionalTests
	//Description - This function verifies functional smoke tests like symbol search , symbol lookup.
	//Argument -  Name of the field.
	//Returns - This function returns a test object whose name matches with the given string .
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean tradingPage_FunctionalTests(int rowNum, String fieldName, String dropDown_Value, String fields_Value, String strClient) throws Exception {
		WebElement screenshot_Object = Set_ParentObjForImg();
		try{
		
			switch (fieldName){
			//Quote Widget verifies that symbol searched from symbol textbox, opens a quote widget window with same symbol.
			case "QuoteWidget":
				trading_ST_QuoteWidget(rowNum,  fields_Value,  fieldName);
				break;
			//Verify Quotebox section for Your Positions displays the test data symbol
			case "Existing_QuoteWidget_Positions":
				trading_ST_QuoteWidget_YourPositions(rowNum,  fields_Value,  fieldName);
				break;
			//Not part of smoke test
			case "Existing_QuoteWidget_Orders":
				trading_ST_QuoteWidget_YourOrders(rowNum,  fields_Value,  fieldName);
				break;
			//This case verifies symbol lookup is working. User can clikc on link and that opens a widget with search symbol in 1st cell.
			case "SymbolLookup":
				trading_ST_SymbolLookup(rowNum,  fields_Value,  fieldName);
				break;
			//This case verifies that clear button is working.
			case "Clear":
				trading_ST_ClearBtn(rowNum,  fields_Value,  fieldName, strClient);
				break;
			//This case verifies that an error is thrown if Share quantity is not a whole number
			case "Shares_Check":
				trading_ST_QtyCheck(rowNum,  fields_Value,  fieldName, strClient);
				break;
			// This case check text boxes for stop, limit and stoplimit selection	
			case "OrderType_Options":
				trading_ST_OrderTypeCheck(rowNum,  fields_Value,  fieldName);
				break;
			//Test data is invalid for keyword	
			default:
				Reporting.reportStep(rowNum, "Failed", fieldName + " - This filed name is not valid. Please check!!", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> " + fieldName + " - This field name is not valid for Trading functional test. Please check!!");
				return false;	
			}//End of Switch stmnt
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Filter Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>tradingPage_FunctionalTests, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch
	}//End of Function

	//######################################################################################################################
	//######################################################################################################################
	//Name - trading_ST_QuoteWidget
	//Description - This function enters a symbol and checks that quote widget shows up with same symbol
	//Argument -  row number, test data value of the field,  Name of the field.
	//Returns - true or flase based on verification.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean trading_ST_QuoteWidget(int rowNum, String fields_Value, String fieldName) throws InterruptedException{
		try{
			//enter symbol and check quote widget shows up with same symbol
			obj_Symbol.sendKeys(fields_Value.trim());
			//click anywhere outside symbol textbox for search to work and wait for page load
			obj_Shares.click();
			Thread.sleep(100);
			//WM_CommonFunctions.wait_ForPageLoad(rowNum);
			//Thread.sleep(10);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, trading_QuoteBox, "SymbolLookup Symbol")){
				Reporting.reportStep(rowNum, "Failed", "Symbol lookup's symbol object is not visible.", "", "Trading_SyLookupUICheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> SymbolLookup object not found.");
				return false;
			}
			//Verify if Quotebox appears
			if(CommonUtils.isElementPresent(trading_QuoteBox) == true){
				//Report
				Reporting.reportStep(rowNum, "Passed", fieldName + " - element is present.", "", "Trading_UICheck", driver, "", "", null);
				System.out.println("PASSED  >> " + fieldName + " - element is present in Tradings page." );
				//check symbol value in quotebox matches symbol search by test data.
				if(obj_QuoteSymbol.getText().equals(fields_Value)){
					Reporting.reportStep(rowNum, "Passed", fieldName + " - displays searched symbol - " + fields_Value, "", "Trading_UICheck", driver, "", "", null);
					System.out.println("PASSED  >> " + fieldName + " - displays searched symbol." );
				}else{
					Reporting.reportStep(rowNum, "Failed", fieldName + " - does not display searched symbol - " + fields_Value, "", "Trading_UICheck", driver, "", "", screenshot_Object);	
					System.out.println("FAILED  >> " + fieldName + " - does not display searched symbol - " + fields_Value);
					return false;
				}//end of if-else for symbol check
			}else{
				Reporting.reportStep(rowNum, "Failed", fieldName + " - element is not present. ", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> " + fieldName + " - element is not present in Tradings page." );
				return false;
			}//end of if-else for quotebox presence check
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Quote Widget Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>trading_ST_QuoteWidget, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch
	}//End of Function####################################################################################################################
	//######################################################################################################################
	//Name - trading_ST_QuoteWidget_YourPositions
	//Description - This function verifies Your Position symbol in quotebox.
	//Argument -  row number, test data value of the field,  Name of the field.
	//Returns - true or flase based on verification.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean trading_ST_QuoteWidget_YourPositions(int rowNum, String fields_Value, String fieldName) throws InterruptedException{
		try{
				//click on shares texbox in case any dopdown may be open from previous test case
				obj_Shares.click();
				//Enter symbol 
				obj_Symbol.sendKeys(fields_Value.trim());
				//again clikc on shares for search to work
				obj_Shares.click();
				Thread.sleep(700);
				WM_CommonFunctions.wait_ForObject_ToDisappear(rowNum, obj_QuoteWidget_urPositions_Loading, "Your Positions");
				boolean flgPass = true;
				WM_CommonFunctions.wait_ForPageLoad(rowNum);
				//Verify is You Position object is present
				if(!CommonUtils.isElementPresent(quoteBox_YourPositions) == true){
					Reporting.reportStep(rowNum, "Failed", "Quote widget - Symbol object for your positions not found.", "", "Trading_FunctionalCheck", driver, "", "", screenshot_Object);	
					System.out.println("FAILED  >> Quote widget - Symbol object for your positions not found.");
					WM_CommonFunctions.delete_textboxValue(rowNum, obj_Symbol);
					return false;
				}//End of verification if
				//Verify if Symbol in Your Position is same as test data symbol
				if(quoteBox_YourPositions.getText().equals(fields_Value)){
					Reporting.reportStep(rowNum, "Passed", fieldName + " - symbol displayed in Your Positions." + fields_Value, "", "Trading_FunctionalCheck", driver, "", "", null);
					System.out.println("PASSED  >> " + fieldName + " - symbol displayed in Your Positions." );
					flgPass = true;
				}else{
					Reporting.reportStep(rowNum, "Failed", fieldName + " - symbol is not displayed in Your Positions - " + fields_Value, "", "Trading_FunctionalCheck", driver, "", "", screenshot_Object);	
					System.out.println("FAILED  >> " + fieldName + " - symbol does not display in Your Positions - " + fields_Value);
					flgPass = false;
				}//End of verification if
				//cleanup - clear symbol value from search
				WM_CommonFunctions.delete_textboxValue(rowNum, obj_Symbol);
				return flgPass;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Your Positions Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>trading_ST_QuoteWidget_YourPositions, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch
	}//End of Function	
	//######################################################################################################################
	//######################################################################################################################
	//Name - trading_ST_QuoteWidget_YourOrders
	//Description - This function verifies Your Orders symbol in quotebox.
	//Argument -  row number, test data value of the field,  Name of the field.
	//Returns - true or flase based on verification.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean trading_ST_QuoteWidget_YourOrders(int rowNum, String fields_Value, String fieldName) throws InterruptedException{
		try{
			trading_ST_QuoteWidget_YourOrders(rowNum,  fields_Value,  fieldName);
			//enter symbol and check quote widget shows up with same symbol
			obj_Shares.click();
			obj_Symbol.sendKeys(fields_Value.trim());
			obj_Shares.click();
			Thread.sleep(5000);
			WM_CommonFunctions.wait_ForPageLoad(rowNum);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded(true);",quoteBox_YourOrders);
			if(!CommonUtils.isElementPresent(quoteBox_YourOrders) == true){
				Reporting.reportStep(rowNum, "Failed", "Quote widget - Symbol object for Your Orders not found.", "", "Trading_FunctionalCheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> Quote widget - Symbol object for Your Orders not found.");
				WM_CommonFunctions.delete_textboxValue(rowNum, obj_Symbol);
				return false;
			}//End of verification if
			//Verify if Symbol in Your Order is same as test data symbol
			if(quoteBox_YourOrders.getText().equals(fields_Value)){
				Reporting.reportStep(rowNum, "Passed", fieldName + " - symbol displayed in Your Orders." + fields_Value, "", "Trading_FunctionalCheck", driver, "", "", null);
				System.out.println("PASSED  >> " + fieldName + " - symbol displayed in Your Orders." );
			}else{
				Reporting.reportStep(rowNum, "Failed", fieldName + " - symbol is not displayed in Your Orders - " + fields_Value, "", "Trading_FunctionalCheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> " + fieldName + " - symbol does not display in Your Orders - " + fields_Value);
			}//End of verification if
			//cleanup - clear symbol value from search
			WM_CommonFunctions.delete_textboxValue(rowNum, obj_Symbol);
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Your Orders Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>trading_ST_QuoteWidget_YourOrders, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch
	}//End of Function
	//######################################################################################################################
	//######################################################################################################################
	//Name - trading_ST_ClearBtn
	//Description - This function verifies Clear button cleans up all selection and values go back to deafult selection.
	//Argument -  row number, test data value of the field,  Name of the field.
	//Returns - true or flase based on verification.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean trading_ST_ClearBtn(int rowNum, String fields_Value, String fieldName, String strClient) throws InterruptedException{
		try{
			//clear any previous symbol value
			obj_Symbol.click();
			WM_CommonFunctions.delete_textboxValue(rowNum, obj_Symbol);
			//Created 2 array. 1 for each object and other for value to be put in each object.
			//array to get object for each field
			String[] list_AllFields = ("Action;Shares;Symbol;OrderType;Duration;AccountType;Qualifier").split(";");
			//array for value to be used in each field
			String[] list_AllFields_Value = (fields_Value).split(";");
			//loop to enter value in each object
			WebElement testObj = null;
			for(int i = 0; i<list_AllFields.length; i++){
				//get object for given name
				testObj = (get_ObjectOfGivenName("obj_" + list_AllFields[i]));
				if(!((list_AllFields[i].equals("Symbol")) || (list_AllFields[i].equals("Shares")))){
					WM_CommonFunctions.selectDD_clickOnOption(rowNum, testObj, list_AllFields[i], list_AllFields_Value[i]);
				}else{
					testObj.sendKeys(list_AllFields_Value[i]);
				}//End of if to enter value
			}//End of For loop
			
			Thread.sleep(50);
			//Click on Clear button after entering all values
			obj_ClearBtn.click();
			
			//check all fields are back to default values
			String actual_DefaultValue, expected_DefaultValue = "";
			for(int i = 0; i<list_AllFields.length; i++){
				expected_DefaultValue = get_DefaultDdSelection(list_AllFields[i], strClient);
				//get default values from UI
				if(!((list_AllFields[i].equals("Symbol")) || (list_AllFields[i].equals("Shares")))){
					actual_DefaultValue = new Select(get_ObjectOfGivenName("obj_" + list_AllFields[i])).getFirstSelectedOption().getText();
				}else{
					actual_DefaultValue = (get_ObjectOfGivenName("obj_" + list_AllFields[i]).getText()).trim();
				}
				//compare actual n expected default value
				if(actual_DefaultValue.equals(expected_DefaultValue)){
					Reporting.reportStep(rowNum, "Passed", "After Clear action >> " + list_AllFields[i] + " - dropdown has expected default value selected.", "", "Trading_UICheck", driver, "", "", null);
					System.out.println("PASSED  >> " + list_AllFields[i] + " - default dropdown values is as expected." );
				}else{
					Reporting.reportStep(rowNum, "Failed", "After Clear action >> " + list_AllFields[i] + " - default dropdown values dont match expected result . Actul / Expected = " + actual_DefaultValue + "/" + expected_DefaultValue, "", "Trading_UICheck", driver, "", "", screenshot_Object);	
					System.out.println("FAILED  >> " + list_AllFields[i] + " - default Dropdown values dont match expected result. Actul / Expected = " + actual_DefaultValue + "/" + expected_DefaultValue);
					//return false;
				}//End of verification if
			}
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Your Orders Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>trading_ST_ClearBtn, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch
	}//End of Function
	//######################################################################################################################
	//Name - trading_ST_QtyCheck
	//Description - This function verifies non whole number for Shares throws an error.
	//Argument -  row number, test data value of the field,  Name of the field.
	//Returns - true or flase based on verification.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean trading_ST_QtyCheck(int rowNum, String fields_Value, String fieldName, String strClient) throws InterruptedException{
		try{
			//enter a non whole number value from test data 
			obj_Shares.sendKeys(fields_Value);
			//click on Preview Order button
			obj_PreviewOrderBtn.click();
			String expectedMsg = "";
			if(strClient.equals("USB")){
				expectedMsg = "Shares should be a whole number.";
			}else{
				expectedMsg = "Amount should be a whole number.";
			}
			//Verify an error message is thrown for non whole number value
			String errorMsg = obj_SharesError.getText();
			if(errorMsg.equals(expectedMsg)){
				Reporting.reportStep(rowNum, "Passed", "Shares field throws an error if quantity is not a whole number.", "", "Trading_UICheck", driver, "", "", null);
				System.out.println("PASSED  >> Shares field only takes whole number." );
			}else{
				Reporting.reportStep(rowNum, "Failed", "Shares field did not throw an error for non whole number value.", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("Failed  >> Shares field did not throw an error for non whole number value.");
				return false;
			}//End of verification if
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Shares or invalid share amount error message,  Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>trading_ST_QtyCheck, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch
	}//End of Function	
	//######################################################################################################################
	//Name - trading_ST_OrderTypeCheck
	//Description - This function verifies textboxes for stop, limit and stoplimit.
	//Argument -  row number, test data value of the field,  Name of the field.
	//Returns - true or flase based on verification.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean trading_ST_OrderTypeCheck(int rowNum, String fields_Value, String fieldName) throws InterruptedException{
		try{
			boolean flgVerify = true;
			//Stop price check
			WM_CommonFunctions.selectDD_clickOnOption(rowNum, obj_OrderType, "OrderType_Stop", "Stop");
			if(CommonUtils.isElementPresent(obj_stopPrice) == true){
				//Report
				Reporting.reportStep(rowNum, "Passed", "Order type check for Stop has passed. Stop textbox is present.", "", "Trading_UICheck", driver, "", "", null);
				System.out.println("PASSED  >> Order type check for Stop has passed. Stop textbox is present" );
			}else{
				Reporting.reportStep(rowNum, "Failed", "Order type - Stop textbox is missing from UI.", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("Failed  >> Order type - Stop textbox is missing from UI.");
				flgVerify = false;
			}//End of stop verification If
			//Limit price check
			WM_CommonFunctions.selectDD_clickOnOption(rowNum, obj_OrderType, "OrderType_Stop", "Limit");
			if(CommonUtils.isElementPresent(obj_limitPrice) == true){
				//Report
				Reporting.reportStep(rowNum, "Passed", "Order type check for Stop has passed. Limit textbox is present.", "", "Trading_UICheck", driver, "", "", null);
				System.out.println("PASSED  >> Order type check for Limit has passed. Limit textbox is present" );
			}else{
				Reporting.reportStep(rowNum, "Failed", "Order type - Limit textbox is missing from UI.", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("Failed  >> Order type - Limit textbox is missing from UI.");
				flgVerify = false;
			}//End of limit verification If
			// Stop Limit price check for Stop
			WM_CommonFunctions.selectDD_clickOnOption(rowNum, obj_OrderType, "OrderType_Stop", "Stop Limit");
			Thread.sleep(100);
			if(CommonUtils.isElementPresent(obj_stopLimitStopPrice) == true){
				//Report
				Reporting.reportStep(rowNum, "Passed", "Order type = 'Stop Limit'>> check for Stop has passed. Stop textbox is present.", "", "Trading_UICheck", driver, "", "", null);
				System.out.println("PASSED  >> Order type check for Stop Limit >> Stop has passed. Stop textbox is present" );
			}else{
				Reporting.reportStep(rowNum, "Failed", "Order type - Stop Limit >> Stop textbox is missing from UI.", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("Failed  >> Order type Stop Limit >> Stop - Stop textbox is missing from UI.");
				flgVerify = false;
			}//End of stop verification If
			// Stop Limit price check for Limit
			if(CommonUtils.isElementPresent(obj_stopLimitLimitPrice) == true){
				//Report
				Reporting.reportStep(rowNum, "Passed", "Order type = 'Stop Limit'>> check for Limit textbox has passed. Stop textbox is present.", "", "Trading_UICheck", driver, "", "", null);
				System.out.println("PASSED  >> Order type check for Stop Limit >> Limit textbox has passed. Limit textbox is present" );
			}else{
				Reporting.reportStep(rowNum, "Failed", "Order type = 'Stop Limit'>> Limit textbox is missing from UI.", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("Failed  >> Order type= 'Stop Limit'>>  Limit textbox is missing from UI.");
				flgVerify = false;
			}//End of limit verification If
			return flgVerify;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Your Orders Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>trading_ST_OrderTypeCheck, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of try-catch
	}//End of Function
	//######################################################################################################################
	//Name - trading_ST_QuoteWidget_YourPositions
	//Description - This function verfies symbol look widget works and displays searched symbol.
	//Argument -  row number, test data value of the field,  Name of the field.
	//Returns - true or flase based on verification.
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public boolean trading_ST_SymbolLookup(int rowNum, String fields_Value, String fieldName) throws InterruptedException{
		try{
			//click on the link
			obj_SymbolLookup.click();
			Thread.sleep(100);
			//enter symbol
			symbolLookup_textbox.sendKeys(fields_Value);
			Thread.sleep(10);
			if(!WM_CommonFunctions.wait_dynamicFluentWait(rowNum, trading_SymbolLookup_Symbol, "SymbolLookup Symbol")){
				Reporting.reportStep(rowNum, "Failed", "Symbol lookup's symbol object is not visible.", "", "Trading_SyLookupUICheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> SymbolLookup object not found.");
				return false;
			}
			//check if Symbol widget opens
			if(CommonUtils.isElementPresent(trading_SymbolLookup_Widget) == true){
				Reporting.reportStep(rowNum, "Passed", "Symbol Lookup Widgte is present.", "", "", driver, "", "", null);	
				System.out.println("Passed  >> Symbol lookup widget is present.");
				//Check that symbol present in 1st cell is either same or is contained within the string found.
				if(CommonUtils.isElementPresent(trading_SymbolLookup_Symbol) == true){
					String lookup_Symbol = trading_SymbolLookup_Symbol.getText();
					System.out.println("Symbol lookup displayed value = " + lookup_Symbol);
					if(lookup_Symbol.contains(fields_Value)){
						Reporting.reportStep(rowNum, "Passed", fieldName + " - first cell in widget shows correct symbol - " + fields_Value, "", "Trading_UICheck", driver, "", "", null);
						System.out.println("PASSED  >> " + fieldName + " - searched symbol is correctly displayed in widget." );
					}else{
						Reporting.reportStep(rowNum, "Failed", fieldName + " - searched symbol is not displayed in Symbol lookup widget -Actual / Expected = " + fields_Value + " / " + lookup_Symbol, "", "Trading_UICheck", driver, "", "", screenshot_Object);	
						System.out.println("FAILED  >> " + fieldName + " - exxpected symbol is not displayed in Symbol lookup widget - " + fields_Value);
					}//end of verification if-else
				}else{
					Reporting.reportStep(rowNum, "Failed", fieldName + " - widget does not have symbol object yet.", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
					System.out.println("FAILED  >> " + fieldName + " - symbol object is missing");
				}//end of verification if-else for symbol
			}else{
				Reporting.reportStep(rowNum, "Failed", fieldName + " - widget is missing - ", "", "Trading_UICheck", driver, "", "", screenshot_Object);	
				System.out.println("FAILED  >> " + fieldName + " - widget is missing");
			}////end of verification if-else for Widget
			obj_Shares.click();
			return true;
		}catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Trading page filter could not be verified. Symbol lookup Objects may be missing from UI. Please check!!!. Row - " + rowNum, "", "Trading_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>trading_ST_SymbolLookup, class = Trading_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}//End of Try-catch
	}//End of Function
				
//###############################################################################################################
//###############################################################################################################
}//End of Class
