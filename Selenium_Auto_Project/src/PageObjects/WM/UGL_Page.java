package PageObjects.WM;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class UGL_Page {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	// ************* Filter dropdowns Elements ************************************
	//***************************************************************************
	//Panel to be used for screenshot highlight
	@FindBy(how = How.CSS, using = "#pageTitle>.form-inline")
	public WebElement panel_AllFilters;
	
	@FindBy(how = How.CSS, using = ".asset-class-dropdown")
	public WebElement ddTab_AllAssetTypeFilter;
	
	@FindBy(how = How.CSS, using = ".symbol-class-input input")
	public WebElement txtBox_Symbol;
	
	
	// Alert message 
	@FindBy(how = How.XPATH, using = ".//*[@id='svi-unrealized-gain-loss']/div[1]/div/div/div[1]/div[1]/p")
	public WebElement alert_NoResult_Message;
	

	//--------------  Constructor to initialize all elements  --------------
	public UGL_Page(){
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }


	//########################     FUNCTIONS       #############
	//Decription - This functions sets the object for screenshot purpose. If Filter panel 
	//itself is not present, then null object is returned in which case full page screenshot is taken.
	public  WebElement Set_ParentObjForImg() throws Exception{
		if(CommonUtils.isElementPresent(panel_AllFilters) == false){
			panel_AllFilters = null;
		}
		return panel_AllFilters;
	}
	
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - checkAllNavTabs_arePresent
	//Description - This function verifies that all parent navigation tabs are present.
	//Argument - row no: of test data
	//Returns - This function returns a String variable with the names of all failed / missing tabs seprated by semi colon.
	//Date - Oct 2017
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean checkFilters_ValueAndFunctioning( int rowNum, String strKeyword, String strFilterName, String strAllAssetTypeValue, String strAllTermTypeValue, String strSymbolSearchValue, String strClient) throws Exception{
		//Set screenshot object as Filter panel
		WebElement screenshot_Object = Set_ParentObjForImg();
		try{
			//wait for page title to load
			wait = new WebDriverWait(driver,15);
			wait.until(ExpectedConditions.titleContains("Unrealized Gain/Loss"));
			//wait.until(ExpectedConditions.visibilityOf(page_TitleRgl));
			System.out.println("After UGL Page title presence check..");
			System.out.println("UGL Page Filter test   |     Filter name = " + strFilterName + "    |    For Row = " + rowNum);
			
			//###################################################################################################################################
					//Checkpoint 1 - If keyword is UI Test, verify drop down filters are present with expected values.
					//Test case will fail if a filter is missing or has incorrect/missing dropdown value.
			//###################################################################################################################################
		
			List<WebElement> strDropDown = null;
			WebElement DropdownBtn = null;
			String strExpectedDropDownValues = "";
			ResultTable_Section ResultTable_Section = new ResultTable_Section();
			//Assigning dropdown button and lost for given test case
			//using contain to handle both All Asset Types and All Asset Classes
			if(strFilterName.contains("All Asset ")){
				strDropDown = ResultTable_Section.dd_AllAssetTypes;
				DropdownBtn = ddTab_AllAssetTypeFilter;
				strExpectedDropDownValues = strAllAssetTypeValue;
			}else if(strFilterName.equals("All Term Types")){
				strDropDown = ResultTable_Section.dd_AllTermTypes;
				DropdownBtn = ResultTable_Section.ddTab_AllTermTypeFilter;
				strExpectedDropDownValues = strAllTermTypeValue;
			}//End of IF-Else
			//Verifying dropdown options
			if(strKeyword.equals("UI Test")){
				if(strExpectedDropDownValues.equals("")){
					Reporting.reportStep(rowNum, "Failed", "Filter name is not correct. Does not match expected options -  " + strFilterName , "", "RGL_ddCheck", driver, "", "", screenshot_Object);
					return false;
				}
				//get actual dropdown values and expected dropdown values
				String strActualDropDownOption = WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, strDropDown, DropdownBtn);
				System.out.println("UGL page - Actual DropDown option for " + strFilterName + " = " + strActualDropDownOption);
				System.out.println("UGL Page - Expected DropDown option for " + strFilterName + " = " + strExpectedDropDownValues);
				//compare and set result
				if(StringManipulation.compareString_Match(strActualDropDownOption, strExpectedDropDownValues)){
					System.out.println("UGL Page - Filter UI test has Passed for "+ strFilterName);
					Reporting.reportStep(rowNum, "Passed", strFilterName + " dropdown list has expected values - " + strActualDropDownOption, "", "", driver, "", "", null);
				}else{
					System.out.println("UGL Page - Filter UI test has Failed "+ strFilterName);
					Reporting.reportStep(rowNum, "Failed", strFilterName + " dropdown value check failed. Actual // Expected = " + strActualDropDownOption + "//" + strExpectedDropDownValues, "", "UGL_ddCheck", driver, "", "", screenshot_Object);
					
				}//End of nested IF-else
			}//End of IF
			//###################################################### END of 1st Checkpoint ######################################################
			//###################################################################################################################################
			
			//###################################################################################################################################
				//Checkpoint 2 - If keyword is not UI Test then apply the filter value for one or multiple combination of filters and verify 
				//result table has only those rows that match or qualify applied filter. 
				//Test case will fail if a filter is missing OR filter value to be applied is missing OR no results are displayed in result table.
			//###################################################################################################################################
			else{
				//###################################################################################################################################
				//Apply filters >> Asset, Term or Year
				//###################################################################################################################################
				
				strSymbolSearchValue = strSymbolSearchValue.trim();
				//Apply Symbol search if Symbol value is not ##/blank
				 if(!strSymbolSearchValue.equals("##")){
					//Enter a symbol to be searched
					System.out.println("UGL page - Symbol search filter test");
					WM_CommonFunctions.clickItem( txtBox_Symbol, "Symbol Textbox", rowNum);
					//txtBox_Symbol.click();
					txtBox_Symbol.sendKeys(strSymbolSearchValue);
					TimeUnit.SECONDS.sleep(15);
				 }//End of IF

				//Asset filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Asset Types", strAllAssetTypeValue)){
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF
				 //Term filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Term Types", strAllTermTypeValue)){
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF
				
				//###################################################################################################################################
				//###################################################################################################################################
				
				//###################################################################################################################################
				 			// Wait for page load, check for no result message and then expand all rows.
				//###################################################################################################################################
				//Wait for page to load with expected result table rows, in case spinning "loading" icon is displayed in UI
				 TimeUnit.SECONDS.sleep(5);
				 WM_CommonFunctions.wait_ForPageLoad(rowNum);
				
				//Check if "No Results found" is displayed and thus result table is missing.Add Failed to the report.
				String msg = alert_NoResult_Message.getText();
				if(alert_NoResult_Message.getText().equalsIgnoreCase("There is no unrealized gain/loss to display.")){
					System.out.println("UGL Page - No result found for applied search/filter");
					Reporting.reportStep(rowNum, "Failed", "No Result displayed for this search / Filter. " , "", "UGL_ddCheck", driver, "", "", screenshot_Object);
					return false;
				}//End of IF
				
				//Expand all rows in result table.
				ResultTable_Section.expandAll_Rows(rowNum);
				//###################################################################################################################################
				//###################################################################################################################################
				
				//###################################################################################################################################
							//Test all rows for - Asset Type, Term Type and Year filter test if applicable
				//###################################################################################################################################
				
				//###################################  verify Symbol filter  ###################################
				
				//6 - Test all symbol column values if applicable
				if(!(strSymbolSearchValue.equals("##"))){
					boolean SymbolPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Symbol / CUSIP", strSymbolSearchValue);
					System.out.println("UGL Page - Symbol value in all rows match = " + SymbolPass);
					if(SymbolPass){
						Reporting.reportStep(rowNum, "Passed", "All Symbol / CUSIP col has expected value = " + strSymbolSearchValue, "", "", driver, "", "", null);
						System.out.println("All rows have same value for col - Symbol / CUSIP ==>> " + strSymbolSearchValue);
					}else{
						System.out.println("UGL Page - All Symbol column do not have expected searched symbol value -  "+ strSymbolSearchValue);
						Reporting.reportStep(rowNum, "Failed", "All Symbol column values do not have expected searched symbol value -  "+ strSymbolSearchValue , "", "UGL_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-Else
				}//End of IF
				//###################################  verify All Asset Type filter  ###################################
				//Add Report for client = USB, s All asset filter can not be verified for it
				if((strClient.equalsIgnoreCase("USB")) && (strFilterName.contains("All Asset "))){
					Reporting.reportStep(rowNum, "Not Executed", "", "", "UGL_ddCheck", driver, "", "", screenshot_Object);
					
				}else if((strFilterName.contains("All Asset "))&& (!strClient.equalsIgnoreCase("USB"))){
					System.out.println("strFilterName = " + strFilterName + " and strClient = " + strClient);
					boolean flg_FilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Asset Type", strAllAssetTypeValue);
					if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "All Asset Type col has the applied expected value = " + strAllAssetTypeValue, "", "", driver, "", "", null);
						System.out.println("UGL page - All rows have same value for col - Asset Types ==>> " + strAllAssetTypeValue);
					}else{
						System.out.println("UGL Page - All Asset type column do not have expected applied filter value -  "+ strAllAssetTypeValue);
						Reporting.reportStep(rowNum, "Failed", "All Asset type column values do not have expected applied filter value -  "+ strAllAssetTypeValue , "", "UGL_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-Else
				}//End of IF
				
				//###################################  verify All Term Type filter  ###################################
				if(strFilterName.contains("All Term Types")){
					//******check DateDiff is 1yr or more******
					boolean flg_TermFilterPass = false;
					if(strClient.equalsIgnoreCase("USB")){
						String strTermType = strAllTermTypeValue.replace(" Term", "");
						flg_TermFilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Term", strTermType);
					}else{
						flg_TermFilterPass = ResultTable_Section.verify_AllColValues_QualifyThisTermType(rowNum, strAllTermTypeValue);
					}
					if(flg_TermFilterPass){
						Reporting.reportStep(rowNum, "Passed", "All displayed rows match the applied Term per Acquired date i.e. = " + strAllTermTypeValue, "", "", driver, "", "", null);
						System.out.println("UGL Page - All displayed rows match the applied Term per Acquired date i.e. = " + strAllTermTypeValue);
					}else{
						System.out.println("UGL Page - All Asset type column do not have expected applied filter value -  "+ strAllTermTypeValue);
						Reporting.reportStep(rowNum, "Failed", "All displayed rows do not match the applied Term per Acquired date i.e. ="+ strAllTermTypeValue , "", "UGL_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-Else
					}//End of IF
				//###################################################################################################################################
				//###################################################################################################################################
				 }//End of IF
			return true;
		}//End of Try
		catch(Exception e){
			Reporting.reportStep(rowNum, "Failed", "Exception thrown. UGL page filter could not be verified for row - " + rowNum, "", "UGL_pageCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>checkFilters_ValueAndFunctioning, class = ResultTable_Section. Please check!!! ");
  			e.printStackTrace();
  			return false;
			
		}//End of Catch

	}
	//************************  End of functions **********************************
	//************************************************************************************************************
}
 