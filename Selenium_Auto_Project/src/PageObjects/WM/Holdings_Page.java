package PageObjects.WM;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import functionalLibrary.Global.CommonUtils;
import functionalLibrary.Global.ManageDriver_WM;
import functionalLibrary.Global.Reporting;
import functionalLibrary.Global.StringManipulation;
import functionalLibrary.WM.WM_CommonFunctions;


public class Holdings_Page {
	WebDriver driver = ManageDriver_WM.getManageDriver().getDriver();
	Reporting Reporting = new Reporting();
	StringManipulation StringManipulation = new StringManipulation();
	WebDriverWait wait;
	
	// ************* Filter dropdowns Elements ************************************
	//***************************************************************************
	//Panel to be used for screenshot highlight
	@FindBy(how = How.CSS, using = "#pageTitle>.form-inline")
	public WebElement panel_AllFilters;

	
	//*****************    Positions Page-specific filter    **********************
	// Asset type filter under Asset view
	@FindBy(how = How.CSS, using = "h3.ng-scope>span")
	public List<WebElement> list_AllAssetClasses;
	
	// Alert message 
	@FindBy(how = How.XPATH, using = ".//*[starts-with(@id, 'ext-')]/div[1]/div/div/div[1]/div[1]/p")
	public WebElement alert_NoResult_Message;
	
	//--------------  Constructor to initialize all elements  --------------
	public Holdings_Page(){
        this.driver = driver;
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }


	//########################     FUNCTIONS       #############
	//Description - This functions sets the object for screenshot purpose. If Filter panel 
	//itself is not present, then null object is returned in which case full page screenshot is taken.
	//###########################################################
	public  WebElement Set_ParentObjForImg() throws Exception{
		if(CommonUtils.isElementPresent(panel_AllFilters) == false){
			panel_AllFilters = null;
		}
		return panel_AllFilters;
	}
	WM_CommonFunctions WM_CommonFunctions = new WM_CommonFunctions();
	
	//######################################################################################################################
	//######################################################################################################################
	//Name - checkAllNavTabs_arePresent
	//Description - This function verifies that all parent navigation tabs are present.
	//Argument - row no: of test data
	//Returns - This function returns a String variable with the names of all failed / missing tabs seprated by semi colon.
	//Date - Oct 2017
	//Created By - Neelesh
	//######################################################################################################################
	//######################################################################################################################
	public  boolean checkFilters_ValueAndFunctioning( int rowNum, String strKeyword, String strFilterName, String strAllAssetTypeValue, String strHeldAway, String strClient) throws Exception{
		//Set screenshot object as Filter panel
		WebElement screenshot_Object = Set_ParentObjForImg();
		try{
			//wait for page title to load
			
			wait = new WebDriverWait(driver,15);
			//wait.until(ExpectedConditions.titleContains("Holdings - Investor"));
			wait.until(ExpectedConditions.titleContains(getBrowserTitle_Holdings(strClient)));
			System.out.println("Expected Page title has loaded for Holdings page.");
			System.out.println("Holdings Page Filter test   |     Filter name = " + strFilterName + "    |    For Row = " + rowNum);
			
			//###################################################################################################################################
				//Checkpoint 1 - If keyword is UI Test, verify drop down filters are present with expected values.
				//Test case will fail if a filter is missing or has incorrect/missing dropdown value.
			//###################################################################################################################################
			
			List<WebElement> listDropDown = null;
			WebElement DropdownBtn = null;
			String strExpectedDropDownValues = "";
			ResultTable_Section ResultTable_Section = new ResultTable_Section();
			//Assigning drop down button and list for UI test case
			if(strFilterName.contains("All Asset ")){
				listDropDown = ResultTable_Section.dd_AllAssetTypes;
				DropdownBtn = ResultTable_Section.ddTab_AllAssetTypeFilter;
				strExpectedDropDownValues = strAllAssetTypeValue;
			}else if(strFilterName.contains("Held")){
				listDropDown = ResultTable_Section.dd_HeldAway;
				DropdownBtn = ResultTable_Section.ddTab_HeldAwayFilter;
				strExpectedDropDownValues = strHeldAway;
			}
				//End of IF-Else
			boolean flgReturn = true;
			//Verifying drop down options for keyword = UI test
			if(strKeyword.contains("UI Test")){
				//get actual dropdown values and expected dropdown values
				String strActualDropDownOption = WM_CommonFunctions.dropdown_GetAllActualOptions(rowNum, listDropDown, DropdownBtn);
				//System.out.println("Holdings page - Actual DropDown option for " + strFilterName + " = " + strActualDropDownOption);
				System.out.println("Holdings Page - Expected DropDown option for " + strFilterName + " = " + strExpectedDropDownValues);
				//compare and set result
				if(StringManipulation.compareString_Match(strActualDropDownOption, strExpectedDropDownValues)){
					System.out.println("PASSED >> Holdings Page - Filter UI test has Passed for - "+ strFilterName);
					flgReturn = true;
					Reporting.reportStep(rowNum, "Passed", strFilterName + " dropdown list has expected values - " + strActualDropDownOption, "", "", driver, "", "", null);
				}else{
					System.out.println("FAILED >> Holdings Page - Filter UI test has Failed for - "+ strFilterName);
					if(strActualDropDownOption.equals("")){
						Reporting.reportStep(rowNum, "Failed", strFilterName + " dropdown is empty/missing. ", "", "Page_ddCheck", driver, "", "", screenshot_Object);
					}else{
						Reporting.reportStep(rowNum, "Failed", strFilterName + " dropdown value check failed. Actual // Expected = " + strActualDropDownOption + "//" + strExpectedDropDownValues, "", "Page_ddCheck", driver, "", "", screenshot_Object);
					}
					flgReturn = false;
				}//End of IF-Else
			}//End of IF
			//###################################################### END of 1st Checkpoint ######################################################
			//###################################################################################################################################
			
			//###################################################################################################################################
				//Checkpoint 2 - If keyword is not UI Test then apply the filter value for one or multiple combination of filters and verify 
				//result table has only those rows that match or qualify applied filter. 
				//Test case will fail if a filter is missing OR filter value to be applied is missing OR no results are displayed in result table.
			//###################################################################################################################################
			else{
				//###################################################################################################################################
													//Apply filters >> Asset, Term or Year
				//###################################################################################################################################
				//Asset filter
				 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "All Asset Types", strAllAssetTypeValue)){
					 System.out.println("FAILED >> All Asset Types = " + strAllAssetTypeValue + " >> dropdown filter could not be applied. Skiping further execution.");
					 throw new SkipException("Dropdown filter could not be applied");
				 }//End of IF
				//Held Away filter
				 if(strClient.equalsIgnoreCase("USB")){
					 if(!ResultTable_Section.apply_DropDownFilter(rowNum, "Held at USBI", strHeldAway)){
						 throw new SkipException("Dropdown filter could not be applied");
					 }//End of IF 
				 }
				 
			
				//###################################################################################################################################
				 			// Wait for page load, check for no result message and then expand all rows.
				//###################################################################################################################################
				//Wait for page to load with expected result table rows, in case spinning "loading" icon is displayed in UI
				 Thread.sleep(2000);
				 WM_CommonFunctions.wait_ForPageLoad(rowNum);
					
					
				//Check if "No Results found" is displayed and thus result table is missing.Add Failed to the report.
				Thread.sleep(1000);
				if(CommonUtils.isElementPresent(alert_NoResult_Message)){
					String msg = alert_NoResult_Message.getText();
					if(alert_NoResult_Message.getText().equalsIgnoreCase("There are no holdings to display.")){
						System.out.println("Holdings Page - No result found for applied filter testcase- " + strFilterName);
						Reporting.reportStep(rowNum, "Failed", "No Result displayed for this Filter -  " + strFilterName , "", "page_ddCheck", driver, "", "", screenshot_Object);
						return false;	
					}//End of IF
				}
				 //Expand all rows in result table.
				ResultTable_Section.expandAll_Rows(rowNum);
			
				//###################################################################################################################################
							//Test all rows for - Asset Type
				//###################################################################################################################################
				
				//###################################  verify All Asset Type filter  ###################################
				if(strFilterName.contains("All Asset Types") && strKeyword.equals("FilterTest SecurityView")){
					boolean flg_FilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Asset", strAllAssetTypeValue);
					if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "All rows for Asset Type col, have applied expected value = " + strAllAssetTypeValue, "", "", driver, "", "", null);
						flgReturn = true;
						System.out.println("PASSED  >>  Holdings page - All rows have same value for col - Asset Types ==>> " + strAllAssetTypeValue);
					}else{
						System.out.println("FAILED  >>  Holdings Page - All rows for Asset type column, do not have expected applied filter value -  "+ strAllAssetTypeValue);
						flgReturn = false;
						Reporting.reportStep(rowNum, "Failed", "All Asset type column values do not have expected applied filter value -  "+ strAllAssetTypeValue , "", "Holdings_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-Else
				}//End of IF
				//###################################  verify Held Away filter  ###################################
				if(strFilterName.contains("Held ")){
					boolean flg_FilterPass = ResultTable_Section.verify_AllColValues_MatchThisValue(rowNum, "Description", strHeldAway);
					if(flg_FilterPass){
						Reporting.reportStep(rowNum, "Passed", "All rows for Asset Type col, have applied expected value = " + strHeldAway, "", "", driver, "", "", null);
						flgReturn = true;
						System.out.println("PASSED  >>  Holdings page - All rows have same value for col - Asset Types ==>> " + strHeldAway);
					}else{
						System.out.println("FAILED  >>  Holdings Page - All rows for Asset type column, do not have expected applied filter value -  "+ strHeldAway);
						flgReturn = false;
						Reporting.reportStep(rowNum, "Failed", "All Asset type column values do not have expected applied filter value -  "+ strHeldAway , "", "Holdings_ddCheck", driver, "", "", screenshot_Object);
						
					}//End of IF-Else
				}//End of IF
				
				
				if(strFilterName.contains("All Asset Types") && strKeyword.equals("FilterTest AssetView")){// only for asset type filter
					//create 2 arrays of asset section names, actual from ui and expected from data sheet
					String[] actualListArray = WM_CommonFunctions.convert_ElementList_To_StringArray(list_AllAssetClasses);
					if(strClient.equals("DAV")){
						strAllAssetTypeValue =strAllAssetTypeValue.replace("Cash Equivalents", "Cash");
					} // end of if
								String[] expAssetList = strAllAssetTypeValue.split(";");
					
					CommonUtils CommonUtils = new CommonUtils();
					//compare these 2 arrays
					boolean checkPaased = CommonUtils.compare_2StringArrays(actualListArray, expAssetList, true);
					
					if(checkPaased){
						System.out.println("PASSED  >>  Holdings page Asset View >> Actual / Expected = " + Arrays.toString(actualListArray)+ "/" + Arrays.toString(expAssetList));
						Reporting.reportStep(rowNum, "Passed", "All expected asset class sections are present.", "", "Holdings_AssetClassCheck", driver, "", "", screenshot_Object);
						
					}else{
						System.out.println("FAILED  >>  Holdings page - Asset View sections dont match>> Actual / Expected = " + Arrays.toString(actualListArray)+ "/" + Arrays.toString(expAssetList));
						Reporting.reportStep(rowNum, "Failed", "Expected asset class sections are not present. Actual / Expected = " + Arrays.toString(actualListArray)+ "/" + Arrays.toString(expAssetList), "", "Holdings_ddCheck", driver, "", "", screenshot_Object);
						
					}//end of if-else
					
				}//end of IF
				//###################################################################################################################################
			
			}//End of ELSE
			return flgReturn;
		}catch(Exception e){
			//Reporting.reportStep(rowNum, "Failed", "Holdings page filter could not be verified due to missing objects.", "", "Holdings_ddCheck", driver, "", "", screenshot_Object);
			System.out.println("Exception found in method >>checkFilters_ValueAndFunctioning, class = Holdings_Page. Please check!!! ");
  			e.printStackTrace();
  			return false;
		}

	}
	
	public String getBrowserTitle_Holdings(String strClient){
		String strTitle = "";
		switch(strClient){
		case "STR":
		case "USB":
			strTitle = "Holdings - Investor";
			break;
		case "RWB":
			strTitle = "Positions - Baird Online";
			break;
		case "BOW":
			strTitle = "Accounts - Positions - BancWest";
			break;
		case "LPL":
		case "1DB":
			strTitle = "Positions - Investor";
			break;
		case "DAV":	
			strTitle = "Positions - Davenport";
			break;
		}
		return strTitle;
	}
	//************************  End of functions **********************************
	//************************************************************************************************************
}
 